#version 330

layout (std140, push_constant) uniform PushConsts 
{
    mat4 ProjMtx;
} pushConsts;

in vec2 Position;
in vec2 UV;
in vec4 Color;

out vec2 Frag_UV;
out vec4 Frag_Color;

void main()
{
	Frag_UV 	= UV;
	Frag_Color 	= Color;
	gl_Position = pushConsts.ProjMtx * vec4(Position.xy,0,1);
}
