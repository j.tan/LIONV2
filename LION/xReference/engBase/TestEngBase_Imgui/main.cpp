//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "eng_base.h"
#include "imgui_impl_engbase.h"


//-------------------------------------------------------------------------------------------

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
    //
    // Initialize the app
    //
    g_context::Init();
    g_context::get().m_Scheduler.Init();
    g_context::get().m_Scheduler_LogChannel.TurnOff();

    //
    // Deal with the actual app
    //
    xndptr_s<eng_instance>      Instance;
    eng_device*                 pDevice = nullptr;
    eng_window*                 pWindow = nullptr;

    // Create engine instance
    {
        eng_instance::setup         Setup( (void*)hInstance );

      //  Setup.m_bValidation = true;

        auto Err = eng_instance::CreateInstance( Instance, Setup );
        x_assert( !Err );
    }

    // create a device
    {
        eng_device::setup    Setup;
        auto Err = Instance->CreateDevice( pDevice, Setup );
        x_assert( !Err );
    }

    // create a window 
    {
        eng_window::setup   Setup( *pDevice );
        auto Err = Instance->CreateWindow( pWindow, Setup );  
        x_assert( !Err );
    }

    //
    // Initialize the scene
    //
    static const xarray<xvector3,5>  Centers
    {
        xvector3( 0.0f), 
        xvector3( 1.0f),  
        xvector3( 0.5f),  
        xvector3(-0.5f), 
        xvector3(-1.0f) 
    };

    eng_view                        View;
    eng_texture::handle             hTexture;
    eng_sampler::handle             hSampler;
    eng_shader_vert::handle         hVertShader;
    eng_shader_frag::handle         hFragShader;
    eng_material_type::handle       hMaterial;
    eng_material_informed::handle   hMaterialInformed;
    eng_pipeline::handle            hPipeline;

    //
    // Setup the view
    //
    View.setFov         ( xradian{ 91.5_deg } );
    View.setFarZ        ( 256.0f );
    View.setNearZ       ( 0.1f );

    //
    // Create a default texture
    //
    {
        xbitmap                 Bitmap;
        Bitmap.setDefaultTexture();
        pDevice->CreateTexture( hTexture, Bitmap );
    }

    //
    // Create a Sampler for the texture
    //
    if( (1) )
    {
        eng_sampler::setup Setup( hTexture );
        pDevice->CreateSampler( hSampler, Setup );
    }

    //
    // Create shaders
    //
    {
        //
        // Setup vertex shader
        //
        {
            static 
            #include "Implementation/engVulkan/Implementation/draw.vert.spv.h"
            
            eng_shader_vert::setup Setup
            { 
                { (xbyte*)draw_vert, static_cast<xuptr>(draw_vert_size) }, 
                eng_shader_vert::setup::memory_buffer::IS_SHADER_DATA,
                { nullptr, 0 }
            };

            pDevice->CreateVertexShader( hVertShader, Setup );
        }

        //
        // Setup fragment shader
        //
        {
            static 
            #include "Implementation/engVulkan/Implementation/draw.frag.spv.h"

            eng_shader_frag::setup Setup
            { 
                { (xbyte*)draw_frag, static_cast<xuptr>(draw_frag_size) }, 
                eng_shader_vert::setup::memory_buffer::IS_SHADER_DATA,
                { nullptr, 0 } 
            };

            pDevice->CreateFragmentShader( hFragShader, Setup );
        }
    }

    //
    // Create a material type
    //
    {
        xarray<eng_material_type::sampler_binding_declarations,1> Bindings;
        Bindings.i<0>().m_iBind     = 0;

        eng_material_type::setup Setup;
        Setup.m_nSamplerBindings = Bindings.getCount<int>();
        Setup.m_SamplerBindings.CopyToFrom( Bindings );
        Setup.m_hVertShader = hVertShader;
        Setup.m_hFragShader = hFragShader;

        pDevice->CreateMaterialType( hMaterial, Setup );
    }

    //
    // Create an informed material
    //
    {
        xarray<eng_material_informed::sampler_binding,1> Bindings;
        Bindings.i<0>().m_iBind     = 0;
        Bindings.i<0>().m_hSampler  = hSampler;
        
        eng_material_informed::setup Setup{ Bindings };
        Setup.m_hMaterialType   = hMaterial;

        pDevice->CreateMaterialInformed( hMaterialInformed, Setup );
    }

    //
    // Create a solid pipeline, it is the default
    //
    {
        eng_pipeline::setup Setup;
        Setup.m_Blend.setupAlphaOriginal();
        pDevice->CreatePipeline( hPipeline, Setup );
    }


    //
    // Starting the imgui
    //
    ImGui_engBase_Init( *pDevice, *pWindow, false );

    //
    // Do the Integration
    //
    bool show_another_window = false;
    bool show_test_window = false;
    ImVec4 clear_col = ImColor(114, 144, 154);
    while( pWindow->HandleEvents() )
    {
        auto CameraAngles   = View.getAngles(); 
        CameraAngles.m_Yaw += xradian{ 0.1_deg };
        
        View.LookAt( 2.5f, CameraAngles, Centers[0] );
        
        pWindow->BeginRender( View );
        
        // Updates the view port and all the cached matrices
        View = pWindow->getActiveView();

        ImGui_engBase_NewFrame();

        // 1. Show a simple window
        // Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
        {
            static float f = 0.0f;
            ImGui::Text("Hello, world!");
            ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
            ImGui::ColorEdit3("clear color", (float*)&clear_col);
            if (ImGui::Button("Test Window")) show_test_window ^= 1;
            if (ImGui::Button("Another Window")) show_another_window ^= 1;
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        }

        // 2. Show another simple window, this time using an explicit Begin/End pair
        if (show_another_window)
        {
            ImGui::SetNextWindowSize(ImVec2(200,100), ImGuiSetCond_FirstUseEver);
            ImGui::Begin("Another Window", &show_another_window);
            ImGui::Text("Hello");
            ImGui::End();
        }

        // 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
        if (show_test_window)
        {
            ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);     // Normally user code doesn't need/want to call it because positions are saved in .ini file anyway. Here we just want to make the demo initial state a bit more friendly!
            ImGui::ShowTestWindow(&show_test_window);
        }

        if( 0 )
        {
            auto& CmdList = pWindow->getDisplayCmdList( 0 );
            CmdList.Draw( eng_draw::pipeline{}, [&]( eng_draw& Draw )
            {
                for( auto& Center : Centers ) 
                {
                    Draw.DrawBBox( View.getW2C(), Center, {~0u} );
                }
            });
            pWindow->SubmitDisplayCmdList( CmdList );
        }
        else if( 1 )
        {
            x_job_block     m_Block;
            xuptr           i = 0;

            for( auto& Center : x_iter_ref( i, Centers ) ) m_Block.SubmitJob( 
                [  
                    x_qcapture_ref_const( hSampler ),
                    x_qcapture_val_const( pWindow  ), 
                    x_qcapture_ref_const( Center   ),
                    x_qcapture_val_const( i        ), 
                    x_qcapture_ref_const( View     ),
                    x_qcapture_ref_const( hMaterialInformed ),
                    x_qcapture_ref_const( hPipeline )
                ]()                     
            {
                auto& CmdList = pWindow->getDisplayCmdList( static_cast<f32>(i) );

                //
                // 2d test
                //
                if( 1 )
                CmdList.Draw( eng_draw::pipeline{}, [&]( eng_draw& Draw )
                {
                    auto Buffers = Draw.popBuffers( 5*2, 5*2 );
                    constexpr const float w = 0.5f;
                    constexpr const float h = 0.5f;
                    constexpr const float x = 0.0f;
                    constexpr const float y = 0.0f;
                    constexpr const float z = 0.0f;

                    Buffers.m_Vertices[0].setup( xvector3( x,       y,      z),     xcolor{~0u} );
                    Buffers.m_Vertices[1].setup( xvector3( x + w,   y,      z),     xcolor{~0u} );

                    Buffers.m_Vertices[2].setup( xvector3( x + w,   y,      z),     xcolor{~0u} );
                    Buffers.m_Vertices[3].setup( xvector3( x + w,   y + h,  z),     xcolor{255,0,0,255} );
                                                                    
                    Buffers.m_Vertices[4].setup( xvector3( x + w,   y + h,  z),     xcolor{255,0,0,255} );
                    Buffers.m_Vertices[5].setup( xvector3( x,       y + h,  z),     xcolor{~0u} );
                                                                                                                             
                    Buffers.m_Vertices[6].setup( xvector3( x,       y + h,  z),     xcolor{~0u} );                                   
                    Buffers.m_Vertices[7].setup( xvector3( x,       y,      z),     xcolor{~0u} );

                    Buffers.m_Vertices[8].setup( xvector3( x + w,   y + h,  z),     xcolor{255,0,0,255} );
                    Buffers.m_Vertices[9].setup( xvector3( x,       y,      z),     xcolor{~0u} );
                    Buffers.FillIndexAsUnconnectedLines();

                    static const xmatrix4 Parametric2D = 
                    []{
                        xmatrix4 M;
                        M.setIdentity   ();
                        M.setScale      ( xvector3(2.0f,2.0f,1.0f) );
                        M.setTranslation( xvector3( -1.0f, -1.0f, 0.1f ) );
                        return M;
                    }();

                    static const xmatrix4 Parametric2D_FlipY = 
                    []{
                        xmatrix4 M;
                        M.setIdentity   ();
                        M.setScale      ( xvector3(2.0f,-2.0f,1.0f) );
                        M.setTranslation( xvector3( -1.0f, 1.0f, 0.1f ) );
                        return M;
                    }();

                    const xmatrix4 PixelBased2D = 
                    [&]{
                        xmatrix4 M;
                        f32 XRes = (f32)View.getViewport().getWidth();
                        f32 YRes = (f32)View.getViewport().getHeight();
                        M.setIdentity   ();
                        M.setScale      ( xvector3( 2.0f/XRes,  2.0f/YRes,  1.0f) );
                        M.setTranslation( xvector3( -1.0f, -1.0f, 0.1f ) );
                        return M;
                    }();

                    const xmatrix4 PixelBased2D_FlipY = 
                    [&]{
                        xmatrix4 M;
                        f32 XRes = (f32)View.getViewport().getWidth();
                        f32 YRes = (f32)View.getViewport().getHeight();
                        M.setIdentity   ();
                        M.setScale      ( xvector3( 2.0f/XRes,  -2.0f/YRes,  1.0f) );
                        M.setTranslation( xvector3( -1.0f, 1.0f, 0.1f ) );
                        return M;
                    }();
                    
                    // If we want to take 2d stuff to world space it will be:  
                    // xmatrix4 Matrix = View.getC2W() * PixelBased2D;

                    // If we want to Render something from the world/local space into the screen
                    // xmatrix4 Final = View.getW2C() * Matrix;
                    Draw.DrawBufferLines( View.getParametric2D() );

                    if( (0) )
                    {
                        Draw.DrawShadedRect( 
                            View.getParametric2D(), 
                            xrect(0.5f,0.5f,1.0f,1.0f), 
                            xcolor::getColorCategory(0),
                            xcolor::getColorCategory(1),
                            xcolor::getColorCategory(2),
                            xcolor::getColorCategory(3) );
                    }

                    if( (0) )
                    {
                        Draw.setSampler(hSampler);
                        Draw.DrawTexturedRect( 
                            View.getParametric2D(), 
                            xrect(0.5f,0.5f,1.0f,1.0f) );
                    }
                });

                //
                // 3d test
                //
                if( 1 )
                CmdList.Draw( eng_draw::pipeline{eng_draw::BLEND_ALPHA_ORIGINAL}, [&]( eng_draw& Draw )
                {
                    // Draw bbox
                    Draw.DrawDebugMarker( View.getW2C(), Center, xcolor::getColorCategory( static_cast<int>(i) ) );
                    
                    Draw.setSampler(hSampler);
                    Draw.DrawSphere( xmatrix4(View.getW2C()).PreRotateY( xradian{90.0_deg} ).PreTranslate(Center), xcolor::getColorCategory( static_cast<int>(i) ) );

                    // Draw Lines
                    auto Buffers = Draw.popBuffers( 6, 6 );
                    Buffers.m_Vertices[0].setup( Center,                            xcolor{255,  0,  0,  255} );
                    Buffers.m_Vertices[1].setup( Center + xvector3::Right() * 2,    xcolor{255,  0,  0,  255} );
                    Buffers.m_Vertices[2].setup( Center,                            xcolor{0,    255,0,  255} );
                    Buffers.m_Vertices[3].setup( Center + xvector3::Up() * 2,       xcolor{0,    255,0,  255} );
                    Buffers.m_Vertices[4].setup( Center,                            xcolor{0,    0,  255,255} );
                    Buffers.m_Vertices[5].setup( Center + xvector3::Forward() * 2,  xcolor{0,    0,  255,255} );
                    Buffers.FillIndexAsUnconnectedLines();
                    Draw.DrawBufferLines( View.getW2C() );

                    Draw.popSolidCube( xbbox{ Center, 0.5f  }, xcolor::getColorCategory( static_cast<int>(i) ).ComputeNewAlpha( 0.5f ) );
                    Draw.DrawBufferTriangles2( View.getW2C(), hMaterialInformed, hPipeline );
                });

                pWindow->SubmitDisplayCmdList( CmdList );
            });
            m_Block.Join();
        }
        

        ImGui::Render();

        pWindow->EndRender();
        pWindow->PageFlip();
    }

    //
    // Shut down
    //
    ImGui_engBase_Shutdown();

    //
    // End the app
    //
    g_context::Kill();
    return 0;
}

