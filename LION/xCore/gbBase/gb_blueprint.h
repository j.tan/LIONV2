//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_blueprint
{
    //------------------------------------------------------------------------------
    // Description:
    //          This component will have a list of entities. These entities will move
    //          relative to whichever entity this component belongs. 
    // Restrictions:
    //          The entity which used this component must be dynamic
    //          This component should update before the entity that has this component updates. 
    //          Require since it needs T0 from previous frame.
    //          All significant T1 transformations should have taken place for the entity.
    //          
    //------------------------------------------------------------------------------
  /*
    class component_hirarchycal_transform : public base
    {
        x_object_type( component_hirarchycal_transform, is_quantum_lock_free, rtti(base), is_not_copyable, is_not_movable )
    
    protected:
        
        virtual void onExecute( void ) noexcept override
        {
            //
            // Update all the dependent entity relative to the blue print
            // aka: When the blue print transform the other entities will also transform
            //
            auto&               T0                = getEntity().getT0Data<mutable_data>();
            auto&               T1                = getEntity().getT1Data<mutable_data>();
            const xmatrix4      RelativeMatrix    = T1.m_L2W * xmatrix4(T0.m_L2W).InvertSRT();

            for( auto& Entry : m_EntityList )
            {
                auto pEntity = m_GInterface.m_GameMgr.getEntity( Entry );
                x_assert( pEntity );

                pEntity->msgTransformSet( RelativeMatrix, []( xmatrix4& Final, const xmatrix4& RelativeMatrix )
                {
                    Final = RelativeMatrix * Final;
                });
            }
        }

    protected:

        xvector<entity::guid>   m_EntityList;
    };
    */

    //------------------------------------------------------------------------------
    // Description:
    //------------------------------------------------------------------------------
    /*
    class editable_blue_print : public entity_dynamic
    {
        x_object_type( editable_blue_print, is_quantum_lock_free, rtti(entity_dynamic), is_not_copyable, is_not_movable )

    protected:
        
        virtual void onExecute( void ) noexcept override
        {
            //
            // Update all the dependent entity relative to the blue print
            // aka: When the blue print transform the other entities will also transform
            //
            auto&               T0                = getT0Data<mutable_data>();
            auto&               T1                = getT1Data<mutable_data>();
            const xmatrix4      RelativeMatrix    = T1.m_L2W * xmatrix4(T0.m_L2W).InvertSRT();

            for( auto& EntryGuid : m_EntityList )
            {
                auto pEntity = m_GInterface.m_GameMgr.getEntity<entity>( EntryGuid );
                x_assert( pEntity );

                pEntity->msgTransformSet( RelativeMatrix, []( xmatrix4& Final, const xmatrix4& RelativeMatrix )
                {
                    Final = RelativeMatrix * Final;
                });
            }
        }

    protected:

        xvector<entity::guid>   m_EntityList;
    };
    */

    //------------------------------------------------------------------------------
    // Description:
    //------------------------------------------------------------------------------
    struct bundled_entity 
    {
        void    SaveAsEntity            ( const char* pHeader, gb_component::entity::guid Guid, xtextfile& TextFile ) noexcept;
        void    SaveAsBlueprintInstance ( const char* pHeader, gb_component::entity::guid Guid, gb_blueprint::guid BPGuid, xtextfile& TextFile ) noexcept;
        void    LoadAsEntity            ( gb_component::entity::guid& Guid, xtextfile& TextFile ) noexcept;
        void    LoadAsBlueprintInstance ( gb_component::entity::guid& Guid, gb_blueprint::guid& BPGuid, xtextfile& TextFile ) noexcept;

        gb_component::type_base::guid               m_EntityTypeGuid        {};
        xndptr<gb_component::type_base::guid>       m_lComponentTypes       {};
        xproperty_data_collection                   m_lEntityProperties     {};
    };

    //------------------------------------------------------------------------------
    // Description:
    //------------------------------------------------------------------------------
    struct result
    {
        gb_component::entity::guid                  m_Guid      { nullptr };
        gb_component::entity*                       m_pEntity   { nullptr };
    };

    //------------------------------------------------------------------------------
    // Description:
    //------------------------------------------------------------------------------
    class master 
    {
        x_object_type( master, is_linear, is_not_copyable, is_not_movable )

    public:

        using   t_entity = gb_blueprint::master;
        using   guid     = gb_blueprint::guid;

    public:

                                master          ( void ) = delete;
                                master          ( guid Guid ) : m_Guid{Guid}{}
        guid                    getGuid         ( void )                                        { return m_Guid; }
        void                    setup           ( const gb_component::entity& Entity ) { x_assert( Entity.isInWorld() == false ); Entity.LinearSaveToBlueprintBundle( m_Bundle ); }
        template< typename T >
        T&                      CreateEntity  ( gb_component::entity::guid Guid, const xmatrix4& L2W, gb_game_graph::base& GameMgr )
        {
            return CreateInstance( Guid, L2W, GameMgr ).SafeCast<T>();
        }

        template< typename T >
        T&                      CreateEntity  ( gb_component::entity::guid Guid, const bundled_entity& InstanceOverrides, gb_game_graph::base& GameMgr )
        {
            return CreateInstance( Guid, InstanceOverrides, GameMgr ).SafeCast<T>();
        }

        gb_component::entity&   CreateInstance  ( gb_component::entity::guid Guid, const xmatrix4& L2W, gb_game_graph::base& GameMgr );
        gb_component::entity&   CreateInstance  ( gb_component::entity::guid Guid, const bundled_entity& InstanceOverrides, gb_game_graph::base& GameMgr );
        const bundled_entity&   getBundle       ( void )                                { return m_Bundle; }
        void                    Save            ( xtextfile& TextFile ) noexcept;
        void                    Load            ( xtextfile& TextFile, gb_game_graph::base& GameMgr ) noexcept;

    protected: 

        guid                            m_Guid;
        bundled_entity                  m_Bundle;
    };

}

