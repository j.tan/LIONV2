//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component  { class base; class type_base; class entity; }
namespace gb_game_graph { class sync_point; class base; }

namespace gb_component_mgr
{
    //------------------------------------------------------------------------------
    // Description:
    //      A component manager deals with components.
    //      They run them, create them, add them to the world, etc.
    //      In fact the collection of managers is what the game world as made of.
    //      Every manager gets updated every frame
    //      Ones a manager runs it updates all the components associated with it.
    //      Ones it has done its job it will notify its gb_trigger
    //      This class is the base class for all game component managers
    //------------------------------------------------------------------------------
    class base : public x_ll_object_harness< base, x_job<10> >
    {
        x_object_type( base, is_quantum_lock_free, rtti(x_ll_object_harness), is_not_copyable, is_not_movable )

    public:
        
        x_constexprvar bool t_is_component_mgr  = true;
        using               guid                = xguid<t_self>;
        
    public:
            
        x_forceinline                                   base                    ( x_ll_circular_pool& MsgAllocQueue )                                       noexcept;
        virtual                                        ~base                    ( void )                                                                    noexcept = default;
        x_inline        void                            msgDestroy              ( gb_component::base& Component )                                           noexcept;
        x_incppfile     void                            LinearDestroy           ( gb_component::base& Component )                                           noexcept;
        x_forceinline   x_ll_circular_pool&             getMsgAllocQueue        ( void )                                                                    noexcept { return m_MsgAllocPool; }
        constexpr       const guid                      getGuid                 ( void )                                                            const   noexcept { return m_Guid;     }
        constexpr       const char*                     getTypeName             ( void )                                                            const   noexcept { return m_Name; }
        x_inline        void                            setup                   ( guid Guid, const char* pName, s32 MaxEntities, s32 MsgPoolSize )          noexcept; 
        x_inline        void                            RegisterComponentType   ( gb_component::type_base& Type )                                           noexcept;
        x_inline        void                            UnregisterComponentType ( gb_component::type_base& Type )                                           noexcept;
        x_forceinline   void                            msgAddToWorld           ( gb_component::base& Component )                                           noexcept{ SendMsg([&]{ onAddToWorld( Component );      } ); }
        x_forceinline   void                            msgRemoveFromWorld      ( gb_component::base& Component )                                           noexcept{ SendMsg([&]{ onRemoveFromWorld( Component ); } ); }

    protected:
            
        enum : u32
        {
            MAX_SYNC_POINTS = 8
        };
            
    protected:
            
        virtual         void                            onRemoveFromWorld       ( gb_component::base& Component )                                           noexcept;
        virtual         void                            onAddToWorld            ( gb_component::base& Component )                                           noexcept;
        virtual         void                            onDestroy               ( gb_component::base& Component )                                           noexcept;
        virtual         void                            onExecute               ( void )                                                                    noexcept;
        virtual         void                            qt_onRun                ( void )                                                                    noexcept override final;
        virtual         void                            qt_onDone               ( void )                                                                    noexcept override final;
        virtual         void                            onEndOfFrameCleanUp     ( void )                                                                    noexcept;
        x_inline        void                            DirectDestroy           (  gb_component::base& Component )                                          noexcept { onDestroy( Component );    }
        x_inline        void                            LinearAddToWorld        (  gb_component::base& Component )                                          noexcept { onAddToWorld( Component ); }

    protected:
        
        gb_component::type_base*                                m_pTypeHead         { nullptr };
        x_ll_mpsc_node_queue<local_message>                     m_DestroyMsgQueue   {};
        x_ll_circular_pool                                      m_DestroyMsgPool    {};
        guid                                                    m_Guid              {};
        xarray<char, 256>                                       m_Name              { {char{0}} };

    protected:
        
        friend class gb_game_graph::sync_point;
        friend class gb_component::entity;
    };

    //------------------------------------------------------------------------------
    // Description:
    //      This class allows to construct managers easily by dealing with common tasks.
    //      Most game component managers should use this class as their base class.
    //------------------------------------------------------------------------------
    template< typename T_MGR >
    class standard_behavior_harness : public base
    {
        x_object_type( standard_behavior_harness, is_quantum_lock_free, is_not_copyable, is_not_movable )

    public: // --- class traits ---
        
        using t_parent  = base;
        using t_mgr     = T_MGR;
        
    protected:

        constexpr       standard_behavior_harness  ( s32 MaxEntities, xuptr MsgPoolSize )   noexcept : t_parent( m_MsgAllocPool, MaxEntities ),m_MsgAllocPool{ MsgPoolSize } {}
        constexpr       standard_behavior_harness  ( void )                                 noexcept : t_parent( m_MsgAllocPool )  {}
        
    protected:
        
        x_ll_circular_pool        m_MsgAllocPool {};
    };

    //------------------------------------------------------------------------------
    // Description:
    //      This class allows to construct managers easily by dealing with common tasks.
    //      Most game component managers should use this class as their base class.
    //------------------------------------------------------------------------------
    class general_mgr : public gb_component_mgr::standard_behavior_harness< general_mgr >
    {
        x_object_type( general_mgr, is_quantum_lock_free, rtti(gb_component_mgr::base), is_not_copyable, is_not_movable )

    public: 
        
                general_mgr( void ) = default;
    };

};


