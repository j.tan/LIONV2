//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component {


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT ENTITY
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Description:
//      component entity
//------------------------------------------------------------------------------
x_forceinline 
entity::entity( const base_construct_info& C ) noexcept : base{ C } 
{ 
    x_assert( isEntity() ); 
}

//------------------------------------------------------------------------------
// Description:
//      Adds components to an entity. Please make sure the entity is not in the world.
//------------------------------------------------------------------------------
x_inline 
void entity::setupAddComponent( base& Component ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert_lqnone( m_Debug_LQMsg );
    x_assert( isInWorld() == false );
    Component.m_pNextComp = m_pNextComp;
    m_pNextComp = &Component;
}

//------------------------------------------------------------------------------
// Description:
//      Add the entity to the world, the entity will add alls its components to the world too
//------------------------------------------------------------------------------
x_inline 
void entity::msgAddToWorld( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    m_GInterface.m_Manager.msgAddToWorld( *this );
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        pNext->m_GInterface.m_Manager.msgAddToWorld( *pNext );
    }
}

//------------------------------------------------------------------------------
// Description:
//      Add the entity to the world, the entity will add alls its components to the world too
//------------------------------------------------------------------------------
x_inline 
void entity::LinearAddToWorld( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    m_GInterface.m_Manager.LinearAddToWorld( *this );
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        pNext->m_GInterface.m_Manager.LinearAddToWorld( *pNext );
    }
}

//------------------------------------------------------------------------------
// Description:
//      Destroys the entity and its components
//------------------------------------------------------------------------------
x_inline 
void entity::msgDestroy( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    //
    // Officially notify the manager to destroy the components
    //
    m_GInterface.m_Manager.msgDestroy( *this );
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        pNext->m_GInterface.m_Manager.msgDestroy( *pNext );
    }
}

//------------------------------------------------------------------------------
// Description:
//      Destroys the entity and its components, this function should be call from a thread safe place
//------------------------------------------------------------------------------
x_inline            
void entity::LinearDestroy( void ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        pNext->m_GInterface.m_Manager.LinearDestroy( *pNext );
    }
    m_GInterface.m_Manager.LinearDestroy( *this );
}


//------------------------------------------------------------------------------
// Description:
//      Removes the entity from the world
//------------------------------------------------------------------------------
x_inline 
void entity::msgRemoveFromWorld( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    m_GInterface.m_Manager.msgRemoveFromWorld( *this );
    for( base* pNext = m_pNextComp; pNext; pNext = pNext->getNextComponent() )
    {
        m_GInterface.m_Manager.msgRemoveFromWorld( *pNext );
    }
}

//------------------------------------------------------------------------------
// Description:
//      Gets an entity of a certain type
//------------------------------------------------------------------------------
template< typename T >  x_inline
T* entity::getComponent( gb_component::base* pFrom ) const noexcept
{
    static_assert( T::t_is_component, "T in the template should be of type component" );
    const char* pType = T::t_type_string;

    // If the entity is of the same type as the component the user is looking for then there is a problem
    // because the user already has have it and should know the type of the entity.
    x_assert( pType != getGlobalInterface().m_Type.getTypeName() );
    if( pFrom )     pFrom = pFrom->m_pNextComp;
    else            pFrom = m_pNextComp;

    for( gb_component::base* p = pFrom; p ; p = p->m_pNextComp )
    {
        if( pType == p->getType().getTypeName() )
            return &p->SafeCast<T>();
    }

    return nullptr;
}

///////////////////////////////////////////////////////////////////////////////
// END NAME SPACE
///////////////////////////////////////////////////////////////////////////////
}
