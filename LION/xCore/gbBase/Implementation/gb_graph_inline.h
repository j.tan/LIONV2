//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_game_graph {

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// SYNC POINT
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
x_inline
sync_point& sync_point::setup( guid Guid, const char* pUniqueName ) noexcept
{
    for( int i=0; (m_Name[i] = pUniqueName[i]) ; ++i );
    m_Guid = Guid;
    return *this;
}

//------------------------------------------------------------------------------
x_inline
void sync_point::setupGraphMgrWillNotifyMe ( gb_component_mgr::base& Mgr ) noexcept
{                                    
    m_NotificationCounter++;
    m_nMgrWillNotifyMe++;
    Mgr.onAppenTrigger( *this );
}
        
//------------------------------------------------------------------------------
x_inline
void sync_point::setupGraphManagersToRunsAfterTriggered( gb_component_mgr::base& Manager ) noexcept
{
    m_ManagersToBeRetrigger[m_nManagersToBeRetrigger] = &Manager;
    m_nManagersToBeRetrigger++;
}
        
//------------------------------------------------------------------------------
// Virtual
x_inline
sync_point::~sync_point( void ) noexcept
{
    // No need to check the trigger_base based assert
    m_NotificationCounter = 0;
}

//------------------------------------------------------------------------------
// Virtual
x_inline
void sync_point::qt_onTriggered( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
            
    // We can now officially switch buffers
    // we need to do this before releasing all the jobs
    m_iCurrent = 1 - m_iCurrent;
            
    //
    // Reset counter
    //
    m_NotificationCounter = m_nMgrWillNotifyMe;
            
    //
    // Release all the jobs to the quantum world
    //
    for( int i = 0; i < m_nManagersToBeRetrigger; ++i )
    {
        g_context::get().m_Scheduler.AddJobToQuantumWorld( *m_ManagersToBeRetrigger[i] );
    }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// GAME GRAPH :: base
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------------
x_inline
void base::InitializeGraphConnection( sync_point& From, gb_component_mgr::base& Mgr, sync_point& End ) noexcept
{
    From.setupGraphManagersToRunsAfterTriggered( Mgr );
    End.setupGraphMgrWillNotifyMe( Mgr );
}

        
//------------------------------------------------------------------------------
// virtual
inline
void base::qt_onRun( void ) noexcept 
{
    x_assert_quantum( m_Debug_LQ );
            
    flags LocalFlags = MsgForceLock();
            
    {
        x_assert_linear( m_Debug_LQMsg );
                
        t_ProcessMessages( LocalFlags );
                
        m_nFrames++;
        onStartFrame();
    }
            
    MsgUnlock();
}
        
//------------------------------------------------------------------------------
// virtual
inline
void base::qt_onDone ( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    for( int i = 0; i < m_nTriggers; i++ )
    {
        // must notify the trigger that we are done
        NotifyTrigger( *m_TriggerList[i] );
    }
}
        
//------------------------------------------------------------------------------
template< typename T_COMP > x_inline
typename T_COMP::t_type& base::RegisterComponentAndDependencies( gb_component_mgr::base& ComponentMgr, gb_game_graph::sync_point& SyncPoint ) noexcept
{
    static_assert(T_COMP::t_is_component,                           "This is not a component");
    static_assert(T_COMP::t_type::t_is_component_type,              "This is not a component type");
            
    x_assert_linear( m_Debug_LQ );
    gb_component::type_base& Type = m_CompTypeDB.FindOrRegister( T_COMP::t_type::getCompileTimeStaticGuid(), type_dbase::LIFETIME_DELETE_WHEN_DONE, [&]() -> gb_component::type_base&
    {
        auto& Type = *x_new( typename T_COMP::t_type,
        {
            ComponentMgr,
            *this,
            SyncPoint  
        });
        
        // Register the actual type
        ComponentMgr.RegisterComponentType( Type );
        return Type;
    } );
            
    // make sure that the types in fact are the same
    x_assume( T_COMP::t_type::getCompileTimeStaticGuid().m_Value == Type.getGuid().m_Value );
    return reinterpret_cast<typename T_COMP::t_type&>(Type);
}
        
//------------------------------------------------------------------------------
x_inline
void base::qt_onEntityDestroy( const gb_component::entity& Entity ) noexcept
{
    m_EntityHash.DeleteEntry( Entity.getGuid().m_Value );
}

//------------------------------------------------------------------------------
// virtual
inline
void base::onEndFrame( void ) noexcept 
{ 
    m_Stats_LogicTime = m_Stats_Timer.TripNanoseconds(); 
}

//------------------------------------------------------------------------------
x_inline
void base::Initialize( void ) noexcept        
{ 
    x_assert_linear( m_Debug_LQ ); 
    onInitialize();
}

//------------------------------------------------------------------------------
x_inline
u64 base::getGameFrame ( void ) const noexcept 
{ 
    x_assert_quantum( m_Debug_LQ ); 
    return m_nFrames; 
}
        
//------------------------------------------------------------------------------
x_inline
base::base( xuptr MaxEntities ) noexcept : t_parent( m_MsgPool )
{
    m_CompTypeDB.Init( 1024 );
    m_SyncDB.Init( 128 );
    m_CompManagerDB.Init( 128 );
    m_ConstDataDB.Init( 128 );
    m_BlueprintDB.Init( 1024 );

    m_StartSyncPoint.sync_point::setup( sync_point_start::guid( x_constStrCRC32("StartSyncPoint") ), "StartSyncPoint" );
    m_EndSyncPoint.sync_point::setup  ( sync_point_start::guid( x_constStrCRC32("EndSyncPoint") ),   "EndSyncPoint"   );

    m_SyncDB.RegisterButRetainOwnership( m_StartSyncPoint );
    m_SyncDB.RegisterButRetainOwnership( m_EndSyncPoint );
            
    m_StartSyncPoint.setup( this );
    m_EndSyncPoint.setup( this );

    m_EntityHash.Initialize( MaxEntities * 4 );
}

//------------------------------------------------------------------------------
x_inline        
gb_component::base& base::CreateComponent( gb_component::type_base& ComponentType, gb_component::entity& Entity, gb_component::const_data* pData ) noexcept
{
    auto& Comp = ComponentType.CreateComponent( Entity, pData );
    
    // Please use the Create entity function rather then Create Component
    x_assert( Comp.isKindOf<gb_component::entity>() == false );

    return Comp;
}

        
//------------------------------------------------------------------------------

template< typename T_COMPONENT_CLASS > x_inline
T_COMPONENT_CLASS& base::CreateComponent( gb_component::entity& Entity, gb_component::const_data* pData )  noexcept
{
    static_assert( T_COMPONENT_CLASS::t_is_component, "This is not a component" );
    static_assert( T_COMPONENT_CLASS::t_is_entity == false, "Please use CreateEntity to create entities" );
    return CreateComponent( 
        m_CompTypeDB.Find( T_COMPONENT_CLASS::t_type::getCompileTimeStaticGuid() ),
        Entity,
        pData ).template SafeCast<T_COMPONENT_CLASS>();
}

//------------------------------------------------------------------------------
x_inline
gb_component::entity& base::CreateEntity( 
    gb_component::type_base&        EntityType, 
    gb_component::entity::guid    InstanceGuid, 
    gb_component::const_data*       pData ) noexcept
{
    gb_component::entity&    Entity      = EntityType.CreateEntity( pData ).SafeCast<gb_component::entity>();

    // Please use the Create component function rather then Create Component
    x_assert( Entity.isKindOf<gb_component::entity>() );

    //
    // Set the guid
    //
    Entity.m_Guid = InstanceGuid;

    //
    // Add entity to the system
    //
    m_EntityHash.cpAddEntry( InstanceGuid.m_Value, [&Entity]( entity_hash::t_entry& pEntry )
    {
        pEntry = &Entity;
    });
            
    return Entity;
}

//------------------------------------------------------------------------------

template< typename T_ENTITY_CLASS > x_inline
T_ENTITY_CLASS& base::CreateEntity( gb_component::entity::guid InstanceGuid, gb_component::const_data* pData ) noexcept
{
    static_assert( T_ENTITY_CLASS::t_is_entity, "This is not an entity" );
    return CreateEntity(
        m_CompTypeDB.Find( T_ENTITY_CLASS::t_type::getCompileTimeStaticGuid() ),
        InstanceGuid,
        pData
    ).template SafeCast<T_ENTITY_CLASS>();
}

//------------------------------------------------------------------------------
template< typename T_ENTITY_CLASS > x_inline
T_ENTITY_CLASS* base::getEntity( gb_component::entity::guid Guid ) const noexcept
{
    T_ENTITY_CLASS* pEntity = nullptr;
    m_EntityHash.cpGetEntry( Guid.m_Value, [&pEntity]( const entity_hash::t_entry& pTheEntity )
    {
        pEntity = reinterpret_cast<T_ENTITY_CLASS*>(pTheEntity);
    } );
    return pEntity;
}


///////////////////////////////////////////////////////////////////////////////
// END NAME SPACE
///////////////////////////////////////////////////////////////////////////////
}
