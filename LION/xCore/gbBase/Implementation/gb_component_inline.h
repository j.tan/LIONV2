//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component {

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT BASE
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Description:
//      Goes through all the objects messages and tries to process them
//------------------------------------------------------------------------------
inline
bool base::t_ProcessMessages( flags LocalFlags ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );
    
    //
    // Determine if we need to update the new write memory
    // While WAIT FOR UPDATE is on we are not allow to consume messages
    //
    x_assert_linear( m_Debug_LQMsg );
    if( LocalFlags.m_MSG_WAIT_FOR_UPDATE )
    {
        do
        {
            static_assert( flags::MASK_T0 == 1, "" );
            const unsigned int Diff = (m_GInterface.m_SyncPoint.getICurrent() ^ LocalFlags.m_Flags) & flags::MASK_T0;
            if( Diff )
            {
                const u32       RemoveFlags = ~( (LocalFlags.m_Flags & flags::MASK_T0) | flags::MASK_MSG_WAIT_FOR_UPDATE );
                const u32       AddFlags    = m_GInterface.m_SyncPoint.getICurrent() & flags::MASK_T0;
                const flags     NewFlags    {  AddFlags | (LocalFlags.m_Flags & RemoveFlags) };

                if( m_qtFlags.compare_exchange_weak( LocalFlags, NewFlags ) )
                {
                    const u32 iT0 = AddFlags & flags::MASK_T0;
                    x_assume( static_cast<xuptr>(!iT0) <= 1 );
                    m_pMutableData[ !iT0 ]->onUpdateMutableData( *m_pMutableData[ iT0 ] );
                    LocalFlags = NewFlags;
                    break;
                }
            }
            // The sync point has not updated yet so we are not allow to consume messages
            else return false;
            
        } while( 1 );
    }

    //
    // Process messages
    //
    x_assert( LocalFlags.m_MSG_WAIT_FOR_UPDATE == false );
    t_parent::t_ProcessMessages();

    return true;
}

//------------------------------------------------------------------------------
// Description:
//      This function makes sure that all mutations are linearized
//      The Manager will run this function every frame.
//      Any pending messages will be handle
//      The component hierarchy will have a change to update itself thought onExecute
//      We will also lock our component from processing more messages until the sync point
//      is trigger.
//------------------------------------------------------------------------------
inline
void base::qt_onRun( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );

    //
    // We must be able to lock our component
    //
    flags LocalFlags = MsgForceLock();

    //
    // Now process any pending messages
    //
    {
        x_assert_linear( m_Debug_LQMsg );
        
        // Make sure all messages are process
        t_ProcessMessages(LocalFlags); 
        
        // These need to be the same
        x_assert( (( m_qtFlags.load().m_Flags ^ m_GInterface.m_SyncPoint.getICurrent() ) & flags::MASK_T0) == 0 );

        // Allow the user to compute its component if need be
        onExecute();
    }

    // Update our flags ( remove lock and add waiting for the update )
    // at this point we are assuming our write buffer is final
    MsgUnlock( flags::MASK_MSG_WAIT_FOR_UPDATE );
}


//------------------------------------------------------------------------------
// Description:
//      This function lets the component knows that it just been added to the world
//------------------------------------------------------------------------------
inline
void base::onNotifyInWorld( void ) noexcept
{
}

//------------------------------------------------------------------------------
// Description:
//      The official function to get the constant data for TIME0
//------------------------------------------------------------------------------
template< typename T_CLASS > inline
const T_CLASS& base::getT0Data( void ) const noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assert( m_pMutableData[m_GInterface.m_SyncPoint.getICurrent()&component_flags::T0] );
    return reinterpret_cast<const T_CLASS&>(*m_pMutableData[m_qtFlags.load().m_T0]);
}

//------------------------------------------------------------------------------
// Description:
//      The official function to get the mutable data for TIME1
//------------------------------------------------------------------------------
template< typename T_CLASS > x_forceinline   
T_CLASS& base::getT1Data( void ) const noexcept 
{ 
    x_assert_quantum( m_Debug_LQ ); 
    x_assume( m_pMutableData[static_cast<xuptr>(!m_qtFlags.load().m_T0)] ); 
    return reinterpret_cast<T_CLASS&>(*m_pMutableData[static_cast<xuptr>(!m_qtFlags.load().m_T0)]); 
}

//------------------------------------------------------------------------------
// Description:
//      Get the constant data of the component
//------------------------------------------------------------------------------
template< typename T_CLASS > x_forceinline
const T_CLASS& base::getConstData ( void ) const noexcept 
{ 
    x_assert_quantum( m_Debug_LQ ); 
    x_assume( m_ConstData.isValid() ); 
    return m_ConstData->SafeCast<const T_CLASS>(); 
}

//------------------------------------------------------------------------------
// Description:
//          Enumerates the base properties
//------------------------------------------------------------------------------
// virtual
inline
void base::onPropEnum( xproperty_enum&  Enum  )  const noexcept  
{ 
    x_assert_quantum( m_Debug_LQ ); 
    x_assert_linear( m_Debug_LQMsg );

    Enum.beginScope( X_STR("Base"), [&]( xproperty_enum::scope& Scope )
    {
        // This properties are useful for the editor they wont be saved
        Scope.AddProperty<xprop_guid>( X_STR("ConstantData") );
    });
}

//------------------------------------------------------------------------------
// Description:
//          Checks to see if it is an entity
//------------------------------------------------------------------------------
x_orforceconst  
bool base::isEntity ( void ) const noexcept 
{ 
    return static_cast<base*>(&m_Entity) == this;        
}

//------------------------------------------------------------------------------
// Description:
//          sets/gets base properties
//------------------------------------------------------------------------------
// virtual
inline
bool base::onPropQuery ( xproperty_query& Query ) noexcept    
{ 
    x_assert_quantum( m_Debug_LQ ); 
    x_assert_linear( m_Debug_LQMsg ); 
        
    X_PROP_MAINSCOPE( Query, "Base", 
        // --- Scopes ---
        {}
        , // --- Properties --- (notice the comma)
        X_PROP_IS_PROP( "ConstantData", 
            if( Query.isReciving() )
            {
                const const_data::guid Guid{ Query.getProp<xprop_guid>().getGUID().m_Value };
                
                if( Guid.m_Value )
                {
                    m_ConstData = getGlobalInterface().m_GameMgr.m_ConstDataDB.Find( Guid );
                }
                else
                {
                    m_ConstData.setNull();
                }
            }
            else
            {
                x_constexprvar char* const pType = "ConstantData"; 
                if( hasConstData() )
                {
                    const const_data& ConstData = getConstData<const_data>();
                    xprop_guid::Send( Query, ConstData.getGuid().m_Value, pType );
                }
                else
                {
                    xprop_guid::Send( Query, 0, pType );
                }
            }
        )
    )
}



///////////////////////////////////////////////////////////////////////////////
// END NAME SPACE
///////////////////////////////////////////////////////////////////////////////
}
