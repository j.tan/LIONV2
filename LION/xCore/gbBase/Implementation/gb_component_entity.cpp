//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "gb_base.h"

namespace gb_component {


//------------------------------------------------------------------------------
// Description:
//      Save the entity with its components
//------------------------------------------------------------------------------        
void entity::LinearSaveToBlueprintBundle( gb_blueprint::bundled_entity& Bundle ) const noexcept
{
    //
    // Set the entity guid type
    //
    Bundle.m_EntityTypeGuid = getType().getGuid();

    //
    // Copy all the component types
    //
    {
        int nComponents = [&]{ int c=0; for( auto* pComp = m_pNextComp ; pComp ; pComp = pComp->m_pNextComp, c++ )(void)nullptr; return c; }();
        Bundle.m_lComponentTypes.New( nComponents );
        nComponents = 0;
        for( auto* pComp = m_pNextComp; pComp; pComp = pComp->m_pNextComp )
        {
            Bundle.m_lComponentTypes[nComponents++] = pComp->getType().getGuid();
        }
    }

    //
    // Collect all properties
    //
    LinearPropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
    {
        // Just collect them all inside an static array
        Bundle.m_lEntityProperties.m_List.append() = Data;
    }, true, xproperty_enum::mode::SAVE );

    //
    // Collect all the data for the properties
    //
    LinearPropGet( [&]( xproperty_query& Query, xproperty_data& Data )
    {
        //
        // First read the results if any
        //
        if( Query.hasResults() )
        {
            Bundle.m_lEntityProperties.m_List[ Query.getResultsIndex() ] = Query.getResults();
        }

        //
        // are we done?
        //
        if( Query.getPropertyIndex() == Bundle.m_lEntityProperties.m_List.getCount() ) 
        {
            return false;
        }

        //
        // Tell which property to get next
        //
        Data = Bundle.m_lEntityProperties.m_List[ Query.getPropertyIndex() ];

        return true;
    });
}

//------------------------------------------------------------------------------
// Description:
//      Save the entity with its components
//------------------------------------------------------------------------------        
entity& entity::LinearLoadFromBlueprintBundle( guid EntityGuid, gb_blueprint::bundled_entity& Bundle, gb_game_graph::base& GameMgr ) noexcept
{
    //
    // Create the entity 
    //
    entity& Entity  = GameMgr.CreateEntity( 
        GameMgr.m_CompTypeDB.Find( Bundle.m_EntityTypeGuid ), 
        EntityGuid, 
        nullptr );

    //
    // Create all the components
    //
    for( int i = Bundle.m_lComponentTypes.getCount<int>()-1; i>=0; --i )
    {
        GameMgr.CreateComponent( 
            GameMgr.m_CompTypeDB.Find( Bundle.m_lComponentTypes[i] ),
            Entity, 
            nullptr );
    }

    //
    // Load/Set and set all the properties
    //
    Entity.LinearPropSet( [&]( xproperty_query& Query, xproperty_data& Data )
    {
        //
        // are we done?
        //
        if( Query.getPropertyIndex() >= Bundle.m_lEntityProperties.m_List.getCount() ) 
        {
            return false;
        }

        //
        // Tell which property to get next
        //
        Data = Bundle.m_lEntityProperties.m_List[ Query.getPropertyIndex() ];

        return true;
    });

    return Entity;
}

//------------------------------------------------------------------------------
// Description:
//
//------------------------------------------------------------------------------        
void entity::LinearPropEnum ( xfunction<void( xproperty_enum&  Enum, xproperty_data& Data )> Function, bool bFullPath, xproperty_enum::mode Mode ) const noexcept
{
    //
    // Make sure T1 is up to date
    //
    const_cast<entity*>(this)->FlushMessages();
    x_assert_linear( m_Debug_LQ ); 
    x_assert_linear( m_Debug_LQMsg );

    //
    // Collect all the properties
    //
    PropEnum( Function, bFullPath, Mode );
    for( auto* pNext = m_pNextComp; pNext ; pNext = pNext->m_pNextComp )
    {
        pNext->PropEnum( Function, bFullPath, Mode );
    }
}

//------------------------------------------------------------------------------
// Description:
//          Getting properties from the entity as its components
//------------------------------------------------------------------------------
bool entity::LinearPropGet ( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function ) const noexcept
{
    //
    // Make sure T1 is up to date
    //
    const_cast<entity*>(this)->FlushMessages();
    x_assert_linear( m_Debug_LQ ); 
    x_assert_linear( m_Debug_LQMsg );

    //
    // Collect all the properties
    //
    xproperty_query Query;
    if( Query.RunGet( *this, Function ) ) return true;
    for( auto* pNext = m_pNextComp; pNext ; pNext = pNext->m_pNextComp )
    {
        if( Query.NextPropertyOrChangeScope() == false ) return false;
        if( pNext->onPropQuery( Query ) ) return true;
    }

    return false;
}

//------------------------------------------------------------------------------
// Description:
//          Setting properties from the entity as its components
//------------------------------------------------------------------------------
bool entity::LinearPropSet( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function ) noexcept
{
    //
    // Make sure T1 is up to date
    //
    FlushMessages();
    x_assert_linear( m_Debug_LQ ); 
    x_assert_linear( m_Debug_LQMsg );

    //
    // Collect all the properties
    //
    xproperty_query Query;
    if( Query.RunSet( *this, Function ) ) return true;
    for( auto* pNext = m_pNextComp; pNext ; pNext = pNext->m_pNextComp )
    {
        if( Query.NextPropertyOrChangeScope() == false ) return false;
        if( pNext->onPropQuery( Query ) ) return true;
    }

    return false;
}

//------------------------------------------------------------------------------
// Description:
//          Sending a message to change the transform
//------------------------------------------------------------------------------
void entity::msgTransformSet( const xmatrix4& L2W, void(*Callback)(xmatrix4& DestL2W, const xmatrix4& SrcL2W) ) noexcept 
{ 
    SendMsgComplex( [this,&L2W,Callback]( bool bDirect )
    { 
        if( bDirect ) 
        {
            onTransformSet( L2W, Callback );
        }
        else
        {
            struct mymessage : local_message
            {
                xmatrix4              m_Matrix;
            };

            auto& MyMessage = m_MsgAllocPool.x_ll_circular_pool::New<mymessage>(); 
            MyMessage.m_Matrix   = L2W;
            MyMessage.m_Callback = [this, &Matrix = MyMessage.m_Matrix, Callback ]
            { 
                onTransformSet( Matrix, Callback ); 
            };
            Push( MyMessage ); 
        }
    });
}


//------------------------------------------------------------------------------
// Description:
//------------------------------------------------------------------------------
bool entity::onPropQuery( xproperty_query& Query ) noexcept    
{ 
    x_assert_quantum( m_Debug_LQ ); 
    x_assert_linear( m_Debug_LQMsg ); 
        
    X_PROP_MAINSCOPE( Query, "Entity", 
        // --- Scopes ---
        X_PROP_SCOPE_CHILD( t_parent::onPropQuery( Query ) )
        , // --- Properties --- (notice the comma)
        X_PROP_IS_PROP( "GUID",         xprop_guid::SendOrReceive( Query, m_Guid.m_Value, "Entity" ) )
        X_PROP_IS_PROP( "isInWorld", 
            if( Query.isReciving() )
            {
                if( xprop_bool::Recive( Query ) )
                {
                    appendFlags( component_flags::IN_THE_WORLD );
                }
                else
                {
                    removeFlags( component_flags::IN_THE_WORLD );
                }
            }
            else
            {
                xprop_bool::Send( Query, isInWorld() );
            }
        )
    )
        
    return false; 
}

//------------------------------------------------------------------------------
// Description:
//------------------------------------------------------------------------------
void entity::onPropEnum ( xproperty_enum&  Enum  )  const noexcept  
{ 
    x_assert_quantum( m_Debug_LQ ); 
    x_assert_linear( m_Debug_LQMsg );

    Enum.beginScope( X_STR("Entity"), [&]( xproperty_enum::scope& Scope )
    {
        // Base properties
        t_parent::onPropEnum( Enum );

        // This properties are useful for the editor they wont be saved
        Scope.AddProperty<xprop_guid>( X_STR("GUID"),       xproperty_enum::flags::MASK_DONT_SAVE | xproperty_enum::flags::MASK_READ_ONLY );
        Scope.AddProperty<xprop_bool>( X_STR("isInWorld"), xproperty_enum::flags::MASK_READ_ONLY | xproperty_enum::flags::MASK_FORCE_SAVE );
    });
}

 
//------------------------------------------------------------------------------
// END
//------------------------------------------------------------------------------        
}
