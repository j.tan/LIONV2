//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "gb_base.h"

#if _X_TARGET_WINDOWS
    #include <windows.h>
    #include<tchar.h>
#elif _X_TARGET_MAC
    #include <iostream>
    #include <dlfcn.h>
#endif


//-----------------------------------------------------------------------------
#if _X_TARGET_WINDOWS

    #if 0
    //    std::vector<std::string> slListOfDllFunctions;
    //    ListDLLFunctions( pPath, slListOfDllFunctions );
    #include <string>
    #include <vector>
    #include "imagehlp.h"
    #pragma comment(lib,"imagehlp.lib")
    void ListDLLFunctions( std::string sADllName, std::vector<std::string>& slListOfDllFunctions )
    {
        DWORD*                      dNameRVAs(0);
        _IMAGE_EXPORT_DIRECTORY*    ImageExportDirectory;
        unsigned long               cDirSize;
        _LOADED_IMAGE               LoadedImage;
        std::string                 sName;

        slListOfDllFunctions.clear();
        if (MapAndLoad(sADllName.c_str(), NULL, &LoadedImage, TRUE, TRUE))
        {
            ImageExportDirectory = (_IMAGE_EXPORT_DIRECTORY*)ImageDirectoryEntryToData(LoadedImage.MappedAddress,false, IMAGE_DIRECTORY_ENTRY_EXPORT, &cDirSize);
            if (ImageExportDirectory != NULL)
            {
                dNameRVAs = (DWORD *)ImageRvaToVa(LoadedImage.FileHeader, LoadedImage.MappedAddress, ImageExportDirectory->AddressOfNames, NULL);
                for(size_t i = 0; i < ImageExportDirectory->NumberOfNames; i++)
                {
                    sName = (char *)ImageRvaToVa(LoadedImage.FileHeader, LoadedImage.MappedAddress, dNameRVAs[i], NULL);
                    slListOfDllFunctions.push_back(sName);
                }
            }
            UnMapAndLoad(&LoadedImage);
        }
    }
    #endif

    //-----------------------------------------------------------------------------

    static const char* LoadScriptLibrary( const char* pFileName, xuptr& Lib, xuptr& Function )
    {
        wchar_t szBuff[1024];
        swprintf( szBuff, 1024, L"%hs", pFileName );

        Lib = reinterpret_cast<xuptr>( LoadLibrary( szBuff ) );
        if( !Lib ) return "Unable to load script";

        Function = reinterpret_cast<xuptr>( GetProcAddress( reinterpret_cast<HMODULE>(Lib), "?ScriptInterface@@YA_NPEAVbase@gb_game_graph@@PEAUg_context@@@Z" ));
        if( !Function ) return "Unable to find the ScriptInterface";

        return nullptr;
    }

#elif _X_TARGET_MAC
    //-----------------------------------------------------------------------------

    static const char* LoadScriptLibrary( const char* pFileName, xuptr& Lib, xuptr& Function )
    {
        Lib = reinterpret_cast<xuptr>( dlopen( pFileName, RTLD_LOCAL ) );
        if( !Lib )
            return "Unable to load script";
        
        Function = reinterpret_cast<xuptr>( dlsym( reinterpret_cast<void*>(Lib), "ScriptInterface" ));
        if( !Function ) return "Unable to find the ScriptInterface";
        
        return nullptr;
    }

#endif

////////////////////////////////////////////////////////////////////////////////
// NAME SPACE STARTS
////////////////////////////////////////////////////////////////////////////////
namespace gb_game_graph {

////////////////////////////////////////////////////////////////////////////////
// BASE
////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------------------
void LinkWithGameBase( void ) {}

//-----------------------------------------------------------------------------

void base::ImportScript( const char* pPath ) noexcept
{
    script Script;

//    std::vector<std::string> slListOfDllFunctions;
//    ListDLLFunctions( pPath, slListOfDllFunctions );

    xuptr Lib;
    xuptr Function;
    const char* pErr = LoadScriptLibrary( pPath, Lib, Function );
    if( !pErr )
    {
        Script.m_pFileName  = pPath;
        Script.m_DLL        = Lib;
        Script.m_pInterface = reinterpret_cast<script_interface>(Function);
    }
    else
    {
        x_assert(false);
    }

    Script.m_pInterface( this, &g_context::get() );
//    FreeLibrary( reinterpret_cast<HMODULE>(Script.m_DLL) );
    //dlclose( reinterpret_cast<void*>(Script.m_DLL) )
}

//----------------------------------------------------------------------------------------

void base::LinearSaveGame ( xwstring FileName ) noexcept
{
    xtextfile TextFile;
    TextFile.openForWriting( FileName );
    
    //
    // Make sure that the types are registered 
    //
    xproperty_data::RegisterTypes( TextFile );

    //
    // Save all the entities in the game
    //
    xarray<gb_blueprint::bundled_entity,2> Bundle;
    int Index = 0;
    x_job_block JobBlock;
    for( const auto& Entry : m_EntityHash )
    {
        gb_blueprint::bundled_entity& ActiveBundle = Bundle[Index];

        //
        // Make sure that everything is clear
        //
        ActiveBundle.m_lEntityProperties.m_List.DeleteAllEntries();

        //
        // Collect the entity bundle
        //
        if( Entry->getBlueprintGuid().m_Value )
        {
            gb_blueprint::bundled_entity EntityBundle;
            
            // Save the entity into a temporary bundle 
            Entry->LinearSaveToBlueprintBundle( EntityBundle );

            // Get the actual blue print
            auto& Blueprint = m_BlueprintDB.Find( Entry->getBlueprintGuid() );

            // Filter the temporary bundle with the blue print
            ActiveBundle.m_lEntityProperties.appendSubtractFilterByNameAndValue( 
                EntityBundle.m_lEntityProperties, 
                xbuffer_view<const xproperty_data>{ Blueprint.getBundle().m_lEntityProperties } );
        }
        else
        {
            Entry->LinearSaveToBlueprintBundle( ActiveBundle );
        }

        //
        // Save the actual bundle
        //
        JobBlock.Join();
        JobBlock.SubmitJob( 
        [
            &Bundle, 
            &TextFile, 
            &Entity = *Entry, 
            &ActiveBundle 
        ]()
        {
            if( Entity.getBlueprintGuid().m_Value )     ActiveBundle.SaveAsBlueprintInstance( "BPI", Entity.getGuid(), Entity.getBlueprintGuid(), TextFile );
            else                                        ActiveBundle.SaveAsEntity( "Entity", Entity.getGuid(), TextFile );
        });

        //
        // Switch the active bundle
        //
        Index = 1 - Index; 
    }
    JobBlock.Join();
}

//----------------------------------------------------------------------------------------

void base::LinearLoadGame ( xwstring FileName ) noexcept
{
    struct info
    {
        gb_blueprint::bundled_entity    m_Bundle;
        gb_component::entity::guid      m_Guid;
        gb_blueprint::guid              m_BPGuid;
    };

    xtextfile TextFile;
    TextFile.openForReading( FileName );


    xarray<info,2>  Info;
    int             Index = 0;
    x_job_block     JobBlock;

    if( TextFile.ReadRecord() )
    do 
    {
        info&           ActiveInfo      = Info[Index];
        const u32       CRC             = x_strCRC<xchar>( TextFile.getRecordName() );

        //
        // Load
        //
        switch( CRC )
        {
            case x_constStrCRC32( "Entity" ):       
                ActiveInfo.m_Bundle.LoadAsEntity( ActiveInfo.m_Guid, TextFile ); 
                ActiveInfo.m_BPGuid.m_Value = 0;   
                break;
            case x_constStrCRC32( "BPI" ):     
                ActiveInfo.m_Bundle.LoadAsBlueprintInstance( ActiveInfo.m_Guid, ActiveInfo.m_BPGuid, TextFile );    
                break;
            //case x_constStrCRC32( "Blueprint" ):    Bundle.LoadAsBlueprint( TextFile );             break;
        }

        //
        // Resolve
        //
        JobBlock.Join();
        JobBlock.SubmitJob(
        [
            this,
            &TextFile,
            &ActiveInfo
        ]
        ()
        {
            //
            // Create the entity
            //
            gb_component::entity& Entity = [&]() -> gb_component::entity&
            {
                if( ActiveInfo.m_BPGuid.m_Value ) 
                {
                    auto& Blueprint = m_BlueprintDB.Find( ActiveInfo.m_BPGuid );
                    return Blueprint.CreateInstance( ActiveInfo.m_Guid, ActiveInfo.m_Bundle, *this );
                }
                else
                {
                    return gb_component::entity::LinearLoadFromBlueprintBundle( ActiveInfo.m_Guid, ActiveInfo.m_Bundle, *this );
                }
            }();

            //
            // Add entity in the world if it was there before
            //
            if( Entity.isInWorld() ) Entity.LinearAddToWorld();

            //
            // Make sure we clear all for next round
            //
            ActiveInfo.m_Bundle.m_lEntityProperties.m_List.DeleteAllEntries();
        });

        //
        // Get ready for the next one
        //
        Index = 1 - Index;

    } while( TextFile.isEOF() == false );
    JobBlock.Join();

}


//----------------------------------------------------------------------------------------

void base::LinearDeleteAllEntities( void ) noexcept
{
    auto        Next = m_EntityHash.begin();
    const auto  End  = m_EntityHash.end();
    while( Next != End )
    {
        auto& Entity = *(Next++);
        Entity->LinearDestroy();
    }

    x_assert( m_EntityHash.begin().m_pHashEntry == nullptr );
}

//----------------------------------------------------------------------------------------

gb_blueprint::master& base::CreateBlueprint( gb_blueprint::guid Guid ) noexcept
{
    if( Guid.m_Value == 0 )
    {
        Guid.Reset();
        Guid.m_Value ^= (Guid.m_Value << 16); 
        Guid.m_Value |= 0xffff;  
    }
    else
    {
        x_assert( (Guid.m_Value & 0xffff) == 0xffff );
    }

    //
    // Allocated the blueprint
    //
    gb_blueprint::master& Blueprint = m_BlueprintPool.pop( Guid );

    //
    // Register the blue print
    //
    m_BlueprintDB.RegisterButRetainOwnership( Blueprint );

    return Blueprint;
}


////////////////////////////////////////////////////////////////////////////////
// NAME SPACE END
////////////////////////////////////////////////////////////////////////////////
}

