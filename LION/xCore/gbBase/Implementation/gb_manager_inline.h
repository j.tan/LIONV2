//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component_mgr {

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT MANAGER BASE
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

x_forceinline                      
base::base    ( x_ll_circular_pool& MsgAllocQueue ) noexcept : 
    x_ll_object_harness( MsgAllocQueue )  {}

//------------------------------------------------------------------------------
x_inline        
void base::msgDestroy( gb_component::base& Component ) noexcept
{ 
    if( Component.isInWorld() == false )
    {
        x_assert_linear( Component.m_Debug_LQ );
        DirectDestroy( Component );
        return;
    }

    x_assert_quantum( Component.m_Debug_LQ ); 

    // We should remove this guy first
    msgRemoveFromWorld(Component); 

    // then we will destroy it at the end of the frame
    local_message& MsgBase = m_DestroyMsgPool.New<local_message>();
    MsgBase.m_Callback = [&]{ onDestroy( Component ); };
    m_DestroyMsgQueue.push( MsgBase );
}
            
//------------------------------------------------------------------------------
x_inline        
void base::setup( guid Guid, const char* pName, s32 MaxEntities, s32 MsgPoolSize ) noexcept
{
    m_Guid = Guid;
    for( int i=0; (m_Name[i] = pName[i]); ++i ); 

    m_DestroyMsgPool.Init( MaxEntities * sizeof(local_message) );
    m_MsgAllocPool.Init( MsgPoolSize );
}

//------------------------------------------------------------------------------
//virtual
x_inline
void base::onEndOfFrameCleanUp( void ) noexcept
{
    //
    // Compute any pending messages
    //
    FlushMessages();

    //
    // Delete all pending objects
    //
    for( local_message* pDestroy = m_DestroyMsgQueue.pop(); pDestroy; pDestroy = m_DestroyMsgQueue.pop() )
    {
        pDestroy->m_Callback();
        m_DestroyMsgPool.Free( pDestroy );
    }
}

//------------------------------------------------------------------------------
//virtual
inline
void base::qt_onRun( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
            
    //
    // Make sure we can lock
    //
    MsgForceLock();
            
    //
    // Start processing
    //
    {
        x_assert_linear( m_Debug_LQMsg );
        t_ProcessMessages( );
                
        //
        // Let the user handle any work for its manager
        //
        onExecute();
    }
            
    //
    // Remove the consuming flag
    //
    MsgUnlock();
}
        
//------------------------------------------------------------------------------
// virtual
inline
void base::qt_onDone( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    for( int i = 0; i < m_nTriggers; i++ )
    {
        // must notify the trigger that we are done
        NotifyTrigger( *m_TriggerList[i] );
    }
}
            
//------------------------------------------------------------------------------
// Description:
//      Removes the component from the world, which means it won't be updated any more
//------------------------------------------------------------------------------
x_inline
void base::onRemoveFromWorld( gb_component::base& Component ) noexcept
{
    auto    GI   = Component.getGlobalInterface();
    GI.m_Type.msgRemoveFromWorld( Component );
    
    // Remove the flags that is in the world
    Component.removeFlags( gb_component::base::component_flags::IN_THE_WORLD );

    // Some people may still be accessing the component but we will notify it that it is been taken out of the world
    Component.onNotifyOutWorld();
}
    
//------------------------------------------------------------------------------
// Description:
//      Adds the component into the world by adding it into our active list
//      and notifying the component itself.
//------------------------------------------------------------------------------
x_inline
void base::onAddToWorld( gb_component::base& Component ) noexcept
{
    auto&   GI   = Component.getGlobalInterface();
     
    //
    // We also need to sync the component with its sync point
    // We also need to prepare the flags
    //
    x_constexprvar u32  RemoveFlags = ~( gb_component::base::T0 );
    const u32           AddFlags    = gb_component::base::component_flags::IN_THE_WORLD | ( GI.m_SyncPoint.getICurrent() & flags::MASK_T0 );

    // Set the flags
    for( gb_component::base::flags LocalFlags = Component.m_qtFlags.load(); Component.m_qtFlags.compare_exchange_weak( LocalFlags, gb_component::base::flags{ AddFlags | (LocalFlags.m_Flags & RemoveFlags) } ) == false; );

    x_assert( (( Component.m_qtFlags.load().m_Flags ^ GI.m_SyncPoint.getICurrent() ) & gb_component::base::flags::MASK_T0) == false );

    // Before to put it in the quantum world we will send a notification
    // last chance for the user to do something safely
    Component.onNotifyInWorld();

    // Officially add the component into the world
    GI.m_Type.msgAddToWorld( Component );
}
   
//------------------------------------------------------------------------------
// Description:
//      Registers a component type in the manager
//------------------------------------------------------------------------------
x_inline
void base::RegisterComponentType( gb_component::type_base& Type ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );
    
    if( m_pTypeHead ) m_pTypeHead->m_pMgrPrev = &Type;
    Type.m_pMgrNext = m_pTypeHead;
    Type.m_pMgrPrev = nullptr;
    m_pTypeHead     = &Type;
}

//------------------------------------------------------------------------------
// Description:
//      Removes the component from the world, which means it won't be updated any more
//------------------------------------------------------------------------------
x_inline
void base::UnregisterComponentType ( gb_component::type_base& Type ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );

    if( &Type == m_pTypeHead )
    {
        x_assert( Type.m_pMgrPrev == nullptr );
        m_pTypeHead = Type.m_pMgrNext;
        if( m_pTypeHead ) m_pTypeHead->m_pMgrPrev = nullptr;
    }
    else
    {
        x_assert( Type.m_pMgrPrev );
        
        if( Type.m_pMgrNext )
        {
            // somewhere in the middle
            Type.m_pMgrPrev->m_pMgrNext = Type.m_pMgrNext;
            Type.m_pMgrNext->m_pMgrPrev = Type.m_pMgrPrev;
        }
        else
        {
            //end of the list
            Type.m_pMgrPrev->m_pMgrNext = nullptr;
        }
    }
    
    Type.m_pMgrPrev = nullptr;
    Type.m_pMgrNext = nullptr;
}
 
//------------------------------------------------------------------------------
// Description:
//      It will run each component as a independent job
//      This is not the most optimum way to do this.
//------------------------------------------------------------------------------
x_inline
void base::onExecute( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assert_linear( m_Debug_LQMsg );
    
    // Start running components
    x_job_block JobBlock;
    for( gb_component::type_base* pNext = m_pTypeHead; pNext; pNext = pNext->m_pMgrNext )
        JobBlock.SubmitJob( [pNext]( void )
    {
        pNext->qt_Execute();
    });
    
    JobBlock.Join();
}

//------------------------------------------------------------------------------
// Description:
//      Lets the type remove the component officially
//------------------------------------------------------------------------------
x_inline
void base::onDestroy( gb_component::base& Component ) noexcept
{
    x_assert( Component.isInWorld() == false );
    Component.getGlobalInterface().m_Type.msgDestroy( Component );
}
    
///////////////////////////////////////////////////////////////////////////////
// END NAME SPACE
///////////////////////////////////////////////////////////////////////////////
}
