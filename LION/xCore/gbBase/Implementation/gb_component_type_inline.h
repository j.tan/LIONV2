//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component {


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT TYPE BASE
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
x_inline 
void type_base::msgAddToWorld( gb_component::base& Comp ) noexcept
{
    x_assert_linear( m_Debug_LQ );

    if( m_pHead ) m_pHead->m_pMgrPrev = &Comp;
    Comp.m_pMgrNext = m_pHead;
    Comp.m_pMgrPrev = nullptr;
    m_pHead = &Comp;
}
        
//------------------------------------------------------------------------------
x_inline 
void type_base::msgRemoveFromWorld( gb_component::base& Comp ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert( Comp.isInWorld() );

    if( &Comp == m_pHead )
    {
        x_assert( Comp.m_pMgrPrev == nullptr );
        m_pHead = Comp.m_pMgrNext;
        if( m_pHead ) m_pHead->m_pMgrPrev = nullptr;
    }
    else
    {
        x_assert( Comp.m_pMgrPrev );
                
        if( Comp.m_pMgrNext )
        {
            // somewhere in the middle
            Comp.m_pMgrPrev->m_pMgrNext = Comp.m_pMgrNext;
            Comp.m_pMgrNext->m_pMgrPrev = Comp.m_pMgrPrev;
        }
        else
        {
            //end of the list
            Comp.m_pMgrPrev->m_pMgrNext = nullptr;
        }
    }
            
    Comp.m_pMgrPrev = nullptr;
    Comp.m_pMgrNext = nullptr;
}
        
//------------------------------------------------------------------------------
x_inline 
void type_base::qt_Execute( void ) noexcept
{
    // Start running components
    x_job_block JobBlock;
    for( gb_component::base* pNext = m_pHead; pNext; pNext = pNext->m_pMgrNext )
        JobBlock.SubmitJob( [pNext]( void )
        {
            pNext->qt_onRun();
        });
            
    JobBlock.Join();
}



///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// COMPONENT TYPE POOL
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
// Description:
//          Allocates a component as well as construct it
//------------------------------------------------------------------------------
template< typename T_COMP >
void type_entity_pool<T_COMP>::msgDestroy ( base& Component ) noexcept
{
    x_assert_quantum( this->m_Debug_LQ );
    
    // Let the manager know that it must remove all references for this entity
    Component.getGlobalInterface().m_GameMgr.qt_onEntityDestroy( Component.SafeCast<entity>() );
    
    // Now you can deal with the rest
    t_parent::msgDestroy( Component );
}
    
//------------------------------------------------------------------------------
// Description:
//          Allocates a component as well as construct it
//------------------------------------------------------------------------------
template< typename T_COMP >
entity& type_entity_pool<T_COMP>::CreateEntity( const const_data* pData ) noexcept
{
    static_assert( T_COMP::t_is_entity,"This is not an entity" );
    //x_assert_quantum( this->m_Debug_LQ );
    
    compact_component& Node = this->m_MemoryPool.popDontConstruct();
    
    // Construct our component in with the right order (mutable data first)
    (void)x_construct( compact_component, &Node,
                      {
                          gb_component::base::base_construct_info
                          {
                              Node.m_Component,
                              t_parent::m_GlobalInterface,
                              (t_parent::m_GlobalInterface.m_SyncPoint.getICurrent() & T_COMP::flags::MASK_T0) ? T_COMP::flags::MASK_T0 : 0,
                              {{
                                  x_construct( typename T_COMP::mutable_data, { &Node.m_MutableData[0] } ),
                                  x_construct( typename T_COMP::mutable_data, { &Node.m_MutableData[1] } ),
                              }},
                              ( pData ) ? const_cast<const_data*>( pData ) : nullptr
                          }
                      });
    
    // Ok we are ready to return the component
    return Node.m_Component;
}
    
//------------------------------------------------------------------------------
// Description:
//      Allocates a component as well as construct it
//------------------------------------------------------------------------------
template< typename T_COMP > x_inline
base& type_pool<T_COMP>::CreateComponent( entity& Entity, const const_data* pData ) noexcept
{
//    x_assert_quantum( m_Debug_LQ );

    // Create an un-constructed object
    compact_component& Node = m_MemoryPool.popDontConstruct();

    // Construct our component in with the right order (mutable data first)
    (void)x_construct( compact_component, &Node, 
    { 
        gb_component::base::base_construct_info
        { 
            Entity, 
            m_GlobalInterface, 
            ( (m_GlobalInterface.m_SyncPoint.getICurrent() & T_COMP::flags::MASK_T0) ) ? T_COMP::flags::MASK_T0 : 0,
            {{
                x_construct( typename t_component::mutable_data, { &Node.m_MutableData[0] } ),
                x_construct( typename t_component::mutable_data, { &Node.m_MutableData[1] } ),
            }},
            ( pData ) ? const_cast<const_data*>( pData ) : nullptr
        } 
    });

    // Add the component to the entity
    Entity.setupAddComponent( Node.m_Component );
        
    // Ok we are ready to return the component
    return Node.m_Component;
}

///////////////////////////////////////////////////////////////////////////////
// END NAME SPACE
///////////////////////////////////////////////////////////////////////////////
}
