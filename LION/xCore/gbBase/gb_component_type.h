//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_component
{
    //------------------------------------------------------------------------------
    // Description:
    //      This class gives the common functionality for a given type of component
    //      It particular their allocation and deletion.
    //      It also provides the mechanism to identify each component type
    //------------------------------------------------------------------------------
    class type_base 
    {
        x_object_type( type_base, is_linear, rtti_start, is_not_copyable, is_not_movable )

    public:

        using                   guid                    = xguid<t_self>;
        x_constexprvar bool     t_is_component_type     = true;

    public:
        constexpr                           type_base           ( void ) = delete;
        virtual                            ~type_base           ( void )                                                            noexcept = default; 
        x_orforceconst                      type_base           ( gb_component_mgr::base& ComponentMgr, gb_game_graph::base& GameMgr, gb_game_graph::sync_point& SyncPoint ) noexcept :
                                                            m_GlobalInterface
                                                            { 
                                                                ComponentMgr, 
                                                                *this, 
                                                                GameMgr,
                                                                SyncPoint 
                                                            } { x_assert_linear( m_Debug_LQ ); }

        virtual         entity&             CreateEntity            ( const const_data* pData )                                     noexcept { x_assume(0); return *(entity*)(nullptr); }
        virtual         base&               CreateComponent         ( entity& Entity, const const_data* pData )                     noexcept = 0;
        virtual         void                msgDestroy              ( base& Component )                                             noexcept = 0;
        virtual         guid                getGuid                 ( void )                                                const   noexcept = 0;
        virtual         const char*         getTypeName             ( void )                                                const   noexcept = 0;
        x_forceinline   base*               getHead                 ( void )                                                        noexcept { return m_pHead; }
        x_forceinline   const base*         getHead                 ( void )                                                const   noexcept { return m_pHead; }
        x_forceinline   type_base*          getNext                 ( void )                                                        noexcept { return m_pMgrNext; }
        x_forceconst    const type_base*    getNext                 ( void )                                                const   noexcept { return m_pMgrNext; }
        x_forceinline   type_base*          getPrev                 ( void )                                                        noexcept { return m_pMgrPrev; }
        x_forceconst    const type_base*    getPrev                 ( void )                                                const   noexcept { return m_pMgrPrev; }

    protected:

        using global_interface = base::global_interface;
        
    protected:
        
        x_inline        void                msgAddToWorld           ( gb_component::base& Comp )                                    noexcept;
        x_inline        void                msgRemoveFromWorld      ( gb_component::base& Comp )                                    noexcept;
        x_inline        void                qt_Execute              ( void )                                                        noexcept;

    protected:

        x_debug_linear_quantum          m_Debug_LQ              {};
        gb_component::base*             m_pHead                 { nullptr };
        global_interface                m_GlobalInterface;
        type_base*                      m_pMgrNext              { nullptr };
        type_base*                      m_pMgrPrev              { nullptr };
        
    protected:

        friend class gb_component_mgr::base;
    };

    //------------------------------------------------------------------------------
    // Description:
    //      Specializes the component type into a particular memory scheme for components.
    //      This is the class that it is used as the default to allocate component types.
    //------------------------------------------------------------------------------
    template< typename T_COMP >
    class type_pool : public type_base
    {
        x_object_type( type_pool, is_linear, rtti(type_base), is_not_copyable, is_not_movable )

    public:
        using t_component = T_COMP;

    public:

        constexpr                           type_pool                   (   gb_component_mgr::base&       ComponentMgr, 
                                                                            gb_game_graph::base&          GameMgr, 
                                                                            gb_game_graph::sync_point&    SyncPoint )           noexcept            : type_base( ComponentMgr, GameMgr, SyncPoint ) { }
        virtual                            ~type_pool                   ( void )                                                noexcept override   = default;
        virtual             guid            getGuid                     ( void )                                        const   noexcept override   { return getCompileTimeStaticGuid();                       }
        virtual             const char*     getTypeName                 ( void )                                        const   noexcept override   { return t_component::t_type_string;              }
        x_forceinline static    guid        getCompileTimeStaticGuid    ( void )                                                noexcept
        {
            static guid         s_CompileTimeGuid{ t_component::t_type_string };
            return s_CompileTimeGuid;
        }

    protected:

        struct compact_component
        {
            struct mutable_data 
            { 
                alignas(  typename t_component::mutable_data ) xbyte m_Data[sizeof( typename t_component::mutable_data)]; 
                ~mutable_data( void )
                {
                    (void)x_destruct( reinterpret_cast<typename  t_component::mutable_data*>( &m_Data ) );
                }
            };

            x_forceinline  compact_component( const typename t_component::base_construct_info& C ) noexcept : m_Component( C ) {}

            t_component     m_Component;
            mutable_data    m_MutableData[2];       // we want this data to remain uninitialize in the constructor seems is going to be constructed outside
        };

        x_constexprvar int  COMPONENT_MEMBER_OFFSET = offsetof( compact_component, m_Component );
        using               pool                    = x_ll_page_pool_jitc< compact_component, t_component::t_component_pool_paged_size >;

    protected:

        virtual     base&       CreateComponent                ( entity& Entity, const const_data* pData )             noexcept override;
        virtual     void        msgDestroy                     ( base& Component )                                     noexcept override
        {
            x_assert_quantum( m_Debug_LQ );

            // make sure that destructor gets call
            compact_component* pComponent = reinterpret_cast<compact_component*>( reinterpret_cast<xbyte*>(&Component) - COMPONENT_MEMBER_OFFSET );
            
            m_MemoryPool.push( *pComponent );
        }

    protected:
        
        pool                        m_MemoryPool     {};
    };

    //------------------------------------------------------------------------------
    // Description:
    //      For component entities there is a need to farther specialize the type pool.
    //      Because entity components require special creation/construction.
    //------------------------------------------------------------------------------
    template< typename T_COMP >
    class type_entity_pool final : public type_pool<T_COMP>
    {
        x_object_type( type_entity_pool, is_linear, rtti(type_pool<T_COMP>), is_not_copyable, is_not_movable )

    public:
        
        using               compact_component = typename t_parent::compact_component;

    public:

        x_forceconst type_entity_pool( gb_component_mgr::base& ComponentMgr, gb_game_graph::base& GameMgr, gb_game_graph::sync_point& SyncPoint ) noexcept : t_parent{ ComponentMgr, GameMgr, SyncPoint } {}

    protected:

        virtual entity& CreateEntity( const const_data* pData ) noexcept override;
        virtual void msgDestroy ( base& Component ) noexcept override;
    };
}
