                                                    //----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#include <random>
#include <stdlib.h>

//---------------------------------------------------------------------------------
// Test 5 
//---------------------------------------------------------------------------------
namespace gb_unit_test_05
{
    class my_game_graph;
    
    //---------------------------------------------------------------------------------
    // BUILDING MY OWN ENTITY
    //---------------------------------------------------------------------------------
    class my_render_component final : public gb_component::base
    {
        x_object_type( my_render_component, is_quantum_lock_free, rtti(gb_component::base), is_not_copyable, is_not_movable )

    public: // --- class traits ---

        x_constexprvar char* const  t_type_string                   = "my_render_component";
        using                       t_type                          = gb_component::type_pool<t_self>;

        struct const_data : public gb_component::const_data
        {
            x_object_type( const_data, is_linear, rtti(gb_component::const_data), is_not_copyable, is_not_movable )

            constexpr                   const_data      ( const guid Guid ) noexcept : t_parent{ Guid } {}

            xcolor m_ShareColor {};
        };
        
    public: // --- public functions ---

         my_render_component( const base_construct_info& C ) noexcept : t_parent( C ) {}

    protected: // --- class hierarchy functions ---
        
        virtual void onExecute( void ) noexcept override
        {
            x_assert_quantum( m_Debug_LQ );
            x_assert_linear( m_Debug_LQMsg );
            
            t_parent::onExecute();

            // I guess we would do some render call here...
            x_assert( getConstData<const_data>().m_ShareColor >= 1 );
            x_assert( getConstData<const_data>().m_ShareColor <= 2 );
        }

        virtual         void                onPropEnum              ( xproperty_enum&  Enum  )  const   noexcept override 
        { 
            x_assert_quantum( m_Debug_LQ ); 
            x_assert_linear( m_Debug_LQMsg );

            Enum.beginScope( X_STR("my_render_component"), [&]( xproperty_enum::scope& Scope )
            {
                t_parent::onPropEnum( Enum );
                Scope.AddProperty<xprop_s32>( X_STR("RenderSize") ); 
                Scope.AddProperty<xprop_color>( X_STR("MyColor") ); 
            });
        }

        virtual         bool                onPropQuery             ( xproperty_query& Query )          noexcept override 
        { 
            x_assert_quantum( m_Debug_LQ ); 
            x_assert_linear( m_Debug_LQMsg );

            X_PROP_MAINSCOPE( Query, "my_render_component", 
                // --- Scopes ---
                X_PROP_SCOPE_CHILD( t_parent::onPropQuery( Query ) )
                , // --- Properties --- (notice the comma)
                X_PROP_IS_PROP( "RenderSize",   xprop_s32::SendOrReceive( Query, m_RenderSize, 0, 10 ) )
                X_PROP_IS_PROP( "MyColor",      xprop_color::SendOrReceive( Query, m_MyColor ) )
            )

            return false; 
        }

    protected: // --- class hierarchy functions ---
        
        int     m_RenderSize { 3 };
        xcolor  m_MyColor{ 128, 128, 128, 255 };
    };

    //---------------------------------------------------------------------------------
    // TEST CONSTANT DATA AS WELL
    //---------------------------------------------------------------------------------
    xarray< my_render_component::const_data*, 2> s_ConstType;

    //---------------------------------------------------------------------------------
    // BUILDING MY OWN ENTITY
    //---------------------------------------------------------------------------------
    class my_entity final : public gb_component::entity_dynamic
    {
        x_object_type( my_entity, is_quantum_lock_free, rtti(gb_component::entity_dynamic), is_not_copyable, is_not_movable )

    public: // --- class traits ---
        
        x_constexprvar char* const  t_type_string                   = "my_entity";
        x_constexprvar xuptr        t_component_pool_paged_size     = 1000;
        using                       t_type                          = gb_component::type_entity_pool<t_self>;
        x_constexprvar  int         MAX_LIFE                        = 1000;

    public: // --- mutable data ---

        struct mutable_data : t_parent::mutable_data
        {
            virtual void onUpdateMutableData( const t_mutable_base& Src ) noexcept override
            {
                const mutable_data& NewSrc = reinterpret_cast<const mutable_data&>(Src);
                // copy the hold thing
                *this = NewSrc;
            }

            xvector3       m_Velocity;
        };

    public: // --- public functions ---
        
        my_entity( const base_construct_info& C ) noexcept : t_parent( C )
        {
            auto& Data = getT1Data<mutable_data>();

            xrandom_small Rnd;
            Rnd.setSeed64( static_cast<u64>(xtimer::getNowMs()) );
            Data.m_Velocity.setup( Rnd.RandF32( -1, 1 ),Rnd.RandF32( -1, 1 ),Rnd.RandF32( -1, 1 ));
            Data.m_Velocity.Normalize();
            Data.m_L2W.RotateY( 90_deg );

            m_Death = Rnd.Rand32( 5, MAX_LIFE );

            // change the value
            m_Bools.m_I_HAVE_HEALTH = true;
        }

    protected: 
        
            X_DEFBITS( mybools, u32, 0,
                X_DEFBITS_ARG( bool, I_HAVE_ARMOR, 1),
                X_DEFBITS_ARG( bool, I_HAVE_HEALTH, 1)
            )

            enum some_enum
            {
                SOMEENUM_NULL,
                SOMEENUM_ONE,
                SOMEENUM_TWO,
                SOMEENUM_FIVE,
            };

    protected: // --- class hierarchy functions ---
        
        virtual void onExecute( void ) noexcept override
        {
            x_assert_quantum( m_Debug_LQ );
            x_assert_linear( m_Debug_LQMsg );

            t_parent::onExecute();
            
            auto& Mutable = getT1Data<mutable_data>();

            xvector3 NewPos = Mutable.m_L2W.getTranslation() + Mutable.m_Velocity;

            Mutable.m_L2W.setTranslation( NewPos );

            if( --m_Death == 0 )
            {
                msgDestroy();
                auto& Entity = m_GInterface.m_GameMgr.CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ), nullptr );
                auto& Render = m_GInterface.m_GameMgr.CreateComponent<my_render_component>( Entity, s_ConstType[ rand()%2 ] );

                Entity.setupAddComponent( Render );
                Entity.msgAddToWorld();
            }
        }

        virtual void onNotifyInWorld( void ) noexcept override
        {
            // This is an example on how to get components from an entity
            m_pRenderComp = getComponent<my_render_component>();
        }

        virtual         void                onPropEnum              ( xproperty_enum&  Enum  )  const   noexcept override 
        { 
            x_assert_quantum( m_Debug_LQ ); 
            x_assert_linear( m_Debug_LQMsg );

            Enum.beginScope( X_STR("my_entity"), [&]( xproperty_enum::scope& Scope )
            {
                t_parent::onPropEnum( Enum );
                Scope.AddProperty<xprop_s32>( X_STR("Death") ); 
                Scope.AddProperty<xprop_v3>( X_STR("Velocity") ); 
                Scope.AddProperty<xprop_bool>( X_STR("bHaveArmor") ); 
                Scope.AddProperty<xprop_bool>( X_STR("bHaveHealth") ); 
                Scope.AddProperty<xprop_enum>( X_STR("SomeEnum") ); 
            });
        }

        virtual         bool                onPropQuery             ( xproperty_query& Query )          noexcept override 
        { 
            x_assert_quantum( m_Debug_LQ ); 
            x_assert_linear( m_Debug_LQMsg );

            static const xproperty_data::data_enum::entry SomeEnumList[]
            {
                { SOMEENUM_NULL, X_STR("INVALID")   },
                { SOMEENUM_ONE,  X_STR("ONE")       },
                { SOMEENUM_TWO,  X_STR("TWO")       },
                { SOMEENUM_FIVE, X_STR("FIVE")      }
            };

            X_PROP_MAINSCOPE( Query, "my_entity", 
                // --- Scopes ---
                X_PROP_SCOPE_CHILD( t_parent::onPropQuery( Query ) )
                , // --- Properties --- (notice the comma)
                X_PROP_IS_PROP( "Death",        xprop_s32::SendOrReceive( Query, m_Death, 0, MAX_LIFE ) )
                X_PROP_IS_PROP( "Velocity",     xprop_v3::SendOrReceive( Query, getT1Data<mutable_data>().m_Velocity ) )
                X_PROP_IS_PROP( "bHaveArmor",   xprop_bool::SendOrReceive( Query, m_Bools.m_Flags, mybools::MASK_I_HAVE_ARMOR ) )
                X_PROP_IS_PROP( "bHaveHealth",  xprop_bool::SendOrReceive( Query, m_Bools.m_Flags, mybools::MASK_I_HAVE_HEALTH ) )
                X_PROP_IS_PROP( "SomeEnum",     xprop_enum::SendOrReceive( Query, m_SomeEnum, SomeEnumList, x_countof( SomeEnumList ) ) )
            )

            return false; 
        }

        
    protected:  // --- class hierarchy hidden variables ---
        
        int                     m_Death;
        my_render_component*    m_pRenderComp   { nullptr };
        mybools                 m_Bools         {};
        some_enum               m_SomeEnum      { SOMEENUM_FIVE };

    };
    
    //---------------------------------------------------------------------------------
    // MY GAME MANAGER
    //---------------------------------------------------------------------------------
    class my_game_graph final : public gb_game_graph::base
    {
        x_object_type( my_game_graph, is_quantum_lock_free, rtti(gb_game_graph::base), is_not_copyable, is_not_movable )
  
    public: // --- class constructor ---

        my_game_graph( void ) noexcept : t_parent( 20000 ) {}
   
    protected: // --- class types ---

        using entity_mgr = gb_component_mgr::general_mgr;
        using render_mgr = gb_component_mgr::general_mgr;
        using logic_sync = gb_game_graph::sync_point;

    protected: // --- class hierarchy functions ---
        
        virtual void onInitialize( void ) noexcept override
        {
            //
            // Notify our parent about initialization
            //
            t_parent::onInitialize();

            //
            // construct const data
            //
            for( auto& Entry : s_ConstType )
            {
                xndptr_s<my_render_component::const_data> ConstData;
                ConstData.New( gb_component::const_data::guid{ gb_component::const_data::guid::RESET } );
                Entry = ConstData;  
                m_ConstDataDB.RegisterAndTransferOwner( ConstData );
            }

            //
            // Register sync points
            // 
            m_LogicSyncPoint.setup( sync_point_start::guid( x_constStrCRC32("LogicSyncPoint") ), "LogicSyncPoint");
            m_SyncDB.RegisterButRetainOwnership( m_LogicSyncPoint );

            //
            // Register managers
            //
            enum : int {    t_msg_pool_size     = 1024 * 160 };
            enum : int {    t_max_entities      = 10000      };

            m_EntityMgr.setup( entity_mgr::guid( x_constStrCRC32("EntityMgr") ), "EntityMgr", t_max_entities, t_msg_pool_size );
            m_CompManagerDB.RegisterButRetainOwnership( m_EntityMgr );            

            m_RenderMgr.setup( render_mgr::guid( x_constStrCRC32("RenderMgr") ), "RenderMgr", t_max_entities, t_msg_pool_size );
            m_CompManagerDB.RegisterButRetainOwnership( m_RenderMgr );            
                         
            //
            // Build the game graph
            //
            InitializeGraphConnection( m_StartSyncPoint, m_EntityMgr, m_LogicSyncPoint );
            InitializeGraphConnection( m_LogicSyncPoint, m_RenderMgr, m_EndSyncPoint );
            
            //
            // Register all the component types
            //
            RegisterComponentAndDependencies<my_entity>( m_EntityMgr, m_LogicSyncPoint );
            RegisterComponentAndDependencies<my_render_component>( m_RenderMgr, m_EndSyncPoint );

            //
            // Initialize my const types to some value
            //
            s_ConstType[0]->m_ShareColor = 1;
            s_ConstType[1]->m_ShareColor = 2;

            //
            // Create a blue print 
            //
            gb_blueprint::guid gBlueprint;
            {
                //
                // Create a blue print
                //
                auto& Blueprint = CreateBlueprint();
                gBlueprint = Blueprint.getGuid();

                //
                // Create an entity
                //
                auto& Entity    = CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ), nullptr );
                auto& Render    = CreateComponent<my_render_component>( Entity, s_ConstType[ rand()%2 ] );

                // 
                // Add entity to the blue print
                //
                Blueprint.setup( Entity );

                //
                // Pretend that the entity came from a blue print
                //
                Entity.setupBlueprintGuid( gBlueprint );
            }
            
            //
            // Create a bunch of entities in the world
            //
            x_constexprvar int nEntities = 100;
            for( s32 i=0; i<nEntities; i++ )
            {
                auto& Blueprint = m_BlueprintDB.Find( gBlueprint );

                //
                // Create an instance of the blue print
                //
                auto& Entity = [&] () -> my_entity&
                {
                    if( i & 1 )
                    {
                        xmatrix4 L2W;
                        L2W.setIdentity();
                        L2W.setTranslation( xvector3( x_frand(-10,10) ) );
                        auto& Entity = Blueprint.CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ), L2W, *this );
                        Entity.msgAddToWorld();
                        return Entity;
                    }
                    else
                    {
                        auto& Entity    = CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ), nullptr );
                        auto& Render    = CreateComponent<my_render_component>( Entity, s_ConstType[ rand()%2 ] );
                        Entity.msgAddToWorld();
                        return Entity;
                    }
                }();

                //
                // Create a new entity
                //
                if( 0 )
                {
                    //
                    // Collect all properties
                    //
                    xvector<xproperty_data> Props;
                    Entity.LinearPropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
                    {
                        // Just collect them all inside an static array
                        Props.append() = Data;
                    });

                    //
                    // Test getting values
                    //
                    Entity.LinearPropGet( [&]( xproperty_query& Query, xproperty_data& Data )
                    {
                        //
                        // First read the results if any
                        //
                        if( Query.hasResults() )
                        {
                            const int Index  = Query.getResultsIndex();
                            auto      Res    = Query.getResults();
                
                            // Result must match the type and the name at least 
                            x_assert( Res.getType() == Props[ Query.getPropertyIndex() - 1 ].getType() );
                            x_assert( 0 == x_strcmp<xchar>( Res.m_Name, Props[ Query.getPropertyIndex() - 1 ].m_Name ) );

                            // Store the data in our result array
                            Props[ Query.getPropertyIndex() - 1 ] = Res;
                        }                                                          

                        //
                        // are we done?
                        //
                        if( Query.getPropertyIndex() == Props.getCount() ) 
                        {
                            return false;
                        }

                        //
                        // Tell which property to get next
                        //
                        Data = Props[ Query.getPropertyIndex() ];

                        return true;
                    });
                }

                int a = 55;
            } 
        }
        
        virtual void onEndFrame( void ) noexcept override
        {
            static int x=0;
            gb_game_graph::base::onEndFrame();
            x++;
            //if( (x%60)==0 )
            {
                std::cout << "Frame #" << m_nFrames << " SecondsPerFrame: " << xtimer::ToSeconds( m_Stats_LogicTime ) << "\n";
            }

            //
            // Save it to disk
            //
            LinearSaveGame( X_WSTR("temp:/testentitysaving.txt") );

            //
            // Delete all the entities
            //
            LinearDeleteAllEntities();

            //
            // Load all the entities
            //
            LinearLoadGame( X_WSTR("temp:/testentitysaving.txt") );
            // LinearSaveGame( X_WSTR("temp:/testentitysaving2.txt") );
            
            //
            // We will exit when we do 1000 frames
            //
            if( x > 4 )
            {
                m_bLoop = false;
                g_context::get().m_Scheduler.MainThreadStopWorking();
            }
        }

    protected:

        logic_sync      m_LogicSyncPoint    {};
        entity_mgr      m_EntityMgr         {};
        render_mgr      m_RenderMgr         {};
    };
    
    //---------------------------------------------------------------------------------
    // Test
    //---------------------------------------------------------------------------------
    void Test( void )
    {
        my_game_graph GameMgr;
        GameMgr.Initialize();
        g_context::get().m_Scheduler.AddJobToQuantumWorld( GameMgr );
        g_context::get().m_Scheduler.MainThreadStartsWorking();
    }
};

