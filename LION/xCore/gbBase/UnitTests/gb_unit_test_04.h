//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#include <random>

//---------------------------------------------------------------------------------
// Test 1 - Basic registration and execution one component and a very simple graph
//---------------------------------------------------------------------------------
namespace gb_unit_test_04
{
    class my_game_graph;
    
    
    //---------------------------------------------------------------------------------
    // MY GAME MANAGER
    //---------------------------------------------------------------------------------
    class my_game_graph final : public gb_game_graph::base
    {
        x_object_type( my_game_graph, is_quantum_lock_free, rtti(gb_game_graph::base), is_not_copyable, is_not_movable )

    public: // --- class constructor ---

        my_game_graph( void ) noexcept : t_parent( 20000 ) {}
       
    protected: // --- class hierarchy functions ---
         
        virtual void onInitialize( void ) noexcept override
        {
            //
            // Notify our parent about initialization
            //
            t_parent::onInitialize();
            
            //
            // Build the game graph
            //
            #if _X_TARGET_WINDOWS
                #if _X_DEBUG
                    ImportScript("x64/Debug/gb_unitest_dll_component.dll");
                #else
                    ImportScript("x64/Release/gb_unitest_dll_component.dll");
                #endif
            #elif _X_TARGET_MAC
                #if _X_DEBUG
                    ImportScript("Library/Developer/Xcode/DerivedData/TestGameLib_Basics-gbzroyhqvezvaadiapfexcmgufdd/Build/Products/Debug/libgb_unitest_dll_component.dylib");
                #else
                    ImportScript("Library/Developer/Xcode/DerivedData/TestGameLib_Basics-gbzroyhqvezvaadiapfexcmgufdd/Build/Products/Release/libgb_unitest_dll_component.dylib");

                #endif
            #endif
            gb_component_mgr::base& EntityMgr = m_CompManagerDB.Find( gb_component_mgr::base::guid( x_constStrCRC32( "EntityMgr" ) ) );
            
            InitializeGraphConnection( m_StartSyncPoint, EntityMgr, m_EndSyncPoint );
         
            //
            // Create a bunch of entities
            //
            gb_component::type_base& EntityType = m_CompTypeDB.Find( gb_component::type_base::guid{ x_constStrCRC32("my_entity") } );
            x_job_block JobBlock;
            const int nEntities = 10000; 
            for( int i = 0; i < nEntities; i++ )
            {
                const int Step = nEntities / 10;

                //
                // We add 1000 entities in parallel at a time to make sure we don't over saturate the system
                //
                for( int j=0; j<Step; j++ )
                    JobBlock.SubmitJob( [this, &EntityType]()
                    {
                       CreateEntity( EntityType, gb_component::entity::guid( gb_component::entity::guid::RESET ), nullptr ).msgAddToWorld();
                    });
                JobBlock.Join();

                i += Step;
                std::cout << "Created: " << i << " Instances\n";
            }
        }
        
        virtual void onEndFrame( void ) noexcept override
        {
            static int x=0;
            gb_game_graph::base::onEndFrame();
            x++;
            if( (x%60)==0 )
            {
                std::cout << "Frame #" << m_nFrames << " FrameMS: " << xtimer::ToMilliseconds( m_Stats_LogicTime ) << "\n";
            }
            
            //
            // We will exit when we do 1000 frames
            //
            if( x > 60*100 )
            {
                m_bLoop = false;
                g_context::get().m_Scheduler.MainThreadStopWorking();
            }
        }
    };
    
    //---------------------------------------------------------------------------------
    // Test
    //---------------------------------------------------------------------------------
    void Test( void )
    {
        my_game_graph GameMgr;
        GameMgr.Initialize();
        g_context::get().m_Scheduler.AddJobToQuantumWorld( GameMgr );
        g_context::get().m_Scheduler.MainThreadStartsWorking();
    }
};

