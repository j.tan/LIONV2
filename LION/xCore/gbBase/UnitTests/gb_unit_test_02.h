//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#include <random>
#include <stdlib.h>

//---------------------------------------------------------------------------------
// Test 2 
// This test is meant to test a more complex entity (entity + dependent component)
// It also test a bit more complex game graph, (two dependent managers)         
//---------------------------------------------------------------------------------
namespace gb_unit_test_02
{
    class my_game_graph;
    
    //---------------------------------------------------------------------------------
    // BUILDING MY OWN ENTITY
    //---------------------------------------------------------------------------------
    class my_render_component final : public gb_component::base
    {
        x_object_type( my_render_component, is_quantum_lock_free, rtti(gb_component::base), is_not_copyable, is_not_movable )

    public: // --- class traits ---

        x_constexprvar char* const  t_type_string                   = "my_render_component";
        using                       t_type                          = gb_component::type_pool<t_self>;

        struct const_data : public gb_component::const_data
        {
            x_object_type( const_data, is_linear, rtti(gb_component::const_data), is_not_copyable, is_not_movable )

            constexpr                   const_data      ( const guid Guid ) noexcept : t_parent{ Guid } {}

            float m_Color {};
        };
        
    public: // --- public functions ---

         my_render_component( const base_construct_info& C ) noexcept : t_parent( C ) {}

    protected: // --- class hierarchy functions ---
        
        virtual void onExecute( void ) noexcept override
        {
            x_assert_quantum( m_Debug_LQ );
            x_assert_linear( m_Debug_LQMsg );
            
            t_parent::onExecute();

            // I guess we would do some render call here...
            x_assert( getConstData<const_data>().m_Color >= 1 );
            x_assert( getConstData<const_data>().m_Color <= 2 );
        }
    };

    //---------------------------------------------------------------------------------
    // TEST CONSTANT DATA AS WELL
    //---------------------------------------------------------------------------------
    xarray< x_ll_share_ref<my_render_component::const_data>, 2> s_ConstType;

    //---------------------------------------------------------------------------------
    // BUILDING MY OWN ENTITY
    //---------------------------------------------------------------------------------
    class my_entity final : public gb_component::entity
    {
        x_object_type( my_entity, is_quantum_lock_free, rtti(gb_component::entity), is_not_copyable, is_not_movable )

    public: // --- class traits ---
        
        x_constexprvar  char*   t_type_string                   = "my_entity";
        using                   t_type                          = gb_component::type_entity_pool<t_self>;
        
    public: // --- public functions ---
        
        my_entity( const base_construct_info& C ) noexcept : t_parent( C )
        {
            // Generate random number from 5 to 1000-1
            std::random_device                  rd;
            std::mt19937                        mt(rd());
            std::uniform_int_distribution<int>  dist(5, 1000 );
            m_Death = dist(mt);
        }
        
    protected: // --- class hierarchy functions ---
        
        virtual void onExecute( void ) noexcept override
        {
            x_assert_quantum( m_Debug_LQ );
            x_assert_linear( m_Debug_LQMsg );
            
            t_parent::onExecute();
            
            if( --m_Death == 0 )
            {
                msgDestroy();
                auto& Entity = m_GInterface.m_GameMgr.CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ), nullptr );
                auto& Render = m_GInterface.m_GameMgr.CreateComponent<my_render_component>( Entity, s_ConstType[ rand()%2 ].operator->() );

                Entity.setupAddComponent( Render );
                Entity.msgAddToWorld();
            }
        }

        virtual void onNotifyInWorld( void ) noexcept override
        {
            // This is an example on how to get components from an entity
            m_pRenderComp = getComponent<my_render_component>();
        }
        
    protected:  // --- class hierarchy hidden variables ---
        
        int                     m_Death;
        my_render_component*    m_pRenderComp { nullptr };

    };
    
    //---------------------------------------------------------------------------------
    // MY GAME MANAGER
    //---------------------------------------------------------------------------------
    class my_game_graph final : public gb_game_graph::base
    {
        x_object_type( my_game_graph, is_quantum_lock_free, rtti(gb_game_graph::base), is_not_copyable, is_not_movable )
  
    public: // --- class constructor ---

        my_game_graph( void ) noexcept : t_parent( 20000 ) {}
   
    protected: // --- class types ---

        using entity_mgr = gb_component_mgr::general_mgr;
        using render_mgr = gb_component_mgr::general_mgr;
        using logic_sync = gb_game_graph::sync_point;

    protected: // --- class hierarchy functions ---
        
        virtual void onInitialize( void ) noexcept override
        {
            //
            // Notify our parent about initialization
            //
            t_parent::onInitialize();

            //
            // Create constant data
            //
            for( auto& Entry : s_ConstType )
                Entry.New( gb_component::const_data::guid{ gb_component::const_data::guid::RESET } );

            //
            // Register sync points
            // 
            m_LogicSyncPoint.setup( sync_point_start::guid( x_constStrCRC32("LogicSyncPoint") ), "LogicSyncPoint");
            m_SyncDB.RegisterButRetainOwnership( m_LogicSyncPoint );

            //
            // Register managers
            //
            enum : int {    t_msg_pool_size     = 1024 * 160 };
            enum : int {    t_max_entities      = 10000      };

            m_EntityMgr.setup( entity_mgr::guid( x_constStrCRC32("EntityMgr") ), "EntityMgr", t_max_entities, t_msg_pool_size );
            m_CompManagerDB.RegisterButRetainOwnership( m_EntityMgr );            

            m_RenderMgr.setup( render_mgr::guid( x_constStrCRC32("RenderMgr") ), "RenderMgr", t_max_entities, t_msg_pool_size );
            m_CompManagerDB.RegisterButRetainOwnership( m_RenderMgr );            
                         
            //
            // Build the game graph
            //
            InitializeGraphConnection( m_StartSyncPoint, m_EntityMgr, m_LogicSyncPoint );
            InitializeGraphConnection( m_LogicSyncPoint, m_RenderMgr, m_EndSyncPoint );
            
            //
            // Register all the component types
            //
            RegisterComponentAndDependencies<my_entity>( m_EntityMgr, m_LogicSyncPoint );
            RegisterComponentAndDependencies<my_render_component>( m_RenderMgr, m_EndSyncPoint );

            //
            // Initialize my const types to some value
            //
            s_ConstType[0]->m_Color = 1;
            s_ConstType[1]->m_Color = 2;
            
            //
            // Create a bunch of entities
            //
            
            x_job_block JobBlock;
            const xuptr nInstances = 10000;
            for( xuptr i = 0; i < nInstances; )
            {
                const int Step = nInstances / 20;
                //
                // We add 1000 entities in parallel at a time to make sure we don't over saturate the system
                //
                for( int j=0; j<Step; j++ )
                    JobBlock.SubmitJob( [this]() 
                    {
                        auto& Entity = CreateEntity<my_entity>( gb_component::entity::guid( gb_component::entity::guid::RESET ), nullptr );
                        auto& Render = CreateComponent<my_render_component>( Entity, s_ConstType[ rand()%2 ].operator->() );

                        Entity.msgAddToWorld();
                    });
                JobBlock.Join();

                i += Step;
                std::cout << "Created: " << i << " Instances\n";
            }
        }
        
        virtual void onEndFrame( void ) noexcept override
        {
            static int x=0;
            gb_game_graph::base::onEndFrame();
            x++;
            if( (x%60)==0 )
            {
                std::cout << "Frame #" << m_nFrames << " FrameMS: " << xtimer::ToMilliseconds( m_Stats_LogicTime ) << "\n";
            }
            
            //
            // We will exit when we do 1000 frames
            //
            if( x > 60*100 )
            {
                m_bLoop = false;
                g_context::get().m_Scheduler.MainThreadStopWorking();
            }
        }

    protected:

        logic_sync      m_LogicSyncPoint    {};
        entity_mgr      m_EntityMgr         {};
        render_mgr      m_RenderMgr         {};
    };
    
    //---------------------------------------------------------------------------------
    // Test
    //---------------------------------------------------------------------------------
    void Test( void )
    {
        my_game_graph GameMgr;
        GameMgr.Initialize();
        g_context::get().m_Scheduler.AddJobToQuantumWorld( GameMgr );
        g_context::get().m_Scheduler.MainThreadStartsWorking();
    }
};

