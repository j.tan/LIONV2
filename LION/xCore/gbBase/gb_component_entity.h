 //----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_blueprint
{
    struct bundled_entity;
    class master;
    using guid = xguid<bundled_entity>;
}

namespace gb_component
{
    //------------------------------------------------------------------------------
    // Description:
    //      The entity is the container for a collection of components for a single entity.
    //      Generally speaking you will have as many entities and game object instances.
    //      A Entity also has the GUID for an instance. This GUID together with the component type
    //      allows any component access to any other entity component.
    //------------------------------------------------------------------------------
    class entity : public base
    {
        x_object_type( entity, is_quantum_lock_free, rtti(base), is_not_copyable, is_not_movable )

    public:

        x_constexprvar bool t_is_entity     = true;
        using               guid            = xguid<entity>;

    public:

        x_forceinline                           entity                      ( const base_construct_info& C )                                                                noexcept;
        x_forceinline       void                setupBlueprintGuid          ( gb_blueprint::guid Guid )                                                                     noexcept { m_BlueprintGuid = Guid; }
        x_inline            void                msgAddToWorld               ( void )                                                                                        noexcept;
        x_inline            void                msgDestroy                  ( void )                                                                                        noexcept;
        x_inline            void                msgRemoveFromWorld          ( void )                                                                                        noexcept;
        x_forceinline       guid                getGuid                     ( void )                                                                                const   noexcept { return m_Guid; }
        x_forceinline       auto                getBlueprintGuid            ( void )                                                                                const   noexcept { return m_BlueprintGuid; }
        virtual             const xmatrix4&     getTransform                ( void )                                                                                const   noexcept { return xmatrix4::Identity(); }
        virtual             void                LinearTransformSet          ( const xmatrix4& L2W, void(*Callback)(xmatrix4& DestL2W, const xmatrix4& SrcL2W) =[](xmatrix4&D, const xmatrix4&S){D=S;} ) noexcept {}
        x_incppfile         void                msgTransformSet             ( const xmatrix4& L2W, void(*Callback)(xmatrix4& DestL2W, const xmatrix4& SrcL2W) =[](xmatrix4&D, const xmatrix4&S){D=S;} ) noexcept;
        template< typename T >
        x_inline            T*                  getComponent                 ( gb_component::base* pFrom = nullptr )                                                const   noexcept;
        x_incppfile         void                LinearSaveToBlueprintBundle  ( gb_blueprint::bundled_entity& Bundle )                                               const   noexcept;
        x_incppfile static  entity&             LinearLoadFromBlueprintBundle( guid EnityGuid, gb_blueprint::bundled_entity& Bundle, gb_game_graph::base& GameMgr )         noexcept;
        x_incppfile         void                LinearPropEnum               (  xfunction<void( xproperty_enum&     Enum,  
                                                                                xproperty_data& Data)>              Function, 
                                                                                bool                                bFullPath   = true, 
                                                                                xproperty_enum::mode                Mode        = xproperty_enum::mode::REALTIME )  const   noexcept;
        x_incppfile         bool                LinearPropGet                ( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function ) const  noexcept;
        x_incppfile         bool                LinearPropSet                ( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function )        noexcept;
        x_inline            void                LinearAddToWorld             ( void )                                                                                       noexcept;
        x_inline            void                LinearDestroy                ( void )                                                                                       noexcept;

    protected:

        x_inline            void                setupAddComponent           ( base& Component )                                                                             noexcept;
        virtual             bool                onPropQuery                 ( xproperty_query& Query )                                                                      noexcept override;   
        virtual             void                onPropEnum                  ( xproperty_enum&  Enum  )                                                              const   noexcept override; 
        virtual             void                onTransformSet              ( const xmatrix4& L2W, void(*Callback)(xmatrix4&, const xmatrix4&) )                            noexcept {}

    protected:

        guid                            m_Guid          { nullptr };            // Guid of the entity
        gb_blueprint::guid              m_BlueprintGuid { nullptr };            // Guid of the blue print that was used to create this entity

    public:

        friend class gb_game_graph::base;
        template< typename T_COMP > friend class type_pool;
    };

    //------------------------------------------------------------------------------
    // Description:
    //------------------------------------------------------------------------------
    class entity_static : public entity
    {
        x_object_type( entity_static, is_quantum_lock_free, rtti(entity), is_not_copyable, is_not_movable )

    protected:

        x_forceinline                           entity_static               ( const base_construct_info& C )            noexcept : t_parent{ C }{}
        virtual         const xmatrix4&         getTransform                ( void )                            const   noexcept override { return m_L2W; }
        virtual         void                    LinearTransformSet          ( const xmatrix4& L2W, void(*Callback)(xmatrix4& DestL2W, const xmatrix4& SrcL2W) =[](xmatrix4&D, const xmatrix4&S){D=S;} ) noexcept override { Callback(m_L2W, L2W ); }
        virtual         bool                    onPropQuery                 ( xproperty_query& Query )                  noexcept override   
        { 
            x_assert_quantum( m_Debug_LQ ); 
            x_assert_linear( m_Debug_LQMsg ); 
        
            X_PROP_MAINSCOPE( Query, "Static", 
                // --- Scopes ---
                X_PROP_SCOPE_CHILD( t_parent::onPropQuery( Query ) )
                , // --- Properties --- (notice the comma)
                X_PROP_IS_PROP( "Scale",

                    if( Query.isReciving() )
                    {
                        auto Rot  = m_L2W.getRotationR3();
                        m_L2W.setup( xprop_v3::Recive( Query ), Rot, m_L2W.getTranslation() );
                    }
                    else
                    {
                        xprop_v3::Send( Query, m_L2W.getScale() );
                    }
                )
                X_PROP_IS_PROP( "Rotation",
                    
                    if( Query.isReciving() )
                    {
                        auto    Scale   = m_L2W.getScale();
                        m_L2W.setup( Scale, xprop_r3::Recive( Query ), m_L2W.getTranslation() );
                    }
                    else
                    {
                        xprop_r3::Send( Query, m_L2W.getRotationR3() );
                    }
                )
                X_PROP_IS_PROP( "Position", 

                    if( Query.isReciving() )
                    {
                        m_L2W.setTranslation( xprop_v3::Recive( Query ) );
                    }
                    else
                    {
                        xprop_v3::Send( Query, m_L2W.getTranslation() );
                    }
                )
            )
        
            return false; 
        }
        virtual         void                onPropEnum              ( xproperty_enum&  Enum  )  const   noexcept override 
        { 
            x_assert_quantum( m_Debug_LQ ); 
            x_assert_linear( m_Debug_LQMsg );

            Enum.beginScope( X_STR("Static"), [&]( xproperty_enum::scope& Scope )
            {
                t_parent::onPropEnum( Enum );

                // Transform properties
                Scope.AddProperty<xprop_v3>( X_STR("Position") );
                Scope.AddProperty<xprop_r3>( X_STR("Rotation") );
                Scope.AddProperty<xprop_v3>( X_STR("Scale")    );
            });
        }

        virtual void onTransformSet ( const xmatrix4& L2W, void(*Callback)(xmatrix4&, const xmatrix4&) ) noexcept override
        { 
            Callback( m_L2W, L2W );
        }

    protected:

        xmatrix4 m_L2W { xmatrix4::Identity() };
    };

    //------------------------------------------------------------------------------
    // Description:
    //------------------------------------------------------------------------------
    class entity_dynamic : public entity
    {
        x_object_type( entity_dynamic, is_quantum_lock_free, rtti(entity), is_not_copyable, is_not_movable )

    protected:

        struct mutable_data : public entity::mutable_data
        {
            virtual void onUpdateMutableData( const t_mutable_base& Src ) noexcept override { *this = reinterpret_cast<const mutable_data&>(Src); }
            xmatrix4 m_L2W{ xmatrix4::Identity() };
        };

    protected:

        x_forceinline                           entity_dynamic              ( const base_construct_info& C )            noexcept : t_parent{ C }{}
        virtual             const xmatrix4&     getTransform                ( void )                            const   noexcept override { return getT0Data<mutable_data>().m_L2W; }
        virtual             void                LinearTransformSet          ( const xmatrix4& L2W, void(*Callback)(xmatrix4& DestL2W, const xmatrix4& SrcL2W) =[](xmatrix4&D, const xmatrix4&S){D=S;} ) noexcept override { Callback(getT1Data<mutable_data>().m_L2W, L2W ); }
        virtual             bool                onPropQuery                 ( xproperty_query& Query )                  noexcept override   
        { 
            x_assert_quantum( m_Debug_LQ ); 
            x_assert_linear( m_Debug_LQMsg ); 
        
            X_PROP_MAINSCOPE( Query, "Dynamic", 
                // --- Scopes ---
                X_PROP_SCOPE_CHILD( t_parent::onPropQuery( Query ) )
                , // --- Properties --- (notice the comma)
                X_PROP_IS_PROP( "Scale",

                    if( Query.isReciving() )
                    {
                        auto& L2W = getT1Data<mutable_data>().m_L2W;
                        auto Rot  = L2W.getRotationR3();
                        L2W.setup( xprop_v3::Recive( Query ), Rot, L2W.getTranslation() );
                    }
                    else
                    {
                        const auto& L2W = getT1Data<mutable_data>().m_L2W;
                        xprop_v3::Send( Query, L2W.getScale() );
                    }
                )
                X_PROP_IS_PROP( "Rotation",
                    
                    if( Query.isReciving() )
                    {
                        auto&   L2W     = getT1Data<mutable_data>().m_L2W;
                        auto    Scale   = L2W.getScale();
                        L2W.setup( Scale, xprop_r3::Recive( Query ), L2W.getTranslation() );
                    }
                    else
                    {
                        const auto& L2W = getT1Data<mutable_data>().m_L2W;
                        xprop_r3::Send( Query, L2W.getRotationR3() );
                    }
                )
                X_PROP_IS_PROP( "Position", 

                    if( Query.isReciving() )
                    {
                        getT1Data<mutable_data>().m_L2W.setTranslation( xprop_v3::Recive( Query ) );
                    }
                    else
                    {
                        const auto& L2W = getT1Data<mutable_data>().m_L2W;
                        xprop_v3::Send( Query, L2W.getTranslation() );
                    }
                )
            )
        
            return false; 
        }
        virtual         void                onPropEnum              ( xproperty_enum&  Enum  )  const   noexcept override 
        { 
            x_assert_quantum( m_Debug_LQ ); 
            x_assert_linear( m_Debug_LQMsg );

            Enum.beginScope( X_STR("Dynamic"), [&]( xproperty_enum::scope& Scope )
            {
                // Base properties
                t_parent::onPropEnum( Enum );

                // Transform properties
                Scope.AddProperty<xprop_v3>( X_STR("Position") );
                Scope.AddProperty<xprop_r3>( X_STR("Rotation") );
                Scope.AddProperty<xprop_v3>( X_STR("Scale")    );
            });
        }

        virtual void onTransformSet ( const xmatrix4& L2W, void(*Callback)(xmatrix4&, const xmatrix4&) ) noexcept override  
        { 
            Callback( getT1Data<mutable_data>().m_L2W, L2W );
        }
    };
}