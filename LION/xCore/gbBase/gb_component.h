//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace gb_game_graph
{
    class base;
    class sync_point;
};

namespace gb_component
{
    class entity;
    class type_base;

    //------------------------------------------------------------------------------
    // Description:
    //          Constant data is sharable across different components. This data should not change
    //          while the application is running. It is defined as constant data.
    //------------------------------------------------------------------------------
    class const_data : public x_ll_share_object, public xproperty   
    { 
        x_object_type( const_data, is_linear, rtti_restart, is_not_copyable, is_not_movable )

    public:

        using guid = xguid<const_data>;
             
    public: 

        constexpr                   const_data      ( const guid Guid )                 noexcept            : m_Guid{ Guid } {}
        x_inline        guid&       getMutableGuid  ( void )                            noexcept            { return m_Guid; }
        x_forceconst    guid        getGuid         ( void )                    const   noexcept            { return m_Guid; }
        virtual         bool        onPropQuery     ( xproperty_query& Query )          noexcept override   { return false;  }
        virtual         void        onPropEnum      ( xproperty_enum&  Enum  )  const   noexcept override   { }

    protected: 

        guid                    m_Guid;
    };

    //------------------------------------------------------------------------------
    // Description:
    //      This is the base class for all the game components.
    //      The class is a quantum object so all relevant rules apply
    //      A component has two main states. In the world where the game will try to update it.
    //      Out from the world where you can do things like build entities etc, because they are
    //      thought to be in a linear world.
    //      Every component class needs to have an accompany gb_component_type_base
    //------------------------------------------------------------------------------
    class base : public x_ll_object_harness< base >, protected xproperty
    {
        x_object_type( base, is_quantum_lock_free, rtti_restart, is_not_copyable, is_not_movable )

    protected:

        struct mutable_data;

    public:

        struct global_interface
        {
            gb_component_mgr::base&     m_Manager;          // This component manager
            type_base&                  m_Type;             // Type for the component
            gb_game_graph::base&        m_GameMgr;          // Global Game Manager
            gb_game_graph::sync_point&  m_SyncPoint;        // Sync where are time is based on
        };

        struct base_construct_info
        {
            entity&                     m_Entity;
            const global_interface&     m_GI;
            u32                         m_Flags;
            xarray<mutable_data*,2>     m_MutableData;
            const_data*                 m_pConstantData;
        };

        using                   t_gi                            = const global_interface;
        using                   t_parent                        = x_ll_object_harness< base >;
        x_constexprvar bool     t_is_component                  = true;
        x_constexprvar bool     t_is_entity                     = false;
        x_constexprvar xuptr    t_component_pool_paged_size     = 512;                              // Override to tell the page pool how many components per each page we should allocate

    public:
                                    base                ( void )                                                 = delete;
        x_forceinline               base                ( const base_construct_info& C )                noexcept :  t_parent       { C.m_GI.m_Manager.getMsgAllocQueue(), C.m_Flags     },
                                                                                                                    m_Entity       { C.m_Entity                                         },
                                                                                                                    m_GInterface   { C.m_GI                                             },
                                                                                                                    m_ConstData    { C.m_pConstantData                                  },
                                                                                                                    m_pMutableData {{ C.m_MutableData[0], C.m_MutableData[1]            }}
                                                                                                                    
                                                                                                                    { x_assert_linear( m_Debug_LQ );          }
        virtual                    ~base                ( void )                                        noexcept { x_assert_linear( m_Debug_LQ ); x_assert_lqnone( m_Debug_LQMsg );     }
        x_orforceconst  t_gi&       getGlobalInterface  ( void )                                const   noexcept { return m_GInterface;                                 }
        x_orforceconst  bool        isInWorld           ( void )                                const   noexcept { return hasFlags( component_flags::IN_THE_WORLD );    }
        x_orforceconst  base*       getNextComponent    ( void )                                const   noexcept { return m_pNextComp;                                  }
        x_orforceconst  bool        isEntity            ( void )                                const   noexcept;
        x_orforceconst  type_base&  getType             ( void )                                const   noexcept { return m_GInterface.m_Type;                          }         
        x_forceinline   base*       getMgrNext          ( void )                                        noexcept { return m_pMgrNext;                                   }
        x_orforceconst  const base* getMgrNext          ( void )                                const   noexcept { return m_pMgrNext;                                   }
        x_forceinline   auto&       getEntity           ( void )                                        noexcept { return m_Entity;                                     }            
        x_forceinline   auto&       getEntity           ( void )                                const   noexcept { return m_Entity;                                     }

    protected:

        struct mutable_data { virtual void onUpdateMutableData( const mutable_data& Src ) noexcept {}    };
        using               t_mutable_base  = mutable_data;

        enum component_flags : u32
        {
            T0                      = t_parent::flags::MASK_T0,                 
            MSG_LOCK                = t_parent::flags::MASK_MSG_LOCK,
            MSG_WAIT_FOR_UPDATE     = t_parent::flags::MASK_MSG_WAIT_FOR_UPDATE,
            IN_THE_WORLD            = t_parent::flags::MASK_USER_1,                 // Tells that this component is in the world
            LAST_ENTRY              = t_parent::flags::MASK_USER_3, 
        };
        
    protected:

        template< typename T_CLASS >
        x_forceinline   const T_CLASS&      getConstData            ( void )                    const   noexcept;

        x_forceinline   bool                hasConstData            ( void )                    const   noexcept { return m_ConstData.isValid(); } 

        template< typename T_CLASS >
        x_inline        const T_CLASS&      getT0Data               ( void )                    const   noexcept;

        template< typename T_CLASS >
        x_forceinline   T_CLASS&            getT1Data               ( void )                    const   noexcept;

    protected:

        virtual         void                onExecute               ( void )                            noexcept            { x_assert_quantum( m_Debug_LQ ); x_assert_linear( m_Debug_LQMsg ); x_assert( isInWorld() ); }
        virtual         void                onNotifyOutWorld        ( void )                            noexcept            { x_assert_quantum( m_Debug_LQ ); x_assert_linear( m_Debug_LQMsg ); }
        virtual         void                onNotifyInWorld         ( void )                            noexcept;
        virtual         void                onActivate              ( const bool bActivate )            noexcept            { x_assert_quantum( m_Debug_LQ ); x_assert_linear( m_Debug_LQMsg ); }
        virtual         bool                onPropQuery             ( xproperty_query& Query )          noexcept override;   
        virtual         void                onPropEnum              ( xproperty_enum&  Enum  )  const   noexcept override; 

    protected:

        entity&                             m_Entity;                                           // Reference to the entity
        const global_interface&             m_GInterface;                                       // How this instance has access to the global state
        base*                               m_pNextComp                 { nullptr };            // Link list of components for the entity
        base*                               m_pMgrNext                  { nullptr };            // link list for the managers to use
        base*                               m_pMgrPrev                  { nullptr };            // link list for the managers to use

    private:

        // This function gets call every time a new message comes in
        x_inline        bool                t_ProcessMessages       ( flags LocalFlags )                noexcept;

        // This function gets call ones per frame to update the component
        virtual         void                qt_onRun                ( void )                            noexcept override final;

    private:

        x_ll_share_ref<const_data>          m_ConstData                 {};                     // This a buffer containing all the read only data pure immutable 
        xarray<mutable_data*, 2>            m_pMutableData              {{ nullptr }};          // This is a double buffer array containing the mutable data

    private:

                                    friend class entity;
                                    friend  t_parent;
        template< typename T_COMP > friend class type_pool;
        template< typename T_COMP > friend class type_entity_pool;
                                    friend class type_base;
                                    friend class gb_component_mgr::base;
    };
};

