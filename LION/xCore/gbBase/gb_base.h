//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#ifndef _GB_BASE_H
#define _GB_BASE_H
#pragma once
#include "x_base.h"

//------------------------------------------------------------------------------
// Description:
//      A log system for the scheduler
//------------------------------------------------------------------------------
#if _BOOL_LOG
    extern log_channel g_ComponentLog;
    #define COMPONENT_LOG(A) LOG_CHANNEL( g_ComponentLog, A )
#else
    #define COMPONENT_LOG(A)
#endif

//------------------------------------------------------------------------------
// Description:
//      Gamebase includes classes
//------------------------------------------------------------------------------
#include "gb_manager.h"
#include "gb_component.h"
#include "gb_component_entity.h"
#include "gb_component_type.h"
#include "gb_blueprint.h"
#include "gb_graph.h"

//------------------------------------------------------------------------------
// Description:
//      inline functions
//------------------------------------------------------------------------------
#include "Implementation/gb_manager_inline.h"
#include "Implementation/gb_component_inline.h"
#include "Implementation/gb_component_entity_inline.h"
#include "Implementation/gb_component_type_inline.h"
#include "Implementation/gb_graph_inline.h"


#endif
