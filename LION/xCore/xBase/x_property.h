//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#define X_PROP_MAINSCOPE( QUERY, SCOPE_NAME, SCOPES, PROPS )      X_VARG_EXPAND( auto& __PropQuery = QUERY; if( __PropQuery.isScope( X_STR_CRCINFO(SCOPE_NAME) ) ) do {                                                    if( Query.isSearchingForScopes() ) { if(0){} SCOPES; } else { if(0){} PROPS; } } while( __PropQuery.NextPropertyOrChangeScope() );  else return false; return __PropQuery.isDone();      )
#define X_PROP_SCOPE_CHILD( CHILD_SCOPE, ... )                    X_VARG_EXPAND( else                       if( CHILD_SCOPE )                                    {  __VA_ARGS__; }                                                                                                                                                                                                               )
#define X_PROP_IS_SCOPE( PROP_NAME, SCOPES, PROPS )               X_VARG_EXPAND( else                       if( __PropQuery.isScope( X_STR_CRCINFO(PROP_NAME)  ) ) do {                                                    if( Query.isSearchingForScopes() ) { if(0){} SCOPES; } else { if(0){} PROPS; } } while( __PropQuery.NextPropertyOrChangeScope() );                                    )
#define X_PROP_IS_ARRAY( PROP_NAME, INDEX_VAR, SCOPES, PROPS )    X_VARG_EXPAND( else                       if( __PropQuery.isScope( X_STR_CRCINFO(PROP_NAME)  ) ) do { const int INDEX_VAR = __PropQuery.getArrayIndex(); if( Query.isSearchingForScopes() ) { if(0){} SCOPES; } else { if(0){} PROPS; } } while( __PropQuery.NextPropertyOrChangeScope() );                                    )
#define X_PROP_IS_PROP( PROP_NAME, ... )                          X_VARG_EXPAND( else                       if( __PropQuery.isVar  ( X_STR_CRCINFO(PROP_NAME)  ) )    {  __VA_ARGS__; }                                                                                                                                                                                                          )

//--------------------------------------------------------------------------
namespace x_property_details { template< typename, char > class xprop_general_atomic; };

#define xproperty_friends                                               \
    friend class xprop_s32;                                             \
    friend class xprop_string;                                          \
    friend class xprop_f32;                                             \
    friend class xprop_guid;                                            \
    friend class xprop_vector3;                                         \
    friend class xproperty_query;                                       \
    friend class xproperty_enum;                                        \
    friend class xproperty_data;


//--------------------------------------------------------------------------

class  xproperty_query;
class  xproperty_enum;
class  xproperty_query;
class  xproperty_data;
 
 //--------------------------------------------------------------------------

class xproperty_data
{
    x_object_type( xproperty_data, is_linear );

public:

    X_DEFBITS( flags,
               u16,
               0x0000,
               X_DEFBITS_ARG ( bool,      READ_ONLY,           1 ),    
               X_DEFBITS_ARG ( bool,      FORCE_SAVE,          1 ),                                         
               X_DEFBITS_ARG ( bool,      DONT_SAVE,           1 ),                                         
               X_DEFBITS_ARG ( bool,      REFRESH_ONCHANGE,    1 ),                                         
               X_DEFBITS_ARG ( bool,      NOT_VISIBLE,         1 )                                         
    );

    // Note that this list bust be alphabetically sorted 
    enum class type
    {
        INVALID,
        ANGLES3,
        BOOL,
        BUTTON,
        ENUM,
        FLOAT,
        GUID,
        INT,
        RGBA,
        STRING,
        VECTOR2,
        VECTOR3,
        ENUM_COUNT
    };

    struct data_s32
    {
        s32         m_Value {};
        s32         m_Min   {};
        s32         m_Max   {};
    };

    struct data_guid
    {
        u64         m_Value {};
        const char* m_pType {};
    };

    struct data_v3
    {
        xvector3    m_Value;
        xvector3    m_Min;
        xvector3    m_Max;
    };

    struct data_v2
    {
        xvector2    m_Value;
        xvector2    m_Min;
        xvector2    m_Max;
    };

    struct data_f32
    {
        f32         m_Value {};
        f32         m_Min   {};
        f32         m_Max   {};
    };

    struct data_r3
    {
        xradian3    m_Value;
        xradian3    m_Min;
        xradian3    m_Max;
    };

    struct data_enum
    {
        struct entry
        {
            enum special_values
            {
                 END_DISPLAY = -1,
                 END_ENUM    = -2
            };

            int             m_Value;
            const xstring   m_Name;
        };

        const entry*    m_pEnumList {};
        xstring         m_String    {};
    };

public:

    xproperty_data( void ) {}
    xproperty_data( const xproperty_data& A ) { (*this) = A; }
    xproperty_data& operator = ( const xproperty_data& A ); 

    ~xproperty_data( void )
    {
        // exit safely
        setup( type::INVALID );
    }

    x_inline                bool                        operator ==     ( const xproperty_data& B ) const;
    x_forceconst            type                        getType         ( void ) const          { return m_Type; }
    x_forceinline           bool&                       getBool         ( void )                { return x_assert( m_Type == type::BOOL     ), m_Data.m_Bool;       }
    x_forceinline           xstring&                    getButton       ( void )                { return x_assert( m_Type == type::BUTTON   ), m_Data.m_Button;     }
    x_forceinline           xcolor&                     getColor        ( void )                { return x_assert( m_Type == type::RGBA    ), m_Data.m_Color;      }
    x_forceinline           data_enum&                  getEnum         ( void )                { return x_assert( m_Type == type::ENUM     ), m_Data.m_Enum;       }
    x_forceinline           data_s32&                   getS32          ( void )                { return x_assert( m_Type == type::INT      ), m_Data.m_S32;        }
    x_forceinline           data_f32&                   getF32          ( void )                { return x_assert( m_Type == type::FLOAT    ), m_Data.m_F32;        }
    x_forceinline           data_guid&                  getGUID         ( void )                { return x_assert( m_Type == type::GUID     ), m_Data.m_Guid;       }
    x_forceinline           data_v3&                    getV3           ( void )                { return x_assert( m_Type == type::VECTOR3  ), m_Data.m_Vector3;    }
    x_forceinline           data_v2&                    getV2           ( void )                { return x_assert( m_Type == type::VECTOR2  ), m_Data.m_Vector2;    }
    x_forceinline           data_r3&                    getR3           ( void )                { return x_assert( m_Type == type::ANGLES3  ), m_Data.m_Radian3;    }
    x_forceinline           xstring&                    getString       ( void )                { return x_assert( m_Type == type::STRING   ), m_Data.m_String;     }
    x_forceconst            const bool                  getBool         ( void ) const          { return x_assert( m_Type == type::BOOL     ), m_Data.m_Bool;       }
    x_forceconst            const xstring&              getButton       ( void ) const          { return x_assert( m_Type == type::BUTTON   ), m_Data.m_Button;     }
    x_forceconst            const xcolor                getColor        ( void ) const          { return x_assert( m_Type == type::RGBA    ), m_Data.m_Color;      }
    x_forceconst            const data_enum&            getEnum         ( void ) const          { return x_assert( m_Type == type::ENUM     ), m_Data.m_Enum;       }
    x_forceconst            const data_s32&             getS32          ( void ) const          { return x_assert( m_Type == type::INT      ), m_Data.m_S32;        }
    x_forceconst            const data_f32&             getF32          ( void ) const          { return x_assert( m_Type == type::FLOAT    ), m_Data.m_F32;        }
    x_forceconst            const data_guid&            getGUID         ( void ) const          { return x_assert( m_Type == type::GUID     ), m_Data.m_Guid;       }
    x_forceconst            const data_v3&              getV3           ( void ) const          { return x_assert( m_Type == type::VECTOR3  ), m_Data.m_Vector3;    }
    x_forceconst            const data_v2&              getV2           ( void ) const          { return x_assert( m_Type == type::VECTOR2  ), m_Data.m_Vector2;    }
    x_forceinline           const data_r3&              getR3           ( void ) const          { return x_assert( m_Type == type::ANGLES3  ), m_Data.m_Radian3;    }
    x_forceinline           const xstring&              getString       ( void ) const          { return x_assert( m_Type == type::STRING   ), m_Data.m_String;     }
    x_inline        static  void                        RegisterTypes   ( xtextfile& DataFile ); 
    x_inline                void                        SerializeOut    ( xtextfile& DataFile ) const;
    x_inline                void                        SerializeIn     ( xtextfile& DataFile );
    x_inline                t_self&                     setup           ( type Type );
            
public:

    xstring             m_Name  {};             // Name of the property
    flags               m_Flags {};

protected:

    union data_raw
    {
        constexpr           data_raw  ( void )          {}
                           ~data_raw  ( void )          {}

        data_s32            m_S32                       {};
        bool                m_Bool;
        xcolor              m_Color;
        data_f32            m_F32;
        float               m_Float;
        data_guid           m_Guid;
        data_v3             m_Vector3;
        data_v2             m_Vector2;
        xstring             m_String;
        xwstring            m_WString;
        data_enum           m_Enum;
        xstring             m_Button;
        data_r3             m_Radian3;
    };

    struct type_info
    {
        type            m_EnumType;
        const char*     m_pStringType;
        const char*     m_pTextFileType;
    };

protected:

    const char* getWriteTypeString( void ) const
    {
        return &getTable()[m_Type].m_pStringType[x_constStrLength("Value:")];
    }

    const char* getReadTypeString( void ) const
    {
        return getTable()[m_Type].m_pStringType;
    }
    
    x_forceinline static const xarray< type_info, static_cast<xuptr>(type::ENUM_COUNT) >& getTable( void )
    {
        static const xarray< type_info, static_cast<xuptr>(type::ENUM_COUNT) > s_Table =
        {{
            { type::INVALID,    "",                 ""                     },
            { type::ANGLES3,    "Value:.ANGLES3",   "fff"                  },
            { type::BOOL,       "Value:.BOOL",      "d"                    },
            { type::BUTTON,     "Value:.BUTTON",    "s"                    },
            { type::ENUM,       "Value:.ENUM",      "e"                    },
            { type::FLOAT,      "Value:.FLOAT",     "f"                    },
            { type::GUID,       "Value:.GUID",      "g"                    },
            { type::INT,        "Value:.INT",       "d"                    },
            { type::RGBA,       "Value:.RGBA",      "cccc"                 },
            { type::STRING,     "Value:.STRING",    "s"                    },
            { type::VECTOR2,    "Value:.VECTOR2",   "ff"                   },
            { type::VECTOR3,    "Value:.VECTOR3",   "fff"                  }
        }};
        return s_Table;
    }

protected:

    type                m_Type  { type::INVALID };             // type of property
    data_raw            m_Data  {};


    // VS2016 is crashing
    /*
    static_assert( s_Table[static_cast<int>(type::FLOAT)].m_EnumType      == type::FLOAT, "" );
    static_assert( s_Table[static_cast<int>(type::INT)].m_EnumType        == type::INT, "" );
    static_assert( s_Table[static_cast<int>(type::STRING)].m_EnumType     == type::STRING, "" );
    static_assert( s_Table[static_cast<int>(type::GUID)].m_EnumType       == type::GUID, "" );
    static_assert( s_Table[static_cast<int>(type::VECTOR3)].m_EnumType    == type::VECTOR3, "" );
    */

};

//--------------------------------------------------------------------------

struct xproperty_data_collection
{
    // Creates a new collection with the result of filtering out all the properties from a FilterList
    void                        appendSubtractFilterByNameAndValue                  ( const xbuffer_view<xproperty_data> A, const xbuffer_view<xproperty_data> B );
    void                        appendUnionFilterByName                             ( const xbuffer_view<xproperty_data> A, const xbuffer_view<xproperty_data> B );
                                operator const xbuffer_view<const xproperty_data>   ( void )                                                                        const   { return { &m_List[0], m_List.getCount() }; }
                                operator xbuffer_view<xproperty_data>               ( void )                                                                                { return { &m_List[0], m_List.getCount() }; }
    
    xvector<xproperty_data> m_List {};
};

//--------------------------------------------------------------------------

class xproperty_enum
{
public:

    using callback = xfunction<void( xproperty_enum& Enum, xproperty_data& Data )>;
    using flags    = xproperty_data::flags;

    enum class mode
    {
        SAVE,
        REALTIME 
    };

    class scope_array;
    class scope 
    {
    public:
        
                                        scope                       ( void ) = delete;
        constexpr                       scope                       ( xproperty_enum& E ) : m_Enum{ E } {}
   
        x_forceinline                   operator xproperty_enum&    ( void ) { return m_Enum; }
        
        template< typename T >
        x_forceinline   void            AddProperty                 ( xstring::const_char* pPropertyName, flags Flags = 0 );
        
        x_inline        void            beginScope                  ( xstring::const_char* pScopeName, xfunction<void(scope& Scope)> CallBack );
        x_inline        void            beginScopeArray             ( xstring::const_char* pScopeName, int Count, xfunction<void(scope_array& ScopeArray)> CallBack );

    protected:

        x_incppfile     void            AddProperty                 ( xstring::const_char* pPropertyName, xproperty_data::type Symbol, flags Flags = 0 );

    protected:
        
        xproperty_enum& m_Enum;
    };

    class scope_array 
    {
    public:
                                        scope_array                 ( void ) = delete;
        constexpr                       scope_array                 ( xproperty_enum& E ) : m_Enum{ E } {}
        x_inline        void            beginScopeArrayEntry        ( int Index, xfunction<void(scope& Scope)> CallBack );

    protected:
        
        xproperty_enum& m_Enum;
    };

public:

                                    xproperty_enum                  ( void ) = delete;
                                    xproperty_enum                  ( callback CallBack, mode Mode = mode::REALTIME ) : m_EnumMode{ Mode }, m_Lambda{ CallBack } {}
    x_forceinline   void            setForceFullPaths               ( bool bForceThem )                                                                 { m_bForceFullPaths = bForceThem; }
    x_forceinline   void            beginScope                      ( xstring::const_char* pScopeName, xfunction<void(scope& Scope)> CallBack )        { scope{ *this }.beginScope( pScopeName, CallBack ); }
    x_forceinline   void            beginScopeArray                 ( xstring::const_char* pScopeName, int Count, xfunction<void(scope_array& Scope)> CallBack )  { scope{ *this }.beginScopeArray( pScopeName, Count, CallBack ); }

protected:

    struct scope_entry
    {
        xstring::const_char*    m_pName      {nullptr};
        int                     m_ArrayIndex {-1};    //
        int                     m_ArrayCount {-1};
        int                     m_nProp      {0};
        int                     m_Sequence   {0};
    };

protected:

    mode                        m_EnumMode;
    xarray<scope_entry, 16>     m_Scope                     {};
    int                         m_iScope                    {-1};
    int                         m_Sequence                  {0};
    callback                    m_Lambda;
    xarray<xproperty_data,2>    m_PropertyData              {};
    int                         m_nProccessedProperties     {-1};
    bool                        m_bForceFullPaths           { false };
    int                         m_iLastPropertyScope        {-1};
    int                         m_LastPropertySequence      {-1};

protected:

    xproperty_friends;
};


//-----------------------------------------------------------------------------

class xproperty
{
    x_object_type( xproperty, is_linear, rtti_start );

public:

    x_inline        void            PropEnum        ( xfunction<void( xproperty_enum&  Enum,  xproperty_data& Data)> Function, bool bFullPath = true, xproperty_enum::mode Mode = xproperty_enum::mode::REALTIME ) const;
    x_inline        bool            PropQueryGet    ( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function ) const;
    x_inline        bool            PropQuerySet    ( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function );
    x_inline        void            PropToCollection( xproperty_data_collection& Collection, bool bFullPath = true ) const;
                    void            Save            ( xtextfile& DataFile  ) ;
                    bool            Load            ( xtextfile& DataFile  ) ;
                    void            Save            ( const xwstring& FileName, xtextfile::access_type AccessType = {0} );
                    bool            Load            ( const xwstring& FileName );

protected:

    x_forceinline   bool            onPropQuery     ( xproperty_query& Query ) const    noexcept { return const_cast<xproperty*>(this)->onPropQuery( Query ); }
    virtual         bool            onPropQuery     ( xproperty_query& Query )          noexcept = 0;
    virtual         void            onPropEnum      ( xproperty_enum&  Enum  ) const    noexcept = 0;

protected:

    xproperty_friends

//            const xproperty_type*   getPropertyType ( const char* pPropertyName );
           
};

//--------------------------------------------------------------------------

class xproperty_query
{
public:

    using callback =  xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )>;

public: // --- To be used by the person who is doing the query ---

//    constexpr bool isRealTime       ( void ) const { return m_Flags.m_IS_REAL_TIME; }
    
//    constexpr                   xproperty_query                     ( void )                                                = default;
    constexpr       bool        isDone                              ( void )                                    const       { return m_Flags.m_STATE == state::DONE;                                                                    }           
    x_forceinline   bool        RunGet                              ( const xproperty& Property, callback CallBack )        { Reset(); m_Flags.m_IS_GET = false; m_NextPropertyLambda = CallBack; NextPropertyOrChangeScope(); return Property.onPropQuery( *this ); }
    x_forceinline   bool        RunSet                              ( xproperty& Property, callback CallBack )              { Reset(); m_Flags.m_IS_GET = true;  m_NextPropertyLambda = CallBack; NextPropertyOrChangeScope(); return Property.onPropQuery( *this ); }

public: // --- To be used by the lambda function ----

    constexpr       auto&       getResults                          ( void )                                    const       { return m_PropertyData[1-(m_nProccessedProperties&1)];                                                     }
    constexpr       int         getResultsIndex                     ( void )                                    const       { return m_nProccessedProperties - 1;                                                                       }
    constexpr       int         hasResults                          ( void )                                    const       { return x_assert(false==isReciving()), m_nProccessedProperties > 0;                                        }

public: // ---- To be used inside the property class ----

    constexpr       bool        isSearchingForScopes                ( void )                                    const       { return m_Flags.m_STATE == state::SEARCHING && m_Flags.m_IS_SEARCH_SCOPES;                                 }
    constexpr       int         getArrayIndex                       ( int MaxRange = 10000 )                    const       { return x_assert( m_CurPropArrayIndex < MaxRange ), m_CurPropArrayIndex;                                   }
    constexpr       int         getPropertyIndex                    ( void )                                    const       { return m_nProccessedProperties;                                                                           }
    x_incppfile     bool        NextPropertyOrChangeScope           ( void );

    template< typename T_CHAR, u32 T_CRC, int T_LENGTH > 
    x_inline        bool        isVar                               ( xstring_crcinfo<T_CHAR,T_CRC,T_LENGTH> Node );

    template< typename T_CHAR, u32 T_CRC, int T_LENGTH > 
    x_inline        bool        isScope                             ( xstring_crcinfo<T_CHAR, T_CRC, T_LENGTH> Node );
    constexpr       bool        isReciving                          ( void )                                    const       { return m_Flags.m_IS_GET; }
    constexpr       bool        isSending                           ( void )                                    const       { return m_Flags.m_IS_GET == false; }

    template< typename T >  
    constexpr       const xproperty_data&       getProp             ( void ) const { return x_assert( T::t_symbol == getProperty().getType() ), x_assert( isReciving() ), getProperty(); }

    template< typename T > 
    x_forceinline   xproperty_data&             setProp             ( void ); 

protected:
    
    enum class state : u16
    {
        SEARCHING,
        POPING_SCOPES,
        DONE,
        ENUM_COUNT
    };

    X_DEFBITS( flags,
               u16,
               0x0000,
               X_DEFBITS_ARG ( state,       STATE,                x_Log2IntRoundUp( int(state::ENUM_COUNT) - 1) ),    
               X_DEFBITS_ARG ( u16,         IS_GET,               1 ),                                         
               X_DEFBITS_ARG ( u16,         IS_REAL_TIME,         1 ),                                        
               X_DEFBITS_ARG ( u16,         IS_SEARCH_SCOPES,     1 )                                         
    );

    struct scope
    {
        u32         m_CRC;              // current scope CRC
        int         m_ArrayIndex;       // If the scope is an array then this will be the array index
        bool        m_isArray;          // tells the system if this entry is an array
        const char* m_DEBUG_pScopeName; // current scope Name
        int         m_DEBUG_Length;     // Length of the scope name
    };

protected:

    x_forceinline   xproperty_data&             getProperty         ( void )       { return m_PropertyData[m_nProccessedProperties&1]; }
    constexpr       const xproperty_data&       getProperty         ( void ) const { return m_PropertyData[m_nProccessedProperties&1]; }
                    void                        PrepareNextScope    ( void );
    x_inline        void                        Reset               ( void );

protected:

    callback                    m_NextPropertyLambda            {};
    xarray<xproperty_data,2>    m_PropertyData                  {{ xproperty_data{}, xproperty_data{} }};
    u32                         m_CurPropNextCRC                {};
    int                         m_CurPropStringIndex            {};
    int                         m_CurPropNextStringIndex        {};
    int                         m_CurPropArrayIndex             {};
    int                         m_nScopesToPop                  {};
    bool                        m_bPropFound                    {};
    flags                       m_Flags                         {};
    int                         m_nProccessedProperties         {-1};
    xarray<scope,32>            m_ScopeData                     {};
    int                         m_iScope                        {0};

protected:

    xproperty_friends;
};

