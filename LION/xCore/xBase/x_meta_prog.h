//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//------------------------------------------------------------------------------
// Description:
//      Static casting with safe ranges 
//------------------------------------------------------------------------------
template< typename T_TO, typename T_FROM > x_forceconst 
T_TO x_static_cast( const T_FROM& a ) noexcept
{
    CMD_DEBUG( using max_type = typename x_size_to_unsigned< int(sizeof(T_TO) > sizeof(T_FROM) ? sizeof(T_TO) : sizeof(T_FROM)) >::type );
    CMD_DEBUG( using min_type = typename x_size_to_signed  < int(sizeof(T_TO) > sizeof(T_FROM) ? sizeof(T_TO) : sizeof(T_FROM)) >::type );
    return x_assert( static_cast<min_type>(a) >= static_cast<min_type>(std::numeric_limits<T_TO>::lowest()) && 
                     static_cast<max_type>(a) <= static_cast<max_type>(std::numeric_limits<x_size_to_unsigned<sizeof(T_TO)>::type>::max()) ), 
           static_cast<T_TO>(a);
}

namespace k_x_debug
{
    template <typename T, T k >
    struct print_constexpr_value;
}

#define kx_print_constexpr_value(A) static_assert( k_x_debug::print_constexpr_value<decltype(A),A>::value, "" );

template< typename T_TO, typename T_FROM > x_forceconst
T_TO x_static_cast( const T_FROM&& a ) noexcept
{
    CMD_DEBUG( using max_type = typename x_size_to_unsigned< int(sizeof(T_TO) > sizeof(T_FROM) ? sizeof(T_TO) : sizeof(T_FROM)) >::type );
    CMD_DEBUG( using min_type = typename x_size_to_signed  < int(sizeof(T_TO) > sizeof(T_FROM) ? sizeof(T_TO) : sizeof(T_FROM)) >::type );
    return x_assert( static_cast<min_type>(a) >= static_cast<min_type>(std::numeric_limits<T_TO>::lowest()) && 
                     static_cast<max_type>(a) <= static_cast<max_type>(std::numeric_limits<x_size_to_unsigned<sizeof(T_TO)>::type>::max()) ), 
           static_cast<T_TO>(a);
}

//------------------------------------------------------------------------------
// Description:
//      Shortcut for check if a type has virtual functions 
//------------------------------------------------------------------------------
 #define x_hasvirtual(A)    std::is_polymorphic<A>::value

//------------------------------------------------------------------------------
// Description:
//      Converts an rvalue to an lvalue 
//------------------------------------------------------------------------------
template <typename T>
constexpr T &x_lvalue(T &&r) noexcept { return r; }

//------------------------------------------------------------------------------
// Description:
//      Simple move without magic 
//------------------------------------------------------------------------------
template <typename T>
typename std::remove_reference<T>::type&& x_move( T&& arg )
{
    return static_cast<typename std::remove_reference<T>::type&&>(arg);
}

//------------------------------------------------------------------------------
// Description:
//      return how many elements there is in an array
//------------------------------------------------------------------------------
namespace _x_type
{
    template< typename T_TYPE > constexpr
    xuptr countof( void ) noexcept
    {
        using T = typename std::remove_reference<T_TYPE>::type;
        static_assert( std::is_array<T>::value,   "countof() requires an array argument"  );
        static_assert( std::extent<T>::value > 0, "zero- or unknown-size array"           );
        return std::extent<T>::value;
    }
};

#define x_countof(a) _x_type::countof< decltype(a) >( )

//------------------------------------------------------------------------------
// Description:
//      useful const related functions
//------------------------------------------------------------------------------

template<typename T> 
struct x_remove_const
{
    using type = T;
};

template<typename T> 
struct x_remove_const< const T >
{
    using type = T;
};

 #define x_as_const(A)      std::add_const<decltype(A)>(A)
 #define x_as_no_const(A)   const_cast< std::remove_const< decltype(A) >::type >(A)
 #define x_is_const(A)      std::is_same< A, const A >::value

//------------------------------------------------------------------------------
// Description:
//      empty class, used in default templatize classes
//------------------------------------------------------------------------------
class x_empty_class {};

//------------------------------------------------------------------------------
// Description:
//      Getting the address of a temporary variable (rval)
//------------------------------------------------------------------------------
template<typename T> constexpr 
T* x_getAddressOfTemporary(T&& v) { return &v; }

//------------------------------------------------------------------------------
// Description:
//      CRC32 Table
//------------------------------------------------------------------------------
namespace x_meta_prog_details
{
    namespace crc32
    {
        // CRC32 Table (zlib polynomial)
        x_constexprvar u32 s_CRCTable[256] = {
            0x00000000u, 0x77073096u, 0xee0e612cu, 0x990951bau, 0x076dc419u, 0x706af48fu,
            0xe963a535u, 0x9e6495a3u, 0x0edb8832u, 0x79dcb8a4u, 0xe0d5e91eu, 0x97d2d988u,
            0x09b64c2bu, 0x7eb17cbdu, 0xe7b82d07u, 0x90bf1d91u, 0x1db71064u, 0x6ab020f2u,
            0xf3b97148u, 0x84be41deu, 0x1adad47du, 0x6ddde4ebu, 0xf4d4b551u, 0x83d385c7u,
            0x136c9856u, 0x646ba8c0u, 0xfd62f97au, 0x8a65c9ecu,	0x14015c4fu, 0x63066cd9u,
            0xfa0f3d63u, 0x8d080df5u, 0x3b6e20c8u, 0x4c69105eu, 0xd56041e4u, 0xa2677172u,
            0x3c03e4d1u, 0x4b04d447u, 0xd20d85fdu, 0xa50ab56bu,	0x35b5a8fau, 0x42b2986cu,
            0xdbbbc9d6u, 0xacbcf940u, 0x32d86ce3u, 0x45df5c75u, 0xdcd60dcfu, 0xabd13d59u,
            0x26d930acu, 0x51de003au, 0xc8d75180u, 0xbfd06116u, 0x21b4f4b5u, 0x56b3c423u,
            0xcfba9599u, 0xb8bda50fu, 0x2802b89eu, 0x5f058808u, 0xc60cd9b2u, 0xb10be924u,
            0x2f6f7c87u, 0x58684c11u, 0xc1611dabu, 0xb6662d3du,	0x76dc4190u, 0x01db7106u,
            0x98d220bcu, 0xefd5102au, 0x71b18589u, 0x06b6b51fu, 0x9fbfe4a5u, 0xe8b8d433u,
            0x7807c9a2u, 0x0f00f934u, 0x9609a88eu, 0xe10e9818u, 0x7f6a0dbbu, 0x086d3d2du,
            0x91646c97u, 0xe6635c01u, 0x6b6b51f4u, 0x1c6c6162u, 0x856530d8u, 0xf262004eu,
            0x6c0695edu, 0x1b01a57bu, 0x8208f4c1u, 0xf50fc457u, 0x65b0d9c6u, 0x12b7e950u,
            0x8bbeb8eau, 0xfcb9887cu, 0x62dd1ddfu, 0x15da2d49u, 0x8cd37cf3u, 0xfbd44c65u,
            0x4db26158u, 0x3ab551ceu, 0xa3bc0074u, 0xd4bb30e2u, 0x4adfa541u, 0x3dd895d7u,
            0xa4d1c46du, 0xd3d6f4fbu, 0x4369e96au, 0x346ed9fcu, 0xad678846u, 0xda60b8d0u,
            0x44042d73u, 0x33031de5u, 0xaa0a4c5fu, 0xdd0d7cc9u, 0x5005713cu, 0x270241aau,
            0xbe0b1010u, 0xc90c2086u, 0x5768b525u, 0x206f85b3u, 0xb966d409u, 0xce61e49fu,
            0x5edef90eu, 0x29d9c998u, 0xb0d09822u, 0xc7d7a8b4u, 0x59b33d17u, 0x2eb40d81u,
            0xb7bd5c3bu, 0xc0ba6cadu, 0xedb88320u, 0x9abfb3b6u, 0x03b6e20cu, 0x74b1d29au,
            0xead54739u, 0x9dd277afu, 0x04db2615u, 0x73dc1683u, 0xe3630b12u, 0x94643b84u,
            0x0d6d6a3eu, 0x7a6a5aa8u, 0xe40ecf0bu, 0x9309ff9du, 0x0a00ae27u, 0x7d079eb1u,
            0xf00f9344u, 0x8708a3d2u, 0x1e01f268u, 0x6906c2feu, 0xf762575du, 0x806567cbu,
            0x196c3671u, 0x6e6b06e7u, 0xfed41b76u, 0x89d32be0u, 0x10da7a5au, 0x67dd4accu,
            0xf9b9df6fu, 0x8ebeeff9u, 0x17b7be43u, 0x60b08ed5u, 0xd6d6a3e8u, 0xa1d1937eu,
            0x38d8c2c4u, 0x4fdff252u, 0xd1bb67f1u, 0xa6bc5767u, 0x3fb506ddu, 0x48b2364bu,
            0xd80d2bdau, 0xaf0a1b4cu, 0x36034af6u, 0x41047a60u, 0xdf60efc3u, 0xa867df55u,
            0x316e8eefu, 0x4669be79u, 0xcb61b38cu, 0xbc66831au, 0x256fd2a0u, 0x5268e236u,
            0xcc0c7795u, 0xbb0b4703u, 0x220216b9u, 0x5505262fu, 0xc5ba3bbeu, 0xb2bd0b28u,
            0x2bb45a92u, 0x5cb36a04u, 0xc2d7ffa7u, 0xb5d0cf31u, 0x2cd99e8bu, 0x5bdeae1du,
            0x9b64c2b0u, 0xec63f226u, 0x756aa39cu, 0x026d930au, 0x9c0906a9u, 0xeb0e363fu,
            0x72076785u, 0x05005713u, 0x95bf4a82u, 0xe2b87a14u, 0x7bb12baeu, 0x0cb61b38u,
            0x92d28e9bu, 0xe5d5be0du, 0x7cdcefb7u, 0x0bdbdf21u, 0x86d3d2d4u, 0xf1d4e242u,
            0x68ddb3f8u, 0x1fda836eu, 0x81be16cdu, 0xf6b9265bu, 0x6fb077e1u, 0x18b74777u,
            0x88085ae6u, 0xff0f6a70u, 0x66063bcau, 0x11010b5cu, 0x8f659effu, 0xf862ae69u,
            0x616bffd3u, 0x166ccf45u, 0xa00ae278u, 0xd70dd2eeu, 0x4e048354u, 0x3903b3c2u,
            0xa7672661u, 0xd06016f7u, 0x4969474du, 0x3e6e77dbu, 0xaed16a4au, 0xd9d65adcu,
            0x40df0b66u, 0x37d83bf0u, 0xa9bcae53u, 0xdebb9ec5u, 0x47b2cf7fu, 0x30b5ffe9u,
            0xbdbdf21cu, 0xcabac28au, 0x53b39330u, 0x24b4a3a6u, 0xbad03605u, 0xcdd70693u,
            0x54de5729u, 0x23d967bfu, 0xb3667a2eu, 0xc4614ab8u, 0x5d681b02u, 0x2a6f2b94u,
            0xb40bbe37u, 0xc30c8ea1u, 0x5a05df1bu, 0x2d02ef8du
        };
    }
};

//------------------------------------------------------------------------------
// Description:
//      Static string crc computation 
//------------------------------------------------------------------------------
namespace x_meta_prog_details
{
    constexpr
    u32 constStrCRC32( const char* data, const int length, const u32 crc = ~0u )
    {
        return length ? x_meta_prog_details::constStrCRC32( &data[1], length - 1, (crc >> 8) ^ x_meta_prog_details::crc32::s_CRCTable[ (crc & 0xFF) ^ data[0] ] ) : ~crc;
    }

    template< typename T_CHAR >
    constexpr int _x_constStrLength( const T_CHAR* str )
    {
        return (*str == 0) ? 0 : _x_constStrLength(str + 1) + 1;
    }

    constexpr
    u32 _x_constStrCRC32( const char* data, const u32 crc = 0u )
    {
        return constStrCRC32( data, _x_constStrLength( data ), ~crc );
    }
}

//------------------------------------------------------------------------------
// Description:
//      In theory should be able to detect if an expression is constexpr but in reality does it half way...
// Algorithm:
//     Stackoverflow
//      http://stackoverflow.com/questions/13299394/is-is-constexpr-possible-in-c11
//------------------------------------------------------------------------------
template<typename T> constexpr typename std::remove_reference<T>::type __x_makeprval(T && t) { return t; }
#define x_isConstexpr(e) noexcept(__x_makeprval( e ))

//------------------------------------------------------------------------------
// Description:
//      compile-time int to string
// Algorithm:
//      http://accu.org/index.php/articles/2137
//------------------------------------------------------------------------------
namespace _x_meta_prog
{
    template< size_t I > 
    struct itos : itos< I / 10 > 
    {
        const char m_MyChar;

        constexpr itos( ) : m_MyChar( 0x30 + I % 10 ) 
        {
            static_assert( sizeof( itos ) == sizeof( itos< I / 10 > ) + 1, "unwanted padding" );
        }

        struct out 
        {
            const itos< I >     m_MyFinalChars; 
            const char                        m_Nil;
            constexpr out( ) : m_Nil( '\0' ) 
            {
                static_assert( sizeof( out ) == sizeof( m_MyFinalChars ) + 1, "unwanted padding" );
            }
            constexpr operator const char* const( ) const { return m_MyFinalChars; }
        };
    };

    #define X_DIGITS X(0) X(1) X(2) X(3) X(4) X(5) X(6) X(7) X(8) X(9)
    #define X( I )                                                  \
    template< > struct itos< I > {                                  \
        const char m_MyChar;                                        \
        constexpr itos( ) : m_MyChar( 0x3##I ) { }                  \
        constexpr static const char* const out( ) {                 \
            return #I;                                              \
        }                                                           \
        constexpr operator const char* const( ) const {             \
            return &m_MyChar;                                       \
        }                                                           \
    };                                                              \
    X_DIGITS
    #undef X
    #undef X_DIGITS
}

//------------------------------------------------------------------------------
// Description:
//      compile_time string
// Algorithm:
//      http://stackoverflow.com/questions/15858141/conveniently-declaring-compile-time-strings-in-c/15863826#15863826
// Examples:
//      constexpr auto a = x_const_string("abc");
//      constexpr auto b = a + x_const_string("def"); 
//      constexpr auto c = x_const_string('R') + x_const_string("abc");
//------------------------------------------------------------------------------
namespace x_meta_prog_details
{
    template<int>
    using charDummy = char;

    template< int i >
    constexpr auto str_at(const char* a) { return a[i]; }

    template<int... dummy>
    struct const_string
    {
        const char m_Chars[sizeof...(dummy) + 1];

        constexpr const_string  ( const char* a         ) noexcept : m_Chars{ str_at<dummy>(a)..., 0}     {}
        constexpr const_string  ( charDummy<dummy>... a ) noexcept : m_Chars{ a..., 0}                    {}
        constexpr const_string  ( const const_string& a ) noexcept : m_Chars{ a.m_Chars[dummy]..., 0}  {}

        template<int... dummyB>
        constexpr const_string<dummy..., sizeof...(dummy) + dummyB... > operator + ( const_string<dummyB...> b ) const noexcept
        {
            return { m_Chars[dummy]..., b.m_Chars[dummyB]... };
        }

        constexpr operator const char* ( void ) const noexcept { return m_Chars; } 
    };

    template<int I>
    struct get_string
    {
        constexpr static auto g( const char* a ) noexcept -> decltype( get_string<I-1>::g(a) + const_string<0>(a + I) ) 
        {
            return get_string<I-1>::g(a) + const_string<0>(a + I);
        }
    };

    template<>
    struct get_string<0>
    {
        constexpr static const_string<0> g(const char* a) noexcept
        {
            return {a};
        }
    };
}

//------------------------------------------------------------------------------

template<int I>
constexpr auto x_const_string( const char (&a)[I] ) noexcept -> decltype( x_meta_prog_details::get_string<I-2>::g(a) ) 
{
    return x_meta_prog_details::get_string<I-2>::g(a);
}

//------------------------------------------------------------------------------

constexpr auto x_const_string( char Code ) noexcept
{
    return x_const_string<2>( { Code, char(0) } );
}

//------------------------------------------------------------------------------
// Description:
//      Traits to determine whether some type is quantum/thread safe or not 
//      it is meant to use in a place that is in quantum such inside a job/task
//------------------------------------------------------------------------------
template< typename T_TYPE, bool T_IS_POD >
struct x_is_quantum_safe;

template< typename T_TYPE >
struct x_is_quantum_safe< T_TYPE, true >
{
    enum : bool 
    { 
        value = std::is_const< typename std::remove_reference<T_TYPE>::type>::value     || 
                  !( std::is_reference<T_TYPE>::value                         || 
                     std::is_pointer<T_TYPE>::value         
                   )
    };
};

template< typename T_TYPE >
struct x_is_quantum_safe< T_TYPE, false >
{
    enum : bool 
    { 
        value = std::remove_reference<typename std::remove_pointer<T_TYPE>::type >::type::t_is_quantum ||
                x_is_quantum_safe< T_TYPE, true >::value 
    }; 
};

//------------------------------------------------------------------------------
// Description:
//      Finding the traits from a function.
//      https://github.com/kennytm/utils/blob/master/traits.hpp
//      https://functionalcpp.wordpress.com/2013/08/05/function-traits/
// Example:
//      template< typename T_FUNCTION >
//      struct task_continue
//      {
//          using t_self                = task_continue;
//          using t_function_traits     = xfunction_traits<T_FUNCTION>;
//          using t_function_arg        = typename t_function_traits::template t_arg<0>::type;
//          using t_function_return     = typename t_function_traits::t_return_type;
//          using t_function_type       = typename x_make_function< function_return, function_arg >::type
//          using t_lambda_job          = lambda_job< t_function_return, t_function_arg >;
//      };
//------------------------------------------------------------------------------

template< typename T_RET, typename... T_ARGS >
struct x_make_function
{
    using type = T_RET( T_ARGS... );
};

template< typename T_RET >
struct x_make_function< T_RET, void >
{
    using type = T_RET();
};

template< typename T >
struct xfunction_traits : public xfunction_traits< decltype(&T::operator()) > {};

template< int T_I, int T_MAX, typename... T_ARGS  >
struct xfunction_traits_args
{
    using type = typename std::tuple_element< T_I, std::tuple<T_ARGS...> >::type;
};

template<>
struct xfunction_traits_args<0,0>
{
    using type = void;
};

template< typename T_CLASS_TYPE, typename T_RETURN_TYPE, typename... T_ARGS >
struct xfunction_traits< T_RETURN_TYPE(T_CLASS_TYPE::*)(T_ARGS...) const >
{
    using                        t_return_type      = T_RETURN_TYPE;
    enum  : int {                t_arg_count        = sizeof...(T_ARGS) };
    using                        t_class_type       = T_CLASS_TYPE;
    using                        t_self             = xfunction_traits< T_RETURN_TYPE(T_CLASS_TYPE::*)(T_ARGS...) const >;

    template< int T_I > 
    struct t_arg      
    { 
        using type = typename xfunction_traits_args<T_I, t_arg_count,T_ARGS...>::type;
    };
 };


template< typename T_RETURN_TYPE, typename... T_ARGS >
struct xfunction_traits<T_RETURN_TYPE(T_ARGS...)>
{
    using                        t_return_type      = T_RETURN_TYPE;
    enum  : int {                t_arg_count        = sizeof...(T_ARGS) };

    template< int T_I > 
    struct t_arg      
    { 
        using type = typename xfunction_traits_args<T_I ,t_arg_count,T_ARGS...>::type;
    };
 };

//---------------------------------------------------------------------------------------------------------
// Description:
//          The rtti it is used to give Run Time Type Information to any structure/class hierarchy 
//          that you want. The type information main priority is to keep casting always safe. 
//          The most common case of casting is when you have a base object trying to be cast
//          to one of its children. But the RTTI can be use for more than just casting. It also
//          can be use to switch between the different types.
//  Example:
//
//        struct one_a
//        {
//            x_object_type( one_a, rtti_start );
//        };
//
//        struct one_b
//        {
//            x_object_type( one_b, rtti_start );
//        };
//
//        struct two : public one_a
//        {
//            x_object_type( two, rtti( one_a ) );
//        };
//
//        struct c : public one_b, public two
//        {
//            x_object_type( c, rtti( one_b, two ) );
//        };
//
//        void Test( void )
//        {
//            c       C;
//            one_a   Aa;
//            one_b   Ab;
//            two     Two;
//
//            one_a* pA = &Two;
//
//            Aa.SafeCast<one_a>();
//
//            x_assert( Aa.isKindOf<one_a>(),     "" );
//            x_assert( Two.isKindOf<one_a>(),    "" );
//            x_assert( C.isKindOf<two>(),        "" );
//            x_assert( C.isKindOf<one_a>(),      "" );
//
//            x_assert( pA->isKindOf<one_a>(),    "" );
//            x_assert( pA->isKindOf<two>(),      "" );
//
//            two&    TwoCasted = pA->SafeCast<two>();
//            one_a&  OneCasted = pA->SafeCast<one_a>();
//        }
//---------------------------------------------------------------------------------------------------------

     
//---------------------------------------------------------------------------------------------------------

class x_rtti_base
{
public:

//    constexpr  x_rtti_base( void ) = default;
    constexpr  x_rtti_base( const char* pName ) : 
        CMD_DEBUG( m_DEBUG_String{ pName }, ) m_CRC{ x_meta_prog_details::_x_constStrCRC32(pName) } {}
    
    constexpr bool Check( const x_rtti_base& A ) const
    {
        // Must use a CRC to compare and can not use the address of a variable because it needs to work across dynamic libraries
        return m_CRC == A.m_CRC; // &m_CRC == &A.m_CRC || 
    }

    virtual bool isKindofV( const x_rtti_base& A ) const 
    { 
        return Check(A);
    }

    constexpr int getNumberOfParents( void ) const { return 0; }

protected:

    template< typename T >
    struct kindof
    {
        x_forceconst static bool isKindof( const x_rtti_base& A )  { return T::getRTTI().Check( A ); }
    };

protected:

    CMD_DEBUG( const char* m_DEBUG_String ); 
    const u32   m_CRC;
    template< typename T_CLASS, typename ...T_ARGS > friend class x_rtti;
};

//--------------------------------------------------------------------------------------------------------

template< typename T_CLASS, typename ...T_ARGS >
class x_rtti final : public x_rtti_base 
{
public:

    constexpr  x_rtti( void ) = default;
    constexpr  x_rtti( const char* pName ) : x_rtti_base{ pName } {}

    constexpr int getNumberOfParents( void ) const { return t_total_parents; }

    virtual bool isKindofV( const x_rtti_base& A ) const override
    { 
        return Check(A) || kindof< T_CLASS >::isKindof( A );
    }

protected:

    x_constexprvar int t_total_parents{ sizeof...(T_ARGS) };
    x_constexprvar int t_start_index  { t_total_parents - 1};

    template< int T_I >
    struct t_arg      
    { 
        using type = typename xfunction_traits_args< T_I, t_total_parents, T_ARGS... >::type;

        x_forceconst static bool isKindof( const x_rtti_base& Rtti ) 
        {
            return type::getRTTI().Check( Rtti ) || std::remove_reference<decltype(type::getRTTI())>::type::template kindof<type>::isKindof( Rtti );
        }
    };

    template< typename T, int T_COUNT = t_start_index >
    struct kindof
    {
        x_forceconst static bool isKindof( const x_rtti_base& Rtti ) 
        { 
            return t_arg<T_COUNT>::isKindof( Rtti ) ? true : kindof<T,T_COUNT-1>::isKindof( Rtti );
        }           
    };

    template< typename T >
    struct kindof< T, -1 >
    {
        x_forceconst static bool isKindof( const x_rtti_base& Rtti ) 
        { 
            return false ;
        }
    };  
    
protected:

    template< typename , typename ... > friend class x_rtti;         
};
