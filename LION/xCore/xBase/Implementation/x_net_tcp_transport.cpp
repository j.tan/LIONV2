//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "x_base.h"

#if _X_TARGET_WINDOWS

    #include <windows.h>
    #include <winsock2.h>
    #include <ws2tcpip.h>
    //#include <winsock.h>
    #include <process.h>
    #include <math.h>

    using   socklen_t   = int;
    using   ssize_t     = int;

    #pragma comment( lib, "ws2_32.lib" )

#elif _X_TARGET_IOS || _X_TARGET_MAC

    #include <alloca.h>
    #include <netdb.h>
    #include <unistd.h>
    #include <errno.h>
    #include <sys/socket.h>
    #include <sys/ioctl.h>
    #include <sys/fcntl.h>
    #include <netinet/in.h>

    #define SOCKADDR_IN     sockaddr_in
    #define INVALID_SOCKET  (-1)
    #define SOCKET_ERROR    (-1)

    #define WSAEADDRINUSE   EADDRINUSE
    #define WSAECONNRESET   ECONNRESET
    #define WSAEWOULDBLOCK  EWOULDBLOCK

    using   SOCKET      = int             ;
    using   SOCKADDR    = struct sockaddr ;

#endif


//-------------------------------------------------------------------------------
static
s32 GetLastSocketError( void )
{
#if _X_TARGET_WINDOWS
    return WSAGetLastError();
#elif _X_TARGET_IOS || _X_TARGET_MAC
    return errno;
#endif
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// TCP CLIENT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------

xnet_tcp_client::err xnet_tcp_client::openSocket( s32 Port ) noexcept
{
    return m_Socket.openTCPSocket( Port );
}

//-------------------------------------------------------------------------------

xnet_tcp_client::err xnet_tcp_client::ConnectToServer( xnet_address Address ) noexcept
{
    return m_Socket.ConnectTCPServer( Address );
}

//-------------------------------------------------------------------------------

xnet_tcp_client::err xnet_tcp_client::Send( const xbuffer_view<xbyte> Packet ) noexcept
{
    if ( send( m_Socket.getLowLevelSocket(), (char*)&Packet[0], Packet.getCount<int>(), 0) < 0) 
    {
        s32 err = GetLastSocketError();
        X_NET_LOGGER( "TCP socket error while sending data! Error = " << err ); 
        return x_error_code( errors, ERR_FAILURE, "TCP failure" );
    }

    return x_error_ok( errors );
}

//-------------------------------------------------------------------------------

xnet_tcp_client::err xnet_tcp_client::Receive( xbuffer_view<xbyte> Packet, s32& UserSize, bool bBlock ) noexcept
{
    xnet_address Address;
    return m_Socket.ReadFromSocket( Packet, UserSize, Address, bBlock );
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// TCP SERVER
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------

xnet_tcp_server::err xnet_tcp_server::openSocket( s32 Port ) noexcept
{
    // Open the port
    auto Err = m_Socket.openTCPSocket( Port );
    if( Err ) return Err;

    
    //
    // Bind to that port
    //
    struct sockaddr_in sin;

    sin.sin_port = htons(Port);
    sin.sin_addr.s_addr = 0;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_family = AF_INET;

    if(bind(m_Socket.getLowLevelSocket(), (struct sockaddr *)&sin,sizeof(struct sockaddr_in) ) < 0 )
    {
        const auto err = GetLastSocketError();
        X_NET_LOGGER( "TCP bind to socket error! Error " << err ); 
        return x_error_code( errors, ERR_FAILURE, "TCP bind to socket error!" );
    }

    //
    // Start Listening in the port
    //
    if( listen(m_Socket.getLowLevelSocket(), 0) < 0 ) 
    {
        const auto err = GetLastSocketError();
        X_NET_LOGGER( "TCP socket error while trying to listen! Error = " << err ); 
        return x_error_code( errors, ERR_FAILURE,  "TCP socket error while trying to listen!" );;
    }

    return x_error_ok( errors );
}

//-------------------------------------------------------------------------------
xnet_tcp_server::err xnet_tcp_server::AcceptConnections( void ) noexcept
{
    t_client_container::t_handle hClient;

    do  
    {
        xnet_tcp_client& Client = m_lClient.pop( hClient );

        auto Err = m_Socket.AcceptTCPConnection( Client.m_Socket );
        if( Err ) 
        {
            m_lClient.push( hClient );
            return Err;
        }

    } while( true );

    return x_error_ok( errors );
}



