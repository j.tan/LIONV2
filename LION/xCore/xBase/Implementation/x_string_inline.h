//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
auto x_strlen( const T_CHAR* const pStr )
{
    x_assume( pStr );
    const T_CHAR* pEnd = pStr;
    if( pStr ) while( *pEnd++ ) {}
    return units_chars_any<T_CHAR>{ static_cast<int>( pEnd - pStr - 1 ) };
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
int x_strcpy( T_CHAR* const pDest, const units_chars_any<T_CHAR> MaxChar, const T_CHAR* const pSrc )
{
    x_assert( pSrc );
    x_assert( pDest );
    x_assert( MaxChar.m_Value > 0 );
    
    const int Max = MaxChar.m_Value - 1;
    int i;
    for( i=0; (pDest[i] = pSrc[i]) && (i < Max); i++ );
    
#ifdef _X_DEBUG
    for( int j = i; j < MaxChar.m_Value; j++ ) pDest[j] = T_CHAR{'?'};
#endif
    
    pDest[i] = 0;
    
    return i;
}

//------------------------------------------------------------------------------
// This is a memory function... searches for a char backwards direction
template< typename T_CHAR > x_inline
void* x_memchr( const void* pBuf, const T_CHAR C, const units_chars_any<T_CHAR> aCount )
{
    x_assert( pBuf );
    x_assert( aCount.m_Value >= 0 );

          T_CHAR* p = (T_CHAR*)pBuf;
    const T_CHAR  c = C;
    int Count  = aCount.m_Value;

    while( Count && (*p != c) )
    {
        p++;
        Count--;
    }

    return Count ? p : nullptr;
}

//------------------------------------------------------------------------------
// Description:
//      Converts any 32 bit number into a string which contains the number 
//      represented in a particular base. The base for instance could be 16
//      for hex numbers.
// Returns:
//	    Length of the final string
// See Also:
//     x_atod64 x_atod32 x_atoi32 x_atoi64 x_atof32 x_atof64
//------------------------------------------------------------------------------
namespace x_string
{
    template< typename T_CHAR >
    static int _x_dtoa( const u64 aVal, xbuffer_view<T_CHAR> Buff, const int Base, const bool bHasNegative )
    {
        int     iCursor = 0;
        int     i       = 0;

        x_assert( Buff.getCount() > 1 );
        x_assert( Base > 2 );
        x_assert( Base <= (26+26+10) );

        if( bHasNegative )
        {
            Buff[iCursor++] = '-';
            i++;
        }

        u64 Val = aVal;
        do 
        {
            const auto CVal = (u32)(Val % Base);
            Val     /= Base;

            // convert to ascii and store 
            if ( CVal < 10 )
            {
                Buff[iCursor++] = ( CVal + T_CHAR{'0'} );
            }
            else if( CVal < (10+26) )
            {
                Buff[iCursor++] = ( CVal - 10 + T_CHAR{'a'} );
            }
            else if( CVal < (10+26+26) )
            {
                Buff[iCursor++] = ( CVal - 10 - 26 + T_CHAR{'A'} );
            }
            else
            {
                // The base is too big
                x_assert( false );
            }

        } while( Val > 0 && iCursor < Buff.getCount() );

        //  terminate string; 
        Buff[iCursor] = 0;
        const int Length = iCursor; 

        // Reverse string order
        for( iCursor--; iCursor > i ; i++, iCursor-- )
        {
            x_Swap( Buff[iCursor], Buff[i] );
        }

        return Length;
    }
};

//------------------------------------------------------------------------------
// Description:
//      Converts any 32 bit number into a string which contains the number 
//      represented in a particular base. The base for instance could be 16
//      for hex numbers.
// Returns:
//	    Length of the final string
// See Also:
//     x_atod64 x_atod32 x_atoi32 x_atoi64 x_atof32 x_atof64
//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
int x_dtoa( const int Val, xbuffer_view<T_CHAR> Buffer, const int Base )
{
    if( Val < 0 )
    {
        return x_string::_x_dtoa( static_cast<u64>(-Val), Buffer, Base, true );
    }
    else
    {
        return x_string::_x_dtoa( static_cast<u64>(Val), Buffer, Base, false );
    }
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
int x_dtoa( const u32 Val, xbuffer_view<T_CHAR> Buffer, const int Base )
{
    return x_string::_x_dtoa( static_cast<u64>(Val), Buffer, Base, false );
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
int x_dtoa( const s64 Val, xbuffer_view<T_CHAR> Buffer, const int Base )
{
    if( Val < 0 )
    {
        return  x_string::_x_dtoa( static_cast<u64>(-Val), Buffer, Base, true );
    }
    else
    {
        return  x_string::_x_dtoa( static_cast<u64>(Val), Buffer, Base, false );
    }
}

//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
int x_dtoa( const u64 Val, xbuffer_view<T_CHAR> Buffer, const int Base )
{
    return  x_string::_x_dtoa( u64(Val), Buffer, Base, false );
}

//------------------------------------------------------------------------------
// Description:
//      Converts a string that has been encoded into an integer of a particular base
//      into a actual atomic integer of a particular size (32vs64)bits. If the
//      string contains '_' or ':' characters they will be ignore.
// Returns:
//	    Actual integer number
// See Also:
//     x_atod64 x_atod32 x_atoi32 x_atoi64 x_atof32 x_atof64
//------------------------------------------------------------------------------
template< typename T_CHAR >
s64 x_atod64( const T_CHAR* pStr, const int Base )
{
    T_CHAR  C;          // Current character.
    T_CHAR  Sign;       // If '-', then negative, otherwise positive.
    s64     Total = 0;  // Current total.
    s32     ValidBase;

    x_assert( pStr );
    x_assert( Base > 2 );
    x_assert( Base <= (26+26+10) );

    // Skip whitespace.
    for( ; *pStr == T_CHAR{' '}; ++pStr )
        ; // empty body

     // Save sign indication.
    C = *pStr++;
    Sign = C;

    // Skip sign.
    if( (C == T_CHAR{'-'}) || (C == T_CHAR{'+'}) )
    {
        C = *pStr++;
    }

    // Decode the rest of the string
    for( ; ; C = *pStr++ )
    {
        if( (C >= T_CHAR{'0'}) && (C <= T_CHAR{'9'}) )  
        {
            ValidBase = C - T_CHAR{'0'};
        }
        else if( (C >= T_CHAR{'a'}) && (C <= T_CHAR{'z'}) )  
        {
            ValidBase = C - T_CHAR{'a'} + 10;
        }
        else if( (C >= T_CHAR{'A'}) && (C <= T_CHAR{'Z'}) )  
        {
            ValidBase = C - T_CHAR{'A'} + 10+26;
        }
        else if( C == T_CHAR{'_'} || C == T_CHAR{':'} )  
        {
            // Ignore
            continue;
        }
        else
        {
            // Negate the total if negative.
            if( Sign == T_CHAR{'-'} ) 
                Total = -Total;

            // Any other character is bad news
            return Total;
        }

        x_assert( ValidBase >= 0 );
        x_assert( ValidBase <  Base );

        // Accumulate digit.
        Total = (Base * Total) + ValidBase;
    }

    // Not way to get here
    x_assert( false );
}

//------------------------------------------------------------------------------
template< typename T_CHAR >
int x_atod32( const char* pStr, const int Base )
{
    return static_cast<int>( x_atod64(pStr, Base ));
}

//------------------------------------------------------------------------------
// Description:
//      Case dependent hash
//      FNV hashes are designed to be fast while maintaining a low
//      collision rate. The FNV speed allows one to quickly hash lots
//      of data while maintaining a reasonable collision rate.  See:
//      http://www.isthe.com/chongo/tech/comp/fnv/index.html
//      for more details as well as other forms of the FNV hash.
// Returns:
//	    32 bit hash as a static hash type
// See Also:
//     x_memHash
//------------------------------------------------------------------------------
template< typename T_CHAR >
u32 x_strHash( const T_CHAR* pStr, const u32 Range, const u32 ahVal )
{
    x_assert( pStr );

    // unsigned string 
    auto s = reinterpret_cast<const typename x_size_to_unsigned<sizeof(T_CHAR)>::type*>(pStr);

    //
    // FNV-1 hash each octet in the buffer
    //
    u32 hVal = ahVal;
    while(*s) 
    {
        //.. multiply by the 32 bit FNV magic prime mod 2^32 
        hVal *= 0x01000193;

        // xor the bottom with the current octet 
        hVal ^= static_cast<u32>(*s++);
    }

    // Do we need to compute a different range?
    if( Range != 0xffffffff )
    {
        const u32 RetryLevel = (0xffffffff/Range)*Range;
        while( hVal >= RetryLevel ) 
        {
            hVal = (hVal * ((u32)16777619UL)) + ((u32)2166136261UL);
        }
        hVal %= Range;
    }

    // return our new hash value 
    return hVal;
}

//------------------------------------------------------------------------------
// Description:
//      Case independent hash
//      FNV hashes are designed to be fast while maintaining a low
//      collision rate. The FNV speed allows one to quickly hash lots
//      of data while maintaining a reasonable collision rate.  See:
//      http://www.isthe.com/chongo/tech/comp/fnv/index.html
//      for more details as well as other forms of the FNV hash.
// Returns:
//	    32 bit hash as a static hash type
// See Also:
//     x_memHash
//------------------------------------------------------------------------------
template< typename T_CHAR >
u32 x_strIHash ( const T_CHAR* pStr, const u32 Range, const u32 ahVal )
{
    x_assert( pStr );

    // unsigned string 
    auto s = reinterpret_cast<const typename x_size_to_unsigned<sizeof(T_CHAR)>::type*>(pStr);

    //
    // FNV-1 hash each octet in the buffer
    //
    u32 hVal = ahVal;
    while(*s) 
    {
        //.. multiply by the 32 bit FNV magic prime mod 2^32 
        hVal *= 0x01000193;

        // xor the bottom with the current octet 
         hVal ^= (s32)x_tolower(*s);

        // move to next character
        s++;
    }

    // Do we need to compute a different range?
    if( Range != 0xffffffff )
    {
        const u32 RetryLevel = (0xffffffff/Range)*Range;
        while( hVal >= RetryLevel ) 
        {
            hVal = (hVal * ((u32)16777619UL)) + ((u32)2166136261UL);
        }
        hVal %= Range;
    }

    // return our new hash value 
    return hVal;
}

//------------------------------------------------------------------------------
// Description:
//      Case dependent hash 64 bits
//      FNV hashes are designed to be fast while maintaining a low
//      collision rate. The FNV speed allows one to quickly hash lots
//      of data while maintaining a reasonable collision rate.  See:
//      http://www.isthe.com/chongo/tech/comp/fnv/index.html
//      for more details as well as other forms of the FNV hash.
// Returns:
//	    32 bit hash as a static hash type
// See Also:
//     x_memHash
//------------------------------------------------------------------------------
template< typename T_CHAR >
u64 x_strIHash64( const T_CHAR* pStr, const u64 ahVal )
{
    // unsigned string 
    auto s = reinterpret_cast<const typename x_size_to_unsigned<sizeof(T_CHAR)>::type*>(pStr);
    
    //
    // FNV-1 hash each octet of the string
    //
    u64 hVal = ahVal;
    while( *s )
    {
        // multiply by the 64 bit FNV magic prime mod 2^64
        hVal *= ((u64)0x100000001b3ULL);
        
        // xor the bottom with the current octet
        hVal ^= (u64)x_tolower(*s);
        
        // move to next character
        s++;
    }
    
    // return our new hash value
    return hVal;
}

//------------------------------------------------------------------------------
/*
template< typename T_CHAR >
u32 x_strCRC( const T_CHAR* pStr, u32 crcSum )
{
    s32 Length = x_strlen( pStr );
    return x_memCRC32( pStr, Length, crcSum );
}
*/

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
int x_strncpy( T_CHAR* pDest, const T_CHAR* pSrc, int Count, int DestSize )
{
    x_assert( pDest );
    x_assert( pSrc  );
    x_assert( Count >= 0 );
    x_assert( Count < DestSize );

    pDest[Count--] = 0;
    while( Count >= 0 )
    {
        pDest[Count] = pSrc[Count];
        Count--;
    }
    
    return Count;
}

//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
int x_strcpy( xbuffer_view<T_CHAR> Dest, const T_CHAR* pSrc )
{
    x_assert( Dest.getCount() > 0 );
    x_assert( pSrc  );
    
    const int MaxChar = static_cast<int>(Dest.getCount()) - 1;
    int i;
    for( i=0; (Dest[i] = pSrc[i]) && (i < MaxChar); i++ );
    
#ifdef _X_DEBUG
    for( int j = i; j < MaxChar; j++ ) Dest[j] = T_CHAR{'?'};
#endif
    
    Dest[i] = 0;
    
    return i;
}

//------------------------------------------------------------------------------
template< typename T_CHAR >  x_inline
int x_strcmp( const T_CHAR* pStr1, const T_CHAR* pStr2 )
{
    int Result = 0;

    x_assert( pStr1 );
    x_assert( pStr2 );

    if( pStr1 == pStr2 ) return 0;

    while( !( Result = int(*pStr1) - int(*pStr2) ) && *pStr1 )
        ++pStr1, ++pStr2;

    return Result;
}


//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
int x_strncmp( const T_CHAR* pStr1, const T_CHAR* pStr2, int Count )
{
    x_assert( pStr1 );
    x_assert( pStr2 );
    x_assert( Count >= 0 );
    
    if( !Count )
        return( 0 );
    
    while( --Count && *pStr1 && (*pStr1 == *pStr2) )
    {
        pStr1++;
        pStr2++;
    }
    
    return ((int)*pStr1) - ((int)*pStr2);
}

//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
int x_stricmp( const T_CHAR* pStr1, const T_CHAR* pStr2 )
{
    T_CHAR f, l;

    x_assert( pStr1 );
    x_assert( pStr2 );

    if( pStr1 == pStr2 ) return 0;

    do
    {
        if( ( (l = *(pStr1++) ) >= T_CHAR{'A'}) && (l <= T_CHAR{'Z'}) )
            l -= T_CHAR{'A'} - T_CHAR{'a'};

        if( ( (f = *(pStr2++) ) >= T_CHAR{'A'}) && (f <= T_CHAR{'Z'}) )
            f -= T_CHAR{'A'} - T_CHAR{'a'};

    } while( f && (f == l) );

    return static_cast<int>(l) - static_cast<int>(f) ;
}

//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
int x_strstr( const T_CHAR* const pMainStr, const T_CHAR* const pSubStr )
{
    const T_CHAR* pM = pMainStr;
    const T_CHAR* pS1; 
    const T_CHAR* pS2;

    if( !*pSubStr )
        return -1;

    while( *pM )
    {
        pS1 = pM;
        pS2 = pSubStr;

        while( *pS1 && *pS2 && !(*pS1 - *pS2) )
        {
            pS1++;
            pS2++;
        }

        if( !*pS2 )
            return static_cast<int>(pM - pMainStr);

        pM++;
    }

    return -1;
}

//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
int x_stristr( const T_CHAR* const pMainStr, const T_CHAR* const pSubStr )
{
    const T_CHAR* pM = pMainStr;
    const T_CHAR* pS1; 
    const T_CHAR* pS2;

    if( !*pSubStr )
        return -1;

    while( *pM )
    {
        pS1 = pM;
        pS2 = pSubStr;

        while( *pS1 && *pS2 && !(x_tolower(*pS1) - x_tolower(*pS2)) )
        {
            pS1++;
            pS2++;
        }

        if( !*pS2 )
            return static_cast<int>(pM - pMainStr);

        pM++;
    }

    return -1 ;
}

//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
u32 x_strCRC( const T_CHAR* pStr, u32 crcSum )
{
    const s32 Length = sizeof(T_CHAR) * x_strlen( pStr ).m_Value;
    return x_memCRC32( xbuffer_view<const xbyte>( reinterpret_cast<const xbyte*>(pStr), Length), crcSum );
}


//------------------------------------------------------------------------------
// Description:
//      These functions convert a character string to a double-precision, floating-point value 
//      (atof64 and atof32), an integer, long integer value (atoi32, atoi64).
//
//<P>   The input string is a sequence of characters that can be interpreted as a numerical value 
//      of the specified type. The function stops reading the input string at the first character 
//      that it cannot recognize as part of a number. This character may be the null character 
//      ('\\0' or L'\\0') terminating the string.
//
//<P>   The string argument to atof32 and atof64 has the following form:
//
//<P>   [whitespace] [sign] [digits] [.digits] [ {d | D | e | E }[sign]digits]
//
//<P>   A whitespace consists of space or tab characters, which are ignored; sign is either plus (+) or minus (?; 
//      and digits are one or more decimal digits. If no digits appear before the decimal point, at least one must 
//      appear after the decimal point. The decimal digits may be followed by an exponent, which consists 
//      of an introductory letter (d, D, e, or E) and an optionally signed decimal integer.
//
//<P>   atoi32, and atoi64 do not recognize decimal points or exponents. 
//      The string argument for these functions has the form:
//
//<P>   [whitespace] [sign]digits
//
//<P>   where whitespace, sign, and digits are as described for atof32 and atof64.
// Returns:
//      Each function returns the value produced by interpreting the input characters as a number. 
//      The return value is 0 if the input cannot be converted to a value of that type. 
//      The return value is undefined in case of overflow.
//
// Example:
// <CODE>
//      s32 main( void )
//      {
//         char *s; f64 x; s32 i; s64 li;
//
//          // Test of atof64
//          s = "  -2309.12E-15";     
//          x = x_atof64( s );
//          x_printf( "atof64 test: \\"%s\\"; float:  %e\\n", s, x );
//
//          // Test of atof32 
//          s = "7.8912654773d210";  
//          x = x_atof32( s );
//          x_printf( "atof32 test: \\"%s\\"; float:  %e\\n", s, x );
//
//          // Test of atoi 
//          s = "  -9885 pigs";      
//          i = x_atoi32( s );
//          x_printf( "atoi32 test: \\"%s\\"; integer: %d\\n", s, i );
//
//          // Test of atol 
//          s = "98854 dollars";     
//          li = x_atoi64( s );
//          x_printf( "atoi64 test: \\"%s\\"; long: %Ld\\n", s, li );
//      }
// </CODE>
//
// <TABLE>
//      The Output
//      -----------------------------------------------------
//      atof64 test: "  -2309.12E-15"; float:  -2.309120e-012
//      atof32 test: "7.8912654773d210"; float:  7.891265e+210
//      atoi32 test: "  -9885 pigs"; integer: -9885
//      atoi64 test: "98854 dollars"; long: 98854
// </TABLE>
// See Also:
//     x_sprintf
//------------------------------------------------------------------------------
template< typename T_CHAR >  x_inline
int x_atoi32( const T_CHAR* pStr )
{
    T_CHAR  C;         // Current character.
    T_CHAR  Sign;      // If '-', then negative, otherwise positive.
    int     Total;     // Current total.

    x_assert( pStr );

    // Skip whitespace.
    for( ; *pStr == T_CHAR{' '}; ++pStr )
        ; // empty body

     // Save sign indication.
    C = *pStr++;
    Sign = C;

    // Skip sign.
    if( (C == T_CHAR{'-'}) || (C == T_CHAR{'+'}) )
    {
        C = *pStr++;
    }

    Total = 0;

    while( (C >= T_CHAR{'0'}) && (C <= T_CHAR{'9'}) )
    {
        // Accumulate digit.
        Total = (10 * Total) + (C - '0');

        // Get next char.
        C = *pStr++;
    }

    // Negate the total if negative.
    if( Sign == T_CHAR{'-'} ) 
        Total = -Total;

    return Total;
}

//------------------------------------------------------------------------------
// <COMBINE x_atoi32 >
template< typename T_CHAR >  x_inline
s64 x_atoi64( const T_CHAR* pStr )
{
    T_CHAR C;         // Current character.
    T_CHAR Sign;      // If '-', then negative, otherwise positive.
    s64    Total;     // Current total.

    x_assert( pStr );

    // Skip whitespace.
    for( ; *pStr == T_CHAR{' '}; ++pStr )
        ; // empty body

     // Save sign indication.
    C = *pStr++;
    Sign = C;

    // Skip sign.
    if( (C == T_CHAR{'-'}) || (C == T_CHAR{'+'}) )
    {
        C = *pStr++;
    }

    Total = 0;

    while( (C >= T_CHAR{'0'}) && (C <= T_CHAR{'9'}) )
    {
        // Accumulate digit.
        Total = (10 * Total) + (C - T_CHAR{'0'});

        // Get next char.
        C = *pStr++;
    }

    // Negate the total if negative.
    if( Sign == T_CHAR{'-'} ) 
        Total = -Total;

    return Total;
}

//------------------------------------------------------------------------------
template< typename T_CHAR >  x_inline
bool x_isstrint( const T_CHAR* pStr )
{
    if( pStr[0] == T_CHAR{'-'} )
    {
        if( pStr[1] == 0 )
            return false;

        for( int i=1; pStr[i]; i++ )
        {
            if( pStr[i] < T_CHAR{'0'} || pStr[i] > T_CHAR{'9'} ) return false;
        }
    }
    else
    {
        for( int i=0; pStr[i]; i++ )
        {
            if( pStr[i] < '0' || pStr[i] > '9' ) return false;
        }

    }

    return true;
}

//------------------------------------------------------------------------------
template< typename T_CHAR >  x_inline
bool x_isstrfloat( const T_CHAR* pStr )
{
    x_constexprvar xarray<T_CHAR,11> pF { "Ee.#QNABIF" };

    // Does it have any other of the strange characters?
    if( pStr[0] == T_CHAR{'-'} )
    {
        if ( pStr[1] == 0 )
            return false;

        for( int i=1 ; pStr[i]; i++ )
        {
            int l;

            if( pStr[i] >= T_CHAR{'0'} && pStr[i] <= T_CHAR{'9'} ) 
                continue;

            for( l=0; (pStr[i]!=pF[l]) && pF[l]; l++ );

            // Okay this is not longer a number
            if( pF[l] == 0 ) return false;
        }
    }
    else
    {
        for( int i=0 ; pStr[i]; i++ )
        {
            int l;

            if( pStr[i] >= '0' && pStr[i] <= '9' ) 
                continue;

            for( l=0; (pStr[i]!=pF[l]) && pF[l]; l++ );

            // Okay this is not longer a number
            if( pF[l] == 0 ) return false;
        }
    }

    return true;
}

//------------------------------------------------------------------------------
template< typename T_CHAR >  x_inline
bool x_isstrhex( const T_CHAR* pStr )
{
    for( int i=0; pStr[i]; i++ )
    {
        if( pStr[i] >= T_CHAR{'0'} && pStr[i] <= T_CHAR{'9'} ) continue;
        if( pStr[i] >= T_CHAR{'A'} && pStr[i] <= T_CHAR{'F'} ) continue;
        if( pStr[i] >= T_CHAR{'a'} && pStr[i] <= T_CHAR{'f'} ) continue;
        return false;
    }

    return true;
}

//------------------------------------------------------------------------------
template< typename T_CHAR >  x_inline
bool x_isstrguid( const T_CHAR* pStr )
{
    int nCol=0;
	int i;
    for( i=0; pStr[i]; i++ )
    {
        if( pStr[i] >= T_CHAR{'0'} && pStr[i] <= T_CHAR{'9'} ) continue;
        if( pStr[i] >= T_CHAR{'A'} && pStr[i] <= T_CHAR{'F'} ) continue;
        if( pStr[i] >= T_CHAR{'a'} && pStr[i] <= T_CHAR{'f'} ) continue;
        if( pStr[i] == T_CHAR{':'} ) 
        {
            nCol++;
            if( nCol > 1 ) return false;
            continue;
        }
        return false;
    }

    // the length of the string must be 17 characters long (16+1 for the':')
    if( i != 17 ) return false;

    return true;
}

//------------------------------------------------------------------------------
// This implementation will handle scientific notation such as "1.23e+7".
// <COMBINE x_atoi32 >
template< typename T_CHAR > x_inline
f32 x_atof32( const T_CHAR* pStr )
{
    int   ISign   = 1;    // Integer portion sign.
    int   IValue  = 0;    // Integer portion value.
    int   DValue  = 0;    // Decimal portion numerator.
    int   DDenom  = 1;    // Decimal portion denominator.
    bool  Integer = true; // Is it an integer?

    struct simple
    {
        x_inline static f32 pow( f32 x, int p )
        {
            f32 r;

            if (p == 0   ) return 1.0f;
            if (x == 0.0f) return 0.0f;

            if (p < 0)
            {
                p = -p;
                x = 1.0f / x;
            }

            r = 1.0;
            for(;;)
            {
                if (p & 1) r *= x;
                if ((p >>= 1) == 0) return r;
                x *= x;
            }
        }
    };

    x_assert( pStr );

    // Skip whitespace.
    for( ; *pStr == T_CHAR{' '}; ++pStr )
        ; // empty body

    // Get the sign.
    if( *pStr == T_CHAR{'-'} )  { pStr++; ISign = -1; }
    if( *pStr == T_CHAR{'+'} )  { pStr++;             }

    // Handle integer portion.  Accumulate integer digits.
    while( (*pStr >= T_CHAR{'0'} ) && (*pStr <= T_CHAR{'9'}) )
    {
        IValue = (IValue<<3) + (IValue<<1) + (*pStr-T_CHAR{'0'});
        pStr++;
    }

    // Handle decimal portion.
    if( *pStr == T_CHAR{'.'} )
    {
        // Its not an integer any more!
        Integer = false;

        // Skip the decimal point.
        pStr++;

        // Accumulate decimal digits.
        while( (*pStr >= T_CHAR{'0'}) && (*pStr <= T_CHAR{'9'}) )
        {
            DValue = (DValue<<3) + (DValue<<1) + (*pStr-T_CHAR{'0'});
            DDenom = (DDenom<<3) + (DDenom<<1);
            pStr++;
        }
    }

    // Handle scientific notation.
    if( (*pStr == T_CHAR{'e'}) || (*pStr == T_CHAR{'E'}) || (*pStr == T_CHAR{'d'}) || (*pStr == T_CHAR{'D'}) )
    {
        s32  EValue  = 0;   // exponent as an integer
        s32  ESign   = 1;   // Exponent portion sign.
        f32  e;             // Exponent order.

        // Skip the 'e' (or 'd').
        pStr++;

        // Get the sign for the exponent.
        if( *pStr == T_CHAR{'-'} )  { pStr++; ESign = -1; }
        if( *pStr == T_CHAR{'+'} )  { pStr++;             }

        // Accumulate exponent digits.
        while( (*pStr >= T_CHAR{'0'}) && ( *pStr <= T_CHAR{'9'} ) )
        {
            EValue = (EValue<<3) + (EValue<<1) + (*pStr-T_CHAR{'0'});
            pStr++;
        }

        // Compute exponent.
        x_constexprvar xarray<f32,25> PowTable = {{ 1.0f,10.0f,10e1f,10e2f,10e3f,10e4f,10e5f,10e6f,10e7f,10e8f,10e9f,10e10f,10e11f,10e12f,10e13f,10e14f,10e15f,10e16f,10e17f,10e18f,10e19f,10e20f,10e21f,10e22f,10e23f }};
        if( EValue < 23 )
        {
            e = PowTable[EValue];
        }
        else
        {
            static const auto simple_pow = []( f64 x, int p ) noexcept -> f64
            {
                if (p == 0)   return 1.0;
                if (x == 0.0) return 0.0;
                
                if (p < 0)
                {
                    p = -p;
                    x = 1.0 / x;
                }
                
                f64 r = 1.0;
                for(;;)
                {
                    if (p & 1) r *= x;
                    if ((p >>= 1) == 0) return r;
                    x *= x;
                }
                
                x_assume( false );
            };
            
            e = static_cast<f32>(simple_pow( 10, EValue ));
        }

        // Return final number for exponent case.
        {
            f32 Temp = ISign * (IValue + ((f32)DValue / (f32)DDenom));

            if( ESign < 0 )  return( Temp / e );
            else             return( Temp * e );
        }
    }

    // No exponent
    if( Integer ) return( (f32)(ISign * IValue) );
    
    return( ISign * (IValue + ((f32)DValue/(f32)DDenom)) );
}

//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
f64 x_atof64( const T_CHAR* pStr )
{
   return ::atof( pStr );
}



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// xstring
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

template< typename T_CHAR > x_forceinline
bool xstring_base<T_CHAR>::operator == ( const t_char* pStr2 ) const           { return x_strcmp( m_pString, pStr2 ) == 0; }

template< typename T_CHAR > x_forceinline
bool xstring_base<T_CHAR>::operator == ( t_char* pStr2 ) const                 { return x_strcmp( m_pString, pStr2 ) == 0; }

template< typename T_CHAR > x_forceinline
bool xstring_base<T_CHAR>::operator == ( const const_char* pStr2 ) const       { return x_strcmp( m_pString, reinterpret_cast<const t_char*>(pStr2) ) == 0; }


//------------------------------------------------------------------------------
template< typename T_CHAR > 
template< typename T > x_inline
xstring_base<T> xstring_base<T_CHAR>::To( void ) const
{
    const auto Length = typename xstring_base<T>::t_characters{ getLength().m_Value + 1 };
    xstring_base<T> NewString{ Length };
    
    for( int i=0; i<Length.m_Value; i++ )
    {
        NewString[i] = static_cast<T>( m_pString[i] );
    }
    
    return NewString;
}

//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
void xstring_base<T_CHAR>::setNull( void )                                
{ 
    DecrementRef(); 
    m_pString = nullptr; 
}

//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
xstring_base<T_CHAR>& xstring_base<T_CHAR>::Reset( t_characters nCharsNeeded )
{
    DecrementRef( nCharsNeeded );
    if( m_pString == nullptr && nCharsNeeded.m_Value > 0 ) m_pString = AllocateNewString( nCharsNeeded );
    return *this;
}

//------------------------------------------------------------------------------
template< typename T_CHAR >
template<typename... T_ARGS> x_inline 
xstring_base<T_CHAR> xstring_base<T_CHAR>::Make( t_characters nCharsNeeded, const char* pFormat, const T_ARGS&... Args )
{
    t_self String{ nCharsNeeded };
    x_vsprintf( 
        xbuffer_view<T_CHAR>
        { 
            String.m_pString, 
            static_cast<xuptr>( String.HowMuchMemoryIHave().m_Value ) 
        }, 
        pFormat, 
        xarg_list
        { 
            sizeof...(T_ARGS), 
            Args... 
        }   
    );

    return String;
}

//------------------------------------------------------------------------------
template< typename T_CHAR >
template<typename... T_ARGS> x_inline 
xstring_base<T_CHAR> xstring_base<T_CHAR>::Make( const char* pFormat, const T_ARGS&... Args )
{
    t_self String{ t_characters{ 240 } };
    x_vsprintf( xbuffer_view<T_CHAR>{ String.m_pString, static_cast<xuptr>(String.HowMuchMemoryIHave().m_Value) }, pFormat, xarg_list( sizeof...(T_ARGS), Args... ) );
    return String;
}

//------------------------------------------------------------------------------
template< typename T_CHAR >
template<typename... T_ARGS> x_inline 
xstring_base<T_CHAR>& xstring_base<T_CHAR>::setup( t_characters nCharsNeeded, const char* pFormat, const T_ARGS&... Args )
{
    Reset( nCharsNeeded );
    x_vsprintf( *this, pFormat, xarg_list( sizeof...(T_ARGS), Args... ) );
    return *this;
}

//------------------------------------------------------------------------------
template< typename T_CHAR >
template<typename... T_ARGS> x_inline 
xstring_base<T_CHAR>& xstring_base<T_CHAR>::setup( const char* pFormat, const T_ARGS&... Args )
{
    Reset( t_characters{ 240 } );
    x_vsprintf( *this, pFormat, xarg_list( sizeof...(T_ARGS), Args... ) );
    return *this;
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
xstring_base<T_CHAR>& xstring_base<T_CHAR>::operator = ( const const_char* pString )
{
    DecrementRef();
    m_pString = const_cast<t_char*>(reinterpret_cast<const t_char*>( pString) );
    x_assert( isSystemString() );
    return *this;
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
xstring_base<T_CHAR>& xstring_base<T_CHAR>::operator  = ( const xstring_base& String )
{
    if( String.m_pString == m_pString ) return *this;
    DecrementRef();
    m_pString = const_cast<T_CHAR*>( String.m_pString );
    AddReference();
    return *this;
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
void xstring_base<T_CHAR>::Copy( const t_char* pString, t_characters nCharsNeeded  )
{
    if( pString == m_pString ) return;
    if( pString == nullptr ) return;
    const auto Length = nCharsNeeded + t_characters{ 1 };
    AllocateIfNeedit( Length );
    x_strcpy(m_pString, Length, pString );
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
void xstring_base<T_CHAR>::Copy( const t_char* pString )
{
    if( pString == nullptr ) return;
    if( pString == m_pString ) return;
    Copy( pString, x_strlen( pString ) );
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
void xstring_base<T_CHAR>::Copy( const const_char* pString )
{
    if( pString == m_pString ) return;
    if( pString == nullptr ) return;
    DecrementRef();
    m_pString = pString;
    x_assert( isSystemString() );
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
void xstring_base<T_CHAR>::Copy( const t_self& String )
{
    if( m_pString == String.m_pString ) return;
    if( String.m_pString == nullptr ) return;
    if( String.isSystemString() )
    {
        DecrementRef();
        m_pString = String.m_pString;
    }
    else
    {
        const auto Length = x_strlen( String.m_pString  ) + t_characters{ 1 };
        AllocateIfNeedit( Length );
        x_strcpy(m_pString, Length, String.m_pString  );
    }
}

//------------------------------------------------------------------------------

template< typename T_CHAR > 
template< typename T > x_inline
void xstring_base<T_CHAR>::CopyAndConvert( const T* pString )
{
    if( (void*)m_pString == (void*)pString ) return;
    if( pString == nullptr ) return;
    int length = 0;
    for( length = 0; pString[length]; ++length );

    const auto Length = t_characters{ length + 1 };
    AllocateIfNeedit( Length );

    for( length = 0; ((*this)[length] = static_cast<t_char>(pString[length])); ++length );
}


//------------------------------------------------------------------------------
template< typename T_CHAR > x_inline
void xstring_base<T_CHAR>::append( const t_char* pString  )
{
    if( pString == nullptr ) return;
    if( m_pString == nullptr )
    {
        Copy( pString );
        return;
    }

    const auto MaxCapacity      = HowMuchCharacterCapacityIHave();
    const auto MyLength         = getLength();
    const auto OtherLength      = x_strlen(pString);
    const auto NewStringLength  = MyLength + OtherLength + t_characters{ 1 };

    if( NewStringLength > MaxCapacity )
    {
        auto pNewString = AllocateNewString( NewStringLength );
        x_strcpy( pNewString, NewStringLength, m_pString );
        x_strcpy( &pNewString[MyLength.m_Value], NewStringLength - MyLength, pString );
        Reset();
        m_pString = pNewString; 
    }
    else
    {
        x_strcpy( &m_pString[MyLength.m_Value], NewStringLength - MyLength, pString );
    }
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
typename xstring_base<T_CHAR>::t_characters xstring_base<T_CHAR>::getLength ( void ) const 
{
    if( m_pString == nullptr ) return t_characters{ 0 };
    return x_strlen( m_pString );
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
units_bytes xstring_base<T_CHAR>::HowMuchMemoryIHave( void ) const 
{
    if( nullptr == m_pString )  return 0;
    if( isSystemString() )      return 0;
    auto& Prop = getProperties();
    if( Prop.m_SIZE_TYPE == g_context::small_alloc_type::FULL_ALLOCATION ) return getLength();
    return g_context::getBytesFromAllocType( Prop.m_SIZE_TYPE );
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
typename xstring_base<T_CHAR>::t_characters xstring_base<T_CHAR>::HowMuchCharacterCapacityIHave( void ) const 
{
    if( nullptr == m_pString )  return t_characters{ 0 };
    if( isSystemString() )      return t_characters{ 0 };
    auto& Prop = getProperties();
    if( Prop.m_SIZE_TYPE == g_context::small_alloc_type::FULL_ALLOCATION ) return getLength();
    return t_characters{ ( g_context::getBytesFromAllocType( Prop.m_SIZE_TYPE ) - sizeof(properties) ) / sizeof( t_char ) };
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
void xstring_base<T_CHAR>::DecrementRef ( const t_characters nChars )
{
    if( nullptr == m_pString )  return;
    if( isSystemString() )
    {
        m_pString = nullptr;
        return;
    }

    auto& Prop = getProperties();
    if( --Prop.m_REFERENCES == 0 )
    {
        if( nChars.m_Value > 0 && HowMuchCharacterCapacityIHave() >= nChars )
        {
            // Never-mind we are keeping it
            Prop.m_REFERENCES++;
        }
        else
        {
            g_context::get().SmallFree( reinterpret_cast<xbyte*>(&Prop), Prop.m_SIZE_TYPE );
            m_pString = nullptr;
        }
    }
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
void xstring_base<T_CHAR>::AddReference( void )
{
    if( nullptr == m_pString )  return;
    if( isSystemString() )      return;
    auto& Prop = getProperties();
    ++Prop.m_REFERENCES;

    // If we hit this we run out references!
    x_assert( Prop.m_REFERENCES != properties::MASK_REFERENCES );
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
typename xstring_base<T_CHAR>::t_char* xstring_base<T_CHAR>::AllocateNewString( t_characters nChars ) const 
{
    xbyte*      pString;
    const auto  Size     = ComputeAllocSize( nChars );
    
    const auto  SizeType = g_context::get().SmallAlloc( pString, Size.m_Value );
        
    auto& Prop = reinterpret_cast<properties&>( *pString );

    Prop.m_Flags      = 0;
    Prop.m_REFERENCES = 1;
    Prop.m_SIZE_TYPE  = SizeType;

    // Init the new string to zero
    t_char* pNewString = reinterpret_cast<t_char*>( &reinterpret_cast<xbyte*>(pString)[ sizeof(properties) ] );
    *pNewString = 0;

    // set the final string
    return pNewString;
}
    
//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
bool xstring_base<T_CHAR>::AllocateIfNeedit ( t_characters nChars )
{
    Reset( nChars );
    return true;
}

//------------------------------------------------------------------------------

template< typename T_CHAR > x_inline
void xstring_base<T_CHAR>::CloneIfNeedit( void )
{
    if( nullptr == m_pString )  return;
    if( !isSystemString() ) 
    {
        if( !getProperties().m_CONSTANT )
            return;
    }

    t_characters  StringSize = getLength();
    t_char*       pNewString = AllocateNewString( ComputeAllocSize( StringSize ) );

    for( int i=0; i<StringSize.m_Value; i++) pNewString[i] = m_pString[i];
    pNewString[StringSize.m_Value] = 0;

    DecrementRef();
    m_pString = pNewString;
}

