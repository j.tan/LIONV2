//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline 
xvector3::xvector3( xradian Pitch, xradian Yaw )
{
    x_assume( x_isAlign(this,16) );
    setup( Pitch, Yaw );
}

//------------------------------------------------------------------------------
constexpr
f32 xvector3::operator []( const s32 i ) const
{
    return  x_assume( i >= 0 && i <= 3 ), 
            (&m_X)[i];
}

//------------------------------------------------------------------------------
inline
f32& xvector3::operator []( const s32 i )
{
    x_assert( i >= 0 && i <= 3 );
    return (&m_X)[i];
}

//------------------------------------------------------------------------------
inline 
void xvector3::setZero( void )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_setzero_ps();
#else
    m_X = m_Y = m_Z = m_W = 0;
#endif
}

//------------------------------------------------------------------------------
inline
void xvector3::setIdentity( void )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_setzero_ps();
#else
    m_X = m_Y = m_Z = m_W = 0;
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::setup( f32 X, f32 Y, f32 Z )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_set_ps( 1, Z, Y, X );
#else
    m_X = X;
    m_Y = Y;
    m_Z = Z;
    m_W = 1;
#endif
    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::setup( xradian Pitch, xradian Yaw )
{
    f32 PS, PC;
    f32 YS, YC;

    x_SinCos( Pitch, PS, PC );
    x_SinCos( Yaw,   YS, YC );

    return setup( (YS * PC), -PS, (YC * PC) );
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::setup( const f32 n )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW =_mm_set_ps( 1, n, n, n );
#else
    m_X = m_Y = m_Z = n;
    m_W = 1;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::setup( const f32x4& Register )
{
    m_XYZW = Register;
    return *this;
}

//------------------------------------------------------------------------------
inline 
f32 xvector3::getLength( void ) const
{
#ifdef _X_SSE2_SUPPORT
    f32x4 a = _mm_mul_ps( m_XYZW, m_XYZW );
          a = _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a));
          a = _mm_sqrt_ss(a);

    return x_getf32x4ElementByIndex<0>(a);
#else
    return x_Sqrt( Dot( *this ) );
#endif
}

//------------------------------------------------------------------------------
inline
f32 xvector3::getLengthSquared( void ) const
{
    return Dot( *this );
}

//------------------------------------------------------------------------------
inline
void xvector3::getRotationTowards( const xvector3&   DestV,
                                   xvector3&         RotAxis,
                                   xradian&          RotAngle ) const
{
    //
    // First get the total length of both vectors this, and dest
    //
    const f32 D = getLength() * DestV.getLength();
    if( D < 0.0001f )
    {
        RotAxis  = xvector3::Right();
        RotAngle = xradian{ 0.0_deg };
        return;
    }
    
    //
    // Check for the singularity at when the vectors add to 180 degrees.
    // At this point the cross product won't be able to find a perpendicular axis of rotation.
    // So we will choose a default one.
    //
    f32 Dot = (*this).Dot(DestV) / D;
    if( Dot <= -0.999f )  
    {
        const xvector3 absU = DestV.getAbs();

        // Get Orthogonal
        RotAxis = absU.m_X < absU.m_Y ? 
                    xvector3::Right() 
                    : 
                    (absU.m_Y < absU.m_Z ? 
                        xvector3::Up() 
                        : 
                        xvector3::Forward());

        //
        // Finally compute the final angle
        //
        RotAxis  = Cross( RotAxis ).NormalizeSafe();
        RotAngle = PI;
    }
    else
    {
        if ( Dot >  1.0f )  Dot =  1.0f;

        //
        // get the axis to rotate those vectors by
        //
        RotAxis = Cross( DestV );
    
        //
        // Finally compute the final angle
        //
        RotAxis.NormalizeSafe();
        RotAngle = x_ACos(Dot);
    }
}

//------------------------------------------------------------------------------
inline
f32 xvector3::getDistanceSquare( const xvector3& V ) const
{
    return ((*this) - V).getLengthSquared();
}

//------------------------------------------------------------------------------
inline
f32 xvector3::getDistance( const xvector3& V ) const
{
    return ((*this) - V).getLength();
}

//------------------------------------------------------------------------------
x_forceinline
bool xvector3::isValid( void ) const
{
    return  x_isValid(m_X) && 
            x_isValid(m_Y) && 
            x_isValid(m_Z);
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::Normalize( void )
{
#ifdef _X_SSE2_SUPPORT
    // get len
    f32x4 a = _mm_mul_ps( m_XYZW, m_XYZW );
          a = _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a));
          a = _mm_rsqrt_ss(a);

    const f32x4 oneDivLen = _mm_shuffle_ps( a, a, 0 );
    
    m_XYZW = _mm_mul_ps( m_XYZW, oneDivLen );
    
#else
    *this *= x_InvSqrt( Dot( *this ) );
#endif

    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::NormalizeSafe( void )
{
    const f32   Tof  = 0.00001f;
#ifdef _X_SSE2_SUPPORT
    // get len
    f32x4 a = _mm_mul_ps( m_XYZW, m_XYZW );
          a = _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a));

    if( x_getf32x4ElementByIndex<0>(a) < Tof )
    {
        setup( 1, 0, 0 );
        return *this;
    }

                  a = _mm_rsqrt_ss(a);
    f32x4 oneDivLen = _mm_shuffle_ps( a, a, 0 );
    
    m_XYZW = _mm_mul_ps( m_XYZW, oneDivLen );
#else
    f32 Div = Dot( *this );
	if( Div < Tof )
    {		
        setup( 1, 0, 0 );
        return *this;
    }
    Div = x_InvSqrt( Div );
    m_X *= Div;
    m_Y *= Div;
    m_Z *= Div;
#endif

    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator += ( const xvector3& V )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_add_ps( m_XYZW, V.m_XYZW );
#else
    m_X += V.m_X;
    m_Y += V.m_Y;
    m_Z += V.m_Z;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator -= ( const xvector3& V )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_sub_ps( m_XYZW, V.m_XYZW );
#else
    m_X -= V.m_X;
    m_Y -= V.m_Y;
    m_Z -= V.m_Z;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator *= ( const xvector3& V )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_mul_ps( m_XYZW, V.m_XYZW );
#else
    m_X *= V.m_X;
    m_Y *= V.m_Y;
    m_Z *= V.m_Z;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator *= ( f32 Scalar )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_mul_ps( m_XYZW, _mm_set1_ps( Scalar ) );
#else
    m_X *= Scalar;
    m_Y *= Scalar;
    m_Z *= Scalar;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xvector3& xvector3::operator /= ( f32 Div )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_div_ps( m_XYZW, _mm_set1_ps( Div ) );
#else
    const f32 Scalar = 1.0f/Div;
    m_X *= Scalar;
    m_Y *= Scalar;
    m_Z *= Scalar;
#endif
    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline
xvector3 operator / ( const xvector3& V, f32 Div )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_div_ps( V.m_XYZW, _mm_set1_ps( Div ) ) };
#else
    const f32 Scalar = 1.0f/Div;
    return xvector3{    V.m_X * Scalar, 
                        V.m_Y * Scalar, 
                        V.m_Z * Scalar };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3& V, f32 Scalar )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_mul_ps( V.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xvector3{    V.m_X * Scalar, 
                        V.m_Y * Scalar, 
                        V.m_Z * Scalar };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( f32 Scale, const xvector3& V )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_mul_ps( V.m_XYZW, _mm_set1_ps( Scale ) ) };
#else
    return xvector3{    V.m_X * Scale, 
                        V.m_Y * Scale, 
                        V.m_Z * Scale };
#endif
}


//------------------------------------------------------------------------------
inline
xvector3 operator + ( f32 Scalar, const xvector3& V )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_add_ps( V.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xvector3{    V.m_X + Scalar, 
                        V.m_Y + Scalar, 
                        V.m_Z + Scalar };
#endif
}

//------------------------------------------------------------------------------
inline
xvector3 operator + ( const xvector3& V, f32 Scalar )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_add_ps( V.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xvector3{    V.m_X + Scalar, 
                        V.m_Y + Scalar, 
                        V.m_Z + Scalar };
#endif
}

//------------------------------------------------------------------------------
inline
xvector3 operator - ( f32 Scalar, const xvector3& V )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_sub_ps( _mm_set1_ps( Scalar ), V.m_XYZW ) };
#else
    return xvector3{    Scalar - V.m_X, 
                        Scalar - V.m_Y, 
                        Scalar - V.m_Z };
#endif
}

//------------------------------------------------------------------------------
inline
xvector3 operator - ( const xvector3& V, f32 Scalar )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_sub_ps( V.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xvector3{    V.m_X - Scalar, 
                        V.m_Y - Scalar, 
                        V.m_Z - Scalar };
#endif
}

//------------------------------------------------------------------------------
constexpr 
bool xvector3::operator == ( const xvector3& V ) const
{
    return !(
                (x_Abs( V.m_X - m_X) > XFLT_TOL) ||
                (x_Abs( V.m_Y - m_Y) > XFLT_TOL) ||
                (x_Abs( V.m_Z - m_Z) > XFLT_TOL) 
            );
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3::getMin( const xvector3& V ) const
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_min_ps( m_XYZW, V.m_XYZW ) };
#else
    return xvector3{    x_Min( V.m_X, m_X ), 
                        x_Min( V.m_Y, m_Y ), 
                        x_Min( V.m_Z, m_Z ) };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3::getMax( const xvector3& V ) const
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_max_ps( m_XYZW, V.m_XYZW ) };
#else
    return xvector3{    x_Max( V.m_X, m_X ), 
                        x_Max( V.m_Y, m_Y ), 
                        x_Max( V.m_Z, m_Z ) );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator + ( const xvector3& V0, const xvector3& V1 )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_add_ps( V0.m_XYZW, V1.m_XYZW ) };
#else
    return xvector3{    V0.m_X + V1.m_X,
                        V0.m_Y + V1.m_Y,
                        V0.m_Z + V1.m_Z };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator + ( const xvector3d& V0, const xvector3& V1 )
{
    return V1 + xvector3( V0 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator + ( const xvector3& V0, const xvector3d&  V1 )
{
    return V0 + xvector3( V1 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3d&  V0, const xvector3& V1 )
{
    return xvector3( V0 ) - V1 ;
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3& V0, const xvector3d&  V1 )
{
    return V0 - xvector3( V1 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3d&  V0, const xvector3& V1 )
{
    return V1 * xvector3( V0 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3& V0, const xvector3d&  V1 )
{
    return V0 * xvector3( V1 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3d&  V0, const xvector3& V1 )
{
    return xvector3( V0 ) / V1 ;
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3& V0, const xvector3d&  V1 )
{
    return V0 / xvector3( V1 );
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3& V0, const xvector3& V1 )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_sub_ps( V0.m_XYZW, V1.m_XYZW) };
#else
    return xvector3{    V0.m_X - V1.m_X,
                        V0.m_Y - V1.m_Y,
                        V0.m_Z - V1.m_Z );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator * ( const xvector3& V0, const xvector3& V1 )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_mul_ps( V0.m_XYZW, V1.m_XYZW ) };
#else
    return xvector3{    V0.m_X + V1.m_X,
                        V0.m_Y + V1.m_Y,
                        V0.m_Z + V1.m_Z );
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator / ( const xvector3& V0, const xvector3& V1 )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_div_ps( V0.m_XYZW, V1.m_XYZW ) };
#else
    return xvector3{    V0.m_X / V1.m_X,
                        V0.m_Y / V1.m_Y,
                        V0.m_Z / V1.m_Z };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 operator - ( const xvector3& V )
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_sub_ps( _mm_setzero_ps(), V.m_XYZW ) };
#else
    return xvector3{    - V.m_X,
                        - V.m_Y,
                        - V.m_Z };
#endif
}

//------------------------------------------------------------------------------
inline 
f32 xvector3::Dot( const xvector3& V ) const
{
#ifdef TARGET_PC_32BIT
    f32 Dest;    
    __asm
    {
        mov         ebx, this
        mov         eax, V
        movaps      xmm0, dword ptr [ebx]
        mulps       xmm0, dword ptr [eax]      
        movaps      xmm1, xmm0                          
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1                          
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)    
        addss       xmm0, xmm1                          
        movss       xmmword ptr [Dest], xmm0
    }

    return Dest;
#elif defined _X_SSE2_SUPPORT
    
    const f32x4       A = _mm_mul_ps( m_XYZW, V.m_XYZW );
    const f32x4       B = _mm_add_ss(_mm_shuffle_ps( A, A, _MM_SHUFFLE(0,0,0,0)),
                          _mm_add_ss(_mm_shuffle_ps( A, A, _MM_SHUFFLE(1,1,1,1)),
                                     _mm_shuffle_ps( A, A, _MM_SHUFFLE(2,2,2,2))));

    return x_getf32x4ElementByIndex<0>( B );
#else
    return xvector3{    m_X * V.m_X + 
                        m_Y * V.m_Y +
                        m_Z * V.m_Z };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3 xvector3::Cross( const xvector3& V ) const
{
#ifdef TARGET_PC_32BIT
    // a = v0.y | v0.z | v0.x | xxx
    // b = v1.z | v1.x | v1.y | xxx
    // c = v0.z | v0.x | v0.y | xxx
    // d = v1.y | v1.z | v1.x | xxx
    f32x4 Dest;
    __asm
    {
        mov         ecx, V                     
        mov         edx, this

        movaps      xmm0, dword ptr [edx]
        movaps      xmm2, xmm0
        shufps      xmm0, xmm0, _MM_SHUFFLE(3,0,2,1)    // xmm0: W, X, Z, Y
        shufps      xmm2, xmm2, _MM_SHUFFLE(3,1,0,2)    // xmm2: W, Y, X, Z

        movaps      xmm1, dword ptr [ecx]
        movaps      xmm3, xmm1
        shufps      xmm1, xmm1, _MM_SHUFFLE(3,1,0,2)    // xmm1: W, Y, X, Z
        shufps      xmm3, xmm3, _MM_SHUFFLE(3,0,2,1)    // xmm3: W, X, Z, Y

        mulps       xmm0, xmm1
        mulps       xmm2, xmm3
        subps       xmm0, xmm2

        movaps      xmmword ptr [Dest], xmm0
    }
    return Dest;
#elif defined _X_SSE2_SUPPORT

    enum : int
    {
        X = 0,
        Y = 1,
        Z = 2,
        W = 3
    };

    const f32x4 A = _mm_mul_ps( _mm_shuffle_ps(   m_XYZW,   m_XYZW, _MM_SHUFFLE( W, X, Z, Y )),
                                _mm_shuffle_ps( V.m_XYZW, V.m_XYZW, _MM_SHUFFLE( W, Y, X, Z )) );
    const f32x4 B = _mm_mul_ps( _mm_shuffle_ps(   m_XYZW,   m_XYZW, _MM_SHUFFLE( W, Y, X, Z )),
                                _mm_shuffle_ps( V.m_XYZW, V.m_XYZW, _MM_SHUFFLE( W, X, Z, Y )) );

    return xvector3{ _mm_sub_ps(A, B) };

#else
    return xvector3{    m_Y * V.m_Z - m_Z * V.m_Y,
                        m_Z * V.m_X - m_X * V.m_Z,
                        m_X * V.m_Y - m_Y * V.m_X };
#endif
}

//------------------------------------------------------------------------------
inline
xvector3 xvector3::getAbs( void ) const
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_max_ps( m_XYZW, _mm_sub_ps( _mm_setzero_ps(), m_XYZW )) };
#else
    return { x_Abs( m_X ), x_Abs( m_Y ), x_Abs( m_Z ) };
#endif
}

//------------------------------------------------------------------------------
inline
bool xvector3::isInrange( const f32 Min, const f32 Max ) const
{
#ifdef _X_SSE2_SUPPORT
    const f32x4 t1 = _mm_add_ps( _mm_cmplt_ps( m_XYZW, _mm_set1_ps(Min) ), _mm_cmpgt_ps( m_XYZW, _mm_set1_ps(Max) ) );
    return x_getf32x4ElementByIndex<0>(t1) == 0 &&
           x_getf32x4ElementByIndex<1>(t1) == 0 &&
           x_getf32x4ElementByIndex<2>(t1) == 0;
#else
    return x_InRange( m_X, Min, Max ) &&
           x_InRange( m_Y, Min, Max ) &&
           x_InRange( m_Z, Min, Max );
#endif
}

//------------------------------------------------------------------------------
inline
xvector3 xvector3::getOneOver( void ) const
{
#ifdef _X_SSE2_SUPPORT
    return xvector3{ _mm_rcp_ps( m_XYZW ) };
#else
    return { 1.0f/m_X, 1.0f/m_Y, 1.0f/m_Z };
#endif
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::RotateX( xradian Rx )
{
    if( Rx.m_Value != 0.0f )
    {
        f32 S, C;
        x_SinCos( Rx, S, C );

        const f32 Y = m_Y;
        const f32 Z = m_Z;
        m_Y = (C * Y) - (S * Z);
        m_Z = (C * Z) + (S * Y);
    }
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::RotateY( xradian Ry )
{
    if( Ry.m_Value != 0.0f )
    {
        f32 S, C;
        x_SinCos( Ry, S, C );

        const f32 X = m_X;
        const f32 Z = m_Z;
        m_X = (C * X) + (S * Z);
        m_Z = (C * Z) - (S * X);
    }
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::RotateZ( xradian Rz )
{
    if( Rz.m_Value != 0.0f )
    {
        f32 S, C;
        x_SinCos( Rz, S, C );

        const f32 X = m_X;
        const f32 Y = m_Y;
        m_X = (C * X) - (S * Y);
        m_Y = (C * Y) + (S * X);
    }
    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3& xvector3::Rotate( const xradian3& R )
{
    RotateZ( R.m_Roll  );
    RotateX( R.m_Pitch );
    RotateY( R.m_Yaw   );
    return *this;
}

//------------------------------------------------------------------------------
inline
f32* xvector3::operator()( void )
{
    return &m_X;
}

//------------------------------------------------------------------------------
inline
xvector3 xvector3::getLerp( const f32 T, const xvector3& End ) const
{
    return (*this) + T * ( End - (*this) );
}

//------------------------------------------------------------------------------
inline
xvector3 xvector3::getReflection( const xvector3& V ) const
{
    return V - (2 * Dot(V)) * (*this);
}

//------------------------------------------------------------------------------
constexpr
bool xvector3::isRightHanded( const xvector3& P1, const xvector3& P2 ) const
{
    return ( (P1.m_X - m_X) * (P2.m_Y - m_Y) - 
             (P1.m_Y - m_Y) * (P2.m_X - m_X) ) < 0;
}

//------------------------------------------------------------------------------
inline
xvector3& xvector3::GridSnap( f32 GridX, f32 GridY, f32 GridZ )
{
    setup( x_Round( m_X, GridX ), 
           x_Round( m_Y, GridY ),
           x_Round( m_Z, GridZ ) );
    return *this;
}

//------------------------------------------------------------------------------
inline
xradian xvector3::getPitch( void ) const
{
    const f32 L = x_Sqrt( m_X*m_X + m_Z*m_Z );
    return -x_ATan2( m_Y, L );

}

//------------------------------------------------------------------------------
inline
xradian xvector3::getYaw( void ) const
{
    return x_ATan2( m_X, m_Z );
}

//------------------------------------------------------------------------------
inline
void xvector3::getPitchYaw( xradian& Pitch, xradian& Yaw ) const
{
    Pitch = getPitch();
    Yaw   = getYaw();
}

//------------------------------------------------------------------------------
inline
xradian xvector3::getAngleBetween( const xvector3& V ) const
{
    const f32 D = getLength() * V.getLength();
    if( x_Abs(D) < 0.00001f ) return xradian{ 0 };
    
    f32 Cos = Dot( V ) / D;
    
    if     ( Cos >  1.0f )  Cos =  1.0f;
    else if( Cos < -1.0f )  Cos = -1.0f;
    
    return x_ACos( Cos );
}

//------------------------------------------------------------------------------
//           * Start
//           |
//           <--------------(* this )
//           | Return Vector
//           |
//           |
//           * End
//
// Such: 
//
// this.getClosestVToLSeg(a,b).LengthSquared(); // Is the length square to the line segment
// this.getClosestVToLSeg(a,b) + this;          // Is the closest point in to the line segment
//
//------------------------------------------------------------------------------
inline 
xvector3 xvector3::getVectorToLineSegment( const xvector3& Start, const xvector3& End ) const
{
    xvector3 Diff = *this - Start;
    xvector3 Dir  = End   - Start;
    f32      T    = Diff.Dot( Dir );

    if( T > 0.0f )
    {
        const f32 SqrLen = Dir.Dot( Dir );

        if ( T >= SqrLen )
        {
            Diff -= Dir;
        }
        else
        {
            T    /= SqrLen;
            Diff -= T * Dir;
        }
    }

    return -Diff;
}

//------------------------------------------------------------------------------

inline
f32 xvector3::getSquareDistToLineSeg( const xvector3& Start, const xvector3& End ) const
{
    return getVectorToLineSegment(Start,End).getLengthSquared();
}

//------------------------------------------------------------------------------

inline
xvector3 xvector3::getClosestPointInLineSegment( const xvector3& Start, const xvector3& End ) const
{
    return getVectorToLineSegment(Start,End) + *this;
}

//------------------------------------------------------------------------------
inline
f32 xvector3::getClosestPointToRectangle( 
    const xvector3& P0,                      // Origin from the edges.
    const xvector3& E0,
    const xvector3& E1,
    xvector3&       OutClosestPoint ) const
{
    const xvector3  kDiff    = P0 - *this;
    const f32       fA00     = E0.getLengthSquared();
    const f32       fA11     = E1.getLengthSquared();
    const f32       fB0      = kDiff.Dot( E0 );
    const f32       fB1      = kDiff.Dot( E1 );
    f32             fS       = -fB0;
    f32             fT       = -fB1;
    f32             fSqrDist = kDiff.getLengthSquared();

    if( fS < 0.0f )
    {
        fS = 0.0f;
    }
    else if( fS <= fA00 )
    {
        fS /= fA00;
        fSqrDist += fB0*fS;
    }
    else
    {
        fS = 1.0f;
        fSqrDist += fA00 + 2.0f*fB0;
    }

    if( fT < 0.0f )
    {
        fT = 0.0f;
    }
    else if( fT <= fA11 )
    {
        fT /= fA11;
        fSqrDist += fB1*fT;
    }
    else
    {
        fT = 1.0f;
        fSqrDist += fA11 + 2.0f*fB1;
    }

    // setup the closest point
    OutClosestPoint = P0 + (E0 * fS) + (E1 * fT);

    return x_Abs(fSqrDist);
}


