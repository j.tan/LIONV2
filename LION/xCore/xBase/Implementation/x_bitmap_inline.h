//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//-------------------------------------------------------------------------------
x_forceinline
xbitmap::xbitmap( xbuffer_view<xbyte> Data, u32 Width, u32 Height, bool bReleaseWhenDone ) noexcept :
    m_pData                 { reinterpret_cast<xbitmap::mip *>( &Data[0] )  },
    m_DataSize              { Data.getCount<u64>()                          },
    m_FrameSize             { Data.getCount<u32>() -4                       },
    m_Height                { Height                                        },   
    m_Width                 { Width                                         },
    m_Flags                 { static_cast<bit_pack_fields>((bReleaseWhenDone ? bit_pack_fields::MASK_OWNS_MEMORY : 0) |
                                              (u16{FORMAT_XCOLOR} << bit_pack_fields::OFFSET_FORMAT)) },
    m_nMips                 { 1                                             },    
    m_nFrames               { 1                                             }
{
    x_assert( m_FrameSize == m_DataSize - ((m_nMips*sizeof(s32)) * m_nFrames) );
    x_assert( ( m_Width*m_Height*sizeof(xcolor) + sizeof(int) ) == Data.getByteSize() );
}

//-------------------------------------------------------------------------------
inline
xuptr xbitmap::getMipSize( s32 Mip ) const noexcept
{
    if( m_nMips == 0 )
        return m_DataSize - sizeof(s32);

    const xuptr MipOffset       = reinterpret_cast<xuptr>( getMipPtr(Mip, m_nFrames-1) );
    const xuptr NextMipOffset   = reinterpret_cast<xuptr>( ( Mip < m_nMips-1 ) ? getMipPtr(Mip+1, m_nFrames-1) : &m_RawData.m_pPtr[ m_DataSize ] );
    const xuptr Size            = NextMipOffset - MipOffset;
    
    return Size;
}

//-------------------------------------------------------------------------------
inline
int xbitmap::getFullMipChainCount( void ) const noexcept
{
    const s32 SmallerDimension  = x_Min( m_Height, m_Width );
    const s32 nMips             = x_Log2IntRoundUp( SmallerDimension ) + 1;
    return nMips;
}

//-------------------------------------------------------------------------------
inline
const void* xbitmap::getMipPtr( const int Mip, const int Frame ) const noexcept
{
    x_assume( m_Width  > 0 );
    x_assume( m_Height > 0 );
    x_assume( m_pData );
    x_assume( Mip < m_nMips );
    x_assume( Mip >= 0 );
    x_assume( Frame >= 0 );
    x_assume( Frame < m_nFrames );
    return &reinterpret_cast<const xbyte*>( &m_pData[ m_nMips ] )[ Frame * m_FrameSize + m_pData[ Mip ].m_Offset ];
}

//-------------------------------------------------------------------------------
inline
void* xbitmap::getMipPtr( const int Mip, const int Frame ) noexcept
{
    x_assume( m_Width  > 0 );
    x_assume( m_Height > 0 );
    x_assume( m_pData );
    x_assume( Mip < m_nMips );
    x_assume( Mip >= 0 );
    x_assume( Frame >= 0 );
    x_assume( Frame < m_nFrames );
    return &reinterpret_cast<xbyte*>( &m_pData[ m_nMips ] )[ Frame * m_FrameSize + m_pData[ Mip ].m_Offset ];
}

//-------------------------------------------------------------------------------
template< typename T > inline
xbuffer_view<T> xbitmap::getMip( const int Mip, const int Frame ) noexcept
{
          void* pMipPtr         = getMipPtr(Mip, m_nFrames-1);
    const xuptr MipOffset       = reinterpret_cast<xuptr>( pMipPtr );
    const xuptr NextMipOffset   = reinterpret_cast<xuptr>( ( Mip < m_nMips-1 ) ? getMipPtr(Mip+1, m_nFrames-1) : &m_RawData.m_pPtr[ m_DataSize ] );
    const xuptr Size            = NextMipOffset - MipOffset;

    x_assert( x_isAlign( Size, sizeof(T) ) );
    return xbuffer_view<T>{ reinterpret_cast<T*>(pMipPtr), Size/sizeof(T) };
}

//-------------------------------------------------------------------------------
template< typename T > inline
const xbuffer_view<T> xbitmap::getMip( const int Mip, const int Frame ) const noexcept
{
    const void* pMipPtr         = getMipPtr(Mip, m_nFrames-1);
    const xuptr MipOffset       = reinterpret_cast<xuptr>( pMipPtr );
    const xuptr NextMipOffset   = reinterpret_cast<xuptr>( ( Mip < m_nMips-1 ) ? getMipPtr(Mip+1, m_nFrames-1) : &m_RawData.m_pPtr[ m_DataSize ] );
    const xuptr Size            = NextMipOffset - MipOffset;
    
    x_assert( x_isAlign( Size, sizeof(T) ) );
    return xbuffer_view<T>{ reinterpret_cast<T*>( const_cast<void*>(pMipPtr) ), Size/sizeof(T) };
}

//-------------------------------------------------------------------------------
inline
bool xbitmap::hasAlphaInfo( void ) const noexcept
{
    return ( m_Flags.m_HAS_ALPHA_INFO ) ? hasAlphaChannel() : false;
}

//-------------------------------------------------------------------------------
constexpr
bool xbitmap::isSquare( void ) const noexcept
{
    return  x_assume( m_Width > 0 ),
            x_assume( m_Height > 0 ),
            x_assume( m_pData ),
            m_Width == m_Height;
}

//-------------------------------------------------------------------------------
constexpr
bool xbitmap::isPowerOfTwo( void ) const noexcept
{
    return  x_assume( m_Width > 0 ),
            x_assume( m_Height > 0 ),
            x_assume( m_pData ),
            x_isPowTwo( m_Width ) && x_isPowTwo( m_Height );
}
