//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------------

x_inline
u32 x_memCRC32( const xbuffer_view<xbyte> Buf, const u32 crcSum ) noexcept
{
    u32 crc = ~crcSum;
    for( const auto C : Buf )
    { 
        crc = (crc >> 8) ^ x_meta_prog_details::crc32::s_CRCTable[ (crc & 0xFF) ^ C];
    }
    return ~crc;
}

//---------------------------------------------------------------------------------------------
x_inline
u16 x_memCRC16( const xbuffer_view<xbyte> Buf, const u16 acrcSum ) noexcept
{
    u16 crcSum = ~acrcSum;
    
    for( auto C : Buf )
    {
        const u16 q = C ^ (crcSum >> 8);
        crcSum <<= 8;
        
        u16 r = (q >> 4) ^ q;
        
        crcSum ^= r; r <<= 5;
        crcSum ^= r; r <<= 7;
        crcSum ^= r;
    }
    
    return ~crcSum;
}
