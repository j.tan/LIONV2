//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "x_base.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// BASE PROTOCOL
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

#define BASE_COUNTER_TYPE           u32
#define BASE_COUNTER_BYTES          (sizeof(BASE_COUNTER_TYPE))
#define BASE_COUNTER_BITS           (BASE_COUNTER_BYTES*8)
#define BASE_COUNTER_PROTOCOL_SHIFT (BASE_COUNTER_BITS-2)
#define BASE_COUNTER_MASK           ((1<<BASE_COUNTER_PROTOCOL_SHIFT)-1)
#define BASE_COUNTER_PROTOCOL_MASK  (~BASE_COUNTER_MASK)

//------------------------------------------------------------------------------

void xnet_base_protocol::vResetClient( void ) noexcept
{
    m_BytesSent         = 0;
    m_PacketsSent       = 0;
    m_BytesReceived     = 0;
    m_PackagesReceived  = 0;

    //
    // Remove the client from the pending client queue
    //
    {
        xnet_base_protocol* pPrev = nullptr;
        for( xnet_base_protocol* pNext = m_pMesh->m_pPendingPacketClientList; pNext; pNext = pNext->m_pPendingNext )
        {
            if( pNext == this )
            {
                if( pPrev )
                {
                    pPrev->m_pPendingNext = pNext->m_pPendingNext;
                }
                else
                {
                    m_pMesh->m_pPendingPacketClientList = pNext->m_pPendingNext;
                }
                break;
            }
            pPrev = pNext;
        }
    }
}

//------------------------------------------------------------------------------

void xnet_base_protocol::setupConnection( xnet_address RemoteAddress, xnet_udp_mesh& Mesh ) noexcept 
{ 
    m_ConnectionAddress = RemoteAddress;
    m_pMesh             = &Mesh;
}

//------------------------------------------------------------------------------

u32 xnet_base_protocol::setPacketHeader( local_sequence& LocalSequence, xbuffer_view<xbyte> Buffer, protocol Protocol ) noexcept
{
    // Compute the CRCSeq
    const u32 LargeCRC = x_memCRC32( Buffer );
    const u32 FinalCRC = LargeCRC ^ LocalSequence.m_Value;
    const u32 CRCSeq   = ( FinalCRC & BASE_COUNTER_MASK ) | ( static_cast<u32>(Protocol) << BASE_COUNTER_PROTOCOL_SHIFT );

    // update the sequence
    LocalSequence = local_sequence{ BASE_COUNTER_MASK & (LocalSequence.m_Value + 1) };

    return CRCSeq;
}

//------------------------------------------------------------------------------

xnet_base_protocol::protocol xnet_base_protocol::getProtocol( const void* pData ) noexcept
{
    x_assert( pData );
    return static_cast<protocol>(x_NetworkEndian( *((BASE_COUNTER_TYPE*)pData) ) >> BASE_COUNTER_PROTOCOL_SHIFT);
}

//------------------------------------------------------------------------------

xnet_base_protocol::err xnet_base_protocol::getInfoPacketHeader( local_sequence LocalSequence, xbuffer_view<xbyte> Buffer, remote_sequence& PacketSequence ) noexcept 
{                                                                                                               
    // Definitely the size should be larger than 4 to play our game
    if( Buffer.getCount() < 4 ) return x_error_code( errors, ERR_FAILURE, "Unexpected small size for the packet" );

    // Deal with the CRC
    const u32 LargeCRC = x_memCRC32( xbuffer_view<xbyte>( &Buffer[BASE_COUNTER_BYTES], Buffer.getCount() - BASE_COUNTER_BYTES ) );
    const u32 CRCSeq   = x_NetworkEndian( *((BASE_COUNTER_TYPE*)&Buffer[0]) );
    
    PacketSequence = remote_sequence{ static_cast<int>((CRCSeq^LargeCRC) & BASE_COUNTER_MASK) };

    // Find the shortest distance 
    // as well as set the sequence in the same space
    // as our local sequence
    int Distance;
    if( PacketSequence.m_Value < LocalSequence.m_Value ) 
    {
        const auto Distance1 = LocalSequence.m_Value - PacketSequence.m_Value;
        const auto Distance2 = (PacketSequence.m_Value + BASE_COUNTER_MASK + 1 ) - LocalSequence.m_Value;
        if( Distance1 < Distance2 ) 
        {
            Distance = Distance1;
        }
        else 
        {
            Distance = Distance2;

            // Put sequence in the space as our local sequence
            PacketSequence += remote_sequence{ BASE_COUNTER_MASK+1 };
        }
    }
    else                        
    {
        const auto Distance1 = PacketSequence.m_Value - LocalSequence.m_Value;
        const auto Distance2 = (LocalSequence.m_Value + BASE_COUNTER_MASK+1 ) - PacketSequence.m_Value;
        if( Distance1 < Distance2 ) Distance = Distance1;
        else 
        {
            Distance = Distance2;

            // Put sequence in the space as our local sequence
            PacketSequence = -( remote_sequence{ BASE_COUNTER_MASK + 1 } - PacketSequence);
        }
    }

    // This means that either the packet is too old or that the CRC
    // is not valid. In whatever case we don't want the packet
    if( Distance > 60 )
    {
#ifdef NETWORKDEBUG
        X_NET_LOGGER( "CRC Did not match or Packet is too old (Probably someone hacking)" );
#endif
        return x_error_code( errors, ERR_ALERT_MID, "CRC Did not match or Packet is too old (Probably someone hacking)" );
    }

    // Okay we are done
    return x_error_ok( errors );
}

//------------------------------------------------------------------------------

xnet_socket::err xnet_base_protocol::WriteToSocket( t_hsocket hLocalSocket, const xbuffer_view<xbyte>& Packet ) noexcept
{
    return m_pMesh->WriteToSocket( hLocalSocket, Packet, m_ConnectionAddress );    
}

//------------------------------------------------------------------------------

void xnet_base_protocol::AddClientInPendingList( void ) noexcept
{
    m_pPendingNext = m_pMesh->m_pPendingPacketClientList;
    m_pMesh->m_pPendingPacketClientList = this;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// NON-RELIABLE PROTOCOL
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

void xnet_nonreliable_protocol::vResetClient( void ) noexcept
{
    xnet_base_protocol::vResetClient();

    m_NonRealibleLocalSequence     = local_sequence{ 0 };
    m_NonRealibleRemoteSequence    = remote_sequence{ 0 };
}

//------------------------------------------------------------------------------

xnet_socket::err xnet_nonreliable_protocol::SendNonreliablePacket( t_hsocket hLocalSocket, const xbuffer_view<xbyte> Packet ) noexcept
{
    x_assert( Packet.getCount() < MAX_NONRELIABLE_PACKET_SIZE );

    //
    // Build Final Packet
    //

    // Create the packet and get ready to send it
    const s32   CRCSize      = BASE_COUNTER_BYTES;
    const u32   CRCSqc       = setPacketHeader( m_NonRealibleLocalSequence, Packet, protocol::NONRELIABLE );
    const s32   FinalSize    = Packet.getCount<int>() + CRCSize;
    xbyte*      pFinalPacket = reinterpret_cast<xbyte*>(x_alloca(FinalSize));
    x_assume( pFinalPacket );
    (*(BASE_COUNTER_TYPE*)pFinalPacket) = x_NetworkEndian( CRCSqc );
    x_memcpy( &pFinalPacket[CRCSize], &Packet[0], Packet.getCount() );

    //
    // Send Packet over the network
    //
    auto Err = WriteToSocket( hLocalSocket, xbuffer_view<xbyte>{ pFinalPacket, static_cast<xuptr>(FinalSize) } ); 
    if( Err )
    {
        // Keep track of stats
        m_BytesSent += FinalSize;
        m_PacketsSent++;
        return Err;
    }

    return Err;
}

//------------------------------------------------------------------------------

xnet_socket::err xnet_nonreliable_protocol::SendBroadcast( s32 Port, xnet_socket& LocalSocket,  const xbuffer_view<xbyte> Packet ) noexcept
{
    local_sequence LocalSequence = local_sequence{ 0 };

    // Create the packet and get ready to send it
    const s32   CRCSize      = BASE_COUNTER_BYTES;
    const u32   CRCSqc       = setPacketHeader( LocalSequence, Packet, protocol::NONRELIABLE );
    const s32   FinalSize    = Packet.getCount<int>() + CRCSize;
    xbyte*      pFinalPacket = reinterpret_cast<xbyte*>(x_alloca(FinalSize));

    x_assume( pFinalPacket );
    (*(BASE_COUNTER_TYPE*)pFinalPacket) = x_NetworkEndian( CRCSqc );
    x_memcpy( &pFinalPacket[CRCSize], &Packet[0], Packet.getCount() );

    //
    // Send Packet over the network
    //
    xnet_address Address;
    Address.setup( 0xffffffff, Port );
    return LocalSocket.SendPacket( xbuffer_view<xbyte>{ pFinalPacket, static_cast<xuptr>(FinalSize) }, Address );
}


//------------------------------------------------------------------------------

xnet_nonreliable_protocol::err xnet_nonreliable_protocol::onRecivePacket( xbuffer_view<xbyte> Packet, s32& Size, remote_sequence Sequence ) noexcept
{
    //
    // We are going to filter packets out that are too old, if the user wants to deal with random sequence it should just use the base protocol.
    if( m_NonRealibleRemoteSequence.m_Value > Sequence.m_Value ) 
    {
        X_NET_LOGGER( "I got an unreliable packet that had a sequence of " << Sequence.m_Value << " but we are at " << m_NonRealibleRemoteSequence.m_Value << " so we will throw it out" );
        Size = 0;
        return x_error_ok( errors );  //x_error_code( errors, ERR_FAILURE, "Packet is too old is thrown out" );
    }

    // Update our internal sequence. The next packet we expect to get is the next one over
    m_NonRealibleRemoteSequence = remote_sequence{ BASE_COUNTER_MASK & (Sequence.m_Value + 1) };

    // Correct for the header
    Size = Packet.getCount<int>() - BASE_COUNTER_BYTES;
    Packet.CopyToFrom( Packet.ViewFrom( BASE_COUNTER_BYTES ) );
    //x_memmove( &Packet[0], &Packet[BASE_COUNTER_BYTES], Size );

    return x_error_ok( errors );
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// RELIABLE PROTOCOL
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

// The more you wait the less data is transmitted however the less resistance to 
// packet lost. 1 will mean that for every one reliable packet receive we will send
// back 32/(n+1) confirmations. 2 will mean 32/(2+1) confirmations. etc...
//#define MAX_PACKETS_WAIT_FOR_ACKNOWLEAGE 0

// How many packets we will wait without confirmation until we decide to send the packet again
#define MAX_PACKETS_WAIT_FOR_RESENT      16 //12

//------------------------------------------------------------------------------

void xnet_reliable_protocol::vResetClient( void ) noexcept
{
    xnet_nonreliable_protocol::vResetClient();

    for( s32 i=0; i<m_PendingPacketPool.getCount()-1; i++ )
    {
        m_PendingPacketPool[i].m_pNext = &m_PendingPacketPool[i+1]; 
    }

    m_PendingPacketPool[ m_PendingPacketPool.getCount() - 1 ].m_pNext = nullptr;
    m_pReliablePoolFreeList = &m_PendingPacketPool[0];

    // Initialize all the packets
    m_TotalInQueue = 0;
    m_PendingQueue.MemSet(0);

    // Reliable protocol 
    m_RealibleSequence    = local_sequence{ 0 };
    m_RecivedPacketsMask  = 0;
    m_bForcePing          = false;  

    // Basic stats
    m_nResentPackets = 0;

    // Reset the timer
    m_Timer.Trip();
    m_TimeSinceLastRecived.Trip();
}

//------------------------------------------------------------------------------

xnet_reliable_protocol::xnet_reliable_protocol( void ) noexcept
{
    // Allocate packets
    m_PendingPacketPool.New( m_PendingQueue.getCount() );
    x_assert( m_PendingPacketPool.isValid() );
    for( s32 i=0; i<m_PendingPacketPool.getCount()-1; i++ )
    {
        m_PendingPacketPool[i].m_pNext = &m_PendingPacketPool[i+1]; 
    }
    m_PendingPacketPool[m_PendingPacketPool.getCount()-1].m_pNext = nullptr;
    m_pReliablePoolFreeList = &m_PendingPacketPool[0];

    // Start the timer
    m_Timer.Start();
    m_TimeSinceLastRecived.Start();
}


//------------------------------------------------------------------------------

xnet_reliable_protocol::err xnet_reliable_protocol::Update( t_hsocket hLocalSocket ) noexcept
{
    // Did we run out of memory?
    if( m_pReliablePoolFreeList == NULL )
    {
        X_NET_LOGGER( "No more free packets in the reliable protocol (out of memory) " );
        return x_error_code( errors, ERR_FATAL, "Run out of packets (out of memory)" );
    }

    // If we have not received noting for X seconds we will assume the other client has timed out
    if( m_TimeSinceLastRecived.getSeconds().count() >= 10 )
    {
        X_NET_LOGGER( "We have received nothing for more than 10 seconds... strange!!" );
        return x_error_code( errors, ERR_TIMEOUT, "We have received nothing for more than 10 seconds... strange??" );
    }

    // Lets make sure that we always send heart beats every second
    if( m_Timer.getSeconds().count() >= 1 )
    {
        m_bForcePing = true;
        m_Timer.Trip();
    }

    // If we need to send a ping lets do it
    if( m_bForcePing )
    {
        auto Err = SendReliablePacket( hLocalSocket, xbuffer_view<xbyte>{ nullptr, 0 } );
        if( Err ) 
        {
            X_NET_LOGGER( "The ping packet we just sent fail, with error message: " << Err.getString() );
            Err.IgnoreError();
            return x_error_code( errors, ERR_FAILURE, "Ping Packet sent fail" );
        }

        m_bForcePing = false;
    }

    return x_error_ok( errors );
}

//------------------------------------------------------------------------------

xnet_socket::err xnet_reliable_protocol::SendReliablePacket( t_hsocket hSocket, const xbuffer_view<xbyte> Packet ) noexcept
{
    return SendReliablePacket( hSocket, Packet, protocol::RELIABLE );
}

//------------------------------------------------------------------------------

xnet_socket::err xnet_reliable_protocol::SendReliablePacket( t_hsocket hSocket, const xbuffer_view<xbyte> Packet,  protocol Protocol ) noexcept
{
    x_assert( Packet.getCount() < MAX_RELIABLE_PACKET_SIZE );

    // We are sending a reliable packet so not need to force a ping for sure
    m_bForcePing = false;
    m_Timer.Trip(); 

    //
    // If we deal with length>0 then it is not a heart beat
    //
    if( Packet.getCount() > 0 )
    {
        if( m_TotalInQueue >= m_PendingQueue.getCount() ||
            m_pReliablePoolFreeList == nullptr )
        {
            X_NET_LOGGER( "Run out of temporary memory. Returning an error on sending." );
            return x_error_code( xnet_socket::errors, ERR_FAILURE, "Out of memory");
        }

        //
        // Get a free pending packet
        //
        pending_packet& PendingPacket = *m_pReliablePoolFreeList;
        m_pReliablePoolFreeList = m_pReliablePoolFreeList->m_pNext;

        // Fill the pending packet
        PendingPacket.m_Sequence = m_RealibleSequence;
        PendingPacket.m_pNext    = NULL;
        PendingPacket.m_Size     = Packet.getCount<int>();
        PendingPacket.m_Protocol = Protocol;
        PendingPacket.m_Data.CopyToFrom( Packet );

        // Insert the pending packet in the pending queue
        m_PendingQueue[m_TotalInQueue++] = &PendingPacket;
    }

    //
    // fill the standard packet header
    //
    // Setup the final packet
    const s32   HeaderSize   = 4*2;
    const xuptr FinalSize    = Packet.getCount() + HeaderSize;
    xbyte*      pFinalPacket = reinterpret_cast<xbyte*>(x_alloca(FinalSize));
    x_assume( pFinalPacket );

    if( Packet.isValid() ) x_memcpy( &pFinalPacket[HeaderSize], &Packet[0], Packet.getCount() );

    // Note that m_RealibleSequence will be changed inside the setPacketHeader function
    (*(u32*)&pFinalPacket[4*1]) = x_NetworkEndian( m_RecivedPacketsMask );
    
    const u32 CRCSqc  = setPacketHeader( m_RealibleSequence, xbuffer_view<xbyte>{ &pFinalPacket[4*1], FinalSize-4 }, Protocol );
    (*(u32*)&pFinalPacket[4*0]) = x_NetworkEndian( CRCSqc );

    // shift the sequence forward as we have updated the m_RealibleSequence
    m_RecivedPacketsMask <<=1;

    //
    // Send Packet over the network
    //
    auto Err = WriteToSocket( hSocket, xbuffer_view<xbyte>{ pFinalPacket, FinalSize } );
    if( Err ) return Err;

    // Keep track of stats
    m_BytesSent += static_cast<int>(FinalSize);
    m_PacketsSent++;

    return Err;
}
                                     
//------------------------------------------------------------------------------

void xnet_reliable_protocol::HandleAcknowledge( t_hsocket hSocket, remote_sequence RemoteSequence, u32 RemoteAckBits ) noexcept
{
    // Now remove any pending to ack packets
    s32 TotalAck     = 0;

    // Note that this needs to be set into a local variable 
    // because the resent of the packet will up the m_totalinqueue
    // and we do not want to go though those 
    s32 TotalInQueue = m_TotalInQueue;
    for( s32 i=0; i<TotalInQueue; i++ )
    {
        pending_packet& Pending = *m_PendingQueue[i];

        // Find the actual distance between the two count
        s32 Distance;
        if( RemoteSequence.m_Value < Pending.m_Sequence.m_Value ) 
        {
            const s32 Distance1 = Pending.m_Sequence.m_Value - RemoteSequence.m_Value;
            const s32 Distance2 = (RemoteSequence.m_Value + BASE_COUNTER_MASK) - Pending.m_Sequence.m_Value;
            if( Distance2 < Distance1 ) Distance = Distance2; 
            else 
            {
                // TODO: Try to make sense of the comment below... not sure why I added right now.
                //       also make a better description of it.

                //
                // The vector of the ack bits don't overlap the vector of the pending packages
                //                                           In queue packet
                //     ack_bits    RemoteSequence                   Pending.m_Sequeue
                //     -------------------->*        distance      <*>
                //                          |<--------------------->|
                //
#ifdef NETWORKDEBUG
                X_NET_LOGGER( "[1]Pending packet can not be ack because the remote sequence starts at " << RemoteSequence.m_Value << " and the pending packet is at " << Pending.m_Sequence.m_Value );
#endif
                continue;
            }
        }
        else                        
        {
            const s32 Distance1 = RemoteSequence.m_Value - Pending.m_Sequence.m_Value;
            const s32 Distance2 = (Pending.m_Sequence.m_Value + BASE_COUNTER_MASK) - RemoteSequence.m_Value;
            if( Distance1 < Distance2 ) Distance = Distance1;
            else
            {
                //
                // The vector of the ack bits don't overlap the vector of the pending packages
                //
                //     ack_bits    RemoteSequence                  Pending.m_Sequeue
                //     -------------------->*        distance      <*>
                //                          |<--------------------->|
                //
#ifdef NETWORKDEBUG
                X_NET_LOGGER( "[2]Pending packet can not be ack because the remote sequence starts at " << RemoteSequence.m_Value " and the pending packet is at" << Pending.m_Sequence.m_Value );
#endif
                continue;
            }
        }

        //
        // Check the ack bits to see if we have ack this packet
        //
        if( Distance < 32 && (1&( RemoteAckBits >> Distance )) )
        {
            x_assert( Distance < 32 );
            //X_NET_LOGGER( "confirm with remote sequence " << RemoteSequence.m_Value " pending packet sequence " << Pending.m_Sequence.m_Value << " reliable sequence " << *((u16*)&Pending.m_Data[0]) );

            // Insert the pending packet in the empty list
            Pending.m_pNext         = m_pReliablePoolFreeList;
            m_pReliablePoolFreeList = &Pending;

            // Mark the entry as empty
            m_PendingQueue[i] = nullptr;
            TotalAck++;
        }
        // We will auto acknowledge packets that are too old.
        // If we waited to 12 packets probably is time to send it again
        // assume that a packet gets send at least 10 packets per second
        // TODO: This should be better done with time rather than packets
        else if( Distance > MAX_PACKETS_WAIT_FOR_RESENT )
        {
#ifdef NETWORKDEBUG
//            ASSERT( Distance < 32 );
            x_assert( ((RemoteAckBits>>Distance)&1) == 0 );
#endif
            //
            // Send a completely new package hopefully this one will get ack
            //
            m_nResentPackets++;
            SendReliablePacket( hSocket, xbuffer_view<xbyte>{ Pending.m_Data, static_cast<xuptr>(Pending.m_Size) }, Pending.m_Protocol ).IgnoreError();
            
#ifdef NETWORKDEBUG
            X_NET_LOGGER( "Resending Packet with distance " << Distance << " Total resent " << m_nResentPackets );
#endif
            //
            // Respect to the old one we will assume it got ack from the other client
            // in essence we are going to ignore future ack for it.
            //

            // Insert the pending packet in the empty list
            Pending.m_pNext = m_pReliablePoolFreeList;
            m_pReliablePoolFreeList = &Pending;

            // Mark the entry as empty
            m_PendingQueue[i] = nullptr;
            TotalAck++;
        }

    }
    
    //
    // Collapse the queue
    //
    if( TotalAck > 0 )
    {
        s32 s=0;
        for( s32 i=0; i<m_TotalInQueue; i++ )
        {
            m_PendingQueue[s] = m_PendingQueue[i];
            if( m_PendingQueue[s] ) s++;
        }

        // Clear the last one
        m_PendingQueue[s] = nullptr;

        // Set the new total
        x_assert( m_TotalInQueue == (s+TotalAck) );
        m_TotalInQueue = s;
    }
}

//------------------------------------------------------------------------------

xnet_reliable_protocol::err xnet_reliable_protocol::onRecivePacket( t_hsocket hSocket, xbuffer_view<xbyte> Packet, s32& Size, remote_sequence Sequence ) noexcept
{
    Size = 0;

    //
    // Make sure our size if sufficiently big
    //
    if( Packet.getCount() < (BASE_COUNTER_BYTES+4) )
    {
        X_NET_LOGGER( "We have receive a reliable packet with less size than the protocol requires " << Packet.getCount() );
        return x_error_code( errors, ERR_FAILURE, "Packet size was too small!" );
    }

    //
    // Deal with old packets and recivos
    //
    if( m_RealibleSequence.m_Value >= Sequence.m_Value ) 
    {
#ifdef NETWORKDEBUG
        if( m_RealibleSequence.m_Value > Sequence.m_Value )
        {
            X_NET_LOGGER( "I got an packet out of order (old) with a sequence of " << Sequence.m_Value << " and we are at " << m_RealibleSequence );
        }
#endif
        const s32 Distance = m_RealibleSequence.m_Value - Sequence.m_Value;
        if( Distance < 32 )
        {
            if( (1<<Distance) & m_RecivedPacketsMask )
            {
                X_NET_LOGGER( "I already got this package! It is a sequence " << Sequence.m_Value << " and we are at " << m_RealibleSequence.m_Value );
                return x_error_code( errors, ERR_FAILURE, "I already got this package!" );
            }
            else
            {
                m_RecivedPacketsMask |= (1<<Distance);
            }
        }
        else
        {
            // If a reliable packet is so old that we can not send a recivo then just pretend never got here
            X_NET_LOGGER( "I got a packet that is so old that I can not send a recivo for it. It is a sequence " << Sequence.m_Value << " and we are at " << m_RealibleSequence.m_Value );
            return x_error_code( errors, ERR_FAILURE, "I got a packet that is so old that I can not send a recivo for it." );
        }
    }
    else
    {
        // Update the acknowledge recivo mask
        const s32 Distance = Sequence.m_Value - m_RealibleSequence.m_Value;

        if( Distance >= 32 )
        {
            X_NET_LOGGER( "The remote machine is far ahead of us,  It is at sequence " << Sequence.m_Value << " and we are at " << m_RealibleSequence.m_Value );
        }
/*
        else if( (~m_RecivedPacketsMask)>>(32-Distance))
        {
#ifdef NETWORKDEBUG
            x_LogError( NETLOG, "We are sifting bits that can not longer be acknowledge. Distance %d", Distance );
#endif
        }
*/

        //
        // Sync counters
        //

        // Sync up with host computer
        m_RecivedPacketsMask <<= Distance;
        m_RecivedPacketsMask  |= 1;

        // Update our internal sequence
        m_RealibleSequence.m_Value = Sequence.m_Value;
    }

    //
    // Should we send a packet to acknowledge stuff?
    //
    m_bForcePing = true;


    // Correct for the header
    const u32 RemoteAckBits  = x_NetworkEndian( ((u32*)&Packet[0])[1] );

    Size = Packet.getCount<int>() - (BASE_COUNTER_BYTES + 4);
    x_assert( Size >= 0 );

    Packet.CopyToFrom( Packet.ViewFrom<int>( BASE_COUNTER_BYTES + 4 ) );
    //x_memmove( &Packet[0], &Packet[ BASE_COUNTER_BYTES + 4 ], Size );

    //
    // Deal with the Acknowledge
    //
    HandleAcknowledge( hSocket, Sequence, RemoteAckBits );

    return x_error_ok( errors );
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// IN ORDER PROTOCOL
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

#define IN_ORDER_TYPE   u16
#define IN_ORDER_BYTES (sizeof(IN_ORDER_TYPE))
#define IN_ORDER_BITS  (IN_ORDER_BYTES*8)
#define IN_ORDER_MASK  ((1<<IN_ORDER_BITS)-1)

//------------------------------------------------------------------------------

void xnet_inorder_protocol::vResetClient( void ) noexcept
{
    xnet_reliable_protocol::vResetClient();

    m_pPendingInOrder       = nullptr;
    m_pPendingInOrderLast   = nullptr;
    m_InOrderLocalSequence  = local_sequence{ 0 };
    m_InOrderRemoteSequence = remote_sequence{ 0 };
}

//------------------------------------------------------------------------------

xnet_socket::err xnet_inorder_protocol::SendInOrderPacket( t_hsocket hSocket, xbuffer_view<xbyte> Buffer ) noexcept
{
    x_assert( Buffer.getCount() < MAX_INORDER_PACKET_SIZE );

    const s32   HeaderSize   = IN_ORDER_BYTES;
    const s32   FinalSize    = Buffer.getCount<int>() + HeaderSize;
    xbyte*      pFinalPacket = reinterpret_cast<xbyte*>(x_alloca(FinalSize));
    x_assume( pFinalPacket );

    x_memcpy( &pFinalPacket[HeaderSize], &Buffer[0], Buffer.getCount() );

    // Save the sequence
    (*(IN_ORDER_TYPE*)pFinalPacket)  = x_NetworkEndian( (IN_ORDER_TYPE)m_InOrderLocalSequence.m_Value );
    m_InOrderLocalSequence           = local_sequence{ IN_ORDER_MASK & (m_InOrderLocalSequence.m_Value+1) };

    // Ok now send the packet   
    return SendReliablePacket( hSocket, xbuffer_view<xbyte>{ pFinalPacket, static_cast<xuptr>(FinalSize) }, protocol::INORDER );
}

//------------------------------------------------------------------------------
 
xnet_inorder_protocol::err xnet_inorder_protocol::onRecivePacket( t_hsocket hSocket, xbuffer_view<xbyte> Packet, s32& Size, remote_sequence Sequence )  noexcept
{
    s32 FinalSize = Size;

    // Get the packet
    auto Err = xnet_reliable_protocol::onRecivePacket( hSocket, Packet, FinalSize, Sequence );
    if( Err ) return Err;

    // Make sure our size if sufficiently big
    if( FinalSize < IN_ORDER_BYTES )
    {
        X_NET_LOGGER( "We have receive a in order packet with less size than the protocol requires " << FinalSize );
        return x_error_code( errors, ERR_FAILURE, "We have receive a in order packet with less size than the protocol requires " );
    }

    // Get the order
    const remote_sequence RemoteOrder = remote_sequence{ x_NetworkEndian( *(IN_ORDER_TYPE*)&Packet[0] ) };
    FinalSize -= IN_ORDER_BYTES;

    // Deal with the easy case first
    if( m_InOrderRemoteSequence == RemoteOrder ) 
    {
/*
        // This if deals with duplicates which just happen to arrive while
        // we were already in the process of collecting the packets in the recived queue
        // very rare case but still happens.
        if( m_pPendingInOrderLast )
        {
            // Here as you can see we check for the duplicates
            if( m_pPendingInOrderLast->m_Sequence == m_InOrderRemoteSequence )
            {
                x_LogError( NETLOG, "We have recive the same packet again just as we were collecting the pending packets throwing the duplicate out (%d)", RemoteOrder );

                // if we already had the packet then just ignore the new one
                if(m_pPendingNext == NULL ) 
                    AddClientInPendingList();

                // Tell the system that nothing really new has arrived
                return FALSE;
            }
        }
*/
        // Set the final package
        Size = FinalSize;
        Packet.CopyToFrom( Packet.ViewFrom( IN_ORDER_BYTES ) );
        //x_memmove( &Packet[0], &Packet[IN_ORDER_BYTES], FinalSize );

        // Get ready for the next packet
        m_InOrderRemoteSequence = remote_sequence{ IN_ORDER_MASK & (m_InOrderRemoteSequence.m_Value + 1 ) };

        // Has this packet unlock pending packages if so then tell the mesh to contact us
        if( m_pPendingInOrderLast )
        {
            if( m_pPendingInOrderLast->m_Sequence.m_Value == m_InOrderRemoteSequence.m_Value )
                AddClientInPendingList();
        }

        return x_error_ok( errors );
    }

    //
    // Mark if the sequence rolled
    //
    bool            bRolled = false;

    // Mark whether we have roll our count or not
    {
       s32 Diff = x_Abs(RemoteOrder.m_Value - m_InOrderRemoteSequence.m_Value);
       if( Diff > IN_ORDER_MASK/2) 
       {
           bRolled = true;
       }
    }

    // Deal with old packets that come out of sequence
    // Possible duplication and old at the same time
    if( RemoteOrder < m_InOrderRemoteSequence )
    {
        const auto Diff = m_InOrderRemoteSequence - RemoteOrder;
        if( bRolled )
        {
            if( Diff.m_Value < IN_ORDER_MASK/2 )
            {
                X_NET_LOGGER( "[1]We have receive an in-order packet that is too old " << RemoteOrder.m_Value << " distance " << Diff.m_Value );
                return x_error_code( errors, ERR_FAILURE, "[1]We have receive an in-order packet that is too old " );
            }
        }
        else
        {
            X_NET_LOGGER( "[2]We have receive an in-order packet that is too old " << RemoteOrder.m_Value << " distance " << Diff.m_Value );
            return x_error_code( errors, ERR_FAILURE, "[2]We have receive an in-order packet that is too old " );
        }
    }

    //x_LogMessage( NETLOG, "[%x]InOrderProtocol: Waiting for packet (%d)", this, m_InOrderRemoteSequence );

    // Make sure that there is at least one free node in the list.
    // if not most likely the connection is really bad so we will be force to reset
    if( m_pReliablePoolFreeList == nullptr )
    {
        X_NET_LOGGER( "We have run out of memory buffering packages that are out of order in the reliable protocol" );
        return x_error_code( errors, ERR_FATAL, "Out of memory in the reliable protocol" );
    }

    // Ok so we are not marching in order so lets insert this packet into 
    // First lets create a packet
    // Get a free pending packet
    pending_packet& PendingPacket = *m_pReliablePoolFreeList;
    m_pReliablePoolFreeList = m_pReliablePoolFreeList->m_pNext;

    // Fill the pending packet
    PendingPacket.m_Protocol = protocol::BASE;
    PendingPacket.m_Sequence = local_sequence{ RemoteOrder.m_Value };
    PendingPacket.m_pNext    = nullptr;
    PendingPacket.m_Size     = FinalSize;
    PendingPacket.m_Data.CopyToFrom( Packet.ViewFrom( IN_ORDER_BYTES ) );
    
    // Now insert it in our queue
    pending_packet*  pNext   = m_pPendingInOrder;
    pending_packet*  pPrev   = NULL;

    // Insert the node in the right place in the list
    for( ; pNext; pNext = pNext->m_pNext )
    {
        // Find a place to insert it
        if( pNext->m_Sequence == PendingPacket.m_Sequence )
        {
            X_NET_LOGGER( "We have receive the same packet again throwing the duplicate out " << PendingPacket.m_Sequence.m_Value );

            // Insert our packet back into the free pool
            PendingPacket.m_pNext   = m_pReliablePoolFreeList;
            m_pReliablePoolFreeList = &PendingPacket;

            return x_error_code( errors, ERR_FAILURE, "We have receive the same packet again throwing the duplicate out" );
        }
        else if( bRolled )
        {
            // we are in the same quadrant as the pending packet
            if( pNext->m_Sequence.m_Value < IN_ORDER_MASK/2 )
            {
                //smaller units go first
                if( pNext->m_Sequence < PendingPacket.m_Sequence ) 
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        else if( (PendingPacket.m_Sequence - pNext->m_Sequence) > local_sequence{ IN_ORDER_MASK/2 } )
        {
            // keep going we are not dealing with the same range...
            // Since when we roll over the smaller numbers are first even though they
            // mean that they are they future rolled over sequence.
        }
        else if( pNext->m_Sequence < PendingPacket.m_Sequence ) 
        {
            // TODO: for making this faster We should have the sequence in the same space
            // We want the newer packets to be at the front
            break;
        }

        // Save where we would need to insert the node
        pPrev  = pNext;
    }

    // update the node
    PendingPacket.m_pNext = pNext;
    PendingPacket.m_pPrev = pPrev;

    // update neighbors
    if( pNext ) 
        pNext->m_pPrev = &PendingPacket;

    if( pPrev ) 
        pPrev->m_pNext = &PendingPacket;

    // Insert our packet into the actual queue
    if( pPrev == nullptr )
        m_pPendingInOrder = &PendingPacket;

    // If the new node is the last in the sequence we put the tail there
    if( pNext == nullptr )
        m_pPendingInOrderLast = &PendingPacket;

// Insert the node in the right place in the list
// This code is just for debugging
/*
if( 1 )
{
    for( pending_packet*  pNext = m_pPendingInOrder; pNext; pNext = pNext->m_pNext )
    {
        ASSERT(m_InOrderRemoteSequence <= pNext->m_Sequence ); 
        if( pNext->m_pNext ) 
        {
            ASSERT(pNext->m_Sequence != pNext->m_pNext->m_Sequence );
            ASSERT(pNext->m_Sequence > pNext->m_pNext->m_Sequence );
        }
        else
        {
            ASSERT( m_pPendingInOrderLast == pNext );
        }
    }
}
*/
    return  x_error_code( errors, ERR_FAILURE, "We need to wait for the proper order" );
}

//------------------------------------------------------------------------------

bool xnet_inorder_protocol::getPendingPacketData(  xbuffer_view<xbyte> Packet, s32& Size ) noexcept
{
    // If our queue empty? if so there is nothing to do
    x_assert( m_pPendingInOrder );
    x_assert( m_InOrderRemoteSequence.m_Value == m_pPendingInOrderLast->m_Sequence.m_Value );

    // Okay have we receive the packet that we were missing?
    pending_packet& PendingPacket = *m_pPendingInOrderLast;

    Size = PendingPacket.m_Size;
    Packet.CopyToFrom( PendingPacket.m_Data.ViewTo(Size) );
    //x_memcpy( pData, PendingPacket.m_Data, Size );

    // Move forward to the next entry
    m_pPendingInOrderLast = m_pPendingInOrderLast->m_pPrev;
    if( m_pPendingInOrderLast ) 
        m_pPendingInOrderLast->m_pNext = nullptr;
    else
        m_pPendingInOrder = nullptr;

    // Insert our packet back into the free pool
    PendingPacket.m_pNext           = m_pReliablePoolFreeList;
    m_pReliablePoolFreeList         = &PendingPacket;

    // Get ready for the next packet
    m_InOrderRemoteSequence = remote_sequence{ IN_ORDER_MASK & ( m_InOrderRemoteSequence.m_Value + 1 ) };

    // If we have more unlocked packages we should tell the mesh
    if( m_pPendingInOrderLast )
    {
        if( m_pPendingInOrderLast->m_Sequence.m_Value == m_InOrderRemoteSequence.m_Value )
            return true;
    }

    return false;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// UDP CLIENT
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

void xnet_udp_client::vResetClient( void ) noexcept
{
    xnet_inorder_protocol::vResetClient();
}

//------------------------------------------------------------------------------

void xnet_udp_client::CertifyClient( void ) noexcept
{ 
    // Sorry if it already certify it is because somehow got disconnected
    // and probably is trying to connect again if that is the case we need
    // to have a hard reset
    if( m_ClientType == TYPE_CERTIFIED )
    {
        vResetClient();
    }

    m_ClientType = TYPE_CERTIFIED; 
}

//------------------------------------------------------------------------------

xnet_udp_client::err xnet_udp_client::HandleRecivePacket( t_hsocket hSocket, xbuffer_view<xbyte> Packet, s32& Size ) noexcept
{
    x_assert( Size > 4 );
    
    const auto Protocol = getProtocol( Packet );
    remote_sequence Sequence;

    switch( Protocol )
    {
        default:
        case protocol::BASE: x_assert(0); break;

        case protocol::NONRELIABLE: 
        {
            // Get information about the packet
            auto Err = getInfoPacketHeader( local_sequence{ m_NonRealibleRemoteSequence.m_Value }, Packet, Sequence );
            if( Err ) return Err;

            return xnet_nonreliable_protocol::onRecivePacket( Packet, Size, Sequence );
        }
        case protocol::RELIABLE:
        { 
            // Get information about the packet
            auto Err = getInfoPacketHeader( m_RealibleSequence, Packet, Sequence );
            if( Err ) return Err;

            return xnet_reliable_protocol::onRecivePacket( hSocket, Packet, Size, Sequence );
        }
        case protocol::INORDER: 
        {
            // Get information about the packet
            auto Err = getInfoPacketHeader( m_RealibleSequence, Packet, Sequence );
            if( Err ) return Err;

            return xnet_inorder_protocol::onRecivePacket( hSocket, Packet, Size, Sequence );
        }
    }

    // Everything is ok
    return x_error_ok( errors );
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// UDP MESH
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------

xnet_udp_mesh::t_hclient xnet_udp_mesh::findClient( const xnet_address& FromAddr ) noexcept
{
    for( xuptr i=0; i<m_lClient.getCount(); i++ )
    {
        xnet_udp_client& Client = m_lClient[i];

        if( Client.getAddress() == FromAddr )
            return m_lClient.getHandleByIndex(i);
    }

    return t_hclient{ nullptr };
}

//------------------------------------------------------------------------------

xnet_udp_mesh::err xnet_udp_mesh::getSocketPackets(
    t_hsocket           hLocalSocket,
    xbuffer_view<xbyte> Packet, 
    s32&                outSize, 
    t_hclient&          outhClient, 
    bool                bBlock ) noexcept
{
    outhClient.setNull();

    //
    // Try to Read packets from the client in-order protocol
    // This needs to be here first if not we get strange bugs because we are
    // trying to get this sequence while new packets keep arriving.
    // This also has the positive side effects that frees as many nodes as it can there by saving memory first
    //
    if( m_pPendingPacketClientList )
    {
        // Set the client so the user may know
        outhClient = findClient( m_pPendingPacketClientList->m_ConnectionAddress );
        if( false == m_pPendingPacketClientList->getPendingPacketData( Packet, outSize ) )
        {
            // It has not more packets after this one
            // so remove it from the list
            xnet_base_protocol* pT     = m_pPendingPacketClientList; 
            m_pPendingPacketClientList = m_pPendingPacketClientList->m_pPendingNext;
            pT->m_pPendingNext         = nullptr;
        }

        return x_error_ok( xnet_udp_mesh::errors );
    }

    //
    // Try to read a packet from the socket
    //
    xnet_socket&    Socket      = m_SocketList( hLocalSocket );
    xnet_address    FromAddr;

    do
    {
        //
        // Try to get data from the socket
        //
        {
            auto Err = Socket.ReadFromSocket( Packet, outSize, FromAddr, bBlock );
            if( Err )
            {
                Err.IgnoreError();
                return x_error_code( errors, ERR_SOCKET, "Socket Error" );
            }

            // If we have not data then lets move on
            if( outSize == 0 )
            {
                // Tell the user we got nothing
                outSize = -1;
                break;
            }
            x_assert( outSize >= 0 );
        }

        //
        // Find Client with the specific address
        //
        outhClient = findClient( FromAddr );

        // If can not find the client the client must communicate with the reliable protocol only at this point
        if( outhClient.isNull() )
        {
            // All initial connections must use the reliable protocol only
            // and the size should definitely larger than just a ping
            if( xnet_base_protocol::getProtocol(&Packet[0]) == xnet_base_protocol::protocol::RELIABLE && outSize > (4+4) )
            {
                // We will create a client and let the user decide whether he wants to certify him
                outhClient = CreateClient( FromAddr );

                // if we have an error creating the client we will pretend we never got the packet
                if( outhClient.isNull() )
                {
                    // Visual studio 2015 is crashing with this line
                    //      X_NET_LOGGER( "Fail to connect a client due to CreateClient failure, computer " << (const char*)FromAddr.getStrAddress() << " will be ignore for now" );
                    X_NET_LOGGER( "Fail to connect a client due to CreateClient failure, computer " << "????" << " will be ignore for now" );
                    continue;
                }
            }
            else
            {    
                // Visual studio 2015 is crashing with this line
                //      X_NET_LOGGER( "A computer " << (const char*)FromAddr.getStrAddress() << " address try to connect using the wrong protocol" );
                X_NET_LOGGER( "A computer " << "???????" << " address try to connect using the wrong protocol" );
                continue;
            }
        }

        //
        // Notify the client that a packet has arrived
        //
        xnet_base_protocol& Client = m_lClient( outhClient );
        auto Err = Client.HandleRecivePacket( hLocalSocket, Packet.ViewTo( outSize ), outSize );
        if( Err ) return Err;

        // Reset the timer since we have received data
        Client.m_TimeSinceLastRecived.Trip();

        //
        // If we got a packet that is too small then there is nothing to do
        // Probably a heart beat
        //
        x_assert( outSize >= 0 );
        if( outSize == 0 )
        {
            // Nothing for the user to do
            outSize = -1;
        }
        else
        {
            // we have a valid packet for the user
            return x_error_ok( errors );
        }


    } while( true );

    //
    // Give a chance to the clients to update such ack packets
    //
    for( s32 i=0; i<m_lClient.getCount(); i++ )
    {
        // Update the heart-beat and such
        auto Err = m_lClient[i].Update( hLocalSocket );
        if( Err ) return Err;
    }
    outSize     = -1;
    outhClient.setNull();

    //
    // Do the lag simulator
    //
    for( s32 i=0; i<m_SocketList.getCount(); i++ )
    {
        auto& Socket = m_SocketList[i];
        if( Socket.m_NetworkTester.isValid() ) Socket.m_NetworkTester->Update();
    }

    //
    // Output how many packets we have left
    //
#ifdef NETWORKDEBUG
    if( 1 )
    {
        struct pending_packet
        {
            s32                         m_Protocol;
            s32                         m_Sequence;
            s32                         m_Size;
            pending_packet*             m_pNext;
            pending_packet*             m_pPrev;
            xsafe_array<xbyte,1024>     m_Data;
        };

        //
        // First check how many free packets left
        // 
        for( s32 i=0; i<m_lClient.getCount(); i++ )
        {
            xnet_udp_client& Client = m_lClient[i];
            s32          Count=0;

            for( pending_packet* pNext = (pending_packet*)Client.m_pReliablePoolFreeList;
                                 pNext;
                                 pNext = pNext->m_pNext ) Count++;

            //x_LogMessage( NETLOG, "Client[%X] has (%d) Free", &Client, Count );
            
            //if( Count < 10 ) BREAK;
        }
    }
#endif

    return x_error_ok( errors );
}

//------------------------------------------------------------------------------

xnet_udp_mesh::t_hclient xnet_udp_mesh::CreateClient( xnet_address Address ) noexcept
{
    // Make sure that we don not have this client already
    t_hclient hClient = findClient( Address );
    x_assert( hClient.isNull() );
    
    // create a new client
    xnet_udp_client& Client = m_lClient.pop( hClient );
    Client.setupConnection( Address, *this );

    // Ok we are done
    return hClient;
}

//------------------------------------------------------------------------------

xnet_udp_mesh::t_hsocket xnet_udp_mesh::openSocket( xnet_address::port Port ) noexcept
{
    return FindOrCreateSocket( Port );
}


//------------------------------------------------------------------------------

xnet_udp_mesh::t_hsocket xnet_udp_mesh::FindOrCreateSocket( xnet_address::port Port ) noexcept
{
    t_hsocket hSocket;
   
    //
    // Find Socket
    //
    hSocket.setNull();
    for( s32 i=0; i<m_SocketList.getCount(); i++ )
    {
        if( Port == m_SocketList[i].getPort() )
        {
            m_SocketList[i].AddRef();
            hSocket = m_SocketList.getHandleByIndex(i);
            return hSocket;
        }
    }

    //
    // Create Socket
    //
    xnet_socket& Socket = m_SocketList.pop( hSocket );
    auto Err = Socket.openUDPSocket( Port );
    if( Err )
    {
        Err.IgnoreError();
        m_SocketList.push( hSocket );
        hSocket.setNull();
        return hSocket;
    }

    // Everything when okay
    Socket.AddRef();
    return hSocket;
}

//------------------------------------------------------------------------------

xnet_socket::err xnet_udp_mesh::WriteToSocket( t_hsocket hLocalSocket, xbuffer_view<xbyte> Buffer, xnet_address Address ) noexcept
{
    return m_SocketList(hLocalSocket).SendPacket( Buffer, Address );
}

//------------------------------------------------------------------------------

void xnet_udp_mesh::DeleteClient( t_hclient hClient ) noexcept
{
    xnet_udp_client& Client = m_lClient( hClient );

    // Disconnect the client first

    //
    // Remove the client from the pending client queue
    //
    {
        xnet_base_protocol* pPrev = nullptr;
        for( xnet_base_protocol* pNext = m_pPendingPacketClientList; pNext; pNext = pNext->m_pPendingNext )
        {
            if( pNext == &Client )
            {
                if( pPrev )
                {
                    pPrev->m_pPendingNext = pNext->m_pPendingNext;
                }
                else
                {
                    m_pPendingPacketClientList = pNext->m_pPendingNext;
                }
                break;
            }
            pPrev = pNext;
        }
    }

    //
    // Now we can delete him
    //
    m_lClient.push( hClient ); 
}

