//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// VECTOR BASE
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
template< typename A > inline
void xvector_base<A>::Kill( void ) noexcept
{
    for( int i = 0; i < static_cast<int>(m_Count); i++ )
    {
        x_destruct( &m_pMem[ i ] );
    }
    m_Count = 0;
}


//---------------------------------------------------------------------------------------
template< typename A > inline
void xvector_base<A>::GrowBy( const t_counter NewItems ) noexcept
{
    if( NewItems == 0 )
        return;

    //
    // Allocate the new count
    //
    const auto CurCount = m_pMem.getCount();
    if( CurCount == 0 )
    {
        m_pMem.Alloc( NewItems );
    }
    else
    {
        const auto NewCapacity = m_Count + NewItems;
        m_pMem.Resize( NewCapacity );
    }
}

//------------------------------------------------------------------------------
template< typename A > inline
void xvector_base<A>::DeleteAllEntries( void ) noexcept
{
    for( t_counter i=0; i<m_Count; ++i )
    {
        x_destruct( &m_pMem[i] );
    }

    m_Count = 0;
}

//------------------------------------------------------------------------------
template< typename A > inline
void xvector_base<A>::DeleteWithCollapse( const t_counter Index, const t_counter Count ) noexcept
{
    if( m_Count <= 0)
        return;
    
    x_assert( Index >= 0 );
    x_assert( (Index+Count) <= m_Count );
    
    //
    // Lets free all these guys
    //
    for( t_counter i=0; i<Count; ++i )
    {
        x_destruct( &m_pMem[Index + i] );
    }
    
    //
    // Okay if there is any full node lets move him down
    //
    const t_counter LastMovedIndex = (Index+Count);
    const t_counter ItemsToMove    = m_Count - LastMovedIndex;

    x_assert( ItemsToMove <= m_Count );
    if( ItemsToMove > 0 ) m_pMem.CopyToFrom( t_view( &m_pMem[LastMovedIndex], ItemsToMove ), Index );
    
    //
    // update the count
    //
    m_Count -= Count;
}

//------------------------------------------------------------------------------
template< typename A > inline
void xvector_base<A>::DeleteWithSwap( const t_counter Index, const t_counter Count ) noexcept
{
    if( m_Count <= 0)
        return;
    
    x_assert( Index >= 0 );
    x_assert( (Index+Count) <= m_Count );

    //
    // Lets free all these guys
    //
    for( t_counter i=0; i<Count; ++i )
    {
        x_destruct( &m_pMem[Index + i] );
    }
    
    //
    // Copy all the nodes from the end to where we are
    //
    if( (Index + Count) < m_Count )
    {
        const auto iSwap = m_Count - 1;
        for( t_counter i = 0; i<Count; i++ )
        {
            m_pMem[ Index + i ] = m_pMem[ iSwap - i ];
        }
    }
    
    //
    // update the count
    //
    m_Count -= Count;
}

//---------------------------------------------------------------------------------------
// Will copy/replace any code in the region been asked to copy, will change its size if it needs to
template< typename A > inline
void xvector_base<A>::CopyToFromDynamically( const t_view View, t_counter DestinationOffset ) noexcept
{
    const auto NewMaxCount = x_Max( m_Count, DestinationOffset + View.getCount() );

    //
    // Delete thecesary entries 
    //
    const auto HoleCount = x_Min( m_Count, DestinationOffset + View.getCount() );
    for( auto i = DestinationOffset; i<HoleCount; i++ )
    {
        x_destruct( &m_pMem[i] );
    }

    //
    // Grow if we need to
    //
    if( m_pMem.getCount() < NewMaxCount )
    {
        const auto GrowCount = x_Max( DEFAULT_GROWTH, NewMaxCount - m_pMem.getCount() );
        GrowBy( GrowCount );
    }

    //
    // Set the new count
    //
    m_Count = View.getCount() + DestinationOffset;

    //
    // Copy all the entires
    //
    if( std::is_pod<t_entry>::value )
    {
        m_Count = View.getCount();
        x_memcpy( &m_pMem[DestinationOffset], &View[0], sizeof(t_entry) * View.getCount() );
    }
    else
    {
        t_counter i=0;
        for( const auto& Entry : x_iter_ref( i, View ) )
            x_construct( t_entry, &m_pMem[DestinationOffset + i], { Entry } );
    }
}

//---------------------------------------------------------------------------------------
template< typename A > inline
void xvector_base<A>::InsertList( const t_counter Index, const t_counter Count ) noexcept
{
    x_assert( Index >= 0 );
    x_assert( Index <= m_Count );
    
    //
    // Make sure that we have space
    //
    x_assert( static_cast<t_counter>(m_Count + Count) < std::numeric_limits<t_counter>::max() );

    const t_counter NewCount = m_Count + Count;
    if( NewCount >= m_pMem.getCount() )
        GrowBy( x_Max( NewCount/2, Count + DEFAULT_GROWTH ) );

    //
    // Okay lets move all the elements
    //
    const auto ItemsToMove       = m_Count - Index; 
    const auto NewPosOldItems    = Index + Count; 
    if( ItemsToMove > 0 ) m_pMem.CopyToFrom( t_view( &m_pMem[Index], ItemsToMove ), NewPosOldItems );
    
    //
    // Lets construct
    //
    for( t_counter i = Index; i<NewPosOldItems; ++i )
        (void)x_construct( t_entry, &m_pMem[i] );
    
    //
    // Finally set the new count
    //
    m_Count = NewCount;
}

//---------------------------------------------------------------------------------------
template< typename A > inline
void xvector_base<A>::InsertList( const t_counter Index, const t_view View ) noexcept
{
    x_assert( Index >= 0 );
    x_assert( Index <= m_Count );
    
    //
    // Make sure that we have space
    //
    x_assert( static_cast<s64>(m_Count + View.getCount()) < std::numeric_limits<t_counter>::max() );

    const auto NewCount = m_Count + View.getCount();
    if( NewCount >= m_pMem.getCount() )
        GrowBy( x_Max( NewCount/2, View.getCount() + DEFAULT_GROWTH ) );
        
    //
    // Okay lets move all the elements
    //
    const auto ItemsToMove       = m_Count - Index; 
    const auto NewPosOldItems    = Index + View.getCount();
    if( ItemsToMove > 0 ) m_pMem.CopyToFrom( t_view( &m_pMem[NewPosOldItems], ItemsToMove ), Index );
    
    //
    // Lets copy construct all the new items
    //
    t_counter i=0;
    for( const auto& Entry : x_iter_ref( i, View ) )
        x_construct( t_entry, &m_pMem[Index+i], { Entry } );
    
    //
    // Finally set the new count
    //
    m_Count = NewCount;
}

//---------------------------------------------------------------------------------------
template< typename A > 
template<typename ...T_ARG> inline
void xvector_base<A>::appendList( const t_counter Count, T_ARG&&...Args ) noexcept
{
    //
    // Make sure that we have memory
    //
    if( Count > (m_pMem.getCount() - m_Count) )
        GrowBy( x_Max( (m_pMem.getCount() + Count)/2, Count + DEFAULT_GROWTH ) );

    //
    // Construct all of them
    //
    const auto FinalCount = m_Count + Count; 
    for( auto i=m_Count; i<FinalCount; i++ )
    {
        (void)x_construct( t_entry, &m_pMem[i], { Args... } );
    }
    
    // Set the new count and return
    m_Count = FinalCount;
}

//---------------------------------------------------------------------------------------
template< typename A > inline
void xvector_base<A>::appendList( const t_view View ) noexcept
{
    const int Index = getCount();
    
    //
    // Make sure that we have memory
    //
    if( View.getCount() > (getCount() - m_Count) )
        GrowBy( x_Max( (getCount()+View.getCount())/2, View.getCount() + DEFAULT_GROWTH ) );

    //
    // Construct all of them
    //
    t_counter i = 0;
    for( const auto& Entry : x_iter_ref( i, View ) )
        x_construct( t_entry, &m_pMem[Index+i], { Entry } );
    
    // Set the new count and return
    m_Count += View.getCount();
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// PAGE VECTOR BASE
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D >
template< typename I > x_forceinline
typename xphvector_base<A,B,C,D>::t_entry& xphvector_base<A,B,C,D>::operator[] ( const I index ) noexcept
{ 
    return (*this)( m_Iterator[index].m_hEntry );
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D >
template< typename I > x_forceconst 
const typename xphvector_base<A,B,C,D>::t_entry& xphvector_base<A,B,C,D>::operator[] ( const I index ) const noexcept
{ 
    return (*this)( m_Iterator[index].m_hEntry );
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D > x_inline 
typename xphvector_base<A,B,C,D>::t_entry& xphvector_base<A,B,C,D>::operator() ( const t_handle Handle  ) noexcept
{
    const s32     iPage   = Handle.m_Value >> HANDLE_PAGE_SHIFT;
    const s32     iEntry  = Handle.m_Value & HANDLE_ENTRY_MASK;
    page&         Page    = m_lPages[ iPage ];
    t_entry&      Entry   = Page.m_Entry[ iEntry ];

    return Entry;
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D > x_inline 
const typename xphvector_base<A,B,C,D>::t_entry& xphvector_base<A,B,C,D>::operator() ( t_handle Handle  ) const noexcept
{
    const s32     iPage   = Handle.m_Value >> HANDLE_PAGE_SHIFT;
    const s32     iEntry  = Handle.m_Value & HANDLE_ENTRY_MASK;
    page&         Page    = m_lPages[ iPage ];
    t_entry&      Entry   = Page.m_Entry[ iEntry ];

    return Entry;
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D > x_forceconst 
typename xphvector_base<A,B,C,D>::t_counter xphvector_base<A,B,C,D>::getIndexByHandle( t_handle Handle ) const noexcept
{
    return m_Iterator[ Handle.m_Value ].m_iHandle;
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D > 
template< typename I > x_forceconst
typename xphvector_base<A,B,C,D>::t_handle xphvector_base<A,B,C,D>::getHandleByIndex( I Index ) const noexcept
{
    return m_Iterator[ Index ].m_hEntry;
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D > x_inline
void xphvector_base<A,B,C,D>::SanityCheck( void ) const noexcept
{
    //
    // Sanity check the pages
    //
    for( auto& Page : m_lPages )
    {
        // Make sure that the number of allocated entries for this page is proper
        s32 nFree = 0;
        for( s32 iFree = Page.m_iFreeHead; iFree != 0xffff; iFree = *((u16*)&Page.m_Entry[iFree]) )
            nFree++;

        x_assert( (ENTRIES_PER_PAGE - nFree) == Page.m_nAlloced );
    }

    //
    // Sanity check the iterators
    //
    for( const auto& Iterator : m_Iterator )
    {
        const u32   Index  = s32(&Iterator - &m_Iterator[0]);
        const s32   iPage  = Iterator.m_hEntry.m_Value >> HANDLE_PAGE_SHIFT;
        const s32   iEntry = Iterator.m_hEntry.m_Value & HANDLE_ENTRY_MASK;
        const s32   iIter  = Iterator.m_hEntry.m_Value;
        const auto& Iter   = m_Iterator[ iIter ];
        const auto& Page   = m_lPages[iPage];
        const auto& Entry  = Page.m_Entry[iEntry];

        x_assert( Iter.m_iHandle == Index );

        // Lets make sure that that is properly free/allocated
        if( Index < m_Count )
        {
            for( s32 iFree = Page.m_iFreeHead; iFree != 0xffff; iFree = *((u16*)&Page.m_Entry[iFree]) )
            {
                x_assert( &Entry != &Page.m_Entry[iFree] );
            }
        }
        else
        {
            bool bFound = false;
            for( s32 iFree = Page.m_iFreeHead; iFree != 0xffff; iFree = *((u16*)&Page.m_Entry[iFree]) )
            {
                if( &Entry == &Page.m_Entry[iFree] )
                {
                    bFound = true;
                    break;
                }
            }
            x_assert(bFound);
        }
    }
}

//------------------------------------------------------------------------------
    
template< template< typename, typename > class A, typename B, xuptr C, typename D > x_inline
typename xphvector_base<A,B,C,D>::t_entry&  xphvector_base<A,B,C,D>::pop( void ) noexcept
{
    t_handle Handle;
    return pop( Handle );
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D >
template<typename ...T_ARG> x_inline
typename xphvector_base<A,B,C,D>::t_entry& xphvector_base<A,B,C,D>::pop( t_handle& Handle, T_ARG&&...Args ) noexcept
{
    if( m_iFreePage == 0xffff )
    {
        AllocPage();
        x_assert( m_iFreePage != 0xffff );
    }

    page&       Page     = m_lPages[ m_iFreePage ];
    const s32   iEntry   = Page.m_iFreeHead;
    const s32   iPage    = m_iFreePage;

    Page.m_iFreeHead = *((u16*)&Page.m_Entry[ iEntry ]); 
    Handle.m_Value  = iEntry | ( iPage << HANDLE_PAGE_SHIFT );
    Page.m_nAlloced++;

    if( Page.m_nAlloced >= ENTRIES_PER_PAGE )
    {
        m_iFreePage = Page.m_iNextFreePage;
    }

    //
    // Deal with iterator
    //
    {
        const s32   iLastEntry    = static_cast<int>(m_Count);
        auto&       Iterator      = m_Iterator[ Handle.m_Value ];
        const s32   Index         = Iterator.m_iHandle;

        x_Swap( m_Iterator[ iLastEntry ].m_hEntry, m_Iterator[Index].m_hEntry );
        Iterator.m_iHandle        = iLastEntry;  

        const s32 LastIndex  = m_Iterator[Index].m_hEntry.m_Value; 
        x_assert( m_Iterator[ LastIndex ].m_iHandle == iLastEntry );

        m_Iterator[ LastIndex ].m_iHandle = Index;
    }

    m_Count++;  

    (void)x_construct( t_entry, &Page.m_Entry[ iEntry ], { Args... } );

    return Page.m_Entry[ iEntry ];
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D > 
template< typename I > x_inline
void xphvector_base<A,B,C,D>::push( I index ) noexcept
{ 
    push( m_Iterator[index].m_hEntry ); 
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D > x_inline
void xphvector_base<A,B,C,D>::push( t_handle Handle ) noexcept
{
    x_assert( m_Count > 0 );

    const s32   iPage   = Handle.m_Value >> HANDLE_PAGE_SHIFT;
    const s32   iEntry  = Handle.m_Value & HANDLE_ENTRY_MASK;
    page&       Page    = m_lPages[ iPage ];
        
    //
    // Deal with page and entry
    //
    x_destruct( &Page.m_Entry[ iEntry ] );

    *((u16*)&Page.m_Entry[ iEntry ]) = Page.m_iFreeHead;
    Page.m_iFreeHead = iEntry;
        
    x_assert(Page.m_nAlloced>0);
    Page.m_nAlloced--;
        
    //
    // Deal with iterator
    //
    if( m_Count > 1 )
    {
        const s32   iLastEntry    = static_cast<int>(m_Count-1);
        auto&       Iterator      = m_Iterator[ Handle.m_Value ];
        const s32   Index         = Iterator.m_iHandle;

        x_Swap( m_Iterator[ iLastEntry ].m_hEntry, m_Iterator[Index].m_hEntry );
        Iterator.m_iHandle        = iLastEntry;  

        const s32 LastIndex  = m_Iterator[Index].m_hEntry.m_Value;
        x_assert( m_Iterator[ LastIndex ].m_iHandle == iLastEntry );

        m_Iterator[ LastIndex ].m_iHandle = Index;
    }

    //
    // Deal with the main class
    //
    if( Page.m_nAlloced == ENTRIES_PER_PAGE )
    {
        Page.m_iNextFreePage = m_iFreePage;
        m_iFreePage = iPage;
    }

    m_Count--;
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D > x_inline
void xphvector_base<A,B,C,D>::AllocPage( void ) noexcept
{
    u16 nPages;

    //
    // Grow the page numbers
    //
    if( m_lPages.isValid() )
    {
        nPages = m_lPages.template getCount<int>();
        m_lPages.Resize     ( nPages + 1 );
        m_lPages[nPages].New();
        m_Iterator.Resize   ( m_Iterator.getCount() + ENTRIES_PER_PAGE );
    }
    else
    {
        nPages = 0;
        m_lPages.New(1);
        m_lPages[0].New();
        m_Iterator.Alloc( ENTRIES_PER_PAGE );
    }

    //
    // Initialize the new page
    //
    page& Page = m_lPages[ nPages ];
    
    Page.m_nAlloced           = 0;
    Page.m_iNextFreePage      = 0xffff;
    Page.m_iFreeHead          = 0;

    for( s32 i=0; i<ENTRIES_PER_PAGE; i++ )
        *((u16*)&Page.m_Entry[i]) = i + 1;

    *((u16*)&Page.m_Entry[ ENTRIES_PER_PAGE - 1 ]) = 0xffff;

    Page.m_iNextFreePage    = m_iFreePage;
    m_iFreePage             = nPages;

    //
    // Fill the iterator info
    //
    for( s32 i=0; i<ENTRIES_PER_PAGE; i++ )
    {
        const s32   IteratorIndex    = nPages * ENTRIES_PER_PAGE + i;
        auto&       Iterator         = m_Iterator[ IteratorIndex ];
        t_handle    Handle;
            
        Handle.set( (nPages << HANDLE_PAGE_SHIFT) | i );

        Iterator.m_iHandle = IteratorIndex;
        Iterator.m_hEntry   = Handle;
    }
}

//------------------------------------------------------------------------------

template< template< typename, typename > class A, typename B, xuptr C, typename D > x_inline
xphvector_base<A, B, C, D>::~xphvector_base( void ) noexcept
{
    // Call the destructor for any pending entry
    const auto Count = getCount();
    for( int i = 0; i < Count; i++ )
    {
        x_destruct( &(*this)( m_Iterator[i].m_hEntry ) );
    }
}

