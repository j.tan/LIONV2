//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "x_base.h"

#if _X_TARGET_WINDOWS

    #include <windows.h>
    #include <winsock2.h>
    #include <ws2tcpip.h>
    //#include <winsock.h>
    #include <process.h>
    #include <math.h>

    using   socklen_t   = int;
    using   ssize_t     = int;

    #pragma comment( lib, "ws2_32.lib" )

#elif _X_TARGET_IOS || _X_TARGET_MAC

    #include <alloca.h>
    #include <netdb.h>
    #include <unistd.h>
    #include <errno.h>
    #include <sys/socket.h>
    #include <sys/ioctl.h>
    #include <sys/fcntl.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>

    #define SOCKADDR_IN     sockaddr_in
    #define INVALID_SOCKET  (-1)
    #define SOCKET_ERROR    (-1)

    #define WSAEADDRINUSE   EADDRINUSE
    #define WSAECONNRESET   ECONNRESET
    #define WSAEWOULDBLOCK  EWOULDBLOCK

    using   SOCKET      = int             ;
    using   SOCKADDR    = struct sockaddr ;

#endif


/////////////////////////////////////////////////////////////////////////////////
// DEFINES
/////////////////////////////////////////////////////////////////////////////////
//#define NETWORKDEBUG

/////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------
static
s32 GetLastSocketError( void )
{
#if _X_TARGET_WINDOWS
    return WSAGetLastError();
#elif _X_TARGET_IOS || _X_TARGET_MAC
    return errno;
#endif
}

//-------------------------------------------------------------------------------

void __x_net_init( g_context& GContext )
{
#if _X_TARGET_WINDOWS
    WSADATA     ws_data;
    WORD        ver = MAKEWORD(2,2);
    s32         Err = WSAStartup(ver, &ws_data);

    if( Err ) 
    {
        X_NET_LOGGER( "There was an error initializing networking! Error= " << Err );
        return;
    }
#else
    // Nothing to do for IOS/OSX
#endif
    
    // Send message
    X_NET_LOGGER( "XNetwork initiated successfully!" );
}

//------------------------------------------------------------------------------

void __x_net_kill( g_context& GContext )
{
#if _X_TARGET_WINDOWS
    WSACleanup();
#else
    // Nothing to do for IOS/OSX
#endif
}

//==============================================================================
//==============================================================================
//==============================================================================
// ADDRESS
//==============================================================================
//==============================================================================
//==============================================================================

//------------------------------------------------------------------------------

xnet_address::ip4 getLocalIP4( void )
{
    struct addrinfo hints, *res, *p;
    int                 status;
    char                tbuf[128];
    xnet_address::ip4   IP4;

    gethostname( tbuf, static_cast<int>(x_countof(tbuf)-1) );

    memset(&hints, 0, sizeof hints);
    hints.ai_family   = AF_INET; 
    hints.ai_socktype = SOCK_STREAM;

    if ((status = getaddrinfo( tbuf, NULL, &hints, &res)) != 0) 
        return 0xffffffff;

    for( p = res; p != nullptr; p = p->ai_next ) 
    {
        // IPv4
        if (p->ai_family == AF_INET)
        { 
            struct sockaddr_in* ipv4 = (struct sockaddr_in *)p->ai_addr;
            
            // Set the value in a raw form since it is already converted
            IP4.m_Value = ipv4->sin_addr.s_addr;
            break; 
        } 
    }

    // free the linked list
    freeaddrinfo(res); 

    return IP4;
}

//------------------------------------------------------------------------------

xnet_address::ip6 getLocalIP6( void )
{
    struct addrinfo hints, *res, *p;
    int                 status;
    char                tbuf[128];
    xnet_address::ip6   IP6;

    gethostname( tbuf, 127 );

    memset(&hints, 0, sizeof hints);
    hints.ai_family   = AF_INET6; 
    hints.ai_socktype = SOCK_STREAM;

    if ((status = getaddrinfo( tbuf, NULL, &hints, &res)) != 0) 
        return {{0}};

    for( p = res; p != nullptr; p = p->ai_next ) 
    {
        // IPv6
        if( p->ai_family == AF_INET6 ) 
        { 
            struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
            
            // copy it raw
            x_memcpy( &IP6, &ipv6->sin6_addr, sizeof(ipv6->sin6_addr) );
            static_assert( sizeof(ipv6->sin6_addr) == sizeof(IP6),"" );
            break;
        }
    }

    freeaddrinfo(res); // free the linked list
    return IP6;
}

//------------------------------------------------------------------------------

int getLocalANY( int ip_type )
{
    struct addrinfo hints, *res, *p;
    int     status;
    char    ipstr[INET6_ADDRSTRLEN];
    char    tbuf[128];

    gethostname( tbuf, 127 );

    memset(&hints, 0, sizeof hints);
    hints.ai_family   = AF_UNSPEC; 
    hints.ai_socktype = SOCK_STREAM;

    if ((status = getaddrinfo( tbuf, NULL, &hints, &res)) != 0) 
    {
        return -1;
    }

    printf("IP addresses for %s:\n\n", tbuf);

    for( p = res; p != nullptr; p = p->ai_next ) 
    {
        void *addr;
        char *ipver;

        // get the pointer to the address itself,
        // different fields in IPv4 and IPv6:
        if (p->ai_family == AF_INET) 
        { 
            // IPv4
            struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
            addr = &(ipv4->sin_addr);
            ipver = "IPv4";
        } 
        else
        { 
            // IPv6
            struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
            addr = &(ipv6->sin6_addr);
            ipver = "IPv6";
        }

        // convert the IP to a string and print it:
        inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
        printf("  %s: %s\n", ipver, ipstr);
    }

    freeaddrinfo(res); // free the linked list
    return 0;
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// NET SIMULATOR
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------------
 
bool xnet_socket::xnetwork_tester::SendTo( 
    s32                     Socket,
    xbuffer_view<xbyte>     ViewData, 
    s32                     SomeThing, 
    sockaddr*               pAddr, 
    s32                     Size ) noexcept
{ 
#ifdef NETWORKDEBUG
    X_NET_LOGGER( "LagSimulator: Lost " << m_PacketLost << " Hacked Packets " << m_Currupted << "TotalSent " << m_TotalPacketsSent );
#endif

    m_TotalPacketsSent++;

    //
    // Through away the packet
    //
    if( m_Random.Rand32(0, 100) < m_PercentagePackageLost )
    {
        m_PacketLost++;
        X_NET_LOGGER( "LagSimulator: Losing a packet" );
        return true;
    }

    //
    // Ok save the packet because we will send it at some point
    //
    lag_noise_backup& Data = m_BackupData.append();

    // The wait could go as low as 0 seconds to a max of 30sec
    Data.m_Wait     = xtimer::getNowMs();
    Data.m_Data.CopyToFrom( ViewData );
    Data.m_Lenth    = ViewData.getCount<int>();
    x_memcpy( &Data.m_Addr, pAddr, sizeof(*pAddr) );
    static_assert( sizeof(Data.m_Addr) >= sizeof(*pAddr), "" );
    Data.m_Size     = Size;
    Data.m_Socket   = Socket;

    //
    // Ok deal with corruption
    //
    if( m_Random.Rand32(0, 100) < m_PercentageHackedData )
    {
        m_Currupted++;
        X_NET_LOGGER( "LagSimulator: Corrupting packet" );

        const s32 Count = m_Random.Rand32( 1, Data.m_Size / 4 );
        for( s32 i=0; i<Count; i++ )
        {
            const s32 Pos = m_Random.Rand32( 0, Data.m_Size-1);
            Data.m_Data[Pos] = 0xCC;
        }
    }

    //
    // Deal with packet delay
    // 
    if( x_irand(0,100) < m_PercentageLagNoise )
    {
        const auto Delay = x_irand( 0, ( 2000 * m_PercentageLagNoise ) / 100 ); 
        X_NET_LOGGER( "LagSimulator: Delaying a packet: " << Delay );

        // Up to 2 seconds of delay
        Data.m_Wait += Delay; 
    }

    //
    // Deal with duplication
    //
    if( m_Random.Rand32(0, 100) < m_PercentagePacketDuplication )
    {
        X_NET_LOGGER( "LagSimulator: Duplicating packet!" );
        return SendTo( 
            Socket,
            ViewData,
            SomeThing,
            pAddr, 
            Size );
    }

    return true;
}

//------------------------------------------------------------------------------
 
void xnet_socket::xnetwork_tester::Update( void ) noexcept
{
    s32 i;
    if( m_BackupData.getCount() == 0 ) return;

    const auto Time = xtimer::getNowMs();
    for( i=0; i<m_BackupData.getCount(); i++ )
    {
        lag_noise_backup& Data = m_BackupData[i];
        if( Time > Data.m_Wait )
        {
            const long status = sendto( 
                Data.m_Socket, 
                (const char*)&Data.m_Data[0], 
                Data.m_Lenth, 
                0, 
                (sockaddr*)&Data.m_Addr, 
                Data.m_Size );

            if( status <= 0 ) 
            {
                X_NET_LOGGER( "SendTo returned an error code " << GetLastSocketError() );
            }

            // Delete entry
            m_BackupData.DeleteWithCollapse( i );
            i--;
        }
    }
}

//------------------------------------------------------------------------------

void xnet_socket::xnetwork_tester::EnableLagSimulator( 
    s32 PercentagePackageLost, 
    s32 PercentageDuplicated, 
    s32 PercentageHackedData, 
    s32 PercentageLagNoise, 
    s32 PercentageLagDistance ) noexcept
{
    m_PercentagePackageLost         = PercentagePackageLost;
    m_PercentageHackedData          = PercentageHackedData;
    m_PercentageLagNoise            = PercentageLagNoise;
    m_PercentageLagDistance         = PercentageLagDistance;
    m_PercentagePacketDuplication   = PercentageDuplicated;
    m_bLagSimulatorOn               = true;
    m_LagSimNextTime                = xtimer::getNowMs() + 50;
}

//------------------------------------------------------------------------------

void xnet_socket::xnetwork_tester::DisableLagSimulator( void ) noexcept
{
    m_bLagSimulatorOn = false;
}
 
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// SOCKET CLASS
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// ERROR CODES FOR NETWORK
// https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man2/intro.2.html
//------------------------------------------------------------------------------
void xnet_socket::closeSocket( void ) noexcept
{
    if( m_Socket != ~0u )
    {
#if _X_TARGET_WINDOWS
        closesocket(m_Socket);
#else
        close(m_Socket);
#endif
        m_Socket = ~0u; 
    }
}

//-------------------------------------------------------------------------------

xnet_socket::~xnet_socket ( void ) noexcept
{
    closeSocket();
}

//------------------------------------------------------------------------------

xnet_socket::err xnet_socket::openUDPSocket( xnet_address::port Port ) noexcept
{   
    SOCKADDR_IN     SockAddr;

    // Set the port number
    m_Port = Port;
    m_bTCP = false;

    // Set up socket
    SOCKET UDPSocket = INVALID_SOCKET;
    
    // Initialize the UDP socket
    UDPSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if( UDPSocket == INVALID_SOCKET ) 
    {
        const auto Err = GetLastSocketError();
        X_NET_LOGGER( "Cannot create UDP socket: " << Err );
        return x_error_code( errors, ERR_CREATE_SOCKET, "Cannot create UDP socket:" );
    }

    // bind the socket
    x_memset( &SockAddr, 0, sizeof(SOCKADDR_IN) );
    SockAddr.sin_family         = AF_INET; 
    SockAddr.sin_addr.s_addr    = htonl(INADDR_ANY);
    SockAddr.sin_port           = Port.m_Value;
 
    if( bind( UDPSocket, (SOCKADDR*)&SockAddr, sizeof(SockAddr) ) == SOCKET_ERROR )
    {
        // increment port if that port is assigned
        if( GetLastSocketError() == WSAEADDRINUSE )
        {
            X_NET_LOGGER( "Error it seems like this port " << m_Port.get() << " is in use. Try a different one.");
            return x_error_code( errors, ERR_ALRADY_INUSE, "port in used." );
        }
        else
        {
            // if some other error, nothing we can do...abort
            const auto Err = GetLastSocketError();
            X_NET_LOGGER( "Couldn't bind UDP socket " << Err << "! Invalidating" );
            return x_error_code( errors, ERR_BINDING, "fail to bind the UDP port" );
        }
    }

    // set this socket to non-blocking, so we can poll it
#if _X_TARGET_WINDOWS
    u_long  dwNoBlock = true;
    ioctlsocket( UDPSocket, FIONBIO, &dwNoBlock );
#else
    fcntl( UDPSocket, F_SETFL, O_NONBLOCK);
#endif 
    
    // Set the mode of the socket to allow broadcasting.  We need to be able to broadcast
    // when a game is searched for in IPX mode.
    u_long  dwBroadcast = true;

#if _X_TARGET_WINDOWS
    setsockopt( UDPSocket, SOL_SOCKET, SO_BROADCAST, (LPSTR)&dwBroadcast, sizeof(dwBroadcast));
#else
    setsockopt( UDPSocket, SOL_SOCKET, SO_BROADCAST, &dwBroadcast, sizeof(dwBroadcast));
#endif
    
    // Set the socket
    m_Socket = (u32)UDPSocket;

    // Success!
    X_NET_LOGGER( "UDP Initialized at port: " << Port.get() );
    
    return x_error_ok( errors );
}

//------------------------------------------------------------------------------

xnet_socket::err  xnet_socket::openTCPSocket( xnet_address::port Port ) noexcept
{
    // Set the port number
    m_Port = Port;
    m_bTCP = true;

    // Set up socket
    SOCKET TCPSocket = INVALID_SOCKET;

    // Initialize the UDP socket
    TCPSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if( TCPSocket == INVALID_SOCKET ) 
    {
        const auto Err = GetLastSocketError();
        X_NET_LOGGER( "Cannot create TCP socket: " << Err );
        return x_error_code( errors, ERR_CREATE_SOCKET, "Cannot create TCP socket:" );
    }

    // set this socket to non-blocking, so we can poll it
#if _X_TARGET_WINDOWS
    u_long  dwNoBlock = true;
    ioctlsocket( TCPSocket, FIONBIO, &dwNoBlock );
#else
    fcntl( TCPSocket, F_SETFL, O_NONBLOCK );
#endif
    
    // Set the socket
    m_Socket = (u32)TCPSocket;

    // Success!
    X_NET_LOGGER( "TCP Initialized at port: " << Port.get() );

    return x_error_ok( errors );
}

//------------------------------------------------------------------------------
 
xnet_socket::err xnet_socket::ReadFromSocket( xbuffer_view<xbyte> Buffer, s32& Length, xnet_address& FromAddr, bool bBlock ) noexcept
{
    // read socket stuff
    SOCKADDR_IN     ip_addr;        // UDP/TCP socket structure
    fd_set	        rfds;
    timeval	        TimeOut;
    socklen_t       FromLen;

    Length = 0;
    x_assert( Buffer.getCount() > 0 );
    
    // check if there is any data on the socket to be read.  The amount of data that can be
    // atomically read is stored in len.
    FD_ZERO(&rfds);
    FD_SET (m_Socket, &rfds);
    TimeOut.tv_sec  = 0;
    TimeOut.tv_usec = 0;

    // If we are blocking lets wait for 10 sec maximum
    if( bBlock )
    {
        TimeOut.tv_usec  = 500;
    }

    if( select( 0, &rfds, NULL, NULL, &TimeOut) == SOCKET_ERROR ) 
    {
        const auto LastError = GetLastSocketError();
        X_NET_LOGGER( "select( 0, &rfds, NULL, NULL, &TimeOut) Fail with error " << LastError );
        return x_error_code( errors, ERR_FAILURE, "select failure!" );
    }

    // if the read file descriptor is not set, then bail!
    // Do we have any data?
    if( !FD_ISSET(m_Socket, &rfds) ) 
    {
        //const auto LastError = WSAGetLastError();
        //X_NET_LOGGER( "!FD_ISSET(m_Socket, &rfds) Fail with error: " << LastError );
        //return x_error_code( errors, ERR_NOREADY, "read descriptor no set" );
        return x_error_ok( errors );
    }
    
    // Get data off the socket and process.
    FromLen = sizeof(SOCKADDR_IN);

#if _X_TARGET_WINDOWS
    Length = recvfrom( m_Socket, (char*)&Buffer[0], Buffer.getCount<int>(), 0, (SOCKADDR*)&ip_addr, &FromLen );
#else
    /*
    ssize_t	recvfrom(int, void *, size_t, int, struct sockaddr * __restrict,
                     socklen_t * __restrict) __DARWIN_ALIAS_C(recvfrom);
    */
    
    ssize_t l = recvfrom( (int)m_Socket, (void*)&Buffer[0],  Buffer.getCount<int>(), 0, (SOCKADDR*)&ip_addr, &FromLen );
    
    Length = (s32)l;
    x_assert( Length == l );
#endif

    if( Length == SOCKET_ERROR ) 
    {
        // We could use WSAECONNRESET to signify the socket was closed by the
        // target side. This *should* only apply to TCP sockets but it also
        // seems to apply in Windows for UDP sockets if sent to the same machine.
#ifdef NETWORKDEBUG
        s32 LastError = GetLastSocketError();
        if( LastError == EAGAIN )
        {
            // We simply did not get a packet
            return FALSE;
        }
        
        if ( (LastError != WSAEWOULDBLOCK) && (LastError != WSAECONNRESET) )
        {
            X_NET_LOGGER( "RecvFrom returned an error " << LastError);
        }
        else
        {
            X_NET_LOGGER( "RecvFrom returned a potential error " << LastError);
        }
#endif

        return x_error_code( errors, ERR_FAILURE, "Socket error while receiving" );
    }

    // If this gets hit, data will be overwritten.  BAD!!	
    x_assert( Length <= Buffer.getCount<int>() );

    // Set the address
    xnet_address::port Port;
    xnet_address::ip4  IP4;
    
    // setup the raw values to avoid any conversion
    Port.m_Value = ip_addr.sin_port;
    IP4.m_Value  = ip_addr.sin_addr.s_addr;
    FromAddr.setup( IP4,Port );

    return x_error_ok( errors );
}

//------------------------------------------------------------------------------

xnet_socket::err xnet_socket::SendPacket( xbuffer_view<xbyte> Buffer, xnet_address Address ) noexcept
{
    struct sockaddr_in sockto;

    // address your package and stick a stamp on it :-)
    x_memset( &sockto, 0, sizeof(sockaddr_in) );
    sockto.sin_family       = AF_INET;
    sockto.sin_port         = Address.getPort().m_Value;
    sockto.sin_addr.s_addr  = Address.getIP4().m_Value;

    if( m_NetworkTester.isValid() && m_NetworkTester->isEnable() )
    {
        if( m_NetworkTester->SendTo( m_Socket, Buffer, 0, (struct sockaddr*)&sockto, sizeof(sockto) ) )
            return x_error_ok( errors );
        return x_error_code( errors, ERR_FAILURE, "Socket error while sending" );
    }
    else
    {
        const ssize_t status = sendto( m_Socket, (const char*)&Buffer[0], Buffer.getCount<int>(), 0, (struct sockaddr*)&sockto, sizeof(sockto) );
        if( status <= 0 )
        {
            X_NET_LOGGER( "SendTo returned an error code " << GetLastSocketError() );
            return x_error_code( errors, ERR_FAILURE, "Socket error while sending" );
        }
    }

    return x_error_ok( errors );
}

//------------------------------------------------------------------------------

xnet_address::ip4 xnet_socket::getLocalIP4( void ) noexcept
{
    xnet_address a;
    a.setIP( ::getLocalIP4() );
    return a.getIP4();       
}

//-------------------------------------------------------------------------------

xnet_socket::err xnet_socket::AcceptTCPConnection( xnet_socket& NewSocket ) const noexcept
{
    //
    // Try to accept any incoming connection
    //
    sockaddr_in addr;
    socklen_t   addr_len    = sizeof(addr);
    SOCKET      newConnSD   = accept(m_Socket, (sockaddr *)&addr, &addr_len );
    
    if( newConnSD == INVALID_SOCKET ) 
    {
        const auto err = GetLastSocketError();

        // Since we have made the socket to be non blocking
        // returning a null if nothing is happening is ok
        if( err == WSAEWOULDBLOCK ) 
            return x_error_ok( errors );

        X_NET_LOGGER( "TCP connection accept failed! Error=" << err ); 

        return x_error_code( errors, ERR_FAILURE, "TCP connection accept failed!" );
    }
// not sure to remove this line
//    s32 Port = ntohs(addr.sin_port);

    //
    // Get the port that we are open for the new client
    //
    s32 Port = 0;
    {
        sockaddr_in addr;
        socklen_t addr_len = sizeof(addr);

        if( getsockname( m_Socket, (sockaddr *)&addr, &addr_len) < 0 ) 
        {
            const auto err = GetLastSocketError();
            X_NET_LOGGER( "TCP socket fail to return the port! Error= " << err ); 
            return x_error_code( errors, ERR_FAILURE, "TCP socket fail to return the por" );;
        }
        else
        {
            Port = ntohs(addr.sin_port);
        }
    }

    //
    // Fill the return structure
    //
    NewSocket.m_bTCP     = true;
    NewSocket.m_Port     = Port;
    NewSocket.m_Socket   = (u32)newConnSD;
    NewSocket.m_RefCount = 1;

    return x_error_ok( errors );
}

//-------------------------------------------------------------------------------

xnet_socket::err xnet_socket::ConnectTCPServer( xnet_address Address ) noexcept
{
    struct sockaddr_in sockto;

    // address your package and stick a stamp on it :-)
    x_memset( &sockto, 0, sizeof(struct sockaddr_in) );
    sockto.sin_family       = AF_INET;
    sockto.sin_port         = Address.getPort().m_Value;
    sockto.sin_addr.s_addr  = Address.getIP4().m_Value;

    if( connect( m_Socket, (sockaddr *) &sockto, sizeof(sockto)) < 0) 
    {
        const auto Err = GetLastSocketError();

#if _X_TARGET_WINDOWS == false
    #define WSAEALREADY EALREADY
#endif
        // If the client is in the process of connecting then we are all good...
        if( Err == WSAEALREADY || Err == WSAEWOULDBLOCK )
        {
            return x_error_ok( errors );
        }
        
#if _X_TARGET_WINDOWS
        // If already connected???
        if( Err == EISCONN || Err == EINPROGRESS )
        {
            return x_error_ok( errors );
        }
#endif
        X_NET_LOGGER( "TCP error connecting to the server! Error= " << Err );
        return x_error_code( errors, ERR_FAILURE, "TCP error connecting to the server!" );
    }

    return x_error_ok( errors );
}

//-------------------------------------------------------------------------------

void xnet_socket::EnableLagSimulator ( s32 PercentagePackageLost, s32 PercentageDuplicated, s32 PercentageHackedData, s32 PercentageLagNoise, s32 PercentageLagDistance ) noexcept
{
    x_assert( m_NetworkTester.isValid() == false );
    m_NetworkTester.New();

    m_NetworkTester->EnableLagSimulator( 
        PercentagePackageLost, 
        PercentageDuplicated, 
        PercentageHackedData, 
        PercentageLagNoise, 
        PercentageLagDistance );
}

//-------------------------------------------------------------------------------

void xnet_socket::DisableLagSimulator( void ) noexcept
{
    if( m_NetworkTester.isValid() )
    {
        m_NetworkTester->DisableLagSimulator();
    }
}
