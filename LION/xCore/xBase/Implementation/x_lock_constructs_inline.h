//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// x_lk_spinjob
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
inline
void x_lk_spinlock::do_light_jobs::t_Work( void ) const
{
    auto pJob = m_S.getLightJob();
    if(pJob) m_S.ProcessWork(pJob);
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// x_lk_semaphore_smart_lock
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------

template< typename T_DOWORK > inline
void x_lk_semaphore_smart_lock<T_DOWORK>::ChangeBehavior( flags Flags ) const noexcept
{

    semaphore LocalReality = m_Semapore.load();
    do
    {
        semaphore       NewReality   = LocalReality;
        
        NewReality.m_Flags = Flags;
        NewReality.m_Counter++;

        if ( m_Semapore.compare_exchange_weak( LocalReality, NewReality ) )
            break;

    } while( 1 );
}

//---------------------------------------------------------------------------------------

template< typename T_DOWORK > inline
void x_lk_semaphore_smart_lock<T_DOWORK>::RefReader( bool bInc ) const noexcept
{
    semaphore LocalReality = m_Semapore.load();
    do
    {
        if( bInc )
        {
            x_assert( LocalReality.m_Flags.m_QT_READABLE );

            s32 MaxReaders = LocalReality.m_Flags.m_SINGLE_READER ? 1 : 0xff;            
            if( LocalReality.m_nWritters > 0 || LocalReality.m_nReaders >= MaxReaders )
            {
                T_DOWORK DoWork;

                //
                // Do work if the user ask us to do it
                //
                do
                {
                    DoWork.t_Work();
                    LocalReality = m_Semapore.load();
                    MaxReaders   = LocalReality.m_Flags.m_SINGLE_READER ? 1 : 0xff;            
                } while( LocalReality.m_nWritters > 0 || LocalReality.m_nReaders >= MaxReaders );
            }
        }

        x_assert( LocalReality.m_nWritters == 0 );
        x_assert( LocalReality.m_nReaders  < 0xff );

        semaphore NewReality   = LocalReality;
            
        NewReality.m_Counter++;
        if( bInc ) NewReality.m_nReaders++;
        else       NewReality.m_nReaders--;

        if ( m_Semapore.compare_exchange_strong( LocalReality, NewReality ) )
            break;

    } while( 1 );
}

//---------------------------------------------------------------------------------------
template< typename T_DOWORK > inline
void x_lk_semaphore_smart_lock<T_DOWORK>::RefMutate( bool bInc ) const noexcept
{
    semaphore LocalReality = m_Semapore.load();
    do
    {
        if( bInc )
        {
            x_assume( LocalReality.m_Flags.m_QT_MUTABLE ); 
            x_assume( false == LocalReality.m_Flags.m_READ_ONLY ); 

            s32 MaxWritters = LocalReality.m_Flags.m_SINGLE_WRITTER ? 1 : 0xff;
            
            if( LocalReality.m_nReaders > 0 || LocalReality.m_nWritters >= MaxWritters )
            {
                T_DOWORK DoWork;

                //
                // Do work if the user ask us to do it
                //
                do
                {
                    DoWork.t_Work();
                    LocalReality = m_Semapore.load();

                    x_assume( LocalReality.m_Flags.m_QT_MUTABLE ); 
                    x_assume( false == LocalReality.m_Flags.m_READ_ONLY ); 
                    
                    MaxWritters = LocalReality.m_Flags.m_SINGLE_WRITTER ? 1 : 0xff;
                
                } while( LocalReality.m_nReaders > 0 || LocalReality.m_nWritters >= MaxWritters );
            }
        }

        x_assume( LocalReality.m_nWritters <= ( LocalReality.m_Flags.m_SINGLE_WRITTER ? 1 : 0xff ) );
        x_assume( LocalReality.m_nReaders  == 0 );

        semaphore NewReality   = LocalReality;
            
        NewReality.m_Counter++;
        if( bInc ) NewReality.m_nWritters++;
        else       NewReality.m_nWritters--;

        if ( m_Semapore.compare_exchange_strong( LocalReality, NewReality ) )
            break;

    } while( 1 );
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// x_lk_semaphore_smart_lock
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------

template< typename T_ENTRY, typename T_COUNTER > inline          
void x_linear_link_list<T_ENTRY,T_COUNTER>::AppendFirst( t_entry& Entry ) noexcept
{
    Entry.m_pNext = m_pHead;
    
    if( m_pHead )  
    {
        Entry.m_pPrev       = m_pHead->m_pPrev;
        m_pHead->m_pPrev    = &Entry;
    }
    else
    {
        Entry.m_pPrev = &Entry; 
    }
    m_pHead = &Entry;

    m_Count++;
}

//---------------------------------------------------------------------------------------

template< typename T_ENTRY, typename T_COUNTER > inline          
void x_linear_link_list<T_ENTRY,T_COUNTER>::Append( t_entry& Entry ) noexcept
{
    Entry.m_pNext = nullptr;
    
    if( m_pHead )  
    {
        Entry.m_pPrev               = m_pHead->m_pPrev;
        
        x_assert( Entry.m_pPrev->m_pNext == nullptr );
        Entry.m_pPrev->m_pNext      = &Entry;
        m_pHead->m_pPrev            = &Entry;
    }
    else
    {
        Entry.m_pPrev = &Entry; 
        m_pHead       = &Entry;
    }

    m_Count++;
}

//---------------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER > inline          
void x_linear_link_list<T_ENTRY,T_COUNTER>::Remove( t_entry& Entry ) noexcept
{
    if( m_pHead == &Entry )
    {
        m_pHead = Entry.m_pNext;
    }
    else
    {
        Entry.m_pPrev->m_pNext  = Entry.m_pNext;
    }

    if( Entry.m_pNext ) Entry.m_pNext->m_pPrev  = Entry.m_pPrev; 

    Entry.m_pPrev = nullptr;
    Entry.m_pNext = nullptr;

    m_Count--;
}

//---------------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER > inline          
void x_linear_link_list<T_ENTRY,T_COUNTER>::InsertAfter ( t_entry& AfterEntry, t_entry& Entry ) noexcept
{
    if( AfterEntry.m_pNext ) AfterEntry.m_pNext->m_pPrev = &Entry;

    Entry.m_pPrev       = &AfterEntry;
    Entry.m_pNext       = AfterEntry.m_pNext;
    AfterEntry.m_pNext  = &Entry;

    m_Count++;
}

//---------------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER > inline          
void x_linear_link_list<T_ENTRY,T_COUNTER>::InsertBefore ( t_entry& BeforeEntry, t_entry& Entry ) noexcept
{
    Entry.m_pPrev       = BeforeEntry.m_pPrev;
    Entry.m_pNext       = &BeforeEntry;

    BeforeEntry.m_pPrev = &Entry;

    if( &BeforeEntry == m_pHead )
    {
        m_pHead = &Entry;
    }

    m_Count++;
}


