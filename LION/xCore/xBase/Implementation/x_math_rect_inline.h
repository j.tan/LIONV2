//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
inline
xrect::xrect( f32 Left, f32 Top, f32 Right, f32 Bottom )
{
    setup( Left, Top, Right, Bottom );
}

//------------------------------------------------------------------------------
inline
xrect& xrect::setup( f32 X, f32 Y, f32 Size )
{
    m_Left   = X-Size;
    m_Top    = Y-Size;
    m_Right  = X+Size;
    m_Bottom = Y+Size;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::setup( f32 Left, f32 Top, f32 Right, f32 Bottom )
{
    m_Left   = Left;
    m_Top    = Top;
    m_Right  = Right;
    m_Bottom = Bottom;
    return *this;
}

//------------------------------------------------------------------------------
inline
void xrect::setZero( void )
{
    m_Left      = 0;
    m_Top       = 0;
    m_Right     = 0;
    m_Bottom    = 0;
}

//------------------------------------------------------------------------------
inline
void xrect::setMax( void )
{
    m_Left      =  X_F32_MAX;
    m_Top       =  X_F32_MAX;
    m_Right     = -X_F32_MAX;
    m_Bottom    = -X_F32_MAX;
}

//------------------------------------------------------------------------------
inline
bool xrect::Intersect( const xrect& Rect )
{
    return  ( m_Left   <= Rect.m_Right  ) &&
            ( m_Top    <= Rect.m_Bottom ) &&
            ( m_Right  >= Rect.m_Left   ) &&
            ( m_Bottom >= Rect.m_Top    );
}

//------------------------------------------------------------------------------
inline
bool xrect::Intersect( xrect& R, const xrect& Rect )
{
    if( Intersect( Rect ) == false )
        return( false );

    R.m_Left    = x_Max( m_Left,   Rect.m_Left    );
    R.m_Top     = x_Max( m_Top,    Rect.m_Top     );
    R.m_Right   = x_Min( m_Right,  Rect.m_Right   );
    R.m_Bottom  = x_Min( m_Bottom, Rect.m_Bottom  );

    return true ;
}

//------------------------------------------------------------------------------
inline
bool xrect::PointInRect( f32 X, f32 Y ) const
{
    return ((X >= m_Left) && (X <= m_Right) && (Y >= m_Top) && (Y <= m_Bottom));
}

//------------------------------------------------------------------------------
inline
bool xrect::PointInRect( const xvector2& Pos ) const
{
    return PointInRect( Pos.m_X, Pos.m_Y );
}

//------------------------------------------------------------------------------
inline
xrect& xrect::AddPoint( f32 X, f32 Y )
{
    m_Left   = x_Min( m_Left  , X );
    m_Top    = x_Min( m_Top   , Y );
    m_Right  = x_Max( m_Right , X );
    m_Bottom = x_Max( m_Bottom, Y );
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::AddRect( const xrect& Rect )
{
    m_Left   = x_Min( m_Left  , Rect.m_Left   );
    m_Top    = x_Min( m_Top   , Rect.m_Top    );
    m_Right  = x_Max( m_Right , Rect.m_Right  );
    m_Bottom = x_Max( m_Bottom, Rect.m_Bottom );
    return *this;
}

//------------------------------------------------------------------------------
inline
f32 xrect::getWidth( void ) const
{
    return m_Right - m_Left;
}

//------------------------------------------------------------------------------
inline
f32 xrect::getHeight( void ) const
{
    return m_Bottom - m_Top;
}

//------------------------------------------------------------------------------
inline
xvector2 xrect::getSize( void ) const
{
    return xvector2( getWidth(), getHeight() );
}

//------------------------------------------------------------------------------
inline
xvector2 xrect::getCenter( void ) const
{
    return xvector2( m_Left + getWidth()/2.0f, m_Top + getHeight()/2.0f );
}

//------------------------------------------------------------------------------
inline
xrect& xrect::setWidth( f32 W )
{
    m_Right = m_Left + W;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::setHeight( f32 H )
{
    m_Bottom = m_Top + H;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::setSize( f32 W, f32 H )
{
    m_Right  = m_Left + W;
    m_Bottom = m_Top  + H;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::Translate( f32 X, f32 Y )
{
    m_Left   += X;
    m_Top    += X;
    m_Right  += Y;
    m_Bottom += Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::Inflate( f32 X, f32 Y )
{
    m_Left    -= X;
    m_Top     -= Y;
    m_Right   += X;
    m_Bottom  += Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
xrect& xrect::Deflate( f32 X, f32 Y )
{
    m_Left    += X;
    m_Top     += Y;
    m_Right   -= X;
    m_Bottom  -= Y;
    return *this;
}

//------------------------------------------------------------------------------
inline
bool xrect::InRange( f32 Min, f32 Max ) const
{
    return (m_Left   >= Min) && (m_Left   <= Max) &&
           (m_Top    >= Min) && (m_Top    <= Max) &&
           (m_Right  >= Min) && (m_Right  <= Max) && 
           (m_Bottom >= Min) && (m_Bottom <= Max);
}

//------------------------------------------------------------------------------
inline
bool xrect::isEmpty( void ) const
{
    return( (m_Left>=m_Right) || (m_Top>=m_Bottom) );
}

//------------------------------------------------------------------------------
inline
bool xrect::operator == ( const xrect& R ) const
{
    return( (m_Left   == R.m_Left  ) &&
            (m_Top    == R.m_Top   ) &&
            (m_Right  == R.m_Right ) &&
            (m_Bottom == R.m_Bottom) );
}

//------------------------------------------------------------------------------
inline
bool xrect::operator != ( const xrect& R ) const
{
    return( (m_Left   != R.m_Left  ) ||
            (m_Top    != R.m_Top   ) ||
            (m_Right  != R.m_Right ) ||
            (m_Bottom != R.m_Bottom) );
}

//------------------------------------------------------------------------------
inline
xrect xrect::Interpolate( f32 T, const xrect& Rect ) const
{
    const xvector4 V1( m_Left, m_Top, m_Right, m_Bottom );
    const xvector4 V2( m_Left, m_Top, m_Right, m_Bottom );
    
    xvector4       Ret( V1 + T * ( V2 - V1 ) );
    
    return xrect( Ret.m_X, Ret.m_Y, Ret.m_Z, Ret.m_W );
}
