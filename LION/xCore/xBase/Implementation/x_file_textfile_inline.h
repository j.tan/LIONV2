//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------

x_forceconst
const xstring& xtextfile::getRecordName( void ) const noexcept
{
    return m_Record.m_Name;
}

//------------------------------------------------------------------------------
x_forceconst
int xtextfile::getRecordCount( void ) const noexcept
{
    return m_Record.m_Count;
}

//------------------------------------------------------------------------------
x_forceinline
int xtextfile::getAtomicSize( char Type ) noexcept
{
    switch( Type )
    {
        case 'f': return 4; 
        case 'F': return 8; 
        case 'd': return 4; 
        case 'h': return 4; 
        case 'D': return 8; 
        case 'c': return 1; 
        case 'C': return 2; 
        case 'S': return sizeof(const char*);
        case 's': return sizeof(xstring);
        case 'e': return sizeof(const char*);
        case 'E': return sizeof(xstring);
        case 'g': return 8;
    }

    return -1;
}

//------------------------------------------------------------------------------
x_forceinline
bool xtextfile::isAtomicString( char Type ) noexcept
{
    switch( Type )
    {
        case 'S': return true;
        case 's': return true;
        case 'e': return true;
        case 'E': return true;
    }
    
    return false;
}

//------------------------------------------------------------------------------
x_forceconst
int xtextfile::getNumberFields( void ) const noexcept      
{ 
    return m_Field.getCount<int>();  
}

//------------------------------------------------------------------------------
x_inline
void xtextfile::getFieldDesc( int Index, xstring& String ) const noexcept
{
    const field& Field = m_Field[Index];
    
    if( m_States.m_bBinary && Field.m_Type[Field.m_TypeOffset] == '.' )
    {
        String.Copy( Field.m_Type );
        String[ Field.m_TypeOffset ] = 0;
        
        const s32 iUserType = Field.m_iUserType;
        String.append( m_UserTypes[ iUserType ].m_UserType );
    }
    else
    {
        String = Field.m_Type;
    }
}

//------------------------------------------------------------------------------
x_inline
void xtextfile::getFieldName( int Index, xstring& String ) const noexcept 
{ 
    String.Copy( m_Field[Index].m_Type );
    String[m_Field[Index].m_TypeOffset-1]=0; 
}

//------------------------------------------------------------------------------
x_inline
const char* xtextfile::getFieldType( int Index ) const noexcept
{ 
    const auto  tof   =  m_Field[Index].m_TypeOffset;
    const char* pData = &m_Field[Index].m_Type[tof]; 
    return pData;
}

//------------------------------------------------------------------------------
x_inline
u8 xtextfile::getUserTypeUID( int Index ) const noexcept
{
    const field&        Field       = m_Field[ Index ];
    const user_type&    UserType    =  m_UserTypes[ Field.m_iUserType ];
    return UserType.m_UID;
}

//------------------------------------------------------------------------------
inline
int xtextfile::AddUserType( const char* pSystemTypes, const char* pUserType, u8 UID ) noexcept
{
    user_type   Key;
    s32         Index;
    
    // Must have this in order to follow standard
    x_assert(pUserType[0]=='.');
    
    Key.m_UserType.Copy( pUserType );
    if( m_UserTypes.BinarySearch( Key, Index ) )
    {
        // Make sure that this registered type is fully duplicated
        x_assert( x_strcmp( pSystemTypes, &m_UserTypes[Index].m_SystemType[0] ) == 0 );
        
        // If the user is calling to initialize the UID
        if( UID != 0xff && m_UserTypes[Index].m_UID == 0xff )
        {
            m_UserTypes[Index].m_UID = UID;
        }
        
        // we already have it so we don't need to added again
        return Index;
    }
    
    //
    // Sanity check the user types
    //
#ifdef _X_DEBUG
    for( s32 i=0; pUserType[i]; i++ )
    {
        x_assert( false == x_isspace(pUserType[i]) );
    }
    
    for( s32 i=0; pSystemTypes[i]; i++ )
    {
        x_assert( false == x_isspace(pSystemTypes[i]) );
    }
#endif
    
    //
    // Fill the entry
    //
    user_type&  Entry   = m_UserTypes.Insert( Index );
    
    Entry.m_SystemType.Copy( pSystemTypes );
    Entry.m_UserType     = Key.m_UserType;
    Entry.m_bSaved       = false;
    Entry.m_nSystemTypes = Entry.m_SystemType.getLength().m_Value;
    Entry.m_UID          = UID;
    
    return Index;
}

