//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY DATA
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------
x_inline
xproperty_data& xproperty_data::operator = ( const xproperty_data& A ) 
{
    // setup the new type properly
    setup( A.m_Type );

    // copy over the trivial types
    m_Name  = A.m_Name;
    m_Flags = A.m_Flags;
  
    // copy the actual data
    switch( A.m_Type ) 
    {
        case type::BOOL:        m_Data.m_Bool       = A.m_Data.m_Bool;      break;
        case type::BUTTON:      m_Data.m_Button     = A.m_Data.m_Button;    break;
        case type::RGBA:       m_Data.m_Color      = A.m_Data.m_Color;     break;
        case type::ENUM:        m_Data.m_Enum       = A.m_Data.m_Enum;      break;
        case type::INT:         m_Data.m_S32        = A.m_Data.m_S32;       break;
        case type::FLOAT:       m_Data.m_S32        = A.m_Data.m_S32;       break;
        case type::ANGLES3:     m_Data.m_Radian3    = A.m_Data.m_Radian3;   break;
        case type::STRING:      m_Data.m_String     = A.m_Data.m_String;    break;
        case type::GUID:        m_Data.m_Guid       = A.m_Data.m_Guid;      break;
        case type::VECTOR2:     m_Data.m_Vector2    = A.m_Data.m_Vector2;   break;
        case type::VECTOR3:     m_Data.m_Vector3    = A.m_Data.m_Vector3;   break;
        default: x_assert( false );
    }  

    return *this; 
}

//------------------------------------------------------------------------------------------------
x_inline
void xproperty_data::RegisterTypes   ( xtextfile& DataFile ) 
{
    for( int i=1; i<getTable().getCount(); i++ )
    {
        auto& Entry = getTable()[i];
        DataFile.AddUserType( Entry.m_pTextFileType, &Entry.m_pStringType[x_constStrLength("Value:")], static_cast<int>(Entry.m_EnumType) );
    }
}
    
//------------------------------------------------------------------------------------------------
x_inline
void xproperty_data::SerializeOut    ( xtextfile& DataFile ) const
{
    DataFile.WriteField( "Type:<?>", getWriteTypeString()    );
    DataFile.WriteField( "Name:s",   (const char*)m_Name );

    static const char* pValueType = "Value:<Type>";
    switch( m_Type )
    {
        case type::BOOL:     DataFile.WriteField( pValueType, m_Data.m_Bool );                                                                                          break;
        case type::BUTTON:   DataFile.WriteField( pValueType, (const char*) m_Data.m_Button );                                                                          break;
        case type::RGBA:    DataFile.WriteField( pValueType, m_Data.m_Color.m_R, m_Data.m_Color.m_G, m_Data.m_Color.m_B, m_Data.m_Color.m_A );                         break;
        case type::ENUM:     DataFile.WriteField( pValueType, (const char*)m_Data.m_Enum.m_String );                                                                    break;
        case type::INT:      DataFile.WriteField( pValueType, m_Data.m_S32.m_Value );                                                                                   break;
        case type::FLOAT:    DataFile.WriteField( pValueType, m_Data.m_F32.m_Value );                                                                                   break;
        case type::ANGLES3:  DataFile.WriteField( pValueType, m_Data.m_Radian3.m_Value.m_Pitch.getDegrees().m_Value,
                                                              m_Data.m_Radian3.m_Value.m_Yaw.getDegrees().m_Value,
                                                              m_Data.m_Radian3.m_Value.m_Roll.getDegrees().m_Value   );                                                 break;
        case type::STRING:   DataFile.WriteField( pValueType, (const char*) m_Data.m_String );                                                                          break;
        case type::GUID:     DataFile.WriteField( pValueType, m_Data.m_Guid.m_Value );                                                                                  break;
        case type::VECTOR2:  DataFile.WriteField( pValueType, m_Data.m_Vector2.m_Value.m_X, m_Data.m_Vector2.m_Value.m_Y  );                                            break;
        case type::VECTOR3:  DataFile.WriteField( pValueType, m_Data.m_Vector3.m_Value.m_X, m_Data.m_Vector3.m_Value.m_Y, m_Data.m_Vector3.m_Value.m_Z  );              break;
        default: x_assert( false );
    }

    DataFile.WriteLine  ();
}

//------------------------------------------------------------------------------------------------
x_inline
void xproperty_data::SerializeIn    ( xtextfile& DataFile )
{
    xstring         TypeUID;
    xstring         PropName;

    auto Type = static_cast<type>( DataFile.ReadField( "Type:<?>", &TypeUID ) + 1 );
    x_assert( static_cast<int>(Type) >= static_cast<int>(type::INVALID) );
    x_assert( static_cast<int>(Type) <  static_cast<int>(type::ENUM_COUNT) );
    setup( Type );

    DataFile.ReadFieldXString( "Name:s", m_Name );

    switch( m_Type )
    {
        case type::BOOL:    DataFile.ReadField( getReadTypeString(),        &m_Data.m_Bool );                                                                                   break;
        case type::BUTTON:  DataFile.ReadFieldXString( getReadTypeString(), m_Data.m_Button       );                                                                            break;
        case type::RGBA:   DataFile.ReadField( getReadTypeString(),        &m_Data.m_Color.m_R, &m_Data.m_Color.m_G, &m_Data.m_Color.m_B, &m_Data.m_Color.m_A );               break;
        case type::ENUM:    DataFile.ReadFieldXString( getReadTypeString(), m_Data.m_Enum.m_String );                                                                           break;
        case type::INT:     DataFile.ReadField( getReadTypeString(),        &m_Data.m_S32.m_Value  );                                                                           break;
        case type::FLOAT:   DataFile.ReadField( getReadTypeString(),        &m_Data.m_F32.m_Value  );                                                                           break;
        case type::ANGLES3: 
        {
            xdegree Pitch, Yaw, Roll;
            DataFile.ReadField( getReadTypeString(), &Pitch.m_Value, &Yaw.m_Value, &Roll.m_Value  );
            m_Data.m_Radian3.m_Value.setup( xradian{ Pitch }, xradian{ Yaw }, xradian{ Roll } );
            break;
        }
        case type::STRING:  DataFile.ReadFieldXString( getReadTypeString(), m_Data.m_String       );                                                                            break;
        case type::GUID:    DataFile.ReadField( getReadTypeString(),        &m_Data.m_Guid.m_Value );                                                                           break;
        case type::VECTOR2: DataFile.ReadField( getReadTypeString(),        &m_Data.m_Vector2.m_Value.m_X, &m_Data.m_Vector2.m_Value.m_Y );                                     break;
        case type::VECTOR3: DataFile.ReadField( getReadTypeString(),        &m_Data.m_Vector3.m_Value.m_X, &m_Data.m_Vector3.m_Value.m_Y, &m_Data.m_Vector3.m_Value.m_Z );      break;
        default: x_assert( false );
    }
}

//------------------------------------------------------------------------------------------------
x_inline
typename xproperty_data::t_self& xproperty_data::setup( type Type )
{
    if( m_Type == Type ) return *this;
    //
    // Release formats if we have to
    //
    switch( m_Type )
    {
        case type::BUTTON: m_Data.m_Button.~xstring_base();             break;
        case type::STRING: m_Data.m_String.~xstring_base();             break;
        case type::ENUM:   m_Data.m_Enum.m_String.~xstring_base();      break;
        default: break;
    }

    //
    // Prepare formats if we have to
    //
  
    switch( Type )
    {
        case type::BUTTON: (void)x_construct( xstring,    &m_Data.m_Button ); break;
        case type::STRING: (void)x_construct( xstring,    &m_Data.m_String ); break;
        case type::ENUM:   (void)x_construct( data_enum,  &m_Data.m_Enum );   break;
        default: break;
    }
     
    //
    // Finally set the type
    //
    m_Type = Type;

    return *this;
}

//------------------------------------------------------------------------------------------------
x_inline
bool xproperty_data::operator == ( const xproperty_data& B ) const
{
    if( m_Type != B.m_Type ) return false;
    if( m_Name != B.m_Name ) return false;

    switch( m_Type )
    {
        case type::BOOL:    return m_Data.m_Bool                            == B.m_Data.m_Bool;
        case type::BUTTON:  return m_Data.m_Button                          == B.m_Data.m_Button;
        case type::RGBA:    return m_Data.m_Color.m_Value                   == B.m_Data.m_Color.m_Value;
        case type::ENUM:    return m_Data.m_String                          == B.m_Data.m_String;
        case type::INT:     return m_Data.m_S32.m_Value                     == B.m_Data.m_S32.m_Value;
        case type::FLOAT:   return m_Data.m_F32.m_Value                     == B.m_Data.m_F32.m_Value;
        case type::ANGLES3: return m_Data.m_Radian3.m_Value                 == B.m_Data.m_Radian3.m_Value; 
        case type::STRING:  return m_Data.m_String                          == B.m_Data.m_String;
        case type::GUID:    return m_Data.m_Guid.m_Value                    == B.m_Data.m_Guid.m_Value;
        case type::VECTOR2: return m_Data.m_Vector2.m_Value                 == B.m_Data.m_Vector2.m_Value;
        case type::VECTOR3: return m_Data.m_Vector3.m_Value                 == B.m_Data.m_Vector3.m_Value;
        default: x_assert( false );
    }

    return false;
}

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY 
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------
x_inline
void xproperty::PropEnum ( xfunction<void( xproperty_enum&  Enum,  xproperty_data& Data)> Function, bool bFullPath, xproperty_enum::mode Mode ) const
{
    xproperty_enum Enum{ Function, Mode };
    Enum.setForceFullPaths( bFullPath );
    onPropEnum( Enum );
}

//------------------------------------------------------------------------------------------------
x_inline
bool xproperty::PropQueryGet( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function ) const
{
    xproperty_query Query;
    return Query.RunGet( const_cast<xproperty&>(*this), Function );
}

//------------------------------------------------------------------------------------------------
x_inline
bool xproperty::PropQuerySet( xfunction<bool( xproperty_query& Query, xproperty_data& PropertyToQuery )> Function )
{
    xproperty_query Query;
    return Query.RunSet( *this, Function );
}

//------------------------------------------------------------------------------------------------
x_inline
void xproperty::PropToCollection( xproperty_data_collection& Collection, bool bFullPath ) const
{
    //
    // Enumerate all the properties
    //
    PropEnum( [&]( xproperty_enum& Enum, xproperty_data& Data )
    {
        // Just collect them all inside an static array
        Collection.m_List.append() = Data;
        
    }, bFullPath );

    //
    // Test getting values
    //
    PropQueryGet( [&]( xproperty_query& Query, xproperty_data& Data )
    {
        //
        // First read the results if any
        //
        if( Query.hasResults() )
        {
            Collection.m_List[ Query.getResultsIndex() ] = Query.getResults();
        }

        //
        // are we done?
        //
        if( Query.getPropertyIndex() == Collection.m_List.getCount() ) 
        {
            return false;
        }

        //
        // Tell which property to get next
        //
        Data.m_Name  = Collection.m_List[ Query.getPropertyIndex() ].m_Name;
        Data.m_Flags = Collection.m_List[ Query.getPropertyIndex() ].m_Flags;

        return true;
    });
}


//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY QUERY
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------

template< typename T_CHAR, u32 T_CRC, int T_LENGTH > x_inline 
bool xproperty_query::isVar( xstring_crcinfo<T_CHAR, T_CRC,T_LENGTH> Node )
{
    x_assert( m_Flags.m_STATE == state::SEARCHING );
    x_assert( isSearchingForScopes() == false );

    if( T_CRC != m_CurPropNextCRC )
        return false;

    m_bPropFound = true;
    return true;
}

//------------------------------------------------------------------------------------------------

template< typename T_CHAR, u32 T_CRC, int T_LENGTH > x_inline 
bool xproperty_query::isScope( xstring_crcinfo<T_CHAR, T_CRC, T_LENGTH> Node )
{
    x_assert( m_Flags.m_STATE == state::SEARCHING );
    x_assert( isSearchingForScopes() == true );

    if( T_CRC != m_CurPropNextCRC )
        return false;

    // officially move to the next scope
    m_ScopeData[m_iScope].m_CRC                 = T_CRC;
    m_ScopeData[m_iScope].m_ArrayIndex          = m_CurPropArrayIndex;
#if _X_DEBUG
    m_ScopeData[m_iScope].m_DEBUG_pScopeName    = Node.m_pDEBUG_Data;
    m_ScopeData[m_iScope].m_DEBUG_Length        = T_LENGTH;
#endif
    m_iScope++;

    // Get ready for the next scope/property
    PrepareNextScope();
    return true;
}

//------------------------------------------------------------------------------------------------

template< typename T > x_forceinline
xproperty_data& xproperty_query::setProp( void ) 
{
    x_assert( false == isReciving() );
    auto& Prop = getProperty();
    Prop.setup( T::t_symbol ); 
    return Prop;
}


//------------------------------------------------------------------------------------------------
x_inline
void xproperty_query::Reset( void )
{
    m_Flags.m_STATE             = state::SEARCHING;
    m_Flags.m_IS_SEARCH_SCOPES  = true;
    m_iScope                    = 0;
    m_nProccessedProperties     = -1;
}

//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
// PROPERTY ENUM
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------
template< typename T > x_forceinline
void xproperty_enum::scope::AddProperty( xstring::const_char* pPropertyName, flags Flags )
{
    static_assert( T::t_symbol != xproperty_data::type::INVALID, "This symbol is not supported as a property type" );
    AddProperty( pPropertyName, T::t_symbol, Flags );
}

//------------------------------------------------------------------------------------------------

void xproperty_enum::scope::beginScope ( xstring::const_char* pScopeName, xfunction<void(scope& Scope)> CallBack )
{
    auto& Entry = m_Enum.m_Scope[++m_Enum.m_iScope];
    Entry.m_pName       = pScopeName;
    Entry.m_Sequence    = m_Enum.m_Sequence++;
    CallBack( *this );

    // clean entry when we leave
    --m_Enum.m_iScope;
    Entry.m_ArrayIndex  = -1;
    Entry.m_ArrayCount  = -1;
}

//------------------------------------------------------------------------------------------------

void xproperty_enum::scope::beginScopeArray ( xstring::const_char* pScopeName, int Count, xfunction<void(scope_array& ScopeArray)> CallBack )
{
    auto& Entry = m_Enum.m_Scope[++m_Enum.m_iScope];
    Entry.m_pName       = pScopeName;
    Entry.m_Sequence    = m_Enum.m_Sequence++;
    Entry.m_ArrayCount  = Count;

    // output the count
    AddProperty<xprop_s32>( X_STR("[]Count") );

    // handle the user 
    CallBack( reinterpret_cast<scope_array&>(*this) );
    --m_Enum.m_iScope;

    // clean entry when we leave
    Entry.m_ArrayIndex  = -1;
    Entry.m_ArrayCount  = -1;
}

//------------------------------------------------------------------------------------------------

void xproperty_enum::scope_array::beginScopeArrayEntry   ( int Index, xfunction<void(scope& Scope)> CallBack )
{
    CMD_DEBUG( const int ArrayCount = m_Enum.m_Scope[m_Enum.m_iScope].m_ArrayCount );
    x_assert( Index < ArrayCount );
    auto& Entry = m_Enum.m_Scope[m_Enum.m_iScope];
    Entry.m_ArrayIndex  = Index;

    CallBack( reinterpret_cast<scope&>(*this) );
}



