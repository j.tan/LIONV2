//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//


//------------------------------------------------------------------------------
// Description:
//      Share functionality across all the linear allocators
//------------------------------------------------------------------------------
#ifdef x_linear_constructs_buffer_hardness

//------------------------------------------------------------------------------
// Checks to make sure if the memory which is own by the class is in a valid state
public: constexpr 
bool isValid ( void ) const noexcept 
{ 
    return m_pMem != nullptr; 
}

//------------------------------------------------------------------------------
// Checks to see if it is null
public: constexpr 
bool isNull ( void ) const noexcept 
{ 
    return m_pMem == nullptr; 
}

//------------------------------------------------------------------------------
public: template< typename T > constexpr 
bool isIndexValid( const T Index ) const   noexcept 
{ 
    return Index >= 0 && Index < getCount(); 
}

//------------------------------------------------------------------------------
public: constexpr   
t_counter getIndexByEntry ( const t_entry& Entry ) const noexcept
{ 
    x_assert( isIndexValid( static_cast<xuptr>( &Entry - m_pMem) ) ); 
    return static_cast<t_counter>( &Entry-m_pMem ); 
}
    
//------------------------------------------------------------------------------
public: inline
// Safely copies items from a view to destination of this buffer      
void CopyToFrom( const xbuffer_view<t_entry> View, 
                 const t_counter             DestinationOffset = static_cast<t_counter>(0) ) noexcept
{
    x_assert( (DestinationOffset + View.getCount()) <= getCount() );
    memmove( &m_pMem[DestinationOffset], View, View.getByteSize() );
}

//------------------------------------------------------------------------------
public:       
template< typename T > inline     
t_view ViewFromRange( T iStart, T iEnd )
{
    x_assert( iStart >= 0 );
    x_assert( iEnd <= getCount() );
    x_assert( iStart <= iEnd );
    return { &m_pMem[ iStart ], static_cast<t_counter>(iEnd - iStart) };
}

//------------------------------------------------------------------------------
public:       
template< typename T > inline     
t_view ViewFrom( T iStart )
{
    x_assert( iStart >= 0 );
    x_assert( iStart <= getCount() );
    return { &m_pMem[ iStart ], (getCount() - static_cast<t_counter>(iStart)) };
}

//------------------------------------------------------------------------------
public: 
template< typename T > inline    
t_view ViewTo( T iEnd )
{
    x_assert( iEnd <= getCount() );
    return { m_pMem, static_cast<t_counter>(iEnd) };
}

//------------------------------------------------------------------------------
public: inline      
void QSort( xfunction<int(const t_entry&,const t_entry&)> Function ) noexcept
{ 
    x_qsort( 
        m_pMem, 
        getCount(), 
        sizeof(t_entry), 
        [&Function] ( const void* pA, const void* pB ) -> int 
        { 
            return Function( *(t_entry*)pA, *(t_entry*)pB ); 
        } ); 
}

//------------------------------------------------------------------------------

template< class KEY > inline
bool BinarySearch( const KEY Key, int& Index ) const
{
    const int   Count = getCount<int>();
    int         From  = 0;
    int         To     = Count - 1;
    
    // Search for the Node
    while( From <= To )
    {
        Index = ( To + From ) >> 1;
        
        if( Key < m_pMem[ Index ] )
        {
            To = Index - 1;
        }
        else
        {
            if( m_pMem[ Index ] < Key )
            {
                From = Index + 1;
            }
            else
            {
                return true;
            }
        }
    }
    
    if( Count <= 0 )          Index = 0;
    else if( From > Index )   Index = From;
    
    return false;
}

//------------------------------------------------------------------------------
public:      
template< typename T, typename = std::enable_if< !std::is_polymorphic<t_entry>::value > >
void MemSet( const T C ) noexcept
{ 
    x_assume(m_pMem);
    x_assume( x_hasvirtual(t_entry) == false );

    memset( 
        m_pMem, 
        C, 
        sizeof(t_entry) * getCount() ); 
}

//------------------------------------------------------------------------------
public: constexpr 
bool Belongs( const void* pPtr )    const   noexcept
{ 
    return pPtr >= static_cast<const void*>(m_pMem) && 
           pPtr <  static_cast<const void*>(m_pMem + getCount() ); 
}

//------------------------------------------------------------------------------
public: constexpr   
t_counter getByteSize ( void ) const noexcept
{ 
    return sizeof(t_entry) * getCount(); 
}

//------------------------------------------------------------------------------
public: constexpr   
int getEntrySize ( void ) const noexcept
{ 
    return sizeof(t_entry); 
}

//------------------------------------------------------------------------------
public: template< typename T_INDEX > constexpr  
const t_entry&  operator [] ( const T_INDEX Index ) const   noexcept    
{
    return 
        x_assume( Index >= static_cast<T_INDEX>(0) && static_cast<t_counter>(Index) < getCount() ), 
        m_pMem[static_cast<xuptr>(Index)]; 
}

//------------------------------------------------------------------------------
public: template< typename T_INDEX > inline     
t_entry&  operator [] ( const T_INDEX Index ) noexcept    
{ 
    x_assume( Index >= static_cast<T_INDEX>(0) && static_cast<t_counter>(Index) < getCount() ); 
    return m_pMem[static_cast<xuptr>(Index)]; 
}

//------------------------------------------------------------------------------
public: constexpr
operator const t_view ( void ) const  noexcept
{ 
    return { const_cast<t_entry*>(m_pMem), getCount() };           
}

//------------------------------------------------------------------------------
#ifdef x_linear_constructs_buffer_hardness_exclude_operator_view
    #undef x_linear_constructs_buffer_hardness_exclude_operator_view
#else
    #undef x_linear_constructs_buffer_hardness_exclude_operator_view
    public: x_forceinline
    operator t_view ( void ) noexcept
    { 
        return { m_pMem, getCount() };
    }
#endif

//------------------------------------------------------------------------------
public: constexpr 
operator  const t_entry*    ( void ) const   noexcept    
{ 
    return 
        x_assume(m_pMem),
        m_pMem;
}

//------------------------------------------------------------------------------
public: inline 
operator  t_entry* ( void ) noexcept    
{ 
    return 
        x_assume(m_pMem),
        m_pMem;
}

//------------------------------------------------------------------------------
public: inline      
x_iter< t_self > begin ( void ) noexcept 
{ 
    return { 0,*this  };
}

//------------------------------------------------------------------------------
public: inline      
x_iter< t_self > end ( void ) noexcept 
{ 
    return {*this     };
}

//------------------------------------------------------------------------------
public: constexpr   
x_iter< const t_self > begin ( void ) const   noexcept 
{ 
    return { 0,*this  };
}

//------------------------------------------------------------------------------
public: constexpr 
x_iter< const t_self > end ( void ) const   noexcept    
{ 
    return { *this    };                      
}

//------------------------------------------------------------------------------
public: inline 
x_iter_r< t_self > rbegin ( void ) noexcept 
{ 
    return { getCount()-1, *this      };      
}

//------------------------------------------------------------------------------
public: inline
x_iter_r< t_self > rend ( void ) noexcept 
{ 
    return { *this };
}

//------------------------------------------------------------------------------
public: constexpr   
x_iter_r< const t_self > rbegin ( void ) const   noexcept
{ 
    return { getCount()-1, *this      };      
}

//------------------------------------------------------------------------------
public: constexpr   
x_iter_r< const t_self > rend ( void ) const   noexcept 
{ 
    return { *this };
}

//------------------------------------------------------------------------------
#undef x_linear_constructs_buffer_hardness
#endif
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

