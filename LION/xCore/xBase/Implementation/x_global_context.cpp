//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "x_base.h"

//
// Will output memory leaks and such
//
#if _X_TARGET_WINDOWS && _X_DEBUG
    #define CRTDBG_MAP_ALLOC
    #include <stdlib.h>
    #include <crtdbg.h>
#endif

void __x_file_init  ( g_context& GContext );
void __x_file_kill  ( g_context& GContext );
void __x_net_init   ( g_context& GContext );
void __x_net_kill   ( g_context& GContext );

//------------------------------------------------------------------------------
// Description:
//      Official function
//------------------------------------------------------------------------------

class pow2_small_allocs 
{
public:

    using alloc_type = g_context::small_alloc_type;

    inline alloc_type Alloc( xbyte*& pPtr, units_bytes Size )
    {
        const alloc_type AllocType = static_cast<alloc_type>( x_Log2IntRoundUp( Size.m_Value ) - 3 );
        switch( AllocType )
        {
            case alloc_type::POW8    : pPtr = m_Pool8.pop().m_Memory;     break;
            case alloc_type::POW16   : pPtr = m_Pool16.pop().m_Memory;    break;
            case alloc_type::POW32   : pPtr = m_Pool32.pop().m_Memory;    break;
            case alloc_type::POW64   : pPtr = m_Pool64.pop().m_Memory;    break;
            case alloc_type::POW128  : pPtr = m_Pool128.pop().m_Memory;   break;
            case alloc_type::POW256  : pPtr = m_Pool256.pop().m_Memory;   break;
            case alloc_type::POW512  : pPtr = m_Pool512.pop().m_Memory;   break;
            case alloc_type::POW1024 : pPtr = m_Pool1024.pop().m_Memory;  break;
            case alloc_type::POW2048 : pPtr = m_Pool2048.pop().m_Memory;  break;
            default: 
                pPtr = reinterpret_cast<xbyte*>(g_context::get().aligned_alloc( Size.m_Value, 8 )); 
                return alloc_type::FULL_ALLOCATION;
        }
       
        return AllocType;
    }

    inline void Free( xbyte* pPtr, alloc_type Type )
    {
        switch( Type )
        {
            case alloc_type::POW8    : m_Pool8.push   ( *reinterpret_cast<block<  8>*>  (pPtr) ); break;
            case alloc_type::POW16   : m_Pool16.push  ( *reinterpret_cast<block< 16>*>  (pPtr) ); break;
            case alloc_type::POW32   : m_Pool32.push  ( *reinterpret_cast<block< 32>*>  (pPtr) ); break;
            case alloc_type::POW64   : m_Pool64.push  ( *reinterpret_cast<block< 64>*>  (pPtr) ); break;
            case alloc_type::POW128  : m_Pool128.push ( *reinterpret_cast<block<128>*>  (pPtr) ); break;
            case alloc_type::POW256  : m_Pool256.push ( *reinterpret_cast<block<256>*>  (pPtr) ); break;
            case alloc_type::POW512  : m_Pool512.push ( *reinterpret_cast<block<512>*>  (pPtr) ); break;
            case alloc_type::POW1024 : m_Pool1024.push( *reinterpret_cast<block<1024>*> (pPtr) ); break;
            case alloc_type::POW2048 : m_Pool2048.push( *reinterpret_cast<block<2048>*> (pPtr) ); break;
            case alloc_type::FULL_ALLOCATION : g_context::get().aligned_free( pPtr ); break;
            default: x_assume( false );
        }
    }

    void SanityCheck( void )
    {
        m_Pool8.SanityCheck();
        m_Pool16.SanityCheck();
        m_Pool32.SanityCheck();
        m_Pool64.SanityCheck();
        m_Pool128.SanityCheck();
        m_Pool256.SanityCheck();
        m_Pool512.SanityCheck();
        m_Pool1024.SanityCheck();
        m_Pool2048.SanityCheck();
    }

    template< int T_SIZE >
    struct alignas(u32) block
    {
        xbyte   m_Memory[T_SIZE]; 
    };

    x_ll_page_pool< block<8>,    2048 >    m_Pool8      {};
    x_ll_page_pool< block<16>,   1024 >    m_Pool16     {};
    x_ll_page_pool< block<32>,    512 >    m_Pool32     {};
    x_ll_page_pool< block<64>,    256 >    m_Pool64     {};
    x_ll_page_pool< block<128>,   128 >    m_Pool128    {};
    x_ll_page_pool< block<256>,   128 >    m_Pool256    {};
    x_ll_page_pool< block<512>,   128 >    m_Pool512    {};
    x_ll_page_pool< block<1024>,  128 >    m_Pool1024   {};
    x_ll_page_pool< block<2048>,  128 >    m_Pool2048   {};
};
//------------------------------------------------------------------------------
static inline
void* s_aligned_alloc( xuptr Size, xuptr Alignment )
{
#ifdef _WIN32
    return _aligned_malloc( Size, Alignment );
#else
    void* pPtr;
    posix_memalign( &pPtr, Alignment, Size);
    return pPtr;
#endif
}

//------------------------------------------------------------------------------

static inline  
void s_aligned_free( void* pPtr )
{
#ifdef _WIN32
    _aligned_free( pPtr );
#else
    free( pPtr );
#endif
}

//------------------------------------------------------------------------------
static inline
void* s_aligned_realloc( void* pPtr, xuptr Size, xuptr Alignment )
{
#ifdef _WIN32
    return _aligned_realloc( pPtr, Size, Alignment );
#else
    // TODO: HACK: Hacky realloc
    void* pNew = s_aligned_alloc( Size, Alignment );
    x_memcpy( pNew, pPtr, Size );
    s_aligned_free( pPtr );
    return nullptr;
#endif
}

//--------------------------------------------------------------------------------------
// GLOBAL CONTEXT
//--------------------------------------------------------------------------------------
g_context* g_context::m_pInstance = nullptr;
bool       g_context::m_bImported = false;

//--------------------------------------------------------------------------------------

g_context::g_context( void ) :
    m_GuidGeneration    ( *x_construct( xguid64_generation, s_aligned_alloc( sizeof(xguid64_generation),  alignof(xguid64_generation)    ) ) ),
    m_Scheduler         ( *x_construct( x_scheduler,         s_aligned_alloc( sizeof(x_scheduler),          alignof(x_scheduler)            ) ) ),
    m_SmallBlockPools   ( *x_construct( pow2_small_allocs,    s_aligned_alloc( sizeof(pow2_small_allocs),    alignof(pow2_small_allocs)     ) ) )
{
    x_assume(!m_pInstance);
    m_pInstance = this;

    __x_file_init( *this );
    __x_net_init( *this );
}

//--------------------------------------------------------------------------------------

g_context::~g_context( void )
{
    if( m_bImported == true )
        return;

    __x_net_kill( *this );
    __x_file_kill( *this );

    x_delete( &m_Scheduler );
    x_delete( &m_GuidGeneration );
    x_delete( &m_SmallBlockPools );
}

//------------------------------------------------------------------------------
// Description:
//      These functions should be standard in T_ALLOCATOR++11 but compilers seem to not have them
//------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------

void* g_context::aligned_alloc( xuptr Size, xuptr Alignment )
{
    return s_aligned_alloc( Size, Alignment );
}

//--------------------------------------------------------------------------------------

void g_context::aligned_free( void* pPtr )
{
    s_aligned_free( pPtr );
}

//--------------------------------------------------------------------------------------

void* g_context::aligned_realloc( void* pPtr, xuptr Size, xuptr Aligment )
{
    return s_aligned_realloc( pPtr, Size, Aligment );
}

//------------------------------------------------------------------------------

g_context::small_alloc_type g_context::SmallAlloc ( xbyte*& Ptr, units_bytes Size )
{
    return m_SmallBlockPools.Alloc( Ptr, Size );
}

//------------------------------------------------------------------------------

void g_context::SmallFree ( xbyte*  Ptr, small_alloc_type Type )
{
    m_SmallBlockPools.Free( Ptr, Type );
}

//------------------------------------------------------------------------------

void g_context::SanityCheck( void )
{
    m_SmallBlockPools.SanityCheck();
}

//------------------------------------------------------------------------------
// Description:
//      Replace the global new and delete with ours
//------------------------------------------------------------------------------
void g_context::Init( void )
{
    //
    // For memory leak detection
    //
#if _X_TARGET_WINDOWS && _X_DEBUG
    _CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
    //_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );
  //  _CrtSetBreakAlloc(241);
#endif

    //
    // Build an instance of the system
    //
    (void)x_construct( g_context, s_aligned_alloc( sizeof(g_context), alignof(g_context) ) );
}

//------------------------------------------------------------------------------

void g_context::Kill( void )
{
    if(m_pInstance && (m_bImported == false) )
    {
        x_destruct( m_pInstance );
        s_aligned_free( m_pInstance );
    }
    m_pInstance = nullptr;
}

//------------------------------------------------------------------------------
// Description:
//      Replace the global new and delete with ours
//------------------------------------------------------------------------------
void*   operator new        ( std::size_t n )                           throw(std::bad_alloc)   { return g_context::get().aligned_alloc( n, 8 );    }
void*   operator new[]      ( std::size_t n )                           throw(std::bad_alloc)   { return g_context::get().aligned_alloc( n, 8 );    }
void*   operator new        ( std::size_t n, const std::nothrow_t& )    noexcept                { return g_context::get().aligned_alloc( n, 8 );    }
void    operator delete     ( void* p )                                 throw()                 { g_context::get().aligned_free( p );               }
void    operator delete[]   ( void* p )                                 throw()                 { g_context::get().aligned_free( p );               }

