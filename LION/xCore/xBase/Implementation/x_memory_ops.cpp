//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "x_base.h"
#include "lz4h/lz4.h"
#include "lz4h/lz4hc.h"


//------------------------------------------------------------------------------

void x_DecompressMem ( xbuffer_view<xbyte> DestinationUncompress, const xbuffer_view<xbyte> SourceCompressed  ) noexcept 
{
    x_assert( DestinationUncompress.isValid() );
    x_assert( SourceCompressed.isValid() );
    x_assert( DestinationUncompress.getCount() >= SourceCompressed.getCount() );

    if( DestinationUncompress.getCount() == SourceCompressed.getCount() )
    {
        DestinationUncompress.CopyToFrom( SourceCompressed );
    }
    else
    {
        const auto Total = LZ4_decompress_safe( (const char*)&SourceCompressed[0], (char*)&DestinationUncompress[0], x_static_cast<int>(SourceCompressed.getCount()), x_static_cast<int>(DestinationUncompress.getCount()) );
        //const auto Total = LZ4_decompress_fast( (const char*)&SourceCompressed[0], (char*)&DestinationUncompress[0], x_static_cast<int>(DestinationUncompress.getCount()) );
        (void)Total;
        x_assert( DestinationUncompress.getCount() == Total  );
    }
}

//------------------------------------------------------------------------------

xuptr x_CompressMem( xbuffer_view<xbyte> DestinationCompress, const xbuffer_view<xbyte> SourceUncompress ) noexcept
{
    x_assert( DestinationCompress.isValid() );
    x_assert( SourceUncompress.isValid() );
    x_assert( DestinationCompress.getCount() >= SourceUncompress.getCount() );

    const auto Length = LZ4_compressHC_limitedOutput( 
        (const char*)&SourceUncompress[0], 
        (char*)&DestinationCompress[0], 
         x_static_cast<int>(SourceUncompress.getCount()), 
         x_static_cast<int>(SourceUncompress.getCount()) );
    x_assert( Length <= DestinationCompress.getCount() );

    // Let the user deal with the problem
    if( Length >= SourceUncompress.getCount() || Length == 0 )
    {
        DestinationCompress.CopyToFrom( SourceUncompress );
        return SourceUncompress.getCount();
    }
    
    return Length;
}








