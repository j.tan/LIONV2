//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// VARIABLES
//==============================================================================

static const xquaternion q_Identity( 0,0,0,1 );

//==============================================================================
// FUNCTIONS
//==============================================================================


//------------------------------------------------------------------------------
inline
xquaternion::xquaternion( const xradian3& R )
{
    x_assert( x_isAlign( this, 16 ) ); 
    setup( R );
}

//------------------------------------------------------------------------------
inline
xquaternion::xquaternion( const xvector3& Axis, xradian Angle )
{
    x_assert( x_isAlign( this, 16 ) ); 
    setup( Axis, Angle );
}

//------------------------------------------------------------------------------
inline
xquaternion::xquaternion( const xmatrix4& M )
{
    setup( M );
}

//------------------------------------------------------------------------------
inline
void xquaternion::setIdentity( void )
{
    *this = q_Identity;
}

//------------------------------------------------------------------------------
inline
void xquaternion::setZero( void )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_setzero_ps();
#else
    m_X = 0.0f;
    m_Y = 0.0f;
    m_Z = 0.0f;
    m_W = 0.0f;
#endif
}

//------------------------------------------------------------------------------
inline static
void q_Multiply( xquaternion& Dest, const xquaternion& Q1, const xquaternion& Q2 )
{
#ifdef _X_SSE2_SUPPORT

    const f32x4 w = _mm_mul_ps( Q1.m_XYZW, Q2.m_XYZW );

    Dest = xquaternion
    {
        _mm_sub_ps( _mm_add_ps( _mm_add_ps(
        _mm_mul_ps( Q2.m_XYZW, _mm_set1_ps( Q1.m_W ) ),
        _mm_mul_ps( Q1.m_XYZW, _mm_set1_ps( Q2.m_W ) )),
        _mm_mul_ps( _mm_shuffle_ps( Q1.m_XYZW, Q1.m_XYZW, _MM_SHUFFLE(3,0,2,1) ), _mm_shuffle_ps( Q2.m_XYZW, Q2.m_XYZW, _MM_SHUFFLE(3,1,0,2) ))),
        _mm_mul_ps( _mm_shuffle_ps( Q1.m_XYZW, Q1.m_XYZW, _MM_SHUFFLE(3,1,0,2) ), _mm_shuffle_ps( Q2.m_XYZW, Q2.m_XYZW, _MM_SHUFFLE(3,0,2,1) )) )
    };
    Dest.m_W = ((f32*)&w)[3] - ((f32*)&w)[2] - ((f32*)&w)[1] - ((f32*)&w)[0];

#else
    Dest = xquaternion
    {
        (Q1.m_W * Q2.m_X) + (Q2.m_W * Q1.m_X) + (Q1.m_Y * Q2.m_Z) - (Q1.m_Z * Q2.m_Y),
        (Q1.m_W * Q2.m_Y) + (Q2.m_W * Q1.m_Y) + (Q1.m_Z * Q2.m_X) - (Q1.m_X * Q2.m_Z),
        (Q1.m_W * Q2.m_Z) + (Q2.m_W * Q1.m_Z) + (Q1.m_X * Q2.m_Y) - (Q1.m_Y * Q2.m_X),
        (Q1.m_W * Q2.m_W) - (Q2.m_X * Q1.m_X) - (Q1.m_Y * Q2.m_Y) - (Q1.m_Z * Q2.m_Z)
    };
#endif
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::RotateX( xradian Rx )
{
    f32 s, c;
    x_SinCos( Rx / xradian{ 2.0f }, s, c );
    q_Multiply( *this, xquaternion( s, 0, 0, c), *this );
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::RotateY( xradian Ry )
{
    f32 s, c;
    x_SinCos( Ry / xradian{ 2.0f }, s, c );
    q_Multiply( *this, xquaternion( 0, s, 0, c), *this );
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::RotateZ( xradian Rz )
{
    f32 s, c;
    x_SinCos( Rz / xradian{ 2.0f }, s, c );
    q_Multiply( *this, xquaternion( 0, 0, s, c), *this );
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::PreRotateX( xradian Rx )
{
    f32 s, c;
    x_SinCos( Rx / xradian{ 2.0f }, s, c );
    q_Multiply( *this, *this, xquaternion( s, 0, 0, c) );
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::PreRotateY( xradian Ry )
{
    f32 s, c;
    x_SinCos( Ry / xradian{ 2.0f }, s, c );
    q_Multiply( *this, *this, xquaternion( 0, s, 0, c) );
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::PreRotateZ( xradian Rz )
{
    f32 s, c;
    x_SinCos( Rz / xradian{ 2.0f }, s, c );
    q_Multiply( *this, *this, xquaternion( 0, 0, s, c) );
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::setRotationX( xradian Rx )
{
    f32 s, c;
    x_SinCos( Rx / xradian{ 2.0f }, s, c );
    setup( s, 0, 0, c);
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::setRotationY( xradian Ry )
{
    f32 s, c;
    x_SinCos( Ry / xradian{ 2.0f }, s, c );
    setup( 0, s, 0, c);
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::setRotationZ( xradian Rz )
{
    f32 s, c;
    x_SinCos( Rz / xradian{ 2.0f }, s, c );
    setup( 0, 0, s, c);
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::Conjugate( void )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW     = _mm_sub_ps( _mm_setzero_ps(), m_XYZW);
    m_W        = -m_W;
#else
    m_X = -m_X;
    m_Y = -m_Y;
    m_Z = -m_Z;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::Invert( void )
{              
    const f32 n = LengthSquared();
    if( n > 0.0f ) (*this) /= n;
    Conjugate();   
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::setup( const xradian3& R )
{
    setIdentity();
    RotateZ( R.m_Roll  );
    RotateX( R.m_Pitch );
    RotateY( R.m_Yaw   );
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::setup( f32 X, f32 Y, f32 Z, f32 W )
{
    m_X = X;
    m_Y = Y;
    m_Z = Z;
    m_W = W;
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::setup( const xvector3& Axis, xradian Angle )
{
    f32 s, c;
    x_SinCos( Angle * xradian{ 0.5f }, s, c );
    setup(s*Axis.m_X, s*Axis.m_Y, s*Axis.m_Z, c );
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::setup( const xvector3& StartDir, const xvector3& EndDir )
{
    xvector3    Axis;
    xradian     Angle;

    StartDir.getRotationTowards( EndDir, Axis, Angle );
    setup( Axis, Angle );

    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion operator * ( const xquaternion& Q1, const xquaternion& Q2 )
{
    xquaternion Dest;
    q_Multiply( Dest, Q1, Q2 );
    return Dest;
}

//------------------------------------------------------------------------------
inline 
const xquaternion& xquaternion::operator += ( const xquaternion& Q )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_add_ps( m_XYZW, Q.m_XYZW );
#else
    m_X += Q.m_X;
    m_Y += Q.m_Y;
    m_Z += Q.m_Z;
    m_W += Q.m_W;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xquaternion& xquaternion::operator -= ( const xquaternion& Q )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_sub_ps( m_XYZW, Q.m_XYZW );
#else
    m_X -= Q.m_X;
    m_Y -= Q.m_Y;
    m_Z -= Q.m_Z;
    m_W -= Q.m_W;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xquaternion& xquaternion::operator *= ( const xquaternion& Q )
{
#ifdef _X_SSE2_SUPPORT
    q_Multiply( *this, *this, Q );
#else
    m_X *= Q.m_X;
    m_Y *= Q.m_Y;
    m_Z *= Q.m_Z;
    m_W *= Q.m_W;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xquaternion& xquaternion::operator *= ( f32 Scalar )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_mul_ps( m_XYZW, _mm_set1_ps( Scalar ) );
#else
    m_X *= Scalar;
    m_Y *= Scalar;
    m_Z *= Scalar;
    m_W *= Scalar;
#endif
    return *this;
}

//------------------------------------------------------------------------------
inline 
const xquaternion& xquaternion::operator /= ( f32 Scalar )
{
#ifdef _X_SSE2_SUPPORT
    m_XYZW = _mm_div_ps( m_XYZW, _mm_set1_ps( Scalar ) );
#else
    m_X /= Scalar;
    m_Y /= Scalar;
    m_Z /= Scalar;
    m_W /= Scalar;
#endif
    return *this;
}

//------------------------------------------------------------------------------
constexpr
bool xquaternion::operator == ( const xquaternion& Q ) const
{
    return  ( x_Abs( Q.m_X - m_X) < 0.001f ) &&
            ( x_Abs( Q.m_Y - m_Y) < 0.001f ) &&
            ( x_Abs( Q.m_Z - m_Z) < 0.001f ) &&
            ( x_Abs( Q.m_W - m_W) < 0.001f );
}

//------------------------------------------------------------------------------
inline 
xquaternion operator + ( const xquaternion& Q1, const xquaternion& Q2 )
{
#ifdef _X_SSE2_SUPPORT
    return xquaternion{ _mm_add_ps( Q1.m_XYZW, Q2.m_XYZW ) };
#else
    return xquaternion{ Q1.m_X + Q2.m_X,
                        Q1.m_Y + Q2.m_Y,
                        Q1.m_Z + Q2.m_Z,
                        Q1.m_W + Q2.m_W };
#endif
}

//------------------------------------------------------------------------------
inline 
xquaternion operator - ( const xquaternion& Q1, const xquaternion& Q2 )
{
#ifdef _X_SSE2_SUPPORT
    return xquaternion{ _mm_sub_ps( Q1.m_XYZW, Q2.m_XYZW ) };
#else
    return xquaternion{ Q1.m_X -  Q2.m_X,
                        Q1.m_Y -  Q2.m_Y,
                        Q1.m_Z -  Q2.m_Z,
                        Q1.m_W -  Q2.m_W };
#endif
}

//------------------------------------------------------------------------------
inline 
xquaternion operator - ( const xquaternion& Q )
{
#ifdef _X_SSE2_SUPPORT
    return xquaternion{ _mm_sub_ps( _mm_setzero_ps(), Q.m_XYZW ) };
#else
    return xquaternion( -  Q.m_X,
                        -  Q.m_Y,
                        -  Q.m_Z,
                        -  Q.m_W );
#endif
}

//------------------------------------------------------------------------------
inline 
xquaternion operator * ( const xquaternion& Q, f32 Scalar )
{
#ifdef _X_SSE2_SUPPORT
    return xquaternion{ _mm_mul_ps( Q.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xquaternion{ Q.m_X * Scalar,
                        Q.m_Y * Scalar,
                        Q.m_Z * Scalar,
                        Q.m_W * Scalar };
#endif
}

//------------------------------------------------------------------------------
inline 
xquaternion operator * ( f32 Scalar, const xquaternion& Q )
{
#ifdef _X_SSE2_SUPPORT
    return xquaternion{ _mm_mul_ps( Q.m_XYZW, _mm_set1_ps( Scalar ) ) };
#else
    return xquaternion{ Q.m_X * Scalar,
                        Q.m_Y * Scalar,
                        Q.m_Z * Scalar,
                        Q.m_W * Scalar };
#endif
}

//------------------------------------------------------------------------------
inline
xvector3 operator * ( const xquaternion& Q, const xvector3& V )
{
#ifdef TARGET_PC_32BIT
    xvector3 Result;
    __asm
    {
        mov         ecx, V                     
        mov         edx, Q

        // First cross product
        movaps      xmm0, dword ptr [edx]
        movaps      xmm2, xmm0
        shufps      xmm0, xmm0, _MM_SHUFFLE(3,0,2,1)    // xmm0: W, X, Z, Y
        shufps      xmm2, xmm2, _MM_SHUFFLE(3,1,0,2)    // xmm2: W, Y, X, Z

        movaps      xmm1, dword ptr [ecx]
        movaps      xmm3, xmm1
        shufps      xmm1, xmm1, _MM_SHUFFLE(3,1,0,2)    // xmm1: W, Y, X, Z
        shufps      xmm3, xmm3, _MM_SHUFFLE(3,0,2,1)    // xmm3: W, X, Z, Y

        mulps       xmm1, xmm0
        mulps       xmm3, xmm2
        subps       xmm1, xmm3 

        // back up v1 for later
        movaps      xmm4, xmm1                          // v1

        // second cross product
        movaps      xmm3, xmm1
        shufps      xmm1, xmm1, _MM_SHUFFLE(3,1,0,2)    // xmm1: W, Y, X, Z
        shufps      xmm3, xmm3, _MM_SHUFFLE(3,0,2,1)    // xmm3: W, X, Z, Y

        mulps       xmm0, xmm1
        mulps       xmm2, xmm3
        subps       xmm0, xmm2                          // v2

        // do mults
        addps       xmm0, xmm0                          // 2 * v2
        addps       xmm4, xmm4                          // 2 * v1

        // mul w
        movss       xmm2, [edx+3*4]
        shufps      xmm2, xmm2, _MM_SHUFFLE(0,0,0,0)  
        mulps       xmm4, xmm2                          // v1 * w

        // final add
        movaps      xmm2, dword ptr [ecx]
        addps       xmm4, xmm2
        addps       xmm0, xmm4

        // get the value out of here
        movaps      xmmword ptr [Result], xmm0
    }

    return Result;
#else

// Two methods for v to q mul
/*
    xvector3 Result;
    xvector3 v1;
    xvector3 v2;

    Result.setup( Q.m_X, Q.m_Y, Q.m_Z );
    v1     = Result.Cross( V  );
    v2     = Result.Cross( v1 );
    v1    *= 2.0f * Q.m_W;
    v2    *= 2.0f;
    Result = V + v1 + v2;
    return Result;
*/
    const xvector3&  u = *((xvector3*)&Q);
    const f32        s = Q.m_W;

    return 2.0f * u.Dot( V ) * u + 
          ( s*s - u.Dot(u) ) * V +
          2.0f * s * u.Cross(V);
#endif
}

//------------------------------------------------------------------------------
inline 
f32 xquaternion::LengthSquared( void ) const
{
    return (*this).Dot( *this );
}

//------------------------------------------------------------------------------
inline
f32 xquaternion::Length( void ) const
{
#ifdef TARGET_PC_32BIT
    f32 Result;
    __asm 
    {
        //
        // Length square
        //
        mov         ecx, this
        movaps      xmm0, dword ptr [ecx]
        mulps       xmm0, xmm0
        movaps      xmm1, xmm0                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)  
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm1, xmm0                        

        // Find the sqrt  of x
        sqrtss     xmm1, xmm1

        // done
        movss      xmmword ptr [Result], xmm1
    }

    return Result;

#elif defined _X_SSE2_SUPPORT

    f32x4 a = _mm_mul_ps( m_XYZW, m_XYZW );
    a = _mm_add_ss( _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a)),_mm_shuffle_ps(a, a, 3) );
    a = _mm_sqrt_ss(a);

    return x_getf32x4ElementByIndex<0>(a);

#else
    return x_Sqrt( m_X*m_X + m_Y*m_Y + m_Z*m_Z + m_W*m_W );
#endif
}

//------------------------------------------------------------------------------
inline
bool xquaternion::isValid( void ) const
{
    if( !((m_X >= -1.01f) && ( m_X <= 1.01f) &&
          (m_Y >= -1.01f) && ( m_Y <= 1.01f) &&
          (m_Z >= -1.01f) && ( m_Z <= 1.01f) &&
          (m_W >= -1.01f) && ( m_W <= 1.01f)) )
         return false;

    const f32 L = Length();
    return (L>=0.9f) && (L<=1.1f);
}

//------------------------------------------------------------------------------
inline
xvector3 xquaternion::getAxis( void ) const
{
    xvector3 Axis;
    f32 s;

    s = x_Sin( x_ACos( x_Range( m_W, -1.0f, 1.0f ) ) );

    if( (s > -0.00001f) && (s < 0.00001f) ) 
    {
        Axis.setZero();
    }
    else 
    {
        Axis.setup( m_X, m_Y, m_Z );
        Axis /= s; 
    }

    return Axis;
}

//------------------------------------------------------------------------------
inline
xradian xquaternion::getAngle( void ) const
{
    return x_ACos( x_Range( m_W, -1.0f, 1.0f ) ) * xradian{ 2.0f };
}

//------------------------------------------------------------------------------
inline
xradian3 xquaternion::getRotation( void ) const
{
    f32 tx  = 2.0f * m_X;   // 2x
    f32 ty  = 2.0f * m_Y;   // 2y
    f32 tz  = 2.0f * m_Z;   // 2z
    f32 txw =   tx * m_W;   // 2x * w
    f32 tyw =   ty * m_W;   // 2y * w
    f32 tzw =   tz * m_W;   // 2z * w
    f32 txx =   tx * m_X;   // 2x * x
    f32 tyx =   ty * m_X;   // 2y * x
    f32 tzx =   tz * m_X;   // 2z * x
    f32 tyy =   ty * m_Y;   // 2y * y
    f32 tzy =   tz * m_Y;   // 2z * y
    f32 tzz =   tz * m_Z;   // 2z * z

    xradian  Pitch, Yaw, Roll;

    //
    // First get pitch (Rx).
    //
    Pitch = x_ASin( -x_Range( tzy - txw, -1.0f, 1.0f ) );

    //
    // get yaw (Ry) and roll (Rz).    
    //
    if( (Pitch > -xradian{ 89.0_deg } ) || (Pitch < xradian{ 89.0_deg } ) )
    {
        Yaw  = x_ATan2( tzx + tyw, 1.0f-(txx+tyy) );
        Roll = x_ATan2( tyx + tzw, 1.0f-(txx+tzz) );
    }
    else
    {
        Yaw  = xradian{ 0.0f };
        Roll = x_ATan2( tzx - tyw, 1.0f-(tyy+tzz) );
    }

    return xradian3( Pitch, Yaw, Roll );
}

//------------------------------------------------------------------------------
inline
f32 xquaternion::Dot( const xquaternion& Q ) const
{
#ifdef TARGET_PC_32BIT
    f32 Dest;
    __asm 
    {
        mov         ecx, this
        mov         eax, Q
        movaps      xmm0, dword ptr [ecx]
        mulps       xmm0, dword ptr [eax]      
        movaps      xmm1, xmm0                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)  
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm1, xmm0                        
        movss       xmmword ptr [Dest], xmm1
    }
    return Dest;

#elif _X_SSE2_SUPPORT

    const f32x4       A = _mm_mul_ps( m_XYZW, Q.m_XYZW );
    const f32x4       B = _mm_add_ss( _mm_shuffle_ps( A, A, _MM_SHUFFLE(3,3,3,3)), 
                          _mm_add_ss( _mm_shuffle_ps( A, A, _MM_SHUFFLE(0,0,0,0)),
                          _mm_add_ss( _mm_shuffle_ps( A, A, _MM_SHUFFLE(1,1,1,1)), 
                                      _mm_shuffle_ps( A, A, _MM_SHUFFLE(2,2,2,2)))));

    return x_getf32x4ElementByIndex<0>(B);

#else
    return  m_X*Q.m_X +
            m_Y*Q.m_Y +
            m_Z*Q.m_Z +
            m_W*Q.m_W;
#endif
}

//------------------------------------------------------------------------------
inline 
xquaternion& xquaternion::Normalize( void )
{
#ifdef _X_SSE2_SUPPORT
    f32x4 a = _mm_mul_ps( m_XYZW, m_XYZW );
          a = _mm_add_ss( _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a)),_mm_shuffle_ps(a, a, 3) );
          a = _mm_rsqrt_ss(a);

    const f32x4 oneDivLen = _mm_shuffle_ps( a, a, 0 );
    
    m_XYZW = _mm_mul_ps( m_XYZW, oneDivLen );
    return *this;

#else
    const f32 Div = x_InvSqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z+ m_W*m_W );
    m_X *= Div;
    m_Y *= Div;
    m_Z *= Div;
    m_W *= Div;
    return *this;
#endif
}

//------------------------------------------------------------------------------
inline 
xquaternion& xquaternion::NormalizeSafe( void )
{
    const f32   Tof  = 0.00001f;

#ifdef _X_SSE2_SUPPORT
    f32x4 a = _mm_mul_ps( m_XYZW, m_XYZW );
          a = _mm_add_ss( _mm_add_ss( _mm_shuffle_ps(a, a, 1),_mm_add_ps( _mm_movehl_ps(a, a), a)),_mm_shuffle_ps(a, a, 3) );

    if( x_getf32x4ElementByIndex<0>(a) < Tof )
    {
        setup( 1, 0, 0, 0 );
        return *this;
    }

                    a           = _mm_rsqrt_ss(a);
    const f32x4     oneDivLen   = _mm_shuffle_ps( a, a, 0 );
    
    m_XYZW = _mm_mul_ps( m_XYZW, oneDivLen );
    return *this;

#else
    const f32 dsq = m_X*m_X + m_Y*m_Y + m_Z*m_Z+ m_W*m_W;
	if( dsq < Tof ) 
    {		
        setup( 1.0f, 0.0f, 0.0f, 0.0f );
        return *this;
    }

    const f32 Div = x_InvSqrt(dsq);
    m_X *= Div;
    m_Y *= Div;
    m_Z *= Div;
    m_W *= Div;
    return *this;
#endif
}


//------------------------------------------------------------------------------
inline
xquaternion xquaternion::BlendFast( f32 T, const xquaternion& End ) const
{
#ifdef TARGET_PC_32BIT
    register xquaternion Dest;
    __asm 
    {
        mov         ecx, this
        mov         eax, End

        //
        // Determine if quats are further than 90 degrees
        // If dot is negative flip one of the quaternions
        //
        movaps      xmm0, dword ptr [ecx]
        movaps      xmm2, xmm0

        // find the dot product
        mulps       xmm0, dword ptr [eax]
        movaps      xmm1, xmm0                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)  
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1

        // now lets compare to see if we have to invert the xquaternion
        xorps       xmm1, xmm1
        comiss      xmm1, xmm0      // compare (b,a)
        jbe         DONT_INVERT     // branch if a>=b

        // Do inversion
        subps       xmm1, xmm2
        movaps      xmm2, xmm1
        DONT_INVERT:

        //
        // Compute interpolated values in a linear fashion
        //
        movaps      xmm0, dword ptr [eax]
        subps       xmm0, xmm2
        movss       xmm1, T                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,0,0,0)
        mulps       xmm0, xmm1
        addps       xmm2, xmm0

        //
        // Normalize new xquaternion
        //

        // find the distance square
        movaps      xmm0, xmm2
        mulps       xmm0, xmm0      
        movaps      xmm1, xmm0                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)  
        addss       xmm0, xmm1                        
        shufps      xmm1, xmm1, _MM_SHUFFLE(0,3,2,1)
        addss       xmm0, xmm1

        // find the rsqrt x
        rsqrtss     xmm0, xmm0

        // now do final normalize step
        shufps      xmm0, xmm0, _MM_SHUFFLE(0,0,0,0)
        mulps       xmm0, xmm2

        //
        // Done
        //
        movaps      xmmword ptr [Dest], xmm0
    }

    return Dest;
#else
    f32 Dot;
    f32 LenSquared;
    f32 OneOverL;
    f32 x0,y0,z0,w0;

    // Determine if quats are further than 90 degrees
    Dot = m_X*End.m_X + m_Y*End.m_Y + m_Z*End.m_Z + m_W*End.m_W;

    // If dot is negative flip one of the quaterions
    if( Dot < 0.0f )
    {
        x0 = -m_X;
        y0 = -m_Y;
        z0 = -m_Z;
        w0 = -m_W;
    }
    else
    {
        x0 =  m_X;
        y0 =  m_Y;
        z0 =  m_Z;
        w0 =  m_W;
    }

    // Compute interpolated values
    x0 = x0 + T*(End.m_X - x0);
    y0 = y0 + T*(End.m_Y - y0);
    z0 = z0 + T*(End.m_Z - z0);
    w0 = w0 + T*(End.m_W - w0);

    // get squared length of new quaternion
    LenSquared = x0*x0 + y0*y0 + z0*z0 + w0*w0;

    // Use home-baked polynomial to compute 1/sqrt(LenSquared)
    // Input range is 0.5 <-> 1.0
    // Ouput range is 1.414213 <-> 1.0

    if( LenSquared<0.857f )
        OneOverL = (((0.699368f)*LenSquared) + -1.819985f)*LenSquared + 2.126369f;    //0.0000792
    else
        OneOverL = (((0.454012f)*LenSquared) + -1.403517f)*LenSquared + 1.949542f;    //0.0000373

    // Renormalize and return quaternion
    return xquaternion( x0*OneOverL, y0*OneOverL, z0*OneOverL, w0*OneOverL );

#endif
}

//------------------------------------------------------------------------------
inline
xquaternion xquaternion::BlendAccurate( f32 T, const xquaternion& End ) const
{
    bool bFlip = false;

    // Determine if quats are further than 90 degrees
    f32 Cs = Dot(End);
    if( Cs < 0.0f) 
    {
        Cs = -Cs;
        bFlip = !bFlip;
    }

    f32 inv_T;
    if( (1.0f - Cs) < 0.000001f ) 
    {
        inv_T = 1.0f - T;
    } 
    else 
    {
        const xradian   Theta   = x_ACos( Cs );
        const f32       S       = 1.0f / x_Sin( Theta );

        inv_T = x_Sin( xradian{ 1.0f - T } * Theta) * S;
        T     = x_Sin( xradian{ T        } * Theta) * S;
    }

    if( bFlip ) 
    {
        T = -T;
    }
    
    return End * T + (*this) * inv_T;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::setup( const xmatrix4& M )
{
    xquaternion& Q = *this;
    f32 T;
    f32 X2, Y2, Z2, W2;  // squared magniudes of xquaternion components
    s32 i;

    // remove scale from xmatrix
    xmatrix4 O = M;
    O.ClearScale();

    // first compute squared magnitudes of xquaternion components - at least one
    // will be greater than 0 since xquaternion is unit magnitude
    W2 = 0.25f * (O.m_Cell[0][0] + O.m_Cell[1][1] + O.m_Cell[2][2] + 1.0f );
    X2 = W2 - 0.5f * (O.m_Cell[1][1] + O.m_Cell[2][2]);
    Y2 = W2 - 0.5f * (O.m_Cell[2][2] + O.m_Cell[0][0]);
    Z2 = W2 - 0.5f * (O.m_Cell[0][0] + O.m_Cell[1][1]);

    // find maximum magnitude component
    i = (W2 > X2 ) ?
        ((W2 > Y2) ? ((W2 > Z2) ? 0 : 3) : ((Y2 > Z2) ? 2 : 3)) :
        ((X2 > Y2) ? ((X2 > Z2) ? 1 : 3) : ((Y2 > Z2) ? 2 : 3));

    // compute signed xquaternion components using numerically stable method
    switch( i ) 
    {
    case 0:
        Q.m_W = x_Sqrt(W2);
        T = 0.25f / Q.m_W;
        Q.m_X = (O.m_Cell[1][2] - O.m_Cell[2][1]) * T;
        Q.m_Y = (O.m_Cell[2][0] - O.m_Cell[0][2]) * T;
        Q.m_Z = (O.m_Cell[0][1] - O.m_Cell[1][0]) * T;
        break;
    case 1:
        Q.m_X = x_Sqrt(X2);
        T = 0.25f / Q.m_X;
        Q.m_W = (O.m_Cell[1][2] - O.m_Cell[2][1]) * T;
        Q.m_Y = (O.m_Cell[1][0] + O.m_Cell[0][1]) * T;
        Q.m_Z = (O.m_Cell[2][0] + O.m_Cell[0][2]) * T;
        break;
    case 2:
        Q.m_Y = x_Sqrt(Y2);
        T = 0.25f / Q.m_Y;
        Q.m_W = (O.m_Cell[2][0] - O.m_Cell[0][2]) * T;
        Q.m_Z = (O.m_Cell[2][1] + O.m_Cell[1][2]) * T;
        Q.m_X = (O.m_Cell[0][1] + O.m_Cell[1][0]) * T;
        break;
    case 3:
        Q.m_Z = x_Sqrt(Z2);
        T = 0.25f / Q.m_Z;
        Q.m_W = (O.m_Cell[0][1] - O.m_Cell[1][0]) * T;
        Q.m_X = (O.m_Cell[0][2] + O.m_Cell[2][0]) * T;
        Q.m_Y = (O.m_Cell[1][2] + O.m_Cell[2][1]) * T;
        break;
    }

    Q.Normalize();
    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::Ln( void )
{
    f32 scale = x_Sqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z );

    if( scale > 0.0f ) 
    {
        xradian theta = x_ATan2( scale, m_W );
        scale = theta.m_Value/scale;
    }

    m_X = scale * m_X;
    m_Y = scale * m_Y;
    m_Z = scale * m_Z;
    m_W = 0.0f;

    x_assert( isValid() );

    return *this;
}

//------------------------------------------------------------------------------
inline
xquaternion& xquaternion::Exp( void )
{
    x_assert( m_W == 0.0f );
    static const xradian Q_EPSILON{ 1e-10f };

    f32 scale;
    xradian theta{ x_Sqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z ) };
    if( theta > Q_EPSILON ) 
    {
        scale = x_Sin( theta ) / theta.m_Value;
    } 
    else 
    {
        scale = 1.0f;
    }

    m_X = scale * m_X;
    m_Y = scale * m_Y;
    m_Z = scale * m_Z;
    m_W = x_Cos(theta);

    x_assert( isValid() );

    return *this;
}

//------------------------------------------------------------------------------
inline 
xvector3 xquaternion::getLookDirection( void ) const
{
    return (*this) * xvector3::Forward();
}
