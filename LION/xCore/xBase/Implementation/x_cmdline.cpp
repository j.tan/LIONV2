//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "x_Base.h"


//==============================================================================

void xcmdline::Parse( s32 argc, const char** argv ) throw(xthrow)
{    
    xvector<xstring> Arguments;

    // User needs help
    if( argc == 1 ) 
    {
        s32 TotalMustHave = 0;
        for( s32 i=0; i<m_CmdDef.getCount(); i++ )
        {
            TotalMustHave += m_CmdDef[i].m_nMinTimes;
        }

        if( TotalMustHave > 0 )
        {
            ClearArguments();
            m_bNeedHelp = true;
            return;
        }
    }

    // Put all the arguments into the list
    // skip argv 0 sinec it is the file name.
    for( s32 i=1; i<argc; i++ )
    {
        x_assert( argv[i] );
        
        Arguments.append().Copy( argv[i] );
        if( Arguments[i-1][0] == '-' ) 
            Arguments[i-1][0] = 1;
    }

    // Parse these arguments
    Parse( Arguments );
}

//==============================================================================

void xcmdline::Parse( const char* pString ) throw(xthrow)
{
    s32             iStart;
    s32             iEnd;
    s32	            iNext;
    s32             Length = x_strlen(pString).m_Value;
    xstring         strCmd;
    xvector<xstring> Arguments;

    strCmd.Copy(pString);

    // process all the arguments
    iStart = 0;
    while( iStart < Length )
    {
        // Skip Whitespace
        while( (iStart < Length) && x_isspace( strCmd[iStart] ) )
        {
            iStart++;
        }

        // At end of string?
        if( iStart < Length )
        {
            // Find limits of string
            iEnd = iStart+1;
            bool bStartArgument = false;
            if( strCmd[iStart] == '"' )
            {
                bStartArgument = true;
                if( iStart > 0 )
                {
                    if( strCmd[iStart-1]=='\\')
                        bStartArgument = false;
                }
            }
            else if (strCmd[iStart] == '-' && !x_isdigit(strCmd[iStart+1]) && strCmd[iStart+1]!='.')
            {
                // Replace the switch identifier to ascii 1
                strCmd[iStart] = 1;
            }

            if(bStartArgument)
            {
                // iStart++; // old vesion removed the quotes new version will keep them
                while( iEnd<Length )
                {
                    if(strCmd[iEnd] == '"')
                    {
                        if(strCmd[iEnd-1]!='\\')
                        {
                            iEnd++;     // old version removed the quotes but the new version keeps it
                            break;
                        }
                    }
                    iEnd++;
                }
                iNext = iEnd+1;
            }
            else
            {
                while( (iEnd<Length) && !x_isspace(strCmd[iEnd]) )
                    iEnd++;
                iNext = iEnd;
            }

            // Add to argument array
            Arguments.append().Copy( &strCmd[ iStart ], xstring::t_characters{ iEnd - iStart } );

            // Set start past end of string
            iStart = iNext;
        }
    }

    // Parse these arguments
    Parse( Arguments );
}

//==============================================================================

void xcmdline::ProcessResponseFile( xwstring& PathName ) throw(xthrow)
{
    xfile File;

    // Open the file
    if( File.Open( PathName, "rt" ) == false )
        x_throw( "Error: Unable to open the response file [%s]", PathName );

    //
    // Read the hold file in
    //
    auto Length = File.getFileLength();
    xafptr<char>  Buff;
    Buff.Alloc( Length+1 );
    s32 L = File.ReadList( Buff.ViewTo( Length ) );
    File.Close();

    // Make sure to terminate everything
    Buff[L]=0;

    //
    // Now process the data
    //
    Parse( &Buff[0] );
}

//==============================================================================

void xcmdline::Parse( xvector<xstring>& Args ) throw(xthrow) 
{
    s32 i;
    s32 iMainSwitch=-1;
/*
    for( i=0; i<Args.getCount(); i++)
    {
        xstring&  curArg = Args[i];
        s32 iLength = curArg.GetLength();
        char* pReading = new char[iLength+1];
        char* pWriting = new char[iLength+1];
        char* pSource = pReading;
        char* pTarget = pWriting;
        x_strcpy(pReading, iLength+1, curArg);
        x_strcpy(pWriting, iLength+1, pReading);
        s32 iSlash = FindCharacterInString(pSource, '\\');
        while( iSlash>=0 )
        {
            s32 iOffset = 0;
            if(pSource+iSlash < pReading+iLength-1)
                if( pSource[iSlash+1]=='\"' )
                    iOffset = 1;

            if(iOffset)
                x_strcpy( pTarget+iSlash, x_strlen(pSource)-iSlash+1-iOffset, pSource+iSlash+iOffset );
            pSource = pSource+iSlash+1;
            pTarget = pTarget+iSlash+1-iOffset;
            if( pSource-pReading >= iLength )
                break;
            iSlash = FindCharacterInString(pSource, '\\');
        }

        curArg.Clear();
        curArg.Format(pWriting);
        delete[] pReading;
        delete[] pWriting;
    }
*/

    // Process Args
    for( i=0 ; i<Args.getCount(); i++ )
    {
        xstring&  a = Args[i];

        // Check for Help?
        if( (a == "?") || (a == "\1?") || (a == "\1HELP") )
        {
            ClearArguments();
            m_bNeedHelp = true;
            return;
        }

        // Check for response file
        if( a[0] == '@' )
        {
            xwstring FileName;
            FileName.CopyAndConvert( &a[1] );
            ProcessResponseFile( FileName );
            continue;
        }

        // Check for a command
        // The switch identifier is replaced by ascii 1 at this time
        // if( a[0] == '-' && ( (!x_isdigit(a[1])) && (a[1] != '.')) )
        if( a[0] == 1 && ( (!x_isdigit(a[1])) && (a[1] != '.')) )
        {
            bool   Found = false;

            // Remove leading '-' and find crc
            u32 CRC = x_strCRC<xchar>( &a[1] );

            // check for minimum rage of the previous switch
            if( m_Command.getCount() )
            {
                cmd_entry& Cmd = m_Command[ m_Command.getCount()-1 ];

                if( m_CmdDef[ Cmd.m_iCmdDef ].m_MinArgCount > Cmd.m_ArgCount )
                {
                    ClearArguments();
                    x_throw( "Error: We found this switch [%s] had too few arguments we expected at least [%d].", (const char*)a, m_CmdDef[ Cmd.m_iCmdDef ].m_MinArgCount );
                    return;
                }                
            }

            // Search for option and read value into option list
            for( s32 j=0 ; j<m_CmdDef.getCount(); j++ )
            {
                // Check if found.
                // The switch we are looking for must match the name and its parent id
                if( m_CmdDef[j].m_crcName == CRC && 
                   iMainSwitch == m_CmdDef[j].m_iParentID  )
                {
                    xstring OptionValue;

                    Found = true;

                    // Add the cmd entry
                    cmd_entry& Cmd = m_Command.append();
                    Cmd.m_iCmdDef  = j;
                    Cmd.m_iArg     = m_Arguments.getCount<int>();
                    Cmd.m_ArgCount = 0;

                    // if it is a main switch make sure that there are not other ones active
                    if( m_CmdDef[j].m_bMainSwitch )
                    {
                        // Set the variable to have the id of the main switch
                        iMainSwitch = j;
                        
                        for( s32 t=0; t<m_CmdDef.getCount(); t++ )
                        {
                            if( t == j ) continue;
                            if( m_CmdDef[t].m_RefCount <= 0 ) continue;
                            if( m_CmdDef[t].m_bMainSwitch == false ) continue;

                            ClearArguments();
                            x_throw( "Error: We found two main switches [%s] and [%s]. You can only enter one of this type of switches.", (const char*)m_CmdDef[t].m_Name, (const char*)m_CmdDef[j].m_Name );
                            return;                            
                        }
                    }

                    // make sure that we don't have this reference too many times
                    m_CmdDef[j].m_RefCount++;
                    if( m_CmdDef[j].m_RefCount > m_CmdDef[j].m_nMaxTimes && m_CmdDef[j].m_nMaxTimes != -1 )
                    {
                        ClearArguments();
                        x_throw( "Error: We found this switch [%s] too many times in the command-line. We were expecting to find it only [%d] times", (const char*)a, m_CmdDef[j].m_nMaxTimes );
                        return;
                    }

                    // make sure that fallows the proper order in the sequence
                    if( m_CmdDef[j].m_bFallowOrder )
                    {
                        for( s32 t=0; t<m_Command.getCount(); t++ )
                        {
                            if( j < m_Command[t].m_iCmdDef )
                            {
                                ClearArguments();
                                x_throw( "Error: This switch:[%s] is out of order check help for proper usage.", (const char*)a );
                                return;
                            }
                        }
                    }

                    break;
                }
                else
                {
                    //
                    // We are going to be nice and search to see if there is another switch
                    // that the user may have been talking about but it is the wrong parenting
                    //
                    if( m_CmdDef[j].m_crcName == CRC )
                    {
                        bool bFoundSameName = false;
                        for( s32 k=j+1 ; k<m_CmdDef.getCount(); k++ )
                        {
                            if( m_CmdDef[k].m_crcName == CRC && iMainSwitch == m_CmdDef[k].m_iParentID )
                            {
                                bFoundSameName = true;
                            }
                        }
                        
                        if( bFoundSameName == false )
                        {
                            // check whether it needs another switch to exits
                            if( m_CmdDef[j].m_iParentID != -1 )
                            {
                                s32 Index = m_CmdDef[j].m_iParentID;
                                if( m_CmdDef[ Index ].m_RefCount <= 0 )
                                {
                                    ClearArguments();
                                    x_throw( "Error: We found a switch:[%s] which can only be use with this other switch:[%s]", (const char*)m_CmdDef[j].m_Name, (const char*)m_CmdDef[Index].m_Name );
                                    return;
                                }
                            }
                        }
                    }
                }
            }

            // Check if option was found
            if( !Found )
            {
                ClearArguments();
                x_throw( "Error: Unable to find a match for this switch [%s]", (const char*)a );
                return;
            }
        }
        else
        {
            // Add to argument list
            m_Arguments.append() = a;

            // make sure that we at least have one command going on
            if( m_Command.getCount() <= 0 )
            {
                // We forgive argument zero because it is the name of the exe
                if( m_Arguments.getCount() > 1 )
                {
                    ClearArguments();
                    x_throw( "Error: Arguments been pass without setting switches Arg:[%s]", (const char*)a );
                }
            }
            else
            {
                // Notify the cmd entry about its new arg
                s32 iCommand = m_Command.getCount<int>()-1;
                m_Command[ iCommand ].m_ArgCount++;

                // Make sure that we have the righ number of maximun arguments
                cmd_def& Def = m_CmdDef[ m_Command[ iCommand ].m_iCmdDef ];
                if( m_Command[ iCommand ].m_ArgCount > Def.m_MaxArgCount && Def.m_MaxArgCount != -1 )
                {
                    ClearArguments();
                    x_throw( "Error: The command has too many Arguments for the switch:[%s]", (const char*)Def.m_Name );
                }

                // Make sure that the type matches with the expected type
                if( Def.m_Type != type::NONE )
                {
                    const char* pTypeString = nullptr;
                    const char* pExpectType = nullptr;
                    switch( Def.m_Type )
                    {
                    case type::NONE:
                            x_assert(0);
                            break;
                    case type::INT:
                        {
                            if( x_isstrint<char>( a ) == true )
                                pTypeString = "INT";
                            pExpectType = "INT";
                            break;
                        }
                    case type::FLOAT:
                        {
                            if( x_isstrfloat<char>( a ) == true )
                                pTypeString = "FLOAT";
                            pExpectType = "FLOAT";
                            break;
                        }
                    case type::GUID:
                        {
                            if( x_isstrguid<char>( a ) == true )
                                pTypeString = "GUID";
                            pExpectType = "GUID";
                            break;
                        }
                    case type::HEX:
                        {
                            if( x_isstrhex<char>( a ) == true )
                                pTypeString = "HEX";
                            pExpectType = "HEX";
                            break;
                        }
                    case type::STRING:
                        {
                            pTypeString = "STRING";

                            // The new version removed quotes for string only
                            if( a[0] == '"' )
                            {
                                s32 l = a.getLength().m_Value;
                                for( s32 i=0;i<l; i++ )
                                {
                                    a[i] = a[i+1];
                                }
                                a[l-2] = 0;
                            }
                            break;
                        }
                    case type::STRING_RETAIN_QUOTES:
                        {
                            pTypeString = "STRING_RETAIN_QUOTES";
                            break;
                        }
                    default:
                        {
                            x_assert(0);
                        }
                    }

                    if( !pTypeString )
                    {
                        ClearArguments();
                        x_throw( "Error: expecting a [%s] but found something different for the argument of the switch:[%s]", pExpectType, (const char*)Def.m_Name );
                    }
                }            
            }
        }
    }

    // check for minimum rage of the last switch (how many arguments can it have)
    if( m_Command.getCount() )
    {
        cmd_entry& Cmd = m_Command[ m_Command.getCount()-1 ];

        if( m_CmdDef[ Cmd.m_iCmdDef ].m_MinArgCount > Cmd.m_ArgCount )
        {
            ClearArguments();
            x_throw( "Error: We found this switch [%s] had too few arguments we expected at least [%d].", (const char*)m_CmdDef[ Cmd.m_iCmdDef ].m_Name, m_CmdDef[ Cmd.m_iCmdDef ].m_MinArgCount );
            return;
        }                
    }

    // check whether the command line was expecting for a switch but it never happen
    for( i=0; i<m_CmdDef.getCount(); i++ )
    {
        const cmd_def& CmdDef = m_CmdDef[i];
         
        if( CmdDef.m_iParentID != iMainSwitch && 
            CmdDef.m_iParentID != -1 )
            continue;
        
        if( CmdDef.m_RefCount < CmdDef.m_nMinTimes )
        {
            ClearArguments();
            x_throw( "Error: We were expecting this switch[%s] to happen [%d] times, but we found it [%d] times.", (const char*)CmdDef.m_Name, CmdDef.m_nMinTimes, CmdDef.m_RefCount );
            return;
        }
    }
}


//==============================================================================

void xcmdline::ClearArguments( void ) noexcept
{
    m_bNeedHelp = false;
    m_Arguments.DeleteAllEntries();
    m_Command.DeleteAllEntries();

    for( s32 i=0; i<m_CmdDef.getCount(); i++ )
    {
        m_CmdDef[i].m_RefCount = 0;
    }
}
