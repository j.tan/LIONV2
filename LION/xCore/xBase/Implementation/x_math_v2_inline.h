//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//==============================================================================
// FUNCTIONS
//==============================================================================

//------------------------------------------------------------------------------
constexpr
xvector2 operator - ( const xvector2& V1, const xvector2& V2 )
{
    return { V1.m_X - V2.m_X, 
             V1.m_Y - V2.m_Y };
}

//------------------------------------------------------------------------------
inline 
xvector2& xvector2::setup( f32 X, f32 Y )
{
    m_X = X; 
    m_Y = Y;
    x_assert( isValid() );
    return *this;
}

//------------------------------------------------------------------------------
inline 
void xvector2::setZero( void )
{
    m_X = 0; 
    m_Y = 0;
}

//------------------------------------------------------------------------------
inline 
f32 xvector2::getLength( void ) const
{
    return x_Sqrt( m_X*m_X + m_Y*m_Y );
}

//------------------------------------------------------------------------------
constexpr 
f32 xvector2::getLengthSquared( void ) const
{
    return m_X*m_X + m_Y*m_Y;
}

//------------------------------------------------------------------------------
inline 
xradian xvector2::getAngle( void ) const
{
    return x_ATan2( m_Y, m_X );
}

//------------------------------------------------------------------------------
inline 
xradian xvector2::getAngleBetween( const xvector2& V ) const
{
    f32 D, Cos;

    D = getLength() * V.getLength();

    if( x_Abs(D) < 0.00001f ) return xradian{ 0.0_deg };

    Cos = Dot( V ) / D;

    if     ( Cos >  1.0f )  Cos =  1.0f;
    else if( Cos < -1.0f )  Cos = -1.0f;

    return x_ACos( Cos );
}

//------------------------------------------------------------------------------
inline 
f32 xvector2::getDistance( const xvector2& V ) const
{
    return x_Sqr(m_X - V.m_X) + x_Sqr(m_Y - V.m_Y);
}

//------------------------------------------------------------------------------
constexpr 
bool xvector2::isInrange( f32 Min, f32 Max ) const
{
    return x_isInrange(m_X, Min, Max) &&
           x_isInrange(m_Y, Min, Max);
}

//------------------------------------------------------------------------------
x_forceinline
bool xvector2::isValid( void ) const
{
    return x_isValid(m_X) && x_isValid(m_Y);
}

//------------------------------------------------------------------------------
inline
xvector2& xvector2::Rotate( xradian Angle )
{
    f32  S, C; 
    x_SinCos( Angle, S, C );

    f32 tX = m_X;
    f32 tY = m_Y;

    setup( (C * tX) - (S * tY), (C * tY) + (S * tX) );
    return *this;
}

//------------------------------------------------------------------------------
constexpr
xvector2 xvector2::getLerp( f32 t, const xvector2& V1 ) const
{
    return { m_X + (( V1.m_X - m_X ) * t),
             m_Y + (( V1.m_Y - m_Y ) * t) };

}

//------------------------------------------------------------------------------
// Returns the closest point on the 2D LINE defined by line_v1 and line_v2
// to the point pt.  Note that output is NOT necissarily between line_v1
// and line_v2.
//
// Reference: http://astronomy.swin.edu.au/~pbourke/geometry/pointline/
//
// out:		(output) Closest point on the line.
// pt:		Point
// line_v1:	First point on the line.
// line_v2: Second point on the line.
//------------------------------------------------------------------------------
inline
xvector2 xvector2::getClosestPointInLine( const xvector2 &V0, const xvector2 &V1 ) const
{
    // safety checks
    x_assert( (V0.m_X != V1.m_X) && (V0.m_Y != V1.m_Y) );

    f32 u = (m_X - V1.m_X) * (V1.m_X - V0.m_X) + (m_Y - V0.m_Y) * (V1.m_Y - V0.m_Y);
    u /= (V0 - V1).getLengthSquared();

    return V0.getLerp( u, V1 );
}

//------------------------------------------------------------------------------
// Returns the closest point on the 2D LINESEGMENT defined by line_v1 and line_v2
// to the point pt.  Note that output WILL BE between line_v1 and line_v2 (or 
// equal to one of them).
//
// out:		(output) Closest point on the line segment.
// pt:		Point
// line_v1: Endpoint of the line segment.
// line_v2: Endpoint of the line segment.
//------------------------------------------------------------------------------
inline
xvector2 xvector2::getClosestPointInLineSegment( const xvector2 &V0, const xvector2 &V1 ) const
{
    // degenerate case
    if( V0 == V1 ) 
    {
        return V0;
    }
    
    f32 u = (m_X - V1.m_X) * (V1.m_X - V0.m_X) + (m_Y - V0.m_Y) * (V1.m_Y - V0.m_Y);
    u /= (V0 - V1).getLengthSquared();
    
    // cap u to the range [0..1]
    u = x_Range( u, 0.0f, 1.0f );
    
    return V0.getLerp( u, V1 );
}

//------------------------------------------------------------------------------
// Determines which side of a line a is point on.
//
// Note that the value returned divided by the distance from line_v1 to line_v2
// is the minimum distance from the point to the line.  That may be useful 
// for determining the distance relationship between points, without actually
// having to calculate the distance.
//
// pt:		Point
// line_v1: Endpoint of the line
// line_v2: Endpoint of the line
//
// returns: > 0.0f if pt is to the left of the line (line_v1->line_v2)
//				< 0.0f if pt is to the right of the line (line_v1->line_v2)
//				= 0.0f if pt is on the line
//
//------------------------------------------------------------------------------
constexpr
f32 xvector2::getWhichSideOfLine( const xvector2& V0, const xvector2& V1 ) const
{
	return ((m_Y - V0.m_Y) * (V1.m_X - V0.m_X) - (m_X - V0.m_X) * (V1.m_Y - V0.m_Y));
}

//------------------------------------------------------------------------------
constexpr
f32 xvector2::Dot( const xvector2& V ) const
{
    return (m_X*V.m_X) + (m_Y*V.m_Y);
}

//------------------------------------------------------------------------------
constexpr
bool xvector2::operator == ( const xvector2& V ) const
{
    return !( (x_Abs( V.m_X - m_X) > XFLT_TOL) ||
              (x_Abs( V.m_Y - m_Y) > XFLT_TOL)
            );
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator += ( const xvector2& V )
{
    setup( m_X + V.m_X, m_Y + V.m_Y );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator -= ( const xvector2& V )
{
    setup( m_X - V.m_X, m_Y - V.m_Y );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator *= ( const xvector2& V )
{
    setup( m_X * V.m_X, m_Y * V.m_Y );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator *= ( f32 Scalar )
{
    setup( m_X * Scalar, m_Y * Scalar );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
const xvector2& xvector2::operator /= ( f32 Div )
{
    f32 Scalar = 1.0f/Div;
    setup( m_X * Scalar, m_Y * Scalar );
	 return (*this);
}

//------------------------------------------------------------------------------
inline
xvector2& xvector2::Normalize( void )
{
    const f32 div = x_InvSqrt( m_X*m_X + m_Y*m_Y );
    m_X *= div;
    m_Y *= div;
    return *this;
}

//------------------------------------------------------------------------------
inline
xvector2& xvector2::NormalizeSafe( void )
{
    const f32 sqrdis = m_X*m_X + m_Y*m_Y;
    if( sqrdis < 0.0001f )
    {
        m_X = 1;
        m_Y = 0;
        return *this;
    }
	f32 imag = x_InvSqrt( sqrdis );

    m_X *= imag;
    m_Y *= imag;
    return *this;
}

//------------------------------------------------------------------------------
constexpr
xvector2 operator + ( const xvector2& V1, const xvector2& V2 )
{
    return { V1.m_X + V2.m_X, V1.m_Y + V2.m_Y };
}

//------------------------------------------------------------------------------
constexpr
xvector2 operator - ( const xvector2& V )
{
    return { -V.m_X, -V.m_Y };
}

//------------------------------------------------------------------------------
inline
xvector2 operator / ( const xvector2& V, f32 S )
{
    S = 1.0f/S;
    return { V.m_X*S, V.m_Y*S };
}

//------------------------------------------------------------------------------
constexpr
xvector2 operator * ( const xvector2& V, f32 S )
{
    return { V.m_X*S, V.m_Y*S };
}

//------------------------------------------------------------------------------
constexpr
xvector2 operator * ( f32 S, const xvector2& V )
{
    return { V.m_X*S, V.m_Y*S };
}

//------------------------------------------------------------------------------
constexpr
xvector2 operator * ( const xvector2& V1, const xvector2& V2 )
{
    return { V1.m_X * V2.m_X, 
             V1.m_Y * V2.m_Y };

}
