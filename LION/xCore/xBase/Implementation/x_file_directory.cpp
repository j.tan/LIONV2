//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#include "x_base.h"
#include "tinydir/tinydir.h"

#ifdef _X_COMPILER_VS_2015
    #include <direct.h>
#endif

#ifdef MAX_PATH
    #undef MAX_PATH
#endif

//------------------------------------------------------------------------
//------------------------------------------------------------------------
namespace x_io {
//------------------------------------------------------------------------
//------------------------------------------------------------------------

//------------------------------------------------------------------------

err dir::openPath( xwstring& Path ) noexcept
{
    x_assert( Path.isValid() );
    x_assert( m_pData );

    xowner<tinydir_dir&>&   Entry = *x_new( tinydir_dir );

    if( tinydir_open( &Entry, Path ) == -1 )
    {
        x_delete( &Entry );
        return x_error_code( errors, ERR_FAILURE, "Fail to open the directory");
    }
    else
    {
        m_pData = &Entry;
    }
    
    return x_error_ok();
}

//------------------------------------------------------------------------

err dir::getNext( file_info& Info ) noexcept
{
    tinydir_dir&    TinyDir = *reinterpret_cast<tinydir_dir*>( m_pData );
    tinydir_file    File;
    
    // Do we have anything to give to the user?
    if( TinyDir.has_next == false )
        return x_error_code( x_io::errors, ERR_FAILURE, "This is not a directory" );
    
    // Get the file structure
    if( tinydir_readfile( &TinyDir, &File ) == -1 )
    {
        return x_error_code( x_io::errors, ERR_FAILURE, "Read directory information" );
    }
    
    // Copy everything to our structure
    x_strcpy<xwchar>( Info.m_FileName, units_wchars{ xfile::MAX_FNAME }, File.name );
    x_strcpy<xwchar>( Info.m_Path,     units_wchars{ xfile::MAX_PATH  }, File.path );
    
    Info.m_bRegular     = !!File.is_reg;
    Info.m_bDir         = !!File.is_dir;
    
    // Make sure we set the right extension
    s32 Ext = s32(File.extension - File.name);

    if( Ext <= 0 || Ext > xfile::MAX_PATH )
        Info.m_pExtension   = NULL;
    else
        Info.m_pExtension   = &Info.m_FileName[ Ext ];
    
    // Get ready for next time
    tinydir_next( &TinyDir );
    
    return x_error_ok();
}

//------------------------------------------------------------------------

void dir::close( void ) noexcept
{
    xowner<tinydir_dir&> TinyDir = *reinterpret_cast<tinydir_dir*>( m_pData );
 
    tinydir_close(&TinyDir);
    
    x_delete( &TinyDir );
    m_pData = nullptr;
}

//------------------------------------------------------------------------------

err dir::MakeDir( const char* pPath, s32 LevelsUp ) noexcept
{
    s32                 NPaths      = 0;
    xarray<s32, 64>     PathIndex;
    s32                 StrLen      = 0;
    
    x_assert( pPath );
    x_assert( LevelsUp >= 0 );
    
    LevelsUp++;
    
    // Find the length of the string as well as how many paths does it contain
    for( StrLen=0; pPath[StrLen]; StrLen++ )
    {
        const u8& Value = pPath[StrLen];
        if( Value == '/' && StrLen > 0 ) PathIndex[ NPaths++ ] = StrLen;
    }
    
    x_assert( StrLen > 0 );
    
    if( NPaths == 0 || PathIndex[ NPaths-1 ] != StrLen - 1 )
    {
        PathIndex[ NPaths++ ] = StrLen;
    }
    
    // Ok user said that he wants to make sure we create NLevels of dir for him
    // So we start from the the Level he asked us to.
    const s32 Level = x_Max( NPaths - LevelsUp, 0 );
    
    for( s32 i = Level; i<NPaths; i++ )
    {
        char TempString[ xfile::MAX_PATH ];
        
        // Copy the string
        x_strncpy<xchar>( TempString, pPath, x_static_cast<int>(PathIndex[ i ]), xfile::MAX_PATH );
        
        // Now we can ask the system to create the path
#ifdef _X_COMPILER_VS_2015
        const s32       Err     = _mkdir( TempString );
#else
        const mode_t    mode    = 0777;
        const s32       Err     = mkdir(TempString, mode);
#endif
        if ( Err == -1 && errno == ENOENT )
            return x_error_code( errors, ERR_FAILURE, "Fail making a new directory" );
    }
    
    return x_error_ok();
}

//------------------------------------------------------------------------------
#if !_X_TARGET_WINDOWS

#include <sys/stat.h>
#include <iostream>

#ifndef S_ISDIR
    #define S_ISDIR(mode)  (((mode) & S_IFMT) == S_IFDIR)
#endif

//------------------------------------------------------------------------------

x_io::err x_io::FileDetails( const char* pPath, file_details& FileDetails )
{
    x_assert( pPath );
    
    struct stat fileInfo;
    
    if( stat( pPath, &fileInfo) != 0 )
        return x_error_code( errors, ERR_FAILURE, "Fail collecting details on a directory" );
    
    FileDetails.m_bDir          = (fileInfo.st_mode & S_IFMT) == S_IFDIR;
    FileDetails.m_FileSize      = fileInfo.st_size;

#ifndef _X_COMPILER_VS_2015 
    FileDetails.m_MSTimeCreated  = fileInfo.st_ctimespec.tv_sec * (double)1000 + fileInfo.st_ctimespec.tv_nsec / (double)1000;
    FileDetails.m_MSTimeModified = fileInfo.st_mtimespec.tv_sec * (double)1000 + fileInfo.st_mtimespec.tv_nsec / (double)1000;
#else
    FileDetails.m_MSTimeCreated  = fileInfo.st_ctime * (double)1000 + fileInfo.st_ctime / (double)1000;
    FileDetails.m_MSTimeModified = fileInfo.st_mtime * (double)1000 + fileInfo.st_mtime / (double)1000;
#endif

    return x_error_ok();
}

//------------------------------------------------------------------------------

bool x_io::doesPathExists( const char* pPath ) noexcept
{
    // The variable that holds the file information
    // the type stat and function stat have exactly the same names, so to refer the type, we put struct before it to indicate it is an structure.
    struct stat fileAtt;
    
    // Use the stat function to get the information
    // start will be 0 when it succeeds
    // So on non-zero, throw an exception
    if( stat(pPath, &fileAtt) != 0 )
        return false;
    
    return S_ISDIR( fileAtt.st_mode );
}

//------------------------------------------------------------------------------

bool x_io::doesFileExists( const char* pFile ) noexcept
{
    // The variable that holds the file information
    // the type stat and function stat have exactly the same names, so to refer the type, we put struct before it to indicate it is an structure.
    struct stat fileAtt;
    
    // Use the stat function to get the information
    // start will be 0 when it succeeds
    // So on non-zero, throw an exception
    if( stat(pFile, &fileAtt) != 0 )
        return false;

    // S_ISREG is a macro to check if the filepath referers to a file.
    // If you don't know what a macro is, it's ok, you can use S_ISREG as any other function, it 'returns' a bool.
    return !S_ISDIR(fileAtt.st_mode);
}

#elif _X_TARGET_WINDOWS

#include <Windows.h>
#include <iostream>

using namespace std;

bool dir::doesPathExists( const char* pPath ) noexcept
{
    //This will get the file attributes bitlist of the file
    const DWORD fileAtt = GetFileAttributesA( pPath );
    
    //If an error occurred it will equal to INVALID_FILE_ATTRIBUTES
    //So lets throw an exception when an error has occurred
    if( fileAtt == INVALID_FILE_ATTRIBUTES )
        return false;

    return true;
}

//------------------------------------------------------------------------------

bool doesFileExists( const char* pFile ) noexcept
{
    // This will get the file attributes bitlist of the file
    const DWORD fileAtt = GetFileAttributesA( pFile );
    
    // If an error occurred it will equal to INVALID_FILE_ATTRIBUTES
    // So lets throw an exception when an error has occurred
    if( fileAtt == INVALID_FILE_ATTRIBUTES )
        return false;
    
    // If the path refers to a directory it should also not exists.
    return ( fileAtt & FILE_ATTRIBUTE_DIRECTORY ) == 0;
}

#endif

//------------------------------------------------------------------------------
// END
//------------------------------------------------------------------------------
}

