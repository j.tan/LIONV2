//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// JOB BASE
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
inline      
x_job_base& x_job_base::setupDefinition ( definition Def )  noexcept 
{ 
    x_assert_linear( m_Debug_LQ ); 
    m_Definition = Def; 
    return *this;
}

//------------------------------------------------------------------------------
inline      
x_job_base& x_job_base::setupJobType ( jobtype JobType ) noexcept 
{ 
    x_assert_linear( m_Debug_LQ ); 
    m_Definition.m_JOBTYPE  = JobType; 
    return *this;                 
}

//------------------------------------------------------------------------------
inline      
x_job_base& x_job_base::setupLifeTime ( lifetime LifeTime )   noexcept 
{ 
    x_assert_linear( m_Debug_LQ ); 
    m_Definition.m_LIFETIME = LifeTime; 
    return *this;                
}

//------------------------------------------------------------------------------
inline      
x_job_base& x_job_base::setupPriority ( priority Priority ) noexcept 
{ 
    x_assert_linear( m_Debug_LQ ); 
    m_Definition.m_PRIORITY = Priority; 
    return *this;                
}

//------------------------------------------------------------------------------

inline 
void x_job_base::NotifyTrigger( x_trigger_base& Trigger ) noexcept
{
    Trigger.qt_onNotify();
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// X_JOB
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template< unsigned int T_NUM_TRIGGERS > inline
void x_job<T_NUM_TRIGGERS>::qt_onDone( void ) noexcept  
{ 
    x_assert_linear( m_Debug_LQ ); 
    while( m_nTriggers )
    {
        NotifyTrigger( *m_TriggerList[--m_nTriggers] ); 
    }
}

//------------------------------------------------------------------------------
template< unsigned int T_NUM_TRIGGERS > inline
void x_job<T_NUM_TRIGGERS>::onAppenTrigger( x_trigger_base& Trigger ) noexcept  
{ 
    x_assert_linear( m_Debug_LQ ); 
    x_assume( m_nTriggers < T_NUM_TRIGGERS ); 
    m_TriggerList[m_nTriggers++] = &Trigger; 
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// TRIGGER BASE
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
x_orinlineconst 
x_trigger_base::x_trigger_base( flags Flags ) noexcept :
    m_NotificationCounter{ 0 },
    m_bDeleteWhenDone{ Flags == LIFETIME_DELETE_WHEN_DONE }
{
    x_assert_linear( m_Debug_LQ );
}

//------------------------------------------------------------------------------
x_orinlineconst 
bool x_trigger_base::isGoingToBeDeletedWhenDone( void ) const noexcept
{
    x_assert_quantum( m_Debug_LQ );
    // this could run in quantum or linear
    return m_bDeleteWhenDone;
}

//------------------------------------------------------------------------------
inline
void x_trigger_base::Join ( void ) noexcept
{
    g_context::get().m_Scheduler.ProcessWhileWait( *this );
}

//------------------------------------------------------------------------------
template< class T_JOB > inline
void x_trigger_base::JobWillNotifyMe ( T_JOB& Job ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    ++m_NotificationCounter;
    Job.onAppenTrigger( *this );
}

//------------------------------------------------------------------------------
inline
void x_trigger_base::DontTriggerUntilReady( void ) noexcept
{
    x_assert_linear( m_Debug_LQ );

    // we increment by one to make sure we don't trigger
    // this function should be call in the linear world
    x_assert( m_Debug_bReady == true );
    x_assume( m_NotificationCounter == 0 );
    m_NotificationCounter.fetch_add( 1, std::memory_order_relaxed );
    CMD_DEBUG( m_Debug_bReady = false );
}

//------------------------------------------------------------------------------
inline
void x_trigger_base::DoTriggerWhenReady( std::atomic<bool>* pNotify ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assume( m_NotificationCounter > 0 );

    // This assert will happen if you did not call DontTriggerUntilReady
    // this is require for triggers that are dealing with the quantum world
    // if everything happens in the linear world then DontTriggerUntilReady function 
    // is not needed, so this function should not be call neither.
    x_assert( m_Debug_bReady == false );
    CMD_DEBUG( m_Debug_bReady    = true );
    m_pNotifyVar        = pNotify;

    // Life time for "this" could be gone after qt_onNotify
    qt_onNotify();
    //////////////////
    //
    // DEAD SPACE - by qt_onNotify
    //
    //////////////////
}

//------------------------------------------------------------------------------
inline
void x_trigger_base::qt_onNotify( void ) noexcept 
{
    // Can not have this assert here since by the time the destructor is call the class could be gone
    // x_assert_quantum( m_Debug_LQ );
    X_SHEDULER_LOG( "Trigger: notify: " << m_NotificationCounter << "\n" );
    x_assume( m_NotificationCounter > 0 );
    if( --m_NotificationCounter == 0 )
    {
        X_SHEDULER_LOG( "Trigger: Ready to Releasing Jobs!\n" )

        // Lets backup the notification flag
        // notification should happen at the very end since after that we are back into some unknown state
        std::atomic<bool>*   pNotifyVar = m_pNotifyVar;
        m_pNotifyVar = nullptr;

        // Backup also the delete flag
        const bool bDelete = isGoingToBeDeletedWhenDone();

        // Notify our OO-hierarchy that we are ready to release jobs
        // Here the user may do some nasty things, so that is why we play safe and backup the delete flag
        // This is the last safe point in this function after this we assume dead space
        qt_onTriggered();

        ///// DEAD SPACE FROM HERE DOWN
        // lets release the object if we have to
        // note that deleting this here is not a great since we are still inside the class
        // but because other workers could be here anyways it does not matter too much.
        if( bDelete ) x_delete(this);
        //////
        //
        // DEAD SPACE
        //
        //////
        // now we are really officially done, so notify people that may care
        if( pNotifyVar ) *pNotifyVar = false;
    }
    ////////////////
    //
    // DEAD SPACE!!! By self (means the class could be deleted at this point)
    //
    ////////////////
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// X TRIGGER 
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template< int T_MAX_JOBS, typename T_JOB_CLASS > inline
x_trigger<T_MAX_JOBS,T_JOB_CLASS>::~x_trigger ( void ) noexcept
{
    x_assume( m_NotificationCounter == 0 );
    x_assert( m_Debug_bReady == true );
}

//------------------------------------------------------------------------------
template< int T_MAX_JOBS, typename T_JOB_CLASS > inline
void x_trigger<T_MAX_JOBS,T_JOB_CLASS>::AddJobToBeTrigger( x_job_base& Job ) noexcept
{
    const int iLocal = m_nJobsToTrigger++;
#if _X_DEBUG
    if( m_Debug_bReady == false )
    {
        x_assert_quantum( m_Debug_LQ );
        m_JobToTrigger[iLocal] = &Job;
    }
    else
    {
        x_assert_linear( m_Debug_LQ );
        m_JobToTrigger[iLocal] = &Job;
    }
#else
    m_JobToTrigger[iLocal] = &Job;
#endif
}

//------------------------------------------------------------------------------
template< int T_MAX_JOBS, typename T_JOB_CLASS > inline
void x_trigger<T_MAX_JOBS,T_JOB_CLASS>::qt_onTriggered( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ ); 

    const int nJobs = m_nJobsToTrigger;
    for( int i=0; i<nJobs; i++ )
    {
        g_context::get().m_Scheduler.AddJobToQuantumWorld( *m_JobToTrigger[i] );
    }

    if( nJobs )
    {
        X_SHEDULER_LOG( "Trigger: Released [" << nJobs << "] Jobs.\n" );
    }
    else
    {
        X_SHEDULER_LOG( "Trigger: is Completed with no jobs to release\n" );
    }
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// JOB BLOCK 
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
inline
x_job_block::x_job_block( void ) noexcept
{
    DontTriggerUntilReady();
        
    x_assert_linear( m_Debug_LQ );
    for( int i = 0; i < MAX_CONCURRENT_JOBS; i++ )
    {
        auto& Job = m_FreeJobs.get( i );
        Job.m_pJobBlock = this;
        Job.setupJobType( x_job_base::JOBTYPE_LIGHT );
    }
    m_ProcessState.store( STATE_PROCESS_STATE_READY );
}

//------------------------------------------------------------------------------
inline
x_job_block::~x_job_block( void ) noexcept
{
    x_assume( m_bDeleteWhenDone == false );
    x_assume( m_ProcessState == STATE_PROCESS_STATE_DONE );
    x_assert_linear( m_Debug_LQ );
}

//------------------------------------------------------------------------------
inline
void x_job_block::SubmitJob( const xfunction<void( void )> func ) noexcept
{
    // Get everything ready
    if( m_ProcessState == STATE_PROCESS_STATE_DONE )
    {
        x_assert_linear( m_Debug_LQ );
        DontTriggerUntilReady();
        m_ProcessState.store( STATE_PROCESS_STATE_READY );
    }

    x_assert_quantum( m_Debug_LQ );
    lambda_job* pFreeJob;

    // If we have not empty slots then help process work
    while( (pFreeJob = m_FreeJobs.pop()) == nullptr )
    {
        auto& S = g_context::get().m_Scheduler;
        x_job_base* pJob = S.getLightJob();
        if( pJob ) S.ProcessWork( pJob );
    }

    // increment the total jobs that must notify me 
    ++m_NotificationCounter;

    // setup the callback
    pFreeJob->m_Function = func;

    g_context::get().m_Scheduler.AddJobToQuantumWorld( *pFreeJob );
}

//------------------------------------------------------------------------------
inline
void x_job_block::Join( void ) noexcept
{
    x_assert_quantum( m_Debug_LQ );
    x_assume( m_bDeleteWhenDone == false );

    if( m_ProcessState == STATE_PROCESS_STATE_READY )
    {
        g_context::get().m_Scheduler.ProcessWhileWait( *this );
        ////////
        //
        // Dead Space -- at this point the this pointer could be invalid
        //
        ////////
    }
}

//------------------------------------------------------------------------------
inline // virtual
void x_job_block::qt_onNotify( void ) noexcept 
{
    // Can not have this assert here since by the time the destructor is call the class could be gone
    // x_assert_quantum( m_Debug_LQ );
        
    X_SHEDULER_LOG( "Trigger: notify: " << m_NotificationCounter << "\n" );
    x_assume( m_NotificationCounter > 0 );
    if( --m_NotificationCounter == 0 )
    {
        X_SHEDULER_LOG( "Trigger: Ready to Releasing Jobs!\n" )
            
        // Notify our OO-hierarchy that we are ready to release jobs
        qt_onTriggered();
            
        x_assume( isGoingToBeDeletedWhenDone() == false );
            
        // now we are really officially done, so notify people that may care
        if( m_pNotifyVar ) *m_pNotifyVar = false;
    }
}

//------------------------------------------------------------------------------
inline // virtual
void x_job_block::qt_onTriggered( void ) noexcept  
{
    x_assert_quantum( m_Debug_LQ );
    x_assume( m_ProcessState == STATE_PROCESS_STATE_READY );
    m_ProcessState.store( STATE_PROCESS_STATE_DONE );
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// JOB CHAIN
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

namespace x_scheduler_jobs{ namespace job_chain
{
    //------------------------------------------------------------------------------
    inline      
    bool state_block::function_context::NextState( void ) noexcept
    {
        // Update states
        m_iStackIndex = 1 - m_iStackIndex;
        m_iFunction++; 

        // watch out for end state
        if( m_iFunction == m_pBlock->m_nFuctions )
        {
            if( m_pBlock->m_pNext == nullptr )
                return false;
            m_pBlock    = m_pBlock->m_pNext;
            m_iFunction = 0;
            x_assume( 0 != m_pBlock->m_nFuctions );
        }

        return true;
    }

    //------------------------------------------------------------------------------
    inline 
    void state_block::function_context::ExecuteStates( void ) noexcept
    {
        // Can we consume any states?
        if( isEndState() ) 
            return;

        // Start consuming states
        do
        {
            // Execute this future
            auto& Futures = m_pBlock->m_Futures[m_iFunction];
            Futures.m_pSpecialCall( Futures.m_Function, getRetBuff(), getArgBuff() );
            
        } while( NextState() );
    }

    //-------------------------------------------------------------------------------------------

    template< typename T > inline constexpr
    void state_block::t_DeleteSharedInstance( T* pData ) noexcept
    {
        // Delete the hold chain
        if ( pData->m_pNext ) t_DeleteSharedInstance( pData->m_pNext );
        g_context::get().m_Scheduler.system_JobChainBlockPool.push( reinterpret_cast<state_block&>(*x_destruct( pData )) );
    }

    //-------------------------------------------------------------------------------------------

    template< typename T_CLASS, typename ...T_ARG > inline constexpr
    T_CLASS* state_block::t_NewSharedInstance( T_ARG&&...Args ) noexcept
    {
        return x_construct( T_CLASS, g_context::get().m_Scheduler.system_JobChainBlockPool.pop(), { Args... } );
    }

    //-------------------------------------------------------------------------------------------

    template< typename T_RET, typename T_ARG >
    struct linear_future
    {
        static void SpecialFunction( const xbyte* pFunction, void* pRet, void* pArg )
        {
            static_assert( sizeof(T_RET) < sizeof(state_block::stack_space), "This wont feed in the stack" );
            auto& ProperFunction = reinterpret_cast<const xfunction<T_RET( T_ARG )>&>( *pFunction );    
            (void)x_construct( T_RET, pRet, { ProperFunction( *reinterpret_cast<T_ARG*>(pArg) ) } );
        }
    };

    //-------------------------------------------------------------------------------------------

    template< typename T_RET >
    struct linear_future<T_RET,void>
    {
        static void SpecialFunction( const xbyte* pFunction, void* pRet, void* pArg )
        {
            static_assert( sizeof(T_RET) < sizeof(state_block::stack_space), "This wont feed in the stack" );
            auto& ProperFunction = reinterpret_cast<const xfunction<T_RET( void )>&>( *pFunction );    
            (void)x_construct( T_RET, pRet, { ProperFunction() } );
        }
    };

    //-------------------------------------------------------------------------------------------

    template< typename T_ARG >
    struct linear_future<void, T_ARG>
    {
        static void SpecialFunction( const xbyte* pFunction, void* pRet, void* pArg )
        {
            auto& ProperFunction = reinterpret_cast<const xfunction<void( T_ARG )>&>( *pFunction );
            ProperFunction( *reinterpret_cast<T_ARG*>(pArg) );
        }
    };

    //-------------------------------------------------------------------------------------------

    template<>
    struct linear_future<void, void>
    {
        static void SpecialFunction( const xbyte* pFunction, void* pRet, void* pArg )
        {
            reinterpret_cast<const xfunction<void(void)>&>( *pFunction )();
        }
    };

    //-------------------------------------------------------------------------------------------
    inline // virtual
    void lambda_job::qt_onDone( void ) noexcept
    { 
        m_bHasCompleted.store( true );
        // Remove our quantum reference
        t_SubtractSharedReference<lambda_job>( this );
        /////
        // 
        // DEAD SPACE
        //
        /////
    }

    //-------------------------------------------------------------------------------------------
    // This function will make sure that in fact is completed
    inline 
    bool lambda_job::Complete( void ) noexcept
    {
        if( m_bHasCompleted.load() )
        {
            if( m_Context.isEndState() == false )
            {
                m_Context.ExecuteStates();
            }

            return true;
        }
        return false; 
    }

    //-------------------------------------------------------------------------------------------
    template< typename T > inline constexpr
    void lambda_job::t_DeleteSharedInstance( T* pData ) noexcept
    {
        g_context::get().m_Scheduler.system_LambdaJobChainPool.push( reinterpret_cast<lambda_job&>(*x_destruct( pData )) );
    }

    //-------------------------------------------------------------------------------------------
    template< typename T_CLASS, typename ...T_ARG > inline constexpr
    T_CLASS* lambda_job::t_NewSharedInstance( T_ARG&&...Args ) noexcept
    {
        return x_construct( T_CLASS, g_context::get().m_Scheduler.system_LambdaJobChainPool.pop(), { Args... } );
    }

}};

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET >
template< typename T_FUNCTION > inline
auto x_job_chain<T_JOB_CHAIN_FUNCTION,T_RET>::Create( const T_FUNCTION Function ) noexcept
{
    using function_traits       = xfunction_traits<T_FUNCTION>;
    using function_arg          = typename function_traits::template t_arg<0>::type;
    using function_return       = typename function_traits::t_return_type;
    using function_type         = typename x_make_function< function_return, function_arg >::type; //function_return( function_arg );
    using new_job_chain         = x_job_chain< t_job_chain_type, function_return >;

    static_assert( function_traits::t_arg_count <= 1, "Tasks can not have more than one parameter" );

    // Create new job
    new_job_chain Task;
    Task.m_hStateBlock.New();

    auto&                           Block     = (*Task.m_hStateBlock);
    auto&                           Futures   = Block.m_Futures[0];
    const xfunction<function_type> tempfunc{ Function };

    static_assert( sizeof(decltype(Futures.m_Function)) == sizeof(decltype(tempfunc)), "xfunctions should always be the same size" );
    x_memcpy( Futures.m_Function, &tempfunc, sizeof(decltype(Futures.m_Function)) );
    Futures.m_pSpecialCall   = &x_scheduler_jobs::job_chain::linear_future<function_return, function_arg>::SpecialFunction;
    Block.m_nFuctions        = 1;

    return Task;
};

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET >
template< typename T_FUNCTION > inline                   
auto& x_job_chain<T_JOB_CHAIN_FUNCTION,T_RET>::Then( const T_FUNCTION&& Function ) noexcept
{
    using function_traits       = xfunction_traits<T_FUNCTION>;
    using function_arg          = typename function_traits::template t_arg<0>::type;
    using function_return       = typename function_traits::t_return_type;
    using function_type         = typename x_make_function< function_return, function_arg >::type; //function_return( function_arg );
    using new_job_chain         = x_job_chain< t_job_chain_type, function_return >;
      
    static_assert( function_traits::t_arg_count <= 1, "Tasks can not have more than one parameter" );
    static_assert( std::is_same< 
                        function_arg,
                        t_function_return
                        >::value,
                        "The return type of the previous job_chain is not convertible to the argument type of the next job_chain"  );


    // Create an function entry
    auto* pBlock  = &(*m_hStateBlock);
    auto& Futures = [&]() -> x_scheduler_jobs::job_chain::state_block::executable_state& 
    {
        while( pBlock->m_nFuctions == pBlock->m_Futures.getCount() )
        {
            if( pBlock->m_pNext == nullptr )
                pBlock->m_pNext = x_scheduler_jobs::job_chain::state_block::t_NewSharedInstance<x_scheduler_jobs::job_chain::state_block>();
            pBlock = pBlock->m_pNext;    
        }
        return pBlock->m_Futures[ pBlock->m_nFuctions ];
    }();

    Futures.m_Function       = reinterpret_cast<decltype(Futures.m_Function)&>   ( x_lvalue< xfunction<function_type> >( xfunction<function_type>{ Function } ) );
    Futures.m_pSpecialCall   = reinterpret_cast<decltype(Futures.m_pSpecialCall)>( &x_scheduler_jobs::job_chain::linear_future<function_return, function_arg>::SpecialFunction );
        
    // This delay increment is to make sure that the entry is fully full before we increment the count
    pBlock->m_nFuctions++;

    return reinterpret_cast<new_job_chain&>(*this);
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > 
template< typename T  >  inline
void x_job_chain<T_JOB_CHAIN_FUNCTION,T_RET>::Join( T&& Arg ) noexcept
{
    static_assert( std::is_convertible<
                    typename std::remove_reference<T>::type,
                    t_job_chain_function_arg
                    >::value 
                    ||
                    std::is_same<
                    t_job_chain_function_arg, 
                    const typename std::remove_reference<T>::type
                    >::value, 
                    "wrong argument type --- The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    static_assert( std::is_same<
                    t_function_return, 
                    t_job_chain_function_return
                    >::value, 
                    "The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    static_assert( std::is_same<
                    t_function_return,
                    void>::value, "You can not call wait when your job_chain-return type is not null, call 'get' stead" );

    x_scheduler_jobs::job_chain::state_block::function_context Context;
    Context.m_pBlock = &(*m_hStateBlock);
    (void)x_construct( t_job_chain_function_arg, Context.getArgBuff(), { x_move(Arg) } );
    Context.ExecuteStates();
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > inline
void x_job_chain<T_JOB_CHAIN_FUNCTION,T_RET>::Join( void ) noexcept
{
    static_assert( std::is_same<
                    t_job_chain_function_arg, 
                    void
                    >::value, 
                    "wrong argument type --- The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    static_assert( std::is_same<
                    t_function_return, 
                    t_job_chain_function_return
                    >::value, 
                    "The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    static_assert( std::is_same<
                    t_function_return,
                    void>::value, "You can not call wait when your job_chain-return type is not null, call 'get' stead" );

    x_scheduler_jobs::job_chain::state_block::function_context Context;
    Context.m_pBlock = &(*m_hStateBlock);
    Context.ExecuteStates();
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > 
template< typename T  > inline
auto x_job_chain<T_JOB_CHAIN_FUNCTION,T_RET>::get( T&& Arg ) noexcept
{
    static_assert( std::is_same<
                    t_job_chain_function_arg, 
                    T
                    >::value, 
                    "wrong argument type --- The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    static_assert( !std::is_same<
                    t_function_return,
                    void>::value, "You can not call a get when the job_chain-return type is void, call 'wait' stead" );

    static_assert( std::is_same<
                    t_function_return, 
                    t_job_chain_function_return
                    >::value, 
                    "The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );
        
    x_scheduler_jobs::job_chain::state_block::function_context Context;
    Context.m_pBlock = &(*m_hStateBlock);
    (void)x_construct( t_job_chain_function_arg, Context.getArgBuff(), { std::move(Arg) } );
    Context.ExecuteStates();
    return *reinterpret_cast<t_job_chain_function_return*>(Context.getArgBuff());
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > inline 
auto x_job_chain<T_JOB_CHAIN_FUNCTION,T_RET>::get( void ) noexcept
{
    static_assert( std::is_same<
                    t_job_chain_function_arg, 
                    void
                    >::value, 
                    "wrong argument type --- The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    static_assert( !std::is_same<
                    t_function_return,
                    void>::value, "You can not call a get when the job_chain-return type is void, call 'wait' stead" );
                         
    static_assert( std::is_same<
                    t_function_return, 
                    t_job_chain_function_return
                    >::value, 
                    "The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    x_scheduler_jobs::job_chain::state_block::function_context Context;
    Context.m_pBlock = &(*m_hStateBlock);
    Context.ExecuteStates();
    return std::move(*reinterpret_cast<t_job_chain_function_return*>(Context.getArgBuff()));
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > 
template< typename T > inline
auto x_job_chain<T_JOB_CHAIN_FUNCTION,T_RET>::Run( T&& Arg, x_job_base::jobtype Type ) noexcept
{
    static_assert( std::is_convertible< 
                        typename std::remove_reference<T>::type, 
                        t_job_chain_function_arg
                    >::value 
                    ||
                    std::is_same<
                        t_job_chain_function_arg, 
                        const typename std::remove_reference<T>::type
                    >::value, 
                    "wrong argument type --- The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    static_assert( std::is_same< 
                        t_function_return, 
                        t_job_chain_function_return
                    >::value, 
                    "The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    auto& Scheduler = g_context::get().m_Scheduler;
    x_ll_share_ref< x_scheduler_jobs::job_chain::lambda_job, int >    hJob      {};

    typename x_job_chain<t_job_chain_type,t_function_return>::executable_instance ExecI;
    ExecI.m_hLambdaJob.New();

    // setup the job
    auto& Block                     = *m_hStateBlock;
    auto& LambdaJob                 = *ExecI.m_hLambdaJob;

    LambdaJob.setupJobType( Type );
    LambdaJob.m_Context.m_pBlock    = &Block;
    LambdaJob.m_hStateBlock         = m_hStateBlock;         
    
    (void)x_construct( t_job_chain_function_arg, LambdaJob.m_Context.getArgBuff(), { std::move(Arg) } );

    // Add extra reference because it is in the quantum world
    LambdaJob.template t_AddSharedReference<x_scheduler_jobs::job_chain::lambda_job>( &LambdaJob );
    Scheduler.AddJobToQuantumWorld( LambdaJob );

    // return the executable instance
    return std::move(ExecI);
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > inline 
auto x_job_chain<T_JOB_CHAIN_FUNCTION,T_RET>::Run( x_job_base::jobtype Type ) noexcept
{
    static_assert(  std::is_same<
                    t_job_chain_function_arg, 
                    void
                    >::value, 
                    "wrong argument type --- The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    static_assert( std::is_same<
                    t_function_return, 
                    t_job_chain_function_return
                    >::value, 
                    "The type of job_chain 'job_chain<ret_type(arg_type>' and the final continuation (job_chain/job) return type, must be the same" );

    auto& Scheduler = g_context::get().m_Scheduler;
    x_ll_share_ref< x_scheduler_jobs::job_chain::lambda_job, int >    hJob      {};

    typename x_job_chain<t_job_chain_type,t_function_return>::executable_instance ExecI;
    ExecI.m_hLambdaJob.New();

    // setup the job
    auto& Block                     = *m_hStateBlock;
    auto& LambdaJob                 = *ExecI.m_hLambdaJob;

    LambdaJob.setupJobType( Type );
    LambdaJob.m_Context.m_pBlock    = &Block;
    LambdaJob.m_hStateBlock         = m_hStateBlock;         
    
    // Add extra reference because it is in the quantum world
    LambdaJob.template t_AddSharedReference<x_scheduler_jobs::job_chain::lambda_job>( &LambdaJob );
    Scheduler.AddJobToQuantumWorld( LambdaJob );

    // return the executable instance
    return std::move(ExecI);
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > inline 
auto x_job_chain<T_JOB_CHAIN_FUNCTION, T_RET>::executable_instance::getTask( void ) noexcept
{
    x_job_chain<t_job_chain_type, t_function_return> X;
    X.m_hStateBlock = m_hLambdaJob->m_hStateBlock;
    return std::move(X);
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > inline 
auto x_job_chain<T_JOB_CHAIN_FUNCTION, T_RET>::executable_instance::get( void ) noexcept
{
    static_assert( !std::is_same<
                    void,
                    t_job_chain_function_return>::value, "You can not call a get when the job_chain-return type is void, call 'Join' stead" );

    Sync();
    return *reinterpret_cast<t_job_chain_function_return*>( m_hLambdaJob->m_Context.getArgBuff() );
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > inline 
void x_job_chain<T_JOB_CHAIN_FUNCTION, T_RET>::executable_instance::Join( void ) noexcept
{
    static_assert( std::is_same<
                    t_function_return,
                    void>::value, "You can not call a get when the job_chain-return type is void, call 'get' stead" );
    Sync();
}

//-------------------------------------------------------------------------------------------
template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > inline 
void x_job_chain<T_JOB_CHAIN_FUNCTION, T_RET>::executable_instance::Sync( void ) noexcept
{
    auto& LambdaJob = *m_hLambdaJob;
    if( true == LambdaJob.Complete() ) 
        return;

    auto& Scheduler = g_context::get().m_Scheduler;
    do 
    {
        auto* pJob = Scheduler.getJob();  // should be a light job?
        if( pJob ) Scheduler.ProcessWork( pJob );

    } while( LambdaJob.Complete() == false );
}




