//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------

inline 
xfile& xserialfile::getW( void ) const noexcept
{
    x_assert( m_pWrite );
    return m_pWrite->m_Packs[ m_iPack ].m_Data;
}

//------------------------------------------------------------------------------

inline 
bool xserialfile::isLocalVariable( u8* pRange ) noexcept
{
    return (pRange >= m_pClass) && (pRange < ( m_pClass + m_ClassSize ));
}

//------------------------------------------------------------------------------

inline 
s32 xserialfile::ComputeLocalOffset( u8* pItem ) noexcept
{
    x_assert( isLocalVariable( pItem ) );
    return (s32)(pItem - m_pClass);
}

//------------------------------------------------------------------------------

template< class T > inline
xfile::err xserialfile::Save( const xwstring& FileName, const T& Object, mem_type ObjectFlags, bool bEndianSwap ) noexcept
{
    // Open the file to make sure we are okay
    xfile File;
    
    // TODO: Set the endian swap flag
    auto Err = File.Open( FileName, "wb" );
    if( Err ) return Err;

    // Call the actual save
    Err = Save( File, Object, ObjectFlags, bEndianSwap );

    // done with the file
    File.Close();

    return Err;
}

//------------------------------------------------------------------------------

template< class T > inline
xfile::err xserialfile::Save( xfile& File, const T& Object, mem_type ObjectFlags, bool bSwapEndian ) noexcept
{
    //
    // Allocate the writing structure
    //
    xndptr_s<writting> Write;  
    Write.New();

    // Assign it so it is accessible for other functions
    // but we keep the owner
    m_pWrite = Write;

    // back up the pointer
    Write->m_pFile = &File;

    //
    // Initialize class members
    //
    m_iPack             = Write->AllocatePack( ObjectFlags );
    m_ClassPos          = 0;
    m_pClass            = (u8*)((void*)&Object);
    m_ClassSize         = sizeof(Object);
    Write->m_bEndian    = bSwapEndian;

    // Save the initial class
    getW().putC( ' ', m_ClassSize, true );

    // Start the saving 
    Object.SerializeIO( *this );

    // Save the file
    auto Err = SaveFile();

    // clean up
    m_pWrite = nullptr;

    return Err;
}

inline void xserialfile::Handle( const s8&   A ) noexcept { getW().Write( A );   }
inline void xserialfile::Handle( const s16&  A ) noexcept { getW().Write( A );   }
inline void xserialfile::Handle( const s32&  A ) noexcept { getW().Write( A );   }
inline void xserialfile::Handle( const s64&  A ) noexcept { getW().Write( A );   }

inline void xserialfile::Handle( const u8&   A ) noexcept { getW().Write( A );   }
inline void xserialfile::Handle( const u16&  A ) noexcept { getW().Write( A );   }
inline void xserialfile::Handle( const u32&  A ) noexcept { getW().Write( A );   }
inline void xserialfile::Handle( const u64&  A ) noexcept { getW().Write( A );   }

inline void xserialfile::Handle( const f32&  A ) noexcept { getW().Write( A );   }
inline void xserialfile::Handle( const f64&  A ) noexcept { getW().Write( A );   }

//------------------------------------------------------------------------------

inline void xserialfile::Handle( const xmatrix4&    A ) noexcept { getW().WriteList( xbuffer_view<const u32>{ (const u32*)&A, xuptr{sizeof(xmatrix4)/sizeof(f32)}     } );  }
inline void xserialfile::Handle( const xvector3&    A ) noexcept { getW().WriteList( xbuffer_view<const u32>{ (const u32*)&A, xuptr{sizeof(xvector3)/sizeof(f32)}     } );  }
inline void xserialfile::Handle( const xvector3d&   A ) noexcept { getW().WriteList( xbuffer_view<const u32>{ (const u32*)&A, xuptr{sizeof(xvector3d)/sizeof(f32)}    } );  }
inline void xserialfile::Handle( const xbbox&       A ) noexcept { getW().WriteList( xbuffer_view<const u32>{ (const u32*)&A, xuptr{sizeof(xbbox)/sizeof(f32)}        } );  }
inline void xserialfile::Handle( const xcolor&      A ) noexcept { getW().WriteList( xbuffer_view<const u32>{ (const u32*)&A, xuptr{1}                                } );  }
inline void xserialfile::Handle( const xvector2&    A ) noexcept { getW().WriteList( xbuffer_view<const u32>{ (const u32*)&A, xuptr{sizeof(xvector2)/sizeof(f32)}     } );  }
inline void xserialfile::Handle( const xvector4&    A ) noexcept { getW().WriteList( xbuffer_view<const u32>{ (const u32*)&A, xuptr{sizeof(xvector4)/sizeof(f32)}     } );  }
inline void xserialfile::Handle( const xquaternion& A ) noexcept { getW().WriteList( xbuffer_view<const u32>{ (const u32*)&A, xuptr{sizeof(xquaternion)/sizeof(f32)}  } );  }
template< typename T >
inline void xserialfile::Handle( const xguid<T>&    A ) noexcept { getW().Write( A.m_Value );                                          }

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Handle( const T& A ) noexcept
{
    // Reading or writing?
    // TODO: Add support for complex reading
    if( m_pWrite == NULL ) 
        return;

    // Copy most of the data
    xserialfile File(*this);

    File.m_iPack        = m_iPack;
    File.m_ClassPos     = x_static_cast<u32>(getW().Tell());
    File.m_pClass       = (u8*)&A;
    File.m_ClassSize    = sizeof( A );
    A.SerializeIO( File );

    // Something very strange happen. We allocated more memory space than we expected
    //x_assert( getW().Tell() <= (File.m_ClassPos + sizeof( A )) );

    // Go the end of the structure 
    getW().SeekOrigin( File.m_ClassPos + sizeof( A ));
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Array( const T& A, s32 Count ) noexcept
{
    u8* pA = (u8*)((void*)A);

    x_assert( Count >= 0 );

    // Make sure that we are at the right offset
    getW().SeekOrigin( m_ClassPos + ComputeLocalOffset( pA ) );

    // Is an array of structures of items so we want to preallocate the space
    // Since it is an array we don't need to put the "* Count"
    getW().putC( ' ', sizeof(A), false );

    // Loop throw all the items
    for( s32 i=0; i<Count; i++ )
    {
        Handle( A[i] );
    }
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::HandlePtr( const T& A, s32 Count, mem_type MemoryFlags ) noexcept
{
    // Back up the pack index
    xbool BackupPackIndex = m_iPack;

    // Handle pointer details
    HandlePtrDetails( ((u8*)&A), sizeof(*A), Count, MemoryFlags );

    //
    // Loop throw all the items
    //

    // Now loop
    for( s32 i=0; i<Count; i++ )
    {
        Handle( A[i] );

        x_assert(  getW().Tell()>= i*(s32)sizeof(A[0]) );
    }

    //
    // Restore the old pack
    //
    m_iPack = BackupPackIndex;
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Serialize( const xdataptr<T>& A, s32 Count, mem_type MemoryFlags ) noexcept
{
    HandlePtr( A.m_pPtr, Count, MemoryFlags );
}


//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Serialize( const xdataptr<const T>& A, s32 Count, mem_type MemoryFlags ) noexcept
{
    HandlePtr( A.m_pPtr, Count, MemoryFlags );
}

//------------------------------------------------------------------------------

template< class T, s32 C > inline
void xserialfile::Serialize( const xarray<T,C>& A ) noexcept
{
    Array( &A[0], C );
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Serialize( const T& A, s32 Count, mem_type MemoryFlags ) noexcept
{ 
    const u8* pA = (const u8*)((void*)A);
    const u8* pB = (const u8*)((void*)&A);

    // Check whether is an pointer
    if( pA != pB )
    {
        // This is not longer allowed. Please use xserial::ptr structure for any pointer you need to serialize
        // TODO: I may need to think whether this is correct or not...
        //       break compatibility with 64/32bits... however virtual pointers in base class already cause trouble so... 
        x_assert(0);
        HandlePtr( A, Count, MemoryFlags );
    }
    else
    {
        Array( A, Count );  
    }
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::Serialize( const T& A ) noexcept
{ 
    u8* pA = (u8*)((void*)&A);
    x_assert( isLocalVariable( pA ), "The variable must be a member of the class" );

    // Make sure that we are at the right offset
    getW().SeekOrigin( m_ClassPos + ComputeLocalOffset( pA ) );

    // IF YOU GET AN ERROR HERE WHILE LINKING CHANCES ARE YOU ARE TRYING TO PASS
    // A POINTER WHEN YOU SHOULD USE --- Serialize( pPtr, Count, flags ) ---
    Handle( A );  
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::SerializeEnum( const T& A ) noexcept
{ 
    x_assert( sizeof(A) <= 4 );

    if( sizeof(A) == 1 ) Serialize( *((const u8*)&A)  );
    else if( sizeof(A) == 2 ) Serialize( *((const u16*)&A) );
    else if( sizeof(A) == 4 ) Serialize( *((const u32*)&A) );
    else
    {
        x_assert( 0 );
    }
}

//------------------------------------------------------------------------------

template< class T > inline
void xserialfile::SerializeEnum( const T& A, s32 Count, mem_type MemoryFlags ) noexcept
{
    x_assert( sizeof( *A ) <= 4 );

    if ( sizeof( *A ) == 1 ) Serialize( ( (const u8*)A ), Count, MemoryFlags );
    else if ( sizeof( *A ) == 2 ) Serialize( ( (const u16*)A ), Count, MemoryFlags );
    else if ( sizeof( *A ) == 4 ) Serialize( ( (const u32*)A ), Count, MemoryFlags );
    else
    {
        x_assert( 0 );
    }
}

//------------------------------------------------------------------------------

template< class T, s32 C > inline
void xserialfile::SerializeEnum( const xarray<T, C>& A ) noexcept
{
    for ( const auto& S : A )
    {
        SerializeEnum( S );
    }
}

//------------------------------------------------------------------------------

template< class T > void xserialfile::ResolveObject( T*& pObject ) noexcept
{
    // Initialize all
    (void)x_construct( T, pObject, { *this } );

    // deal with temp data
    if ( m_bFreeTempData && m_pTempBlockData )
        x_delete( (xbyte*)m_pTempBlockData );
}


//------------------------------------------------------------------------------

template< class T > inline
xfile::err xserialfile::Load( xfile& File, T*& pObject ) noexcept 
{
    auto Err = LoadHeader( File, sizeof(*pObject) );
    pObject = (T*)LoadObject( File );
    ResolveObject( pObject );
    return Err;
}

//------------------------------------------------------------------------------

template< class T > inline
xfile::err xserialfile::Load( const xwstring& FileName, T*& pObject ) noexcept 
{
    // Open the file
    xfile File;

    auto Err = File.Open( FileName, "rb" );
    if( Err ) return Err;

    // Load the object
    Load( File, pObject );

    // Close the file
    File.Close();

    return Err;
}

//------------------------------------------------------------------------------
inline
void xserialfile::setResourceVersion( u16 ResourceVersion ) noexcept
{
    m_Header.m_ResourceVersion = ResourceVersion;
}

//------------------------------------------------------------------------------
inline
u16 xserialfile::getResourceVersion( void ) const noexcept
{
    return m_Header.m_ResourceVersion;
}

//------------------------------------------------------------------------------
inline
bool xserialfile::SwapEndian( void ) const noexcept
{
    return m_pWrite->m_bEndian;
}
