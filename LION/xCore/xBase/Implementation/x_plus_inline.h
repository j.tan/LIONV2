//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//     Toggles a bit flag on or off depending of its previous status. 
// Arguments:
//     N - Is the atomic type to activate the flag. Note that the variable passed will changed its value.
//     F - Is the bit flags which identifies which which bits to toggle
// Returns:
//     void
// See Also:
//     x_FlagOn x_FlagOff x_FlagsAreOn X_BIN X_BIT x_FlagIsOn
//------------------------------------------------------------------------------
template< class T > constexpr 
void x_FlagToggle( T& N, const u32 F )
{ 
    static_assert( std::is_integral<T>::value,"" );
    N = static_cast<T>(N^F);
}

//------------------------------------------------------------------------------
// Description:
//     Turns a bit on in an atomic integer type. 
// Arguments:
//     N - Is the atomic type to activate the flag. Note that the variable passed will changed its value.
//     F - Is the flag
// Returns:
//     void
// See Also:
//     x_FlagOff x_FlagsAreOn X_BIN X_BIT x_FlagIsOn x_FlagToggle
//------------------------------------------------------------------------------
template< class T > constexpr 
void x_FlagOn( T& N, const u32 F )
{ 
    static_assert( std::is_integral<T>::value,"" );
    N = static_cast<T>(N|F); 
}

//------------------------------------------------------------------------------
// Description:
//     Turns a bit off in an atomic integer type. 
// Arguments:
//     N - Is the atomic type to deactivate the flag. Note that the variable passed will changed its value.
//     F - Is the flag
// Returns:
//     void
// See Also:
//     x_FlagOn x_FlagIsOn X_BIN X_BIT x_FlagsAreOn x_FlagToggle
//------------------------------------------------------------------------------
template< class T > constexpr 
void x_FlagOff( T& N, const u32 F )
{ 
    static_assert( std::is_integral<T>::value,"" );
    N = static_cast<T>(N & (~F)); 
}

//------------------------------------------------------------------------------
// Description:
//     Test whether in a set of flags one IS on or not.
// Arguments:
//     N - Is the atomic type that contains the current value.  
//     F - Is the set of flags to test.
// Returns:
//     TRUE if any of the flags in F turns to be on, FALSE other wise.
// See Also:
//     x_FlagOff X_BIN X_BIT x_FlagsAreOn x_FlagOn x_FlagToggle
//------------------------------------------------------------------------------
template< class T > constexpr 
bool x_FlagIsOn( const T  N, const u32 F )
{ 
    static_assert( std::is_integral<T>::value,"" );
    return !!(N&F); 
}

//------------------------------------------------------------------------------
// Description:
//     Test whether in a set of flags all ARE on or not.
// Arguments:
//     N - Is the atomic type that contains the current value.  
//     F - Is the set of flags to test.
// Returns:
//     TRUE if any of the flags in F turns to be on, FALSE other wise.
// See Also:
//     x_FlagOff X_BIN X_BIT x_FlagIsOn x_FlagOn x_FlagToggle
//------------------------------------------------------------------------------
template< class T > constexpr 
bool x_FlagsAreOn( const T  N, const u32 F )
{ 
    static_assert( std::is_integral<T>::value,"" );
    return (N&F)==F; 
}

//------------------------------------------------------------------------------
// Description:
//     Binary search of a sorted array, with where item should be in the 
//     list in case of a miss.
//     The input to the binary search functions must be a list sorted in ascending 
//     order.  The standard ANSI bsearch function does not allow for duplicates in 
//     the list.  The xBase versions handle duplicates.  
// Arguments:
//     View            -  A buffer view of the data.
//     Compare         -  A comparation lambda function.
//     iLocation       -  Where item is/should be in list.
// Returns:
//     The first true if it found the item other wise false.
// See Also:
//     x_qsort x_bsearch
//------------------------------------------------------------------------------
template< typename T_ENTRY> inline
bool x_BinSearch(   const xbuffer_view<T_ENTRY>                 View,
                    xuptr&                                      iLocation,
                    xfunction< int ( const T_ENTRY& ) >        Compare )
{
    if( View.getCount() == 0 )
    {
        iLocation = 0;
        return false;
    }

    //
    // If there are at least two items in the list, then we want to do the
    // binary search stuff.
    //
    xuptr nItems    = View.getCount();
    xuptr iLo       = 0;
    xuptr iHi       = nItems-1;
    while( iLo <= iHi )
    {
        //
        // If there are at least two items in the list, then we want to do the
        // binary search stuff.
        //
        // But if there is only one item, then just do a single compare and be
        // done with it.  The "normal" code path (for 2 or more items) would 
        // work, but it is far less efficient.
        //
        // A "no items in the list" case is impossible based on the nature of
        // the surrounding while loop.
        //
        if( nItems >= 2 )
        {
            const xuptr Half1  = nItems / 2;
            const xuptr Half2  = nItems - Half1 - 1;
            const xuptr iMid   = iLo + Half1;
            const auto  Result = Compare( View[iMid] );

            if( Result < 0 )
            {
                // The mid item is too "high".  
                // Keep searching.  New range is [Lo, Mid-1].
                iHi    = iMid - 1;
                nItems = Half1;
            }
            else if( Result > 0 )
            {
                // The mid item is too "low".  
                // Keep searching.  New range is [Mid+1, Hi].
                iLo    = iMid + 1;
                nItems = Half2;
            }
            else // if( Result == 0 )
            {
                //
                // In a version which is not prepared for duplicates, this case
                // would simply "return( pMid )".
                //
                // Instead, we will continue to search since there may be items
                // just prior to pMid which are the same.  We want to return the
                // first match in the list, not the first match we find.
                //
                // Check the pMid-1 item.  If it doesn't match, then we are 
                // assuredly done.
                //
                // Note that it is impossible for pLo and pMid to be the same at
                // this point.  (Or, more to the point, it is impossible for 
                // Mid-1 to go out of the valid range.)
                //
                x_assume( iMid >= 1 );
                s32 R;
                xuptr Iter = iMid; 
                do
                {
                    --Iter;
                    R = Compare( View[Iter] );
                    x_assume( R  >= 0 );

                } while( R > 0 && Iter > 0 );

                iLocation = Iter+1;
                return true;
            }
        }
        else
        {
            const auto Result = Compare( View[iLo] );
            if( Result == 0 )
            {
                // We got it!
                iLocation = iLo;
                return true;
            }

            //
            // If we are here, then the desired item is not in the list.  The
            // only thing remaining is to decide where it would go if it were
            // to be added.  The results from the most recent compare tell us
            // all we need to know.
            //
            if( Result > 0 )
            {
                iLocation = iLo + 1;
            }
            else // if( Result < 0 )
            {
                iLocation = iLo;
            }

            // That's it.  The pLocation has been set.  Return NULL to indicate
            // the search failed.
            return false;
        }
    }

    // If we are here, then the desired item is NOT in the list.  And, if it 
    // were to be inserted, it would go at the end of the list.  So, set the
    // pLocation variable and return NULL.
    iLocation = iHi + 1;
    return false;
}

