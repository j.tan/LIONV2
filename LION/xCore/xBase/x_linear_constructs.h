//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//------------------------------------------------------------------------------
// Description:
//          A temporary view to all the linear allocators memory.
//          It is recommended that this class is only use in function arguments.
//          You should never have an instance of this class that is not in the stack.
// Notes:
//          The real definition of xbuffer_view is in x_types. There T_COUNTER = xuptr
//          by default.
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER > 
class xbuffer_view 
{
    x_object_type( xbuffer_view, is_linear, is_not_allocatable );

public:
 
    using   t_entry         = T_ENTRY;
    using   t_counter       = T_COUNTER;
    using   t_view          = t_self;

    #define x_linear_constructs_buffer_hardness
    #define x_linear_constructs_buffer_hardness_exclude_operator_view
    #include "Implementation/x_linear_constructs_hardness.h"

public:

    constexpr               xbuffer_view                            ( t_entry* const pEntry, const t_counter n ) noexcept : m_pMem{ pEntry }, m_Count{ n } { }
    constexpr               xbuffer_view                            ( void ) = delete;
    template< typename T = t_counter >    
    constexpr T             getCount                                ( void ) const noexcept { return x_static_cast<T>(m_Count); }

    // allows to go from "pepe<const T>" --> "const pepe<T>"
    x_forceinline operator const xbuffer_view< typename std::remove_const<t_entry>::type, t_counter>& ( void ) const noexcept 
    { return reinterpret_cast< const xbuffer_view< typename std::remove_const<t_entry>::type, t_counter >&>(*this); }

protected:
    
    t_entry* const      m_pMem;
    const t_counter     m_Count;
};

//------------------------------------------------------------------------------
// Description:
//          A unique buffer (pointer owner) that allocates base on new/delete 
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER = xuptr > 
class xndptr 
{
    x_object_type( xndptr, is_linear, is_not_copyable );

public:
    
    using t_entry   = T_ENTRY;
    using t_counter = T_COUNTER;
    using t_view    = xbuffer_view< t_entry, t_counter >;

    #define x_linear_constructs_buffer_hardness
    #include "Implementation/x_linear_constructs_hardness.h"

public:

    constexpr               xndptr                                 ( void )                    = default;
    inline                  xndptr                                 ( xndptr&& Ptr )             noexcept    { setup( Ptr.TransferOwnership(), Ptr.getCount() ); }

    ~xndptr( void ) noexcept
    {
        x_delete_arrayv( m_pMem, m_Count ); //-V2002
    }
    
    void New( t_counter Count = 1 ) noexcept
    {
        Delete();
        m_Count = Count;
        m_pMem  = x_new_arrayv( t_entry, m_Count );
    }

    template<typename ...T_ARG>
    void New( t_counter Count, T_ARG&&...Args ) noexcept
    {
        Delete();
        m_Count = Count;
        m_pMem  = x_new_arrayv( t_entry, m_Count, { Args... } );
    }

    void setup( xowner<t_entry*> pData, t_counter Count ) noexcept
    {
        Delete();
        m_pMem  = pData;
        m_Count = Count; 
    }
    
    void Delete( void ) noexcept
    {
        x_delete_arrayv( m_pMem, m_Count );
        m_pMem  = nullptr;
        m_Count = 0;
    }

    template<typename ...T_ARG>
    void Resize( t_counter Count,  T_ARG&&...Args ) noexcept
    {
        if( Count == m_Count ) return;
        if( m_pMem )
        {
            if( Count > m_Count )
            {
                m_pMem = x_realloc( t_entry, m_pMem, Count );
                for( t_counter i = m_Count; i < Count; i++ )
                {
                    x_construct( t_entry, &m_pMem[i], { Args... } );
                }
                m_Count = Count;
            }
            else
            {
                for( t_counter i = Count; i < m_Count; i++ )
                {
                    x_destruct( &m_pMem[i] );
                }
                m_pMem = x_realloc( t_entry, m_pMem, Count );
                m_Count = Count;
            }
        }
        else
        {
            New( Count, Args... );
        }
    }

    xowner<t_entry*> TransferOwnership( void ) noexcept
    {
        auto pTemp = m_pMem;
        m_pMem  = nullptr;
        m_Count = 0;
        return pTemp;
    }

    template< typename T = t_counter >
    constexpr T getCount( void ) const noexcept { return x_static_cast<T>(m_Count); }

protected:
    xowner<t_entry*>    m_pMem  { nullptr   };
    t_counter           m_Count { 0         };

};

//------------------------------------------------------------------------------
// Description:
//          A unique single heap entry that allocates base on new/delete 
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER = xuptr > 
class xndptr_s 
{
    x_object_type( xndptr_s, is_linear, is_not_copyable );

public:
    
    using t_entry   = T_ENTRY;
    using t_counter = T_COUNTER;
    using t_view    = xbuffer_view< t_entry, t_counter >;

    #define x_linear_constructs_buffer_hardness
    #include "Implementation/x_linear_constructs_hardness.h"

public:

    constexpr               xndptr_s                                ( void )                                     = default;
    constexpr               explicit xndptr_s                       ( bool bConstruct )                 noexcept { if(bConstruct) New(); else m_pMem = nullptr;      }
    constexpr               explicit xndptr_s                       ( xndptr_s&& Ptr )                  noexcept { setup( Ptr.TransferOwnership() );                            }
    constexpr               xndptr_s                                ( xowner<t_entry*> pEntry )         noexcept { x_assume(pEntry); setup( pEntry );                           }

    constexpr   t_entry*    operator ->                             ( void )                    const   noexcept { x_assume(m_pMem); return  m_pMem;                         }
    constexpr               operator t_entry&                       ( void )                    const   noexcept { x_assume(m_pMem); return *m_pMem;                         }
    inline                  operator const t_entry&                 ( void )                            noexcept { x_assume(m_pMem); return *m_pMem;                         }

    constexpr               xndptr_s                                ( const xndptr_s&& Ptr )   = delete;
                xndptr_s&   operator =                              ( const xndptr_s&& Ptr )   = delete;

    ~xndptr_s( void )
    {
        x_delete( m_pMem ); //-V2002
    }
    
    template<typename ...T_ARG>
    void New( T_ARG&&...Args )
    {
        Delete();
        m_pMem  = x_new( t_entry, { Args... } );
    }

    void New( void )
    {
        Delete();
        m_pMem  = x_new( t_entry );
    }

    void setup( xowner<t_entry*> pData )
    {
        Delete();
        m_pMem  = pData;
    }
    
    void Delete( void )
    {
        x_delete( m_pMem );
        m_pMem  = nullptr;
    }

    xowner<t_entry*> TransferOwnership( void )
    {
        auto pTemp = m_pMem;
        m_pMem  = nullptr;
        return pTemp;
    }

    void move( t_self& Node )
    {
        x_delete(m_pMem);
        m_pMem = Node.m_pMem;
        Node.m_pMem = nullptr;
    }

    template< typename T = t_counter >
    constexpr T getCount( void ) const noexcept { return x_static_cast<T>(m_pMem?1:0); }

protected:
    
    xowner<t_entry*>    m_pMem  { nullptr   };
};

//------------------------------------------------------------------------------
// Description:
//          A unique buffer (pointer owner) that allocates base on alloc/free 
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER = xuptr > 
class xafptr 
{
    x_object_type( xafptr, is_linear );

public:
    
    using t_entry   = T_ENTRY;
    using t_counter = T_COUNTER;
    using t_view    = xbuffer_view< t_entry, t_counter >;
    
    #define x_linear_constructs_buffer_hardness
    #include "Implementation/x_linear_constructs_hardness.h"

public:

    x_forceconst                    xafptr                      ( xowner<t_entry*> const pPtr, t_counter Count ) noexcept : m_pMem{ pPtr }, m_Count{ Count } {}
    constexpr                       xafptr                      ( void )                                         noexcept = default;
    x_forceconst                    xafptr                      ( xafptr&& Ptr )                                 noexcept : m_pMem{ Ptr.m_pMem }, m_Count{ Ptr.m_Count }{ Ptr.m_pMem = nullptr; Ptr.m_Count=0;             }
    x_forceinline                  ~xafptr                      ( void )                                         noexcept { x_free( m_pMem )                                                                               }
    x_forceinline      xafptr&      operator =                  ( xafptr&& Ptr )                                 noexcept { setup( Ptr.TransferOwnership(), Ptr.getCount() ); return *this;                                }
    x_forceconst                    xafptr                      ( xafptr&  Ptr )                                 noexcept { setup( Ptr.TransferOwnership(), Ptr.getCount() );                                              }
    x_forceinline      xafptr&      operator =                  ( xafptr&  Ptr )                                 noexcept { setup( Ptr.TransferOwnership(), Ptr.getCount() ); return *this;                                }

    x_forceinline void Alloc( t_counter Count ) noexcept
    {
        Free();
        m_Count = Count;
        m_pMem  = x_malloc( t_entry, m_Count );
    }

    x_forceinline void setup( xowner<t_entry*> pData, t_counter Count ) noexcept
    {
        Free();
        m_pMem  = pData;
        m_Count = Count; 
    }
    
    x_forceinline void Free( void ) noexcept
    {
        x_free( m_pMem );
        m_pMem  = nullptr;
        m_Count = 0;
    }

    x_forceinline void Resize( xuptr NewSize ) noexcept
    {
        m_pMem  = x_realloc( t_entry, m_pMem, NewSize );
        m_Count = NewSize;
    }

    template< typename T = t_counter >
    constexpr T getCount( void ) const noexcept { return x_static_cast<T>(m_Count); }

    xowner<t_entry*> TransferOwnership( void ) noexcept
    {
        auto pTemp = m_pMem;
        m_pMem  = nullptr;
        m_Count = 0;
        return pTemp;
    }

protected:
    
    xowner<t_entry*>    m_pMem          { nullptr };
    t_counter           m_Count         { 0       };
};

//------------------------------------------------------------------------------
// Description:
//          A unique heap entry that allocates base on alloc/free 
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_COUNTER = xuptr > 
class xafptr_s 
{
    x_object_type( xafptr_s, is_linear, is_not_copyable );

public:
    
    using t_entry   = T_ENTRY;
    using t_counter = T_COUNTER;
    using t_view    = xbuffer_view< t_entry, t_counter >;
    
public:

    constexpr               xafptr_s                                ( void )                    = default;
    constexpr               xafptr_s                                ( xafptr_s&& Ptr ) noexcept { setup( Ptr.TransferOwnership() ); }

    constexpr   t_entry*    operator ->                             ( void ) const  noexcept    { x_assume(m_pMem); return  m_pMem;                         }
    constexpr               operator t_entry&                       ( void ) const  noexcept    { x_assume(m_pMem); return *m_pMem;                         }
    inline                  operator const t_entry&                 ( void )        noexcept    { x_assume(m_pMem); return *m_pMem;                         }

    constexpr               xafptr_s                                ( const xafptr_s&& Ptr )   = delete;
                xafptr_s&   operator =                              ( const xafptr_s&& Ptr )   = delete;

    ~xafptr_s( void )
    {
        x_free( m_pMem );
    }
    
    void Alloc( void )
    {
        Free();
        m_pMem  = x_malloc( t_entry, 1 );
    }
    
    void setup( xowner<t_entry*> pData )
    {
        Free();
        m_pMem  = pData;
    }
    
    void Free( void )
    {
        x_free( m_pMem );
        m_pMem  = nullptr;
    }

    template< typename T = t_counter >
    constexpr T getCount( void ) const noexcept { return x_static_cast<T>(m_pMem?1:0); }

    xowner<t_entry*> TransferOwnership( void )
    {
        auto pTemp = m_pMem;
        m_pMem  = nullptr;
        return pTemp;
    }

protected:
    
    xowner<t_entry*>    m_pMem          { nullptr };
};

//------------------------------------------------------------------------------
// Description:
//          A replacement for the standard std:array. It plays nices with
//          the rest of the system sharing common code and functionality. 
//          farther more it does not construct object by itself so mamory
//          remains raw unless the user construct it.
//------------------------------------------------------------------------------
template< typename T_ENTRY, xuptr T_COUNT, typename T_COUNTER = xuptr > 
class xarray_raw 
{
    x_object_type( xarray_raw, is_linear );

public:
    
    using                       t_entry   = T_ENTRY;
    using                       t_counter = T_COUNTER;
    using                       t_view    = xbuffer_view< t_entry, t_counter >;
    x_constexprvar  t_counter   t_count   = static_cast<t_counter>(T_COUNT);
    
    #define x_linear_constructs_buffer_hardness
    #include "Implementation/x_linear_constructs_hardness.h"

public:

    template< class T = t_counter >  
    constexpr T         getCount    ( void )            const noexcept  { return x_static_cast<T>(T_COUNT); }

    //------------------------------------------------------------------------------
    public: template< xuptr T_INDEX > inline          
    t_entry& i( void ) noexcept     
    {
        static_assert( T_INDEX < t_count, "Out of range" );      // this should be possible if getcount evaluates to constexpr
        return m_pMem[ T_INDEX ]; 
    }

    //------------------------------------------------------------------------------
    public: template< xuptr T_INDEX > constexpr       
    const t_entry&  i ( void ) const  noexcept     
    {
        static_assert( T_INDEX < static_cast<decltype(T_INDEX)>(t_count), "Out of range" );      // this should be possible if getcount evaluates to constexpr
        return m_pMem[ T_INDEX ]; 
    }

protected:

    struct t_entry_raw
    {
        alignas( t_entry ) xbyte m_Data[sizeof(t_entry)];

                    operator t_entry& ( void )          { return *reinterpret_cast<t_entry*>(this);         }
        constexpr   operator t_entry& ( void ) const    { return *reinterpret_cast<const t_entry*>(this);   }
    };

protected:

    t_entry_raw m_pMem[T_COUNT] {};
};

//------------------------------------------------------------------------------
// Description:
//          A replacement for the standard std::array. It plays nice with
//          the rest of the system sharing common code and functionality. 
//------------------------------------------------------------------------------
template< typename T_ENTRY, xuptr T_COUNT, typename T_COUNTER = xuptr >
class xarray 
{
    x_object_type( xarray, is_linear );

public:
    
    using                       t_entry   = T_ENTRY;
    using                       t_counter = T_COUNTER;
    using                       t_view    = xbuffer_view< t_entry, t_counter >;
    x_constexprvar  t_counter   t_count   = static_cast<t_counter>(T_COUNT);
    
    #define x_linear_constructs_buffer_hardness
    #include "Implementation/x_linear_constructs_hardness.h"

public:
   
    template< class T = t_counter >  
    constexpr T         getCount    ( void )            const   noexcept  
    { 
        return x_static_cast<T>(T_COUNT); 
    }

    //------------------------------------------------------------------------------
    public: template< xuptr T_INDEX > inline          
    t_entry& i( void ) noexcept     
    {
        static_assert( T_INDEX < t_count, "Out of range" );      // this should be possible if getcount evaluates to constexpr
        return m_pMem[ T_INDEX ]; 
    }

    //------------------------------------------------------------------------------
    public: template< xuptr T_INDEX > constexpr       
    const t_entry&  i ( void ) const  noexcept     
    {
        static_assert( T_INDEX < static_cast<decltype(T_INDEX)>(t_count), "Out of range" );      // this should be possible if getcount evaluates to constexpr
        return m_pMem[ T_INDEX ]; 
    }

public:

    t_entry     m_pMem[ t_count ];
};





/*
//------------------------------------------------------------------------------
// Description:
//      Simple linear queue
//------------------------------------------------------------------------------
template< typename T >
class queue2
{
public:
    
    void push( T& Node )
    {
        Node.m_pNext = nullptr;
        std::lock_guard<std::mutex> Lock( m_Mutex );
        
        if( m_pHead == nullptr ) m_pHead            = &Node;
        else                     m_pLast->m_pNext   = &Node;
        
        m_pLast = &Node;
    }
    
    T* pop( void )
    {
        std::lock_guard<std::mutex> Lock( m_Mutex );
        if( m_pHead == nullptr ) return nullptr;
        T* pNode = m_pHead;
        m_pHead  = (T*)m_pHead->m_pNext;
        return pNode;
    }
    
    T* steal( void ) { return pop(); }
    
    void Clear( void )
    {
        std::lock_guard<std::mutex> Lock( m_Mutex );
        m_pHead = nullptr;
        m_pLast = nullptr;
    }
    
protected:
    
    std::mutex      m_Mutex;
    T*              m_pHead = nullptr;
    T*              m_pLast = nullptr;
};
//------------------------------------------------------------------------------
// Description:
//      A lock memory pool
//      Simple linear memory stack
//------------------------------------------------------------------------------
template< typename T, int T_MAX_NODES >
class l_fixed_pool_static
{
public:
    
    l_fixed_pool_static( void )
    {
        for( int i = 0; i < T_MAX_NODES - 1; i++ )
        {
            m_Data[i].m_pNext = &m_Data[i + 1];
        }
        
        m_Data[T_MAX_NODES - 1].m_pNext = nullptr;
        m_pHead = &m_Data[0];
    }
    
    void Free( T& Node )
    {
        std::lock_guard<std::mutex> Lock( m_Mutex );
        Node.m_pNext = m_pHead;
        m_pHead = &Node;
    }
    
    T* Alloc( void )
    {
        std::lock_guard<std::mutex> Lock( m_Mutex );
        if( m_pHead == nullptr ) return nullptr;
        T* pNode = m_pHead;
        m_pHead = (T*)m_pHead->m_pNext;
        return pNode;
    }
    
    T& get( int i ) { assert( i >= 0 && i < T_MAX_NODES ); return m_Data[i]; }
    
protected:
    
    std::mutex      m_Mutex;
    T*              m_pHead;
    T               m_Data[T_MAX_NODES];
};
*/

/*
//------------------------------------------------------------------------------
// Description:
//  Simple linear memory stack
//------------------------------------------------------------------------------
template< typename T, int T_MAX_NODES >
class memory_stack_raw
{
public:
    
    ~memory_stack_raw( void )
    {
        int Count = 0;
        for( node* pNode = m_pHead; pNode; pNode = pNode->m_pNext )
        {
            Count++;
        }
        assert( Count == T_MAX_NODES );
        x_free( m_pData );
    }
    
    memory_stack_raw( void )
    {
        static_assert( sizeof( T ) >= sizeof( node* ),"" );
        m_pData = (alignof(T) < alignof(node*)) ? x_malloc( node*, T_MAX_NODES ) : x_malloc( T, T_MAX_NODES );
        
        node* pData;
        for( int i = 0; i < T_MAX_NODES - 1; i++ )
        {
            pData           = (node*)&m_pData[ sizeof( T ) * i];
            pData->m_pNext  = (node*)&m_pData[ sizeof( T ) * (1+i)];
        }
        
        pData           = (node*)&m_pData[ sizeof( T ) * (T_MAX_NODES-1)];
        pData->m_pNext  = nullptr;
        
        m_pHead = (node*)m_pData;
    }
    
    void Free( T& Node )
    {
        node* pNode     = (node*)&Node;
        std::lock_guard<decltype(m_Mutex)> Lock( m_Mutex );
        pNode->m_pNext  = m_pHead;
        m_pHead         = pNode;
    }
    
    T* Alloc( void )
    {
        if( m_pHead == nullptr ) return nullptr;
        std::lock_guard<decltype(m_Mutex)> Lock( m_Mutex );
        node* pNode = m_pHead;
        m_pHead     = pNode->m_pNext;
        return (T*)pNode;
    }
    
protected:
    
    struct node
    {
        node*  m_pNext;
    };
    
protected:
    
    x_spinlock      m_Mutex;
    node*           m_pHead;
    xbyte*           m_pData;
};
*/


