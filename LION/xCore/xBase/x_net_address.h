//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//          This function converts from and to network endian. Network endian is
//          defined as big endian. So this macro will convert TO and FROM Big endian.
//------------------------------------------------------------------------------
#ifdef _X_TARGET_LITTLE_ENDIAN
    #define x_NetworkEndian(X)      x_EndianSwap( X )
#else
    #define x_NetworkEndian(X)      (X)
#endif

/////////////////////////////////////////////////////////////////////////////////
// net address contains the address of a connection IP and the port number.
/////////////////////////////////////////////////////////////////////////////////
class xnet_address
{                                                                   
    x_object_type( xnet_address, is_linear )

public:
    
    enum class type : u8
    {
        IP4,
        IP6
    };

    struct ip4
    {
        constexpr       ip4             ( void )                            noexcept = default;
        constexpr       ip4             ( u32 x )                           noexcept : m_Value{ x_NetworkEndian(x) }{}
        constexpr       ip4             ( u8 a, u8 b, u8 c, u8 d )          noexcept : m_Value{ u32{a}|(u32{b}<<8)|(u32{c}<<16)|(u32{d}<<24) } {}
        constexpr u32   get             ( void )                    const   noexcept { return x_NetworkEndian(m_Value); }
        constexpr bool  operator ==     ( const ip4& B )            const   noexcept { return m_Value == B.m_Value; }
        u32 m_Value {};
    };

    struct ip6
    { 
        constexpr       ip6             ( void )                        noexcept = default;
        constexpr       ip6             ( u16 a, u16 b, u16 c, u16 d, 
                                          u16 e, u16 f, u16 g, u16 h )  noexcept : m_Value{{ x_NetworkEndian( a ), x_NetworkEndian( b ),
                                                                                             x_NetworkEndian( c ), x_NetworkEndian( d ),
                                                                                             x_NetworkEndian( e ), x_NetworkEndian( f ),
                                                                                             x_NetworkEndian( g ), x_NetworkEndian( h ) }} {}
        constexpr       ip6             ( const xarray<u16,8>& Array )  noexcept : m_Value{{ x_NetworkEndian( Array[0] ), x_NetworkEndian( Array[1] ),
                                                                                             x_NetworkEndian( Array[2] ), x_NetworkEndian( Array[3] ),
                                                                                             x_NetworkEndian( Array[4] ), x_NetworkEndian( Array[5] ),
                                                                                             x_NetworkEndian( Array[6] ), x_NetworkEndian( Array[7] ) }} {}
        constexpr xarray<u16,8>   get   ( void )                    const   noexcept { return {{ x_NetworkEndian( m_Value[0] ), x_NetworkEndian( m_Value[1] ),
                                                                                                 x_NetworkEndian( m_Value[2] ), x_NetworkEndian( m_Value[3] ),
                                                                                                 x_NetworkEndian( m_Value[4] ), x_NetworkEndian( m_Value[5] ),
                                                                                                 x_NetworkEndian( m_Value[6] ), x_NetworkEndian( m_Value[7] ) }};
                                                                                     }
        xarray<u16,8> m_Value {}; 
    };

    struct port
    {
        constexpr       port            ( void )                        noexcept = default;
        constexpr       port            ( u16 x )                       noexcept : m_Value{ x_NetworkEndian(x) }{}
        constexpr u16   get             ( void )                const   noexcept { return x_NetworkEndian(m_Value); }
        constexpr bool  operator ==     ( const port& B )       const   noexcept { return m_Value == B.m_Value; }
        u16 m_Value {};
    };

public:

        constexpr                       xnet_address        ( void )                                    noexcept  {}
        constexpr                       xnet_address        ( ip4 IP4, port Port )                      noexcept : m_IP4{ IP4 }, m_Port{Port}, m_Type{type::IP4} {}
        constexpr                       xnet_address        ( const ip6& IP6, port Port )               noexcept : m_IP6{ IP6 }, m_Port{Port}, m_Type{type::IP6} {}
        x_inline                        xnet_address        ( const char* pAddrStr )                    noexcept { setStrAddress ( pAddrStr );   }
        x_inline        void            Clear               ( void )                                    noexcept;
        x_inline        bool            isValid             ( void )                            const   noexcept;

        x_inline        void            setup               ( ip4 IP4, port Port )                      noexcept; 
        x_inline        void            setup               ( const ip6& IP6, port Port )               noexcept; 
        x_inline        void            setup               ( const xarray<u16,8>& IP6, port Port )     noexcept; 
        x_inline        void            setIP               ( ip4 IP )                                  noexcept;
        x_inline        void            setIP               ( ip6 IP )                                  noexcept;
        x_inline        void            setPort             ( port Port )                               noexcept;
        x_inline        const char*     setStrIP            ( const char* pIPStr   )                    noexcept;
        x_inline        const char*     setStrAddress       ( const char* pAddrStr )                    noexcept;

        constexpr       const ip4       getIP4              ( void )                            const   noexcept { return x_assert(m_Type ==  type::IP4), m_IP4; } 
        constexpr       const ip6&      getIP6              ( void )                            const   noexcept { return x_assert(m_Type ==  type::IP6), m_IP6; }
        constexpr       port            getPort             ( void )                            const   noexcept { return m_Port;                                }
        x_inline        xstring         getStrIP            ( void )                            const   noexcept;
        x_inline        xstring         getStrAddress       ( void )                            const   noexcept;

        constexpr       bool            operator ==         ( const xnet_address& A )           const   noexcept;
        constexpr       bool            operator !=         ( const xnet_address& A )           const   noexcept;

private:    

        union
        {
            ip4             m_IP4       { 0 };
            ip6             m_IP6;
        };
        port                m_Port      { 0 };
        type                m_Type      { type::IP4 };
};
