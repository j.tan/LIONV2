//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


#ifndef _X_BASE
#define _X_BASE

//
// Good reference online compilers:
//  http://rextester.com/l/cpp_online_compiler_clang
//  http://gcc.godbolt.org/
//  http://webcompiler.cloudapp.net/
//

//
// Deal with different targets
//
#include <cassert>
#include "x_target.h"

#if _X_TARGET_WINDOWS
    #include <SDKDDKVer.h>
    #ifndef WIN32_LEAN_AND_MEAN
        #define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
    #endif
#endif


//
// Standard C++ require includes
//
#include <memory>
#include <vector>
#include <thread>
#include <mutex>
#include <iostream>
#include <condition_variable>
#include <iostream>
#include <sstream>
#include <atomic>
#include <stdlib.h>
#include <chrono>
#include <limits>
#include <set>
#include <utility>
#include <stdarg.h>
#include <cmath>

#ifdef max
    #undef max
#endif

//
// key definitions files from xbase
//
#include "x_macros.h"
#include "x_types.h"
#include "x_units.h"
#include "x_time.h"
#include "x_reporting.h"
#include "x_meta_prog.h"
#include "x_memory_ops.h"
#include "x_debug.h"
#include "x_errors.h"
#include "x_global_context.h"
#include "x_memory_newdelete.h"
#include "x_plus.h"
#include "x_math.h"
#include "x_linear_constructs.h"
#include "x_arg_list.h"
#include "x_lock_constructs.h"
#include "x_linear_containers.h"
#include "x_lockless_constructs.h"
#include "x_string.h"
#include "x_scheduler_jobs.h"
#include "x_scheduler.h"
#include "x_guid.h"
#include "x_color.h"
#include "x_bitmap.h"
#include "x_linear_messages.h"
#include "x_file.h"
#include "x_file_textfile.h"
#include "x_property.h"
#include "x_property_data.h"
#include "x_random.h"
#include "x_net_address.h"
#include "x_net_socket.h"
#include "x_net_udp_transport.h"
#include "x_net_tcp_transport.h"
#include "x_bitstream.h"
#include "x_file_serialfile.h"
#include "x_cmdline.h"

//
// Includes all the inline functions from xbase
//
#include "Implementation/x_errors_inline.h"
#include "Implementation/x_string_inline.h"
#include "Implementation/x_arg_list_inline.h"
#include "Implementation/x_memory_ops_inline.h"
#include "Implementation/x_plus_inline.h"
#include "Implementation/x_lock_constructs_inline.h"
#include "Implementation/x_lockless_constructs_inline.h"
#include "Implementation/x_math_general_inline.h"
#include "Implementation/x_math_r3_inline.h"
#include "Implementation/x_math_v2_inline.h"
#include "Implementation/x_math_v3d_inline.h"
#include "Implementation/x_math_v3_inline.h"
#include "Implementation/x_math_v4_inline.h"
#include "Implementation/x_math_m4_inline.h"
#include "Implementation/x_math_q_inline.h"
#include "Implementation/x_math_p_inline.h"
#include "Implementation/x_math_bbox_inline.h"
#include "Implementation/x_math_sph_inline.h"
#include "Implementation/x_math_irect_inline.h"
#include "Implementation/x_math_rect_inline.h"
#include "Implementation/x_color_inline.h"

#include "Implementation/x_bitmap_inline.h"
#include "Implementation/x_linear_containers_inline.h"

#include "Implementation/x_scheduler_jobs_inline.h"
#include "Implementation/x_property_inline.h"
#include "Implementation/x_file_inline.h"
#include "Implementation/x_net_address_inline.h"
#include "Implementation/x_random_inline.h"
#include "Implementation/x_bitstream_inline.h"
#include "Implementation/x_file_textfile_inline.h"
#include "Implementation/x_file_serialfile_inline.h"
#include "Implementation/x_cmfline_inline.h"


#endif

