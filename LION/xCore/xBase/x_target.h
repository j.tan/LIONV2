//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#ifndef _X_TARGET_H
#define _X_TARGET_H

// Apple defines these, so undefine them 
#ifdef LITTLE_ENDIAN
    #undef LITTLE_ENDIAN
    #undef BIG_ENDIAN
#endif

//------------------------------------------------------------------------------
// Description:
//      Basic information about the platform for this program
//------------------------------------------------------------------------------
namespace x_target
{
    // _X_TARGET_DYNAMIC_LIB tells if we are compiling a dynamic linking library

    // X_COMPILER - will be assigned at compile time one of the following
    enum compiler
    {
        VS_2013,        // for #ifs use: _X_COMPILER_VS_2013        
        VS_2015,        // for #ifs use: _X_COMPILER_VS_2015
        CLANG,          // for #ifs use: _X_COMPILER_CLANG even if it has LLVM backend
        GCC             // for #ifs use: _X_COMPILER_GCC
    };

    // X_TARGET - will be assigned at compile time one of the following
    enum platform
    {
        WINDOWS,        // for #ifs use: _X_TARGET_WINDOWS
        MAC,            // for #ifs use: _X_TARGET_MAC
        IOS,            // for #ifs use: _X_TARGET_IOS
        LINUX,          // for #ifs use: _X_TARGET_LINUS
        ANDROID,        // for #ifs use: _X_TARGET_ANDROID
        PLATFORM_COUNT
    };
    
    // X_TARGET_CHIPSET - will be assigned at compile time one of the following
    enum chipset
    {
        INTEL,          // for #ifs use: _X_TARGET_INTEL
        ARM             // for #ifs use: _X_TARGET_ARM
    };
    
    // X_TARGET_ENDIAN - will be assigned at compile time one of the following
    enum endian
    {
        LITTLE_ENDIAN,  // for #ifs use: _X_TARGET_LITTLE_ENDIAN
        BIG_ENDIAN      // for #ifs use: _X_TARGET_BIG_ENDIAN
    };
    
    // X_TARGET_BITS - will be assigned at compile time one of the following
    enum bits
    {
        BITS_64 = 64,   // for #ifs use: _X_TARGET_64BITS
        BITS_32 = 32,   // for #ifs use: _X_TARGET_32BITS
    };
    
    // Constant expressions functions to find out detail of the about the target been build
    constexpr unsigned      getCacheLineSize    ( void ) { return 64; }
    constexpr platform      getPlatform         ( void );
    constexpr chipset       getChipset          ( void );
    constexpr endian        getEndian           ( void );
    constexpr bits          getBits             ( void );
    inline    const char*   getPlatformString   ( const platform Platform )
    {
        constexpr static const char* const pStrings[]
        { 
            "WINDOWS", 
            "MAC", 
            "IOS", 
            "LINUX", 
            "ANDROID" 
        };
        return pStrings[Platform];
    }
};

//------------------------------------------------------------------------------
// Description:
//      Determine platforms
//------------------------------------------------------------------------------
#ifdef _WIN32
    #define _X_TARGET_WINDOWS           true
    #define _X_TARGET_INTEL             true
    #define _X_TARGET_LITTLE_ENDIAN     true
    #define _X_SSE2_SUPPORT             true

    //
    // Must identify the compiler that we are using
    //
    #if defined(_MSC_VER) && !defined(__clang__)
        #define _X_COMPILER_VS_2015     true
    #elif defined __clang__
        #define _X_COMPILER_CLANG       true
    #else
        #error "Sorry I dont know what compiler you are using"
    #endif

    //
    // Deal with static vs dynamic libraries
    //
    #if defined(_USRDLL) 
        #define _X_TARGET_DYNAMIC_LIB        true
    #endif

    namespace x_target
    {
        constexpr platform      getPlatform      ( void ) { return x_target::WINDOWS;        }
        constexpr chipset       getChipset       ( void ) { return x_target::INTEL;          }
        constexpr endian        getEndian        ( void ) { return x_target::LITTLE_ENDIAN;  }
        constexpr compiler      getCompiler      ( void ) { return x_target::VS_2015;        }
    };

    #if 0 //ndef _X_STATIC_LIB
        #if _X_TARGET_DYNAMIC_LIB
            #define x_import __declspec(dllexport)
        #else
            #define x_import __declspec(dllimport)
        #endif
    #else
        #define x_import 
    #endif

#elif __APPLE__
    #include "TargetConditionals.h"

    #define _X_COMPILER_CLANG               true

    #if defined( DLL )
        #define _X_TARGET_DYNAMIC_LIB        true
    #endif

    #if TARGET_IPHONE_SIMULATOR
        #define _X_TARGET_IOS             true
        #define _X_TARGET_ARM             true
        #define _X_TARGET_LITTLE_ENDIAN   true
        namespace x_target
        {
            constexpr platform getPlatform      ( void ) { return x_target::IOS;            }
            constexpr chipset  getChipset       ( void ) { return x_target::ARM;            }
            constexpr endian   getEndian        ( void ) { return x_target::LITTLE_ENDIAN;  }
            constexpr compiler getCompiler      ( void ) { return x_target::CLANG;          }
        };
    #elif TARGET_OS_IPHONE
        #define _X_TARGET_IOS             true
        #define _X_TARGET_ARM             true
        #define _X_TARGET_LITTLE_ENDIAN   true
        namespace x_target
        {
            constexpr platform getPlatform      ( void ) { return x_target::IOS;            }
            constexpr chipset  getChipset       ( void ) { return x_target::ARM;            }
            constexpr endian   getEndian        ( void ) { return x_target::LITTLE_ENDIAN;  }
            constexpr compiler getCompiler      ( void ) { return x_target::CLANG;          }
        };
    #elif TARGET_OS_MAC
        #define _X_TARGET_MAC               true
        #define _X_TARGET_INTEL             true
        #define _X_TARGET_LITTLE_ENDIAN     true
        #define _X_SSE2_SUPPORT             true

        namespace x_target
        {
            constexpr platform getPlatform      ( void ) { return x_target::MAC;            }
            constexpr chipset  getChipset       ( void ) { return x_target::INTEL;          }
            constexpr endian   getEndian        ( void ) { return x_target::LITTLE_ENDIAN;  }
            constexpr compiler getCompiler      ( void ) { return x_target::CLANG;          }
        };

    #else
        #error "Unknown Apple platform"
    #endif
#elif defined __linux__ || defined __unix__
    #define _X_TARGET_LINUX             true
    #define _X_TARGET_INTEL             true
    #define _X_TARGET_LITTLE_ENDIAN     true
    #define _X_COMPILER_GCC             true
    namespace x_target
    {
        constexpr platform getPlatform      ( void ) { return x_target::LINUX;          }
        constexpr chipset  getChipset       ( void ) { return x_target::INTEL;          }
        constexpr endian   getEndian        ( void ) { return x_target::LITTLE_ENDIAN;  }
        constexpr compiler getCompiler      ( void ) { return x_target::GCC;            }
    };
#elifdef _POSIX_VERSION
    #error "Unknown platform -- error:3"
#else
    #error "Unknown platform -- error:4"
#endif

//------------------------------------------------------------------------------
// Description:
//      Find out if we are compiling for 32/64bits 
//------------------------------------------------------------------------------
#if defined(_WIN64) || defined(__x86_64__)
    #define _X_TARGET_64BITS          true
    namespace x_target
    {
        constexpr bits getBits      ( void ) { return x_target::BITS_64; }
    }
#else
    #define _X_TARGET_32BITS          true
    namespace x_target
    {
        constexpr bits getBits      ( void ) { return x_target::BITS_32; }
    }
#endif

//------------------------------------------------------------------------------
// Description:
//      Determine configuration (debug/release)
//------------------------------------------------------------------------------
#if defined(_DEBUG) || defined(DEBUG)
    #define _X_DEBUG                    true
#else
    #define _X_RELEASE                  true
#endif

//------------------------------------------------------------------------------
// Description:
//      x_assume -- very similar to assert but it gives hints to the compiler for optimizations.
//                  This means that whatever is assume MUST never happen.
//                  Also a release time all asociated variables must be valid.
//      x_assert -- This is something that should never happen at run time. However we do not
//                  give the optimizer any hints. So if it happens at run time it wont be so dramatic.
//                  A release the expression will be removed so asociated variables can not exits.
//                  Please do not miss used asserts. An assert is not an error it is a crash.
//------------------------------------------------------------------------------
#if _X_RELEASE
    #ifdef assert
        #undef assert
    #endif

    #define x_assert(A,...) ((void)0)

    #if _X_COMPILER_VS_2015
        #define x_assume(A,...) ((void)__assume( A ))
    #else
        #define x_assume(A,...) ((void)(0))
    #endif

    #define x_verify(A,...) ((void)(A))

#else

    #if _X_COMPILER_VS_2015
        #define x_assert(A, ... ) assert(A)
        #define x_assume(A, ... ) assert(A), __assume( A )

//#pragma inline_recursion( on )
//#pragma inline_depth( 255 )

    #else
        constexpr bool x_assert_helper( bool a ) { assert(a); return a; }
        #define x_assume(A,...) (__builtin_expect(!(A), 0) ? x_assert_helper(!!(A)) : 0 )
        #define x_assert(A,...) (__builtin_expect(!(A), 0) ? x_assert_helper(!!(A)) : 0 )
    #endif

    #define x_verify( A, ... ) x_assert(A, __VA_ARGS__ )

#endif

//------------------------------------------------------------------------------
// Description:
//      Macro used to only execute the instruction if it is in debug mode
//------------------------------------------------------------------------------
#if _X_DEBUG
    #define CMD_ASSUME( ... )    __VA_ARGS__
    #define CMD_DEBUG( ... )     __VA_ARGS__
    #define CMD_RELEASE( ... )
#else
    #define CMD_ASSUME( ... )    __VA_ARGS__
    #define CMD_DEBUG( ... )
    #define CMD_RELEASE( ... )   __VA_ARGS__
#endif


//------------------------------------------------------------------------------
// Description:
//      inline options:
//          x_inline        - Gives the compiler a hint that it should inline. However in debug mode it forces not to inline.
//          x_noinline      - it wont inline a function
//          x_foceinline    - will force inline any function. Make sure to enable this in your compiler.
//          x_orinlineconst - inline constexpr will make the function inline in debug, and inline constexpr in release
//          x_orforceconst  - forceinline constexpr will make the function forceinline in debug, and forceinline constexpr in release
//          x_forceconst    - will make the function forceinline constexpr 
//          x_inlineconst   - This is more for symmetry sake. It marks a function inline constexpr 
//          x_incppfile     - tells the user that the function is inside the cpp there for wont be inline
//------------------------------------------------------------------------------
#define x_constexpr constexpr 
#if _X_COMPILER_VS_2015
    #define x_noinline          __declspec(noinline)
    #define x_forceinline       __forceinline
    
    #if _X_RELEASE
        #pragma inline_recursion( on )
        #pragma inline_depth( 255 )
    #endif

#elif _X_COMPILER_CLANG
    #define x_noinline          __attribute__((__noinline__))
    #define x_forceinline       __attribute__((always_inline))
#else
    #define x_noinline          __attribute__((noinline))
    #define x_forceinline       __attribute__((forceinline))
#endif

// Change behavior between debug vs release
#if _X_RELEASE
    #define x_inline            inline
    #define x_orinlineconst     x_inline        constexpr
    #define x_orforceconst      x_forceinline   constexpr
#else 
    // When we are in debug mode we do not inline
    #define x_inline            inline // x_noinline
    #define x_orinlineconst     x_inline
    #define x_orforceconst      x_inline
#endif

#define x_forceconst            x_forceinline   constexpr
#define x_inlineconst           x_inline        constexpr
#define x_incppfile       

//------------------------------------------------------------------------------
// Description:
//      shortcut macro for constexpr variables
//------------------------------------------------------------------------------
#define x_constexprvar          constexpr static const 

//------------------------------------------------------------------------------
// Description:
//      Compilers micro optimizations hints for conditional branching 
// Algorithm:
//      http://stackoverflow.com/questions/1440570/likely-unlikely-equivalent-for-msvc
//------------------------------------------------------------------------------
#if _X_COMPILER_GCC

    #define xif_likely(expr)    if( __builtin_expect(!(expr), 0 ) )
    #define xif_unlikely(expr)  if( __builtin_expect((expr),  0 ) )

#else

    #define xif_unlikely(expr)   if (!(expr)); else 
    #define xif_likely(expr)     if (expr)

#endif

//------------------------------------------------------------------------------
// Description:
//      Disable warnings 
//------------------------------------------------------------------------------

#ifdef _X_COMPILER_VS_2015
    #pragma warning(disable:4290) // warning C4290: C++ exception specification ignored except to indicate a function is not __declspec(nothrow)
#endif 


//------------------------------------------------------------------------------
// Description:
//      Additional includes for each platform
//------------------------------------------------------------------------------
#if _X_TARGET_MAC
    // SSE support
    #include "emmintrin.h"
#endif

//------------------------------------------------------------------------------
// Description:
//     The macro BREAK will cause a debugger breakpoint if possible on any given 
//     platform.  If a breakpoint cannot be caused, then a divide by zero will be 
//     forced.  Note that the BREAK macro is highly platform specific.  The 
//     implementation of BREAK on some platforms prevents it from being used 
//     syntactically as an expression.  It can only be used as a statement.
//------------------------------------------------------------------------------
#if _X_COMPILER_VS_2015
    #define X_BREAK      { __debugbreak(); }
#elif _X_COMPILER_CLANG
    #if _X_TARGET_IOS
        // http://iphone.m20.nl/wp/2010/10/xcode-iphone-debugger-halt-assertions/
        #define X_BREAK { __builtin_trap(); }
    #else
        #define X_BREAK abort();//__asm__("int $3")
    #endif 
#else  
    // Generic BREAK to be used if no proper version can be created.
    extern volatile s32 g_xDDBZ;   // Debug Divide By Zero
    #define X_BREAK      {g_xDDBZ=0;g_xDDBZ=1/g_xDDBZ;}
#endif

//------------------------------------------------------------------------------
// Description:
//     Sets a NOP operation in the code assembly. This is commonly used for 
//     debugging. By adding nops allows to see the assembly clearly in code.
//------------------------------------------------------------------------------
#if _X_COMPILER_VS_2015
    #if _X_TARGET_64BITS
        #define X_NOP        __nop() 
    #else
        #define X_NOP        { __asm nop   } //{ __emit(0x00000000); }
    #endif
#else 
    #define X_NOP        { __asm nop   }
#endif

/////////////////////////////////////////////////////////////////////////////////
// DONE
/////////////////////////////////////////////////////////////////////////////////
#endif
