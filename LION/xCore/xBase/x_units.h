//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      allows to construct unit types easily
//------------------------------------------------------------------------------
// Proposed function to add:
//    x_forceconst static type_name Zero     ( void )                      noexcept { return type_name{ static_cast<atomic_type>(0) }; }     
//    x_forceinline  type_name&  setZero     ( void )                      noexcept { m_Value = static_cast<atomic_type>(0); return *this; } 


#define x_units( atomic_type, type_name, ... )                                                                                    \
class type_name                                                                                                                   \
{                                                                                                                                 \
    x_object_type( type_name, is_linear )                                                                                         \
    public:                                                                                                                       \
    atomic_type     m_Value {};                                                                                                   \
    __VA_ARGS__                                                                                                                   \
    x_forceconst               type_name   ( void )                      noexcept  = default;                                     \
    x_forceconst   explicit    type_name   ( atomic_type Value )         noexcept : m_Value    { Value } {}                       \
    x_forceconst   bool        operator == ( const type_name X ) const   noexcept { return m_Value == X.m_Value;              }   \
    x_forceconst   bool        operator >= ( const type_name X ) const   noexcept { return m_Value >= X.m_Value;              }   \
    x_forceconst   bool        operator <= ( const type_name X ) const   noexcept { return m_Value <= X.m_Value;              }   \
    x_forceconst   bool        operator >  ( const type_name X ) const   noexcept { return m_Value >  X.m_Value;              }   \
    x_forceconst   bool        operator <  ( const type_name X ) const   noexcept { return m_Value <  X.m_Value;              }   \
    x_forceinline  type_name   operator ++ ( int )                       noexcept { auto temp{*this}; m_Value+=1; return temp;}   \
    x_forceinline  type_name&  operator ++ ( void )                      noexcept { m_Value+=1; return *this;                 }   \
    x_forceinline  type_name   operator -- ( int )                       noexcept { auto temp{*this}; m_Value-=1; return temp;}   \
    x_forceinline  type_name&  operator -- ( void )                      noexcept { m_Value-=1; return *this;                 }   \
    x_forceinline  type_name&  operator += ( const type_name X )         noexcept { m_Value += X.m_Value; return *this;       }   \
    x_forceinline  type_name&  operator -= ( const type_name X )         noexcept { m_Value -= X.m_Value; return *this;       }   \
    x_forceinline  type_name&  operator *= ( const type_name X )         noexcept { m_Value *= X.m_Value; return *this;       }   \
    x_forceinline  type_name&  operator /= ( const type_name X )         noexcept { m_Value /= X.m_Value; return *this;       }   \
    x_forceconst   typename std::enable_if< std::is_signed<atomic_type>::value, type_name >::type                                 \
                               operator -  ( void              ) const   noexcept { return type_name{ -m_Value };             }   \
    x_forceconst   type_name   operator +  ( const type_name X ) const   noexcept { return type_name{ m_Value + X.m_Value };  }   \
    x_forceconst   type_name   operator -  ( const type_name X ) const   noexcept { return type_name{ m_Value - X.m_Value };  }   \
    x_forceconst   type_name   operator *  ( const type_name X ) const   noexcept { return type_name{ m_Value * X.m_Value };  }   \
    x_forceconst   type_name   operator /  ( const type_name X ) const   noexcept { return type_name{ m_Value / X.m_Value };  }   \
    x_forceinline  type_name&  operator =  ( const type_name X )         noexcept { m_Value = X.m_Value; return *this;        }   \
};

//------------------------------------------------------------------------------
// Description:
//      Bytes
//------------------------------------------------------------------------------

x_units( s64, units_bytes, 
        x_forceconst explicit units_bytes  ( int X ) :                    m_Value{ static_cast<s64>(X) }    {} 
        x_forceconst          units_bytes  ( unsigned long long X ) :     m_Value{ static_cast<s64>(X) }    {} 
    );  

 x_forceconst units_bytes operator "" _bytes ( unsigned long long bytes ) noexcept { return units_bytes{ bytes } ; }


//------------------------------------------------------------------------------
// Description:
//      characters, units_chars<8> == char, units_chars<16> == wchar
//------------------------------------------------------------------------------

template< typename T >
x_units( int, units_chars_any,
        enum { t_bytes = sizeof(T) };  
        static_assert( std::is_same<T,xchar>::value || std::is_same<T,xwchar>::value, "We only support xchar, or xchar16 types right now" );
        x_forceconst explicit units_chars_any ( const units_bytes X ) noexcept : m_Value{ static_cast<int>(X.m_Value / t_bytes) }  { x_assert( X.m_Value == t_bytes*m_Value); } 
        x_forceconst operator units_bytes     ( void ) const          noexcept { return units_bytes{ m_Value * t_bytes };  }  
    );  

using units_chars  = units_chars_any<xchar>;
using units_wchars = units_chars_any<xwchar>;


//------------------------------------------------------------------------------
// Description:
//      Units for Angles
//------------------------------------------------------------------------------
x_units( f32, xdegree,
     x_forceconst explicit xdegree( const s32 X )               noexcept : m_Value{ static_cast<f32>(X) }                      {} 
     x_forceconst explicit xdegree( const f64 X )               noexcept : m_Value{ static_cast<f32>(X) }                      {} 
   );

x_units( f32, xradian, 
     x_forceconst explicit xradian     ( const s32 X )          noexcept : m_Value{ static_cast<f32>(X) }                      {} 
     x_forceconst explicit xradian     ( const long double X )  noexcept : m_Value{ static_cast<f32>(X) }                      {} 
     x_forceconst explicit xradian     ( const xdegree X )      noexcept : m_Value{ X.m_Value * 0.017453292519943295769f }     {} 
     x_forceconst xdegree  getDegrees  ( void )          const  noexcept   { return xdegree{ m_Value * 57.29577951308232087685f }; }
    );  

x_units( f64, xradian64 );

x_forceconst xradian   operator "" _deg    ( long double deg )          noexcept { return xradian{ static_cast<f32>(                 deg  * 0.017453292519943295769) }; }
x_forceconst xradian   operator "" _deg    ( unsigned long long deg )   noexcept { return xradian{ static_cast<f32>(static_cast<f64>(deg) * 0.017453292519943295769) }; }


//------------------------------------------------------------------------------
// Description:
//      binary number constants support
// Example:
//      int a = 101101_bin;
//------------------------------------------------------------------------------
namespace _x_units
{
    template< char... T_DIGITS >
    struct conv2bin;

    //------------------------------------------------------------------------------
    template< char T_HIGH, char... T_DIGITS >
    struct conv2bin< T_HIGH, T_DIGITS ...> 
    {
        static_assert( T_HIGH == '0' || T_HIGH == '1', "no binary num!" );
        enum : int
        { 
            value = (T_HIGH - '0') * (1 << sizeof...(T_DIGITS)) + _x_units::conv2bin<T_DIGITS...>::value
        };
    };

    //------------------------------------------------------------------------------
    template< char T_HIGH >
    struct conv2bin< T_HIGH > 
    {
        static_assert( T_HIGH == '0' || T_HIGH == '1', "There is a digit here that is not 0 || 1, so this is not a binary number!");
        enum : int
        { 
            value = (T_HIGH - '0')
        };
    };
};

//------------------------------------------------------------------------------
template< char... T_DIGITS > constexpr
int operator "" _bin() 
{
    return _x_units::conv2bin< T_DIGITS ... >::value;
}
