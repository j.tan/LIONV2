//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//------------------------------------------------------------------------------
// Description:
//      The xfile class is design to be a direct replacement to the fopen. The class
//      supports most features of the fopen standard plus a few more. The Open function
//      has been change significantly in order to accommodate the new options. The access
//      modes are similar BUT not identical to the fopen. Some of the functionality like
//      the append has drastically change from the standard. Here are the access modes.
//
//<TABLE>
//     Access Mode         Description
//     =================  ----------------------------------------------------------------------------------------
//         "r"            Read only                 - the file must exits. Useful when accessing DVDs or other read only media
//         "r+"           Reading and Writing       - the file must exits
//         "w" or "w+"    Reading and Writing       - the file will be created.  
//         "a" or "a+"    Reading and Writing       - the file must exists, and it will do an automatic SeekEnd(0). 
//  
//         "@"            Asynchronous Mode         - Allows you to use async features.
//         "c"            Enable File Compression   - This must be use with 'w'. It will compress at file close.
//  
//         "b"            Binary files Mode         - This is the default so you don't need to put it really. 
//         "t"            Text file Mode            - For writing or Reading text files. If you don't add this assumes you are doing binary files.             
//                                                      This command basically writes an additional character '\\r' whenever it finds a '\\n' and       
//                                                      when reading it removes it when it finds '\\r\\n' 
//</TABLE>
//
//      To illustrate different possible combinations and their meaning we put together a table 
//      to show a few examples.
//<TABLE>
//     Examples of Modes  Description
//     =================  ----------------------------------------------------------------------------------------
//          "r"           Good old fashion read a file. Note that you never need to do "rc" as the file system detects that automatically.
//          "wc"          Means that we want to write a compress file       
//          "wc@"         Means to create a compress file and we are going to access it asynchronous
//          "r+@"         Means that we are going to read and write to an already exiting file asynchronously.
//</TABLE>
//
//     Other key change added was the ability to have more type of devices beyond the standard set.
//     Some of the devices such the net device can be use in conjunction with other devices.
//     The default device is of course the hard-disk in the PC.
//<TABLE>
//     Known devices      Description
//     =================  ----------------------------------------------------------------------------------------
//          ram:          To use ram as a file device
//          dvd:          To use the dvd system of the console
//          net:          To access across the network
//          temp:         To the temporary folder/drive for the machine
//          memcard:      To access memory card file system
//          localhd:      To access local hard-drives such the ones found in the XBOX
//          buffer:       User provided buffer data
//          c: d: e:      ...etc local devices such PC drives
//</TABLE>
//
//     Example of open strings:
//<CODE>
//     Open( X_WSTR("net:\\\\c:\\test.txt"),        "r" );      // Reads a file across the network
//     Open( X_WSTR("ram:\\\\name doesnt matter"),  "w" );      // Creates a ram file which you can read/write
//     Open( X_WSTR("c:\\dumpfile.bin"),            "wc" );     // Creates a compress file in you c: drive
//     Open( X_WSTR("UseDefaultPath.txt"),          "r@" );     // No device specify so it reads the file from the default path
//</CODE>
//
//------------------------------------------------------------------------------
#ifdef MAX_PATH
    #undef MAX_PATH
#endif

class xfile
{
    x_object_type( xfile, is_linear, is_not_copyable, is_not_movable )

public:

    enum general
    {
        XEOF       = -1
    };

    enum class sync_state : int
    { 
        UNKNOWN_ERR    = -2,
        XEOF           = general::XEOF,
        INCOMPLETE     = 0,
        COMPLETED      = 1
    };
    
    enum lengths : int
    {
        MAX_DRIVE = 32,         // How long a drive name can be
        MAX_DIR   = 256,        // How long a path or directory
        MAX_FNAME = 256,        // File name
        MAX_EXT   = 256,        // Extension
        MAX_PATH  = MAX_DRIVE + MAX_DIR + MAX_FNAME + MAX_EXT
    };

    enum errors : u8
    {
        ERR_OK,
        ERR_FAILURE,
        ERR_DEVICE_FAILURE,
        ERR_CREATING_FILE,
        ERR_OPENING_FILE,
    };

    using err = x_err<errors>;

public:
                                            xfile           ( void )                                                            noexcept = default;
                                           ~xfile           ( void )                                                            noexcept { Close(); }
                    err                     Open            ( const xwstring& FileName,  const char* pMode )                    noexcept;             
                    void                    Close           ( void )                                                            noexcept;                                     
    x_inline        void                    ToFile          ( xfile& File )                                                     noexcept;
    x_inline        void                    ToMemory        ( void* pData, int BufferSize )                                     noexcept;
    x_forceinline   sync_state              Synchronize     ( bool bBlock )                                                     noexcept;
    x_forceinline   void                    AsyncAbort      ( void )                                                            noexcept;
    x_forceinline   void                    setForceFlush   ( bool bOnOff )                                                     noexcept; 
    x_forceinline   void                    Flush           ( void )                                                            noexcept;                                     
    x_forceinline   void                    SeekOrigin      ( s64 Offset )                                                      noexcept;             
    x_forceinline   void                    SeekEnd         ( s64 Offset )                                                      noexcept;             
    x_forceinline   void                    SeekCurrent     ( s64 Offset )                                                      noexcept;             
    x_forceinline   s64                     Tell            ( void )                                                            noexcept;
    x_forceinline   bool                    isEOF           ( void )                                                            noexcept;
    x_forceinline   int                     getC            ( void )                                                            noexcept;
    x_inline        err                     putC            ( int C, int Count=1, bool bUpdatePos = true )                      noexcept; 
    x_inline        void                    AlignPutC       ( int C, int Count=0, int Aligment=4, bool bUpdatePos = true )      noexcept; 
    x_forceinline   u64                     getFileLength   ( void )                                                            noexcept;

                    static bool             getFileTime     ( const char* pFileName, u64& FileTime )                            noexcept;

    template<class T> 
    x_inline        err                     ReadString      ( xstring_base<T>& Val )                                            noexcept;
    template<typename T> 
    x_inline        err                     Write           ( const T& Val )                                                    noexcept;

    template<typename T> 
    x_inline        err                     WriteList       ( const T& View )                                                   noexcept;

    template<typename T> 
    x_inline        err                     WriteView       ( const xbuffer_view<T> View )                                      noexcept { return WriteList( View ); }

    template<typename T> 
    x_inline        err                     WriteString     ( const xstring_base<T>& View )                                     noexcept;

    template<class T> 
    x_inline        err                     Read            ( T& Val )                                                          noexcept;

    template<class T>
    x_inline        err                     ReadList        ( T& View )                                                         noexcept;

    template<class T>
    x_inline        err                     ReadView        ( const xbuffer_view<T> View )                                      noexcept { return ReadList( View ); }

    /*
    template<class T>
    x_inline        err                     ReadList        ( xbuffer_view<xbyte> View  )                                        noexcept
                                                             {
                                                                 return ReadList<xbuffer_view<xbyte>>(View);
                                                             }
*/
    
    x_forceinline   bool                    isBinaryMode    ( void )                                                    const   noexcept;
    x_forceinline   bool                    isReadMode      ( void )                                                    const   noexcept;
    x_forceinline   bool                    isWriteMode     ( void )                                                    const   noexcept;

    template<typename... T_ARGS> 
    x_inline        err                     Printf          ( const xchar* pFormatStr, const T_ARGS&... Args )                  noexcept;

protected:

    X_DEFBITS( access_types, u32, 0,
        X_DEFBITS_ARG( bool,  CREATE        ,   1  ),  // If not create file then we are accessing an existing file
        X_DEFBITS_ARG( bool,  READ          ,   1  ),  // Has read permissions or not
        X_DEFBITS_ARG( bool,  WRITE         ,   1  ),  // Has write permissions or not
        X_DEFBITS_ARG( bool,  ASYNC         ,   1  ),  // Async enable?
        X_DEFBITS_ARG( bool,  COMPRESS      ,   1  ),  // Do compress files (been compress)
        X_DEFBITS_ARG( bool,  TEXT          ,   1  ),  // This is a text file. Note that this is handle at the top layer.
        X_DEFBITS_ARG( bool,  FORCE_FLUSH   ,   1  )   // Forces to flush constantly (good for debugging). Note that this is handle at the top layer.
    );

protected:

    void                    Clear           ( void )                                                            noexcept;
    err                     ReadRaw         (       void* pBuffer, int Size, u64 Count )                        noexcept;  
    err                     WriteRaw        ( const void* pBuffer, int Size, u64 Count )                        noexcept;

protected:

    void*                   m_pFile         { nullptr };
    xfile_device_i*         m_pDevice       { nullptr };
    access_types            m_AccessType    {};
    xwstring                m_FileName      {};

    friend bool x_setCompressDevice( xfile& File, bool bCompressAndText );
    friend class xfile_device_i;
};

//------------------------------------------------------------------------------
// Description:
//     This class is the most low level class for the file system. This class deals
//     with the most low level platform specific API to do its job. For most part
//     users will never deal with this class directly. It is intended to be used
//     by more expert users and low level people. This class defines a device to be 
//     access by the xfile class.
//------------------------------------------------------------------------------
class xfile_device_i
{
    x_object_type( xfile_device_i, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    enum seek_mode
    {
        SKM_ORIGIN,
        SKM_CURENT,
        SKM_END
    };

    using access_types = xfile::access_types;
    using sync_state   = xfile::sync_state; 

public:
                                        xfile_device_i  ( void )                                                noexcept = delete;
    x_forceconst                        xfile_device_i  ( const xstring::const_char* pDeviceName )              noexcept : m_DeviceName{ pDeviceName } {}
    virtual                            ~xfile_device_i  ( void )                                                noexcept {}
    virtual         void*               Open            ( const xwstring& FileName, access_types Flags )        noexcept =0;
    virtual         void                Close           ( void* pFile )                                         noexcept =0;
    virtual         bool                Read            ( void* pFile, void* pBuffer, u64 Count )               noexcept =0;
    virtual         void                Write           ( void* pFile, const void* pBuffer, u64 Count )         noexcept =0;
    virtual         void                Seek            ( void* pFile, seek_mode Mode, s64 Pos )                noexcept =0;
    virtual         s64                 Tell            ( void* pFile )                                         noexcept =0;
    virtual         void                Flush           ( void* pFile )                                         noexcept =0;
    virtual         u64                 Length          ( void* pFile )                                         noexcept =0;
    virtual         bool                isEOF           ( void* pFile )                                         noexcept =0;
    virtual         sync_state          Synchronize     ( void* pFile, bool bBlock )                            noexcept =0;
    virtual         void                AsyncAbort      ( void* pFile )                                         noexcept =0;
    virtual         void                Init            ( const void* )                                         noexcept {}
    virtual         void                Kill            ( void )                                                noexcept {}
    x_forceconst    const char*         getName         ( void )                                        const   noexcept { return m_DeviceName; }

public:

    xfile_device_i*     m_pNext         { nullptr };
    const xstring       m_DeviceName    {};
};

//------------------------------------------------------------------------------
// GENERIC IO FUNCTIONS
//------------------------------------------------------------------------------
// These are intended for tools and such and they deal with native file systems.
//------------------------------------------------------------------------------
namespace x_io
{
    using errors    = x_errdef::errors;
    using err       = x_errdef::err; 

    struct dir
    {
    public:

        struct file_info
        {
            xwchar  m_Path[xfile::MAX_PATH];           // Full path of the file
            xwchar  m_FileName[xfile::MAX_FNAME];      // Includes extension
            xwchar* m_pExtension;                      // Extension offset inside the filename
            bool    m_bDir    :1,                      // is this entry a directory?
                    m_bRegular:1;                      // is this a regular file?
        };

    public:
                           ~dir             ( void )                                            noexcept;
                err         openPath        ( xwstring& Path )                                  noexcept;
                err         getNext         ( file_info& Info)                                  noexcept;
                void        close           ( void )                                            noexcept;
        static  bool        doesPathExists  ( const char* pPath )                               noexcept;
        static  err         MakeDir         ( const char* pPath, int LevelsUp = 0 )             noexcept;

    protected:

        xowner<void*> m_pData = nullptr;
    };
    
    struct file_details
    {
        double  m_MSTimeCreated;                // The time in milliseconds of creation
        double  m_MSTimeModified;               // The time in millisecond of the last modification
        u64     m_FileSize;                     // if it is a file how large is it?
        bool    m_bDir;                         // is this a directory?
    };

    bool        doesFileExists  ( const char* pFile )                               noexcept;
    err         FileDetails     ( const char* pPath, file_details& FileDetails )    noexcept;
};

