//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//-------------------------------------------------------------------------------
// Helpful for debugging in Visual studio:
// Compiler switches:
//      /E      : preprocess to stdout (similar to GCC's -E option)
//      /P      : preprocess to file
//      /EP     : preprocess to stdout without #line directives
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
// Description:
//          This is needed because Microsoft compiler has a bug and they refuse to fix it.
//          this should be use whenever you use __VA_ARGS__
//-------------------------------------------------------------------------------
#if _X_COMPILER_VS_2015
    #define X_VARG_EXPAND(x) x
#else
    #define X_VARG_EXPAND(...) __VA_ARGS__
#endif


//-------------------------------------------------------------------------------
// Description:
//          Concatenates two arguments
//-------------------------------------------------------------------------------
#define X_ARG_CONCATENATE( A, B )      __X_ARG_CONCATENATE1( A, B )
#define __X_ARG_CONCATENATE1( A, B )   __X_ARG_CONCATENATE2( A, B )
#define __X_ARG_CONCATENATE2( A, B )  A##B

//------------------------------------------------------------------------------
// Description:
//      Automatically creates a unique name for a variable
//------------------------------------------------------------------------------
#define X_CREATE_UNIQUE_VARNAME(A)      X_ARG_CONCATENATE(A, __LINE__ )

//------------------------------------------------------------------------------
// Description:
//      Convert an argument to a string
//------------------------------------------------------------------------------
#define __X_STRINGIFY( X ) #X
#define X_STRINGIFY( X ) __X_STRINGIFY( X )

//-------------------------------------------------------------------------------
// Description:
//          Find the count of a __VA_ARGS__
//-------------------------------------------------------------------------------
#define __X_VARG_COUNT_N(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, N, ...) N 
#define X_VARG_COUNT(...) X_VARG_EXPAND(__X_VARG_COUNT_N(__VA_ARGS__, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0))
static_assert( X_VARG_COUNT( 1 ) == 1,"" );
static_assert( X_VARG_COUNT( 1,2,3,4,5 ) == 5,"" );

//-------------------------------------------------------------------------------
// Description:
//          Increments a number
//-------------------------------------------------------------------------------
#define __X_ARG_INC0     1
#define __X_ARG_INC1     2
#define __X_ARG_INC2     3
#define __X_ARG_INC3     4
#define __X_ARG_INC4     5
#define __X_ARG_INC5     6 
#define __X_ARG_INC6     7
#define __X_ARG_INC7     8
#define __X_ARG_INC8     9
#define __X_ARG_INC9    10
#define __X_ARG_INC10   11
#define __X_ARG_INC11   12
#define __X_ARG_INC12   13
#define __X_ARG_INC13   14
#define __X_ARG_INC14   15
#define __X_ARG_INC15   16
#define __X_ARG_INC16   17
#define __X_ARG_INC17   18
#define __X_ARG_INC18   19
#define __X_ARG_INC19   20
#define __X_ARG_INC20   21
#define X_ARG_INC( A )  X_ARG_CONCATENATE( __X_ARG_INC, A )

//-------------------------------------------------------------------------------
// Description:
//          Decrements a number
//-------------------------------------------------------------------------------
#define __X_ARG_DEC0     -1
#define __X_ARG_DEC1      0
#define __X_ARG_DEC2      1
#define __X_ARG_DEC3      2
#define __X_ARG_DEC4      3
#define __X_ARG_DEC5      4
#define __X_ARG_DEC6      5
#define __X_ARG_DEC7      6
#define __X_ARG_DEC8      7
#define __X_ARG_DEC9      8
#define __X_ARG_DEC10     9
#define __X_ARG_DEC11    10
#define __X_ARG_DEC12    11
#define __X_ARG_DEC13    12
#define __X_ARG_DEC14    13
#define __X_ARG_DEC15    14
#define __X_ARG_DEC16    15
#define __X_ARG_DEC17    16
#define __X_ARG_DEC18    17
#define __X_ARG_DEC19    18
#define __X_ARG_DEC20    19
#define X_ARG_DEC( A )  X_ARG_CONCATENATE( __X_ARG_DEC, A )


//-------------------------------------------------------------------------------
// Description:
//          Creates a for loop that goes through each of the var_arg parameters using a user provided callback macro
//          X_FOR_EACH_ONE_PARAM( static_params, _USRCALLBACK, ... )
//              static_params - is what will be pass in the  STATIC variable in the callback
//              _USRCALLBACK  - is the actual callback macro
//          The callback will receive the following arguments:
//          #define USER_CALLBACK( N, INC, DEC, STATIC, ARG )
//              N       - Total count of times that the forloop will execute
//              INC     - A variable that will be increasing each time the callback gets executed. Starting from 0
//              DEC     - A variable that will decrement each time the callback is called. Starting with N-1
//              STATIC  - is a variable that gets pass to all the callbacks and that it wont change
//              ARG     - is the actual va_arg from the original caller. 
// Example:
//      #include<iostream>
//      #define PRINT_NUMBERS_CALLBACK( N, INC, DEC, STATIC, ARG ) << INC
//      #define PRINT_NUMBERS(...) std::cout X_FOR_EACH_ONE_PARAM( 0, PRINT_NUMBERS_CALLBACK, __VA_ARGS__ )
//      void main( void )
//      { PRINT_NUMBERS( a, b, c ); }
//      Outputs: 012
//-------------------------------------------------------------------------------
#define __X_FOR_EACH_ONE_PARAM_1(  N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  0, static_params, x ) )
#define __X_FOR_EACH_ONE_PARAM_2(  N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  1, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_1(  N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_3(  N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  2, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_2(  N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_4(  N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  3, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_3(  N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_5(  N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  4, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_4(  N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_6(  N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  5, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_5(  N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_7(  N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  6, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_6(  N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_8(  N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  7, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_7(  N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_9(  N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  8, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_8(  N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_10( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC,  9, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_9(  N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_11( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 10, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_10( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_12( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 11, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_11( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_13( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 12, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_12( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_14( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 13, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_13( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_15( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 14, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_14( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_16( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 15, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_15( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_17( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 16, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_16( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_18( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 17, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_17( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_19( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 18, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_18( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_20( N, INC, static_params, _USRCALLBACK, x, ... ) X_VARG_EXPAND( _USRCALLBACK( N, INC, 19, static_params, x ) ) X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_19( N, X_ARG_INC(INC), static_params, _USRCALLBACK, __VA_ARGS__ ))
#define __X_FOR_EACH_ONE_PARAM_( N, INC, static_params, _USRCALLBACK, x, ... )   X_VARG_EXPAND( X_ARG_CONCATENATE(__X_FOR_EACH_ONE_PARAM_, N)( N, INC, static_params, _USRCALLBACK, x, __VA_ARGS__ ) )
#define X_FOR_EACH_ONE_PARAM( static_params, _USRCALLBACK, ... )                 X_VARG_EXPAND( __X_FOR_EACH_ONE_PARAM_( X_VARG_COUNT( __VA_ARGS__ ), 0, static_params, _USRCALLBACK, __VA_ARGS__ ) )

//------------------------------------------------------------------------------
// Description:
//      Defining a bit fields and flags in a simple way
// Example:
//          X_DEFBITS( pepe, 
//                      u8, 
//                      (1<<OFFSET_F1) | (1<<OFFSET_F2),
//                      X_DEFBITS_ARG( u8,   F1, 1 ),
//                      X_DEFBITS_ARG( s8,   F2, 1 ),
//                      X_DEFBITS_ARG( bool, F3, 1 )
//                      );
//
//          pepe xx;
//          xx.m_F1 = 1;
//          auto t = xx.m_Flags & pepe::MASK_F1;
//------------------------------------------------------------------------------

#define _X_DEFBITS_FLAGS_ARGS( N_PARAMS, INC, DEC, CLASS_NAME, PARAM ) X_ARG_CONCATENATE( _X_DEFBITS_FLAGS_##INC, PARAM )   
#define _X_DEFBITS_VARS_ARGS( N_PARAMS, INC,  DEC, LASS_NAME,  PARAM ) X_ARG_CONCATENATE( _X_DEFBITS_VARS, PARAM )   

#define __X_DEFBITS_PN(P,N,TYPE,NAME,BITS)                                                                                          \
    x_constexprvar  t_type  NBITS_##NAME    = BITS;                                                                                 \
    x_constexprvar  t_type  OFFSET_##NAME   = BIT_COUNT##P;                                                                         \
    x_constexprvar  t_type  MASK_##NAME     = ((1<<NBITS_##NAME)-1)<<BIT_COUNT##P;                                                  \
    x_constexprvar  t_type  BIT_COUNT##N    = BIT_COUNT##P  + NBITS_##NAME;                                                         \
    constexpr t_type        get##NAME    ( void ) const noexcept     { return static_cast<t_type>(m_##NAME)<<OFFSET_##NAME; }       \
    constexpr static t_self NAME##ToFlags( const TYPE var ) noexcept { return { static_cast<t_type>(static_cast<t_type>(var)<<OFFSET_##NAME) }; } 
    

#define _X_DEFBITS_FLAGS_0_X_DEFBITS_ARG(TYPE,NAME,BITS)                                                                            \
    x_constexprvar  t_type      NBITS_##NAME    = BITS;                                                                             \
    x_constexprvar  t_type      OFFSET_##NAME   = 0;                                                                                \
    x_constexprvar  t_type      MASK_##NAME     = ((1<<NBITS_##NAME)-1)<<0;                                                         \
    x_constexprvar  t_type      BIT_COUNT0      = NBITS_##NAME;                                                                     \
    constexpr t_type            get##NAME    ( void ) const noexcept     { return static_cast<t_type>(m_##NAME)<<OFFSET_##NAME; }       \
    constexpr static t_self     NAME##ToFlags( const TYPE var ) noexcept { return { static_cast<t_type>(static_cast<t_type>(var)<<OFFSET_##NAME) }; } 


#define  _X_DEFBITS_FLAGS_1_X_DEFBITS_ARG(TYPE,NAME,BITS)   __X_DEFBITS_PN( 0,     1,  TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_2_X_DEFBITS_ARG(TYPE,NAME,BITS)   __X_DEFBITS_PN( 1,     2,  TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_3_X_DEFBITS_ARG(TYPE,NAME,BITS)   __X_DEFBITS_PN( 2,     3,  TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_4_X_DEFBITS_ARG(TYPE,NAME,BITS)   __X_DEFBITS_PN( 3,     4,  TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_5_X_DEFBITS_ARG(TYPE,NAME,BITS)   __X_DEFBITS_PN( 4,     5,  TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_6_X_DEFBITS_ARG(TYPE,NAME,BITS)   __X_DEFBITS_PN( 5,     6,  TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_7_X_DEFBITS_ARG(TYPE,NAME,BITS)   __X_DEFBITS_PN( 6,     7,  TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_8_X_DEFBITS_ARG(TYPE,NAME,BITS)   __X_DEFBITS_PN( 7,     8,  TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_9_X_DEFBITS_ARG(TYPE,NAME,BITS)   __X_DEFBITS_PN( 8,     9,  TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_10_X_DEFBITS_ARG(TYPE,NAME,BITS)  __X_DEFBITS_PN( 9,     10, TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_11_X_DEFBITS_ARG(TYPE,NAME,BITS)  __X_DEFBITS_PN( 10,    11, TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_12_X_DEFBITS_ARG(TYPE,NAME,BITS)  __X_DEFBITS_PN( 11,    12, TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_13_X_DEFBITS_ARG(TYPE,NAME,BITS)  __X_DEFBITS_PN( 12,    13, TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_14_X_DEFBITS_ARG(TYPE,NAME,BITS)  __X_DEFBITS_PN( 13,    14, TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_15_X_DEFBITS_ARG(TYPE,NAME,BITS)  __X_DEFBITS_PN( 14,    15, TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_16_X_DEFBITS_ARG(TYPE,NAME,BITS)  __X_DEFBITS_PN( 15,    16, TYPE, NAME, BITS )
#define  _X_DEFBITS_FLAGS_17_X_DEFBITS_ARG(TYPE,NAME,BITS)  __X_DEFBITS_PN( 16,    17, TYPE, NAME, BITS )

#define  _X_DEFBITS_VARS_X_DEFBITS_ARG(TYPE,NAME,BITS)   TYPE    m_##NAME : NBITS_##NAME;

#define X_DEFBITS(TYPENAME,ATOMICTYPE,DEFAULTS, ... )                                                                                                    \
    struct TYPENAME {                                                                                                                                    \
        using t_self    = TYPENAME;                                                                                                                      \
        using t_type    = ATOMICTYPE;                                                                                                                    \
        constexpr                       TYPENAME    ( void )                        noexcept = default;                                                  \
        constexpr                       TYPENAME    ( const t_self& )               noexcept = default;                                                  \
        constexpr                       TYPENAME    ( t_type x )                    noexcept : m_Flags{x}{};                                             \
        x_forceconst    TYPENAME        operator |  ( const TYPENAME Var ) const    noexcept { return { static_cast<t_type>(m_Flags | Var.m_Flags) }; }  \
        inline          void            setDefaults ( void )                        noexcept { m_Flags = DEFAULTS; }                                     \
        X_FOR_EACH_ONE_PARAM( 0, _X_DEFBITS_FLAGS_ARGS,  __VA_ARGS__  )                                                                                  \
        union { t_type m_Flags { DEFAULTS }; struct {                                                                                                    \
        X_FOR_EACH_ONE_PARAM( 0, _X_DEFBITS_VARS_ARGS,  __VA_ARGS__  ) }; };                                                                             \
    }; static_assert( sizeof(TYPENAME) == sizeof(ATOMICTYPE), "X_DEFBITS - Fail to have the correct size between the base type and the structure"); 

#define X_DEFBITS_ARG(TYPE,NAME,BITS) _X_DEFBITS_ARG(TYPE,NAME,BITS)

