//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      The official log system for the scheduler
//------------------------------------------------------------------------------
#if _X_LOGGING
    #define X_SHEDULER_LOG(A) X_LOG_CHANNEL( g_context::get().m_Scheduler_LogChannel, A )
#else
    #define X_SHEDULER_LOG(A) {}
#endif

//------------------------------------------------------------------------------
// Description:
//      A basic scheduler, with knowledge of light jobs and regular jobs
//------------------------------------------------------------------------------
class x_scheduler 
{
    x_object_type( x_scheduler, is_quantum_lock_free );

public:

                               ~x_scheduler                 ( void )                                noexcept;
                void            Init                        ( int nWorkers = -1 )                   noexcept;
                void            Shutdown                    ( void )                                noexcept;
                void            AddJobToQuantumWorld        ( x_job_base& Job )                     noexcept;
                x_job_base*     getJob                      ( void )                                noexcept;
                x_job_base*     getLightJob                 ( void )                                noexcept;
                void            ProcessWork                 ( x_job_base* pJob )                    noexcept;
    static      int             getWorkerUID                ( void )                                noexcept;
    static      void            ProcessWhileWait            ( x_trigger_base& Trigger )             noexcept;
                void            MainThreadStartsWorking     ( void )                                noexcept;
    inline      void            MainThreadStopWorking       ( void )                                noexcept { m_bMainThreadShouldWork = false; m_SleepWorkerCV.notify_all(); }
    constexpr   int             getWorkerCount              ( void )                        const   noexcept { return m_nAllocatedWorkers+1; }

protected:

    enum state : u32
    {
        STATE_NO_INITIALIZED,
        STATE_WORKING,
        STATE_EXITING
    };

    struct worker_kit
    {
        constexpr worker_kit( void ) noexcept = default;
        x_ll_spmc_bounded_queue<x_job_base>     m_LightJobQueue {};
        xuptr                                   m_iNextQueue    {~xuptr(0)};
    };

    using priority_jobs_queue_array     = xarray<x_ll_mpmc_bounded_queue<x_job_base>, x_job_base::PRIORITY_COUNT>;
    using affinity_job_queue_array      = xarray<priority_jobs_queue_array, x_job_base::AFFINITY_COUNT>;

protected:

                void            CleanQueues                 ( void )                            noexcept;
                void            MainThreadBecomesWorker     ( void )                            noexcept;
                void            ThreadBecomesWorker         ( void )                            noexcept;

protected:

    xndptr<worker_kit>                      m_WorkerKit             {};
    affinity_job_queue_array                m_JobQueue              {};
    std::mutex                              m_SleepWorkerMutex      {};
    std::condition_variable                 m_SleepWorkerCV         {};
    std::atomic<int>                        m_nWorkersActive        {0};
    xndptr<std::thread>                     m_lWorkers              {};
    int                                     m_nAllocatedWorkers     { 0 };
    state                                   m_State                 { STATE_NO_INITIALIZED };

#if _X_DEBUG
    x_debug_linear_quantum                  m_Debug_LQ              {};
#endif
    x_reporting::stats_metric               m_Stats_nWorkersWorking {};
    x_reporting::stats_metric               m_Stats_nJobs           {};
    bool                                    m_bMainThreadShouldWork { true };

public:

    x_ll_fixed_pool_static_raw<x_scheduler_jobs::job_chain::state_block, 100>    system_JobChainBlockPool   {};
    x_ll_fixed_pool_static_raw<x_scheduler_jobs::job_chain::lambda_job,  100>    system_LambdaJobChainPool  {};

protected:

    friend void WorkersStartWorking( void ) noexcept;
};

