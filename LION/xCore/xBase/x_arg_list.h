//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#define x_va_start(list,prev)   va_start(list,prev)
#define x_va_end(list)          va_end(list)
#define x_va_arg(list,mode)     va_arg(list,mode)

using xva_list = va_list;

//-------------------------------------------------------------------------------------------
// Description:
//      Creation of an argument list with known types
//-------------------------------------------------------------------------------------------
template< typename > class xstring_base;

namespace xarg_list_args
{
    enum class type
    {
        F32,
        F64,
        S8,
        U8,
        S16,
        U16,
        S32,
        U32,
        S64,
        U64,
        VOID_PTR,
        CHAR_PTR,
        XSTRING,
        UNKNOWN,
        ENUM_COUNT
    };

    template< size_t T_SIZE >  struct _choose_long_type;
    template< size_t T_SIZE >  struct _choose_ulong_type;
    template<> struct _choose_ulong_type<4>{ x_constexprvar type value = type::U32; };
    template<> struct _choose_ulong_type<8>{ x_constexprvar type value = type::U64; };
    template<> struct _choose_long_type<4> { x_constexprvar type value = type::S32; };
    template<> struct _choose_long_type<8> { x_constexprvar type value = type::S64; };

    template< typename T >  struct _choose_type                           { x_constexprvar type value = type::UNKNOWN;   };
    template<>              struct _choose_type<u8>                       { x_constexprvar type value = type::U8;        };
    template<>              struct _choose_type<s8>                       { x_constexprvar type value = type::S8;        };
    template<>              struct _choose_type<u16>                      { x_constexprvar type value = type::U16;       };
    template<>              struct _choose_type<s16>                      { x_constexprvar type value = type::S16;       };
    template<>              struct _choose_type<u32>                      { x_constexprvar type value = type::U32;       };
    template<>              struct _choose_type<s32>                      { x_constexprvar type value = type::S32;       };
    template<>              struct _choose_type<unsigned long>            { x_constexprvar type value = _choose_ulong_type<sizeof(long)>::value; };
    template<>              struct _choose_type<long>                     { x_constexprvar type value = _choose_long_type<sizeof(long)>::value;  };
    template<>              struct _choose_type<u64>                      { x_constexprvar type value = type::U64;       };
    template<>              struct _choose_type<s64>                      { x_constexprvar type value = type::S64;       };
    template<>              struct _choose_type<f32>                      { x_constexprvar type value = type::F32;       }; 
    template<>              struct _choose_type<f64>                      { x_constexprvar type value = type::F64;       }; 
    template<typename T>    struct _choose_type<T*>                       { x_constexprvar type value = type::VOID_PTR;  }; 
    template<>              struct _choose_type<xstring_base<xchar>>      { x_constexprvar type value = type::XSTRING;   }; 
        
    template< typename T >  struct choose_type                            
    { 
        x_constexprvar type value = _choose_type<typename std::remove_const<T>::type >::value;
    };

    template<>                  struct choose_type<char*>               { x_constexprvar type value = type::CHAR_PTR;  }; 
    template<>                  struct choose_type<const char*>         { x_constexprvar type value = type::CHAR_PTR;  }; 

    template< type A >          struct is_convertable;
    template<>                  struct is_convertable<type::UNKNOWN>    { constexpr static bool get( type B ) { return false;                                           } };
    template<>                  struct is_convertable<type::S32>        { constexpr static bool get( type B ) { return !(B < type::S8        || B > type::U32        ); } };
    template<>                  struct is_convertable<type::U32>        { constexpr static bool get( type B ) { return !(B < type::S8        || B > type::U32        ); } };
    template<>                  struct is_convertable<type::S64>        { constexpr static bool get( type B ) { return !(B < type::S8        || B > type::U64        ); } };
    template<>                  struct is_convertable<type::U64>        { constexpr static bool get( type B ) { return !(B < type::S8        || B > type::U64        ); } };
    template<>                  struct is_convertable<type::S16>        { constexpr static bool get( type B ) { return !(B < type::S8        || B > type::U16        ); } };
    template<>                  struct is_convertable<type::U16>        { constexpr static bool get( type B ) { return !(B < type::S8        || B > type::U16        ); } };
    template<>                  struct is_convertable<type::S8>         { constexpr static bool get( type B ) { return !(B < type::S8        || B > type::U8         ); } };
    template<>                  struct is_convertable<type::U8>         { constexpr static bool get( type B ) { return !(B < type::S8        || B > type::U8         ); } };
    template<>                  struct is_convertable<type::F32>        { constexpr static bool get( type B ) { return !(B < type::F32       || B > type::F32        ); } };
    template<>                  struct is_convertable<type::F64>        { constexpr static bool get( type B ) { return !(B < type::F32       || B > type::F64        ); } };
    template<>                  struct is_convertable<type::VOID_PTR>   { constexpr static bool get( type B ) { return !(B < type::VOID_PTR  || B > type::CHAR_PTR   ); } };
    template<>                  struct is_convertable<type::CHAR_PTR>   { constexpr static bool get( type B ) { return !(B < type::CHAR_PTR  || B > type::XSTRING    ); } };
    template<>                  struct is_convertable<type::XSTRING>    { constexpr static bool get( type B ) { return !(B < type::XSTRING   || B > type::XSTRING    ); } };
};

//-------------------------------------------------------------------------------------------
// Description:
//      Creation of an argument list with known types
//-------------------------------------------------------------------------------------------
class xarg_list
{
public:

    class arg
    {
    public:

        using type = xarg_list_args::type;

    public:

        template<typename T>
        constexpr                   arg                     ( const T& Value )  noexcept : m_pValue( static_cast<const void*>( &Value ) ), m_Type { xarg_list_args::choose_type<T>::value } {}
        constexpr                   arg                     ( void )            noexcept {} // visual studio 2015 crashes if we make this = to default
        template< class T >
        constexpr const T&          get                     ( void ) const      noexcept { x_assert( xarg_list_args::choose_type<T>::value == m_Type ); return *reinterpret_cast<const T*> (m_pValue); }
        constexpr bool              isInt                   ( void ) const      noexcept { return !(m_Type < type::S8  || m_Type > type::U64 ); }
        constexpr bool              isFloat                 ( void ) const      noexcept { return !(m_Type < type::F32 || m_Type > type::F64 ); }
        constexpr bool              isString                ( void ) const      noexcept { return !(m_Type < type::CHAR_PTR || m_Type > type::XSTRING ); }
        x_inline  u64               getGenericInt           ( void ) const      noexcept; 
        x_inline  f64               getGenericFloat         ( void ) const      noexcept; 
        x_inline  const xchar*      getGenericCharString    ( void ) const      noexcept; 

    public:
            const void*         m_pValue { nullptr };
            type                m_Type   { type::UNKNOWN  };
    };

    enum : int { t_max_args = 15 };

public:

        constexpr                   xarg_list       ( void )                                    noexcept  = default;
        template<typename... T_ARGS>  
        constexpr                   xarg_list       ( const int Count, const T_ARGS&... Args )  noexcept; 
        template<typename... T_ARGS>  
        x_inline    xarg_list&      setup           ( const T_ARGS&... Args )                   noexcept;

public:

    int                         m_Count {0};
    xarray<arg, t_max_args>     m_lArgs {};
};