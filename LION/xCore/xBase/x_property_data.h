//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//-----------------------------------------------------------------------------

class xprop_s32
{
public:
    
    using                                   t_self      = xprop_s32;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::INT;
    x_constexprvar s32                      t_lowest    = X_S32_MIN;
    x_constexprvar s32                      t_highest   = X_S32_MAX;
    
    xprop_s32(void) = delete;
public:
    
    x_inline
    static bool SendOrReceive  ( xproperty_query& Query, s32& Val, const s32 Min = t_lowest, const s32 Max = t_highest )
    {
        if( Query.isReciving() ) Val = Recive( Query, Min, Max );
        else                           Send  ( Query, Val, Min, Max );
        return true;
    }
    
    constexpr
    static s32 Recive             ( const xproperty_query& Query, const s32 Min = t_lowest, const s32 Max = t_highest )
    {
        return x_assert(Min < Max), x_Max( Min, x_Min( Query.getProp<t_self>().getS32().m_Value, Max) );
    }
    
    x_inline
    static void Send              ( xproperty_query& Query, s32 Val, const s32 Min = t_lowest, const s32 Max = t_highest )
    {
        x_assert(Min < Max);
        auto& Data      = Query.setProp<t_self>().getS32();
        Data.m_Max      = Max;
        Data.m_Min      = Min;
        Data.m_Value    = x_Max( Min, x_Min(Val,Max) );
    }
    
    x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const s32 Value = 0 )
    {
        Data.setup( t_symbol );
        Data.m_Name             = Name;
        Data.getS32().m_Value   = Value;
    }
};

//-----------------------------------------------------------------------------

class xprop_f32
{
public:
    
    using                                   t_self      = xprop_f32;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::FLOAT;
    x_constexprvar f32                      t_lowest    = -X_F32_MAX;
    x_constexprvar f32                      t_highest   =  X_F32_MAX;
    
    xprop_f32(void) = delete;
public:
    
    x_inline
    static bool SendOrReceive  ( xproperty_query& Query, f32& Val, const f32 Min = t_lowest, const f32 Max = t_highest )
    {
        if( Query.isReciving() ) Val = Recive( Query, Min, Max );
        else                           Send  ( Query, Val, Min, Max );
        return true;
    }
    
    constexpr
    static f32 Recive             ( const xproperty_query& Query, const f32 Min = t_lowest, const f32 Max = t_highest )
    {
        return x_assert(Min < Max), x_Max( Min, x_Min( Query.getProp<t_self>().getF32().m_Value, Max) );
    }
    
    x_inline
    static void Send              ( xproperty_query& Query, f32 Val, const f32 Min = t_lowest, const f32 Max = t_highest )
    {
        x_assert(Min < Max);
        auto& Data      = Query.setProp<t_self>().getF32();
        Data.m_Max      = Max;
        Data.m_Min      = Min;
        Data.m_Value    = x_Max( Min, x_Min(Val,Max) );
    }
    
    x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const f32 Value = 0 )
    {
        Data.setup( t_symbol );
        Data.m_Name             = Name;
        Data.getF32().m_Value  = Value;
    }
};

//-----------------------------------------------------------------------------

class xprop_string
{
public:
    
    using                                   t_self      = xprop_string;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::STRING;

    xprop_string(void) = delete;
public:

    x_inline
    static bool SendOrReceive  ( xproperty_query& Query, xstring& String )
    {
        if( Query.isReciving() ) Recive( Query, String );
        else                     Send  ( Query, String );
        return true;
    }
    
    x_inline
    static void Recive             ( const xproperty_query& Query, xstring& String )
    {
        String = Query.getProp<t_self>().getString();
    }
    
    x_inline
    static void Send              ( xproperty_query& Query, xstring& String )
    {
        auto& Data      = Query.setProp<t_self>().getString();
        Data = String;
    }
    
    x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const xstring& Val )
    {
        Data.setup( t_symbol );
        Data.m_Name      = Name;
        Data.getString() = Val;
    }
};

//-----------------------------------------------------------------------------

class xprop_guid
{
public:
    
    using                                   t_self      = xprop_guid;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::GUID;
    
    xprop_guid(void) = delete;
public:
    
    x_inline
    static bool SendOrReceive  ( xproperty_query& Query, u64& Val, const char* pGuidType )
    {
        if( Query.isReciving() ) Val = Recive( Query );
        else                           Send  ( Query, Val, pGuidType );
        return true;
    }
    
    x_forceconst
    static u64 Recive             ( const xproperty_query& Query )
    {
        return Query.getProp<t_self>().getGUID().m_Value;
    }
    
    x_inline
    static void Send              ( xproperty_query& Query, u64 Val, const char* pGuidType )
    {
        auto& Data      = Query.setProp<t_self>().getGUID();
        Data.m_Value    = Val;
        Data.m_pType    = pGuidType;
    }
    
    x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const u64 Guid )
    {
        Data.setup( t_symbol );
        Data.m_Name             = Name;
        Data.getGUID().m_Value  = Guid;
    }
};

//-----------------------------------------------------------------------------

class xprop_v3
{
public:
    
    using                                   t_self      = xprop_v3;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::VECTOR3;
    x_constexprvar f32                      t_lowest    = -X_F32_MAX;
    x_constexprvar f32                      t_highest   =  X_F32_MAX;
    
    xprop_v3(void) = delete;
public:
    
    x_inline
    static bool SendOrReceive  ( xproperty_query& Query, xvector3& Val, const xvector3 Min = {t_lowest,t_lowest,t_lowest}, const xvector3 Max = {t_highest,t_highest,t_highest} )
    {
        if( Query.isReciving() ) Val = Recive( Query, Min, Max );
        else                           Send  ( Query, Val, Min, Max );
        return true;
    }
    
    x_inline
    static xvector3 Recive             ( const xproperty_query& Query, const xvector3 Min = {t_lowest,t_lowest,t_lowest}, const xvector3 Max = {t_highest,t_highest,t_highest} )
    {
        return Query.getProp<t_self>().getV3().m_Value.getMin( Max ).getMax( Min );
    }
    
    x_inline
    static void Send              ( xproperty_query& Query, const xvector3& Val, const xvector3 Min = {t_lowest,t_lowest,t_lowest}, const xvector3 Max = {t_highest,t_highest,t_highest} )
    {
        auto& Data      = Query.setProp<t_self>().getV3();
        Data.m_Max      = Max;
        Data.m_Min      = Min;
        Data.m_Value    = Val.getMin( Max ).getMax( Min );
    }
    
    x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const xvector3& Val )
    {
        Data.setup( t_symbol );
        Data.m_Name            = Name;
        Data.getV3().m_Value   = Val;
    }
};

//-----------------------------------------------------------------------------

class xprop_bool
{
public:
    
    using                                   t_self      = xprop_bool;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::BOOL;
    
    xprop_bool(void) = delete;
public:
    
    template< typename T > x_inline
    static bool SendOrReceive  ( xproperty_query& Query, T& Val, const T BitMask )
    {
        if( Query.isReciving() ) Val = Recive<T>( Query, BitMask );
        else                           Send<T>  ( Query, Val, BitMask );
        return true;
    }

    x_inline
    static bool SendOrReceive  ( xproperty_query& Query, bool& Val )
    {
        if( Query.isReciving() ) Val = Recive( Query );
        else                           Send  ( Query, Val );
        return true;
    }
    
    constexpr
    static bool Recive            ( const xproperty_query& Query )
    {
        return Query.getProp<t_self>().getBool();
    }

    template< typename T > constexpr
    static T Recive             ( const xproperty_query& Query, const T BitMask )
    {
        return  x_assert( BitMask < (sizeof(T)*8) ),
                x_assert( BitMask > 0 ),
                x_assert( x_isPowTwo(BitMask) ),
                x_static_cast<u32>(Query.getProp<t_self>().getBool()) << ( x_RoundToNextPowOfTwo( BitMask ) - 1);
    }
    
    template< typename T > x_inline
    static void Send              ( xproperty_query& Query, const T Value, const T BitMask )
    {
        x_assert( BitMask < (sizeof(T)*8) );
        x_assert( BitMask > 0 );
        x_assert( x_isPowTwo(BitMask) );
        Query.setProp<t_self>().getBool() = !!(Value&BitMask);
    }
    
    x_inline
    static void Send              ( xproperty_query& Query, const bool Value )
    {
        Query.setProp<t_self>().getBool() = Value;
    }

    template< typename T > x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const T Value, const T BitMask )
    {
        x_assert( BitMask < (sizeof(T)*8) );
        Data.setup( t_symbol );
        Data.m_Name             = Name;
        Data.getBool()          = !!(Value&BitMask);
    }

    x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const bool Value )
    {
        Data.setup( t_symbol );
        Data.m_Name             = Name;
        Data.getBool()          = Value;
    }
};

//-----------------------------------------------------------------------------

class xprop_enum
{
public:
    
    using                                   t_self      = xprop_enum;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::ENUM;
    
    using                                   entry       = xproperty_data::data_enum::entry;

    xprop_enum(void) = delete;
public:
    
    template< typename T> x_inline
    static bool SendOrReceive  ( xproperty_query& Query, T& Val, const entry* const pEntry, const xuptr EntryCount  )
    {
        if( Query.isReciving() ) Val = Recive<T>( Query,       pEntry, EntryCount );
        else                           Send<T>  ( Query, Val,  pEntry, EntryCount );
        return true;
    }
    
    template< typename T> x_forceinline
    static T Recive           ( const xproperty_query& Query, const entry* const pEntry, const xuptr EntryCount )
    {
        auto& Entry = Query.getProp<t_self>().getEnum();
        x_assert( Entry.m_String.isValid() );

        for( int i = 0; i < EntryCount; i++ )
        {
            if( Entry.m_String == pEntry[i].m_Name ) 
            {
                return static_cast<T>(pEntry[i].m_Value);
            } 
        }

        return static_cast<T>(0);
    }
    
    template< typename T> x_forceinline
    static void Send              ( xproperty_query& Query, u64 Val, const entry* const pEntry, const xuptr EntryCount )
    {
        int iVal=-1;
        for( int i = 0; i < EntryCount; i++ )
        {
            if( pEntry[i].m_Value == static_cast<int>(Val) ){ iVal = i; break; }
        }
        x_assert( iVal >= 0 );

        auto& Data          = Query.setProp<t_self>().getEnum();
        Data.m_String       = pEntry[iVal].m_Name;
        Data.m_pEnumList    = pEntry;
    }
    
    template< typename T> x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const T Val, const entry* const pEntry, const xuptr EntryCount  )
    {
        int iVal=-1;
        for( int i = 0; i < EntryCount; i++ )
        {
            if( pEntry[i].m_Value == static_cast<int>(Val) )
            { 
                iVal = i; 
                break; 
            }
        }
        x_assert( iVal >= 0 );

        Data.setup( t_symbol );
        auto& Enum = Data.getEnum();
        Enum.m_String       = pEntry[iVal].m_Name;
        Enum.m_pEnumList    = pEntry;
    }
};

//-----------------------------------------------------------------------------

class xprop_color
{
public:
    
    using                                   t_self      = xprop_color;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::RGBA;
    
    xprop_color(void) = delete;
public:
    
    x_inline
    static bool SendOrReceive  ( xproperty_query& Query, xcolor& Color )
    {
        if( Query.isReciving() ) Color = Recive( Query );
        else                             Send  ( Query, Color );
        return true;
    }
    
    constexpr
    static xcolor Recive             ( const xproperty_query& Query )
    {
        return Query.getProp<t_self>().getColor();
    }
    
    x_inline
    static void Send              ( xproperty_query& Query, xcolor Val )
    {
        Query.setProp<t_self>().getColor() = Val;
    }
    
    x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const xcolor Value )
    {
        Data.setup( t_symbol );
        Data.m_Name             = Name;
        Data.getColor()         = Value;
    }
};

//-----------------------------------------------------------------------------

class xprop_button
{
public:
    
    using                                   t_self      = xprop_string;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::BUTTON;

    xprop_button(void) = delete;
public:

    x_inline
    static bool SendOrReceive  ( xproperty_query& Query, xstring& String )
    {
        if( Query.isReciving() ) Recive( Query, String );
        else                     Send  ( Query, String );
        return true;
    }
    
    x_inline
    static void Recive             ( const xproperty_query& Query, xstring& String )
    {
        String = Query.getProp<t_self>().getButton();
    }
    
    x_inline
    static void Send              ( xproperty_query& Query, xstring& String )
    {
        auto& Data      = Query.setProp<t_self>().getButton();
        Data = String;
    }
    
    x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const xstring& Val )
    {
        Data.setup( t_symbol );
        Data.m_Name      = Name;
        Data.getButton() = Val;
    }
};


//-----------------------------------------------------------------------------

class xprop_r3
{
public:
    
    using                                   t_self      = xprop_r3;
    x_constexprvar xproperty_data::type     t_symbol    = xproperty_data::type::ANGLES3;
    x_constexprvar auto                     t_lowest    = xradian{ -X_F32_MAX };
    x_constexprvar auto                     t_highest   = xradian{  X_F32_MAX };
    
    xprop_r3(void) = delete;
public:
    
    x_inline
    static bool SendOrReceive  ( xproperty_query& Query, xradian3& Val, const xradian3 Min = {t_lowest,t_lowest,t_lowest}, const xradian3 Max = {t_highest,t_highest,t_highest} )
    {
        if( Query.isReciving() ) Val = Recive( Query, Min, Max );
        else                           Send  ( Query, Val, Min, Max );
        return true;
    }
    
    x_inline
    static xradian3 Recive     ( const xproperty_query& Query, const xradian3 Min = {t_lowest,t_lowest,t_lowest}, const xradian3 Max = {t_highest,t_highest,t_highest} )
    {
        xradian3 R = Query.getProp<t_self>().getR3().m_Value;
        R.ModAngle(); 
        R.m_Pitch = x_Max( Min.m_Pitch, x_Min( R.m_Pitch, Max.m_Pitch ) );
        R.m_Roll  = x_Max( Min.m_Roll , x_Min( R.m_Roll,  Max.m_Roll  ) );
        R.m_Yaw   = x_Max( Min.m_Yaw  , x_Min( R.m_Yaw,   Max.m_Yaw   ) );
        return R;
    }
    
    x_inline
    static void Send              ( xproperty_query& Query, const xradian3& Val, const xradian3 Min = {t_lowest,t_lowest,t_lowest}, const xradian3 Max = {t_highest,t_highest,t_highest} )
    {
        auto& Data      = Query.setProp<t_self>().getR3();
        Data.m_Max      = Max;
        Data.m_Min      = Min;

        xradian3 R = Val;
        R.ModAngle(); 
        R.m_Pitch       = x_Max( Min.m_Pitch, x_Min( R.m_Pitch, Max.m_Pitch ) );
        R.m_Roll        = x_Max( Min.m_Roll , x_Min( R.m_Roll,  Max.m_Roll  ) );
        R.m_Yaw         = x_Max( Min.m_Yaw  , x_Min( R.m_Yaw,   Max.m_Yaw   ) );
        Data.m_Value    = R;
    }
    
    x_inline
    static void setup           ( xproperty_data& Data, xstring& Name, const xradian3& Val )
    {
        Data.setup( t_symbol );
        Data.m_Name            = Name;
        Data.getR3().m_Value   = Val;
    }
};
