//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      Base class for all the jobs
//------------------------------------------------------------------------------
class x_trigger_base;

class x_job_base
{
    x_object_type( x_job_base, is_quantum_lock_free, rtti_start, is_not_copyable, is_not_movable );

public:

    // Global definition on what the max core count we suport
    enum { MAX_CORES = 24 };

    enum lifetime : u8
    {
        LIFETIME_DONT_DELETE_WHEN_DONE,
        LIFETIME_DELETE_WHEN_DONE,
        LIFETIME_COUNT
    };

    enum jobtype : u8
    {
        JOBTYPE_NORMAL,
        JOBTYPE_LIGHT,
        JOBTYPE_COUNT
    };
    
    enum affinity : u8
    {
        AFFINITY_NORMAL,
        AFFINITY_MAIN_THREAD,
        AFFINITY_NOT_MAIN_THREAD,
        AFFINITY_COUNT
    };

    enum priority : u8
    {
        PRIORITY_BELOW_NORMAL,
        PRIORITY_NORMAL,
        PRIORITY_ABOVE_NORMAL,
        PRIORITY_COUNT
    };
    
    X_DEFBITS(  definition, 
                u8,
                0x00,
                X_DEFBITS_ARG( lifetime, LIFETIME, x_Log2IntRoundUp( LIFETIME_COUNT-1 ) ),
                X_DEFBITS_ARG( jobtype,  JOBTYPE,  x_Log2IntRoundUp( JOBTYPE_COUNT-1  ) ),
                X_DEFBITS_ARG( affinity, AFFINITY, x_Log2IntRoundUp( AFFINITY_COUNT-1 ) ),
                X_DEFBITS_ARG( priority, PRIORITY, x_Log2IntRoundUp( PRIORITY_COUNT-1 ) )
    );

    enum : bool {   t_is_job        = true };
    using           t_base          = x_job_base;

public:
    
    constexpr                   x_job_base              ( void )                                noexcept = default;
    constexpr                   x_job_base              ( definition Def )                      noexcept : m_Definition{ Def } {}
    virtual                    ~x_job_base              ( void )                                noexcept {}
    
    inline      x_job_base&     setupDefinition         ( definition Def )                      noexcept;
    inline      x_job_base&     setupJobType            ( jobtype JobType )                     noexcept;
    inline      x_job_base&     setupLifeTime           ( lifetime LifeTime )                   noexcept;
    inline      x_job_base&     setupPriority           ( priority Priority )                   noexcept;
    constexpr   bool            isDeletedWhenDone       ( void )                        const   noexcept { return m_Definition.m_LIFETIME == LIFETIME_DELETE_WHEN_DONE; }
    constexpr   bool            isLightJob              ( void )                        const   noexcept { return m_Definition.m_JOBTYPE  == JOBTYPE_LIGHT; }
    constexpr   priority        getPriority             ( void )                        const   noexcept { return m_Definition.m_PRIORITY; }
    constexpr   affinity        getAffinity             ( void )                        const   noexcept { return m_Definition.m_AFFINITY; }
    inline      void            NotifyTrigger           ( x_trigger_base& Trigger )             noexcept;
    virtual     int             getTriggerCount         ( void )                        const   noexcept = 0; 

protected:

    virtual     void            qt_onRun                ( void )                                noexcept = 0;
    virtual     void            qt_onDone               ( void )                                noexcept { x_assert_quantum( m_Debug_LQ ); }
    virtual     void            onAppenTrigger          ( x_trigger_base& Trigger )             noexcept = 0;

protected:

#if _X_DEBUG
    x_debug_linear_quantum          m_Debug_LQ          {};
#endif
    definition                      m_Definition{ definition::LIFETIMEToFlags( LIFETIME_DONT_DELETE_WHEN_DONE ) | 
                                                  definition::JOBTYPEToFlags ( JOBTYPE_NORMAL                 ) | 
                                                  definition::AFFINITYToFlags( AFFINITY_NORMAL                ) | 
                                                  definition::PRIORITYToFlags( PRIORITY_NORMAL                ) };

protected:

    friend class x_scheduler;
};

//------------------------------------------------------------------------------
// Description:
//      The standard Job class. A job cares how many triggers it needs to notify
//------------------------------------------------------------------------------
template< unsigned int T_NUM_TRIGGERS >
class x_job : public x_job_base
{
    x_object_type( x_job, is_quantum_lock_free, rtti(x_job_base), is_not_copyable, is_not_movable );

protected:

    constexpr                   x_job           ( void )                            noexcept = default;
    constexpr                   x_job           ( definition Def )                  noexcept : x_job_base( Def ) {}
    virtual     void            qt_onDone       ( void )                            noexcept override;
    virtual     void            onAppenTrigger  ( x_trigger_base& Trigger )         noexcept override;
    virtual     int             getTriggerCount ( void )                    const   noexcept override { return m_nTriggers; } 
    x_inline    auto&           getTrigger      ( int Index )                       noexcept { x_assert( Index < m_nTriggers); return *m_TriggerList[Index]; }
    x_inline    const auto&     getTrigger      ( int Index )               const   noexcept { x_assert( Index < m_nTriggers); return *m_TriggerList[Index]; }

protected:

    xarray<x_trigger_base*,T_NUM_TRIGGERS>     m_TriggerList {};
    x_atomic<int>                              m_nTriggers   {0};
    
protected:
    
    template<int,typename> friend class x_trigger;
    friend class x_trigger_base;
};

//------------------------------------------------------------------------------
// Description:
//      This is the base trigger class used for any future trigger types
//------------------------------------------------------------------------------
class x_trigger_base 
{
    x_object_type( x_trigger_base, is_quantum_lock_free, rtti_start, is_not_copyable, is_not_movable )

public:

    enum flags
    {
        LIFETIME_DONT_DELETE_WHEN_DONE,
        LIFETIME_DELETE_WHEN_DONE
    };

    enum : bool {  t_is_trigger    = true };
    
public:
    
                                    x_trigger_base              ( void )                                        noexcept = default;
    x_orinlineconst                 x_trigger_base              ( flags Flags )                                 noexcept;
    virtual                        ~x_trigger_base              ( void )                                        noexcept { x_assume( m_NotificationCounter == 0 ); }
    x_orinlineconst bool            isGoingToBeDeletedWhenDone  ( void )                                const   noexcept;
    inline          void            Join                        ( void )                                        noexcept;
    template< class T_JOB >
    inline          void            JobWillNotifyMe             ( T_JOB& Job )                                  noexcept;
    inline          void            DontTriggerUntilReady       ( void )                                        noexcept;
    inline          bool            isTriggered                 ( void )                                const   noexcept { return m_NotificationCounter.load() == 0; }
    inline          void            DoTriggerWhenReady          ( std::atomic<bool>* pNotify = nullptr )        noexcept;
    virtual         int             getJobCount                 ( void )                                const   noexcept = 0;

protected:

    virtual         void            qt_onNotify                 ( void )                                        noexcept; 
    virtual         void            qt_onTriggered              ( void )                                        noexcept = 0;

protected:

#if _X_DEBUG
    bool                            m_Debug_bReady              { true };
    x_debug_linear_quantum          m_Debug_LQ                  {};
#endif

    std::atomic<int>                m_NotificationCounter       { 0 };
    std::atomic<bool>*              m_pNotifyVar                { nullptr };
    bool                            m_bDeleteWhenDone           { false };

protected:

    friend class x_job_base;
};

//------------------------------------------------------------------------------
// Description:
//      A general trigger able to notify jobs
//------------------------------------------------------------------------------
template< int T_MAX_JOBS, typename T_JOB_CLASS = x_job_base>
class x_trigger : public x_trigger_base
{
    x_object_type( x_trigger, is_quantum_lock_free, rtti(x_trigger_base), is_not_copyable, is_not_movable )

public:

    enum : unsigned
    { 
        MAX_JOBS               = T_MAX_JOBS
    };

    static_assert( T_JOB_CLASS::t_is_job, "" );

public:

    constexpr               x_trigger               ( void )                    noexcept {}
    constexpr               x_trigger               ( flags Flags )             noexcept : x_trigger_base{ Flags } {}
    inline                 ~x_trigger               ( void )                    noexcept;
    inline      void        AddJobToBeTrigger       ( x_job_base& Job )         noexcept;
    virtual     int         getJobCount             ( void )            const   noexcept override { return m_nJobsToTrigger; }
    x_inline    x_job_base& getJob                  ( int Index )       const   noexcept          { x_assert( Index < m_nJobsToTrigger ); return m_JobToTrigger[Index]; }

protected:

    virtual     void        qt_onTriggered          ( void )                    noexcept override;

protected:

    std::atomic<int>                m_nJobsToTrigger    {0};
    xarray<x_job_base*,T_MAX_JOBS>  m_JobToTrigger      {};
};

//------------------------------------------------------------------------------
// Description:
//      Job specialization for the (no triggers to notify) case
//------------------------------------------------------------------------------
template<>
class x_job<0> : public x_job_base
{
    x_object_type( x_job, is_quantum_lock_free, rtti(x_job_base), is_not_copyable, is_not_movable );

public:

    constexpr           x_job           ( void )                            noexcept = default;
    constexpr           x_job           ( const definition Def )            noexcept : x_job_base{ Def } {}
    virtual     int     getTriggerCount ( void )                    const   noexcept override { return 0; } 


protected:

    virtual     void    onAppenTrigger  ( x_trigger_base& Trigger ) noexcept override { x_assert_linear( m_Debug_LQ ); x_assume( false ); }
};

//------------------------------------------------------------------------------
// Description:
//      A block of jobs very handy for doing inline jobs with lambdas
//------------------------------------------------------------------------------
class x_job_block final : public x_trigger_base
{
    x_object_type( x_job_block, is_quantum_lock_free, rtti(x_trigger_base), is_not_copyable, is_not_movable )

public:

    inline                  x_job_block     ( void )                                    noexcept;
    virtual                ~x_job_block     ( void )                                    noexcept;
    inline      void        SubmitJob       ( const xfunction<void( void )> func )     noexcept;
    inline      void        Join            ( void )                                    noexcept;


protected:

    enum : unsigned { MAX_CONCURRENT_JOBS = x_job_base::MAX_CORES*2 };
    enum state : unsigned
    { 
        STATE_PROCESS_STATE_NOT_READY,
        STATE_PROCESS_STATE_READY,
        STATE_PROCESS_STATE_DONE,
    };

    struct lambda_job final : public x_job<0>
    {
        virtual void    qt_onRun       ( void ) noexcept override       { m_Function(); }
        virtual void    qt_onDone      ( void ) noexcept override 
        { 
            m_pJobBlock->m_FreeJobs.push( *this );

            // Now the job block is free to delete the class
            NotifyTrigger( *m_pJobBlock );
            /////
            // 
            // DEAD SPACE
            //
            /////
        }

        x_job_block*              m_pJobBlock       {};
        xfunction<void( void )>  m_Function        {};
    };

protected:
    
    virtual void    qt_onNotify     ( void )            noexcept override;
    virtual void    qt_onTriggered  ( void )            noexcept override;
    virtual int     getJobCount     ( void )    const   noexcept override { return 0; }
 

protected:
    
    x_ll_fixed_pool_static<lambda_job, MAX_CONCURRENT_JOBS>     m_FreeJobs      {};
    x_atomic<state>                                             m_ProcessState  {};
};

//-------------------------------------------------------------------------------------------
// PRIVATE section for the job_chain
//-------------------------------------------------------------------------------------------
namespace x_scheduler_jobs{ 
namespace job_chain
{
    //-------------------------------------------------------------------------------------------

    struct state_block final : public x_ll_share_object
    {
        enum  { t_stack_aligment = x_Max( alignof(xmatrix4), alignof(x_atomic<xuptr>) ) };

        //-------------------------------------------------------------------------------------------
    
        struct alignas( t_stack_aligment ) stack_space 
        {
            xarray<xbyte, 1024 >        m_Block         {};
        };

        //-------------------------------------------------------------------------------------------

        struct function_context final
        {
            constexpr   int             getArgIndex     ( void ) const   noexcept { return m_iStackIndex;               }
            constexpr   int             getRetIndex     ( void ) const   noexcept { return 1-m_iStackIndex;             }
            constexpr   const void*     getArgBuff      ( void ) const   noexcept { return &m_Stack[m_iStackIndex];     }
            inline      void*           getArgBuff      ( void )         noexcept { return &m_Stack[m_iStackIndex];     }
            inline      void*           getRetBuff      ( void )         noexcept { return &m_Stack[1-m_iStackIndex];   }
            constexpr   const void*     getRetBuff      ( void ) const   noexcept { return &m_Stack[1-m_iStackIndex];   }
            constexpr   bool            isEndState      ( void ) const   noexcept { return m_iFunction == m_pBlock->m_nFuctions && m_pBlock->m_pNext == nullptr; }
            inline      bool            NextState       ( void )         noexcept;
            inline      void            ExecuteStates   ( void )         noexcept;

            state_block*                m_pBlock        { nullptr };
            int                         m_iFunction     { 0 };
            int                         m_iStackIndex   { 0 };
            xarray<stack_space,2>       m_Stack         {};
        };

        //-------------------------------------------------------------------------------------------
        struct executable_state final
        {
            using t         = void( const xbyte* pFunction, void* pRet, void* pArg );
            using storage   = xarray<xbyte,sizeof(xfunction<void(void)>)>; 
            
            t*                                          m_pSpecialCall  { nullptr };
            alignas(xfunction<void(void)>) storage     m_Function      {};
        };

        template< typename T > 
        constexpr static void       t_DeleteSharedInstance  ( T* pData )        noexcept;

        template< typename T_CLASS, typename ...T_ARG >  
        constexpr static T_CLASS*   t_NewSharedInstance     ( T_ARG&&...Args ) noexcept;

        //-------------------------------------------------------------------------------------------
        int                             m_nFuctions     { 0 };
        state_block*                    m_pNext         { nullptr };
        xarray<executable_state,16>     m_Futures       {};
    };

    //-------------------------------------------------------------------------------------------

    struct lambda_job final : 
        public x_job<0>,
        public x_ll_share_object
    {
        virtual             void        qt_onRun        ( void ) noexcept override   { m_Context.ExecuteStates(); }  
        virtual             void        qt_onDone       ( void ) noexcept override; 
        inline              bool        Complete        ( void ) noexcept;

        template< typename T > 
        constexpr static    void        t_DeleteSharedInstance( T* pData )        noexcept;

        template< typename T_CLASS, typename ...T_ARG >  
        constexpr static    T_CLASS*    t_NewSharedInstance( T_ARG&&...Args ) noexcept;

        x_ll_share_ref< state_block, int >              m_hStateBlock   {};
        x_atomic<bool>                                  m_bHasCompleted { false };
        state_block::function_context                   m_Context       {};

        template< typename T_JOB_CHAIN_FUNCTION, typename T_RET > friend class x_job_chain;
    };

}};

//-------------------------------------------------------------------------------------------
// Description:
//              Job chains are some what similar to the following concepts:
//              #include <ppljob_chains.h>
//              #include <future>
//              However unlike those a job_chan is basically a definition of a multi-core
//              function. Which has inputs and outputs. Its modularity allows to easily 
//              compose them. Unlike the previously mention concepts such futures. It has the
//              additional concept of a type and an execution. Such you can have one time and
//              many concurrent executions of that type.
//-------------------------------------------------------------------------------------------

template< typename T_JOB_CHAIN_FUNCTION_TYPE, typename T_RET = void >
class x_job_chain final
{
public:

    using t_self                        = x_job_chain;
    using t_function_return             = T_RET;
    using t_job_chain_type              = T_JOB_CHAIN_FUNCTION_TYPE;
    using t_job_chain_function_traits   = xfunction_traits<t_job_chain_type>;
    using t_job_chain_function_arg      = typename t_job_chain_function_traits::template t_arg<0>::type;
    using t_job_chain_function_return   = typename t_job_chain_function_traits::t_return_type;

    class executable_instance final
    {   
     public:

        constexpr   bool    isDone      ( void )    const   noexcept { return m_hLambdaJob->m_bHasCompleted.load(); }
        inline      auto    getTask     ( void )            noexcept;
        inline      auto    get         ( void )            noexcept;
        inline      void    Join        ( void )            noexcept;

    protected:

        inline      void    Sync        ( void )            noexcept;

    protected:

        x_ll_share_ref< x_scheduler_jobs::job_chain::lambda_job, int > m_hLambdaJob {};

    protected:

        friend class x_job_chain;
    };

public:

                                x_job_chain         ( void )                            noexcept = default;
    constexpr                   x_job_chain         ( x_job_chain&& TaskCnt )           noexcept : m_hStateBlock{ std::move(TaskCnt.m_hStateBlock) } {}
    constexpr                   x_job_chain         ( x_job_chain&  TaskCnt )           noexcept : m_hStateBlock{ TaskCnt.m_hStateBlock            } {}
    inline      x_job_chain&    operator =          ( x_job_chain&& Task )              noexcept { m_hStateBlock = std::move(Task.m_hStateBlock ); return *this; }
    inline      x_job_chain&    operator =          ( x_job_chain&  Task )              noexcept { m_hStateBlock = Task.m_hStateBlock;             return *this; }

    template< typename T_FUNCTION > 
    inline static   auto        Create              ( const T_FUNCTION Function )       noexcept;

    // Todo: deep copys the inJobChain to a new job_chain
    template< typename T_TYPE, typename T_R >
    inline          auto        Clone               ( const x_job_chain<T_TYPE,T_R>& JobChain ) noexcept {return nullptr;}

    template< typename T_FUNCTION >                    
    inline          auto&       Then                ( const T_FUNCTION&& Function )       noexcept;

    // Todo: Appends the command of the inJobChain to this, note incoming jobchain argument type must match with existing return type
    template< typename T_TYPE, typename T_R >
    inline          auto        Then                ( const x_job_chain<T_TYPE,T_R>& JobChain ) noexcept {return nullptr;}

    template< typename T  >
    inline          void        Join                ( T&& Arg )                         noexcept;
    inline          void        Join                ( void )                            noexcept;

    template< typename T  >
    inline          auto        get                 ( T&& Arg )                         noexcept;
    inline          auto        get                 ( void )                            noexcept;

    template< typename T >
    inline          auto        RunNormal            ( T&& Arg )                         noexcept { return std::move(Run( std::move(Arg), x_job_base::jobtype::JOBTYPE_NORMAL )); }
    inline          auto        RunNormal            ( void )                            noexcept { return std::move(Run(                 x_job_base::jobtype::JOBTYPE_NORMAL )); }

    template< typename T >
    inline          auto        RunLight             ( T&& Arg )                         noexcept { return std::move(Run( std::move(Arg), x_job_base::jobtype::JOBTYPE_LIGHT )); }
    inline          auto        RunLight             ( void )                            noexcept { return std::move(Run(                 x_job_base::jobtype::JOBTYPE_LIGHT )); }

protected:

    template< typename T >
    inline          auto        Run                  ( T&& Arg, x_job_base::jobtype Type ) noexcept;
    inline          auto        Run                  (          x_job_base::jobtype Type ) noexcept;

protected:

    x_ll_share_ref< x_scheduler_jobs::job_chain::state_block, int >    m_hStateBlock {};


protected:

    template< typename, typename > friend class x_job_chain;
};

