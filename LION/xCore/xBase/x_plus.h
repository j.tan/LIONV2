//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


 #define x_Sleep(x) std::this_thread::sleep_for(std::chrono::milliseconds(x))

 //------------------------------------------------------------------------------
// Description:
//      returns a power of 2 integer given n. result = 2^n  
//------------------------------------------------------------------------------
template< typename T > constexpr
T x_Bit( T n ) 
{ 
    static_assert( std::is_integral<T>::value,"" ); 
    return static_cast<T>(1 << n); 
}

//------------------------------------------------------------------------------
// Description:
//      Takes an Address or integer and aligns it up base on the given alignment.
//      The result in the next number greater than or equal to "n" 
//      which is a multiple of "a".  For example, x_Align(57,16) is 64.
// Arguments:
//     Addr       - This is the address/number/offset to align
//     AlignTo    - This is a power of 2 number that the user wants it to be align to
//------------------------------------------------------------------------------
template< typename T > constexpr
T x_Align( T Address, const int AlignTo )
{
    static_assert( std::is_integral<T>::value, "This function only works with integer values" );
    using unsigned_t = typename x_size_to_unsigned<sizeof(T)>::type; 
    return static_cast<T>( (unsigned_t( Address ) + (AlignTo - 1)) & (-AlignTo) );
}

//------------------------------------------------------------------------------

template< typename T > inline
T* x_Align( T* Address, const int AlignTo )
{
    return reinterpret_cast<T*>( x_Align( reinterpret_cast<const xuptr>( Address ), AlignTo ) );
}

//------------------------------------------------------------------------------
// Description:
//      Takes an Address or integer and aligns it up base on the given alignment.
//      The result in the next number less than or equal to "n" 
//      which is a multiple of "a".  For example, x_AlignLower(57,16) is 48.
// Arguments:
//     Addr       - This is the address/number/offset to align
//     AlignTo    - This is a power of 2 number that the user wants it to be align to
//------------------------------------------------------------------------------

template< typename T > constexpr
T x_AlignLower( T Address, const int AlignTo )
{
    static_assert( std::is_integral<T>::value, "This function only works with integer values" );
    using unsigned_t = typename x_size_to_unsigned<sizeof(T)>::type; 
    return static_cast<T>( unsigned_t( Address ) & (-AlignTo) );
}

//------------------------------------------------------------------------------

template< typename T > inline
T* x_AlignLower( T* Address, const int AlignTo )
{
    return reinterpret_cast<T*>( x_AlignLower( reinterpret_cast<const xuptr>( Address ), AlignTo ) );
}

//------------------------------------------------------------------------------
// Description:
//     This macro result in a TRUE/FALSE which indicates whether a number/address/offset
//     is align to a certain power of two provided by the user.
// Arguments:
//     Addr       - This is the address/number/offset to align
//     AlignTo    - This is a power of 2 number that the user wants it to be align to
//------------------------------------------------------------------------------
template< typename T > constexpr 
bool x_isAlign( T Addr, const int AlignTo )
{
    static_assert( std::is_pointer<T>::value || std::is_integral<T>::value,"" );
    using unsigned_t = typename x_size_to_unsigned<sizeof(T)>::type; 
    return (unsigned_t(Addr) & (AlignTo-1)) == 0;
}

//------------------------------------------------------------------------------
// Description:
//       Functions to help deal with flags
//------------------------------------------------------------------------------
template< class T > constexpr void    x_FlagToggle(       T& N, const u32 F );
template< class T > constexpr void    x_FlagOn    (       T& N, const u32 F );
template< class T > constexpr void    x_FlagOff   (       T& N, const u32 F );
template< class T > constexpr bool    x_FlagIsOn  ( const T  N, const u32 F );
template< class T > constexpr bool    x_FlagsAreOn( const T  N, const u32 F );

//------------------------------------------------------------------------------
// Description:
//      Determines the minimun power of two that emcapsulates the given number
//      Other cool int bits magic: http://graphics.stanford.edu/~seander/bithacks.html#CopyIntegerSign
//------------------------------------------------------------------------------
namespace _x_plus
{
    //------------------------------------------------------------------------------
    template< typename T> constexpr
    T Log2Int( T x, int p = 0 ) 
    {
        return (x <= 1) ? p : _x_plus::Log2Int(x >> 1, p + 1);
    }

    //------------------------------------------------------------------------------
    template< typename T > constexpr
    int NextPowOfTwo( const T x, const int s )
    {
        static_assert( std::is_integral<T>::value, "" );
        return static_cast<int>( ( s == 0 ) ? 1 + x : _x_plus::NextPowOfTwo<T>( x | (x >> s), s>>1 ) );
    }
}

//------------------------------------------------------------------------------
template< class T > constexpr   
bool x_isPowTwo( const T x )
{ 
    static_assert( std::is_integral<T>::value, "" ); 
    return !((x - 1) & x); 
}

//------------------------------------------------------------------------------
// Description:
//      Determines the minimum power of two that encapsulates the given number
// Example:
//      x_Log2IntRoundUp(3) == 2 // it takes 2bits to store #3
template< typename T> constexpr
T x_Log2IntRoundUp( T x ) 
{
    static_assert( std::is_integral<T>::value, "" ); 
    return x < 1 ? 0 : _x_plus::Log2Int(x) + 1; 
}
static_assert( 0  == x_Log2IntRoundUp(0), "" );
static_assert( 1  == x_Log2IntRoundUp(1), "" );
static_assert( 2  == x_Log2IntRoundUp(2), "" );
static_assert( 2  == x_Log2IntRoundUp(3), "" );
static_assert( 3  == x_Log2IntRoundUp(4), "" );
static_assert( 3  == x_Log2IntRoundUp(5), "" );
static_assert( 3  == x_Log2IntRoundUp(6), "" );
static_assert( 3  == x_Log2IntRoundUp(7), "" );
static_assert( 4  == x_Log2IntRoundUp(8), "" );
static_assert( 10 == x_Log2IntRoundUp(1023), "" );
static_assert( 11 == x_Log2IntRoundUp(1024), "" );

//------------------------------------------------------------------------------
// Description:
//      Rounds a number to the next power of two
// Example:
//      x_RoundToNextPowOfTwo(3) == 4   // The next power from #3 is #4
template< typename T> constexpr
int x_RoundToNextPowOfTwo( const T x )             
{ 
    static_assert( std::is_integral<T>::value, "" ); 
    return ( x == 0 ) ? 0 : _x_plus::NextPowOfTwo<T>( x - 1, static_cast<int>(4*sizeof(T)) ); 
}

//------------------------------------------------------------------------------
// Description:
//       a murmur Hash Key Generator.
// Algorithm:
//      from code.google.com/p/smhasher/wiki/MurmurHash3
//------------------------------------------------------------------------------
namespace _x_plus
{
    //------------------------------------------------------------------------------
    template< int T_SIZE >
    struct murmurHash3_by_size {};

    //------------------------------------------------------------------------------
    template<>
    struct murmurHash3_by_size<4> 
    {
        inline
        static u32 Compute( u32 h )
        {
            h ^= h >> 16;
            h *= 0x85ebca6b;
            h ^= h >> 13;
            h *= 0xc2b2ae35;
            h ^= h >> 16;
            return h;
        }
    };

    //------------------------------------------------------------------------------
    template<>
    struct murmurHash3_by_size<8> 
    {
        inline
        static u64 Compute( u64 h )
        {
            h ^= h >> 33;
            h *= 0xff51afd7ed558ccd;
            h ^= h >> 33;
            h *= 0xc4ceb9fe1a85ec53;
            h ^= h >> 33;
            return h;
        }
    };
}

//------------------------------------------------------------------------------
template< typename T > inline 
T x_MurmurHash3( T h )
{
    static_assert( sizeof(T) >= 4 && sizeof(T) <= 8, "" );
    static_assert( std::is_integral<T>::value,"" );
    return static_cast<T>(_x_plus::murmurHash3_by_size<sizeof(T)>::Compute( static_cast< typename x_size_to_unsigned<sizeof(T)>::type >(h) ));
}

//------------------------------------------------------------------------------
// Description:
//      Quicksorts a buffer  
//------------------------------------------------------------------------------
void x_qsort( void*          apBase,     
              const xuptr    NItems,    
              const int      ItemSize,  
              xfunction<int(const void*pA, const void*pB)> Compare ); 


//------------------------------------------------------------------------------
// Description:
//     Binary search of a sorted array, with where item should be in the 
//     list in case of a miss.
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_KEY > inline
bool x_BinSearch(   const T_KEY&                                            Key,
                    const xbuffer_view<T_ENTRY>                             View,
                    xuptr&                                                  iLocation,
                    xfunction< int ( const T_ENTRY& ) >                    Compare );


//------------------------------------------------------------------------------
// Description:
//     Swap two different items via assignments.
//------------------------------------------------------------------------------
template< class T > inline 
void x_Swap( T& a, T& b ) { const T c(a); a=b; b=c; }