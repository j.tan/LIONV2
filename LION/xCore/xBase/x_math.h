//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

// TODO:
//
//  * All the inline functions should be order such the if a function a needs 
//     function b. To make sure that b comes before a. This is because some
//     compilers only inline if it comes before.
//
//  * Do a pass and see where the vector3 are needed vs the vector3d. Such there is never
//     a reason to return a vector3d always return a vector3. It should never be a reason why
//     to use a vector3d as a temporary variable always use vector3
//
//  * Do a pass on isValid && ASSERTS for all the classes.
// 
//==============================================================================

//==============================================================================
//  GENERAL MATH FUNCTIONS
//==============================================================================
//
// Functions:
//
//  f32   x_Sqrt    ( f32 x )           - Square root.
//  f32   x_Floor   ( f32 x )           - Floor.
//  f32   x_ceil    ( f32 x )           - Ceiling.
//  f32   x_Log     ( f32 x )           - Log base e.  Natural log.
//  f32   x_Log2    ( f32 x )           - Log base 2.
//  f32   x_Log10   ( f32 x )           - Log base 10.
//  f32   x_Exp     ( f32 x )           - Raise e to x power.
//  f32   x_Pow     ( f32 x )           - Raise a number to a power.
//  f32   x_FMod    ( f32 x, f32 y )    - True modulus between two numbers.
//  f32   x_ModF    ( f32 x, f32* y )   - Break number into whole (integer) and fractional parts.
//  T     x_Sqr     ( T x )             - Square.
//  f32   x_InvSqrt ( f32 x )           - One over square root.
//  f32   x_LPR     ( f32 x, f32 y )    - Least Positive Residue.  Non-negative modulus value.
//  T     x_Abs     ( T x, T y )        - Absolute value of any signed numeric type.
//  f32   x_Round   ( f32 x, f32 y )    - Round a number to nearest multiple of another number.
//  bool  x_IsValid ( f32 x )           - Returns TRUE if value is valid (not INF or NAN).
//  T     x_Min     ( f32 x, f32 y )    - Returns what ever number is the minimun. (f32, s32, s8, etc)
//  T     x_Max     ( f32 x, f32 y )    - Returns what ever number is the maximun. (f32, s32, s8, etc)
//  bool  x_Sign    ( f32 x )           - Returns the sign of a number (-1,0,1)  (f32, s32, s8, etc)
//
// Additional functions:
//
//  T   x_InRange( T x, T min, T max )  - Returns a boolean indicating whether a number is in rage of two others.
//  T   x_Range  ( T x, T min, T max )  - Returns a number which will be in the range of two to other.
//
//==============================================================================

//==============================================================================
//  TRIGONOMETRIC MATH FUNCTIONS
//==============================================================================
//
// Types:
//
//  xradian - is a typed defined to describe xradian values.
//
// Functions:
//  
//  f32      x_Sin   ( xradian Angle   )  - Sine.
//  f32      x_Cos   ( xradian Angle   )  - Cosine.
//  f32      x_Tan   ( xradian Angle   )  - Tangent.
//  xradian  x_ASin  ( f32     Sine    )  - Arc sine.
//  xradian  x_ACos  ( f32     Cosine  )  - Arc cosine.
//  xradian  x_ATan  ( f32     Tangent )  - Arc tangent.
//  xradian  x_ATan2 ( f32 y, f32 x    )  - Standard "atan2" arc tangent where y can equal 0.
//                
// Additional functions:
//
//  void     x_SinCos       ( xradian Angle, f32& sin, f32& cos ) - Sine and cosine in one function call.
//  xradian  x_ModAngle     ( xradian Angle )                     - Provide equivalent angle in [    0, 360 ) degrees.
//  xradian  x_ModAngle2    ( xradian Angle )                     - Provide equivalent angle in [ -180, 180 ) degrees.
//  xradian  x_MinAngleDiff ( xradian Angle1, xradian Angle2 )    - Provide smallest angle between two given angles.
//
// There are some macros also defined
//
//  PI                      - It is the constance value of PI. (in floating point)
//  f32 x_DegToRad( T x )   - Converts degrees to xradians
//  f32 x_RadToDeg( T x )   - Converts a number from xradiants to degrees.
//  f32 XRADIAN( T x )      - Works the same way than x_DegToRad
//
//==============================================================================

//==============================================================================
// CONSTANTS
//==============================================================================
#ifdef PI
    #undef PI
#endif

#ifdef M_E
    #undef M_E
#endif

x_constexprvar auto         M_E         { 2.7182818284590452354f  }; // e 
x_constexprvar auto         LOG2E       { 1.4426950408889634074f  }; // log 2e 
x_constexprvar auto         LOG10E      { 0.43429448190325182765f }; // log 10e 
x_constexprvar auto         LN2         { 0.69314718055994530942f }; // log e2 
x_constexprvar auto         LN10        { 2.30258509299404568402f }; // log e10 
x_constexprvar auto         SQRT2       { 1.41421356237309504880f }; // sqrt(2)
x_constexprvar xradian      PI          { 3.14159265358979323846f }; // pi
x_constexprvar xradian64    PI_64       { 3.14159265358979323846  }; // pi 64bits 
x_constexprvar xradian      PI2         { 6.28318530717958647693f }; // pi * 2
x_constexprvar xradian      PI_OVER2    { 1.57079632679489661923f }; // pi / 2
x_constexprvar xradian      PI_OVER4    { 0.78539816339744830962f }; // pi / 4

x_constexprvar auto  XFLT_TOL            {0.001f};

//==============================================================================
// ANGLE TYPES
//==============================================================================

template< typename T > x_forceconst T         x_DegToRad          ( const T   a )  { return a                   * static_cast<decltype(a)>(0.017453292519943295769);    }
                       x_forceconst f32       x_DegToRad          ( const int a )  { return static_cast<f32>(a) * static_cast<f32>(0.017453292519943295769);            }
template< typename T > x_forceconst T         x_RadToDeg          ( const T   a )  { return a                   * static_cast<decltype(a)>(57.29577951308232087685);    }
                       x_forceconst f32       x_RadToDeg          ( const int a )  { return static_cast<f32>(a) * static_cast<f32>(57.29577951308232087685);            }

//==============================================================================
// PREDEFINITIONS 
//==============================================================================
class      xradian3;               // Software base 2 floats no alignment
class      xvector2;               // Software base 2 floats no alignment
class      xvector3d;              // Software base 3 floats no alignment
class      xvector3;               // Hardware accelerated 4 floats and 16 bytes align
class      xvector4;               // Hardware accelerated with 16 bytes align
class      xmatrix4;               // Hardware base matrix Row major with 16 bytes align compatible with d3d
class      xquaternion;            // Hardware accelerated and 16 bytes align
class      xplane;                 // Software base 4 floats no alignment 
class      xbbox;                  // Software base 6 floats no alignment 
class      xsphere;                // Software base 4 floats no alignment
class      xirect;                 // Software base 6 integers no alignment 

//==============================================================================
// BASIC FUNTIONS
//==============================================================================
inline                          xradian             x_ModAngle              ( const xradian Angle );
inline                          xradian             x_ModAngle2             ( const xradian Angle );
inline                          xradian             x_MinAngleDiff          ( const xradian Angle1, const xradian Angle2 );
inline                          xradian             x_LerpAngle             ( const f32 t, const xradian Angle1, const xradian Angle2 );
inline                          void                x_SinCos                ( const xradian Angle, f32& S, f32& C );
inline                          f32                 x_Sin                   ( const xradian x );
inline                          f32                 x_Cos                   ( const xradian x );        
inline                          f64                 x_SinF64                ( const xradian64 x );
inline                          f64                 x_CosF64                ( const xradian64 x );
inline                          xradian             x_ASin                  ( const f32 x );
inline                          xradian             x_ACos                  ( const f32 x );
inline                          f32                 x_Tan                   ( const xradian x );
inline                          f64                 x_TanF64                ( const xradian64 x );
inline                          xradian             x_ATan2                 ( const f32 x, const f32 y );    
inline                          xradian             x_ATan                  ( const f32 x );    

template< class T > 
constexpr                       T                   x_Sqr                   ( const T   x );
inline                          f32                 x_Sqrt                  ( const f32 x );	
inline                          f64                 x_SqrtF64               ( const f64 x );
inline                          f32                 x_InvSqrt               ( const f32 x );
inline                          bool                x_SolvedQuadraticRoots  ( f32& Root1, f32& Root2, const f32 a, const f32 b, const f32 c );
inline                          f32                 x_Exp                   ( const f32 x );
inline                          f64                 x_ExpF64                ( const f64 x );
inline                          f32                 x_Pow                   ( const f32 a, const f32 b );
inline                          f64                 x_PowF64                ( const f64 a, const f64 b );
inline                          f32                 x_FMod                  ( const f32 x, const f32 y );
inline                          f32                 x_ModF                  ( const f32 x, f32& y );
inline                          f32                 x_Log                   ( const f32 x );
inline                          f64                 x_LogF64                ( const f64 x );
inline                          f32                 x_Log2                  ( const f32 x ); 
inline                          f32                 x_Log10                 ( const f32 x );
inline                          f32                 x_LPR                   ( const f32 a, const f32 b );
template< class T > 
constexpr                       T                   x_Lerp                  ( const f32 t, const T  a, const T  b );
template< typename T1, typename T2 > 
constexpr                       auto                x_Min                   ( const T1  a, const T2 b ) -> decltype( a + b ) { return a < b ? a : b; }
template< typename T1, typename T2 > 
constexpr                       auto                x_Max                   ( const T1  a, const T2 b ) -> decltype( a + b ) { return a > b ? a : b; }
inline                          bool                x_FEqual                ( const f32 f0, const f32 f1, const f32 tol = XFLT_TOL );
constexpr                       bool                x_FLess                 ( const f32 f0, const f32 f1, const f32 tol = XFLT_TOL );
constexpr                       bool                x_FGreater              ( const f32 f0, const f32 f1, const f32 tol = XFLT_TOL ); 
constexpr                       f32                 x_FSel                  ( const f32 A,  const f32 B,  const f32 C );
constexpr                       f32                 x_I2F                   ( const f32 i );
constexpr                       s32                 x_F2I                   ( const f32 f );
x_forceinline                   bool                x_isValid               ( const f32 x );
inline                          s32                 x_LRound                ( const f32 x );
inline                          f32                 x_Round                 ( const f32 a, const f32 b );
inline                          f32                 x_Ceil                  ( const f32 x );
inline                          f32                 x_Floor                 ( const f32 x );
template< class T > 
constexpr                       bool                x_Sign                  ( const T x );
template< class T > 
constexpr                       bool                x_isInrange             ( const T X, const T Min, const T Max );
template< class T > 
constexpr                       T                   x_Range                 ( const T X, const T Min, const T Max );
template< class T > 
constexpr                       T                   x_Abs                   ( const T x );

//------------------------------------------------------------------------------
// Description:
//     This class represents a 3D rotation using the classical Pitch, Yaw and Roll.
//     It is very useful when trying to minimize the memory footprint needed to 
//     represent a rotation or if you just want to deal with only angles.
//
//<P>  The class is not hardware accelerated and works only with floats. There
//     is not need for special aligments except for the atomic types.
// See Also:
//     xradian xquaternion xmatrix4
//------------------------------------------------------------------------------
class xradian3 
{
    x_object_type( xradian3, is_linear );

public:
    
    constexpr                               xradian3                ( void ) = default; 
    constexpr                               xradian3                ( const xradian Angles ) : m_Pitch(Angles), m_Yaw(Angles), m_Roll(Angles) {}
    constexpr                               xradian3                ( xradian Pitch, xradian Yaw, xradian Roll ) : m_Pitch(Pitch), m_Yaw(Yaw), m_Roll(Roll) {}
    inline                                  xradian3                ( const xquaternion& Q );
    inline                                  xradian3                ( const xmatrix4& M );

    inline              void                setZero                 ( void );
    inline              xradian3&           setup                   ( xradian Pitch, xradian Yaw, xradian Roll );

    inline              xradian3&           ModAngle                ( void );
    inline              xradian3&           ModAngle2               ( void );
    inline              xradian3            getMinAngleDiff         ( const xradian3& R ) const;

    constexpr           xradian             Difference              ( const xradian3& R ) const;
    constexpr           bool                isInrange               ( xradian Min, xradian Max ) const;
    x_forceinline       bool                isValid                 ( void ) const;

    constexpr           bool                operator ==             ( const xradian3& R ) const;
    inline              const xradian3&     operator +=             ( const xradian3& R );
    inline              const xradian3&     operator -=             ( const xradian3& R );
    inline              const xradian3&     operator *=             ( f32 Scalar );
    inline              const xradian3&     operator /=             ( f32 Scalar );

    inline friend       xradian3            operator +              ( const xradian3& R1, const xradian3& R2 );
    inline friend       xradian3            operator -              ( const xradian3& R1, const xradian3& R2 );
    inline friend       xradian3            operator -              ( const xradian3& R );
    inline friend       xradian3            operator /              ( const xradian3& R,        f32      S  );
    inline friend       xradian3            operator *              ( const xradian3& R,        f32      S  );
    inline friend       xradian3            operator *              (        f32      S,  const xradian3& R  );

public:

    xradian     m_Pitch {};
    xradian     m_Yaw   {};
    xradian     m_Roll  {};
};

//------------------------------------------------------------------------------
// Description:
//     This class represents a 2D vector. 
//
//<P>  The class is not hardware accelerated and works only with floats. There
//     is not need for special aligments except for the atomic types.
// See Also:
//     xvector3 xvector4
//------------------------------------------------------------------------------
class xvector2 
{
    x_object_type( xvector2, is_linear );

public:

    constexpr                               xvector2                        ( void ) = default;
    constexpr                               xvector2                        ( f32 X, f32 Y ) : m_X(X), m_Y(Y) {}
    constexpr                               xvector2                        ( f32 n ) : m_X(n), m_Y(n) {}

    inline              xvector2&           setup                           ( f32 X, f32 Y );
    inline              void                setZero                         ( void );
                                            
    constexpr           f32                 Dot                             ( const xvector2& V ) const;
    constexpr           xvector2            getLerp                         ( f32 t, const xvector2& V1 ) const;
    inline              f32                 getLength                       ( void ) const;
    constexpr           f32                 getLengthSquared                ( void ) const;
    inline              xradian             getAngle                        ( void ) const;
    inline              xradian             getAngleBetween                 ( const xvector2& V ) const;
    inline              f32                 getDistance                     ( const xvector2& V ) const;
    constexpr           bool                isInrange                       ( f32 Min, f32 Max ) const;
    x_forceinline       bool                isValid                         ( void ) const;                                            
    inline              xvector2&           Rotate                          ( xradian Angle );        
    inline              xvector2&           Normalize                       ( void );
    inline              xvector2&           NormalizeSafe                   ( void );

    constexpr           f32                 getWhichSideOfLine              ( const xvector2& V0, const xvector2& V1 ) const;
    inline              xvector2            getClosestPointInLine           ( const xvector2& V0, const xvector2& V1 ) const;
    inline              xvector2            getClosestPointInLineSegment    ( const xvector2& V0, const xvector2& V1 ) const;

    constexpr           bool                operator ==                     ( const xvector2& V ) const;
    inline              const xvector2&     operator +=                     ( const xvector2& V );
    inline              const xvector2&     operator -=                     ( const xvector2& V );
    inline              const xvector2&     operator *=                     ( const xvector2& V );
    inline              const xvector2&     operator *=                     ( f32 Scalar );
    inline              const xvector2&     operator /=                     ( f32 Scalar );  

    friend constexpr    xvector2            operator +                      ( const xvector2& V1, const xvector2& V2 );
    friend constexpr    xvector2            operator -                      ( const xvector2& V1 );
    friend constexpr    xvector2            operator -                      ( const xvector2& V1, const xvector2& V2 );
    friend inline       xvector2            operator /                      ( const xvector2& V,        f32       S  );
    friend constexpr    xvector2            operator *                      ( const xvector2& V,        f32       S  );
    friend constexpr    xvector2            operator *                      ( const xvector2& V1, const xvector2& V2 );
    friend constexpr    xvector2            operator *                      (       f32       S,  const xvector2& V  );

public:

    f32     m_X {};
    f32     m_Y {};                                                      
};                                          

//------------------------------------------------------------------------------
// Description:
//     This class represents a 3D vector. 
//
//<P>  xvector3d is not a hardware accelerated class and works only with floats. There
//     is not need for special aligments thought except for the atomic types. it also
//     doesn't waste memoty like its speedy child xvector3. Only use this class for
//     memory related issues.
//
//<P>  xvector3 is meant for the common case, speed and not memory considerations. 
//     For that it contains four elements stead of the 3 that are actually needed. 
//     The class has special aligment requirements (16byte) Which allow to have an 
//     extra bust for speed. The class uses any hardware feature it can find to speed 
//     it self up.
//
// Example:
//<CODE>
//      xvector3 DoSomething( const xvector3d& V )
//      {
//          xvector3 G;
//          G = V * 0.5f;
//          G = G.GetMax( V );
//          return G.Cross( V );
//      }
//</CODE>
//      
// See Also:
//     xvector3 xvector3d xvector2 xvector4
//------------------------------------------------------------------------------
class xvector3d
{
    x_object_type( xvector3d, is_linear );

public:

    constexpr                               xvector3d                       ( void ) = default;
    constexpr                               xvector3d                       ( f32 X, f32 Y, f32 Z ) : m_X(X), m_Y(Y), m_Z(Z) {} 
                                            xvector3d                       ( xradian Pitch, xradian Yaw );
    constexpr explicit                      xvector3d                       ( const f32x4& Register );
    constexpr                               xvector3d                       ( const f32 n ) : m_X(n), m_Y(n), m_Z(n) {}
    constexpr                               xvector3d                       ( const xvector3& V );

    // Exceptions to the Get/Set rule
    constexpr           xvector3            getLerp                         ( f32 t, const xvector3d& V ) const;
    constexpr           f32                 Dot                             ( const xvector3d& V ) const;
    constexpr           xvector3            Cross                           ( const xvector3d& V ) const;

    // Normal functions
    inline              xvector3d&          setup                           ( f32 X, f32 Y, f32 Z );
    inline              xvector3d&          setup                           ( const f32x4& Register );        
    inline              xvector3d&          setup                           ( const f32 n );
                                
    inline              void                setIdentity                        ( void );
    inline              void                setZero                            ( void );
    x_forceinline       bool                isValid                         ( void ) const;
    constexpr           bool                isInrange                       ( const f32 Min, const f32 Max ) const;

    constexpr           xvector3            getOneOver                      ( void ) const;
    inline              f32                 getLength                       ( void ) const;
    constexpr           f32                 getLengthSquared                ( void ) const;
    constexpr           f32                 getDistanceSquare               ( const xvector3d& V ) const;
    inline              f32                 getDistance                     ( const xvector3d& V ) const;
    inline              xvector3            getMax                          ( const xvector3d& V ) const;
    inline              xvector3            getMin                          ( const xvector3d& V ) const;
    inline              xvector3d&          Abs                             ( void );
    inline              xvector3d&          Normalize                       ( void );
    inline              xvector3d&          NormalizeSafe                   ( void );
                                
    inline              f32*                operator ()                     ( void );
    inline              f32&                operator []                     ( const s32 i );
    constexpr           f32                 operator []                     ( const s32 i ) const;
    constexpr           bool                operator ==                     ( const xvector3d& V ) const;
    inline              const xvector3d&    operator +=                     ( const xvector3d& V );
    inline              const xvector3d&    operator -=                     ( const xvector3d& V );
    inline              const xvector3d&    operator *=                     ( const xvector3d& V );
    inline              const xvector3d&    operator /=                     ( f32 Div );
    inline              const xvector3d&    operator *=                     ( f32 Scalar );
                                    
    inline    friend    xvector3            operator /                      ( const xvector3d& V, f32 Div );
    constexpr friend    xvector3            operator *                      ( f32 Scale, const xvector3d& V );
    constexpr friend    xvector3            operator *                      ( const xvector3d& V, f32 Scale );
    constexpr friend    xvector3            operator -                      ( const xvector3d& V );
    constexpr friend    xvector3            operator *                      ( const xvector3d& V0, const xvector3d& V1 );
    constexpr friend    xvector3            operator -                      ( const xvector3d& V0, const xvector3d& V1 );
    constexpr friend    xvector3            operator +                      ( const xvector3d& V0, const xvector3d& V1 );
    constexpr friend    xvector3            operator /                      ( const xvector3d& V0, const xvector3d& V1 );

public:

    f32     m_X {};
    f32     m_Y {};
    f32     m_Z {};
};

//==============================================================================
// VECTOR3
//==============================================================================

// <COPY xvector3d>
class alignas(f32x4) xvector3 
{
    x_object_type( xvector3, is_linear );

public:

    constexpr                               xvector3                        ( void ) = default;
    constexpr                               xvector3                        ( f32 X, f32 Y, f32 Z ) : m_XYZW{ X, Y, Z, 1 } {}
    constexpr                               xvector3                        ( const xvector3d&  V ) : m_XYZW{ V.m_X, V.m_Y, V.m_Z, 1 } {}
    constexpr                               xvector3                        ( const f32 n ) : m_XYZW{ n, n, n, 1 } {}
    inline                                  xvector3                        ( xradian Pitch, xradian Yaw );
    constexpr explicit                      xvector3                        ( const f32x4& Register ) : m_XYZW( Register ) {}

    // Exceptions to the Get/Set rule
    inline              f32                 Dot                             ( const xvector3& V ) const;
    inline              xvector3            Cross                           ( const xvector3& V ) const;

    // Setup functions
    inline              xvector3&           setup                           ( f32 X, f32 Y, f32 Z );
    inline              xvector3&           setup                           ( const f32x4& Register );
    inline              xvector3&           setup                           ( const f32 n );
    inline              xvector3&           setup                           ( xradian Pitch, xradian Yaw );

    inline              void                setIdentity                     ( void );
    inline              void                setZero                         ( void );
    inline              xvector3&           Normalize                       ( void );
    inline              xvector3&           NormalizeSafe                   ( void );
    inline              xvector3&           RotateX                         ( xradian Rx );           
    inline              xvector3&           RotateY                         ( xradian Ry );           
    inline              xvector3&           RotateZ                         ( xradian Rz ); 
    inline              xvector3&           Rotate                          ( const xradian3& R );
    inline              xvector3&           GridSnap                        ( f32 GridX, f32 GridY, f32 GridZ );


    inline              bool                isInrange                       ( const f32 Min, const f32 Max ) const;
    x_forceinline       bool                isValid                         ( void ) const;
    constexpr           bool                isRightHanded                   ( const xvector3& P1, const xvector3& P2 ) const;
                                
    inline              xvector3            getReflection                   ( const xvector3& V ) const;
    inline              xvector3            getOneOver                      ( void ) const;
    inline              f32                 getLength                       ( void ) const;
    inline              f32                 getLengthSquared                ( void ) const;
    inline              f32                 getDistanceSquare               ( const xvector3& V ) const;
    inline              f32                 getDistance                     ( const xvector3& V ) const;
    inline              xvector3            getMax                          ( const xvector3& V ) const;
    inline              xvector3            getMin                          ( const xvector3& V ) const;
    inline              xvector3            getAbs                          ( void ) const;
    inline              xvector3            getLerp                         ( const f32 T, const xvector3& End ) const;
    inline              xradian             getPitch                        ( void ) const;
    inline              xradian             getYaw                          ( void ) const;
    inline              void                getPitchYaw                     ( xradian& Pitch, xradian& Yaw ) const;
    inline              xradian             getAngleBetween                 ( const xvector3& V ) const;
    inline              void                getRotationTowards              ( const xvector3&   DestV,
                                                                              xvector3&         RotAxis,
                                                                              xradian&          RotAngle ) const; 
    inline              xvector3            getVectorToLineSegment          ( const xvector3&   Start, const xvector3& End ) const;
    inline              xvector3            getClosestPointInLineSegment    ( const xvector3&   Start, const xvector3& End ) const;
    inline              f32                 getSquareDistToLineSeg          ( const xvector3&   Start, const xvector3& End ) const;
    inline              f32		            getClosestPointToRectangle      ( const xvector3&   P0,   // Origin from the edges.
                                                                              const xvector3&   E0,
                                                                              const xvector3&   E1,
                                                                              xvector3&         OutClosestPoint ) const;
                                                                         
    constexpr static    xvector3            Up                              ( void ) { return { 0, 1, 0}; }
    constexpr static    xvector3            Forward                         ( void ) { return { 0, 0, 1}; }
    constexpr static    xvector3            Right                           ( void ) { return { 1, 0, 0}; }
    constexpr static    xvector3            Down                            ( void ) { return { 0,-1, 0}; }
    constexpr static    xvector3            Back                            ( void ) { return { 0, 0,-1}; }
    constexpr static    xvector3            Left                            ( void ) { return {-1, 0, 0}; }
    
    inline              f32*                operator ()                     ( void );
    inline              f32&                operator []                     ( const s32 i );
    constexpr           f32                 operator []                     ( const s32 i ) const;
    constexpr           bool                operator ==                     ( const xvector3& V ) const;
    inline              const xvector3&     operator +=                     ( const xvector3& V );
    inline              const xvector3&     operator -=                     ( const xvector3& V );
    inline              const xvector3&     operator *=                     ( const xvector3& V );
    inline              const xvector3&     operator /=                     ( f32 Div );
    inline              const xvector3&     operator *=                     ( f32 Scalar );
                                        
    inline friend       xvector3            operator /                      ( const xvector3& V, f32 Div );
    inline friend       xvector3            operator *                      ( f32 Scale, const xvector3& V );
    inline friend       xvector3            operator *                      ( const xvector3& V, f32 Scale );
    inline friend       xvector3            operator +                      ( f32 Scale, const xvector3& V );
    inline friend       xvector3            operator +                      ( const xvector3& V, f32 Scale );
    inline friend       xvector3            operator -                      ( f32 Scale, const xvector3& V );
    inline friend       xvector3            operator -                      ( const xvector3& V, f32 Scale );
    
    inline friend       xvector3            operator -                      ( const xvector3& V );
    inline friend       xvector3            operator *                      ( const xvector3& V0, const xvector3& V1 );
    inline friend       xvector3            operator -                      ( const xvector3& V0, const xvector3& V1 );
    inline friend       xvector3            operator +                      ( const xvector3& V0, const xvector3& V1 );
    inline friend       xvector3            operator /                      ( const xvector3& V0, const xvector3& V1 );

    inline friend       xvector3            operator +                      ( const xvector3d& V0, const xvector3&  V1 );
    inline friend       xvector3            operator +                      ( const xvector3&  V0, const xvector3d& V1 );
    inline friend       xvector3            operator -                      ( const xvector3d& V0, const xvector3&  V1 );
    inline friend       xvector3            operator -                      ( const xvector3&  V0, const xvector3d& V1 );
    inline friend       xvector3            operator *                      ( const xvector3d& V0, const xvector3&  V1 );
    inline friend       xvector3            operator *                      ( const xvector3&  V0, const xvector3d& V1 );
    inline friend       xvector3            operator /                      ( const xvector3d& V0, const xvector3&  V1 );
    inline friend       xvector3            operator /                      ( const xvector3&  V0, const xvector3d& V1 );
    
public:

    union
    {
        f32x4       m_XYZW  {};
        struct
        {
            f32     m_X;
            f32     m_Y;
            f32     m_Z;
            f32     m_W;
        };
    };
};

//------------------------------------------------------------------------------
// Description:
//     This class represents a 4D vector. 
//
//<P>  This class is hardware accelerated and requieres a 16 byte aligment.
// See Also:
//     xvector3 xvector3d xvector2
//------------------------------------------------------------------------------
class alignas(f32x4) xvector4 
{
    x_object_type( xvector4, is_linear );

public:

    constexpr                               xvector4                        ( void ) = default;
    constexpr                               xvector4                        ( const f32 x ) : m_XYZW { x,x,x,1 } {}
    constexpr                               xvector4                        ( const f32 X, const f32 Y, const f32 Z, const f32 W=1 ) : m_XYZW { X,Y,Z,W } {}
    constexpr                               xvector4                        ( const xvector3& V, const f32 W=1 ) : m_XYZW { V.m_X,V.m_Y,V.m_Z, W } {}
    constexpr explicit                      xvector4                        ( const f32x4& Register ) : m_XYZW( Register ) {}

    inline              xvector4&           setup                           ( const f32 X, const f32 Y, const f32 Z, const f32 W=0 );

    inline              void                setZero                            ( void );
    inline              void                setIdentity                     ( void );
    inline              f32                 getLength                       ( void ) const;
    inline              f32                 getLengthSquared                ( void ) const;
    inline              f32                 Dot                             ( const xvector4& V ) const;
    inline              bool                isValid                         ( void ) const;

    inline              const xvector4&     operator +=                     ( const xvector4& V );
    inline              const xvector4&     operator -=                     ( const xvector4& V );
    inline              const xvector4&     operator *=                     ( const xvector4& V );
    inline              const xvector4&     operator *=                     ( const f32 Scalar );
    inline              const xvector4&     operator /=                     ( const f32 Scalar );

    inline              xvector4&           Normalize                       ( void );
    inline              xvector4&           NormalizeSafe                   ( void );
    inline              xvector4&           Abs                             ( void );
    inline              bool                isInrange                       ( const f32 Min, const f32 Max ) const;
    inline              xvector4            getMax                          ( const xvector4& V ) const;
    inline              xvector4            getMin                          ( const xvector4& V ) const;
    inline              xvector4&           Homogeneous                     ( void );
    inline              xvector4            OneOver                         ( void ) const;
    constexpr           bool                operator ==                     ( const xvector4& V ) const;

    inline friend       xvector4            operator +                      ( const xvector4& V1, const xvector4& V2 );
    inline friend       xvector4            operator -                      ( const xvector4& V1, const xvector4& V2 );
    inline friend       xvector4            operator *                      ( const xvector4& V1, const xvector4& V2 );
    inline friend       xvector4            operator -                      ( const xvector4& V );
    inline friend       xvector4            operator /                      ( const xvector4& V,        f32       S  );
    inline friend       xvector4            operator *                      ( const xvector4& V,        f32       S  );
    inline friend       xvector4            operator *                      (       f32       S,  const xvector4& V  );
    inline friend       xvector4            operator +                      ( const xvector4& V,        f32       S  );
    inline friend       xvector4            operator +                      (       f32       S,  const xvector4& V  );
    inline friend       xvector4            operator -                      ( const xvector4& V,        f32       S  );
    inline friend       xvector4            operator -                      (       f32       S,  const xvector4& V  );

public:

    union
    {
        f32x4       m_XYZW  {};
        struct
        {
            f32     m_X;
            f32     m_Y;
            f32     m_Z;
            f32     m_W;
        };
    };
};

//==============================================================================
// MATRIX 4
//==============================================================================

//------------------------------------------------------------------------------
// Description:
//     This class represents a Column Major 4x4 matrix.
//     With vectors treated as column matrices.
//
//<P>  This class is hardware accelerated and requieres a 16 byte aligment.
// See Also:
//     xvector3 xvector3d xvector2
//------------------------------------------------------------------------------
class alignas(f32x4) xmatrix4
{
    x_object_type( xmatrix4, is_linear );

public:

    constexpr                                   xmatrix4                ( void ) = default;
    constexpr                                   xmatrix4                ( const f32x4& C1, const f32x4& C2, const f32x4& C3, const f32x4& C4 ) :
                                                                            m_Column1( C1 ), m_Column2( C2 ), m_Column3( C3 ), m_Column4( C4 ) {}

    inline                                      xmatrix4                ( const xradian3& R );
    inline                                      xmatrix4                ( const xquaternion& Q );

                                                                        // Note, because +Z is backwards in a conventional RH
                                                                        // system, the third vector here is the "back" vector
                                                                        // of your local frame.
    inline                                      xmatrix4                ( const xvector3& Right, const xvector3& Up,
                                                                          const xvector3& Back,  const xvector3& Translation );

    inline                                      xmatrix4                ( f32 m11, f32 m12, f32 m13, f32 m14,
                                                                          f32 m21, f32 m22, f32 m23, f32 m24,
                                                                          f32 m31, f32 m32, f32 m33, f32 m34,
                                                                          f32 m41, f32 m42, f32 m43, f32 m44 );

    constexpr                                   xmatrix4                ( f32 Cell[4][4] ) : m_Cell{    {Cell[0][0],Cell[1][0],Cell[2][0],Cell[3][0]},
                                                                                                        {Cell[0][1],Cell[1][1],Cell[2][1],Cell[3][1]},
                                                                                                        {Cell[0][2],Cell[1][2],Cell[2][2],Cell[3][2]},
                                                                                                        {Cell[0][3],Cell[1][3],Cell[2][3],Cell[3][3]} } {}

    inline                                      xmatrix4                ( const xvector3d&   Scale,
                                                                          const xradian3&    Rotation,
                                                                          const xvector3d&   Translation );

    inline                                      xmatrix4                ( const xvector3&    Scale,
                                                                          const xquaternion& Rotation,
                                                                          const xvector3&    Translation );

    constexpr static    const xmatrix4&         Identity                ( void );
    inline              void                    setIdentity             ( void );
    inline              void                    setZero                 ( void );
    inline              bool                    isIdentity              ( void ) const;
    inline              bool                    isValid                 ( void ) const;
    inline              void                    SanityCheck             ( void ) const;
    inline              xmatrix4&               setup                   ( f32 m11, f32 m12, f32 m13, f32 m14,
                                                                          f32 m21, f32 m22, f32 m23, f32 m24,
                                                                          f32 m31, f32 m32, f32 m33, f32 m34,
                                                                          f32 m41, f32 m42, f32 m43, f32 m44 );
    inline              xmatrix4&               setTranslation          ( const xvector3& Translation );
    inline              xmatrix4&               setRotation             ( const xradian3& R );
    inline              xmatrix4&               setRotation             ( const xquaternion& Q );
    inline              xmatrix4&               setScale                ( f32 Scale );
    inline              xmatrix4&               setScale                ( const xvector3& Scale );
                                                 
    inline              xmatrix4&               Rotate                  ( const xradian3& R );
    inline              xmatrix4&               RotateX                 ( xradian Rx );
    inline              xmatrix4&               RotateY                 ( xradian Ry );
    inline              xmatrix4&               RotateZ                 ( xradian Rz );
    inline              xmatrix4&               PreRotateX              ( xradian Rx );
    inline              xmatrix4&               PreRotateY              ( xradian Ry );
    inline              xmatrix4&               PreRotateZ              ( xradian Rz );
    inline              xmatrix4&               PreRotate               ( const xquaternion& Rotation );
    inline              xmatrix4&               PreRotate               ( const xradian3& Rotation );
    inline              xradian3                getRotationR3           ( void ) const;
    inline              xmatrix4&               ClearRotation           ( void );
                                                 
    inline              xmatrix4&               Translate               ( const xvector3& Translation );
    inline              xmatrix4&               PreTranslate            ( const xvector3& Translation );
    inline              xvector3                getTranslation          ( void ) const;
    inline              xmatrix4&               ClearTranslation        ( void );
                                                 
    inline              xmatrix4&               Scale                   ( const xvector3d& Scale );
    inline              xmatrix4&               Scale                   ( f32 Scale );
    inline              xmatrix4&               PreScale                ( f32 Scale );
    inline              xmatrix4&               PreScale                ( const xvector3& Scale );
    inline              xvector3                getScale                ( void ) const;
    inline              xmatrix4&               ClearScale              ( void );

    inline              xmatrix4&               FullInvert              ( void );
    inline              xmatrix4&               InvertSRT               ( void );
    inline              xmatrix4&               InvertRT                ( void );

    inline              xvector3                RotateVector            ( const xvector3& V ) const;
    inline              xvector3                InvRotateVector         ( const xvector3& V ) const;
    inline              xvector3                getForward              ( void ) const;
    constexpr           xvector3                getBack                 ( void ) const;
    constexpr           xvector3                getUp                   ( void ) const;
    inline              xvector3                getDown                 ( void ) const;
    inline              xvector3                getLeft                 ( void ) const;
    constexpr           xvector3                getRight                ( void ) const;

    inline              xmatrix4&               Transpose               ( void );
    inline              f32                     Determinant             ( void ) const;
    inline              xmatrix4&               Orthogonalize           ( void );

    inline              xvector3                Transform3D             ( const xvector3& V ) const;
    inline              xvector4                Transform3D             ( const xvector4& V ) const;

    inline              xmatrix4&               LookAt                  ( const xvector3& From, const xvector3& To, const xvector3& Up );
    inline              xmatrix4&               Billboard               ( const xvector3& From, const xvector3& To, const xvector3& Up );
    inline              xmatrix4&               OrthographicProjection  ( f32 Width, f32 Height, f32 Near, f32 Far );
    inline              xmatrix4&               OrthographicProjection  ( f32 Left, f32 Right, f32 Bottom, f32 Top, f32 Near, f32 Far );
    inline              xmatrix4&               PerspectiveProjection   ( f32 Left, f32 Right, f32 Bottom, f32 Top, f32 Near, f32 Far);

    inline              xmatrix4&               setup                   ( const xvector3& Axis, xradian Angle );
    inline              xmatrix4&               setup                   ( const xvector3& From, const xvector3& To, xradian Angle );
    inline              xmatrix4&               setup                   ( const xradian3& Rotation );
    inline              xmatrix4&               setup                   ( const xquaternion& Q );
    inline              xmatrix4&               setup                   ( const xvector3&    Scale,
                                                                          const xradian3&    Rotation,
                                                                          const xvector3&    Translation );
    inline              xmatrix4&               setup                   ( const xvector3&    Scale,
                                                                          const xquaternion& Rotation,
                                                                          const xvector3&    Translation );
    inline              xvector3                getEulerZYZ             ( void ) const;
    inline              xmatrix4&               setupFromZYZ            ( const xvector3d& ZYZ );

    inline              xmatrix4                getAdjoint              ( void ) const;

    inline              f32                     operator ()             ( const s32 i, const s32 j ) const;
    inline              f32&                    operator ()             ( const s32 i, const s32 j );
    inline              const xmatrix4&         operator +=             ( const xmatrix4& M );
    inline              const xmatrix4&         operator -=             ( const xmatrix4& M );
    inline              const xmatrix4&         operator *=             ( const xmatrix4& M );
    inline friend       xvector4                operator *              ( const xmatrix4& M,  const xvector4&  V  );
    inline friend       xvector3                operator *              ( const xmatrix4& M,  const xvector3d& V  );
    inline friend       xvector3                operator *              ( const xmatrix4& M,  const xvector3&  V  );
    inline friend       xvector2                operator *              ( const xmatrix4& M,  const xvector2&  V  );
    inline friend       xmatrix4                operator *              ( const xmatrix4& M1, const xmatrix4&  M2 );
    inline friend       xmatrix4                operator +              ( const xmatrix4& M1, const xmatrix4&  M2 );
    inline friend       xmatrix4                operator -              ( const xmatrix4& M1, const xmatrix4&  M2 );
    //
    // Missing functions
    //
    /*
    xvector3d         Transform           ( const xvector3d& V ) const;
    void            Transform           (       xvector3d* pDest, 
                                                const xvector3d* pSource, 
                                                s32      NVerts ) const;
    void            Transform           (       xvector4* pDest, 
                                                const xvector4* pSource, 
                                                s32      NVerts ) const;
    */

protected:

    f32     acof( s32 r0, s32 r1, s32 r2, s32 c0, s32 c1, s32 c2 ) const;

    union
    {
        struct
        {
            f32x4   m_Column1;
            f32x4   m_Column2;
            f32x4   m_Column3;
            f32x4   m_Column4;
        };

        // because the memory layout is column-major
        // the first index here is the column, not the row, and vice versa.
        // the (,) and Set operators transpose this for you automatically.
        f32     m_Cell[4][4]    {};
    };

    friend class xquaternion;
    friend void m4_Multiply( xmatrix4& dst, const xmatrix4& src2, const xmatrix4& src1 );
};

//------------------------------------------------------------------------------
// Description:
//     This class represents a quaternion. A quaternion is a rotation described as
//     a 4D vector. Where the vector loosely represents an Axis and an Angle.
//
//<P>  This class is hardware accelerated and requieres a 16 byte aligment.
// See Also:
//     xvector3 xvector3d xradian3 xradian xmatrix4
//------------------------------------------------------------------------------
class alignas(f32x4) xquaternion 
{
    x_object_type( xquaternion, is_linear );

public:

    constexpr                                   xquaternion             ( void ) = default;
    constexpr           explicit                xquaternion             ( const f32 X, const f32 Y, const f32 Z, const f32 W ) : m_XYZW{ X,Y,Z,W } {}
    inline              explicit                xquaternion             ( const f32x4& Register ) : m_XYZW{ Register } {}
    inline              explicit                xquaternion             ( const xradian3&    R );
    inline              explicit                xquaternion             ( const xvector3& Axis, xradian Angle );
    inline              explicit                xquaternion             ( const xmatrix4&    M );

    inline              bool                    isValid                 ( void ) const;
    inline              void                    setIdentity             ( void );
    inline              void                    setZero                 ( void );

    inline              xquaternion&            setup                   ( f32 X, f32 Y, f32 Z, f32 W );
    inline              xquaternion&            setup                   ( const xradian3& R );
    inline              xquaternion&            setup                   ( const xvector3& Axis, xradian Angle );
    inline              xquaternion&            setup                   ( const xvector3& StartDir, const xvector3& EndDir );
    inline              xquaternion&            setup                   ( const xmatrix4& M );

    inline              xquaternion&            setRotationX            ( xradian Rx );
    inline              xquaternion&            setRotationY            ( xradian Ry );
    inline              xquaternion&            setRotationZ            ( xradian Rz );

    inline              f32                     Dot                     ( const xquaternion& Q ) const;
    inline              xquaternion&            Normalize               ( void );
    inline              xquaternion&            NormalizeSafe           ( void );

    inline              xquaternion&            Conjugate               ( void );
    inline              xquaternion&            Invert                  ( void );
    inline              f32                     Length                  ( void ) const;
    inline              f32                     LengthSquared           ( void ) const;
    inline              xvector3                getAxis                 ( void ) const;
    inline              xradian                 getAngle                ( void ) const;
    inline              xradian3                getRotation             ( void ) const;
    inline              xvector3                getLookDirection        ( void ) const;
    inline              xquaternion             getDeltaRotation        ( const xquaternion& B ) const { return xquaternion(*this).Invert() * B; }

    inline              xquaternion&            RotateX                 ( xradian Rx );
    inline              xquaternion&            RotateY                 ( xradian Ry );
    inline              xquaternion&            RotateZ                 ( xradian Rz );
    inline              xquaternion&            PreRotateX              ( xradian Rx );
    inline              xquaternion&            PreRotateY              ( xradian Ry );
    inline              xquaternion&            PreRotateZ              ( xradian Rz );

    inline              xquaternion             BlendFast               ( f32 T, const xquaternion& End ) const;
    inline              xquaternion             BlendAccurate           ( f32 T, const xquaternion& End ) const;

    inline              xquaternion&            Ln                      ( void );
    inline              xquaternion&            Exp                     ( void );

    constexpr           bool                    operator ==             ( const xquaternion& Q ) const;
    
    inline              const xquaternion&      operator /=             ( f32 Scalar );
    inline              const xquaternion&      operator *=             ( f32 Scalar );
    inline              const xquaternion&      operator *=             ( const xquaternion& Q );
    inline              const xquaternion&      operator -=             ( const xquaternion& Q );
    inline              const xquaternion&      operator +=             ( const xquaternion& Q );

    inline friend       xquaternion             operator *              ( const xquaternion& Q1, const xquaternion& Q2 );
    inline friend       xquaternion             operator *              ( f32 Scalar, const xquaternion& Q );
    inline friend       xquaternion             operator *              ( const xquaternion& Q, f32 Scalar );
    inline friend       xvector3                operator *              ( const xquaternion& A, const xvector3d& V );
    inline friend       xquaternion             operator -              ( const xquaternion& Q );
    inline friend       xquaternion             operator -              ( const xquaternion& Q1, const xquaternion& Q2 );
    inline friend       xquaternion             operator +              ( const xquaternion& Q1, const xquaternion& Q2 );

public:

    union
    {
        f32x4       m_XYZW  {};
        struct 
        {
            f32     m_X;
            f32     m_Y;
            f32     m_Z;
            f32     m_W;
        };
    };
};

//------------------------------------------------------------------------------
// Description:
// See Also:
//     xvector3 xvector3d xmatrix4 xquaternion
//------------------------------------------------------------------------------
class xtransform 
{
    x_object_type( xtransform, is_linear );

public:


    inline void Blend( const xtransform& From, const f32 T, const xtransform& To )
    {
        m_Scale         = From.m_Scale + T*( To.m_Scale - From.m_Scale );
        m_Rotation      = From.m_Rotation.BlendAccurate( T, To.m_Rotation );
        m_Translation   = From.m_Translation + T*( To.m_Translation - From.m_Translation );
    }

    inline void Blend( const f32 T, const xtransform& To )
    {
        m_Scale         += T*( To.m_Scale - m_Scale );
        m_Rotation       = m_Rotation.BlendAccurate( T, To.m_Rotation );
        m_Translation   += T*( To.m_Translation - m_Translation );
    }

    inline void setIdentity( void )
    {
        m_Translation.setZero();
        m_Scale.setup( 1.0f, 1.0f, 1.0f );
        m_Rotation.setIdentity();
    }

    inline void getMatrix( xmatrix4& Matrix )
    {
        Matrix.setup( m_Scale, m_Rotation, m_Translation );
    }

    xvector3        m_Scale         {};
    xquaternion     m_Rotation      {};
    xvector3        m_Translation   {};
};

//------------------------------------------------------------------------------
// Description:
//     This class represents a plane. The class is describe by a normal that should
//     remain normalize and D which is the offset of the plane. The plane equation
//     A + B + C + D = 0 equates to m_Normal.m_X + m_Normal.m_Y + m_Normal.m_Z + m_D = 0
//
//<P>  This class is not hardware accelerated and doesn't requiere any special  
//     aligment. 
// See Also:
//     xvector3 xvector3d xmatrix4 
//------------------------------------------------------------------------------
class xplane 
{
    x_object_type( xplane, is_linear );

public:

                            xplane                  ( void ) = default;
                            xplane                  ( const xvector3& P1,
                                                      const xvector3& P2,
                                                      const xvector3& P3 );
                            xplane                  ( const xvector3d& Normal, f32 Distance );
                            xplane                  ( f32 A, f32 B, f32 C, f32 D );
                            xplane                  ( const xvector3d& Normal, const xvector3d& Point );                

    bool                    isValid                 ( void ) const;
    f32                     Dot                     ( const xvector3d& P ) const;
    f32                     getDistance             ( const xvector3d& P ) const;
    s32                     getWhichSide            ( const xvector3d& P ) const;
    void                    ComputeD                ( const xvector3d& P );

    inline void             setup                   ( const xvector3& P1,
                                                      const xvector3& P2,
                                                      const xvector3& P3 );
    inline void             setup                   ( const xvector3d& Normal, f32 Distance );
    inline void             setup                   ( const xvector3d& Normal, const xvector3d& Point );
    void                    setup                   ( f32 A, f32 B, f32 C, f32 D );
    bool                    IntersectLine           ( f32& T, 
                                                      const xvector3d& Start, 
                                                      const xvector3d& Direction ) const;
    bool                    IntersectLineSegment    ( f32&           t,
                                                      const xvector3d& P0, 
                                                      const xvector3d& P1 ) const;

    void                    getComponents           ( const xvector3d& V,
                                                            xvector3d& Parallel,
                                                            xvector3d& Perpendicular ) const; 
    void                    getOrthovectors         ( xvector3d& AxisA,
                                                      xvector3d& AxisB ) const;
    xvector3                getReflection           ( const xvector3& V ) const;

    bool                    ClipNGon                ( xvector3d* pDst, s32& NDstVerts, 
                                                      const xvector3d* pSrc, s32  NSrcVerts ) const;
    bool                    ClipNGon                ( xvector3* pDst, s32& NDstVerts, 
                                                      const xvector3* pSrc, s32  NSrcVerts ) const;

    xplane&                 operator -              ( void );
    friend xplane           operator *              ( const xmatrix4& M, const xplane& xplane );

public:

    xvector3d   m_Normal    {};
    f32         m_D         {};
};

//------------------------------------------------------------------------------
// Description:
//     This class represents three Dimensional Axis Aligned Bounding Box. 
//
//<P>  This class is not hardware accelerated and doesn't requiere any special  
//     aligment. 
// See Also:
//     xvector3 xvector3d xmatrix4 
//------------------------------------------------------------------------------
class xbbox 
{
    x_object_type( xbbox, is_linear );

public:
                            xbbox                   ( void );
                            xbbox                   ( const xvector3d& P1 );
                            xbbox                   ( const xvector3d& P1, const xvector3d& P2 );
                            xbbox                   ( const xvector3d* pVerts, const s32 nVerts );
                            xbbox                   ( const xvector3d& Center, const f32 Radius );

    inline xbbox&           setup                   ( const xvector3d& Center, const f32 Radius );
    inline xbbox&           setup                   ( const xvector3d& P1, const xvector3d& P2 ); 

    void                    setZero                    ( void );
    inline void             setIdentity                ( void );
    bool                    isValid                 ( void ) const;

    xvector3                getSize                 ( void ) const;
    xvector3                getCenter               ( void ) const;
    f32                     getRadius               ( void ) const;
    f32                     getRadiusSquared        ( void ) const;
    f32                     getSurfaceArea          ( void ) const;

    bool                    InRange                 ( f32 Min, f32 Max ) const;
    xbbox&                  Inflate                 ( const xvector3d& S );
    xbbox&                  Deflate                 ( const xvector3d& S );
    xbbox&                  Transform               ( const xmatrix4& M );

    inline bool             Intersect               ( const xvector3d&  Point ) const;
    bool                    Intersect               ( const xbbox&      BBox  ) const;
    bool                    Intersect               ( const xplane&     Plane ) const;
    bool                    Intersect               ( f32&              t,
                                                      const xvector3d&  P0, 
                                                      const xvector3d&  P1 ) const;
    bool                    Intersect               ( const xvector3d* pVert,
                                                      s32              nVerts ) const;
    bool                    IntersectTriangleBBox   ( const xvector3d& P0,
                                                      const xvector3d& P1,
                                                      const xvector3d& P2 )  const;

    f32                     getClosestVertex        ( xvector3d& ClosestPoint, const xvector3d& Point ) const;

    void                    getVerts                ( xvector3d* pVerts, s32 nVerts ) const;
    bool                    Contains                ( const xbbox&     BBox  ) const;
    xbbox&                  AddVerts                ( const xvector3d* pVerts, s32 NVerts );
    xbbox&                  AddVerts                ( const xvector3*  pVerts, s32 NVerts );
    xbbox&                  Translate               ( const xvector3d& Delta );

    inline const xbbox&     operator +=             ( const xbbox&     BBox  );
    inline const xbbox&     operator +=             ( const xvector3d& Point );

    friend  xbbox           operator +              ( const xbbox&     xbbox1, const xbbox&    xbbox2 );
    friend  xbbox           operator +              ( const xbbox&     xbbox,  const xvector3d& Point );
    friend  xbbox           operator +              ( const xvector3d& Point,  const xbbox&    xbbox  );

public:

    xvector3d m_Min {};
    xvector3d m_Max {};
};

//------------------------------------------------------------------------------
// Description:
//     This class represents a three Dimensional Sphere. 
//
//<P>  This class is not hardware accelerated and doesn't requiere any special  
//     aligment. 
// See Also:
//     xvector3 xvector3d xmatrix4 xbbox
//------------------------------------------------------------------------------
class xsphere 
{
    x_object_type( xsphere, is_linear );

public:
                            xsphere                 ( void ) = default;
                            xsphere                 ( const xvector3d& Pos, const f32 R );
                            xsphere                 ( const xbbox& BBox );

    void                    setZero                    ( void );

    void                    setup                   ( const xvector3d& Pos, const f32 R );
    xbbox                   getBBox                 ( void ) const;

    bool                    TestIntersect           ( const xvector3d& P0, const xvector3d& P1 ) const;
    s32                     TestIntersection        ( const xplane& Plane ) const;

    s32                     Intersect               ( f32& t0, f32& t1, const xvector3d& P0, const xvector3d& P1 ) const;
    bool                    Intersect               ( f32& t0, const xvector3d& P0, const xvector3d& P1 ) const;
    bool                    Intersect               ( const xbbox& BBox ) const;
    bool                    Intersect               ( const xsphere& xsphere ) const;
    bool                    Intersect               ( f32&             t0,        
                                                      f32&             t1,        
                                                      const xvector3d& Vel1,       
                                                      const xsphere&   Sph2,      
                                                      const xvector3d& Vel2 );
public:

    xvector3d   m_Pos   {};
    f32         m_R     {};
};

//------------------------------------------------------------------------------
// Description:
//     This class represents a 2D floating point base rectangle. 
//------------------------------------------------------------------------------
class xrect 
{
    x_object_type( xrect, is_linear );

public:
                            xrect                   ( void ) = default;
                            xrect                   ( f32 l, f32 t, f32 r, f32 b );
    
    void                    setMax                  ( void );
    void                    setZero                    ( void );
    inline xrect&           setup                   ( f32 l, f32 t, f32 r, f32 b );
    xrect&                  setup                   ( f32 X, f32 Y, f32 Size );

    bool                    Intersect               ( const xrect& Rect );
    bool                    Intersect               ( xrect& R, const xrect& Rect );

    bool                    PointInRect             ( const xvector2& Pos ) const;
    bool                    PointInRect             ( f32 X, f32 Y ) const;
    xrect&                  AddPoint                ( f32 X, f32 Y );
    xrect&                  AddRect                 ( const xrect& Rect  );

    f32                     getWidth                ( void ) const;
    f32                     getHeight               ( void ) const;
    xvector2                getSize                 ( void ) const;
    xvector2                getCenter               ( void ) const;

    xrect&                  setWidth                ( f32 W );
    xrect&                  setHeight               ( f32 H );
    xrect&                  setSize                 ( f32 W, f32 H );

    xrect&                  Translate               ( f32 X, f32 Y );
    xrect&                  Inflate                 ( f32 X, f32 Y );
    xrect&                  Deflate                 ( f32 X, f32 Y );

    f32                     Difference              ( const xrect& R ) const;
    bool                    InRange                 ( f32 Min, f32 Max ) const;
    bool                    isEmpty                 ( void ) const;

    xrect                   Interpolate             ( f32 T, const xrect& Rect ) const;
    
    bool                    operator ==             ( const xrect& R ) const;
    bool                    operator !=             ( const xrect& R ) const;

public:

    f32     m_Left      {};
    f32     m_Top       {};
    f32     m_Right     {};
    f32     m_Bottom    {};
};

//------------------------------------------------------------------------------
// Description:
//     This class represents a 2D floating point base rectangle.
//------------------------------------------------------------------------------
class xrectwh 
{
    x_object_type( xrectwh, is_linear );

public:

    xvector2    m_Pos   {};
    f32         m_W     {};
    f32         m_H     {};
};

//------------------------------------------------------------------------------
// Description:
//     This class represents a 2D integer base rectangle. 
//------------------------------------------------------------------------------
class xirect
{
    x_object_type( xirect, is_linear );

public:
                            xirect                  ( void ) = default;
                            xirect                  ( s32 l, s32 t, s32 r, s32 b );

    void                    setMax                  ( void );
    void                    setZero                    ( void );
    inline xirect&          setup                   ( s32 l, s32 t, s32 r, s32 b );

    bool                    Intersect               ( const xirect& Rect );
    bool                    Intersect               ( xirect& R, const xirect& Rect );

    bool                    PointInRect             ( s32 X, s32 Y ) const;
    xirect&                 AddPoint                ( s32 X, s32 Y );
    xirect&                 AddRect                 ( const xirect& Rect  );

    s32                     getWidth                ( void ) const;
    s32                     getHeight               ( void ) const;
    xvector2                getSize                 ( void ) const;
    xvector2                getCenter               ( void ) const;

    xirect&                 setWidth                ( s32 W );
    xirect&                 setHeight               ( s32 H );
    xirect&                 setSize                 ( s32 W, s32 H );

    xirect&                 Translate               ( s32 X, s32 Y );
    xirect&                 Inflate                 ( s32 X, s32 Y );
    xirect&                 Deflate                 ( s32 X, s32 Y );

    f32                     Difference              ( const xirect& R ) const;
    bool                    InRange                 ( s32 Min, s32 Max ) const;
    bool                    isEmpty                 ( void ) const;

    bool                    operator ==             ( const xirect& R ) const;
    bool                    operator !=             ( const xirect& R ) const;

public:

    s32 m_Left      {};
    s32 m_Top       {};
    s32 m_Right     {};
    s32 m_Bottom    {};
};


