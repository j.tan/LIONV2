//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

// Based on:
// http://www.gamedev.net/page/resources/_/technical/general-programming/using-varadic-templates-for-a-signals-and-slots-implementation-in-c-r3782
//
// Usage Example:
//      #include <iostream>
//      class body
//      {
//      public:
//           void UpdateDelegates( void ) {  m_MyEvent(*this); }        // Call all delegates for this event type
//           x_message::event<body&> m_MyEvent {};                      // Define an event type that we will emmit from this class 
//           
//           int m_IHave = 7;
//      };
//      
//      class other
//      {
//      public: 
//                  other      ( body& Body )     { m_MyEventDelegate.Connect( Body.m_MyEvent ); }
//          void    HandleEvent( body& Body )     { std::cout << Body.m_IHave; }                        // Handle my messages here
//          x_message::delegate<other,body&> m_MyEventDelegate   { *this, &other::HandleEvent };        // Define my event reciver
//      };
//      
//      int main( void )
//      {
//          body    TestBody;
//          other   TestOther{ TestBody };
//          
//          TestBody.UpdateDelegates();
//          
//          return 0;    
//      }

namespace x_message
{
    template< typename... T_ARGS > class event;

    //--------------------------------------------------------------------------------
    template< typename... T_ARGS > 
    class abstract_delegate 
    {
        x_object_type( abstract_delegate, is_linear )

    protected:
        using event_type = event<T_ARGS...>;

    protected:
        constexpr               abstract_delegate   ( void ) = default;
        constexpr               abstract_delegate   ( event_type& EventType ) : m_pEventType{ &EventType } {}
        inline      void        Attach              ( event_type& S ) { x_assert( m_pEventType == nullptr  ); m_pEventType = &S;                                    }
        inline      void        Dettach             ( event_type& S ) { x_assert( m_pEventType == &S       ); m_pEventType = nullptr;  m_pNext = m_pPrev = nullptr; }
        virtual     void        Call                ( T_ARGS... Args ) const = 0;

    protected:
        event_type*                     m_pEventType    { nullptr };
        abstract_delegate<T_ARGS...>*   m_pNext         { nullptr };
        abstract_delegate<T_ARGS...>*   m_pPrev         { nullptr };

    protected:

        friend class event<T_ARGS...>;
    };
    
    //--------------------------------------------------------------------------------
    template< typename T_CLASS, typename... T_ARGS > 
    class concrete_delegate : public abstract_delegate<T_ARGS...>
    {
    protected:

        using       callback    = void(T_CLASS::*)(T_ARGS...); 
        using       event_type  = event<T_ARGS...>;
        using       t_parent    = abstract_delegate<T_ARGS...>;

    protected:

        constexpr               concrete_delegate   ( T_CLASS& Class, callback pFunction ) : 
                                    m_Class         { Class },
                                    m_pFunction     { pFunction }
                                    {}

        inline                  concrete_delegate   ( T_CLASS& Class, callback pFunction, event_type& Event );
        virtual     void        Call                ( T_ARGS... Args ) const override { (m_Class.*m_pFunction)(Args...); }

                                concrete_delegate   ( const concrete_delegate& )    = delete;
                    void        operator =          ( const concrete_delegate& )    = delete;

    protected:

        T_CLASS&        m_Class;
        callback        m_pFunction;

    protected:
        friend class event<T_ARGS...>;
    };

    //--------------------------------------------------------------------------------
    template< class... T_ARGS > 
    class event final 
    {
        x_object_type( event, is_linear )

    public:

        using base_delegate_concreate = abstract_delegate<T_ARGS...>;

    public:

                                event               ( void ) = default;
        inline                 ~event               ( void )                                { for( auto pDelegate = m_pHead; pDelegate; ) { auto p = pDelegate; pDelegate = p->m_pNext; p->Dettach(*this); } m_pHead = nullptr;         }
        inline      void        Connect             ( base_delegate_concreate& S )          { Append(S); S.Attach(*this);   }
        inline      void        Disconnect          ( base_delegate_concreate& S )          { Remove(S); S.Dettach(*this);  }
        inline      void        Notify              ( T_ARGS... Args )                      { for( auto pDelegate = m_pHead; pDelegate; pDelegate = pDelegate->m_pNext ) pDelegate->Call( Args... );                                    }

    protected:
        inline      void        Append              ( base_delegate_concreate& S )          { S.m_pNext = m_pHead; if( m_pHead ) m_pHead->m_pPrev = &S; m_pHead = &S;                                                 }
        inline      void        Remove              ( base_delegate_concreate& S )          { if( S.m_pNext ) S.m_pNext->m_pPrev = S.m_pPrev; if(S.m_pPrev) S.m_pPrev->m_pNext = S.m_pNext; else m_pHead = S.m_pNext; }

                                event               ( const event& ) = delete;
                    void        operator =          ( const event& ) = delete;

    protected:
        base_delegate_concreate*  m_pHead { nullptr };

    protected:
        template< typename, typename... > friend class concrete_delegate;
    };

    //--------------------------------------------------------------------------------
    template< typename T_CLASS, typename... T_ARGS > inline 
    concrete_delegate<T_CLASS, T_ARGS...>::concrete_delegate( T_CLASS& Class, callback pFunction, event_type& Event ) : 
        abstract_delegate<T_ARGS...>{ Event },
        m_Class                     { Class },
        m_pFunction                 { pFunction }
    {
        Event.Append( *this );
    }

    //--------------------------------------------------------------------------------
    template< typename T_CLASS, typename... T_ARGS > 
    class delegate final : concrete_delegate< T_CLASS, T_ARGS...>
    {
    public:
        using t_parent = concrete_delegate< T_CLASS, T_ARGS...>;

    public:
        constexpr                   delegate            ( T_CLASS& Class, void(T_CLASS::*pFunction)( T_ARGS... ) ) : t_parent{ Class, pFunction } {}
        inline                      delegate            ( T_CLASS& Class, void(T_CLASS::*pFunction)( T_ARGS... ), event<T_ARGS...>& EventType ) : 
                                                            t_parent    { Class, pFunction, EventType } 
                                                            {}
        inline                     ~delegate            ( void )                        { if( t_parent::m_pEventType ) t_parent::m_pEventType->Disconnect( *this ); }
        inline      void            Connect             ( event<T_ARGS...>& EventType ) { EventType.Connect( *this );                           }

                                    delegate            ( const delegate& ) = delete;
                    void            operator =          ( const delegate& ) = delete;
    };
};

