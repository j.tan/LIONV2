//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


 #define x_memset   memset
 #define x_memcpy   memcpy
 #define x_memmove  memmove
 #define x_memcmp   memcmp

//------------------------------------------------------------------------------
// Summary:
//     Binary search of a sorted array, with where item should be in the 
//     list in case of a miss.
//------------------------------------------------------------------------------
x_inline u32     x_memCRC32  ( const xbuffer_view<xbyte> Buf, const u32 CRC = 0x00000000u ) noexcept;
x_inline u16     x_memCRC16  ( const xbuffer_view<xbyte> Buf, const u16 CRC = 0x0000u     ) noexcept;

//------------------------------------------------------------------------------
// Description:
//       Endian swap
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
template<typename T> x_forceconst 
T x_EndianSwap( const T& data, const int size = sizeof(T) ) noexcept
{
    static_assert( std::is_integral<T>::value, "Only atomic values such int floats, etc. allowed for swapping endians" );
    return size == 0 ? static_cast<T>(0) :
        ( (data&0xff) << (8 * (size - 1))) |
        x_EndianSwap<T>( data >> 8, size - 1 );
}

//------------------------------------------------------------------------------
template<> x_forceconst 
u8 x_EndianSwap<u8>( const u8& h, const int size ) noexcept
{
    return h;
}

//------------------------------------------------------------------------------
template<> x_forceconst 
s8 x_EndianSwap<s8>( const s8& h, const int size ) noexcept
{
    return h;
}

//------------------------------------------------------------------------------
template<> x_forceinline 
f32 x_EndianSwap<f32>( const f32& h, const int size ) noexcept
{
    using T             = f32;
    using base_type     = x_size_to_unsigned<sizeof(T)>::type;
    return *reinterpret_cast<const T*>( x_getAddressOfTemporary( x_EndianSwap( *reinterpret_cast<const base_type*>(&h), size) ) );
}

//------------------------------------------------------------------------------
template<> x_forceinline 
f64 x_EndianSwap<f64>( const f64& h, const int size ) noexcept
{
    using T             = f64;
    using base_type     = x_size_to_unsigned<sizeof(T)>::type;
    return *reinterpret_cast<const T*>( x_getAddressOfTemporary( x_EndianSwap( *reinterpret_cast<const base_type*>(&h), size) ) );
}

//------------------------------------------------------------------------------
x_forceconst 
bool x_isSystemLittleEndian( void ) noexcept
{
    return static_cast<const u8&>(0x01) == 0x01;
}

//------------------------------------------------------------------------------
x_forceconst 
bool x_isSystemEndianBig( void ) noexcept
{
    return !x_isSystemLittleEndian();
}

//------------------------------------------------------------------------------
template<typename T> x_forceconst
T x_EndianSystemToLittle( const T& data ) noexcept
{
    return x_isSystemLittleEndian() ? data : x_EndianSwap(data);
}

//------------------------------------------------------------------------------
template<typename T> x_forceconst
T x_EndianSystemToBig( const T& data ) noexcept
{
    return x_isSystemLittleEndian() ? x_EndianSwap(data) : data;
}

//------------------------------------------------------------------------------
template<typename T> x_forceconst
T x_EndianBigToSystem( const T& data ) noexcept
{
    return x_isSystemLittleEndian() ? x_EndianSwap(data) : data;
}

//------------------------------------------------------------------------------
template<typename T> x_forceconst
T x_EndianLittleToSystem( const T& data ) noexcept
{
    return x_isSystemLittleEndian() ? data : x_EndianSwap(data);
}

//------------------------------------------------------------------------------
#if _X_TARGET_LITTLE_ENDIAN
    static_assert( x_isSystemLittleEndian() == true, "" );
#elif  _X_TARGET_BIG_ENDIAN
    static_assert( x_isSystemLittleEndian() == false, "" );
#else
    #error "Not Endian defined"
#endif

static_assert( x_EndianSwap(s64(0xabcdefAA123456ff)) == s64(0xff563412AAefcdab), "" );
static_assert( x_EndianSwap(s32(0xabcd12ff))         == s32(0xff12cdab),         "" );
static_assert( x_EndianSwap(s16(0xabff))             == s16(0xffab),             "" );
static_assert( x_EndianSwap(s8(0xff))                == s8(0xff),                "" );

//------------------------------------------------------------------------------
// Description:
//       Compression functions
//------------------------------------------------------------------------------
x_incppfile xuptr   x_CompressMem   ( xbuffer_view<xbyte> DestinationCompress,                      const xbuffer_view<xbyte> SourceUncompress ) noexcept;
x_incppfile void    x_DecompressMem ( xbuffer_view<xbyte> DestinationUncompress,                    const xbuffer_view<xbyte> SourceCompressed ) noexcept;

