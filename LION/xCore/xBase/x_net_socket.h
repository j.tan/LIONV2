//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      The official log system for the scheduler
//------------------------------------------------------------------------------
#if _X_LOGGING
    #define X_NET_LOGGER(A) X_LOG_CHANNEL( g_context::get().m_Network_LogChannel, A )
#else
    #define X_NET_LOGGER(A) {}
#endif


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class xnet_socket
{
    x_object_type( xnet_socket, is_linear, is_not_movable, is_not_copyable )

public:

    enum errors : u8
    {
        ERR_OK,
        ERR_FAILURE,
        ERR_CREATE_SOCKET,
        ERR_ALRADY_INUSE,
        ERR_BINDING,
        ERR_NOREADY
    };

    using err = x_err<errors>;

public:

// Visual stuiod 2015 crashes in this line
//    constexpr                       xnet_socket             ( void )                                                    noexcept = default;
                                    xnet_socket             ( void )                                                    noexcept {};
                                   ~xnet_socket             ( void )                                                    noexcept;

    x_incppfile static  xnet_address::ip4 getLocalIP4       ( void )                                                    noexcept;
    x_incppfile static  xnet_address::ip6 getLocalIP6       ( void )                                                    noexcept;

    x_incppfile         err         openUDPSocket           ( xnet_address::port Port )                                 noexcept;
    x_incppfile         err         openTCPSocket           ( xnet_address::port Port )                                 noexcept;
    x_incppfile         err         AcceptTCPConnection     ( xnet_socket& NewSocket )                          const   noexcept;
    x_incppfile         err         ConnectTCPServer        ( xnet_address Address )                                    noexcept;
    x_incppfile         err         ReadFromSocket          (   xbuffer_view<xbyte> Buffer, 
                                                                s32&                Length, 
                                                                xnet_address&       FromAddr, 
                                                                bool                bBlock )                            noexcept;
    x_incppfile         void        closeSocket             ( void )                                                    noexcept;
    x_incppfile         err         SendPacket              ( xbuffer_view<xbyte> Buffer, xnet_address Address )        noexcept;
    x_incppfile         s32         AddRef                  ( void )                                                    noexcept { return ++m_RefCount; }
    x_incppfile         s32         DecRef                  ( void )                                                    noexcept { return --m_RefCount; }
    x_incppfile         auto        getPort                 ( void )                                                    noexcept { return m_Port;       }
    x_incppfile         bool        isTCP                   ( void )                                            const   noexcept { return m_bTCP;       }
    x_incppfile         u32         getLowLevelSocket       ( void )                                            const   noexcept { return m_Socket;     }

    x_incppfile         void        EnableLagSimulator      ( s32 PercentagePackageLost, s32 PercentageDuplicated, s32 PercentageHackedData, s32 PercentageLagNoise, s32 PercentageLagDistance ) noexcept;
    x_incppfile         void        DisableLagSimulator     ( void )                                                                                                                             noexcept;

protected:

    class xnetwork_tester
    {
    public:

        void EnableLagSimulator             (   s32 PercentagePackageLost, s32 PercentageDuplicated, s32 PercentageHackedData, 
                                                s32 PercentageLagNoise, s32 PercentageLagDistance ) noexcept;

        void DisableLagSimulator            ( void ) noexcept;
        void Update                         ( void ) noexcept;
        bool SendTo                         ( s32 Socket, const xbuffer_view<xbyte> View, s32 SomeThing, struct sockaddr* pAddr, s32 Size ) noexcept;
        bool isEnable                       ( void ) const noexcept { return m_bLagSimulatorOn; } 

    protected:

        struct lag_noise_backup
        {
            xarray<xbyte, 1024>         m_Data;
            double                      m_Wait;
            s32                         m_Lenth;
            xarray<xbyte,16>            m_Addr;
            s32                         m_Size;
            s32                         m_Socket;
        }; 

    protected:

        f64                          m_LagSimNextTime                   {};
        bool                         m_bLagSimulatorOn                  {false};
        s32                          m_PercentagePackageLost            {0};
        s32                          m_PercentageHackedData             {0};
        s32                          m_PercentageLagNoise               {0};
        s32                          m_PercentageLagDistance            {0};
        s32                          m_PercentagePacketDuplication      {0};
        xvector<lag_noise_backup>    m_BackupData                       {};
        xrandom_small                m_Random                           {};
        s32                          m_TotalPacketsSent                 {0};
        s32                          m_PacketLost                       {0};
        s32                          m_Currupted                        {0};
    };

protected:

    bool                        m_bTCP              { false };
    u32                         m_Socket            { ~0u };
    xnet_address::port          m_Port              { 0 };
    s32                         m_RefCount          {  0  };
    xndptr_s<xnetwork_tester>   m_NetworkTester     {}; 

    friend class xnet_udp_mesh;
};
