//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//------------------------------------------------------------------------------
// Description:
//      This is a nice general class that allows to protect a linear object
//------------------------------------------------------------------------------
template< typename T_CLASS, typename T_LOCK >
class x_locked_object : public T_LOCK
{
    x_object_type( x_locked_object, is_quantum_lock, is_not_copyable, is_not_movable )

public:
    
    using t_parent = T_LOCK;

public:
    
    static_assert( T_LOCK::t_is_lock, "T_LOCK is actually not a lock");
    
    constexpr                       x_locked_object     ( void )        noexcept = default;
    x_forceinline   T_CLASS&        getClass            ( void )        noexcept { x_assert( t_parent::isLocked() ); return m_Class; }
    x_forceinline   const T_CLASS&  getClass            ( void ) const  noexcept { x_assert( t_parent::isLocked() ); return m_Class; }

protected:

    T_CLASS m_Class {}; 
};

//------------------------------------------------------------------------------
// Description:
//      Lock guards to be use together with a x_locked_object
//------------------------------------------------------------------------------
#define x_lk_guard_get( LOCKED_CLASS_VARIABLE, VARIABLE )                   x_lk_guard( LOCKED_CLASS_VARIABLE );            auto&       VARIABLE = LOCKED_CLASS_VARIABLE.getClass();
#define x_lk_guard_get_as_const( LOCKED_CLASS_VARIABLE, VARIABLE )          x_lk_guard( LOCKED_CLASS_VARIABLE );            const auto& VARIABLE = LOCKED_CLASS_VARIABLE.getClass();
#define x_lk_guard_as_const_get_as_const( LOCKED_CLASS_VARIABLE, VARIABLE ) x_lk_guard_as_const( LOCKED_CLASS_VARIABLE );   const auto& VARIABLE = LOCKED_CLASS_VARIABLE.getClass();
#define x_lk_guard_as_const_get( LOCKED_CLASS_VARIABLE, VARIABLE )          x_lk_guard_as_const( LOCKED_CLASS_VARIABLE );   auto&       VARIABLE = LOCKED_CLASS_VARIABLE.getClass();

//------------------------------------------------------------------------------
// Description:
//      Creates a std::lock_guard for the user in a more compact form
//------------------------------------------------------------------------------
#define x_lk_guard(A)           const std::lock_guard<decltype(A)>       X_CREATE_UNIQUE_VARNAME(_UniqueVariable) { A }
#define x_lk_guard_as_const(A)  const std::lock_guard<const decltype(A)> X_CREATE_UNIQUE_VARNAME(_UniqueVariable) { const_cast<const decltype(A)&>(A) }

//------------------------------------------------------------------------------
// Description:
//      a reference iterator is used for lock based containers. 
//      Where you must get a reference first.
//------------------------------------------------------------------------------
template< class T >
struct x_lk_ref_iter
{
    x_object_type( x_lk_ref_iter, is_linear );

public:

    using t_entry =  typename T::t_entry; 

public:

                            x_lk_ref_iter   ( T& Ref ) : m_Ref(Ref){}
                            x_lk_ref_iter   ( s32 i, T& Ref ) : m_Ref(Ref){ m_Ref.getMutableIterator() = i; }
    bool                    operator!=      ( const x_lk_ref_iter& End ) const  { return m_Ref.getIterator() != End.m_Ref.getCount(); }
    const x_lk_ref_iter&    operator++      ( void )                            { ++m_Ref.getMutableIterator(); return *this; }
    const t_entry&          operator*       ( void ) const                      { return m_Ref[m_Ref.getIterator()]; }
    t_entry&                operator*       ( void )                            { return m_Ref[m_Ref.getIterator()]; }

protected:

    T&    m_Ref;
};

//------------------------------------------------------------------------------
// Description:
//      Spin lock with a bunch of options such:
//      * base          - is the actual base class to create the spinlock, then you have to compouse
//                        it with a reentrance type and a do type.
//      * reentrance    - when you want to have recursive or member functions to know you are safe.
//      * do_light_jobs - will do one light job per lock try
//
// Example:
//      using myspinlock =  x_lk_spinlock::base< x_lk_spinlock::reentrance, x_lk_spinlock::do_nothing >;
//      myspinlock MyLock;
//      ...
//      void SomeFunction( void ) { x_lk_guard( MyLock ); ... }
//------------------------------------------------------------------------------
namespace x_lk_spinlock
{
    //------------------------------------------------------------------------------
    // Description:
    //      A spin lock that is thread aware so recursion will work
    //------------------------------------------------------------------------------
    class reentrance
    {
        x_object_type( reentrance, is_quantum_lock  );

    protected:
        
        inline const std::thread::id t_getID( void ) const { return std::this_thread::get_id(); }
        
        inline void t_Unlock  ( x_atomic_flag& State ) const
        {
            x_assume( m_EnteringCount > 0 );
            --m_EnteringCount;
            if( m_EnteringCount == 0 )
            {
                m_FullThreadID = std::thread::id();
                State.clear(std::memory_order_release);
            }
        }

        inline bool t_TryLock( x_atomic_flag& State, const std::thread::id Id ) const
        {
            // Are we dealing with the same thread?
            if( m_FullThreadID == Id )
            {
                x_assume( Id != std::thread::id{} );
                x_assume( m_EnteringCount > 0 );
                m_EnteringCount++;
                return true;
            }

            // try to lock it officially
            if( !State.test_and_set( std::memory_order_acquire ) )
            {
                x_assume( Id              != std::thread::id{} );
                x_assume( m_FullThreadID  == std::thread::id{} );
                x_assume( m_EnteringCount == 0 );
                m_FullThreadID = Id;
                m_EnteringCount++;
                return true;
            }

            return false;
        }
        
    protected:
        
        mutable std::thread::id  m_FullThreadID     {};
        mutable int              m_EnteringCount    {0};
    };

    //------------------------------------------------------------------------------
    // Description:
    //      A spin lock that is not thread aware so recursion won't work
    //------------------------------------------------------------------------------
    class non_reentrance
    {
        x_object_type( non_reentrance, is_quantum_lock  );

    protected:
        
    #if _X_DEBUG
        inline const std::thread::id t_getID( void ) const { return std::this_thread::get_id(); }
    #else
        constexpr int t_getID(void) const { return 0; }
    #endif
        
        inline void t_Unlock     ( x_atomic_flag& State ) const
        {
            x_assert( m_Debug_Counter == 1 );
            X_DEBUG_CMD( m_Debug_Counter-- );
            X_DEBUG_CMD( m_Debug_FullThreadID = std::thread::id{} );
            State.clear(std::memory_order_release);
        }
        
    #if _X_DEBUG
        inline bool t_TryLock ( x_atomic_flag& State, const std::thread::id Id ) const
    #else
        inline bool t_TryLock ( x_atomic_flag& State, int ) const
    #endif
        {
            if( !State.test_and_set(std::memory_order_acquire) )
            {
                X_DEBUG_CMD( m_Debug_Counter++ );
                x_assert( m_Debug_Counter == 1 );
                X_DEBUG_CMD( m_Debug_FullThreadID = Id );
                return true;
            }
            else
            {
    #if _X_DEBUG
            //const std::thread::id Id = std::this_thread::get_id();
            //x_assert( Id == std::this_thread::get_id() );
            
            // if this assert hits you may want to change to the reentrance version
            // since you did try to relock again with the same thread.
            x_assert( m_Debug_FullThreadID != Id );
    #endif
            }
            return false;
        }
        
    protected:
    #if _X_DEBUG
        mutable int              m_Debug_Counter        {0};
        mutable std::thread::id  m_Debug_FullThreadID   {};
    #endif
    };

    //------------------------------------------------------------------------------
    // Description:
    //      Do work - does not do any work at all. This is the typical spinlock
    //------------------------------------------------------------------------------
    struct do_nothing
    {
        inline void t_Work ( void ) const {}
    };
    
    //------------------------------------------------------------------------------
    // Description:
    //      Do work - the type of work that will be done while waiting will be light jobs
    //------------------------------------------------------------------------------
    struct do_light_jobs
    {
        x_scheduler& m_S { g_context::get().m_Scheduler };
        inline void t_Work ( void ) const;
    };

    //------------------------------------------------------------------------------
    // Description:
    //      Base class for a spin lock
    //------------------------------------------------------------------------------
    template < typename T_REENTRANCE, typename T_DOWORK = do_nothing >
    class base : public T_REENTRANCE
    {
    public:
        enum : bool { t_is_lock = true };
        
    public:
        inline void lock       ( void ) const
        {
            const auto Id = T_REENTRANCE::t_getID();
            
            if( T_REENTRANCE::t_TryLock(m_State, Id) == false )
            {
                T_DOWORK DoWork;
                do
                {
                    DoWork.t_Work();
                } while( T_REENTRANCE::t_TryLock(m_State, Id) == false );
            }
        }
        
        inline void unlock     ( void ) const
        {
            T_REENTRANCE::t_Unlock( m_State );
        }
        
        inline bool trylock    ( void ) const
        {
            const auto Id = T_REENTRANCE::t_getID();
            return T_REENTRANCE::t_TryLock( m_State, Id );
        }

        inline  bool isLocked( void ) const   noexcept    { return m_State.isLocked(); }
       
    protected:
        
        mutable x_atomic_flag m_State            { false };
    };
};

//------------------------------------------------------------------------------
// Description:
//      a useless lock for used as default parameters
//------------------------------------------------------------------------------
class x_lk_useless_lock 
{
    x_object_type( x_lk_useless_lock, is_quantum_lock );

public:
    constexpr void lock   ( void ) const       {  }
    constexpr void unlock ( void ) const       {  }
};

//------------------------------------------------------------------------------
// Description:
//      A General semaphore
//------------------------------------------------------------------------------
template< typename T_DOWORK = x_lk_spinlock::do_nothing >
class x_lk_semaphore_smart_lock 
{
    x_object_type( x_lk_semaphore_smart_lock, is_quantum_lock );

public:

    // Use "xptr2_flags" to get access to the flags from outside the class
    // default or 0 is: MULTI_READER | MULTI_WRITTER | NOT_QT_ACCESS
    X_DEFBITS(  flags,
                u8,
                0x00000000,
                X_DEFBITS_ARG ( bool,      READ_ONLY,          1 ),        // Tells if this bitmap is a cubemap 
                X_DEFBITS_ARG ( bool,      QT_READABLE,        1 ),        // if the xbitmap is allowed to free the memory
                X_DEFBITS_ARG ( bool,      QT_MUTABLE,         1 ),        // it tells that the picture has alpha information
                X_DEFBITS_ARG ( bool,      SINGLE_WRITTER,     1 ),        // Tells it the alpha has already been premultiplied
                X_DEFBITS_ARG ( bool,      SINGLE_READER,      1 ),        // What is the color space for the bitmap
                X_DEFBITS_ARG ( bool,      MULTI_READER,       1 ),        // Tells if it can wrap around U
                X_DEFBITS_ARG ( bool,      MULTI_WRITTER,      1 ),        // Tells if it can wrap around V
                X_DEFBITS_ARG ( bool,      NOT_QT_ACCESS,      1 )         // Tells if it can mirror wrap around U
    );

    enum : u8
    {
        MODE_XREADERS_1WRITTER          = 0  
                                        | flags::MASK_QT_READABLE      
                                        | flags::MASK_MULTI_READER      
                                        | flags::MASK_SINGLE_WRITTER   
                                        | flags::MASK_QT_MUTABLE,

        MODE_XREADERS_XWRITTERS         = 0 
                                        | flags::MASK_QT_READABLE      
                                        | flags::MASK_MULTI_READER      
                                        | flags::MASK_QT_MUTABLE,

        MODE_XREADERS_0WRITTERS         = 0 
                                        | flags::MASK_QT_READABLE      
                                        | flags::MASK_MULTI_READER      
                                        | flags::MASK_READ_ONLY,

        MODE_0READERS_1WRITTER          = 0 
                                        | flags::MASK_SINGLE_WRITTER      
                                        | flags::MASK_NOT_QT_ACCESS,
    };
public:

    constexpr   bool    isMutable           ( bool bAssert )    const   noexcept    { return true; }
    constexpr   bool    isReadable          ( bool bAssert )    const   noexcept    { return true; }
    inline      void    RefReader           ( bool bInc    )    const   noexcept;
    inline      void    RefMutate           ( bool bInc    )    const   noexcept;
    inline      void    ChangeBehavior      ( flags Flags  )    const   noexcept;

    inline      void    lock                ( void )                    noexcept    { RefMutate( true  );  }
    inline      void    unlock              ( void )                    noexcept    { RefMutate( false );  }
    inline      void    lock                ( void )            const   noexcept    { RefReader( true  );  }
    inline      void    unlock              ( void )            const   noexcept    { RefReader( false );  }
    inline      bool    isLocked            ( void )            const   noexcept    { const auto A = m_Semapore.load(); return A.m_nWritters || A.m_nReaders; }
    inline      s32     getWritterCount     ( void )            const   noexcept    { return m_Semapore.load().m_nWritters; }
    inline      s32     getReaderCount      ( void )            const   noexcept    { return m_Semapore.load().m_nReaders;  }

protected:

    union semaphore
    {
        u64             m_Raw;
        
        constexpr semaphore( void ) noexcept  : m_Raw {0}{};
        constexpr semaphore( u64 X ) noexcept : m_Raw{ X }  {}

        struct 
        {
            u16         m_Counter;
            flags       m_Flags;
            u8          m_nReaders;
            u8          m_nWritters;
            u8          m_Pad[3];
        };
    };

protected:

    mutable x_atomic<semaphore>  m_Semapore  { static_cast<u64>(MODE_XREADERS_1WRITTER) << (8 * offsetof( semaphore, m_Flags )) };
};

//------------------------------------------------------------------------------
// Description:
//      This is a very more complex lock which is meant to be use for debug builds
//      and trust in release builds
//------------------------------------------------------------------------------
/*
#if 0 //_X_DEBUG

    #define x_lk_debug_smart_lock  x_lk_semaphore_smart_lock<>

#else
    class x_lk_debug_smart_lock
    {
        x_object_type( x_lk_debug_smart_lock, is_quantum_lock );

    public:

        using flags = x_lk_semaphore_smart_lock<>::flags; 

        constexpr   bool    isMutable           ( bool bAssert )    const   noexcept    { return true; }
        constexpr   bool    isReadable          ( bool bAssert )    const   noexcept    { return true; }
        inline      void    RefReader           ( bool bInc    )    const   noexcept    {}
        inline      void    RefMutate           ( bool bInc    )    const   noexcept    {}
        inline      void    ChangeBehavior      ( flags Flags  )    const   noexcept    {}

        inline      void    lock                ( void )                    noexcept    {}
        inline      void    unlock              ( void )                    noexcept    {}
        inline      void    lock                ( void )            const   noexcept    {}
        inline      void    unlock              ( void )            const   noexcept    {}
        inline      bool    isLocked            ( void )            const   noexcept    {}
        inline      s32     getWritterCount     ( void )            const   noexcept    {}
        inline      s32     getReaderCount      ( void )            const   noexcept    {}
    };

#endif
*/
