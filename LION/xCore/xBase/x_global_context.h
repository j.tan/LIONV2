//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//------------------------------------------------------------------------------
// Description:
//      Collects all the global variables to be held in memory
//------------------------------------------------------------------------------
class x_scheduler;
class xguid64_generation;
class x_string_block;
class pow2_small_allocs;
class xfile_device_i;
 
struct g_context
{
    xguid64_generation&    m_GuidGeneration;
    x_scheduler&            m_Scheduler;
    
                                    g_context           ( void );
                                   ~g_context           ( void );
   
public:
#if _X_LOGGING
    x_reporting::log_channel     m_Network_LogChannel       { "NETWORK" };
    x_reporting::log_channel     m_Scheduler_LogChannel     { "SCHEDULER" };
    x_reporting::log_channel     m_General_LogChannel       { "GENERAL" };
#endif
   
    

    static g_context&               get                 ( void ) noexcept               { x_assume(m_pInstance); return *g_context::m_pInstance;                                        }
    void*                           aligned_alloc       ( xuptr Size, xuptr Aligment );
    void                            aligned_free        ( void* pPtr );
    void*                           aligned_realloc     ( void* pPtr, xuptr Size, xuptr Aligment );
    static void                     Import              ( g_context& G )                { x_assume(m_pInstance == nullptr); m_bImported = true; m_pInstance = &G;                       }
    static void                     Init                ( void );
    static void                     Kill                ( void );

    enum class small_alloc_type
    {
        POW8,
        POW16,
        POW32,
        POW64,
        POW128,
        POW256,
        POW512,
        POW1024,
        POW2048,
        FULL_ALLOCATION,
        ENUM_COUNT
    };

    small_alloc_type                SmallAlloc              ( xbyte*& Ptr, units_bytes      Size );
    void                            SmallFree               ( xbyte*  Ptr, small_alloc_type Type );
    constexpr static units_bytes    getBytesFromAllocType   ( small_alloc_type Type )               { return units_bytes { 1 << (3 + static_cast<int>(Type)) }; }
    void                            SanityCheck             ( void );

protected:
    
            pow2_small_allocs&      m_SmallBlockPools;
            xfile_device_i*         m_pDevicelist { nullptr };
    static  g_context*              m_pInstance;
    static  bool                    m_bImported;

public:

    xfile_device_i*&                getFileSystemDevice     ( void ) { return m_pDevicelist; }

};