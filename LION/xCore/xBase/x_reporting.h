//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------
// Description:
//      Let the user know what reporting features are turn on
//------------------------------------------------------------------------------
#ifndef _X_LOGGING
    #define _X_LOGGING              true
#endif

#ifndef _X_STATS_METRICS
    #define _X_STATS_METRICS        true
#endif

namespace x_reporting
{
#if _X_LOGGING
    constexpr bool isLoggingOn( void )  { return true;      }
#else
    constexpr bool isLoggingOn( void )  { return false;     }
#endif
    
#if _X_STATS_METRICS
    constexpr bool isMetricsOn( void )  { return true;      }
#else
    constexpr bool isMetricsOn( void )  { return false;     }
#endif
};

//------------------------------------------------------------------------------
// Description:
//      Logging command an easy way to comment out base if lagging is turn on/off
//------------------------------------------------------------------------------
#if _X_LOGGING
    #define X_LOGGING_CMD(A) { A; }
#else
    #define X_LOGGING_CMD(A)
#endif

//------------------------------------------------------------------------------
// Description:
//      Simple LOG function that wont interleave with each other
//------------------------------------------------------------------------------
namespace x_reporting
{
    class log_channel 
    {
        x_object_type( log_channel, is_linear );

    public:

                            log_channel     ( void )                                noexcept = delete;
        x_inline            log_channel     ( const char* pChannelName )            noexcept : m_pChannel{ pChannelName } {}
        x_inline    void    TurnOn          ( void )                                noexcept { m_bPrint = true;     }
        x_inline    void    TurnOff         ( void )                                noexcept { m_bPrint = false;    }
        constexpr   bool    isPrint         ( void )                        const   noexcept { return m_bPrint;     }
        constexpr   auto&   getMutex        ( void )                        const   noexcept { return m_Mutex;      }
        constexpr   auto    getChannelName  ( void )                        const   noexcept { return m_pChannel;   }

    protected:

        mutable std::mutex      m_Mutex     {};
        const char*             m_pChannel;
        bool                    m_bPrint    { true };
    };
}

#define X_LOG_CHANNEL( CHANNEL, FMS )                               \
{ auto& __Channel = CHANNEL;                                        \
    if( __Channel.isPrint() ) {                                     \
        std::lock_guard<std::mutex> Lock( __Channel.getMutex() );   \
        std::cout << __Channel.getChannelName() << ": " << FMS << "\n";     \
    }                                                               \
}

        
//------------------------------------------------------------------------------
// Description:
//      Enable disable global logging
//------------------------------------------------------------------------------
#if _X_LOGGING
    #define X_LOG( FMS )    X_LOG_CHANNEL( g_context::get().m_General_LogChannel, FMS )
#else
    #define X_LOG( FMS ) {}
#endif

        
//------------------------------------------------------------------------------
// Description:
//      Stat tracking
//------------------------------------------------------------------------------
namespace x_reporting
{
    class stats_metric
    {
        x_object_type( stats_metric, is_linear );

    public:
#if _X_STATS_METRICS
        void inc( void ) { int n = ++m_Metric; for( int newMax = n; m_Max < newMax; m_Max.compare_exchange_strong( newMax, n ) ); }
        void dec( void ) { --m_Metric;      }
        int  get( void ) { return m_Metric; }
#else
        void inc( void ) {  }
        void dec( void ) {  }
        int  get( void ) { return -1; }
#endif
        
    protected:
#if _X_STATS_METRICS
        std::atomic<int>    m_Metric    { 0 };
        std::atomic<int>    m_Max       { 0 };
#endif
    };
}
