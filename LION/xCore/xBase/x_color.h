//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

 namespace x_color {};

//------------------------------------------------------------------------------
// Description:
//     This class represents a color. The color has only 8 bits of precision so
//     there is not HDR support at this point. It is very handy for converting 
//     amount different color spaces, or color layouts. This class also servers
//     as a helper to other classes such the xbitmap.
// See Also:
//     xbitmap
//------------------------------------------------------------------------------
class xcolor
{
    x_object_type( xcolor, is_linear );

public:

    // FORMAT_xxxx  (LOW BITS elements first then moving to HIGH BITS)
    enum format : u32
    {
        FMT_NULL,           

        FMT_16_ABGR_4444,
        FMT_16_ARGB_4444,   
        FMT_16_RGBA_4444,   
        FMT_16_RGB_565  ,
        FMT_16_BGR_565  ,
        FMT_16_ARGB_1555,  
        FMT_16_RGBA_5551,  
        FMT_16_URGB_1555,  
        FMT_16_RGBU_5551,   
        FMT_16_ABGR_1555,   
        FMT_16_UBGR_1555,

        FMT_24_RGB_888  ,     
        FMT_24_ARGB_8565,   

        FMT_32_RGBU_8888,   
        FMT_32_URGB_8888,  
        FMT_32_ARGB_8888,
        FMT_32_RGBA_8888,  FMT_XCOLOR = FMT_32_RGBA_8888,
        FMT_32_ABGR_8888,   
        FMT_32_BGRA_8888,       

        FMT_END,
    };

    struct fmt_desc
    {
        format      m_Format; 
        u32         m_FormatMask;     // One unique bit out of the 32
        s32         m_TB;             // Total Bits( 16, 24, 32 )
        s32         m_NUB;            // Number of Used Bits( 15, 16, 24, 32 )
        s32         m_AShift;        
        s32         m_RShift;        
        s32         m_GShift;        
        s32         m_BShift;        
        u32         m_AMask;         
        u32         m_RMask;         
        u32         m_GMask;         
        u32         m_BMask;         
    };

public:

    constexpr                           xcolor              ( void ) = default;
    constexpr                           xcolor              ( const u32 K ); 
                                        xcolor              ( const xvector3d& C );
                                        xcolor              ( const xvector4& C );
    constexpr                           xcolor              ( int R, int G, int B, int A );
    constexpr                           xcolor              ( u8  R, u8  G, u8  B, u8  A );
    constexpr                explicit   xcolor              ( f32 R, f32 G, f32 B, f32 A );

    inline          xcolor&             setup               ( u8  R, u8  G, u8  B, u8  A );
    inline          xcolor&             setup               ( const u32 K );
    inline          xcolor&             setupFromData       ( u32 RawData, format DataFormat );
    inline          xcolor&             setupFromYIQ        ( f32   Y, f32   I, f32   Q );
    inline          xcolor&             setupFromYUV        ( f32   Y, f32   U, f32   V );
    inline          xcolor&             setupFromCIE        ( f32   C, f32   I, f32   E );
    inline          xcolor&             setupFromRGB        ( xvector3d& Vector );
    inline          xcolor&             setupFromRGB        ( f32  aR, f32  aG, f32  aB );
    inline          xcolor&             setupFromRGBA       ( const xvector4& C );
    inline          xcolor&             setupFromRGBA       ( f32  aR, f32  aG, f32  aB, f32  aA );
    inline          xcolor&             setupFromCMY        ( f32   C, f32   M, f32   Y );
    inline          xcolor&             setupFromHSV        ( f32   H, f32   S, f32   V );
    inline          xcolor&             setupFromLight      ( const xvector3d& LightDir );
    inline          xcolor&             setupFromNormal     ( const xvector3d& LightDir );

    // Access the color in different forms
    inline          u8&                 operator []         ( int Index );
    constexpr                           operator u32        ( void ) const;
    constexpr static xcolor             getColorCategory    ( int Index ) { return g_ColorCategories[Index]; }

    // Math operators
    inline          const xcolor&       operator +=         ( xcolor C );
    inline          const xcolor&       operator -=         ( xcolor C );
    inline          const xcolor&       operator *=         ( xcolor C );
    constexpr       bool                operator ==         ( xcolor C ) const;
    constexpr       bool                operator !=         ( xcolor C ) const;

    // Color data conversions
    inline static   format              FindClosestFormat   ( u32 FormatMask, format Match );
    inline static   format              FindFormat          ( u32 AMask, u32 RMask, u32 GMask, u32 BMask  );
    inline          xcolor              ComputeNewAlpha     ( f32 Alpha ) const;
    inline          xcolor              PremultiplyAlpha    ( void ) const;

    // Color space conversions. 
    inline static   const fmt_desc&     getFormatDesc       ( format Dest )                                     { return g_FormatDesc[Dest]; }
    inline          xcolor              getBlendedColors    ( const xcolor Src1, const xcolor Src2, f32 T );
    inline          u32                 getDataFromColor    ( format DataFormat ) const;
    inline          void                getYIQ              ( f32&  Y, f32&  I, f32&  Q ) const;
    inline          void                getYUV              ( f32&  Y, f32&  U, f32&  V ) const;
    inline          void                getCIE              ( f32&  C, f32&  I, f32&  E ) const;
    inline          void                getRGB              ( f32& aR, f32& aG, f32& aB ) const;
    inline          xvector3            getRGB              ( void ) const;
    inline          void                getRGBA             ( f32& aR, f32& aG, f32& aB, f32& aA ) const;
    inline          xvector4            getRGBA             ( void ) const;
    inline          void                getCMY              ( f32&  C, f32&  M, f32&  Y ) const;
    inline          void                getHSV              ( f32&  H, f32&  S, f32&  V ) const;
    inline          xvector3            getLight            ( void ) const;
    inline          xvector3            getNormal           ( void ) const;

    
public:

    union
    {
        struct 
        { 
            u8      m_R;
            u8      m_G;
            u8      m_B;
            u8      m_A; 
        };

        u32         m_Value {};
    };

public:

    struct best_match
    {
        xarray<format,xcolor::FMT_END> m_Format;
    };

protected:

    static const xarray<best_match,FMT_END>         g_Match;
    static const xarray<xcolor::fmt_desc,FMT_END>   g_FormatDesc;
    static const xarray<xcolor,20>                  g_ColorCategories;
};
