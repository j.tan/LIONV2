//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace x_unit_test { namespace types 
{
    namespace rtti
    {
        struct one_a
        {
            x_object_type( one_a, rtti_start );
        };

        struct one_b
        {
            x_object_type( one_b, rtti_start );
        };

        struct two : public one_a
        {
            x_object_type( two, rtti( one_a ) );
        };

        struct c : public one_b, public two
        {
            x_object_type( c, rtti( one_b, two ) );
        };

        void Test( void )
        {
            c       C;
            one_a   Aa;
            one_b   Ab;
            two     Two;

            one_a* pA = &Two;

            Aa.SafeCast<one_a>();

            x_assert( Aa.isKindOf<one_a>(),     "" );
            x_assert( Ab.isKindOf<one_b>(),     "" );
            x_assert( C.isKindOf<c>(),          "" );
            x_assert( Two.isKindOf<two>(),      "" );
            x_assert( Two.isKindOf<one_a>(),    "" );

        //    x_assert( x_is_kindof2( Two, Ab ), "This is wrong" );

            x_assert( C.isKindOf<two>(),        "" );
            x_assert( C.isKindOf<one_b>(),      "" );
            x_assert( C.isKindOf<one_a>(),      "" );

            x_assert( pA->isKindOf<one_a>(),    "" );
            x_assert( pA->isKindOf<two>(),      "" );

            two& TwoCasted = pA->SafeCast<two>();

            one_a& OneCasted = pA->SafeCast<one_a>();
        //    x_assert( x_is_kindof2( (*pA), c ), "" );
        }

    }

    //----------------------------------------------------------------------------------------

    void Test( void )
    {
        rtti::Test();

    }

}}