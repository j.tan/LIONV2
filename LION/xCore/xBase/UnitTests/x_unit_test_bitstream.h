//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Basic functionality 
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

namespace x_unit_test { namespace bitstream 
{
    //------------------------------------------------------------------------------

     void TestInts( void )
     {
        xafptr<xbyte>   Data;
        s32             Length;
        xbitstream      BitStream;

        //
        // Test 32 bits flags
        //
        {
            u32 Full32bits      = 0xf2309abc;
            u32 Full32bitsTest  = 0;

            BitStream.setupPacking( 1024 );
            BitStream.Serialize( Full32bits, 32 );
            auto PackedDataView = BitStream.getResultOwner( Length );
            x_assert( Length == 4 );

            BitStream.setupUnpackingView( PackedDataView.ViewTo( Length ) );
            BitStream.Serialize( Full32bitsTest, 32 );

            x_assert( Full32bitsTest ==  Full32bits );
        }

        //
        // Test 64 bits flags
        //
        {
            u64 Full64bits      = 0xf2309abcf2309abc;
            u64 Full64bitsTest  = 0;

            BitStream.setupPacking( 1024 );
            BitStream.Serialize64( Full64bits, 64 );
            auto PackedDataView = BitStream.getResultOwner( Length );
            x_assert( Length ==  8 );

            BitStream.setupUnpackingView( PackedDataView.ViewTo( Length ) );
            BitStream.Serialize64( Full64bitsTest, 64 );

            x_assert( Full64bitsTest ==  Full64bits );
        }

        //
        // Test different ranges of flags
        //
        {
            const u64 Full64bits      = 0xf2309abcf2309abc;
            //u64 Full64bitsTest  = 0;

            BitStream.setupPacking( 1024 );
            for( s32 i=1; i<65; i++ )
            {
                if( i <= 32 ) 
                {
                    u32 Val = u32(Full64bits&((1<<i)-1));
                    BitStream.Serialize( Val, i );
                }
                else
                {
                    u64 Val = Full64bits&((u64(1)<<i)-1);
                    BitStream.Serialize64( Val, i );
                }
            }
            auto PackedDataView = BitStream.getResultOwner( Length );

            BitStream.setupUnpackingView( PackedDataView.ViewTo( Length ) );

            for( s32 i=1; i<65; i++ )
            {
                if( i <= 32 ) 
                {
                    u32 ValOriginal = u32(Full64bits&((1<<i)-1));
                    u32 Val;
                    BitStream.Serialize( Val, i );
                    x_assert( ValOriginal ==  Val );
                }
                else
                {
                    u64 ValOriginal = Full64bits&((u64(1)<<i)-1);
                    u64 Val;
                    BitStream.Serialize64( Val, i );
                    x_assert( ValOriginal ==  Val );
                }
            }
        }

        //
        // Test ranges of random values
        //
        {
            xrandom_small Rand;
            const s32 NumberOfValues = 100000;

            Rand.setSeed64( 0xf2309abcf2309abc );
            BitStream.setupPacking( 1024 );
            for( s32 i=0; i< NumberOfValues; i++ )
            {
                s64 Range = x_Abs((s64)(Rand.Rand32() | (((u64)Rand.Rand32())<<32)) )+1;
                s64 Val   = ((s32)Rand.Rand32() | (((u64)Rand.Rand32())<<32) )%Range;
                BitStream.Serialize64( Val, 0, Range );
            }
            auto PackedDataView = BitStream.getResultOwner( Length );

            BitStream.setupUnpackingView( PackedDataView.ViewTo( Length ) );

            Rand.setSeed64( 0xf2309abcf2309abc );
            for( s32 i=0; i< NumberOfValues; i++ )
            {
                s64 Range = x_Abs((s64)(Rand.Rand32() | (((u64)Rand.Rand32())<<32)) )+1;
                u64 ValOld = ((s32)Rand.Rand32() | (((u64)Rand.Rand32())<<32) )%Range;
                u64 NewVal;
                BitStream.Serialize64( NewVal, 0, Range );
                x_assert( NewVal ==  ValOld );
            }
        }

        //
        // Test  RLE
        //
        {
            const xbitmap&  Bitmap = xbitmap::getDefaultBitmap();
            xafptr<xbyte>   BitPacked;
            s32             Length;

            BitStream.setupPacking( 1024 );
            BitStream.SerializeOutRLE( Bitmap.getMip<xbyte>(0) );
            auto PackedDataView = BitStream.getResultOwner( Length );
        
            xafptr<xbyte> Buffer;
            Buffer.Alloc( Bitmap.getMipSize(0) );        

            BitStream.setupUnpackingView( PackedDataView.ViewTo( Length ) );
            BitStream.SerializeInRLE<xbyte>( Buffer );

            for( s32 i=0; i<Buffer.getCount(); i++ )
               x_assert( Buffer[i] == Bitmap.getMip<xbyte>(0)[i] );
        }

        int a=33;
     }

    //-----------------------------------------------------------------------------------------

    void Test( void )
    {
        TestInts();

    }
}}