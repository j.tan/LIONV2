//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Basic functionality 
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

namespace x_unit_test { namespace memory_ops 
{



    //-----------------------------------------------------------------------------------------

    void TestCompression( void )
    {
        const char* pSomething = "What were you doing Sunday, February 17, 2002 around 2PM? Well, some of us went down to"
        "Tower Records in Sherman Oaks and saw Something Corporate perform live at an in store for"
        "the EP, 'Audioboxer.' I recorded this footage when I was with MCA Records but unfortunately,"
        "I never was able to get it up on the SoCo site because I lost the tape. Well, I found it. Four"
        "years later. So, here it is, enjoy.";
        s32             InLength        = x_strlen( pSomething ).m_Value + 1;
        xndptr<xbyte>   Decompress;
        xndptr<xbyte>   CompressBlock;
        xuptr           Size;
    
        CompressBlock.New( InLength );
        CompressBlock.MemSet( 0xBE );
    
        Size = x_CompressMem( CompressBlock, xbuffer_view<xbyte>{(xbyte*)pSomething, x_static_cast<xuptr>(InLength) } );
    
        // Fill bytes that should be not used with trash
        for( auto i=Size; i<InLength; i++ )
        {
            CompressBlock[i] = 0xBE;
        }
    
        const f32 CompressionRatio = 100.0f*(1.0f - Size/(f32)InLength);
    
        x_assert( CompressionRatio );
        Decompress.New( InLength );
        Decompress.MemSet(0xAA);
    
        // Copy to another buffer to test in place decompression
        // NO INPLACE DECOMPRESSION FOR LZ4
        // s32 Offset = InLength - Size;
        // x_memcpy( &Decompress[ Offset ], &CompressBlock[0], Size );
    
        // Now we can decompress
        x_DecompressMem( Decompress, CompressBlock.ViewTo( Size ) );
    
        //
        // Compare both blocks
        //
        x_assert( x_memcmp( pSomething, &Decompress[0], InLength ) == 0 );
    
        //
        // One more test... what about data that can not be compress?
        //
        const char* pData = "12 345 6790 QSAEFG ";
    
        InLength = x_strlen( pData ).m_Value + 1;
        Size = x_CompressMem( CompressBlock, xbuffer_view<xbyte>{(xbyte*)pData, x_static_cast<xuptr>(InLength) } );
        x_assert( Size == InLength );
    
        // Fill bytes that should be not used with trash
        for( auto i=Size; i<CompressBlock.getCount(); i++ )
        {
            CompressBlock[i] = 0xBE;
        }

        // Now we can decompress
        x_DecompressMem( Decompress.ViewTo( InLength ), CompressBlock.ViewTo( Size ) );
        x_assert( x_memcmp( pData, &Decompress[0], InLength ) == 0 );
    }

    //-----------------------------------------------------------------------------------------

    void Test( void )
    {
        TestCompression();
    }
}}