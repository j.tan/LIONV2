//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

#include <random>

namespace x_unit_test { namespace qt_queue
{
    //---------------------------------------------------------------------------------
    // Test 1 - Simple linear space test
    //---------------------------------------------------------------------------------
    namespace spsc_queue_test_simple
    {
        struct mynode : public x_ll_tagptr<mynode>
        {
            using t_base = mynode;
            int m_i;
        };
        
        template< typename T_Q, xuptr T_COUNT >
        void Internal_Test( void )
        {
            enum                { COUNT     = T_COUNT   };
            T_Q                 MyQueue;
            xafptr<mynode>      NodeList;
            
            NodeList.Alloc(COUNT);
            for( int i=0; i<COUNT; i++) NodeList[i].m_i = i;
            
            for( int x = 0; x < 10000; x++ )
            {
                if( (x%1000) ==0 ) X_LOG("UNITESTING QUEUE spsc_queue_test_simple " << x << "\n");
                for( int i = 0; i < COUNT; i++ )
                {
                    MyQueue.push( NodeList[i] );
                }
            
                for( int i = 0; i < COUNT; i++ )
                {
                    CMD_DEBUG( auto* pNode = MyQueue.pop( ) );
                    x_assert( pNode );
                    x_assert( pNode->m_i == i );
                }
            }
        }
        
        template< template<typename,xuptr> class T_Q, xuptr T_COUNT >
        void Test( void ) { Internal_Test< T_Q<mynode,1024>, T_COUNT >(); }
        
        template< template<typename> class T_Q, xuptr T_COUNT >
        void Test( void ) { Internal_Test< T_Q<mynode>, T_COUNT>(); }
    };
    
    //---------------------------------------------------------------------------------
    // Test 2 - Simple Multi-core queue test
    //---------------------------------------------------------------------------------
    namespace qt_mpmc_queue_simple
    {
        struct mynode : public x_ll_tagptr<mynode>
        {
            using t_base = mynode;
        };
        
        template< typename T_Q, xuptr T_COUNT >
        void Internal_Test( void )
        {
            enum                { COUNT     = T_COUNT };
            enum                { FRACTION  = 24   };
            T_Q                 MyQueue;
            xafptr<mynode>      NodeList;
            std::atomic<int>    Count       { 0 };
            x_job_block         Block;
            
            NodeList.Alloc( COUNT );
            for( int x = 0; x < 10000; x++ )
            {
                if( (x%1000) ==0 ) X_LOG("UNITESTING QUEUE qt_mpmc_queue_simple " << x << "\n");
                for( int i = 0; i < (COUNT - FRACTION); i += FRACTION ) Block.SubmitJob( [i,&Count, &NodeList, &MyQueue]()
                {
                    for( int j=0; j<FRACTION; j++ )
                    {
                        MyQueue.push( NodeList[i+j] );
                        Count++;
                    }
                   
                } ); 
                Block.Join();
            
                for( int i = 0; i < (COUNT - FRACTION); i+=FRACTION )Block.SubmitJob( [&Count, &NodeList, &MyQueue]()
                {
                    for( int j = 0; j < FRACTION; j++ )
                    {
                        x_assert( MyQueue.pop( ) );
                        Count--;
                    }
                } ); 
                Block.Join();
            
                x_assert(Count==0);
            }
        }
        
        template< template<typename,xuptr> class T_Q, xuptr T_COUNT >
        void Test( void ) { Internal_Test< T_Q<mynode,1024>, T_COUNT >(); }
        
        template< template<typename> class T_Q, xuptr T_COUNT >
        void Test( void ) { Internal_Test< T_Q<mynode>, T_COUNT >(); }
    };
    
    //---------------------------------------------------------------------------------
    // Test2 - Heavy contention test
    //---------------------------------------------------------------------------------
    namespace qt_mcmp_queue_contention
    {
        struct mynode : public x_ll_tagptr<mynode>
        {
            using t_base = mynode;
        };
        

        enum
        {
            MAX_THREADS  = 4
        };

        template< typename T_Q, xuptr T_COUNT >
        void Internal_Test( void )
        {
            enum                { COUNT = T_COUNT };

            for( int x = 0; x < 10000; x++ )
            {
                xndptr<T_Q>         MyQueue;
                xafptr<mynode>      NodeList;
            
                NodeList.Alloc( 1024 );
                MyQueue.New(1);

                if( (x%1) ==0 ) X_LOG("UNITESTING QUEUE qt_mcmp_queue_contention " << x << "\n");
                std::atomic<int>    PopCount    { 0 };
                std::atomic<int>    PushCount   { 0 };
                std::atomic<int>    iTotal      { 0 };

                auto func = [&PushCount, &PopCount, &NodeList, &MyQueue, &iTotal]()
                {
                    std::random_device                  rd;
                    std::mt19937                        mt(rd());
                    std::uniform_int_distribution<int>  dist(0, 100 );
                     
                    for( s32 i = 0; i < COUNT/MAX_THREADS; i++ )
                    {
                        if( dist(mt)&1 )
                        {
                            if( MyQueue[0].pop() )
                            {
                                ///xuptr push = PushCount;
                                ///xuptr pop  = PopCount;
                                PopCount++;
                            //   assert( pop <= push );
                            }
                        }
                        else
                        {
                            PushCount++;
                            MyQueue[0].push( NodeList[iTotal++] );
                        }
                    }
                };

                xarray<std::thread,MAX_THREADS> Pool;
                for( auto& P : Pool )
                {
                    P  = std::thread( func );
                }
                for( auto& P : Pool ) P.join();
            
                x_assert( PopCount <= PushCount );
            
                PushCount -= PopCount;
                for( auto& P : Pool )
                {
                    P  = std::thread( 
                    [&PushCount, &NodeList, &MyQueue]()
                    {
                        while( PushCount.load() )
                        {
                            if( MyQueue[0].pop() )
                                PushCount--;
                            x_assert(PushCount >= 0);
                        }
                    });
                }
                for( auto& P : Pool ) P.join();
            
                x_assert(PushCount == 0);
            }
        }
        
        template< template<typename,xuptr> class T_Q, xuptr T_COUNT >
        void Test( void ) { Internal_Test< T_Q<mynode,1024>, T_COUNT >(); }
        
        template< template<typename> class T_Q, xuptr T_COUNT >
        void Test( void ) { Internal_Test< T_Q< mynode >, T_COUNT >(); }
    };
    
    template< template<typename, xuptr> class T_Q>
    void MCMPQueueTest( void )
    {
     //   spsc_queue_test_simple::Test<   T_Q,    xuptr(1000)>();
    //    qt_mpmc_queue_simple::Test<     T_Q,    xuptr(1000)>();
        qt_mcmp_queue_contention::Test< T_Q,    xuptr(1000)>();
    }
    
    template< template<typename> class T_Q  >
    void MCMPQueueTest( void )
    {
        spsc_queue_test_simple::Test<   T_Q,    1000>();
        qt_mpmc_queue_simple::Test<     T_Q,    1000>();
        qt_mcmp_queue_contention::Test< T_Q,    1000>();
        
        spsc_queue_test_simple::Test<   T_Q,    2000>();
        qt_mpmc_queue_simple::Test<     T_Q,    2000>();
        qt_mcmp_queue_contention::Test< T_Q,    2000>();
    }
    
    void Test( void )
    {
        if( (1) ) MCMPQueueTest<x_ll_mpmc_bounded_queue>();
        if( (0) ) MCMPQueueTest<x_ll_mpsc_node_queue>();
    }
}}

