//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace x_unit_test { namespace ll_circular_queue
{
    struct node
    {
        int             m_Counter;
        xarray<int, 64> m_Something;
    };

    //-------------------------------------------------------------------------------------------

    void Test( void )
    {
        x_ll_circular_pool      CircularQueue;
        x_job_block             Block;
        xarray<node*, 1000 >    Allocations {{ nullptr }};
        x_atomic<int>           Counter{ 0 };

        CircularQueue.Init( sizeof(node)*(Allocations.getCount() + Allocations.getCount()/8) );

        //
        // Allocate with the Linearly
        //
        Counter.store(0);
        for( int i = 0; i<Allocations.getCount(); i++ ) 
        {
            auto  Index = Counter++;
            auto& Ptr   = Allocations[Index];
            Ptr = &CircularQueue.Alloc<node>();

            Ptr->m_Counter = Index;
            for( int j=0; j<Ptr->m_Something.getCount(); j++ )
                Ptr->m_Something[j] = Index + j;
        }

        //
        // Free Nodes Linearly
        //
        Counter.store(0);
        for( int i = 0; i<Allocations.getCount(); i++ ) 
        {
            auto  Index = Counter++;
            CircularQueue.Free( Allocations[Index] );
        }

        //
        // Allocate with quantum mode
        //
        Counter.store(0);
        for( int i = 0; i<Allocations.getCount(); i++ )  Block.SubmitJob( [&](void) 
        {
            auto  Index = Counter++;
            auto& Ptr   = Allocations[Index];
            Ptr = &CircularQueue.Alloc<node>();

            Ptr->m_Counter = Index;
            for( int j=0; j<Ptr->m_Something.getCount(); j++ )
                Ptr->m_Something[j] = Index + j;
        });
        Block.Join();

        //
        // Free Nodes in quantum mode
        //
        Counter.store(0);
        for( int i = 0; i<Allocations.getCount(); i++ )  Block.SubmitJob( [&](void) 
        {
            auto  Index = Counter++;
            CircularQueue.Free( Allocations[Index] );
            Allocations[Index] = nullptr;
        });
        Block.Join();


        //
        // random allocs and frees
        //
        Counter.store(0);
        for( int i = 0; i<Allocations.getCount()*10; i++ )  Block.SubmitJob( [&](void) 
        {
            int   Index = ( Counter++ )%(100);
            auto& Ptr   = Allocations[Index];
            if( Ptr == nullptr )
            {
                Ptr = &CircularQueue.Alloc<node>();
            }
            else
            {
                CircularQueue.Free( Ptr );
                Ptr = nullptr;
            }
        });
        Block.Join();


        int a = 0;
    }

}};


