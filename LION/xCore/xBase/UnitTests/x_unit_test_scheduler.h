//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

namespace x_unit_test { namespace scheduler
{
    enum { LOOP_COUNT = 100000000 };

    //--------------------------------------------------------------------------------------
    // Test job 1 - basic jobs
    //--------------------------------------------------------------------------------------
    namespace job1
    {
        static std::atomic<int> s_Count;
        class count : public x_job<0>
        {
        public:
            count( void )
            {
                s_Count++;
            }
            
            void qt_onRun( void ) noexcept override 
            {
                for( int x = 0; x < 10; x++ )
                {
                    //  std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
                    X_SHEDULER_LOG( "WORKING IN BANANAS " << char( 'A' + x ) << '\n' );
                    for( int i = 0; i < LOOP_COUNT; i++ );
                }
            }
        };
        
        //--------------------------------------------------------------------------------------
        
        void Test( void )
        {
            X_SHEDULER_LOG( "----------------------------TEST1-----------------------------\n" );
            const int   Count = 8;
            
            for( int i = 0; i < Count; ++i )
            {
                auto* pJob = x_new( count );
                pJob->setupLifeTime( x_job_base::LIFETIME_DELETE_WHEN_DONE );
                g_context::get().m_Scheduler.AddJobToQuantumWorld( *pJob );
            }
            
            // Make the main help
            x_job_base* pJob;
            while( (pJob = g_context::get().m_Scheduler.getJob()) && s_Count )
            {
                g_context::get().m_Scheduler.ProcessWork( pJob );
            }
            
            X_SHEDULER_LOG( "----------------------------TEST1 DONE-----------------------------\n" );
        }
    };
    
    //--------------------------------------------------------------------------------------
    // Test job 2 - basic triggers
    //--------------------------------------------------------------------------------------
    namespace job2
    {
        class count : public x_job<1>
        {
            void qt_onRun( void ) noexcept override
            {
                for( int x = 0; x < 10; x++ )
                {
                    X_SHEDULER_LOG( "WORKING IN APPLES " << char( 'A' + x ) << '\n' );
                    for( int i = 0; i < LOOP_COUNT; i++ );
                }
            }
        };
        
        //--------------------------------------------------------------------------------------
        
        void Test( void )
        {
            X_SHEDULER_LOG( "----------------------------TEST2-----------------------------\n" );
            const int           Count = 4;
            count               Jobs1[Count];
            count               Jobs2[Count];
            x_trigger<4,count>  Trigger;
            x_trigger<4,count>  WaitingTrigger;
            
            WaitingTrigger.DontTriggerUntilReady();
            
            for( int i = 0; i < Count; ++i )
                Trigger.JobWillNotifyMe( Jobs1[i] );
            
            for( int i = 0; i < Count; ++i )
            {
                Trigger.AddJobToBeTrigger( Jobs2[i] );
                WaitingTrigger.JobWillNotifyMe( Jobs2[i] );
            }
            
            for( int i = 0; i < Count; ++i )
                g_context::get().m_Scheduler.AddJobToQuantumWorld( Jobs1[i] );
            
            // Make the main help
            WaitingTrigger.Join();
            X_SHEDULER_LOG( "----------------------------TEST2 DONE-----------------------------\n" );
        }
    };
    
    //--------------------------------------------------------------------------------------
    // Test job 3 - basic triggers with dynamic memory allocation
    //--------------------------------------------------------------------------------------
    namespace job3
    {
        class count : public x_job<1>
        {
        public:
            count( definition Def ) : x_job<1>( Def ) {}
            void qt_onRun( void ) noexcept override
            {
                for( int x = 0; x < 10; x++ )
                {
                    X_SHEDULER_LOG( "WORKING IN APPLES " << char( 'A' + x ) << '\n' );
                    for( int i = 0; i < LOOP_COUNT; i++ );
                }
            }
        };
        
        //--------------------------------------------------------------------------------------
        
        void Test( void )
        {
            X_LOG( "----------------------------TEST3-----------------------------\n" );
            using mytrigger = x_trigger<4,x_job_base>;
            const int       Count           = 4;
            mytrigger*      pTrigger        = x_new( mytrigger, { mytrigger::LIFETIME_DELETE_WHEN_DONE } );
            mytrigger*      pWaitingTrigger = x_new( mytrigger, { mytrigger::LIFETIME_DELETE_WHEN_DONE } );
            
            pWaitingTrigger->DontTriggerUntilReady();
            pTrigger->DontTriggerUntilReady();
            
            for( int i = 0; i < Count; ++i )
            {
                count* pJob2 = x_new( count, { count::definition::LIFETIMEToFlags( count::LIFETIME_DELETE_WHEN_DONE ) } );
                pTrigger->AddJobToBeTrigger( *pJob2 );
                pWaitingTrigger->JobWillNotifyMe( *pJob2 );
            }
            
            for( int i = 0; i < Count; ++i )
            {
                count* pJob1 = x_new( count, { count::definition::LIFETIMEToFlags( count::LIFETIME_DELETE_WHEN_DONE ) } );
                pTrigger->JobWillNotifyMe( *pJob1 );
                g_context::get().m_Scheduler.AddJobToQuantumWorld( *pJob1 );
            }
            
            pTrigger->DoTriggerWhenReady();
            
            // After this function returns all pointers may be gone
            pWaitingTrigger->Join();
            
            X_LOG( "----------------------------TEST3 DONE-----------------------------\n" );
        }
    };
    
    //--------------------------------------------------------------------------------------
    // Test job 4 - basic job block
    //--------------------------------------------------------------------------------------
    namespace job4
    {
        //--------------------------------------------------------------------------------------
        
        void Test( void )
        {
            X_LOG( "----------------------------TEST4-----------------------------\n" );
            const int       Count = 16;
            x_job_block     JobBlock;
            
            for( int i = 0; i < Count; ++i )
            {
                JobBlock.SubmitJob( []()
                {
                    for( int x = 0; x < 5; x++ )
                    {
                       X_SHEDULER_LOG( "WORKING IN BANANAS " << char( 'A' + x ) << '\n' );
                       for( int i = 0; i < LOOP_COUNT; i++ );
                    }
                } );
            }
            
            JobBlock.Join();
            X_LOG( "----------------------------TEST4 DONE----------------------------\n" );
        }
    };
    
    //--------------------------------------------------------------------------------------
    // Test job 5 - multiple job blocks
    //--------------------------------------------------------------------------------------
    namespace job5
    {
        //--------------------------------------------------------------------------------------
        
        void Test( void )
        {
            X_LOG( "----------------------------TEST5-----------------------------\n" );
            const int       Count = 16;
            x_job_block     JobBlock1;
            x_job_block     JobBlock2;
            
            // assumes this can work in concurrency with job APPLES but not with ORANGES are a dependency
            for( int i = 0; i < Count; ++i )
            {
                JobBlock1.SubmitJob( []()
                {
                    for( int x = 0; x < 5; x++ )
                    {
                        X_SHEDULER_LOG( "WORKING IN BANANAS " << char( 'A' + x ) << '\n' );
                        for( int i = 0; i < LOOP_COUNT; i++ );
                    }
                } );
            }
            
            // assumes this can work in concurrency with job BANANAS and ORANGES
            for( int i = 0; i < Count; ++i )
            {
                JobBlock2.SubmitJob( []()
                {
                    for( int x = 0; x < 5; x++ )
                    {
                        X_SHEDULER_LOG( "WORKING IN APPLES " << char( 'A' + x ) << '\n' );
                        for( int i = 0; i < LOOP_COUNT; i++ );
                    }
                } );
            }
            
            JobBlock1.Join();
            
            // assumes this can work in concurrency with job APPLES but not with BANANAS are a precondition
            for( int i = 0; i < Count; ++i )
            {
                JobBlock1.SubmitJob( []()
                {
                    for( int x = 0; x < 5; x++ )
                    {
                        X_SHEDULER_LOG( "WORKING IN ORANGES " << char( 'A' + x ) << '\n' );
                        for( int i = 0; i < LOOP_COUNT; i++ );
                    }
                } );
            }
            
            JobBlock1.Join();
            JobBlock2.Join();
            
            X_LOG( "----------------------------TEST5 DONE----------------------------\n" );
        }
    };
    
    void Test( void )
    {
        job1::Test();
        job2::Test();
        job3::Test();
        job4::Test();
        job5::Test();
    }
}}
