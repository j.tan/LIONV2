//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
// Basic functionality 
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------

namespace x_unit_test { namespace textfile 
{
    //------------------------------------------------------------------------------

    x_constexprvar f32           af  = -50.0f;
    x_constexprvar f32           bf  = -0.0001f;
    x_constexprvar f32           cf  = -50000.0f;
    x_constexprvar s32           ai  = -50;
    x_constexprvar char* const   pT1  = "String Test";
    x_constexprvar char* const   pT2  = "TEST1";
    x_constexprvar u64           llI = 0xffffffff88888888LL;
    x_constexprvar f64           FF  = PI_64.m_Value;


    //------------------------------------------------------------------------------
    static
    void TextFileWrite( const xwstring& FileName, xtextfile::access_type Flags )
    {
        xtextfile   TextFile;
    
        TextFile.openForWriting  ( FileName, Flags );
    
        TextFile.WriteRecord( "FistRecord", 512 );
        for( s32 i=0; i<512; i++ )
        {
            TextFile.WriteField ( "Description:s", pT1 );
            TextFile.WriteField ( "Position:fff", af+i, bf+i*2, cf+i*3 );
            TextFile.WriteField ( "Number:d", ai+i );
            TextFile.WriteField ( "EnumParam:e", pT2 );
            TextFile.WriteLine  ();
        }
    
        //
        // Big types
        //
        TextFile.WriteRecord( "SecondRecord" );
        TextFile.WriteField ( "pepe:DF", llI, FF );
        TextFile.WriteLine  ();
    
        //
        // Dynamic Type
        //
        TextFile.WriteRecord( "TheirdRecord", 3 );
    
        // 1
        TextFile.WriteField( "type:<?>", "fff" );
        TextFile.WriteField( "value:<type>", af, bf, cf );
        TextFile.WriteLine ();
    
        // 2
        TextFile.WriteField( "type:<?>", "s" );
        TextFile.WriteField( "value:<type>", pT1 );
        TextFile.WriteLine ();
    
        // 3
        TextFile.WriteField( "type:<?>", "DF" );
        TextFile.WriteField( "value:<type>", llI, FF );
        TextFile.WriteLine ();

        //
        // Lets test user types
        //
        TextFile.AddUserType( "fff", ".V3",   0 );
        TextFile.AddUserType( "fff", ".V3D",  1 );
        TextFile.AddUserType( "f",   ".F",    2 );
        TextFile.AddUserType( "sf",  ".SS",   3 );

        TextFile.WriteRecord( "UsersTypes", 2 );
    
        // 0
        TextFile.WriteField ( "mytype:<?>", ".V3" );
        TextFile.WriteField ( "myvalue:<mytype>", af, bf, cf );
        TextFile.WriteLine  ();
    
        // 1
        TextFile.WriteField ( "mytype:<?>", ".SS" );
        TextFile.WriteField ( "myvalue:<mytype>", pT1, af );
        TextFile.WriteLine  ();
    
        // Other record
        TextFile.WriteRecord( "UsersTypes2" );
        TextFile.WriteField ( "One:.F", af );
        TextFile.WriteField ( "Two:.V3D", af, bf, cf );
        TextFile.WriteLine  ();
    
        TextFile.close();
    }

    //------------------------------------------------------------------------------
    static
    void TextFileRead( const xwstring& FileName )
    {
        xtextfile   TextFile;
    
        TextFile.openForReading( FileName );
        x_verify( TextFile.ReadRecord() );
        for( s32 i=0; i<TextFile.getRecordCount(); i++ )
        {
            xstring String;
            xstring String2;
            f32 A, B, C;
            s32 I;
            TextFile.ReadLine();
            TextFile.ReadFieldXString ( "Description:s", String );
            TextFile.ReadField        ( "Position:fff", &A, &B, &C );
            TextFile.ReadField        ( "Number:d", &I );
            TextFile.ReadFieldXString ( "EnumParam:e", String2 );
        
            x_assert( String == pT1 );
            x_assert( x_FEqual( af+i, A ) );
            x_assert( x_FEqual( bf+i*2, B ) );
            x_assert( x_FEqual( cf+i*3, C ) );
            x_assert( ai+i == I );
            x_assert( String2 == pT2 );
        }
    
        //
        // Big types
        //
        u64  BigD;
        f64  BigF;
        f32  A, B, C;
        xstring String;
        xstring Typess;
        char Buff[256];
    
        x_verify( TextFile.ReadRecord() );
        TextFile.ReadLine();
        TextFile.ReadField ( "pepe:DF", &BigD, &BigF );
	    x_assert(BigD == (f64)llI);
	    x_assert(x_FEqual((const f32)BigF, (const f32)FF));

        //
        // Dynamic Type
        //
        x_verify( TextFile.ReadRecord() );
    
        // 1
        TextFile.ReadLine();
        TextFile.ReadField( "type:<?>", &Typess );
        TextFile.ReadField( "value:fff", &A, &B, &C );
        x_assert( x_FEqual( af, A ) );
        x_assert( x_FEqual( bf, B ) );
        x_assert( x_FEqual( cf, C ) );
    
        // 2
        TextFile.ReadLine();
        TextFile.ReadField( "type:<?>", &Typess );
        TextFile.ReadFieldXString ( "value:s", String );
        x_assert( String == pT1 );
    
        // 3
        TextFile.ReadLine();
        TextFile.ReadField( "type:<?>", &Typess );
        TextFile.ReadField( "value:DF", &BigD, &BigF );
	    x_assert(BigD == (f64)llI);
	    x_assert(x_FEqual((f32)BigF, (const f32)FF));
    
        //
        // Lets test user types
        //
        x_verify( TextFile.ReadRecord() );
        TextFile.ReadLine();
        TextFile.ReadField ( "mytype:<?>", &Typess );
        TextFile.ReadField ( "myvalue:.V3", &A, &B, &C );
        x_assert( x_FEqual( af, A ) );
        x_assert( x_FEqual( bf, B ) );
        x_assert( x_FEqual( cf, C ) );
    
        TextFile.ReadLine();
        TextFile.ReadField ( "mytype:<?>", &Typess );
        TextFile.ReadField ( "myvalue:.SS", &Buff, &A );
        x_assert( x_FEqual( af, A ) );
        x_assert( x_strcmp( Buff, pT1) == 0 );

    
        x_verify( TextFile.ReadRecord() );
        TextFile.ReadLine();
        TextFile.ReadField ( "One:.F", &A );
        x_assert( x_FEqual( af, A ) );
    
        TextFile.ReadField ( "Two:.V3D", &A, &B, &C);
        x_assert( x_FEqual( af, A ) );
        x_assert( x_FEqual( bf, B ) );
        x_assert( x_FEqual( cf, C ) );
    
        TextFile.close();
    }

    //-----------------------------------------------------------------------------------------

    void Test( void )
    {
        const xwstring TextFileName( X_WSTR( "temp:/TextFileTest" ) ); 
        const xwstring BinaryFileName( X_WSTR( "temp:/BinaryFileTest" ) );

        //
        // Test write and read (Text Style)
        //
        TextFileWrite( TextFileName, 0 );
        TextFileRead( TextFileName );
    
        //
        // Test write and read (Binary Style)
        //
        TextFileWrite( TextFileName,  xtextfile::access_type::MASK_BINARY );
        TextFileRead( TextFileName );

        //
        // Transform to binary format
        //
        xtextfile::TransformTo( TextFileName, BinaryFileName, xtextfile::access_type::MASK_BINARY );
        TextFileRead( BinaryFileName );
    
        //
        // Transform to text format
        //
        xtextfile::TransformTo( BinaryFileName, TextFileName, 0 );
        TextFileRead( TextFileName );
    }
}}
