//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//-------------------------------------------------------------------------------------------
// Description:
//          definition of the const char for strings. This class is meant to be use together
//          with X_STR and X_WSTR
//-------------------------------------------------------------------------------------------
template< typename T_CHAR >
struct xconst_char 
{ 
    const T_CHAR m_Value; 
};

static_assert( sizeof(xconst_char<xchar>)  == sizeof(xchar),  "" );
static_assert( sizeof(xconst_char<xwchar>) == sizeof(xwchar), "" );

//-------------------------------------------------------------------------------------------
// Description:
//          This class is meant to collect information about an static string. Note that the
//          string itself is not kept in release mode. Also you should always use this class
//          together with X_STR_CRCINFO or X_WSTR_CRCINFO
//-------------------------------------------------------------------------------------------
template< typename T_CHAR, u32 T_CRC, int T_LENGTH >
struct xstring_crcinfo
{
    x_object_type( xstring_crcinfo, is_linear ) public:

    enum : u32 { t_crc       = T_CRC    };
    enum : int { t_length    = T_LENGTH };

    x_forceconst    u32     getCRC          ( void ) const noexcept { return t_crc;     }
    x_forceconst    int     getLength       ( void ) const noexcept { return t_length;  }

#if _X_DEBUG
    CMD_DEBUG( const T_CHAR* const m_pDEBUG_Data );
#else
    x_forceconst xstring_crcinfo( const T_CHAR* ) {}
#endif
};

//-------------------------------------------------------------------------------------------
// Description:
//          This class extends the xstring_crcinfo by actually keeping the string. This class
//          is meant to be use with X_STR_CRCSTR or X_WSTR_CRCSTR
//-------------------------------------------------------------------------------------------
template< typename T_CHAR, u32 T_CRC, int T_LENGTH >
struct xstring_crcstr : xstring_crcinfo<T_CHAR,T_CRC,T_LENGTH>
{
    x_object_type( xstring_crcstr, is_linear ) public:

    using t_parent = xstring_crcinfo<T_CHAR,T_CRC,T_LENGTH>;

    x_forceconst    auto        getString                               ( void ) const noexcept                 { return m_pMem; }
    x_forceconst                xstring_crcstr                          ( const xconst_char<T_CHAR>* pData ) : t_parent{ reinterpret_cast<const T_CHAR*>(pData) }, m_pMem{ pData } {}
    x_forceconst                operator const xconst_char<T_CHAR>*     ( void ) const                          { return m_pMem; }

    const xowner<xconst_char<T_CHAR> const *> m_pMem;
};

//-------------------------------------------------------------------------------------------
// Description:
//          The following macros are used to convert regular C++ string into xstring 
//          compatible types. X_STR and X_WSTR are both meant to be use with xstring
//-------------------------------------------------------------------------------------------

#if _X_COMPILER_VS_2015
    #define X_STR(A)  reinterpret_cast<const xconst_char<xchar>*>(     &("$"##A)[1]    )
    #define X_WSTR(A) reinterpret_cast<const xconst_char<xwchar>*>(    &(L"$"##A)[1]   )
#else
    #define X_STR(A)  reinterpret_cast<const xconst_char<xchar>*>(     &("$" A)[1]     )
    #define X_WSTR(A) reinterpret_cast<const xconst_char<xwchar>*>(    &(L"$" A)[1]    )
#endif

#define X_STR_CRCINFO(STR)  xstring_crcinfo< xchar,  x_constStrCRC32(STR), x_constStrLength(STR) >{ reinterpret_cast<const xchar*>( X_STR(STR) )   }
#define X_WSTR_CRCINFO(STR) xstring_crcinfo< xwchar, x_constStrCRC32(STR), x_constStrLength(STR) >{ reinterpret_cast<const xwchar*>( X_WSTR(STR) ) }
#define X_STR_CRCSTR(STR)   xstring_crcstr < xchar,  x_constStrCRC32(STR), x_constStrLength(STR) >{ X_STR(STR)  }
#define X_WSTR_CRCSTR(STR)  xstring_crcstr < xwchar, x_constStrCRC32(STR), x_constStrLength(STR) >{ X_WSTR(STR) }

//-------------------------------------------------------------------------------------------
// https://msdn.microsoft.com/en-us/library/69ze775t.aspx 
template< typename T_CHAR >
class xstring_base
{
    x_object_type( xstring_base, is_linear )

public:
    
    using       t_char          = T_CHAR;
    using       t_characters    = units_chars_any<t_char>;
    using       const_char      = const xconst_char<T_CHAR>;

public:

    constexpr                           xstring_base                        ( void )                                = default;
    x_inline                           ~xstring_base                        ( void )                                { DecrementRef(); }

    constexpr                           xstring_base                        ( t_self&& String )                     : m_pString{ String.m_pString } { String.m_pString = nullptr; }
    x_inline                            xstring_base                        ( const t_self&  String )               { *this = String; }
    constexpr                           xstring_base                        ( const const_char* pString )           : m_pString{ (t_char*)pString } {}
    x_inline                            xstring_base                        ( t_characters Characters )             { Reset( Characters ); }
    x_inline        void                setNull                             ( void );
    x_inline        void                clear                               ( void )                                { if( m_pString ){ if(isSystemString() == false) m_pString[0]=0; } }
    x_inline        bool                isNull                              ( void ) const                          { return nullptr == m_pString; }
    x_inline        bool                isValid                             ( void ) const                          { return nullptr != m_pString; }
    x_inline        bool                isEmpty                             ( void ) const                          { return nullptr == m_pString || !m_pString[0]; }
    template<typename... T_ARGS>
    x_inline static xstring_base        Make                                ( t_characters nCharsNeeded, const char* pFormat, const T_ARGS&... Args );
    template<typename... T_ARGS>        
    x_inline static xstring_base        Make                                ( const char* pFormat, const T_ARGS&... Args );
    template<typename... T_ARGS>
    x_inline        xstring_base&       setup                               ( t_characters nCharsNeeded, const char* pFormat, const T_ARGS&... Args );
    template<typename... T_ARGS>        
    x_inline        xstring_base&       setup                               ( const char* pFormat, const T_ARGS&... Args );
    x_inline        xstring_base&       Reset                               ( t_characters nCharsNeeded = t_characters{ 0 } );
    x_inline        xstring_base&       operator  =                         ( const const_char* pString );
    x_inline        xstring_base&       operator  =                         ( const xstring_base& String );
    x_inline        xstring_base&       operator  =                         ( t_self&& String )                     { m_pString = String.m_pString; String.m_pString = nullptr; return *this; }

    template< typename T >
    x_inline        void                CopyAndConvert                      ( const T* pString );
    x_inline        void                Copy                                ( const t_char* pString );
    x_inline        void                Copy                                ( const t_char* pString, t_characters nCharsNeeded );
    x_inline        void                Copy                                ( const const_char* pString );
    x_inline        void                Copy                                ( const t_self& String );
    x_inline        void                append                              ( const t_char* pString );
    x_inline        t_characters        getLength                           ( void )    const; 
    template< typename T >
    x_forceinline   t_char&             operator []                         ( T Index )                             { x_assert( Index >= 0 ); return m_pString[Index]; }
    template< typename T >
    x_forceinline   const t_char&       operator []                         ( T Index )  const                      { x_assert( Index >= 0 ); return m_pString[Index]; }
    x_forceconst    bool                operator ==                         ( const t_self& Str2 ) const            { return Str2.m_pString == m_pString || x_strcmp( m_pString, Str2.m_pString ) == 0; }
    x_forceconst    bool                operator !=                         ( const t_self& Str2 ) const            { return !(operator == (Str2)) ; }
    x_forceinline   bool                operator ==                         ( const t_char* pStr2 ) const;
    x_forceinline   bool                operator ==                         ( t_char* pStr2 ) const;
    x_forceinline   bool                operator ==                         ( const const_char* pStr2 ) const;
    x_forceinline                       operator xbuffer_view<t_char>       ( void )                                { return xbuffer_view<t_char>{ m_pString, static_cast<xuptr>(HowMuchCharacterCapacityIHave().m_Value) }; }
    x_forceinline                       operator t_char*                    ( void )                                { return m_pString;                     }
    x_forceconst                        operator const t_char*              ( void )    const                       { return m_pString;                     }

    template< typename T >
    x_inline        xstring_base<T>     To                                  ( void )                const;

protected:

    X_DEFBITS( properties,
               u32,
               0x00000000,
               X_DEFBITS_ARG ( u32,                               REFERENCES,     24             ),                                                      // Reference counting 
               X_DEFBITS_ARG ( g_context::small_alloc_type,       SIZE_TYPE,      x_Log2IntRoundUp(int(g_context::small_alloc_type::ENUM_COUNT)-1)   )   // Allocation type for this string
    );

protected:

    x_forceconst    bool                isSystemString                      ( void )                                    const   { return m_pString && m_pString[-1] == '$'; } 
    x_forceconst    units_bytes         ComputeAllocSize                    ( t_characters nCharsNeeded )               const   { return x_assert( nCharsNeeded.m_Value > 0 ), units_bytes{ nCharsNeeded } + units_bytes{ static_cast<int>(sizeof(properties)) }; }
    x_inline        properties&         getProperties                       ( void )                                            { return x_assert( isSystemString() == false ), reinterpret_cast<properties&>(m_pString[-t_characters{ units_bytes{ static_cast<int>(sizeof(properties)) } }.m_Value]); }
    x_forceconst    const properties&   getProperties                       ( void )                                    const   { return x_assert( isSystemString() == false ), reinterpret_cast<properties&>(m_pString[-t_characters{ units_bytes{ static_cast<int>(sizeof(properties)) } }.m_Value]); }
    x_inline        units_bytes         HowMuchMemoryIHave                  ( void )                                    const; 
    x_inline        void                DecrementRef                        ( const t_characters NewSize = t_characters{ 0 } );
    x_inline        void                AddReference                        ( void );
    x_inline        t_char*             AllocateNewString                   ( t_characters nChars )                     const; 
    x_inline        bool                AllocateIfNeedit                    ( t_characters nChars );
    x_inline        void                CloneIfNeedit                       ( void );
    x_inline        t_characters        HowMuchCharacterCapacityIHave       ( void )                                    const;
     
protected:

    t_char*         m_pString { nullptr };
};

using xstring  = xstring_base<xchar>;
using xwstring = xstring_base<xwchar>;


template< typename T_CHAR > x_inline    auto        x_strlen        ( const T_CHAR* const pStr );
template< typename T_CHAR > x_inline    int         x_strncpy       ( T_CHAR* pDest, const T_CHAR* pSrc, int Count, int DestSize );
template< typename T_CHAR > x_inline    int         x_strcpy        ( T_CHAR* const pDest, const units_chars_any<T_CHAR> MaxChar, const T_CHAR* const pSrc );
template< typename T_CHAR > x_inline    int         x_strcpy        ( xbuffer_view<T_CHAR> Dest, const T_CHAR* pSrc );
template< typename T_CHAR > x_inline    void*       x_memchr        ( void* pBuf, const T_CHAR C, const units_chars_any<T_CHAR> aCount );
template< typename T_CHAR > x_inline    int         x_dtoa          ( const u64 Val, xbuffer_view<T_CHAR> Buffer, const int Base );
template< typename T_CHAR > x_inline    int         x_dtoa          ( const s64 Val, xbuffer_view<T_CHAR> Buffer, const int Base );
template< typename T_CHAR > x_inline    int         x_dtoa          ( const u32 Val, xbuffer_view<T_CHAR> Buffer, const int Base );
template< typename T_CHAR > x_inline    int         x_dtoa          ( const int Val, xbuffer_view<T_CHAR> Buffer, const int Base );
template< typename T_CHAR > x_inline    s64         x_atod64        ( const T_CHAR* pStr, const int Base );
template< typename T_CHAR > x_inline    int         x_atod32        ( const char* pStr, const int Base );
template< typename T_CHAR > x_inline    u32         x_strHash       ( const T_CHAR* pStr, const u32 Range, const u32 hVal );
template< typename T_CHAR > x_inline    u32         x_strIHash      ( const T_CHAR* pStr, const u32 Range, const u32 hVal );
template< typename T_CHAR > x_inline    u64         x_strIHash64    ( const T_CHAR* pStr, const u64 hVal );
template< typename T_CHAR > x_inline    int         x_strncmp       ( const T_CHAR* pStr1, const T_CHAR* pStr2, int Count );
template< typename T_CHAR > x_inline    int         x_strcmp        ( const T_CHAR* pStr1, const T_CHAR* pStr2 );
template< typename T_CHAR > x_inline    int         x_stricmp       ( const T_CHAR* pStr1, const T_CHAR* pStr2 );
template< typename T_CHAR > x_inline    int         x_strstr        ( const T_CHAR* const pMainStr, const T_CHAR* const pSubStr );
template< typename T_CHAR > x_inline    int         x_stristr       ( const T_CHAR* const pMainStr, const T_CHAR* const pSubStr );
template< typename T_CHAR > x_inline    int         x_atoi32        ( const T_CHAR* pStr );
template< typename T_CHAR > x_inline    s64         x_atoi64        ( const T_CHAR* pStr );
template< typename T_CHAR > x_inline    bool        x_isstrint      ( const T_CHAR* pStr );
template< typename T_CHAR > x_inline    bool        x_isstrfloat    ( const T_CHAR* pStr );
template< typename T_CHAR > x_inline    bool        x_isstrhex      ( const T_CHAR* pStr );
template< typename T_CHAR > x_inline    bool        x_isstrguid     ( const T_CHAR* pStr );
template< typename T_CHAR > x_inline    f32         x_atof32        ( const T_CHAR* pStr );
template< typename T_CHAR > x_inline    f64         x_atof64        ( const T_CHAR* pStr );
template< typename T_CHAR > x_inline    int         x_toupper       ( const T_CHAR C ) { return( (C >= T_CHAR{'a'}) && (C <= T_CHAR{'z'}) )? C + (T_CHAR{'A'} - T_CHAR{'a'}) : C; }
template< typename T_CHAR > x_inline    int         x_tolower       ( const T_CHAR C ) { return( (C >= T_CHAR{'A'}) && (C <= T_CHAR{'Z'}) )? C + (T_CHAR{'a'} - T_CHAR{'A'}) : C; }
template< typename T_CHAR > x_inline    bool        x_isspace       ( const T_CHAR C ) { return( (C == 0x09) || (C == 0x0A) || (C == 0x0D) || (C == T_CHAR{' '}) ); }
template< typename T_CHAR > x_inline    bool        x_isdigit       ( const T_CHAR C ) { return( (C >=  T_CHAR{'0'}) && (C <= T_CHAR{'9'}) ); }
template< typename T_CHAR > x_inline    bool        x_isalpha       ( const T_CHAR C ) { return( ((C >= T_CHAR{'A'}) && (C <= T_CHAR{'Z'})) || ((C >= T_CHAR{'a'}) && (C <= T_CHAR{'z'})) ); }
template< typename T_CHAR > x_inline    bool        x_isupper       ( const T_CHAR C ) { return( (C >=  T_CHAR{'A'}) && (C <= T_CHAR{'Z'}) ); }
template< typename T_CHAR > x_inline    bool        x_islower       ( const T_CHAR C ) { return( (C >=  T_CHAR{'a'}) && (C <= T_CHAR{'z'}) ); }
template< typename T_CHAR > x_inline    bool        x_ishex         ( const T_CHAR C ) { return( ((C >= T_CHAR{'A'}) && (C <= T_CHAR{'F'})) || ((C >= T_CHAR{'a'}) && (C <= T_CHAR{'f'})) || ((C >=  T_CHAR{'0'}) && (C <= T_CHAR{'9'})) ); }
template< typename T_CHAR > x_inline    u32         x_strCRC        ( const T_CHAR* pStr, u32 crcSum = 0x00000000u  );


//------------------------------------------------------------------------------
// Description:
//              Equivalent to strlen but at compile time
//------------------------------------------------------------------------------
template< typename T_CHAR >
constexpr int x_constStrLength( const T_CHAR* str ) { return x_meta_prog_details::_x_constStrLength( str ); }

//------------------------------------------------------------------------------
// Description:
//              Computes the CRC32 at compile time (similar to strCRC but at compile time)
//------------------------------------------------------------------------------
constexpr
u32 x_constStrCRC32( const char* data, const u32 crc = 0u ) { return x_meta_prog_details::_x_constStrCRC32( data, crc ); }

//------------------------------------------------------------------------------
// Description:
//          Modern version of the vsprintf rather than taken an va_list it takes 
//          The new xarg_list which contains information about the types been past.
//          This will prevent the user from interesting incorrect types.
//          Also this version takes a buffer view which makes sure everything stays
//          in bounds.
//   
//          Type Field Characters
//          Character       Output format
//          =========       --------------------------------------------------------
//             %%	        a percent sign
//             %c	        a character with the given number
//             %s	        a string
//             %d	        a signed integer, in decimal
//             %u	        an unsigned integer, in decimal
//             %o	        an unsigned integer, in octal
//             %x	        an unsigned integer, in hexadecimal (lower case)
//             %e	        a floating-point number, in scientific notation
//             %f	        a floating-point number, in fixed decimal notation
//             %g	        a floating-point number, in %e or %f notation
//             %X	        like %x, but using upper-case letters
//             %E	        like %e, but using an upper-case "E"
//             %G	        like %g, but with an upper-case "E" (if applicable)
//             %p	        a pointer (outputs the Perl value's address in hexadecimal)
//             %n	        special: *stores* the number of characters output so far into the next variable in the parameter list
//
//          Size flags
//          Character    Output
//          =========    ------------------------------------------------------------
//           h           interpret integer as s16 or u16
//           l           interpret integer as s32 or u32
//           q, L        interpret integer as s64 or u64
//           L           interpret floating point as higher precision adds a few extra floating point numbers
//
// Examples:
//
//      x_printf( "%Lf %Lf %Lf", Vec.X, Vec.Y, Vec.Z );  // Note that we use the higher precision floating point representation
//      x_printf( "%Ld" (s64)123 );                      // This is a way to print an s64 bit number
//      x_printf( "%d" 123 );                            // Nothing special here printing an 32bit integer
//      x_printf( "%f" 123.0 );                          // Nothing special here printing a floating point number
//      x_printf( "%+010.4f", 123.456 );                 // The printf works like you will expect
//------------------------------------------------------------------------------
int x_vsprintf( xbuffer_view<char> Buffer, const char* pFormatStr, const xarg_list& ArgList ) noexcept;

template<typename... T_ARGS> x_inline
int x_sprintf( xbuffer_view<char> Buffer, const char* pFormatStr, const T_ARGS&... Args ) noexcept 
{ 
    return x_vsprintf( Buffer, pFormatStr, xarg_list{ sizeof...(T_ARGS), Args... } ); 
}
