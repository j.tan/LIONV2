//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once


//------------------------------------------------------------------------------
// Description:
//      Quantum Tag Pointer. It is an atomic<T*> but the pointer has a tag inside of it.
//      This tag is modify every time the pointer changes. This is a well establish technique
//      used to avoid the lockless ABA issue.
//
//      Standard memory ordering:
//      ACQUIRE semantics is a property which can only apply to operations which READS 
//      from (Quantum/shared memory), whether they are read-modify-write operations or plain loads. 
//      The operation is then considered a read-acquire. Acquire semantics prevent memory 
//      reordering of the read-acquire with any read or write operation which follows it 
//      in program order.
//      Synchronizes all visible side effects from the last release or sequentially consistent operation.
//      "All quantum memory operations stay below this line"
//
//      RELEASE semantics is a property which can only apply to operations which WRITE to 
//      shared memory, whether they are read-modify-write operations or plain stores. 
//      The operation is then considered a write-release. Release semantics prevent memory 
//      reordering of the write-release with any read or write operation which precedes it 
//      in program order.
//      Release	Synchronizes side effects with the next consume or acquire operation.
//      "All quantum memory operations stay above this line"
//------------------------------------------------------------------------------
template<class T_ENTRY>
class x_ll_tagptr 
{
    x_object_type( x_ll_tagptr, is_quantum_lock_free );

public:
    
    using t_entry       = T_ENTRY;
    using t_tagptr_base = t_self;

public:
    
	#if _X_TARGET_64BITS
        using tag       = u16;

        struct local
        {
        public:

            constexpr               local           ( void )                        noexcept = default;
            constexpr bool          operator !=     ( const local LocalB )  const   noexcept { return m_Data != LocalB.m_Data;   }
            constexpr bool          operator ==     ( const local LocalB )  const   noexcept { return m_Data == LocalB.m_Data;   }
            constexpr t_entry*	    getPtr          ( void )                const   noexcept { return reinterpret_cast<t_entry*>(m_Data&PTR_MASK);     }
            constexpr bool          isNull          ( void )                const   noexcept { return getPtr() == nullptr;       }
            constexpr bool          isValid         ( void )                const   noexcept { return getPtr() != nullptr;       }
            constexpr tag           getTag          ( void )                const   noexcept { return m_Tag;                     }

        protected:
            
            enum flags : u64
            {
                PTR_MASK = 0xffffffffffffUL
            };

            union
            {
                u64                m_Data {};      // bits of the qt pointer
                u64                m_Ptr : (3*16);

                struct
                {
                    u16            m_Pad[3];       // Reserve for the actual pointer
                    tag            m_Tag;          // Guard/Tag
                };
            };

            template<class> friend class x_ll_tagptr;
        };

    #else

        using tag       = u32;
    
        union local
        {
        public:
            constexpr bool          operator !=     ( const local& LocalB ) const   noexcept { return m_Data != LocalB.m_Data;   }
            constexpr bool          operator ==     ( const local& LocalB ) const   noexcept { return m_Data == LocalB.m_Data;   }
            constexpr t_entry*      getPtr          ( void ) const                  noexcept { return m_pPtr;                    }
            constexpr bool          isNull          ( void ) const                  noexcept { return getPtr() == nullptr;       }
            constexpr bool          isValid         ( void ) const                  noexcept { return getPtr() != nullptr;       }
            constexpr tag           getTag          ( void ) const                  noexcept { return m_Tag;                     }

            u64                 m_Data;         // bits of the qt pointer

        protected:

            struct
            {
                t_entry*        m_pPtr;         // Actual 32 bits pointer
                tag             m_Tag;          // Guard/Tag
            };

            template<class> friend class x_qt_tagptr;
        };
    #endif

public:

    inline                      x_ll_tagptr     ( void )                                                        noexcept;
    inline explicit             x_ll_tagptr     ( const t_self& Q )                                             noexcept { setup(Q);                                                     }
    inline explicit             x_ll_tagptr     ( const t_self& Q, void* pPtr )                                 noexcept { setup(Q); setup(pPtr);                                        }
    inline                      x_ll_tagptr     ( t_entry* pPtr )                                               noexcept { setup(pPtr);                                                  }

    inline      local           getLocal        ( std::memory_order order = std::memory_order_seq_cst ) const   noexcept { return m_qtPointer.load( order );                             }
    inline      void            setup           ( const t_entry* pPtr )                                         noexcept;
    inline      void            setup           ( const t_self& B )                                             noexcept { m_qtPointer.store( B.m_qtPointer.load(std::memory_order_relaxed), std::memory_order_relaxed );  }
    inline      void            setupNull       ( void )                                                        noexcept { setup(NULL);                                                  }
    constexpr   bool            isValid         ( void ) const                                                  noexcept { return getLocal().getPtr() != NULL;                           }
    constexpr   bool            isNull          ( void ) const                                                  noexcept { return getLocal().getPtr() == NULL;                           }

    inline      bool            CompareAndSet   ( local& LocalPointer, const t_entry* const pNewPtr, std::memory_order Success, std::memory_order Failure )         noexcept;
    inline      bool            CompareAndSet   ( local& LocalPointer, const t_entry* const pNewPtr, std::memory_order MemoryOrder = std::memory_order_seq_cst )    noexcept;
    inline      bool            CompareAndSet   ( local& LocalPointer, const local NextPointer, std::memory_order Success, std::memory_order Failure )              noexcept;
    inline      bool            CompareAndSet   ( local& LocalPointer, const local NextPointer, std::memory_order MemoryOrder = std::memory_order_seq_cst )         noexcept;
    inline      void            LinkListSetNext ( t_self& Node, std::memory_order MemoryOrder = std::memory_order_seq_cst )                                          noexcept;

protected:

    x_atomic<local> m_qtPointer      {};

protected:

    template<typename, template<typename,typename> class, typename> friend class x_ll_fixed_pool_general;
    template< class > friend class x_ll_mpsc_node_queue;
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      mpmc    - Multi-Producer(push), Multi-Consumer(pop) queue
//      bounded - Means that there is an upper bound on how many nodes could be in the queue
//      queue   - FIFO ( First In first out )
//      Queue is ABA resistant.
//      Main advantage between a bounded queue vs a node queue is that bounded queue are faster.
//------------------------------------------------------------------------------
template< typename T_ENTRY, xuptr T_COUNT = 1024 >
class x_ll_mpmc_bounded_queue
{
    x_object_type( x_ll_mpmc_bounded_queue, is_quantum_lock_free, is_not_copyable, is_not_movable );

public:
    
    using t_entry = T_ENTRY; 
    static_assert( ((T_COUNT-1)&T_COUNT) == 0, "Queue size must be power of 2" );
    
public:
    
    constexpr x_ll_mpmc_bounded_queue( void ) noexcept = default;

    inline t_entry* pop( void ) noexcept
    {
        xuptr    iLocal          = m_iTail.load();
        do
        {
            const xuptr             iNext  = iLocal + 1;
            const xuptr             iHead  = m_iHead.load();
            auto&                   Entry  = m_pArray[iLocal&ARRAY_MASK];
            typename qtptr::local   pNode  = Entry.getLocal();
           
            while( pNode.getPtr() )
            {
                if( Entry.CompareAndSet( pNode, nullptr ) )
                {
                    if( (iNext ^ iHead)&ARRAY_MASK ) 
                        m_iTail.compare_exchange_strong( iLocal, iNext );

                    return pNode.getPtr();
                }
            }

            if( !((iNext ^ iHead)&ARRAY_MASK) )
                return nullptr;

            if( m_iTail.compare_exchange_strong( iLocal, iNext ) )
                iLocal = iNext;

        } while(1);
    }
    
    inline void push( t_entry& Node ) noexcept 
    {
        xuptr   iLocal      = m_iHead.load();
        do
        {
            // if this hits then we are out of memory in the array
            x_assume( (m_iTail.load()&ARRAY_MASK) != ((m_iHead.load()+1)&ARRAY_MASK) );

            const xuptr                 iNext  = iLocal + 1;
            auto&                       Entry  = m_pArray[iLocal&ARRAY_MASK];
            typename qtptr::local       pNode  = Entry.getLocal();

            // while the pointer still null and
            // the tags of the pointer tell us how many times we have gone around the buffer.
            // Tags get incremented twice ones in the push and ones in the pop so to get the real loop count we must divided by 2
            // So while the tags are the same then loop
            // 0xff is because we actually just care about 8 bits of the tag
            // because tags will roll over and we don't care about so much precision anyways
            while( pNode.getPtr() == nullptr && !(( (iLocal>>POW_OF_TWO) ^ (pNode.getTag()>>1) )&0xff ) )
            {
                if( Entry.CompareAndSet( pNode, &Node ) )
                {
                    m_iHead.compare_exchange_strong( iLocal, iNext );
                    return;
                }
            }
            
            if( m_iHead.compare_exchange_strong( iLocal, iNext ) ) 
                iLocal = iNext;

        } while(1);
    }
    
    inline t_entry* steal( void ) noexcept { return pop(); }
    
protected:
    
    enum : xuptr { ARRAY_MASK   = T_COUNT-1                   };
    enum : s32   { POW_OF_TWO   = x_Log2IntRoundUp(T_COUNT)   };

    using qtptr = x_ll_tagptr<t_entry>;
    
protected:
    
    x_atomic<xuptr>                                 m_iHead             { 1 };
    x_atomic<xuptr>                                 m_iTail             { 0 };
    xarray< qtptr, T_COUNT >                        m_pArray            {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      spmc    - Single-Producer(push), Multi-Consumer(pop) queue
//      bounded - Means that there is an upper bound on how many nodes could be in the queue
//      queue   - FIFO ( First In first out )
//      Queue is ABA resistant.
//      Main advantage between a bounded queue vs a node queue is that bounded queue are faster.
//------------------------------------------------------------------------------
template< typename T_ENTRY, xuptr T_COUNT = 1024 >
class x_ll_spmc_bounded_queue
{
    x_object_type( x_ll_spmc_bounded_queue, is_quantum_lock_free, is_not_copyable, is_not_movable );
       
public:
    
    using t_entry = T_ENTRY;
    static_assert( ((T_COUNT-1)&T_COUNT) == 0, "Queue size must be power of 2" );
    
public:
    
    constexpr  x_ll_spmc_bounded_queue ( void ) noexcept = default;
    inline t_entry* pop( void ) noexcept 
    {
        xuptr    iLocal          = m_iTail.load( std::memory_order_relaxed );
        do
        {
            const xuptr iNext =  (iLocal+1)&MASK;  
            t_entry*    pNode = m_pArray[iLocal].load();
            
            while( pNode )
            {
                if( m_pArray[iLocal].compare_exchange_weak( pNode, nullptr ) )
                {
                    if( iNext != m_iHead.load() ) 
                        m_iTail.compare_exchange_strong( iLocal, iNext );

                    return pNode;
                }
            };
            
            if( iNext == m_iHead.load() )
                return nullptr;

            if( m_iTail.compare_exchange_strong( iLocal, iNext ) ) 
                iLocal = iNext;
            
        } while(1);
    }
    
    inline void push( t_entry& Node ) noexcept 
    {
        x_assert_linear( m_Debug_LQ );
        
        const xuptr iLocal = m_iHead.load( std::memory_order_relaxed );
#if 0
        // Check for out of memory condition... right now we simply loop and wait
        while( (iLocal+1) == m_iTail.load( std::memory_order_acquire ) )
            iLocal = m_iHead.load( std::memory_order_relaxed );
#else
        x_assume( ((iLocal+1)&MASK) != m_iTail.load( std::memory_order_acquire ) );
#endif
        
        m_pArray[iLocal].store( &Node,  std::memory_order_relaxed );
        m_iHead.store( (iLocal+1)&MASK, std::memory_order_relaxed  );
    }
    
    inline t_entry* steal( void )  noexcept { return pop(); }
    
protected:
    
    enum : xuptr { MASK = T_COUNT-1 };
    
protected:
    
    alignas( x_target::getCacheLineSize() )  std::atomic<xuptr>                        m_iHead             { 1 };
    alignas( x_target::getCacheLineSize() )  std::atomic<xuptr>                        m_iTail             { 0 };
    alignas( x_target::getCacheLineSize() )  xarray<x_atomic<t_entry*>,T_COUNT>        m_pArray            {{ nullptr }};
    x_debug_linear_quantum                                                             m_Debug_LQ          {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      mpsc    - Multi-Producer(push), Single-Consumer(pop) queue
//      node    - Requires a x_tagptr as the base class of the entry
//      queue   - FIFO ( First In first out )
//
//      Queue is ABA resistant thanks for the tagptr. However the user should be carefull
//      when it frees the memory from the node as the node *may* still be in used.
//      Other interesting queues:
//      * https://kjellkod.wordpress.com/2012/11/28/c-debt-paid-in-full-wait-free-lock-free-queue/
//
// Algorithm:
//      Original algorithm by: Dominique Fober, Yann Orlarey, St�ephane Letz
//      This version was inspire by: Randall Tunner
//      Basically a 2005 Fober implementation.
//      Detail of the implementation (Optimized Lock-Free FIFO Queue continued) linked here:
//      http://www.grame.fr/ressources/publications/TR-050523.pdf
//------------------------------------------------------------------------------
template< class T_ENTRY >
class x_ll_mpsc_node_queue
{
    x_object_type( x_ll_mpsc_node_queue, is_quantum_lock_free, is_not_copyable );

public:

    inline x_ll_mpsc_node_queue  ( void ) noexcept 
    {
        m_Tail.setup( getDummyNode() );
        m_pHead = m_Tail.getLocal().getPtr();
        getDummyNode()->setupNull();
    }
    
    inline void push ( T_ENTRY& Node )                        noexcept { push(Node,Node); }
    inline void push ( T_ENTRY& NodeStart, T_ENTRY& NodeEnd ) noexcept
    {
        // Lets make sure that our end node is pointing to NULL
        NodeEnd.setupNull();
        
        do
        {
            local           Tail                { m_Tail.getLocal( std::memory_order_acquire )        };
            t_entry*        qtLastNodeNext      { Tail.getPtr()                                       };
            local           Next                { qtLastNodeNext->getLocal(std::memory_order_acquire) };
            
            // if our local reality does not longer match with the
            // quantum version of the tail then we can't assume we are at the last node.
            // Someone must have inserted something new. Go lets re-grab things again.
            if( Tail != m_Tail.getLocal( std::memory_order_acquire ) )
                continue;
            
            // If the next pointer is not NULL then we are not at the last node
            // move the tail forward.
            if( Next.isValid() )
            {
                // here, pNext is likely to be pTail->pNext
                m_Tail.CompareAndSet( Tail, Next, std::memory_order_relaxed );
                continue;
            }
            
            // If the tail is pointing to the last node then
            // append the new node.
            if( qtLastNodeNext->CompareAndSet( Next, &NodeStart, std::memory_order_release, std::memory_order_relaxed ) )
            {
                // If the tail points to what we thought was the last node
                // then update the tail to point to the new node.
                m_Tail.CompareAndSet( Tail, &NodeEnd, std::memory_order_release, std::memory_order_relaxed );
                break;
            }
            
        } while( true );
    }
    
    inline T_ENTRY* pop ( void ) noexcept
    {
        x_assert_linear( m_Debug_LQ );
        
        local Tail = m_Tail.getLocal( std::memory_order_relaxed);
        do
        {
            t_entry* const pHead = m_pHead;
            t_entry* const pNext = pHead->getLocal( std::memory_order_relaxed ).getPtr();
            
            // Check if the queue is empty.
            if( pHead == Tail.getPtr() )
            {
                if( pNext == nullptr ) return nullptr;
                
                // Special case cause by the tail been equal to the head.
                // Help move the m_Tail forward because it seems to have falling behind.
                m_Tail.CompareAndSet( Tail, pNext, std::memory_order_relaxed );
            }
            else if( pNext )
            {
                m_pHead = pNext;
                if( pHead == getDummyNode() )
                {
                    push( *getDummyNode() );
                    return pop();
                }
                return reinterpret_cast<T_ENTRY*>(pHead);
            }
            else
            {
                Tail = m_Tail.getLocal( std::memory_order_relaxed );
            }
            
        } while(1);
    }
    
    T_ENTRY*                  steal               ( void )    { return pop(); }
    
protected:
    
    enum : s32      {   OFFSET_PTR  = offsetof(T_ENTRY, m_qtPointer)      };
    using               ptr         = typename T_ENTRY::t_tagptr_base;
    using               t_entry     = typename ptr::t_entry;
    using               local       = typename ptr::local;
    
protected:
    
    constexpr T_ENTRY* getDummyNode( void ) const noexcept { return reinterpret_cast<T_ENTRY*>( const_cast<xbyte*>(&reinterpret_cast<const xbyte*>(&m_DummyNode)[-OFFSET_PTR]) ); }
    
protected:
    
    alignas( x_target::getCacheLineSize() )  t_entry*               m_pHead         { nullptr };
    alignas( x_target::getCacheLineSize() )  ptr                    m_Tail          {};
    alignas( x_target::getCacheLineSize() )  ptr                    m_DummyNode     {};
    x_debug_linear_quantum                                          m_Debug_LQ      {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      mpmc    - Multi-Producer(push), Multi-Consumer(pop) queue
//      node    - Requires a x_tagptr as the base class of the entry
//      queue   - FIFO ( First In first out )
//
//      Queue is ABA resistant thanks for the tagptr. However the user should be carefull
//      when it frees the memory from the node as the node *may* still be in used.
//      Other interesting queues:
//      * https://kjellkod.wordpress.com/2012/11/28/c-debt-paid-in-full-wait-free-lock-free-queue/
//
// Algorithm:
//      Original algorithm by: Dominique Fober, Yann Orlarey, St�ephane Letz
//      This version was inspire by: Randall Tunner
//      Basically a 2005 Fober implementation.
//      Detail of the implementation (Optimized Lock-Free FIFO Queue continued) linked here:
//      http://www.grame.fr/ressources/publications/TR-050523.pdf
//-------------------------------------------------------------------------------
template< class T >
class x_ll_mpmc_node_queue 
{
    x_object_type( x_ll_mpmc_node_queue, is_quantum_lock_free, is_not_copyable );

public:
    
    inline x_ll_mpmc_node_queue( void ) noexcept
    {
        m_Tail.setup( getDummyNode() );
        m_Head.setup( m_Tail );
        getDummyNode()->setupNull();
    }
    
    inline void push    ( T& Node )                     noexcept { push( Node, Node ); }
    inline void push    ( T& NodeStart, T& NodeEnd )    noexcept 
    {
        // Lets make sure that our end node is pointing to NULL
        NodeEnd.setupNull();
        
        do
        {
            local		Tail                { m_Tail.getLocal( std::memory_order_acquire )       };
            ptr&        qtLastNodeNext =    *Tail.getPtr();
            local		Next                { qtLastNodeNext.getLocal(std::memory_order_acquire) };
            
            // if our local reality does not longer match with the
            // quantum version of the tail then we can't assume we are at the last node.
            // Someone must have inserted something new. Go lets re-grab things again.
            if( Tail != m_Tail.getLocal( std::memory_order_acquire ) )
                continue;
            
            // If the next pointer is not NULL then we are not at the last node
            // move the tail forward.
            if( Next.isValid() )
            {
                // here, pNext is likely to be pTail->pNext
                m_Tail.CompareAndSet( Tail, Next, std::memory_order_relaxed );
                continue;
            }
            
            // If the tail is pointing to the last node then
            // append the new node.
            if( qtLastNodeNext.CompareAndSet( Next, &NodeStart, std::memory_order_release, std::memory_order_relaxed ) )
            {
                // If the tail points to what we thought was the last node
                // then update the tail to point to the new node.
                m_Tail.CompareAndSet( Tail, &NodeEnd, std::memory_order_release, std::memory_order_relaxed );
                break;
            }
            
        } while( true );
    }
    
    inline T* pop( void ) noexcept
    {
        do
        {
            local Head        { m_Head.getLocal( std::memory_order_acquire ) };
            ptr*  pHead =     Head.getPtr();
            local Next        { pHead->getLocal( std::memory_order_acquire ) };
            
            // Verify that we did not get the pointers in the middle
            // of another update.
            if( Head != m_Head.getLocal( std::memory_order_acquire ) )
                continue;
            
            // Check if the queue is empty.
            if( Head == m_Tail.getLocal( std::memory_order_acquire ) )
            {
                if( Next.isNull() )	return NULL;
                
                // Special case cause by the tail been equal to the head.
                // Help move the m_Tail forward because it seems to have falling behind.
                m_Tail.CompareAndSet( Head, Next, std::memory_order_relaxed );
            }
            else if( Next.isValid() )
            {
                // Move the head pointer, effectively removing the node
                if( m_Head.CompareAndSet( Head, Next, std::memory_order_release, std::memory_order_relaxed ) )
                {
                    // Well so if it turns out that we just got the dummy node.
                    // Re-insert it again and get a new one
                    if( pHead == getDummyNode() )
                    {
                        push( *getDummyNode() );
                        return pop();
                    }
                    
                    assert( Head.getPtr() != getDummyNode() );
                    assert( m_Tail.getLocal(std::memory_order_acquire).getPtr() != pHead );
                    
                    return reinterpret_cast<T*>(pHead);
                }
            }
            
        } while( true );
    }

    inline T* steal ( void ) noexcept { return pop(); }
    
protected:
    
    enum : xuptr   {   OFFSET_PTR  = offsetof(T, m_qtPointer) };
    using               ptr         = x_ll_tagptr<typename T::t_base>;
    using               local       = typename ptr::local;
    
protected:
    
    constexpr T*    getDummyNode( void ) const noexcept { return reinterpret_cast<T*>( &reinterpret_cast<xbyte*>(&m_DummyNode)[-OFFSET_PTR] ); }
    
protected:
    
    alignas( x_target::getCacheLineSize() )  ptr    m_Head          {};
    alignas( x_target::getCacheLineSize() )  ptr    m_Tail          {};
    alignas( x_target::getCacheLineSize() )  ptr    m_DummyNode     {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      general - An allocator object is require and it will handle how to allocate memory
//------------------------------------------------------------------------------
template< typename T_ENTRY, template<typename,typename> class T_ALLOCATOR, typename T_COUNTER = xuptr >
class x_ll_fixed_pool_general 
{
    x_object_type( x_ll_fixed_pool_general, is_quantum_lock_free, is_not_copyable, is_not_movable );

    public:     using                           t_entry         = T_ENTRY;
    public:     enum {                          t_aligment      = x_Max( alignof(x_atomic<void*>), alignof(t_entry) ) };
    protected:  struct alignas( t_aligment )    t_real_node; 
    public:     using                           t_allocator     = T_ALLOCATOR< t_real_node, T_COUNTER >;
    public:     using                           t_counter       = typename t_allocator::t_counter;
    
public:

    x_forceinline x_ll_fixed_pool_general( bool bClear = true ) noexcept { if( bClear ) Clear(); }
    
    inline void Clear ( void ) noexcept  
    {
        const t_counter Count = m_Allocator.getCount() - 1;
        
        for( t_counter i = 0; i<Count; i++ )
        {
            m_Allocator[i].m_Next.store( &m_Allocator[i + 1], std::memory_order_relaxed );
        }
        
        m_Allocator[Count].m_Next.store( nullptr, std::memory_order_relaxed );
        m_Head.store( &m_Allocator[0] );
    }
    
    inline t_entry* pop ( void ) noexcept  
    {
        t_real_node* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality )
        {
            if( m_Head.compare_exchange_weak( pLocalReality, pLocalReality->m_Next, std::memory_order_acquire, std::memory_order_relaxed ) )
            {
                //SanityCheck();
                return &pLocalReality->m_Entry;
            }
        }
        return nullptr;
    }
    
    inline void push ( t_entry& userEntry ) noexcept  
    {
        t_real_node& Node            = *reinterpret_cast<t_real_node*>(&(reinterpret_cast<xbyte*>(&userEntry)[-ENTRY_OFFSET]));
        t_real_node* pLocalReality   = m_Head.load( std::memory_order_relaxed );
        do
        {
            Node.m_Next.store( pLocalReality, std::memory_order_relaxed );
        } while( !m_Head.compare_exchange_weak( pLocalReality, &Node, std::memory_order_release, std::memory_order_relaxed ) );

        //SanityCheck();
    }
    
    inline t_entry& get ( const xuptr Index ) noexcept { return m_Allocator[Index].m_Entry; }
    
    constexpr   bool                    Belongs         ( const void* pPtr )    const   noexcept { return m_Allocator.Belongs(pPtr);        }

    inline void SanityCheck( void )
    {
        // Walk the link list to make sure everything is ok
        t_real_node* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality )
        {
            pLocalReality = pLocalReality->m_Next;
        }
    }

protected:
    
    struct alignas( t_aligment ) t_real_node 
    {
        x_atomic<t_real_node*>      m_Next      {};
        t_entry                     m_Entry     {};
    };
    
    enum : s32 { ENTRY_OFFSET = offsetof( t_real_node, m_Entry ) };
    
protected:
    
    x_atomic<t_real_node*>          m_Head         { nullptr };
    t_allocator                     m_Allocator    {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      general - An allocator object is require and it will handle how to allocate memory
//      jitc    - ( Just In Time Construction/Destruction )
//                  Means that when an entry is pop then we will constructed and push destruct it
// Algorithm:
//      Note the algorithm reuses the entry memory as a pointer to the next node.
//------------------------------------------------------------------------------
template< typename T_ENTRY, template<typename,typename> class T_ALLOCATOR >
class x_ll_fixed_pool_general_jitc 
{
    x_object_type( x_ll_fixed_pool_general_jitc, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    using t_allocator   = T_ALLOCATOR< T_ENTRY, xuptr >;
    using t_entry       = typename t_allocator::t_entry;
    using t_counter     = typename t_allocator::t_counter;

public:

    x_forceinline x_ll_fixed_pool_general_jitc( bool bClear = true ) noexcept { if( bClear ) Clear(); }
    
    ~x_ll_fixed_pool_general_jitc( void )
    {
        DestructAllocatedNodes();
    }

    inline void DestructAllocatedNodes( void )
    {
        // Check
        const t_counter Count = m_Allocator.getCount();
        if( Count == 0 ) return;

        xafptr<bool>    Checks;

        // Alloc and clear the memory to mark what it has been freed so far
        Checks.Alloc( Count );
        Checks.MemSet( false );
        
        // Mark all nodes that have been free so far
        for( head Next = m_Head; Next.load(); Next.store( Next.load()->m_Next ) )
        {
            const auto Index = static_cast<int>( reinterpret_cast<t_entry*>(Next.load()) - reinterpret_cast<t_entry*>( &m_Allocator[0] ) );
            
            // make sure there is not strange situations
            x_assert( Checks[ Index ] == false );

            // mark the node as free
            Checks[ Index ] = true;
        }

        // Destruct all nodes that have not been freed by the user
        // These nodes could be consider a leak....
        for( int i = 0; i < Count; i++ )
        {
            if( Checks[i] == false )
            {
                x_destruct( &m_Allocator[i] );
            }
        }
    }


    inline void Clear ( void ) noexcept 
    {
        const t_counter Count = m_Allocator.getCount() - 1;
        
        for( t_counter i = 0; i<Count; i++ )
        {
            reinterpret_cast<next*>(&m_Allocator[i])->m_Next.store( reinterpret_cast<next*>(&m_Allocator[i + 1]), std::memory_order_relaxed );
        }
        
        reinterpret_cast<next*>(&m_Allocator[Count])->m_Next.store( nullptr, std::memory_order_relaxed );
        m_Head.store( reinterpret_cast<next*>(&m_Allocator[0]), std::memory_order_relaxed );
    }

    inline t_entry* popDontConstruct( void ) noexcept 
    {
        next* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality )
        {
            if( m_Head.compare_exchange_weak( pLocalReality, pLocalReality->m_Next.load(std::memory_order_relaxed), std::memory_order_acquire, std::memory_order_relaxed ) )
            {
                t_entry* pEntry = reinterpret_cast<t_entry*>(pLocalReality);
                return pEntry;
            }
        }
        return nullptr;
    }
    
    template<typename ...T_ARG>
    inline t_entry* pop ( T_ARG&&...Args ) noexcept 
    {
        next* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality )
        {
            if( m_Head.compare_exchange_weak( pLocalReality, pLocalReality->m_Next.load(std::memory_order_relaxed), std::memory_order_acquire, std::memory_order_relaxed ) )
            {
                t_entry* pEntry = reinterpret_cast<t_entry*>(pLocalReality);
  //!!!! COMPILER CHASHING HERE !!!!!              
                (void)x_construct( t_entry, pEntry, { Args... } );
                return pEntry;
            }
        }
 
        return nullptr;
    }
    
    inline void pushDontDestruct ( t_entry& Node ) noexcept 
    {
        next* pLocalReality   = m_Head.load( std::memory_order_relaxed );
        do
        {
            reinterpret_cast<next*>(&Node)->m_Next.store( pLocalReality, std::memory_order_relaxed );
        } while( !m_Head.compare_exchange_weak( pLocalReality, reinterpret_cast<next*>(&Node), std::memory_order_release, std::memory_order_relaxed ) );
    }
    
    inline void push ( t_entry& Node ) noexcept 
    {
        x_destruct( &Node );
        pushDontDestruct( Node );
    }

    inline      t_entry&                get             ( xuptr Index )                 noexcept { return m_Allocator[Index].m_Entry;       }
    inline      t_allocator&            getAllocator    ( void )                        noexcept { return m_Allocator;                      }
    constexpr   const t_allocator&      getAllocator    ( void )                const   noexcept { return m_Allocator;                      }
    constexpr   bool                    Belongs         ( const void* pPtr )    const   noexcept { return m_Allocator.Belongs(pPtr);        }
    constexpr   int                     getIndexByEntry ( const t_entry& Entry ) const  noexcept { return m_Allocator.getIndexByEntry( Entry); }
    template< typename T >
    constexpr   const t_entry&          getEntryByIndex ( const T Index )       const   noexcept { return m_Allocator[Index]; }
    template< typename T >
    x_forceinline   t_entry&            getEntryByIndex ( const T Index )               noexcept { return m_Allocator[Index]; }

protected:

    struct next;
    using  head  = x_atomic<next*>;
    struct next { head m_Next; };
     
    static_assert( alignof(t_entry) >= alignof(next), "Please make sure that your entry has the alignment of an x_atomic<void*>" );
    static_assert( sizeof(t_entry)  >= sizeof(next),  "Please make sure that the entry is byte size is larger than an atomic pointer" );
    
protected:
    
    alignas( x_target::getCacheLineSize() )  head                       m_Head         { nullptr };
    t_allocator                                                         m_Allocator    {};
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      dynamic - The pool will be allocated in the heap, must Init to create the pool size
//------------------------------------------------------------------------------
template< class T_ENTRY >
class x_ll_fixed_pool_dynamic : public x_ll_fixed_pool_general< T_ENTRY, xndptr >
{
public:

    using t_self   = x_ll_fixed_pool_dynamic;
    using t_parent = x_ll_fixed_pool_general< T_ENTRY, xndptr >;
    using t_entry  = typename t_parent::t_entry;
    
public:

    x_forceinline           x_ll_fixed_pool_dynamic     ( void )        noexcept    : t_parent( false ) {}
    inline                 ~x_ll_fixed_pool_dynamic     ( void )        noexcept    { Kill();                                                      }
    inline  void            Init                        ( s32 Count )   noexcept    { t_parent::m_Allocator.New( Count ); t_parent::Clear();       }
    inline  void            Kill                        ( void )        noexcept    { this->DestructAllocatedNodes(); t_parent::m_Allocator.Delete();    }
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      dynamic - The pool will be allocated in the heap, must Init to create the pool size
//      jitc    - ( Just In Time Construction/Destruction )
//                  Means that when an entry is pop then we will constructed and push destruct it
//------------------------------------------------------------------------------
template< class T_ENTRY >
class x_ll_fixed_pool_dynamic_jitc : public x_ll_fixed_pool_general_jitc<T_ENTRY,xafptr>
{
public:

    using t_self   = x_ll_fixed_pool_dynamic_jitc< T_ENTRY >;
    using t_parent = x_ll_fixed_pool_general_jitc< T_ENTRY, xafptr >;
    
public:

    x_forceinline       x_ll_fixed_pool_dynamic_jitc    ( void )         noexcept    : t_parent( false ) {}
    inline             ~x_ll_fixed_pool_dynamic_jitc    ( void )         noexcept   { Kill();                                                               }
    inline void         Init                            ( xuptr Count )  noexcept   { t_parent::m_Allocator.Alloc( Count ); t_parent::Clear();              }
    inline void         Kill                            ( void )         noexcept   { t_parent::DestructAllocatedNodes();   t_parent::m_Allocator.Free();   }
};

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      static  - The pool is created as an array in the class.
//      jitc    - ( Just In Time Construction/Destruction )
//                  Means that when an entry is pop then we will constructed and push destruct it
//------------------------------------------------------------------------------
template< class T_ENTRY, int T_COUNT, typename T_COUNTER = xuptr >
using x_ll_fixed_pool_static_jitc = x_ll_fixed_pool_general_jitc
< 
    T_ENTRY, 
    x_static_to_dynamic_interface< xarray_raw, T_COUNT >::template allocator 
>;

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      static  - The pool is created as an array in the class.
//------------------------------------------------------------------------------
template< class T_ENTRY, int T_COUNT, typename T_COUNTER = xuptr >
using x_ll_fixed_pool_static = x_ll_fixed_pool_general
<
    T_ENTRY, 
    x_static_to_dynamic_interface< xarray, T_COUNT >::template allocator,
    T_COUNTER 
>;

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      fixed   - Memory pool won't grow automatically
//      pool    - Memory pool, (an array of entries)
//      static  - The pool is created as an array in the class
//      raw     - Elements in the pool wont be constructed or destructed
//------------------------------------------------------------------------------
template< class T_ENTRY, int T_COUNT, typename T_COUNTER = xuptr >
using x_ll_fixed_pool_static_raw = x_ll_fixed_pool_general
< 
    T_ENTRY, 
    x_static_to_dynamic_interface< xarray_raw, T_COUNT >::template allocator 
>;

//------------------------------------------------------------------------------
// Description:
//      ll      - Lockless
//      Lockless circular pool with debug build
//------------------------------------------------------------------------------
class x_ll_circular_pool
{
    x_object_type( x_ll_circular_pool, is_quantum_lock_free, is_not_copyable, is_not_movable );

public:
    
                                        x_ll_circular_pool  ( void )  = default;
    inline                              x_ll_circular_pool  ( xuptr ByteCount )                 noexcept { Init( ByteCount );                                                            }
    template< class T > inline  T&      Alloc               ( void )                            noexcept { return *reinterpret_cast<T*>(ByteAlloc( sizeof(T), alignof( T ) ));           }
    template< class T > inline  T&      New                 ( void )                            noexcept { return *x_construct( T, ByteAlloc( sizeof(T), alignof( T ) ) );               }
    inline                      void    Free                ( void* pVoidData )                 noexcept;
    inline                      void    Init                ( xuptr ByteCount )                 noexcept;
    constexpr                   xuptr   getSize             ( void ) const                      noexcept { return m_pPtr.getCount();                                                     }
    
protected:
    
    inline                      xbyte*  ByteAlloc           ( xuptr ByteCount, u32 Aligment )   noexcept ;
    inline                      void    MoveHeadForward     ( void )                            noexcept ;
    
protected:
    
#if _X_DEBUG
    struct node
    {
        x_atomic<void*>     m_Ptr       { nullptr };
        xuptr               m_Size      { 0 };
    };

    xndptr<node>               m_Debug_PtrList  {};
    x_atomic<xuptr>            m_Debug_iTail    { 0 };
    x_atomic<xuptr>            m_Debug_iHead    { 0 };
    x_atomic<xuptr>            m_Stats_MaxUsed  { 0 };
    x_atomic<xuptr>            m_Stats_CurrUsed { 0 };

    x_atomic<xuptr>             m_Debug_HeadLoop{ 0 };
    x_atomic<xuptr>             m_Debug_TailLoop{ 0 };
    x_atomic<void*>             m_Debug_TailPtr { nullptr };

#endif
    
    alignas( x_target::getCacheLineSize() ) x_atomic<xuptr>     m_Head  { 0 };
    alignas( x_target::getCacheLineSize() ) xafptr<xbyte>       m_pPtr  {};
};

//------------------------------------------------------------------------------
// Description:
//      Quantum object used whenever you want to have a quantum base class
//------------------------------------------------------------------------------
class x_ll_object_debug
{
    x_object_type( x_ll_object_debug, is_quantum_lock_free, rtti_start, is_not_copyable, is_not_movable );

public:

    x_ll_object_debug(void) = default;

protected:

    x_debug_linear_quantum m_Debug_LQ {};
    virtual void qt_onRun( void ) = 0;
};

template< typename T_CHILD, typename T_PARENT = x_ll_object_debug >
class x_ll_object_harness : public T_PARENT
{
    x_object_type( x_ll_object_harness, is_quantum_lock_free, rtti(T_PARENT), is_not_copyable, is_not_movable );

public:
    
    using   t_child   = T_CHILD;
    
    struct local_message : x_ll_tagptr<local_message>
    {
        xfunction<void(void)>   m_Callback;
    };
    
public:
    
    x_orforceconst          x_ll_object_harness     ( x_ll_circular_pool& CircularPool )    noexcept : m_MsgAllocPool{ CircularPool }
    {
        x_assert_linear( t_parent::m_Debug_LQ );
    }
    
    x_orforceconst          x_ll_object_harness     ( x_ll_circular_pool& CircularPool, u32 Flags )    noexcept : m_qtFlags{ Flags }, m_MsgAllocPool{ CircularPool }
    {
        x_assert_linear( t_parent::m_Debug_LQ );
    }

    x_inline        void    Push                    ( xowner<local_message&> Node )          noexcept { m_Queue.push( Node ); }

    x_inline        void    FlushMessages           ( void ) noexcept
    {
        const flags LocalFlags = MsgForceLock();
        reinterpret_cast<t_child*>(this)->t_ProcessMessages( LocalFlags );
        MsgUnlock();
    }
    
    x_inline        void    SendMsg                 ( xfunction<void(void)> Callback ) noexcept
    {
        x_assert_quantum( t_parent::m_Debug_LQ );
        
        const flags LocalFlags = MsgTryToLock();
        if( LocalFlags.m_MSG_LOCK )
        {
            //
            // Linear space
            //
            // We must be clear when is linear space and when is not
            // that means that when the destructor clears the linear flag then
            // we can unlock but not a second before
            bool bWasComsumed;
            {
                x_assert_linear( m_Debug_LQMsg );
                
                // Tries to update any pending messages and then it returns if we are allow to 
                // handle additional messages
                bWasComsumed = reinterpret_cast<t_child*>(this)->t_ProcessMessages( LocalFlags );
                if( bWasComsumed )
                {
                    Callback();
                }
            }
            MsgUnlock();
            if(bWasComsumed) return;
        }

        //
        // Append the message in the queue to be consumed later
        //
        xowner<local_message&> MyMessage = m_MsgAllocPool.x_ll_circular_pool::New<local_message>();
        MyMessage.m_Callback = Callback; 
        Push( MyMessage );
    }

    x_inline        void    SendMsgComplex  ( xfunction<void(bool bDirectCall)> Callback ) noexcept
    {
        x_assert_quantum( t_parent::m_Debug_LQ );
        
        const flags LocalFlags = MsgTryToLock();
        if( LocalFlags.m_MSG_LOCK )
        {
            //
            // Linear space
            //
            // We must be clear when is linear space and when is not
            // that means that when the destructor clears the linear flag then
            // we can unlock but not a second before
            bool bWasComsumed;
            {
                x_assert_linear( m_Debug_LQMsg );
                
                // Tries to update any pending messages and then it returns if we are allow to 
                // handle additional messages
                bWasComsumed = reinterpret_cast<t_child*>(this)->t_ProcessMessages( LocalFlags );
                if( bWasComsumed )
                {
                    Callback( true );
                }
            }
            MsgUnlock();
            if(bWasComsumed) return;
        }

        //
        // Lets the user push the message
        //
        Callback( false );
    }
    
protected:
    
    X_DEFBITS( flags, 
                u32,
                0x00000000,
                X_DEFBITS_ARG( bool,    T0,                             1 ),
                X_DEFBITS_ARG( bool,    MSG_LOCK,                       1 ),
                X_DEFBITS_ARG( bool,    MSG_WAIT_FOR_UPDATE,            1 ),
                X_DEFBITS_ARG( bool,    USER_1,                         1 ),
                X_DEFBITS_ARG( bool,    USER_2,                         1 ),
                X_DEFBITS_ARG( bool,    USER_3,                         1 ),
                X_DEFBITS_ARG( bool,    USER_4,                         1 )
    );

protected:
    
    // Returns true if the caller is also allow to consume messages
    x_inline bool t_ProcessMessages( flags LocalFlags = 0 ) noexcept
    {
        x_assert_quantum( t_parent::m_Debug_LQ );
        x_assert_linear ( m_Debug_LQMsg );
        for( local_message* pMsg = m_Queue.pop(); pMsg; pMsg = m_Queue.pop() )
        {
            pMsg->m_Callback();
            m_MsgAllocPool.Free( pMsg );
        }
        return true;
    }
   
    x_inline flags MsgTryToLock( void ) noexcept
    {
        x_assert_quantum( t_parent::m_Debug_LQ );
        
        // Lock our object if we can if it already locked then return
        flags LocalFlags = m_qtFlags.load();
        flags NewFlags;
        do
        {
            if( LocalFlags.m_MSG_LOCK )
                return 0;
            
            NewFlags.m_Flags = LocalFlags.m_Flags | flags::MASK_MSG_LOCK;
        } while( !m_qtFlags.compare_exchange_weak( LocalFlags, NewFlags ) );
        
        //x_assume( (NewFlags.m_Flags & 0xffffff00) == 0 );
        return NewFlags;
    }
    
    x_inline flags MsgForceLock( void ) noexcept;
    
    x_inline void MsgUnlock( u32 NewFlags = 0 ) noexcept
    {
        x_assert_quantum( t_parent::m_Debug_LQ );
        
        // The destructor for this assert needs to be call before the compare exchange happens
        // so we will do a quick check to try to detect something inside its own scope
        { x_assert_linear ( m_Debug_LQMsg ); }
        
        // Remove the consuming flag
        flags LocalFlags = m_qtFlags.load();
        do
        {
            // Is there someone already consuming messages?
            x_assert( LocalFlags.m_MSG_LOCK );

        } while( m_qtFlags.compare_exchange_weak( LocalFlags, flags{ (NewFlags | LocalFlags.m_Flags) & (~flags::MASK_MSG_LOCK) } ) == false );
    }
        
    x_inline virtual void qt_onRun( void ) noexcept override 
    {
        x_assert_quantum( t_parent::m_Debug_LQ );
        flags LocalFlags = MsgTryToLock();
        if( LocalFlags.m_MSG_LOCK )
        {
            {
                x_assert_linear( m_Debug_LQMsg );
                reinterpret_cast<t_child*>(this)->t_ProcessMessages( LocalFlags );
            }
            MsgUnlock();
        }
    }
    
    x_inline flags appendFlags( u32 Flags ) noexcept
    {
        flags LocalFlags = m_qtFlags.load();
        flags NewValue; 
        do
        {
            if( (LocalFlags.m_Flags & Flags ) == Flags ) return LocalFlags;
            NewValue.m_Flags = LocalFlags.m_Flags | Flags;

        } while( !m_qtFlags.compare_exchange_weak( LocalFlags, NewValue ) );

        return NewValue;
    }
    
    x_inline flags removeFlags( const u32 Flags ) noexcept
    {
        const u32  AndFlags    = ~Flags;
        flags      LocalFlags  = m_qtFlags.load();
        flags      NewValue; 
        do
        {
            if( (LocalFlags.m_Flags & Flags) == 0 )
            {
                NewValue = LocalFlags;
                break;
            }
            NewValue.m_Flags = LocalFlags.m_Flags & AndFlags;

        } while( !m_qtFlags.compare_exchange_weak( LocalFlags, NewValue ) );

        x_assert( hasFlags(Flags) == false );
        return NewValue;
    }
    
    constexpr bool hasFlags( u32 Flags ) const noexcept { return (m_qtFlags.load().m_Flags & Flags) == Flags; }
    
protected:
    
    x_ll_mpsc_node_queue<local_message>             m_Queue                 {};
    x_debug_linear_quantum                          m_Debug_LQMsg           {};            // LQ of the mutable data
    x_atomic<flags>                                 m_qtFlags               { flags{} };
    x_ll_circular_pool&                             m_MsgAllocPool;
};

//------------------------------------------------------------------------------
// Description:
//      This hash table is meant to be use in a multi-core environment. There are key events
//      were the hash table must lock parts of it. However it does this in the least disruptive way possible.
//      For example one reason to lock is when the user is mutating entry.
//      we lock only that entry in the hash preventing other simultaneous access which would result in 
//      unpredictable behaviors. In other words we must take that entry into a linear world because until the mutation
//      is complete is could be in a bad state.
//      The only other reason why we lock is when an entry must be deleted. In that case we lock a hash table index entry.
//      So that we can access that index table link list and fix up the pointers. All the other hash index table entries remain lockfree.
//      Note that in most cases index table entries and entries may have a 1:1 relation ship which means there is not much locking at all.
//
// TODO: Right now the Adding is Relax, which means that more of one similar key could be added because the adding is lockless
//       To guaranteed that I can add a restrictive add and leave the relax adding as an option.
//------------------------------------------------------------------------------
template< class ENTRY, class KEY, class DOWORK >
class x_ll_mrmw_hash 
{
    x_object_type( x_ll_mrmw_hash, is_quantum_lock_free, is_not_copyable, is_not_movable )

protected: struct hash_entry; 
public:

    using t_entry           = ENTRY;
    using t_key             = KEY;
    using t_function        = xfunction<void (t_entry&)>;
    using t_const_function  = xfunction<void (const t_entry&)>;

    struct iterator
    {
        inline iterator( t_self& S, bool Start ) : m_Hash{S}{ if(Start) this->operator ++(); }
        inline iterator& operator ++ ( void )                                                    
        { 
            if( m_pHashEntry ) 
            {
                m_pHashEntry = m_pHashEntry->m_Next;
                if( m_pHashEntry ) return *this;
            }

            m_iTableEntry++;
            while( m_iTableEntry < m_Hash.getCount() )
            {
                m_pHashEntry = m_Hash.m_qtHashTable[m_iTableEntry].m_Head.load();
                if( m_pHashEntry ) break;
                m_iTableEntry++;
            }

            return *this; 
        }

        inline iterator operator ++ ( int )                                                    
        { 
            auto I = *this;
            this->operator ++();
            return I; 
        }

        constexpr bool operator != ( const iterator& ) const 
        { 
            return m_iTableEntry < m_Hash.m_qtHashTable.getCount(); 
        }

        inline  t_entry&            operator *  ( void ) noexcept       { return m_pHashEntry->m_UserData; }
        inline  const t_entry&      operator *  ( void ) const noexcept { return m_pHashEntry->m_UserData; }
    
        t_self&     m_Hash;
        int         m_iTableEntry {-1};
        hash_entry* m_pHashEntry  {nullptr};
    };

public:

    constexpr                       x_ll_mrmw_hash          ( void )                                            noexcept = default;
    inline      void                Initialize              ( xuptr MaxEntries )                                noexcept;
    inline      t_key               getKeyFromEntry         ( const t_entry& Entry ) const                      noexcept;
    inline      bool                isEntryInHash           ( t_key Key ) const                                 noexcept;
    inline      void                DeleteEntry             ( t_key Key )                                       noexcept;
    constexpr   xuptr               getCount                ( void ) const                                      noexcept { return m_qtHashTable.getCount(); }
    inline      void                cpFindOrAddEntry        ( t_key Key, t_function Function )                  noexcept;
    inline      void                cpAddEntry              ( t_key Key, t_function Function )                  noexcept;
    inline      void                cpGetEntry              ( t_key Key, t_function Function )                  noexcept;
    inline      void                cpGetEntry              ( t_key Key, t_const_function Function ) const      noexcept;
    inline      bool                cpGetOrFailEntry        ( t_key Key, t_function Function )                  noexcept;
    inline      bool                cpGetOrFailEntry        ( t_key Key, t_const_function Function ) const      noexcept;

    inline      void                cpIterateGetEntry       ( t_function Function )                             noexcept;
    inline      void                cpIterateGetEntry       ( t_const_function Function ) const                 noexcept;

    inline      iterator            begin                   ( void )                                            noexcept { return iterator{ *this, true }; }
    inline      iterator            end                     ( void )                                            noexcept { return iterator{ *this, false }; }

protected:

    struct hash_entry 
    {
        x_atomic<hash_entry*>               m_Next          { nullptr };
        x_lk_semaphore_smart_lock<DOWORK>   m_Semaphore     {};
        t_key                               m_Key           {};
        t_entry                             m_UserData      {};
    };

    struct hash_table_entry
    {
        x_lk_semaphore_smart_lock<DOWORK>   m_Semaphore     {};
        x_atomic<hash_entry*>               m_Head          { nullptr };
    };

protected:

    inline      hash_entry&         getHashEntryFromEntry   ( t_entry& Entry )                                          noexcept;
    inline      const hash_entry&   getHashEntryFromEntry   ( const t_entry& Entry ) const                              noexcept;
    inline      xuptr               KeyToIndex              ( t_key Key ) const                                         noexcept;

    inline      t_entry&            FindOrAddPopEntry       ( t_key Key )                                               noexcept;
    inline      t_entry&            AddPopEntry             ( t_key Key )                                               noexcept;
    inline      t_entry*            getPopEntry             ( t_key Key )                                               noexcept;
    inline      const t_entry*      getPopEntry             ( t_key Key ) const                                         noexcept;
    inline      void                PushEntry               ( t_entry& Entry )                                          noexcept;
    inline      void                PushEntry               ( const t_entry& Entry ) const                              noexcept;

    inline      hash_entry&         FindOrAddHashEntry      ( hash_table_entry& TableEntry, t_key Key )                 noexcept;
    inline      hash_entry&         AddHashEntry            ( hash_table_entry& TableEntry, t_key Key )                 noexcept;
    inline      hash_entry*         FindEntry               ( const hash_table_entry& TableEntry, t_key Key ) const     noexcept;

protected:

    xndptr<hash_table_entry>                    m_qtHashTable       {};
    x_ll_fixed_pool_dynamic_jitc<hash_entry>    m_EntryMemPool      {};

    friend struct iterator;
};


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// EXPERIMENTAL
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Description:
//      A lockless queue where elements can not be pop out
//------------------------------------------------------------------------------
template< typename T_ENTRY >
class x_ll_popless_queue 
{
    x_object_type( x_ll_popless_queue, is_quantum_lock_free, is_not_movable )

public:

    using t_entry = T_ENTRY;

public:

    constexpr x_ll_popless_queue( void ) = default;
    constexpr x_ll_popless_queue( const x_ll_popless_queue& PopLess )           noexcept : m_Head( PopLess.m_Head ), m_Tail( PopLess.m_Tail ) {}

    inline void Push ( t_entry& Node )                                          noexcept
    {
        //
        // Handle case where the HEAD and TAIL is equal to NULL
        //
        while( m_Head == nullptr )
        {
            t_entry* pTail = m_Tail.load();

            if( pTail )
                break;

            // If the node that the tail points to is the last node
            // then update the last node to point at the new node.
            if( m_Tail.compare_exchange_weak( pTail, &Node ) )
            {
                // If we got here it means we own the Tail reality.
                m_Head = &Node;
                return;
            }
        }

        //
        // Handle all other cases
        //
        do
        {
            t_entry* pTail = m_Tail.load();
            t_entry* pNext = pTail->m_Next;

            // this "if" is necessary, because if the tailnode and tail are 
            // out-of-sync, guard is going to be wrong...
            // if tail has moved, start over.
            if( pTail != m_Tail.load() )
                continue;

            if( pNext )
            {
                // here, pNext is likely to be pTail->pNext
                m_Tail.compare_exchange_weak( pTail, pNext );
                continue;
            } 

            // If the node that the tail points to is the last node
            // then update the last node to point at the new node.
            if( pTail->m_Next->compare_exchange_weak( pNext, &Node ) )
            {
                // If the tail points to what we thought was the last node
                // then update the tail to point to the new node.
                m_Tail.compare_exchange_weak( pTail, &Node );
                break;
            }

        } while(true);
    }

    inline void Clear ( void ) noexcept
    {
        m_Head = nullptr;
        m_Tail = nullptr;
    }

public:

    x_atomic<t_entry*>       m_Head     {nullptr};
    x_atomic<t_entry*>       m_Tail     {nullptr};
};

//------------------------------------------------------------------------------
// Description:
//      Simple qt lockless stack aba is here
//------------------------------------------------------------------------------
template< typename T >
class x_ll_mpmc_stack 
{
    x_object_type( x_ll_mpmc_stack, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:
    
    inline void push( T& Node ) noexcept
    {
        T* pLocalReality = m_Head.load( std::memory_order_relaxed );
        do
        {
            Node.m_pNext.store( pLocalReality, std::memory_order_relaxed );
        } while( !m_Head.compare_exchange_weak( pLocalReality, &Node, std::memory_order_release, std::memory_order_relaxed ) );
    }
    
    inline T* pop( void ) noexcept
    {
        T* pLocalReality = m_Head.load( std::memory_order_relaxed );
        while( pLocalReality && !m_Head.compare_exchange_weak( pLocalReality, pLocalReality->m_pNext, std::memory_order_acquire, std::memory_order_relaxed ) );
        return pLocalReality;
    }
    
protected:
    
    alignas( x_target::getCacheLineSize() )  std::atomic<T*> m_Head  {nullptr};
};

//------------------------------------------------------------------------------
// Description:
//      This hash table algorithm is unable to delete entries
//------------------------------------------------------------------------------
template< typename T_ENTRY, typename T_KEY >
class x_ll_mpmc_nondeletable_hashtable
{
    x_object_type( x_ll_mpmc_nondeletable_hashtable, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    inline void setup( s32 HashSize ) noexcept
    {
        m_List.Alloc(HashSize);
        m_List.MemSet(0);
    }

    inline void msgDestroy( void ) noexcept
    {
        m_List.msgDestroy();
    }

    inline xuptr computeHash( T_KEY Key ) const noexcept
    {
        return static_cast<xuptr>( x_murmurHash3( Key )%m_List.getCount() );
    }

    constexpr xuptr IncIterator( xuptr i ) const noexcept
    {
        return ( (i+1) == m_List.getCount() ) ? 0 : (i+1);
    }

    constexpr xuptr DecIterator( xuptr i ) const noexcept
    {
        return ( (i-1) == -1 ) ? m_List.getCount() : (i-1);
    }

    inline void AddItem ( T_KEY Key, T_ENTRY Value ) noexcept
    {
        assert( Key != 0 );
        assert( Value != 0 );

        xuptr i = computeHash(Key);
        do
        {
            auto& Node      = m_List[i];
            T_KEY StoredKey = Node.m_Key.load( std::memory_order_relaxed );
            do
            {
                if( StoredKey != 0 ) 
                {
                    // you can not add the same key twice
                    assert( StoredKey != Key );
                    break;
                }
                               
                if( !Node.m_Key.compare_exchange_weak( StoredKey, Key, std::memory_order_release ) )
                {
                    Node.m_Value.store( Value, std::memory_order_release );
                    return;
                }

            }  while( true );

            // Increment the counter
            i = IncIterator( i );

        } while( true );
    }

    inline T_ENTRY getItem ( T_KEY Key ) const noexcept
    {
        xuptr i = computeHash(Key);
        do
        {
            const auto& Node      = m_List[i];
            const T_KEY StoredKey = Node.m_Key.load( std::memory_order_relaxed );

            if( StoredKey == Key )
                return Node.m_Value.load( std::memory_order_relaxed );

            if( StoredKey == 0 )
                return 0;
            
            // Increment the counter
            i = IncIterator( i );

        } while( true );
    }

    inline xuptr getItemCountSlow( void ) noexcept
    {
        xuptr Count = 0;
        for( xuptr i = 0; i < m_List.getCount(); i++ )
            if( m_List[i].m_Key.load( std::memory_order_relaxed ) ) Count++;
        return Count;
    }

protected:

    using spinlock = x_lk_spinlock::base< x_lk_spinlock::non_reentrance, x_lk_spinlock::do_nothing >;
    
    struct node
    {
        x_atomic< T_KEY >           m_Key;
        x_atomic< T_ENTRY >         m_Value;
        spinlock                    m_Lock;
    };
    
private:
    
    xndptr<node>    m_List {};
};

//------------------------------------------------------------------------------
// Description:
//      This code is for reference only
//      Array Specialization of the x_fixed_pool_general.
//      reference: https://github.com/Amanieu/unvanquished-engine/blob/master/src/Core/Thread/ThreadPool.cpp
//------------------------------------------------------------------------------
/*
template<typename T> 
class x_ll_steal_queue 
{
    x_object_type( x_ll_steal_queue, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:
	x_ll_steal_queue( s32 Length ) noexcept : 
        m_nEntries{ Length } 
	{
		m_pItem = x_new_arrayv( entry, Length );
	}

	~x_ll_steal_queue( void ) noexcept
	{
        x_delete_array( m_pItem, m_nEntries );
	}

	// Push a job to the tail of this thread's queue
	void push( T& Job ) noexcept
	{
		s32 iTail = m_iTail.load( std::memory_order_relaxed );

		// Check if we have space to insert an element
		if( iTail == m_nEntries ) 
        {
			// Lock the queue
            std::lock_guard<spinlock>       Locked{ m_Lock };
            const s32                       iHead { m_iHead.load( std::memory_order_relaxed ) };

            // Reset ihead and the local tail
			iTail -= iHead;
			m_iHead.store( 0, std::memory_order_relaxed );

			// Resize the queue if it is more than 75% full
			if( iTail > (m_nEntries - (m_nEntries / 4)) ) 
            {
				entry*  pOld        = m_pItem;
                s32     OldCount    = m_nEntries;

				m_nEntries *= 2;
				m_pItem     = x_new_arrayv( entry, m_nEntries );
                x_assume( iTail >= iHead );
                memcpy( m_pItem, &pOld[iHead], sizeof(entry) * iTail );               
                x_delete_array( pOld, OldCount );
			} 
            else 
            {
				// Simply shift the items to free up space at the end
                memcpy( m_pItem, &m_pItem[iHead], sizeof(entry) * iTail );
			}
		}

		// Now add the job
		m_pItem[ iTail ] = &Job;
		m_iTail.store( iTail + 1, std::memory_order_release);
	}

	// Pop a job from the tail of this thread's queue
	T* pop( void ) noexcept
	{
		s32 iTail = m_iTail.load( std::memory_order_relaxed );

		// Early exit if our queue is empty
		if( m_iHead.load(std::memory_order_relaxed) >= iTail )
			return nullptr;

		// Make sure tail is stored before we read head
		iTail--;
		m_iTail.store( iTail, std::memory_order_relaxed );
		std::atomic_thread_fence( std::memory_order_seq_cst );

		// Race to the queue
		if( m_iHead.load(std::memory_order_relaxed) <= iTail )
			return m_pItem[ iTail ];

		// There is a concurrent steal, lock the queue and try again
		std::lock_guard<spinlock> Locked( m_Lock );

		// Check if the item is still available
		if( m_iHead.load(std::memory_order_relaxed) <= iTail )
			return m_pItem[ iTail ];

		// Otherwise restore the tail and fail
		m_iTail.store( iTail + 1, std::memory_order_relaxed );
		return nullptr;
	}

	// Steal a job from the head of this thread's queue
	T* steal( void ) noexcept
	{
		// Lock the queue to prevent concurrent steals
		std::lock_guard<spinlock> Locked( m_Lock );

		// Make sure head is stored before we read tail
		s32 iHead = m_iHead.load( std::memory_order_relaxed );
		m_iHead.store( iHead + 1, std::memory_order_relaxed );
		std::atomic_thread_fence( std::memory_order_seq_cst );

		// Check if there is a job to steal
		if( iHead < m_iTail.load(std::memory_order_relaxed) ) 
        {
			// Need acquire fence to synchronize with concurrent push
			std::atomic_thread_fence( std::memory_order_acquire );
			return m_pItem[ iHead ];
		}

		// Otherwise restore the head and fail
		m_iHead.store( iHead, std::memory_order_relaxed );
		return nullptr;
	}

protected:

    using entry = T*;
    using spinlock = x_lk_spinlock::base< x_lk_spinlock::non_reentrance, x_lk_spinlock::do_nothing >;

protected:

	entry*                  m_pItem;	
    s32                     m_nEntries;
    spinlock                m_Lock      {};
	std::atomic<s32>        m_iHead     { 0 };
    std::atomic<s32>        m_iTail     { 0 };
};
*/

//------------------------------------------------------------------------------
// Description:
//      A lockless paged pool. It does however locks when a new page needs to be created.
//------------------------------------------------------------------------------

template< typename T_ENTRY, xuptr T_ENTRIES_PER_PAGE_COUNT >
class x_ll_page_pool_jitc
{
    x_object_type( x_ll_page_pool_jitc, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    using   t_fixedpool = x_ll_fixed_pool_static_jitc<T_ENTRY, T_ENTRIES_PER_PAGE_COUNT >;
    using   t_entry     = typename t_fixedpool::t_entry;

    struct page : public t_fixedpool 
    {
        page*  m_pNext {};
        page*  m_pPrev {};
    };

    using pagepool_linked_list  =  x_locked_object< x_linear_link_list_pool< page >,  x_lk_semaphore_smart_lock<> >;

public:

    constexpr x_ll_page_pool_jitc( void ) noexcept = default;

    //------------------------------------------------------------------------
    template<typename ...T_ARG> x_inline
    t_entry& pop( T_ARG&&...Args ) noexcept
    {
        return *x_construct( t_entry, &popDontConstruct(), { Args... } );
    }

    //------------------------------------------------------------------------
    t_entry& popDontConstruct( void ) noexcept
    {    
        xuptr CurrentPageCount;
        
        //
        // Try to find a free page
        //
        {
            // we can lock as read only since the pagepool itself is lockless
            // and we get the pagepool as a normal
            x_lk_guard_as_const_get( m_PageList, PageList );
           
            CurrentPageCount = PageList.getCount();
            for( auto& Entry : PageList )
            {
                auto* pEntry = Entry.popDontConstruct();
                if( pEntry ) return *pEntry;
            }
        }

        //
        // Lets add a page 
        //
        {
            // we lock here as write because we are about to add a page
            x_lk_guard_get( m_PageList, PageList );

            // Did someone added a page since last time we checked?
            if( CurrentPageCount == PageList.getCount() )
            {
                // Lets add a page
                page& Page = PageList.pop();
                return *Page.popDontConstruct();
            }
        }
      
        //
        // We fail to add a page lets try again
        //
        return popDontConstruct();
    }

    constexpr xuptr getCapacity ( void ) const noexcept { x_lk_guard_get( m_PageList, PageList ); return PageList.getCount(); }
    inline    xuptr getCount    ( void ) const noexcept 
    {
        x_lk_guard_get( m_PageList, PageList ); 
        xuptr Total = 0; 
        for( auto& PageEntry : PageList ) 
            Total += PageEntry.getCount(); return Total; 
    }

    //------------------------------------------------------------------------
    void push( t_entry& Entry ) noexcept
    {
        //
        // Check which page it belongs
        //
        {
            // we lock as constant since the pagepool is lockless so readonly is fine
            // we get it normally
            x_lk_guard_as_const_get( m_PageList, PageList );
            for( auto& PageEntry : PageList )
            {
                if( PageEntry.Belongs( &Entry ) )
                {
                    PageEntry.push( Entry );
                    return;
                }
            }
        }
     
        // We could not find the page that this entry belongs
        x_assert( false );
    }

protected:

 //   using pagepool_linked_list  =   x_linear_link_list< t_fixedpool >;
 //   using page_list             =   x_locked_object< pagepool_linked_list, x_lk_semaphore_smart_lock<> >;
 //   using page                  =   typename pagepool_linked_list::t_entry;

//    static_assert( page_list::t_is_quantum && page_list::t_is_lockfree == false, "Error creating a locked object" );

protected:

    pagepool_linked_list       m_PageList{};
};
  

//------------------------------------------------------------------------------
// Description:
//      A lockless paged pool. It does however locks when a new page needs to be created.
//------------------------------------------------------------------------------

template< typename T_ENTRY, xuptr T_ENTRIES_PER_PAGE_COUNT >
class x_ll_page_pool
{
    x_object_type( x_ll_page_pool, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    using   t_fixedpool = x_ll_fixed_pool_static< T_ENTRY, T_ENTRIES_PER_PAGE_COUNT >;
    using   t_entry     = typename t_fixedpool::t_entry;

public:

    constexpr x_ll_page_pool( void ) noexcept = default;

    //------------------------------------------------------------------------
    x_inline t_entry& pop    ( void ) noexcept
    {    
        xuptr CurrentPageCount;
       
        //
        // Try to find a free page
        //
        {
            // we can lock as read only since the pagepool itself is lockless
            // and we get the pagepool as a normal
            x_lk_guard_as_const( m_Lock );
           
            CurrentPageCount = m_PageList.getCount();
            for( auto& Entry : m_PageList )
            {
                auto* pEntry = Entry.pop();
                if( pEntry ) return *pEntry;
            }
        }
       
        //
        // Lets add a page 
        //
        {
            // we lock here as write because we are about to add a page
            x_lk_guard( m_Lock );

            // Did someone added a page since last time we checked?
            if( CurrentPageCount == m_PageList.getCount() )
            {
                t_fixedpool& Page = m_PageList.pop();
                return *Page.pop();
            }
        }
        
        //
        // We fail to add a page lets try again
        //
        return pop();
    }

    constexpr xuptr getCapacity ( void ) const noexcept { x_lk_guard_get( m_PageList, PageList ); return PageList.getCount(); }
    inline    xuptr getCount    ( void ) const noexcept 
    {
        x_lk_guard_get( m_PageList, PageList ); 
        xuptr Total = 0; 
        for( auto& PageEntry : PageList ) 
            Total += PageEntry.getCount(); return Total; 
    }

    //------------------------------------------------------------------------
    void push( t_entry& Entry ) noexcept
    {
        //
        // Check which page it belongs
        //
        {
            // we lock as constant since the pagepool is lockless so readonly is fine
            // we get it normally
            x_lk_guard_as_const( m_Lock );
            for( auto& PageEntry : m_PageList )
            {
                if( PageEntry.Belongs( &Entry ) )
                {
                    PageEntry.push( Entry );
                    return;
                }
            }
        }
     
        // We could not find the page that this entry belongs
        x_assert( false );
    }

    //------------------------------------------------------------------------
    void SanityCheck( void ) noexcept
    {
        //
        // Check which page it belongs
        //
        {
            // we lock as constant since the pagepool is lockless so readonly is fine
            // we get it normally
            x_lk_guard_as_const( m_Lock );
            for( auto& PageEntry : m_PageList )
            {
                PageEntry.SanityCheck();
            }
        }
    }

protected:

    x_lk_semaphore_smart_lock<>                 m_Lock      {};
    x_linear_link_list_pool< t_fixedpool >      m_PageList  {};
};

//------------------------------------------------------------------------------
// Description:
//          This guys is a quantum object
//------------------------------------------------------------------------------
template< typename, typename T_UNIQUE_TYPE = xuptr > class x_ll_share_ref;

//------------------------------------------------------------------------------
class x_ll_share_object 
{
    x_object_type( x_ll_share_object, is_quantum_lock_free );

public:

    //---------------------------------------------------------------------------------------
    template< typename T_CLASS > inline 
    static bool t_AddSharedReference ( T_CLASS* const pData ) noexcept
    { 
        x_assert( pData );
        pData->x_ll_share_object::m_RefCount++;
        return true;
    }

    //---------------------------------------------------------------------------------------
    template< typename T_CLASS > inline 
    static void t_SubtractSharedReference ( T_CLASS* const pData ) noexcept
    { 
        x_assert( pData );
        if( --pData->x_ll_share_object::m_RefCount == 0 )
        {
            T_CLASS::t_DeleteSharedInstance( pData );
        }
    }

protected:

    enum : bool { t_share_object = true };

private:

    //---------------------------------------------------------------------------------------
    // Default shared instance free from memory, override this one when new method is needed
    template< typename T_CLASS > inline 
    static void t_DeleteSharedInstance ( T_CLASS* const pData ) noexcept
    {
        x_assert( pData );
        x_delete( pData );
    }

    //---------------------------------------------------------------------------------------
    // Default shared instance new from memory, override this one when new method is needed
    template< typename T_CLASS, typename ...T_ARG > inline 
    static T_CLASS* t_NewSharedInstance ( T_ARG&&...Args ) noexcept
    {
        return x_new( T_CLASS, { Args... } );
    }
    
private:

    x_atomic<xuptr>             m_RefCount { 1 };

private:

    template< typename, typename > friend class x_ll_share_ref;
};

//------------------------------------------------------------------------------
// Description:
//          This class is similar to the std::shareptr but more restrictive.
//          it can only point to a x_ll_share_object where it has a ref count.
//          There is a T_UNIQUE_TYPE which helps diferenciate an intance from another.
//------------------------------------------------------------------------------
template< typename T_SHARE_OBJECT, typename T_UNIQUE_TYPE > 
class x_ll_share_ref
{
    x_object_type( x_ll_share_ref, is_linear );

public:

    using t_shared      = T_SHARE_OBJECT;
    using t_unique_type = T_UNIQUE_TYPE;

    static_assert( t_shared::t_share_object, "The share object must be derived from a x_ll_share_object" );

public:

    x_ll_share_ref( void ) noexcept = default;
    
    constexpr bool isValid( void ) const noexcept { return m_pShareObject != nullptr; }

    // create an instance
    template< typename ...T_ARG >
    void New( T_ARG&&...Args ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );

        m_pShareObject = t_shared::template t_NewSharedInstance<t_shared,T_ARG...>( std::forward<T_ARG>(Args)... );
    }

    // add ref
    inline void AddReference( void ) noexcept
    {
        x_assert(m_pShareObject);
        t_shared::t_AddSharedReference( m_pShareObject );
    }

    // sub ref
    inline void SubReference( void ) noexcept
    {
        x_assert(m_pShareObject);
        t_shared::t_SubtractSharedReference( m_pShareObject );
    }

    // set to null
    void setNull( void ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = nullptr;
    }

    // Transfer owner ship
    void TransferOwnerShip( xowner<t_shared*> pObject ) noexcept
    {
        x_assert(pObject);
        x_assert(pObject->m_RefCount == 1);
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = pObject;
    }

    // add ref
    constexpr x_ll_share_ref( t_shared* pShareObj ) noexcept 
    : m_pShareObject{ pShareObj }
    {
        if( m_pShareObject ) t_shared::t_AddSharedReference( m_pShareObject );
    }

    // add ref
    constexpr x_ll_share_ref( t_shared& ShareObj ) noexcept 
    : m_pShareObject{ &ShareObj }
    {
        t_shared::t_AddSharedReference( m_pShareObject );
    }

    // add ref
    constexpr x_ll_share_ref( t_shared&& ShareObj ) noexcept
    : m_pShareObject { &ShareObj }
    {
        t_shared::t_AddSharedReference( m_pShareObject );
    }

    // copy ref
    constexpr x_ll_share_ref( t_self& ShareObj ) noexcept 
    : m_pShareObject{ ShareObj.m_pShareObject }
    {
        if( ShareObj.m_pShareObject ) t_shared::t_AddSharedReference( m_pShareObject );
    }

    // move ref
    constexpr x_ll_share_ref( t_self&& ShareObj ) noexcept
    : m_pShareObject { ShareObj.m_pShareObject }
    {
        ShareObj.m_pShareObject = nullptr;
    }

    // smart add ref
    inline x_ll_share_ref& operator = ( t_shared& ShareRef ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = &ShareRef;
        t_shared::t_AddSharedReference( m_pShareObject );
        return *this;
    }

    // smart move ref
    inline x_ll_share_ref& operator = ( t_self&& ShareObj ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = ShareObj.m_pShareObject;
        ShareObj.m_pShareObject = nullptr;
        return *this;
    }

    // smart copy ref
    inline x_ll_share_ref& operator = ( t_self& ShareObj ) noexcept
    {
        if( m_pShareObject == ShareObj.m_pShareObject ) 
            return *this;

        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
        m_pShareObject = ShareObj.m_pShareObject;
        t_shared::t_AddSharedReference( m_pShareObject );

        return *this;
    }

    // smart release
    inline ~x_ll_share_ref( void ) noexcept
    {
        if( m_pShareObject ) t_shared::t_SubtractSharedReference( m_pShareObject );
    }

    inline      t_shared*               operator ->                     ( void )        noexcept { x_assert(m_pShareObject); return  m_pShareObject;  }
    constexpr   const t_shared*         operator ->                     ( void ) const  noexcept { x_assert(m_pShareObject); return  m_pShareObject;  }
    inline                              operator T_SHARE_OBJECT*        ( void )        noexcept { x_assert(m_pShareObject); return  m_pShareObject;  }
    inline                              operator T_SHARE_OBJECT&        ( void )        noexcept { x_assert(m_pShareObject); return *m_pShareObject;  }
    constexpr                           operator const T_SHARE_OBJECT*  ( void ) const  noexcept { x_assert(m_pShareObject); return  m_pShareObject;  }
    constexpr                           operator const T_SHARE_OBJECT&  ( void ) const  noexcept { x_assert(m_pShareObject); return *m_pShareObject;  }
    constexpr const T_SHARE_OBJECT&     operator *                      ( void ) const  noexcept { x_assert(m_pShareObject); return *m_pShareObject;  }
    inline      T_SHARE_OBJECT&         operator *                      ( void )        noexcept { x_assert(m_pShareObject); return *m_pShareObject;  }

protected:

    t_shared*                   m_pShareObject{ nullptr };
};

//-------------------------------------------------------------------------------------------
// Description: 
//          Pool which try its best to keep allocated nodes as contiguous as possible.
//          So as cache friendly as possible.
//          Certain patterns of allocations could make this version very slow.  
// todo: it may be possible to make it faster by replacing m_nFreeEntries with another 64bit mask
//       that summarizes the status of the page. This mask will not only tell you if there are free
//       entries but also where you will need to jump in the mask array to search for them.
template< typename T_ENTRY, xuptr T_MAX_ENTRIES >
class x_ll_page_ordered_pool_jitc
{
    x_object_type( x_ll_page_ordered_pool_jitc, is_quantum_lock_free, is_not_copyable, is_not_movable )

public:

    using           t_entry         = T_ENTRY;

    struct iterator
    {
        t_self&     m_Obj;
        u64         m_Bits;
        int         m_iPage;
        int         m_iEntry; 

        inline iterator( t_self& Obj ) noexcept :
            m_Obj{ Obj },
            m_Bits{ 0 },
            m_iPage{ 0 },
            m_iEntry{ -1 } 
            { 
                // Search for a page that has something in it
                while( m_Obj.m_PageList[m_iPage].m_JumpToFree.load().m_Next >= ENTRIES_PER_BIT )
                {
                    m_iPage++;
                    if( m_iPage == NUM_PAGES ) return;
                }

                // Set all the index
                this->operator++(); 
            }

        constexpr iterator( t_self& Obj, bool ) noexcept :
            m_Obj{ Obj },
            m_Bits{ 0 },
            m_iPage{ 0 },
            m_iEntry{ 0 } {}

        inline  const iterator& operator ++ ( void ) noexcept                                               
        { 
            int ModedEmptyIndex;
            do 
            {
                ++m_iEntry;
                ModedEmptyIndex = (m_iEntry & 0x3f);
                
                // Move to the next set of bit blocks
                if( 0 == ModedEmptyIndex  )
                {
                    // Check to see if we are done with this page
                    if( m_iEntry == ENTRIES_PER_PAGE )
                    {
                        m_iEntry = 0;

                        // Search for a page that has something in it
                        do 
                        {
                            m_iPage++;
                            if( m_iPage == NUM_PAGES ) return *this;

                        } while( m_Obj.m_PageList[m_iPage].m_JumpToFree.load().m_Next >= ENTRIES_PER_BIT );
                    }

                    // get a new set of bits
                    m_Bits = m_Obj.m_PageListFreeMask[m_iPage][m_iEntry/64].load();
                    if( m_Bits == 0xffffffffffffffffu )
                    {
                        m_iEntry += 64 - 1;
                        continue;
                    }
                }

            } while( 1 & (m_Bits >> ModedEmptyIndex) ); 

            return *this; 
        }
        
        constexpr bool operator != ( const iterator& ) const noexcept                          
        { 
            return ( m_iPage != NUM_PAGES ) ? m_Obj.m_PageList[m_iPage].m_RawPool.isValid() : false;
        }

        constexpr       const auto&         operator *      ( void )        const   noexcept        { return (*m_Obj.m_PageList[m_iPage].m_RawPool)[m_iEntry]; }
        inline          auto&               operator *      ( void )                noexcept        { return (*m_Obj.m_PageList[m_iPage].m_RawPool)[m_iEntry]; }
    };

public:

    //------------------------------------------------------------------------
    inline      iterator        begin   ( void )            noexcept    { return { *this };             }
    inline      iterator        end     ( void )            noexcept    { return { *this, false };      }

    constexpr   iterator        begin   ( void )    const   noexcept    { return { *this };             }
    constexpr   iterator        end     ( void )    const   noexcept    { return { *this, false };      }

    //------------------------------------------------------------------------
    template<typename ...T_ARG>
    t_entry& pop    ( T_ARG&&...Args ) noexcept
    {
        xuptr i = 0;
        for( auto& Page : x_iter_ref( i, m_PageList ) )
        {
            //
            // Make sure that are enough entries allocated
            //
            if( Page.m_RawPool.isValid() == false )
            {
                x_lk_guard( m_PageAllocLock );
                if( Page.m_RawPool.isValid() == false )
                {
                    Page.m_RawPool.New();
                }
                else
                {
                    // Someone allocated before us then try again the hold thing just to make sure
                    i = -1;
                    continue;
                }
            }

            // Since we have allocated one entry in this page we are force to pick one from here
            // so keep looping until we find one
            auto&   FreeMaskArray   = m_PageListFreeMask[i];
            auto    TagFree         = Page.m_JumpToFree.load();
            for( auto iFree = TagFree.m_Next; iFree < ENTRIES_PER_BIT; iFree++  )
            {
                auto& FreeMask = FreeMaskArray[iFree];
                auto  Local    = FreeMask.load();

                // Find which bit is free if none then move on to next mask
                while( Local )
                {
                    // bin search for the first bit which is 1 (free entry)
                    int  c = 1;
                    {
                        auto v = Local;
                        if( (v & 0xffffffff) == 0 ) {  v >>= 32;  c += 32;}
                        if( (v & 0xffff)     == 0 ) {  v >>= 16;  c += 16;}
                        if( (v & 0xff)       == 0 ) {  v >>= 8;   c += 8; }
                        if( (v & 0xf)        == 0 ) {  v >>= 4;   c += 4; }
                        if( (v & 0x3)        == 0 ) {  v >>= 2;   c += 2; }
                        c -= v & 0x1;
                    }

                    // Try setting the new entry
                    const auto New = Local & (~(u64{1} << c));
                    if( FreeMask.compare_exchange_weak( Local, New ) )
                    {
                        const u16 NodeMoreNodes = (New == ~u64{ 0 });

                        // Update the next free if we can
                        if( iFree != TagFree.m_Next || NodeMoreNodes )
                        {
                            tagnext NewTagFree { static_cast<u16>(iFree + NodeMoreNodes), TagFree.m_Tag + 1u };
                            Page.m_JumpToFree.compare_exchange_strong( TagFree, NewTagFree );
                        }

                        // We got it
                        const auto EntryIndex = (FreeMaskArray.getIndexByEntry( FreeMask )<<6) + c;
                        return *x_construct( t_entry, &(*Page.m_RawPool)[ EntryIndex ],  { Args... } );
                    }
                }
            }
        }

        // out of memory!
        x_assume( false );
    }

    //------------------------------------------------------------------------

    void push( t_entry& Entry ) noexcept
    {
        xuptr i = 0;
        for( auto& Page : x_iter_ref( i, m_PageList ) )
        {
            // Does this entry belongs to this pool?
            if( Page.m_RawPool->Belongs( &Entry ) == false )
                continue;
            
            // Ok we can destroy the object now
            x_destruct( &Entry );

            // need to mark the object as free now
            const auto  Index         = Page.m_RawPool->getIndexByEntry( Entry );
            const auto  subIndex      = Index >> 6;         // Index / 64
            const auto  bitIndex      = Index & 0x3f;       // Index % 64
            auto&       FreeMaskArray = m_PageListFreeMask[i];
            auto&       FreeMask      = FreeMaskArray[subIndex];
            auto        TagFree       = Page.m_JumpToFree.load();

            // Remove from the allocated list 
            auto Local = FreeMask.load();
            x_assert( (Local & (u64{1u}<<bitIndex)) == 0 );
            while( false == FreeMask.compare_exchange_weak( Local, Local | (u64{1u}<<bitIndex)) )
            {
                x_assert( (Local & (u64{1u}<<bitIndex)) == 0 );
            }

            // Since the priority is always lower free nodes try to update the hint if nothing has changed
            if( TagFree.m_Next > subIndex )
            {
                const auto NewTagFree = tagnext{ static_cast<u16>(subIndex), TagFree.m_Tag + 1u };
                Page.m_JumpToFree.compare_exchange_strong( TagFree, NewTagFree );
            }

            return;
        }

        x_assume( false );
    }

    //------------------------------------------------------------------------

    x_ll_page_ordered_pool_jitc( void )
    {
        m_PageListFreeMask.MemSet( ~0 );
    }

protected:

    enum
    {
        MAX_PAGES               = 24,
        ENTRIES_PER_PAGE        = x_Align( x_Max(64, T_MAX_ENTRIES / (MAX_PAGES-1) ), 64 ),
        ENTRIES_PER_BIT         = (ENTRIES_PER_PAGE / 64),                    
        NUM_PAGES               = (T_MAX_ENTRIES / ENTRIES_PER_PAGE) + (!!(T_MAX_ENTRIES % ENTRIES_PER_PAGE))
    };

    static_assert( ENTRIES_PER_BIT*64 == ENTRIES_PER_PAGE,      "make sure we have enough bits per entry" );
    static_assert( NUM_PAGES*ENTRIES_PER_PAGE >= T_MAX_ENTRIES, "The computed number of entries is less than the one requested" );
    static_assert( NUM_PAGES < MAX_PAGES,                       "Number of pages should always be less than max pages" );

    struct alignas(int) tagnext  
    {
        u16                                                     m_Next;                                 // Next block where it could be a free node
        u16                                                     m_Tag;                                  // make sure we know that has not changed
    };

    static_assert( ENTRIES_PER_BIT < 0xffff, "u16 can not hold all the max number of bits" );

    struct page_list_entry
    {
        xndptr_s<xarray_raw<t_entry,ENTRIES_PER_PAGE>>          m_RawPool       {};                     // pointer to the actual page of entries
        x_atomic<tagnext>                                       m_JumpToFree    { tagnext{0,0} };       // Number of free nodes
    };

    using  page_alloc_lock = x_lk_spinlock::base< x_lk_spinlock::non_reentrance, x_lk_spinlock::do_nothing >;
      
protected:

    xarray<page_list_entry, NUM_PAGES>                          m_PageList          {};                 // List of pages
    xarray<xarray<x_atomic<u64>,ENTRIES_PER_BIT>, NUM_PAGES>    m_PageListFreeMask  {};                 // 1 bit for each entry saying if it is been use
    page_alloc_lock                                             m_PageAllocLock     {};                 // Lock whenever we need to allocate a new page
};


