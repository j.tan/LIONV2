//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class buffer
{
public:

    //-----------------------------------------------------------------------

    constexpr                       buffer              ( device& Device )                      noexcept : m_Device{ Device } {};
                                   ~buffer              ( void )                                noexcept; 
                                    buffer              ( buffer&& buf )                        noexcept;
    inline          void            TransferData        ( const xbuffer_view<xbyte*> SrcData )  noexcept;
    x_orinlineconst   VkDeviceSize    getCount            ( void )                    const       noexcept { x_assert_quantum( m_Debug_LQ ); return m_Allocation.m_Size / m_EntrySize;                }
    x_orinlineconst                   operator VkBuffer   ( void )                    const       noexcept { x_assert_quantum( m_Debug_LQ ); return m_Buffer;                                         }
    x_orinlineconst   VkBuffer        getVKBuffer         ( void )                    const       noexcept { x_assert_quantum( m_Debug_LQ ); return m_Buffer;                                         }
    x_orinlineconst   auto            getVKBufferOffset   ( void )                    const       noexcept { x_assert_quantum( m_Debug_LQ ); return static_cast<uint32_t>(m_Allocation.m_Offset);     }

    inline          void            CreateBuffer        (   int                 EntrySize, 
                                                            xuptr               Count,
                                                            VkBufferUsageFlags  Usage,
                                                            VkSharingMode       Sharing             = VK_SHARING_MODE_EXCLUSIVE,
                                                            uint32_t            nFamilyIndex        = 0, 
                                                            uint32_t*           pQueueFamilyIndices = nullptr ) noexcept;

    //-----------------------------------------------------------------------

    template<typename T>
    T* CreateMap( void ) noexcept
    {
        x_assert_linear( m_Debug_LQ ); 

    /*
        memory_pool&    MemoryPool  =  s_lMemoryPools[ g_context::get().m_Scheduler.getWorkerUID() ];
        void*           pData       = nullptr;
        VkResult        VKErr       = vkMapMemory( 
            MemoryPool.getEngine().getDevice(), 
            m_Allocation.m_VKDeviceMemory, 
            m_Allocation.m_Offset,
            m_Allocation.m_Size, 
            0, 
            &pData );
        x_assert( !VKErr );
        return reinterpret_cast<T*>(pData);
        */
        return reinterpret_cast<T*>(m_Allocation.m_pSystemPtr);
    }

    //-----------------------------------------------------------------------

    void ReleaseMap( void ) noexcept;

protected:

#if _X_DEBUG
    x_debug_linear_quantum      m_Debug_LQ  {};
#endif

    device&                     m_Device;
    memory_pool::allocation     m_Allocation    {};
    VkBuffer                    m_Buffer        { VK_NULL_HANDLE };
    int                         m_EntrySize     {-1};
};

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
#if 0
class buffer_global_pool
{
/*
public:
    struct setup
    {
        int                     m_MaxVerticesPerBuffer  { 32*1042 };
        int                     m_MaxIndicesPerBuffer   { 32*1042 };
        int                     m_VertexEntrySize       { sizeof(draw::vertex) };
    };
*/

public:
    inline                          buffer_global_pool          ( device& Device )                                      noexcept;
    inline                         ~buffer_global_pool          ( void )                                                noexcept;
    inline      auto                Initialize                  ( /*const setup& Setup = setup{}*/ )                        noexcept;
    inline      display_cmds&       AllocateDisplayCmd          ( const int FrameIndex, const xuptr Size )                noexcept;
//    inline      void                SubmitCmdList               ( const int FrameIndex, display_cmds& CmdList )         noexcept;

protected:

    enum : int { MAX_DISPLAY_CMDS = 128 };
    struct in_used_cmds
    {
        int                             m_nCmdInUsed    { 0 };
        xarray<s16,MAX_DISPLAY_CMDS>    m_lInUsed       {s16(-1)};
    };

    struct buffer_entry : public buffer
    {

    };

protected:

    inline      void                onJustAfterPageFlip         ( void )                                                noexcept;

protected:
    
    device&                                 m_Device;
    int                                     m_nFreeEntries          {MAX_DISPLAY_CMDS};         // 
    xarray<in_used_cmds,2>                  m_InUsedCmdLists        {};                         // Frame dependent in used commands lists
    xarray<s16,MAX_DISPLAY_CMDS>            m_FreeCmds              {};                         // Array to indices of cmds that are free
    xvector<buffer_entry>                   m_BufferPool;                                       // The actual list of buffers. (This is frame indipendent)
    x_message::delegate<buffer_global_pool> m_DelegateJustAfterPageFlip;                        // Delegate responsable to route messages for Just After Page Flip
};
#endif

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

class buffer_paged
{
public:

    struct alloc_return { void* m_pStart; xuptr m_Offset; int m_iBuffer; xuptr m_Count; };

    struct setup
    {
        VkBufferUsageFlags      m_Flags                 { VK_NULL_HANDLE };
        int                     m_MaxBuffers            { -1 };
        int                     m_EntryPerBuffer        { -1 };
        int                     m_EntrySize             { -1 };
    };

public:

    constexpr                           buffer_paged        ( device& Device )              noexcept : m_Device{ Device } {};
    inline          void                Initialize          ( const setup& Setup )          noexcept;
    inline          void                Upload              ( void )                        noexcept;
    inline          alloc_return        Alloc               ( xuptr nEntires )              noexcept;
    inline          auto                getVKBuffer         ( int iBuffer )         const   noexcept { x_assert_quantum( m_Debug_LQ ); return m_EngBuffer[iBuffer].getVKBuffer();         }
    inline          auto                getVKBufferOffset   ( int iBuffer )         const   noexcept { x_assert_quantum( m_Debug_LQ ); return m_EngBuffer[iBuffer].getVKBufferOffset();   }

protected:

    struct entry
    {
        xbyte*      m_pStart        { nullptr   };
        xuptr       m_nEntriesLeft  { xuptr(~0) };
        int         m_iBuffer       { -1        };
    };

    using spinlk = x_lk_spinlock::base< x_lk_spinlock::non_reentrance, x_lk_spinlock::do_nothing >;

protected:

#if _X_DEBUG
    x_debug_linear_quantum  m_Debug_LQ  {};
#endif

    spinlk                  m_SpinLock              {};
    device&                 m_Device;
    xndptr<buffer>          m_EngBuffer             {};
    xndptr<entry>           m_lEntries              {};
    VkBufferUsageFlags      m_Flags                 { VK_NULL_HANDLE };
    int                     m_MaxBuffers            { -1 };
    int                     m_EntryPerBuffer        { -1 };
    int                     m_EntrySize             { -1 };
    int                     m_nActiveBuffers        { -1 };
};

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

class buffer_double_paged
{
public:

    x_units( int, i_index   );
    x_units( int, i_vertex  );

    struct index_alloc     { u32*   m_pStart; uint32_t m_Offset; i_index   m_iBuffer; xuptr m_Count; };
    struct vertex_alloc    { void*  m_pStart; uint32_t m_Offset; i_vertex  m_iBuffer; xuptr m_Count; };

    struct setup
    {
        int                     m_MaxVertexBuffers      { -1 };
        int                     m_MaxIndexBuffers       { -1 };
        int                     m_MaxVerticesPerBuffer  { -1 };
        int                     m_MaxIndicesPerBuffer   { -1 };
        int                     m_VertexEntrySize       { -1 };
    };

    //-----------------------------------------------------------------------
public:

    inline                                  buffer_double_paged         ( device& Device )                  noexcept;
    inline      void                        Initialize                  ( const setup& Setup, const xbuffer_view<tools::vertex_desc::attribute> VertexDesc ) noexcept;
    inline      vertex_alloc                AllocVertex                 ( xuptr Count )                     noexcept;
    inline      index_alloc                 AllocIndex                  ( xuptr Count )                     noexcept;
    inline      tools::vertex_desc&         getVertDesc                 ( void )                            noexcept { return m_VDesc; }
    inline      auto                        getVertexVKBuffer           ( i_vertex iBuffer )        const   noexcept;
    inline      auto                        getVertexVKBufferOffset     ( i_vertex iBuffer )        const   noexcept;
    inline      auto                        getIndexVKBuffer            ( i_index  iBuffer )        const   noexcept;
    inline      auto                        getIndexVKBufferOffset      ( i_index  iBuffer )        const   noexcept;

protected:
    
    enum { FRAMES_BEFORE_REUSING_BUFFER = 2 };
                                                                                          
protected:

    inline      void                        onJustAfterPageFlip         ( void )                            noexcept;
    inline      void                        onJustBeforePageFlip        ( void )                            noexcept;

protected:

    device&                                     m_Device;
    tools::vertex_desc                          m_VDesc                         {};
    buffer_paged                                m_VertexBuffer[FRAMES_BEFORE_REUSING_BUFFER];
    buffer_paged                                m_IndexBuffer[FRAMES_BEFORE_REUSING_BUFFER];
    int                                         m_Index                         {0};
    x_message::delegate<buffer_double_paged>    m_DelegateJustBeforePageFlip;                       // Delegate responsable to route messages for Just After Page Flip
    x_message::delegate<buffer_double_paged>    m_DelegateJustAfterPageFlip;                        // Delegate responsable to route messages for Just After Page Flip
};

