//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

// Make sure to set the following env variables in the computer so the validation layers work.
// VK_LAYER_PATH   = c:/.... /LION/3rdPary/Vulkan/Bin 
// LD_LIBRARY_PATH = c:/.... /LION/3rdPary/Vulkan/Bin
// This is require for vulkan to debug properly 


#define VK_USE_PLATFORM_WIN32_KHR
#include "vulkan.h"

#define VK_FLAGS_NONE           0


#include "../eng_base.h"

#include "../Implementation/engPC/eng_PCSystemWindow.h"

//------------------------------------------------------------------------------
// Logging system for the engine
//------------------------------------------------------------------------------
#define ENG_WARNINGLOG(ENG_INSTANCE,MESSAGE) X_LOG_CHANNEL( ENG_INSTANCE.getErrorLogChannel(), "WARNING: ( " << __FILE__ << ", " << __LINE__ << " ) " << MESSAGE )
#define ENG_ERRORLOG(ENG_INSTANCE,MESSAGE)   X_LOG_CHANNEL( ENG_INSTANCE.getErrorLogChannel(), "ERROR:   ( " << __FILE__ << ", " << __LINE__ << " ) " << MESSAGE )
#define ENG_INFOLOG(ENG_INSTANCE,MESSAGE)    X_LOG_CHANNEL( ENG_INSTANCE.getInfoLogChannel(),  "INFO: " << MESSAGE )

namespace eng_vk
{
    class instance;
    class device;
    class window;
    class display_cmds;
    class texture;
    class material_informed;
    class engpipeline;

    template< typename T > inline
    auto& HandleToObject( const typename T::handle& Handle ) noexcept
    {
        return *reinterpret_cast<T*>( &*const_cast<typename T::handle&>(Handle) );
    }
//     template auto&  HandleToObject<texture>             ( const typename texture::handle&           Handle ) noexcept;

    #include "eng_vkMemoryPool.h"
    #include "eng_vkSwapChain.h"
    #include "eng_vkWindow.h"
    #include "eng_vkInstance.h"
    #include "eng_vkPipeline.h"
    #include "eng_vkTools.h"
    #include "eng_vkTexture.h"
    #include "eng_vkUniformStream.h" 
    #include "eng_vkEngPipeline.h"
    #include "eng_vkSampler.h"
    #include "eng_vkShader.h"
    #include "eng_vkBuffer.h"
    #include "eng_vkMaterialType.h"
    #include "eng_vkMaterialInformed.h"
    #include "eng_vkDraw.h"
    #include "eng_vkDisplayCmds.h"
    #include "eng_vkDevice.h"
    #include "eng_vkDebug.h"


    #include "Implementation/eng_vkInstance_inline.h"
    #include "Implementation/eng_vkTexture_inline.h"
    #include "Implementation/eng_vkMemoryPool_inline.h"
    #include "Implementation/eng_vkDisplayCmds_inline.h"
    #include "Implementation/eng_vkBuffer_inline.h"
};


