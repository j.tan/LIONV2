//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class swapchain
{
public:

    struct scbuffers 
    {
        VkImage         m_VKImage           {};
        VkImageView     m_VKImageView       {};
    };

    enum errors : u8
    {
        ERR_OK,
        ERR_INSTANCE_PROC_FAILURE,
        ERR_DEVICE_PROC_FAILURE,
        ERR_SURFACE_CREATION,
        ERR_SWAPCHAIN_CREATION,
    };

    using err = x_err<errors>;

    /*
    struct setup_surface
    {
#ifdef _WIN32
        void*               m_pHWNDWin;
#elif defined __ANDROID__
        ANativeWindow*      m_pWindow;
#else
        xcb_connection_t*   m_Connection;
        xcb_window_t        m_Window;
#endif
    };
    */

public:

    constexpr                       swapchain           ( device& Device )                                                  noexcept : m_Device{ Device } {}
    constexpr       device&         getTheDevice        ( void ) const                                                      noexcept { return m_Device;                 }
    constexpr       auto            getQueueNodeIndex   ( void ) const                                                      noexcept { return m_QueueNodeIndex;         }
    inline          scbuffers&      getScreenBuffers    ( int Index )                                                       noexcept { return m_lSCBuffers[ Index ];    }
    constexpr       xuptr           getImageCount       ( void ) const                                                      noexcept { return m_lImages.getCount();     }
    constexpr       auto&           getVKColorFormat    ( void ) const                                                      noexcept { return m_VKColorFormat;          }


                    err             Initialize          ( const eng_window::setup& Setup, const window& Window )            noexcept;
                    err             Create              ( VkCommandBuffer cmdBuffer, int& Width, int& Height )              noexcept;
                    VkResult        AcquireNextImage    ( VkSemaphore presentCompleteSemaphore, const uint32_t FrameIndex ) noexcept;
                    VkResult        QueuePresent        ( VkQueue queue, uint32_t currentBuffer )                           noexcept;

                    void            Cleanup             ( void )                                                            noexcept;

protected: 

    device&                                             m_Device;
    VkSurfaceKHR                                        m_VKSurface                                     { VK_NULL_HANDLE };

    // Function pointers
    PFN_vkGetPhysicalDeviceSurfaceSupportKHR            m_VKGetPhysicalDeviceSurfaceSupportKHR          { nullptr };
    PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR       m_VKGetPhysicalDeviceSurfaceCapabilitiesKHR     { nullptr }; 
    PFN_vkGetPhysicalDeviceSurfaceFormatsKHR            m_VKGetPhysicalDeviceSurfaceFormatsKHR          { nullptr };
    PFN_vkGetPhysicalDeviceSurfacePresentModesKHR       m_VKGetPhysicalDeviceSurfacePresentModesKHR     { nullptr };
    PFN_vkCreateSwapchainKHR                            m_VKCreateSwapchainKHR                          { nullptr };
    PFN_vkDestroySwapchainKHR                           m_VKDestroySwapchainKHR                         { nullptr };
    PFN_vkGetSwapchainImagesKHR                         m_VKGetSwapchainImagesKHR                       { nullptr };
    PFN_vkAcquireNextImageKHR                           m_VKAcquireNextImageKHR                         { nullptr };
    PFN_vkQueuePresentKHR                               m_VKQueuePresentKHR                             { nullptr };

    VkFormat                                            m_VKColorFormat                                 {};
    VkColorSpaceKHR                                     m_VKColorSpace                                  {};

    VkSwapchainKHR                                      m_VKSwapChain                                   { VK_NULL_HANDLE };

    xndptr<VkImage>                                     m_lImages                                       {};
    xndptr<scbuffers>                                   m_lSCBuffers                                    {};

    // Index of the deteced graphics and presenting device queue
    uint32_t                                            m_QueueNodeIndex                                { UINT32_MAX };
};
