//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
// COMMON FUNCTIONALITY
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
class shader_common
{
public:

    enum errors : u8 
    {
        ERR_OK,
        ERR_FILE,
        ERR_VKFAILURE
    };

    using err = x_err<errors>;

public:
    constexpr           shader_common           ( device& Device )                                                                                      noexcept : m_Device{ Device } {}
                err     LoadVertexShader        ( const char* fileName,                                        const char* pShaderEntryPoint = "main" ) noexcept;
                err     LoadFragmentShader      ( const char* fileName,                                        const char* pShaderEntryPoint = "main" ) noexcept;
                err     LoadShader              ( const char* fileName,           VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept; 
                err     LoadGLSL                ( const char* fileName,           VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept;
                err     LoadSVP                 ( const char* fileName,           VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept;
                err     CreateShader            ( const xbuffer_view<xbyte> Data, VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept;
                err     CreateShaderGLSL        ( const xbuffer_view<xbyte> Data, VkShaderStageFlagBits stage, const char* pShaderEntryPoint = "main" ) noexcept;
    constexpr   auto&   getStateCreateInfo      ( void ) const                                                                                          noexcept { return m_VkShaderStageCreateInfo; }

protected:

    device&                             m_Device;   
    VkPipelineShaderStageCreateInfo     m_VkShaderStageCreateInfo   {};
};

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
// VERTEX SHADER
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

class shader_vert final : 
    public eng_shader_vert, 
    public shader_common
{
public:

    constexpr                           shader_vert         ( device& Device )                                  noexcept : shader_common{ Device } {}
                        void            Initialize          ( const setup& Setup )                              noexcept;
    virtual             void            Release             ( void )                                            noexcept override;
};

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
// FRAGMENT SHADER
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

class shader_frag final : 
    public eng_shader_frag,
    public shader_common
{
public:

    constexpr                           shader_frag         ( device& Device )                                  noexcept : shader_common{ Device } {}
                        void            Initialize          ( const setup& Setup )                              noexcept;
    virtual             void            Release             ( void )                                            noexcept override;
};


//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
// VERTEX SHADER
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

class shader_geom final : 
    public eng_shader_geom,
    public shader_common
{
public:

    constexpr                           shader_geom         ( device& Device )                                  noexcept : shader_common{ Device } {}
                        void            Initialize          ( const setup& Setup )                              noexcept;
    virtual             void            Release             ( void )                                            noexcept override;
};



