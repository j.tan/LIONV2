//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include <stdio.h>
#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{
namespace tools {

//---------------------------------------------------------------------------
// Load a binary file into a buffer (e.g. SPIR-V)
x_errdef::err readBinaryFile( xafptr<xbyte>& Data, const char* filename ) noexcept
{
//    FILE*           fp = fopen(filename, "rb");
    FILE*           fp;
    errno_t         tr = fopen_s( &fp, filename, "rb");
        
    if (!fp) 
        return x_error_code( x_errdef::errors, ERR_FAILURE, "Fail to open file" );

    fseek(fp, 0L, SEEK_END);
    int size = ftell(fp);

    fseek(fp, 0L, SEEK_SET);

    Data.Alloc( size );
    size_t retval = fread ( &Data[0], Data.getCount(), 1, fp );
    if( retval != 1 ) 
        return x_error_code( x_errdef::errors, ERR_FAILURE, "Fail to read contents of open file" );

    return x_error_ok();
}

//-----------------------------------------------------------------------
// Shader
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------

shader::err shader::LoadVertexShader( const char* fileName, const char* pShaderEntryPoint ) noexcept
{ 
    return LoadShader( fileName, VK_SHADER_STAGE_VERTEX_BIT ,pShaderEntryPoint ); 
}

//-----------------------------------------------------------------------

shader::err shader::LoadFragmentShader( const char* fileName, const char* pShaderEntryPoint ) noexcept
{ 
    return LoadShader( fileName, VK_SHADER_STAGE_FRAGMENT_BIT ,pShaderEntryPoint ); 
}

//-----------------------------------------------------------------------

shader::err shader::LoadShader( const char* fileName, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    const auto L = strlen( fileName );

    //
    // Use the right loader
    //
    for( auto i = L - 1; i != 0; --i )
    {
        if( fileName[i] == '.' )
        {
            if( fileName[i + 1] == 'G' || fileName[i + 1] == 'g' ||     //  .glsl
                fileName[i + 1] == 'F' || fileName[i + 1] == 'f' ||     //  .frag
                fileName[i + 1] == 'V' || fileName[i + 1] == 'v' )      //  .vert
                return LoadGLSL( fileName, stage, pShaderEntryPoint );

            if( fileName[i + 1] == 'S' || fileName[i + 1] == 's' )
                return LoadSVP( fileName, stage, pShaderEntryPoint );
        }
    }

    //
    // Todo: Add auto detect for vertex/fragment shaders base on file extensions
    //

    //
    // Fail
    //
    static const err Error = x_error_code( errors, ERR_FILE, "Unknown file extension" );
    ENG_ERRORLOG( m_Device.getInstance(), 
                    " ("                << 
                    fileName            <<
                    ") : "              << 
                    Error.getString() );

    return Error;
}

//-----------------------------------------------------------------------

shader::err shader::LoadGLSL( const char* fileName, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    //
    // Read binary file
    //
    xafptr<xbyte>   ShaderData;
    {
        x_errdef::err Err = readBinaryFile( ShaderData, fileName );
        if( Err || ShaderData.getCount() < 5 )
        {
            static const err Error = x_error_code( errors, ERR_FILE, "Fail to read a SVP shader file" );
            ENG_ERRORLOG( m_Device.getInstance(), 
                            Error.getString()   << 
                            " ("                << 
                            fileName            <<
                            ") : "              << 
                            Err.getString() );
        }
    }

    //
    // Handle the shader creation
    //
    {
        err Error = CreateShaderGLSL( ShaderData, stage, pShaderEntryPoint );
        {
            ENG_ERRORLOG( m_Device.getInstance(), " Additional Context: " << fileName  );
            return Error;
        }
    }

    return x_error_ok();
}

//-----------------------------------------------------------------------

shader::err shader::CreateShaderGLSL( const xbuffer_view<xbyte> Data, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    //
    // Hack for Vulkan to support for GLSL
    //
    xafptr<xbyte>       NewData;
    {
        static const int    VKGLSLHeaderSize = sizeof(uint32_t) * 3;

        NewData.Alloc( Data.getByteSize() + VKGLSLHeaderSize + 1 );

        // Magic SPV number
        reinterpret_cast<uint32_t*>(&NewData[0])[0] = 0x07230203; 
        reinterpret_cast<uint32_t*>(&NewData[0])[1] = 0;
        reinterpret_cast<uint32_t*>(&NewData[0])[2] = stage;

        NewData.CopyToFrom( Data, VKGLSLHeaderSize );

        // Make sure we terminate with zero
        NewData[NewData.getCount()-1] = 0;
    }

    //
    // Handle the shader creation
    //
    return CreateShader( NewData, stage, pShaderEntryPoint );
}

//-----------------------------------------------------------------------

shader::err shader::LoadSVP( const char* fileName, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    //
    // Read binary file
    //
    xafptr<xbyte>   ShaderData;
    {
        x_errdef::err Err = readBinaryFile( ShaderData, fileName );
        if( Err || ShaderData.getCount() < 5 )
        {
            static const err Error = x_error_code( errors, ERR_FILE, "Fail to read a SVP shader file" ) ;
            ENG_ERRORLOG( m_Device.getInstance(), 
                            Error.getString()   << 
                            " ("                << 
                            fileName            <<
                            ") : "              << 
                            Err.getString() );
            return Error;
        }
    }

    //
    // Handle the shader creation
    //
    {
        err Error = CreateShader( ShaderData, stage, pShaderEntryPoint );
        if( Error )
        {
            ENG_ERRORLOG( m_Device.getInstance(), " Additional Context: " << fileName  );
            return Error;
        }
    }

    return x_error_ok();
}

//-----------------------------------------------------------------------

shader::err shader::CreateShader( const xbuffer_view<xbyte> Data, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    //
    // Initialize shader creation info
    //
    m_VkShaderStageCreateInfo.sType   = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    m_VkShaderStageCreateInfo.pNext   = nullptr;
    m_VkShaderStageCreateInfo.stage   = stage;
    m_VkShaderStageCreateInfo.pName   = pShaderEntryPoint;

    //
    // Create shader module
    //
    {
        VkShaderModuleCreateInfo  moduleCreateInfo {};

        moduleCreateInfo.sType      = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        moduleCreateInfo.pNext      = nullptr;                         

        moduleCreateInfo.codeSize   = Data.getByteSize();
        moduleCreateInfo.pCode      = reinterpret_cast<const uint32_t*>(&Data[0]);
        moduleCreateInfo.flags      = 0;

        VkResult VKErr = vkCreateShaderModule( m_Device.getVKDevice(), &moduleCreateInfo, nullptr, &m_VkShaderStageCreateInfo.module );
        if( VKErr )
        {
            static const err Error = x_error_code( errors, ERR_VKFAILURE, "Fail to create shader module" );
            ENG_ERRORLOG( m_Device.getInstance(), 
                            Error.getString()   << 
                            " : "               << 
                            debug::VulkanErrorString( VKErr ) );
            return Error;
        }
    }

    return x_error_ok();
}

//-----------------------------------------------------------------------
// Buffer
//-----------------------------------------------------------------------
/*
namespace tools
{
    //-----------------------------------------------------------------------
    
    buffer::err buffer::Create( eng_instance::base& Engine, const int EntrySize, const xuptr Count, const type Type )
    {
        struct type_table_entry { buffer::type m_Type; VkBufferUsageFlags m_VkUsage; };
        static const xarray<type_table_entry,4> s_TapeTable 
        {
            type_table_entry{ TYPE_NULL,            VK_NULL_HANDLE                      },
            type_table_entry{ TYPE_VERTEX_BUFFER,   VK_BUFFER_USAGE_VERTEX_BUFFER_BIT   },
            type_table_entry{ TYPE_INDEX_BUFFER,    VK_BUFFER_USAGE_INDEX_BUFFER_BIT    },
            type_table_entry{ TYPE_UNIFORM_BUFFER,  VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT  }
        };
        x_assume( s_TapeTable[ Type ].m_Type == Type );

        //
        // Create the buffer
        //
        VkBufferCreateInfo bufInfo = {};
        bufInfo.sType       = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufInfo.pNext       = nullptr;
        bufInfo.size        = EntrySize * Count;
        bufInfo.usage       = s_TapeTable[ Type ].m_VkUsage;
        bufInfo.flags       = 0;

        VkResult VKErr = vkCreateBuffer( Engine.getDevice(), &bufInfo, nullptr, &m_VkBuffer );
        if( VKErr )
        {
            static const err Error { x_error_code( errors, ERR_VKFAILURE, "Fail to create buffer" ) };
            ENG_ERRORLOG( Engine, 
                            Error.getString()   << 
                            " : "               << 
                            debug::VulkanErrorString( VKErr ) );
            return Error;
        }

        //
        // Collect memory requirements
        //
        VkMemoryRequirements memReqs;
        vkGetBufferMemoryRequirements( Engine.getDevice(), m_VkBuffer, &memReqs );

        //
        // Allocate the memory
        //
        VkMemoryAllocateInfo memAlloc = {};
        memAlloc.sType                  = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        memAlloc.pNext                  = nullptr;
        memAlloc.allocationSize         = memReqs.size;
        memAlloc.memoryTypeIndex        = 0;

        Engine.getMemoryType( memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, memAlloc.memoryTypeIndex );
        VKErr = vkAllocateMemory( Engine.getDevice(), &memAlloc, nullptr, &m_VkDeviceMemory );
        if( VKErr )
        {
            vkDestroyBuffer( Engine.getDevice(), m_VkBuffer, nullptr ); m_VkBuffer = {};
            static const err Error { x_error_code( errors, ERR_OUT_OF_MEMORY, "Fail to allocate memory" ) };
            ENG_ERRORLOG( Engine, 
                            Error.getString()   << 
                            " : "               << 
                            debug::VulkanErrorString( VKErr ) );
            return Error;
        }

        //
        // Bind the memory to the buffer object
        //
        const xuptr StartOffset = 0;
        VKErr = vkBindBufferMemory( Engine.getDevice(), m_VkBuffer, m_VkDeviceMemory, StartOffset );
        assert( !VKErr );

        //
        // Store the size of the buffer
        //
        m_EntrySize = EntrySize;
        m_Count     = Count;

        return x_error_none(errors);
    }

    //-----------------------------------------------------------------------
    
    buffer::err buffer::CreateBuffer( eng_instance::base& Engine, int EntrySize, xuptr Count, type Type, const void* pData )
    {
        err Err = Create( Engine, EntrySize, Count, Type );
        if( Err ) return Err;
        if( pData ) TransferData( Engine, 0, pData, Count );
        return x_error_none(errors);
    }

    //-----------------------------------------------------------------------

    void* buffer::CreateMap( eng_instance::base& Engine, xuptr StartIndex, xuptr Count )
    {
        Count = (Count==~0) ? m_Count : Count;
        x_assert( StartIndex < Count );
        x_assert( (StartIndex + Count ) <= m_Count );

        void*       pData   = nullptr;
        VkResult     VKErr  = vkMapMemory( Engine.getDevice(), m_VkDeviceMemory, StartIndex*m_EntrySize, getByteSize(), 0, &pData );
        x_assert( !VKErr );

        return pData;
    }

    //-----------------------------------------------------------------------

    void buffer::ReleaseMap( eng_instance::base& Engine )
    {
        vkUnmapMemory( Engine.getDevice(), m_VkDeviceMemory );
    }

    //-----------------------------------------------------------------------

    void buffer::TransferData( eng_instance::base& Engine, xuptr DestinationIndex, const void* pSrcData, xuptr Count )
    {
        void* pData = CreateMap( Engine, DestinationIndex, Count );
        x_assert(pData);
        memcpy( pData, pSrcData, Count*m_EntrySize );
        ReleaseMap( Engine );
    }
}
*/

//-----------------------------------------------------------------------
// vertex_desc
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------

void vertex_desc::setup( int VertSize, xbuffer_view<attribute> Data ) noexcept
{
    m_VertexSize    = static_cast<u16>(VertSize);
    m_nAttributes   = static_cast<u16>(Data.getCount());
    m_pData         = &Data[0];
    m_bFreeData     = false;
}

//-----------------------------------------------------------------------
    
void vertex_desc::setup( int VertSize, xowner<const attribute*> pAttribute, int nAttributes, bool bFreeDataWhenDone ) noexcept
{
    m_VertexSize    = static_cast<u16>(VertSize);
    m_nAttributes   = static_cast<u16>(nAttributes);
    m_pData         = pAttribute;
    m_bFreeData     = bFreeDataWhenDone;
}

//-----------------------------------------------------------------------
// vertex pipeline
//-----------------------------------------------------------------------
enum vtypes
{
    VTYPES_SCALED,
    VTYPES_NORM,
    VTYPES_INT,
    VTYPES_FLOAT,
    VTYPES_ENUM_COUNT
};

using attype    = vertex_desc::attribute_src;
using usgtype   = vertex_desc::attribute_usage;

struct vertex_desc_data_table
{
    attype                                            m_SrcAttr;
    const xarray<VkFormat,VTYPES_ENUM_COUNT>    m_VkFormat;
};

static const xarray<vertex_desc_data_table, attype::ATTR_SRC_ENUM_COUNT> s_VertexAttrDataTable
{
    //                      TABLE ENUM VERIFICATION           VTYPES_SCALED                      VTYPES_NORM                   VTYPES_INT                  VTYPES_FLOAT
    vertex_desc_data_table{ attype::ATTR_SRC_NULL,       { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_UNDEFINED,         VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_U8x4_F,     { VK_FORMAT_R8G8B8A8_USCALED,     VK_FORMAT_R8G8B8A8_UNORM,     VK_FORMAT_UNDEFINED,         VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_S8x4_F,     { VK_FORMAT_B8G8R8A8_SSCALED,     VK_FORMAT_R8G8B8A8_SNORM,     VK_FORMAT_UNDEFINED,         VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_U8x4_I,     { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_R8G8B8A8_UINT,     VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_S8x4_I,     { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_R8G8B8A8_SINT,     VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_U16x2_F,    { VK_FORMAT_R16G16_SSCALED,       VK_FORMAT_R16G16_SNORM,       VK_FORMAT_UNDEFINED,         VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_S16x2_F,    { VK_FORMAT_R16G16_SSCALED,       VK_FORMAT_R16G16_SNORM,       VK_FORMAT_UNDEFINED,         VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_U16x2_I,    { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_R16G16_UINT,       VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_S16x2_I,    { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_R16G16_SINT,       VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_U16x4_F,    { VK_FORMAT_R16G16B16A16_USCALED, VK_FORMAT_R16G16B16A16_UNORM, VK_FORMAT_UNDEFINED,         VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_S16x4_F,    { VK_FORMAT_R16G16B16A16_USCALED, VK_FORMAT_R16G16B16A16_UNORM, VK_FORMAT_UNDEFINED,         VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_U16x4_I,    { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_R16G16B16A16_UINT, VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_S16x4_I,    { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_R16G16B16A16_UINT, VK_FORMAT_UNDEFINED           }  },
    vertex_desc_data_table{ attype::ATTR_SRC_F32x1,      { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_UNDEFINED,         VK_FORMAT_R32_SFLOAT          }  },
    vertex_desc_data_table{ attype::ATTR_SRC_F32x2,      { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_UNDEFINED,         VK_FORMAT_R32G32_SFLOAT       }  },
    vertex_desc_data_table{ attype::ATTR_SRC_F32x3,      { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_UNDEFINED,         VK_FORMAT_R32G32B32_SFLOAT    }  },
    vertex_desc_data_table{ attype::ATTR_SRC_F32x4,      { VK_FORMAT_UNDEFINED,            VK_FORMAT_UNDEFINED,          VK_FORMAT_UNDEFINED,         VK_FORMAT_R32G32B32A32_SFLOAT }  }
};                                                        

void vertex_pipeline_descriptors::setup( int BindingIndex, const vertex_desc& VertexDesc, const xbuffer_view< tools::vertex_desc::attribute_link >& Links ) noexcept
{
    m_AttributeDescriptions.New( Links.getCount() );

    //
    // Create final attribute descriptions
    //
    int  iAttr = 0;
    auto pAttrList = VertexDesc.getAttributeList();
    for( int i = 0; i < VertexDesc.getAttributeCount(); ++i )
    {
        const auto& Attribute = pAttrList[i];
        for( int j = 0; j < Links.getCount(); ++j )
        {
            const vertex_desc::attribute_link& Link = Links[j]; 
            if( Link.m_UsageType == Attribute.m_UsageType )
            {
                auto& Attr       = m_AttributeDescriptions[iAttr++];

                vtypes VTypes;
                if( Attribute.m_DataType >= attype::ATTR_SRC_F32x1 ) 
                {
                    VTypes = VTYPES_FLOAT;
                }
                else if( Attribute.m_UsageType == usgtype::ATTR_USAGE_4_INDICES )
                {
                        VTypes = VTYPES_INT;   
                }
                else
                {
                    VTypes = VTYPES_NORM;
                }

                // Make sure that the table is in sync with the enum
                x_assert( s_VertexAttrDataTable[ Attribute.m_DataType ].m_SrcAttr == Attribute.m_DataType );

                // We got an undefined format
                x_assert( s_VertexAttrDataTable[ Attribute.m_DataType ].m_VkFormat[VTypes] != VK_FORMAT_UNDEFINED );

                Attr.binding    = BindingIndex;
                Attr.location   = Link.m_Location;
                Attr.format     = s_VertexAttrDataTable[ Attribute.m_DataType ].m_VkFormat[VTypes];
                Attr.offset     = Attribute.m_Offset;

                x_assert( Attr.offset < 100 );
                break;
            }
        }
    }

    //
    // Sanity check
    //
    #if _X_DEBUG
        //
        // Check attributes
        //
        for( const auto& A : m_AttributeDescriptions )
            for( const auto& B : m_AttributeDescriptions )
            {
                if( &A == &B ) continue;
                x_assert( A.offset   != B.offset );
                x_assert( A.location != B.location );
                x_assert( A.binding == B.binding );
            }
    #endif

    //
    // Setup the binding descritor
    //
    m_VkBindingDescription.binding        = BindingIndex;
    m_VkBindingDescription.stride         = VertexDesc.getVertexSize();
    m_VkBindingDescription.inputRate      = VK_VERTEX_INPUT_RATE_VERTEX;

    //
    // Setup the pipelineInfo
    //
    m_VkPipeLineSetupInfo.sType                             = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    m_VkPipeLineSetupInfo.pNext                             = nullptr;
    m_VkPipeLineSetupInfo.vertexBindingDescriptionCount     = 1;
    m_VkPipeLineSetupInfo.pVertexBindingDescriptions        = &m_VkBindingDescription;
    m_VkPipeLineSetupInfo.vertexAttributeDescriptionCount   = static_cast<uint32_t>(m_AttributeDescriptions.getCount());
    m_VkPipeLineSetupInfo.pVertexAttributeDescriptions      = &m_AttributeDescriptions[0];
}

//-----------------------------------------------------------------------
// Cmd Buffers
//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

void cmd_buffer_double::Initialize( void ) noexcept
{
    VkCommandPoolCreateInfo cmdPoolInfo = {};
    cmdPoolInfo.sType               = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmdPoolInfo.queueFamilyIndex    = m_Device.getQueueNodeIndex();
    cmdPoolInfo.flags               = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    auto VKErr = vkCreateCommandPool( m_Device.getVKDevice(), &cmdPoolInfo, nullptr, &m_CmdPool );
    x_assert( !VKErr );


    VkCommandBufferAllocateInfo cmdBufAllocateInfo = tools::initializers::commandBufferAllocateInfo(
            m_CmdPool,
            VK_COMMAND_BUFFER_LEVEL_SECONDARY,
            (uint32_t)m_CmdBuffers.getCount() );

    VKErr = vkAllocateCommandBuffers( m_Device.getVKDevice(), &cmdBufAllocateInfo, m_CmdBuffers );
    x_assert( !VKErr );
}

//-----------------------------------------------------------------------

void cmd_buffer_double::Kill( void ) noexcept
{
    vkFreeCommandBuffers( m_Device.getVKDevice(), m_CmdPool, static_cast<u32>(m_CmdBuffers.getCount()), m_CmdBuffers );
    vkDestroyCommandPool( m_Device.getVKDevice(), m_CmdPool, nullptr);
}

//-----------------------------------------------------------------------

VkCommandBuffer cmd_buffer_double::getCmdBuffer( void ) noexcept
{ 
    return m_CmdBuffers[ m_Device.getFrameIndex() ]; 
}

//-----------------------------------------------------------------------

VkCommandBuffer  cmd_buffer_double::RenderBegin( 
    VkRenderPass                    GlobalRenderPass, 
    VkFramebuffer                   Framebuffer,
    const xbuffer_view<VkViewport>  ViewPort,
    const xbuffer_view<VkRect2D>    Scissor ) noexcept
{
    auto CmdBuff = getCmdBuffer();  

    if( m_iCurrentBuffer == ~0u )
    {
        m_iCurrentBuffer = m_Device.getFrameIndex();

        // Inheritance infor for secondary command buffers
        VkCommandBufferInheritanceInfo inheritanceInfo {};
        inheritanceInfo.sType       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
        inheritanceInfo.renderPass  = GlobalRenderPass;
        inheritanceInfo.framebuffer = Framebuffer; //Engine.getFrameBuffer()[ Engine.getCurrentFrameIndex() ];

        VkCommandBufferBeginInfo cmdBufInfo {};
        cmdBufInfo.sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        cmdBufInfo.flags            = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
        cmdBufInfo.pInheritanceInfo = &inheritanceInfo;
        vkBeginCommandBuffer( CmdBuff, &cmdBufInfo );

        // Update dynamic viewport state
        vkCmdSetViewport( CmdBuff, 0, static_cast<uint32_t>(ViewPort.getCount()),   &ViewPort[0] );

        // Update dynamic scissor state
        vkCmdSetScissor ( CmdBuff, 0, static_cast<uint32_t>(Scissor.getCount()),    &Scissor[0]  );
    }
    else
    {
        x_assert(  m_iCurrentBuffer == m_Device.getFrameIndex() );
    }

    return  CmdBuff;
}

//-----------------------------------------------------------------------
    
void cmd_buffer_double::RenderEnd( void ) noexcept
{
    x_assert(  m_iCurrentBuffer == m_Device.getFrameIndex() );
}

//-----------------------------------------------------------------------

void cmd_buffer_double::SubmitCmds( void ) noexcept
{
    if( m_iCurrentBuffer == ~0 )
        return;

    x_assert(  m_iCurrentBuffer == m_Device.getFrameIndex() );

    auto CmdBuff = getCmdBuffer(); 

    vkEndCommandBuffer( CmdBuff );

    vkCmdExecuteCommands( m_Device.getVKMainCmdBuffer(), 1, &CmdBuff );

    m_iCurrentBuffer = ~0;
}

//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}
}
    