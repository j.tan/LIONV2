//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

//------------------------------------------------------------------------------------

void material_type::Initialize( const setup& Setup ) noexcept
{
    VkResult VKErr;

    //
    // Copy some of the setup data
    //
    m_Info.m_hVertShader = const_cast<decltype(Setup.m_hVertShader)&>( Setup.m_hVertShader );
    m_Info.m_hFragShader = const_cast<decltype(Setup.m_hFragShader)&>( Setup.m_hFragShader );
    m_Info.m_hGeomShader = const_cast<decltype(Setup.m_hGeomShader)&>( Setup.m_hGeomShader );
    m_Info.m_nSamplerBindings = Setup.m_nSamplerBindings;
    m_Info.m_SamplerBindings  = Setup.m_SamplerBindings;
    m_Info.m_EntriesPerPage   = Setup.m_EntriesPerPage;

    if( m_Info.m_hVertShader.isValid() )  m_ShaderStages[ m_nShaderStages++ ] = HandleToObject<shader_vert>(m_Info.m_hVertShader).getStateCreateInfo();
    if( m_Info.m_hFragShader.isValid() )  m_ShaderStages[ m_nShaderStages++ ] = HandleToObject<shader_frag>(m_Info.m_hFragShader).getStateCreateInfo();
    if( m_Info.m_hGeomShader.isValid() )  m_ShaderStages[ m_nShaderStages++ ] = HandleToObject<shader_geom>(m_Info.m_hGeomShader).getStateCreateInfo();

    //
    // Set the texture descriptor
    //
    {
        const xuptr                             nLayoutBinding  = m_Info.m_nSamplerBindings;
        xarray<VkDescriptorSetLayoutBinding,16> layoutBinding   = {};

        for( xuptr i = 0; i < nLayoutBinding; ++i )
        {
            layoutBinding[i].binding                                = Setup.m_SamplerBindings[i].m_iBind;
            layoutBinding[i].descriptorType                         = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            layoutBinding[i].descriptorCount                        = 1;
            layoutBinding[i].stageFlags                             = VK_SHADER_STAGE_FRAGMENT_BIT;
            layoutBinding[i].pImmutableSamplers                     = nullptr;
        }

        VkDescriptorSetLayoutCreateInfo descriptorLayout            = {};
        descriptorLayout.sType                                      = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        descriptorLayout.pNext                                      = nullptr;
        descriptorLayout.bindingCount                               = static_cast<uint32_t>( nLayoutBinding );
        descriptorLayout.pBindings                                  = layoutBinding;

        VKErr = vkCreateDescriptorSetLayout( m_Device.getVKDevice(), &descriptorLayout, nullptr, &m_VKDescriptorSetLayout[0] );
        assert( !VKErr );
    }

    //
    // Set the push constants for this pipeline
    //
    {
        xarray<VkPushConstantRange,1> pushConstantRange         = {};
        pushConstantRange[0].stageFlags                         = VK_SHADER_STAGE_VERTEX_BIT;
        pushConstantRange[0].offset                             = 0;
        pushConstantRange[0].size                               = sizeof(xmatrix4);

        //
        // Set the pipeline descriptor 
        //
        VkPipelineLayoutCreateInfo PipelineLayoutCreateInfo    = {};
        PipelineLayoutCreateInfo.sType                         = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        PipelineLayoutCreateInfo.pNext                         = nullptr;

        // Set the descriptors for the texture
        PipelineLayoutCreateInfo.setLayoutCount                = static_cast<uint32_t>(m_VKDescriptorSetLayout.getCount());
        PipelineLayoutCreateInfo.pSetLayouts                   = m_VKDescriptorSetLayout;

        // Push constant ranges are part of the pipeline layout
        PipelineLayoutCreateInfo.pushConstantRangeCount        = static_cast<uint32_t>(pushConstantRange.getCount());
        PipelineLayoutCreateInfo.pPushConstantRanges           = pushConstantRange;

        VKErr = vkCreatePipelineLayout( m_Device.getVKDevice(), &PipelineLayoutCreateInfo, nullptr, &m_VKPipelineLayout );
        assert( !VKErr );
    }

    // We need to tell the API the number of max. requested descriptors per type
    xarray<VkDescriptorPoolSize,1> typeCounts;

    // This example only uses one descriptor type (uniform buffer) and only
    // requests one descriptor of this type
    typeCounts[0].type                              = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    typeCounts[0].descriptorCount                   = 1;    // Draw uses only one texture

    // Create the global descriptor pool
    // All descriptors used in this example are allocated from this pool
    VkDescriptorPoolCreateInfo descriptorPoolInfo   = {};
    descriptorPoolInfo.sType                        = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolInfo.pNext                        = nullptr;
    descriptorPoolInfo.poolSizeCount                = static_cast<uint32_t>( typeCounts.getCount() );
    descriptorPoolInfo.pPoolSizes                   = typeCounts;
    descriptorPoolInfo.maxSets                      = m_Info.m_EntriesPerPage;  

    x_lk_guard_get( m_LockedVKDescriptorPool, VKDescriptorPool );
    VKErr = vkCreateDescriptorPool( m_Device.getVKDevice(), &descriptorPoolInfo, nullptr, &VKDescriptorPool.m_VKDescriptorPool );
    x_assert( !VKErr );

}

//------------------------------------------------------------------------------------

void material_type::Release( void ) noexcept
{
    m_Device.ReleaseMaterial( *this );
}

//------------------------------------------------------------------------------------

VkDescriptorSet material_type::AllocDescriptorSet( void ) noexcept
{
    VkDescriptorSet VKDescriptorSet = nullptr;

    x_lk_guard_get( m_LockedVKDescriptorPool, VKDescriptorPool );

    // Update descriptor sets determining the shader binding points
    // For every binding point used in a shader there needs to be one
    // descriptor set matching that binding point
    VkDescriptorSetAllocateInfo allocInfo   = {};
    allocInfo.sType                         = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool                = VKDescriptorPool.m_VKDescriptorPool; 
    allocInfo.descriptorSetCount            = static_cast<uint32_t>(m_VKDescriptorSetLayout.getCount());
    allocInfo.pSetLayouts                   = m_VKDescriptorSetLayout;

    VkResult VKErr = vkAllocateDescriptorSets( m_Device.getVKDevice(), &allocInfo, &VKDescriptorSet );
    assert( !VKErr );

    return VKDescriptorSet;
}

//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}


