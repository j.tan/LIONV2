//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

//------------------------------------------------------------------------------------
    
void rendering_pipeline::setup::setupSolidPipeline( 
    VkRenderPass                                    RenderPass,
    xbuffer_view<VkPipelineShaderStageCreateInfo>   ShaderStages,
    VkPipelineLayout                                aVkPipelineLayout, 
    const VkPipelineVertexInputStateCreateInfo&     aVkPipeLineSetupInfo ) noexcept
{
    m_VKRenderPass = RenderPass;

    //
    //  VkGraphicsPipelineCreateInfo
    //
    m_VkPipelineCreateInfo.sType                        = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    m_VkPipelineCreateInfo.layout                       = aVkPipelineLayout;       
    m_VkPipelineCreateInfo.renderPass                   = m_VKRenderPass;    
    m_VkPipelineCreateInfo.pVertexInputState            = &aVkPipeLineSetupInfo;
    m_VkPipelineCreateInfo.pInputAssemblyState          = &m_VkInputAssemblyState;
    m_VkPipelineCreateInfo.pRasterizationState          = &m_VkRasterizationState;
    m_VkPipelineCreateInfo.pColorBlendState             = &m_VkColorBlendState;
    m_VkPipelineCreateInfo.pMultisampleState            = &m_VkMultisampleState;
    m_VkPipelineCreateInfo.pViewportState               = &m_VkViewportState;
    m_VkPipelineCreateInfo.pDepthStencilState           = &m_VkDepthStencilState;
    m_VkPipelineCreateInfo.pDynamicState                = &m_VkDynamicState;
    m_VkPipelineCreateInfo.stageCount                   = static_cast<uint32_t>(ShaderStages.getCount());    // User must specify this manually...
    m_VkPipelineCreateInfo.pStages                      = ShaderStages;

    //
    // VkPipelineInputAssemblyStateCreateInfo
    //      Vertex input state
    //      Describes the topoloy used with this pipeline
    //      This pipeline renders vertex data as triangle lists
    m_VkInputAssemblyState.sType                        = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    m_VkInputAssemblyState.topology                     = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

    //
    // VkPipelineRasterizationStateCreateInfo
    //      Rasterization state
    //      Solid polygon mode
    //      No culling
    m_VkRasterizationState.sType                        = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    m_VkRasterizationState.polygonMode                  = VK_POLYGON_MODE_FILL;
    m_VkRasterizationState.cullMode                     = VK_CULL_MODE_BACK_BIT;//VK_CULL_MODE_NONE;
    m_VkRasterizationState.frontFace                    = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    m_VkRasterizationState.depthClampEnable             = false;
    m_VkRasterizationState.rasterizerDiscardEnable      = false;
    m_VkRasterizationState.depthBiasEnable              = false;
    m_VkRasterizationState.lineWidth                    = 1.0f;

    //
    // VkPipelineColorBlendStateCreateInfo  +  VkPipelineColorBlendAttachmentState
    //      Color blend state
    //      Describes blend modes and color masks
    //      One blend attachment state
    //      Blending is not used
    m_VkColorBlendState.sType                           = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    m_VkColorBlendState.attachmentCount                 = static_cast<u32>( m_lVkBlendAttachmentState.getCount() );
    m_VkColorBlendState.pAttachments                    = &m_lVkBlendAttachmentState[0];

    m_lVkBlendAttachmentState[0].colorWriteMask         = 0xf;
    m_lVkBlendAttachmentState[0].blendEnable            = false;

    //
    // VkPipelineViewportStateCreateInfo
    //      Viewport state
    //      One viewport
    //      One scissor rectangle
    m_VkViewportState.sType                             = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    m_VkViewportState.viewportCount                     = 1;
    m_VkViewportState.scissorCount                      = 1;

    //
    // VkPipelineDynamicStateCreateInfo + VkDynamicState
    //      Enable dynamic states
    //      Describes the dynamic states to be used with this pipeline
    //      Dynamic states can be set even after the pipeline has been created
    //      So there is no need to create new pipelines just for changing
    //      a viewport's dimensions or a scissor box
    //      The dynamic state properties themselves are stored in the command buffer
    m_VkDynamicState.sType                              = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    m_VkDynamicState.pDynamicStates                     = &m_lVkDynamicStateEnables[0];
    m_VkDynamicState.dynamicStateCount                  = static_cast<u32>( m_lVkDynamicStateEnables.getCount() );
        
    m_lVkDynamicStateEnables[0]                         = VK_DYNAMIC_STATE_VIEWPORT;
    m_lVkDynamicStateEnables[1]                         = VK_DYNAMIC_STATE_SCISSOR;

    //
    // VkPipelineDepthStencilStateCreateInfo
    //      Depth and stencil state
    //      Describes depth and stenctil test and compare ops
    //      Basic depth compare setup with depth writes and depth test enabled
    //      No stencil used 
    m_VkDepthStencilState.sType                         = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    m_VkDepthStencilState.depthTestEnable               = true;
    m_VkDepthStencilState.depthWriteEnable              = true;
    m_VkDepthStencilState.depthCompareOp                = VK_COMPARE_OP_LESS_OR_EQUAL;
    m_VkDepthStencilState.depthBoundsTestEnable         = false;
    m_VkDepthStencilState.back.failOp                   = VK_STENCIL_OP_KEEP;
    m_VkDepthStencilState.back.passOp                   = VK_STENCIL_OP_KEEP;
    m_VkDepthStencilState.back.compareOp                = VK_COMPARE_OP_ALWAYS;
    m_VkDepthStencilState.stencilTestEnable             = false;
    m_VkDepthStencilState.front                         = m_VkDepthStencilState.back;

    //
    // VkPipelineMultisampleStateCreateInfo
    //      Multi sampling state
    //      No multi sampling
    //
    m_VkMultisampleState.sType                          = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    m_VkMultisampleState.pSampleMask                    = nullptr;
    m_VkMultisampleState.rasterizationSamples           = VK_SAMPLE_COUNT_1_BIT;
}

//-----------------------------------------------------------------------

rendering_pipeline::setup::setup(
    VkRenderPass                                    RenderPass,
    xbuffer_view<VkPipelineShaderStageCreateInfo>   ShaderStages,
    VkPipelineLayout                                aVkPipelineLayout, 
    const VkPipelineVertexInputStateCreateInfo&     aVkPipeLineSetupInfo, 
    default_pipelines                               DefaultPipeLine ) noexcept
{
    switch( DefaultPipeLine )
    {
        case SOLID_PIPELINE:    setupSolidPipeline( RenderPass, ShaderStages, aVkPipelineLayout, aVkPipeLineSetupInfo ); break;
    }
}

//-----------------------------------------------------------------------
rendering_pipeline::setup::setup(
    VkRenderPass                                    RenderPass,
    xbuffer_view<VkPipelineShaderStageCreateInfo>   ShaderStages,
    VkPipelineLayout                                aVkPipelineLayout,
    const VkPipelineVertexInputStateCreateInfo&     aVkPipeLineSetupInfo,
    eng_draw::pipeline                              DrawPipeLine,
    bool                                            bRenderPolys ) noexcept
{
    m_VKRenderPass = RenderPass;

    //
    //  VkGraphicsPipelineCreateInfo
    //
    m_VkPipelineCreateInfo.sType                        = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    m_VkPipelineCreateInfo.layout                       = aVkPipelineLayout;       
    m_VkPipelineCreateInfo.renderPass                   = m_VKRenderPass;    
    m_VkPipelineCreateInfo.pVertexInputState            = &aVkPipeLineSetupInfo;
    m_VkPipelineCreateInfo.pInputAssemblyState          = &m_VkInputAssemblyState;
    m_VkPipelineCreateInfo.pRasterizationState          = &m_VkRasterizationState;
    m_VkPipelineCreateInfo.pColorBlendState             = &m_VkColorBlendState;
    m_VkPipelineCreateInfo.pMultisampleState            = &m_VkMultisampleState;
    m_VkPipelineCreateInfo.pViewportState               = &m_VkViewportState;
    m_VkPipelineCreateInfo.pDepthStencilState           = &m_VkDepthStencilState;
    m_VkPipelineCreateInfo.pDynamicState                = &m_VkDynamicState;
    m_VkPipelineCreateInfo.stageCount                   = static_cast<uint32_t>(ShaderStages.getCount());    // User must specify this manually...
    m_VkPipelineCreateInfo.pStages                      = ShaderStages;

    //
    // VkPipelineInputAssemblyStateCreateInfo
    //      Vertex input state
    //      Describes the topoloy used with this pipeline
    //      This pipeline renders vertex data as triangle lists
    m_VkInputAssemblyState.sType                        = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    if( bRenderPolys )
    {
        m_VkInputAssemblyState.topology                 = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    }
    else
    {
        m_VkInputAssemblyState.topology                 = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
    }

    //
    // VkPipelineRasterizationStateCreateInfo
    //      Rasterization state
    //      Solid polygon mode
    //      No culling
    m_VkRasterizationState.sType                        = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;

    switch( DrawPipeLine.m_RASTER )
    {
        case eng_draw::RASTER_SOLID:        m_VkRasterizationState.polygonMode                  = VK_POLYGON_MODE_FILL; break;
        case eng_draw::RASTER_WIRE_FRAME:   m_VkRasterizationState.polygonMode                  = VK_POLYGON_MODE_LINE; break;           
        default: x_assert( false );
    }
   
    switch( DrawPipeLine.m_CULL )
    {
        case eng_draw::CULL_BACK:           m_VkRasterizationState.cullMode                     = VK_CULL_MODE_BACK_BIT; break;
        case eng_draw::CULL_FRONT:          m_VkRasterizationState.cullMode                     = VK_CULL_MODE_FRONT_BIT; break;           
        case eng_draw::CULL_OFF:            m_VkRasterizationState.cullMode                     = VK_CULL_MODE_NONE; break;           
        default: x_assert( false );
    }

    m_VkRasterizationState.frontFace                    = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    m_VkRasterizationState.depthClampEnable             = false;
    m_VkRasterizationState.rasterizerDiscardEnable      = false;
    m_VkRasterizationState.depthBiasEnable              = false;
    m_VkRasterizationState.lineWidth                    = 1.0f;

    //
    // VkPipelineColorBlendStateCreateInfo  +  VkPipelineColorBlendAttachmentState
    //      Color blend state
    //      Describes blend modes and color masks
    //      One blend attachment state
    //      Blending is not used
    m_VkColorBlendState.sType                           = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    m_VkColorBlendState.attachmentCount                 = static_cast<u32>( m_lVkBlendAttachmentState.getCount() );
    m_VkColorBlendState.pAttachments                    = &m_lVkBlendAttachmentState[0];

    switch( DrawPipeLine.m_BLEND )
    {
    case eng_draw::BLEND_OFF: 
        m_lVkBlendAttachmentState[0].colorWriteMask         = 0xf;
        m_lVkBlendAttachmentState[0].blendEnable            = false;
        break;
    case eng_draw::BLEND_ALPHA_ORIGINAL:
        m_lVkBlendAttachmentState[0].colorWriteMask         = 0xf;
        m_lVkBlendAttachmentState[0].blendEnable            = true;

        //     https://developer.nvidia.com/content/alpha-blending-pre-or-not-pre
        // Vulkan Equation: VKScreen = (SourceColor.rgb * srcColorBlendFactor) + (DestinationColor.rgb * dstColorBlendFactor)
        // DestinationColor.rgb = (SourceColor.rgb * SourceColor.a) + (DestinationColor.rgb * (1 - SourceColor.a));
        m_lVkBlendAttachmentState[0].srcColorBlendFactor    = VK_BLEND_FACTOR_SRC_ALPHA;
        m_lVkBlendAttachmentState[0].dstColorBlendFactor    = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        m_lVkBlendAttachmentState[0].colorBlendOp           = VK_BLEND_OP_ADD;

        m_lVkBlendAttachmentState[0].srcAlphaBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstAlphaBlendFactor    = VK_BLEND_FACTOR_ZERO;
        m_lVkBlendAttachmentState[0].alphaBlendOp           = VK_BLEND_OP_ADD;

        break;
    case eng_draw::BLEND_ALPHA_PREMULTIPLY:
        m_lVkBlendAttachmentState[0].colorWriteMask         = 0xf;
        m_lVkBlendAttachmentState[0].blendEnable            = true;

        //     https://developer.nvidia.com/content/alpha-blending-pre-or-not-pre
        // Vulkan Equation: VKScreen = (SourceColor.rgb * srcColorBlendFactor) + (DestinationColor.rgb * dstColorBlendFactor)
        // DestinationColor.rgb = (SourceColor.rgb * One) + (DestinationColor.rgb * (1 - SourceColor.a))
        m_lVkBlendAttachmentState[0].srcColorBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstColorBlendFactor    = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        m_lVkBlendAttachmentState[0].colorBlendOp           = VK_BLEND_OP_ADD;

        m_lVkBlendAttachmentState[0].srcAlphaBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstAlphaBlendFactor    = VK_BLEND_FACTOR_ZERO;
        m_lVkBlendAttachmentState[0].alphaBlendOp           = VK_BLEND_OP_ADD;
        break;
    case eng_draw::BLEND_ADD: 
        m_lVkBlendAttachmentState[0].colorWriteMask         = 0xf;
        m_lVkBlendAttachmentState[0].blendEnable            = true;

        // Vulkan Equation: VKScreen = (SourceColor.rgb * srcColorBlendFactor) + (DestinationColor.rgb * dstColorBlendFactor)
        // DestinationColor.rgb = (SourceColor.rgb * One) + (DestinationColor.rgb * One)
        m_lVkBlendAttachmentState[0].srcColorBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstColorBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].colorBlendOp           = VK_BLEND_OP_ADD;

        m_lVkBlendAttachmentState[0].srcAlphaBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstAlphaBlendFactor    = VK_BLEND_FACTOR_ZERO;
        m_lVkBlendAttachmentState[0].alphaBlendOp           = VK_BLEND_OP_ADD;
        break;
    case eng_draw::BLEND_SUB_SRC_FROM_DST:
        m_lVkBlendAttachmentState[0].colorWriteMask         = 0xf;
        m_lVkBlendAttachmentState[0].blendEnable            = true;

        // Vulkan Equation: VKScreen = (SourceColor.rgb * srcColorBlendFactor) + (DestinationColor.rgb * dstColorBlendFactor)
        // DestinationColor.rgb = (DestinationColor.rgb * One) - (SourceColor.rgb * One) 
        m_lVkBlendAttachmentState[0].srcColorBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstColorBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].colorBlendOp           = VK_BLEND_OP_REVERSE_SUBTRACT;

        m_lVkBlendAttachmentState[0].srcAlphaBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstAlphaBlendFactor    = VK_BLEND_FACTOR_ZERO;
        m_lVkBlendAttachmentState[0].alphaBlendOp           = VK_BLEND_OP_ADD;
        break;
    case eng_draw::BLEND_SUB_DST_FROM_SRC:
        m_lVkBlendAttachmentState[0].colorWriteMask         = 0xf;
        m_lVkBlendAttachmentState[0].blendEnable            = true;

        // Vulkan Equation: VKScreen = (SourceColor.rgb * srcColorBlendFactor) + (DestinationColor.rgb * dstColorBlendFactor)
        // DestinationColor.rgb = (SourceColor.rgb * One) - (DestinationColor.rgb * One)
        m_lVkBlendAttachmentState[0].srcColorBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstColorBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].colorBlendOp           = VK_BLEND_OP_SUBTRACT;

        m_lVkBlendAttachmentState[0].srcAlphaBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstAlphaBlendFactor    = VK_BLEND_FACTOR_ZERO;
        m_lVkBlendAttachmentState[0].alphaBlendOp           = VK_BLEND_OP_ADD;
        break;
    case eng_draw::BLEND_MUL:
        m_lVkBlendAttachmentState[0].colorWriteMask         = 0xf;
        m_lVkBlendAttachmentState[0].blendEnable            = true;

        // Vulkan Equation: VKScreen = (SourceColor.rgb * srcColorBlendFactor) + (DestinationColor.rgb * dstColorBlendFactor)
        // DestinationColor.rgb = (SourceColor.rgb * Dst.rgb) + (DestinationColor.rgb * 0)
        m_lVkBlendAttachmentState[0].srcColorBlendFactor    = VK_BLEND_FACTOR_DST_COLOR;
        m_lVkBlendAttachmentState[0].dstColorBlendFactor    = VK_BLEND_FACTOR_ZERO;
        m_lVkBlendAttachmentState[0].colorBlendOp           = VK_BLEND_OP_ADD;

        m_lVkBlendAttachmentState[0].srcAlphaBlendFactor    = VK_BLEND_FACTOR_ONE;
        m_lVkBlendAttachmentState[0].dstAlphaBlendFactor    = VK_BLEND_FACTOR_ZERO;
        m_lVkBlendAttachmentState[0].alphaBlendOp           = VK_BLEND_OP_ADD;
        break;
    default: x_assert( false );
    } 

    //
    // VkPipelineViewportStateCreateInfo
    //      Viewport state
    //      One viewport
    //      One scissor rectangle
    m_VkViewportState.sType                             = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    m_VkViewportState.viewportCount                     = 1;
    m_VkViewportState.scissorCount                      = 1;

    //
    // VkPipelineDynamicStateCreateInfo + VkDynamicState
    //      Enable dynamic states
    //      Describes the dynamic states to be used with this pipeline
    //      Dynamic states can be set even after the pipeline has been created
    //      So there is no need to create new pipelines just for changing
    //      a viewport's dimensions or a scissor box
    //      The dynamic state properties themselves are stored in the command buffer
    m_VkDynamicState.sType                              = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    m_VkDynamicState.pDynamicStates                     = &m_lVkDynamicStateEnables[0];
    m_VkDynamicState.dynamicStateCount                  = static_cast<u32>( m_lVkDynamicStateEnables.getCount() );
        
    m_lVkDynamicStateEnables[0]                         = VK_DYNAMIC_STATE_VIEWPORT;
    m_lVkDynamicStateEnables[1]                         = VK_DYNAMIC_STATE_SCISSOR;

    //
    // VkPipelineDepthStencilStateCreateInfo
    //      Depth and stencil state
    //      Describes depth and stenctil test and compare ops
    //      Basic depth compare setup with depth writes and depth test enabled
    //      No stencil used 
    m_VkDepthStencilState.sType                         = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;

    switch( DrawPipeLine.m_ZBUFFER )
    {
        case eng_draw::ZBUFFER_ON:           m_VkDepthStencilState.depthTestEnable               = true;    
                                             m_VkDepthStencilState.depthWriteEnable              = true;    break;
        case eng_draw::ZBUFFER_OFF:          m_VkDepthStencilState.depthTestEnable               = false;
                                             m_VkDepthStencilState.depthWriteEnable              = false;   break;           
        case eng_draw::ZBUFFER_READ_ONLY:    m_VkDepthStencilState.depthTestEnable               = true;    
                                             m_VkDepthStencilState.depthWriteEnable              = false;   break;           
        default: x_assert( false );
    }

    m_VkDepthStencilState.depthCompareOp                = VK_COMPARE_OP_LESS_OR_EQUAL;
    m_VkDepthStencilState.depthBoundsTestEnable         = false;
    m_VkDepthStencilState.back.failOp                   = VK_STENCIL_OP_KEEP;
    m_VkDepthStencilState.back.passOp                   = VK_STENCIL_OP_KEEP;
    m_VkDepthStencilState.back.compareOp                = VK_COMPARE_OP_ALWAYS;
    m_VkDepthStencilState.stencilTestEnable             = false;
    m_VkDepthStencilState.front                         = m_VkDepthStencilState.back;

    //
    // VkPipelineMultisampleStateCreateInfo
    //      Multi sampling state
    //      No multi sampling
    //
    m_VkMultisampleState.sType                          = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    m_VkMultisampleState.pSampleMask                    = nullptr;
    m_VkMultisampleState.rasterizationSamples           = VK_SAMPLE_COUNT_1_BIT;
}

//-----------------------------------------------------------------------

rendering_pipeline::setup::setup(
    const VkRenderPass                                      RenderPass,
    const xbuffer_view<VkPipelineShaderStageCreateInfo>     ShaderStages,
    const VkPipelineLayout                                  aVkPipelineLayout,
    const VkPipelineVertexInputStateCreateInfo&             aVkPipeLineSetupInfo,
    const VkPrimitiveTopology                               PrimitiveTopology, 
    const engpipeline&                                      Pipeline ) noexcept
{
    auto Info       = Pipeline.getInfo();
    m_VKRenderPass  = RenderPass;

    //
    //  VkGraphicsPipelineCreateInfo
    //
    m_VkPipelineCreateInfo.sType                        = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    m_VkPipelineCreateInfo.layout                       = aVkPipelineLayout;       
    m_VkPipelineCreateInfo.renderPass                   = m_VKRenderPass;    
    m_VkPipelineCreateInfo.pVertexInputState            = &aVkPipeLineSetupInfo;
    m_VkPipelineCreateInfo.pInputAssemblyState          = &m_VkInputAssemblyState;
    m_VkPipelineCreateInfo.pRasterizationState          = &m_VkRasterizationState;
    m_VkPipelineCreateInfo.pColorBlendState             = &m_VkColorBlendState;
    m_VkPipelineCreateInfo.pMultisampleState            = &m_VkMultisampleState;
    m_VkPipelineCreateInfo.pViewportState               = &m_VkViewportState;
    m_VkPipelineCreateInfo.pDepthStencilState           = &m_VkDepthStencilState;
    m_VkPipelineCreateInfo.pDynamicState                = &m_VkDynamicState;
    m_VkPipelineCreateInfo.stageCount                   = static_cast<uint32_t>(ShaderStages.getCount());    // User must specify this manually...
    m_VkPipelineCreateInfo.pStages                      = ShaderStages;

    //
    // VkPipelineViewportStateCreateInfo
    //      Viewport state
    //      One viewport
    //      One scissor rectangle
    m_VkViewportState.sType                             = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    m_VkViewportState.viewportCount                     = 1;
    m_VkViewportState.scissorCount                      = 1;

    //
    // VkPipelineDynamicStateCreateInfo + VkDynamicState
    //      Enable dynamic states
    //      Describes the dynamic states to be used with this pipeline
    //      Dynamic states can be set even after the pipeline has been created
    //      So there is no need to create new pipelines just for changing
    //      a viewport's dimensions or a scissor box
    //      The dynamic state properties themselves are stored in the command buffer
    m_VkDynamicState.sType                              = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    m_VkDynamicState.pDynamicStates                     = &m_lVkDynamicStateEnables[0];
    m_VkDynamicState.dynamicStateCount                  = static_cast<u32>( m_lVkDynamicStateEnables.getCount() );
        
    m_lVkDynamicStateEnables[0]                         = VK_DYNAMIC_STATE_VIEWPORT;
    m_lVkDynamicStateEnables[1]                         = VK_DYNAMIC_STATE_SCISSOR;

    //
    // VkPipelineInputAssemblyStateCreateInfo
    //      Vertex input state
    //      Describes the topoloy used with this pipeline
    //      This pipeline renders vertex data as triangle lists
    m_VkInputAssemblyState.sType                        = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    m_VkInputAssemblyState.pNext                        = nullptr;
    m_VkInputAssemblyState.flags                        = 0;
    m_VkInputAssemblyState.primitiveRestartEnable       = true;
    m_VkInputAssemblyState.topology                     = PrimitiveTopology;

    //
    // VkPipelineRasterizationStateCreateInfo
    //      Rasterization state
    //      Solid polygon mode
    //      No culling
    using polygon_mode_table = xarray< VkPolygonMode, (int)eng_pipeline::primitive::raster::ENUM_COUNT, eng_pipeline::primitive::raster >;
    static const polygon_mode_table PolygonModeTable =
    []{
        polygon_mode_table PolygonModeTable;
        PolygonModeTable[eng_pipeline::primitive::raster::FILL]      = VK_POLYGON_MODE_FILL;
        PolygonModeTable[eng_pipeline::primitive::raster::WIRELINE]  = VK_POLYGON_MODE_LINE;
        PolygonModeTable[eng_pipeline::primitive::raster::POINT]     = VK_POLYGON_MODE_POINT;
        return PolygonModeTable;
    }();

    using cull_mode_flags_bits_table = xarray< VkCullModeFlagBits, (int)eng_pipeline::primitive::cull::ENUM_COUNT, eng_pipeline::primitive::cull >;
    static const cull_mode_flags_bits_table CullModeFlagsBitsTable =
    []{
        cull_mode_flags_bits_table CullModeFlagsBitsTable {};
        CullModeFlagsBitsTable[eng_pipeline::primitive::cull::BACK]  = VK_CULL_MODE_BACK_BIT;
        CullModeFlagsBitsTable[eng_pipeline::primitive::cull::FRONT] = VK_CULL_MODE_FRONT_BIT;
        CullModeFlagsBitsTable[eng_pipeline::primitive::cull::ALL]   = VK_CULL_MODE_FRONT_AND_BACK;
        CullModeFlagsBitsTable[eng_pipeline::primitive::cull::NONE]  = VK_CULL_MODE_NONE;
        return CullModeFlagsBitsTable;
    }();

    using front_face_table = xarray< VkFrontFace, (int)eng_pipeline::primitive::front_face::ENUM_COUNT, eng_pipeline::primitive::front_face >;
    static const front_face_table FrontFaceTable =
    []{
        front_face_table FrontFaceTable {};
        FrontFaceTable[eng_pipeline::primitive::front_face::CLOCKWISE]          = VK_FRONT_FACE_CLOCKWISE;
        FrontFaceTable[eng_pipeline::primitive::front_face::COUNTER_CLOCKWISE]  = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        return FrontFaceTable;
    }();

    m_VkRasterizationState.sType                        = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    m_VkRasterizationState.flags                        = 0;
    m_VkRasterizationState.pNext                        = nullptr;
    m_VkRasterizationState.lineWidth                    = Info.m_Primitive.m_LineWidth;
    m_VkRasterizationState.polygonMode                  = PolygonModeTable[ Info.m_Primitive.m_Raster ];
    m_VkRasterizationState.cullMode                     = CullModeFlagsBitsTable[ Info.m_Primitive.m_Cull ];
    m_VkRasterizationState.frontFace                    = FrontFaceTable[Info.m_Primitive.m_FrontFace];
    m_VkRasterizationState.depthClampEnable             = Info.m_DepthStencil.m_bDepthClampEnable;
    m_VkRasterizationState.rasterizerDiscardEnable      = Info.m_Primitive.m_bRasterizerDiscardEnable;
    m_VkRasterizationState.depthBiasEnable              = Info.m_DepthStencil.m_bDepthBiasEnable;
    m_VkRasterizationState.depthBiasConstantFactor      = Info.m_DepthStencil.m_DepthBiasConstantFactor;
    m_VkRasterizationState.depthBiasSlopeFactor         = Info.m_DepthStencil.m_DepthBiasSlopeFactor;
    m_VkRasterizationState.depthBiasClamp               = Info.m_DepthStencil.m_DepthBiasClamp;

    //
    // VkPipelineColorBlendStateCreateInfo  +  VkPipelineColorBlendAttachmentState
    //      Color blend state
    //      Describes blend modes and color masks
    //      One blend attachment state
    //      Blending is not used
    using blend_factor_table = xarray< VkBlendFactor, (int)eng_pipeline::blend::factor::ENUM_COUNT, eng_pipeline::blend::factor >; 
    static const blend_factor_table BlendFactorTable =
    []{
        blend_factor_table BlendFactorTable {};
        BlendFactorTable[eng_pipeline::blend::factor::ZERO]                     = VK_BLEND_FACTOR_ZERO;
        BlendFactorTable[eng_pipeline::blend::factor::ONE]                      = VK_BLEND_FACTOR_ONE;
        BlendFactorTable[eng_pipeline::blend::factor::SRC_COLOR]                = VK_BLEND_FACTOR_SRC_COLOR;
        BlendFactorTable[eng_pipeline::blend::factor::ONE_MINUS_SRC_COLOR]      = VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
        BlendFactorTable[eng_pipeline::blend::factor::DST_COLOR]                = VK_BLEND_FACTOR_DST_COLOR;
        BlendFactorTable[eng_pipeline::blend::factor::ONE_MINUS_DST_COLOR]      = VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
        BlendFactorTable[eng_pipeline::blend::factor::SRC_ALPHA]                = VK_BLEND_FACTOR_SRC_ALPHA;
        BlendFactorTable[eng_pipeline::blend::factor::ONE_MINUS_SRC_ALPHA]      = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        BlendFactorTable[eng_pipeline::blend::factor::DST_ALPHA]                = VK_BLEND_FACTOR_DST_ALPHA;
        BlendFactorTable[eng_pipeline::blend::factor::ONE_MINUS_DST_ALPHA]      = VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
        BlendFactorTable[eng_pipeline::blend::factor::CONSTANT_COLOR]           = VK_BLEND_FACTOR_CONSTANT_COLOR;
        BlendFactorTable[eng_pipeline::blend::factor::ONE_MINUS_CONSTANT_COLOR] = VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
        BlendFactorTable[eng_pipeline::blend::factor::CONSTANT_ALPHA]           = VK_BLEND_FACTOR_CONSTANT_ALPHA;
        BlendFactorTable[eng_pipeline::blend::factor::ONE_MINUS_CONSTANT_ALPHA] = VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA;
        BlendFactorTable[eng_pipeline::blend::factor::SRC_ALPHA_SATURATE]       = VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
        BlendFactorTable[eng_pipeline::blend::factor::SRC1_COLOR]               = VK_BLEND_FACTOR_SRC1_COLOR;
        BlendFactorTable[eng_pipeline::blend::factor::ONE_MINUS_SRC1_COLOR]     = VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
        BlendFactorTable[eng_pipeline::blend::factor::SRC1_ALPHA]               = VK_BLEND_FACTOR_SRC1_ALPHA;
        BlendFactorTable[eng_pipeline::blend::factor::ONE_MINUS_SRC1_ALPHA]     = VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
        return BlendFactorTable;
    }();

    using blend_operation_table = xarray< VkBlendOp, (int)eng_pipeline::blend::op::ENUM_COUNT, eng_pipeline::blend::op >;
    static const blend_operation_table BlendOperationTable =
    []{
        blend_operation_table BlendOperationTable {};
        BlendOperationTable[eng_pipeline::blend::op::ADD]                   = VK_BLEND_OP_ADD;
        BlendOperationTable[eng_pipeline::blend::op::SUBTRACT]              = VK_BLEND_OP_SUBTRACT;
        BlendOperationTable[eng_pipeline::blend::op::REVERSE_SUBTRACT]      = VK_BLEND_OP_REVERSE_SUBTRACT;
        BlendOperationTable[eng_pipeline::blend::op::MIN]                   = VK_BLEND_OP_MIN;
        BlendOperationTable[eng_pipeline::blend::op::MAX]                   = VK_BLEND_OP_MAX;
        return BlendOperationTable;
    }();

    m_VkColorBlendState.sType                           = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    m_VkColorBlendState.attachmentCount                 = static_cast<u32>( m_lVkBlendAttachmentState.getCount() );
    m_VkColorBlendState.pAttachments                    = &m_lVkBlendAttachmentState[0];

    m_lVkBlendAttachmentState[0].colorWriteMask         = Info.m_Blend.m_ColorWriteMask;
    m_lVkBlendAttachmentState[0].blendEnable            = Info.m_Blend.m_bEnable;
    m_lVkBlendAttachmentState[0].srcColorBlendFactor    = BlendFactorTable[ Info.m_Blend.m_ColorSrcFactor ];
    m_lVkBlendAttachmentState[0].dstColorBlendFactor    = BlendFactorTable[ Info.m_Blend.m_ColorDstFactor ];
    m_lVkBlendAttachmentState[0].colorBlendOp           = BlendOperationTable[ Info.m_Blend.m_ColorOperation ];
    m_lVkBlendAttachmentState[0].srcAlphaBlendFactor    = BlendFactorTable[ Info.m_Blend.m_AlphaSrcFactor ];
    m_lVkBlendAttachmentState[0].dstAlphaBlendFactor    = BlendFactorTable[ Info.m_Blend.m_AlphaDstFactor ];
    m_lVkBlendAttachmentState[0].alphaBlendOp           = BlendOperationTable[ Info.m_Blend.m_AlphaOperation ];

    //
    // VkPipelineDepthStencilStateCreateInfo
    //      Depth and stencil state
    //      Describes depth and stenctil test and compare ops
    //      Basic depth compare setup with depth writes and depth test enabled
    //      No stencil used 
    using stencil_operations_table = xarray< VkStencilOp, (int)eng_pipeline::depth_stencil::stencil_op::ENUM_COUNT, eng_pipeline::depth_stencil::stencil_op >;
    static const stencil_operations_table StencilOperationsTable =
    []{
        stencil_operations_table StencilOperationsTable {};
        StencilOperationsTable[eng_pipeline::depth_stencil::stencil_op::KEEP]                   =  VK_STENCIL_OP_KEEP;
        StencilOperationsTable[eng_pipeline::depth_stencil::stencil_op::ZERO]                   =  VK_STENCIL_OP_ZERO;
        StencilOperationsTable[eng_pipeline::depth_stencil::stencil_op::REPLACE]                =  VK_STENCIL_OP_REPLACE;
        StencilOperationsTable[eng_pipeline::depth_stencil::stencil_op::INCREMENT_AND_CLAMP]    =  VK_STENCIL_OP_INCREMENT_AND_CLAMP;
        StencilOperationsTable[eng_pipeline::depth_stencil::stencil_op::DECREMENT_AND_CLAMP]    =  VK_STENCIL_OP_DECREMENT_AND_CLAMP;
        StencilOperationsTable[eng_pipeline::depth_stencil::stencil_op::INVERT]                 =  VK_STENCIL_OP_INVERT;
        StencilOperationsTable[eng_pipeline::depth_stencil::stencil_op::INCREMENT_AND_WRAP]     =  VK_STENCIL_OP_INCREMENT_AND_WRAP;
        StencilOperationsTable[eng_pipeline::depth_stencil::stencil_op::DECREMENT_AND_WRAP]     =  VK_STENCIL_OP_DECREMENT_AND_WRAP;
        return StencilOperationsTable;
    }();

    using depth_compare_table = xarray< VkCompareOp, (int)eng_pipeline::depth_stencil::depth_compare::ENUM_COUNT, eng_pipeline::depth_stencil::depth_compare >;
    static const depth_compare_table DepthCompareTable =
    []{
        depth_compare_table DepthCompareTable {};
        DepthCompareTable[eng_pipeline::depth_stencil::depth_compare::LESS]                 =  VK_COMPARE_OP_LESS; 
        DepthCompareTable[eng_pipeline::depth_stencil::depth_compare::LESS_OR_EQUAL]        =  VK_COMPARE_OP_LESS_OR_EQUAL;
        DepthCompareTable[eng_pipeline::depth_stencil::depth_compare::GREATER]              =  VK_COMPARE_OP_GREATER;
        DepthCompareTable[eng_pipeline::depth_stencil::depth_compare::NOT_EQUAL]            =  VK_COMPARE_OP_NOT_EQUAL;
        DepthCompareTable[eng_pipeline::depth_stencil::depth_compare::GREATER_OR_EQUAL]     =  VK_COMPARE_OP_GREATER_OR_EQUAL;
        DepthCompareTable[eng_pipeline::depth_stencil::depth_compare::EQUAL]                =  VK_COMPARE_OP_EQUAL; 
        DepthCompareTable[eng_pipeline::depth_stencil::depth_compare::NEVER]                =  VK_COMPARE_OP_NEVER;
        DepthCompareTable[eng_pipeline::depth_stencil::depth_compare::ALWAYS]               =  VK_COMPARE_OP_ALWAYS;
        return DepthCompareTable;
    }();

    m_VkDepthStencilState.sType                         = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    m_VkDepthStencilState.pNext                         = nullptr;
    m_VkDepthStencilState.flags                         = 0;
    m_VkDepthStencilState.depthTestEnable               = Info.m_DepthStencil.m_bDepthTestEnable;
    m_VkDepthStencilState.depthWriteEnable              = Info.m_DepthStencil.m_bDepthWriteEnable;
    m_VkDepthStencilState.depthCompareOp                = DepthCompareTable[ Info.m_DepthStencil.m_DepthCompare ];
    m_VkDepthStencilState.depthBoundsTestEnable         = Info.m_DepthStencil.m_bDepthBoundsTestEnable;
    m_VkDepthStencilState.stencilTestEnable             = Info.m_DepthStencil.m_StencilTestEnable;

    m_VkDepthStencilState.front.compareOp               = DepthCompareTable[ Info.m_DepthStencil.m_StencilFrontFace.m_CompareOp ];
    m_VkDepthStencilState.front.depthFailOp             = StencilOperationsTable[ Info.m_DepthStencil.m_StencilFrontFace.m_DepthFailOp ];
    m_VkDepthStencilState.front.failOp                  = StencilOperationsTable[ Info.m_DepthStencil.m_StencilFrontFace.m_FailOp ];
    m_VkDepthStencilState.front.passOp                  = StencilOperationsTable[ Info.m_DepthStencil.m_StencilFrontFace.m_PassOp ];
    m_VkDepthStencilState.front.reference               = Info.m_DepthStencil.m_StencilFrontFace.m_Reference;
    m_VkDepthStencilState.front.compareMask             = Info.m_DepthStencil.m_StencilFrontFace.m_CompareMask;

    m_VkDepthStencilState.back.compareOp                = DepthCompareTable[ Info.m_DepthStencil.m_StencilBackFace.m_CompareOp ];
    m_VkDepthStencilState.back.depthFailOp              = StencilOperationsTable[ Info.m_DepthStencil.m_StencilBackFace.m_DepthFailOp ];
    m_VkDepthStencilState.back.failOp                   = StencilOperationsTable[ Info.m_DepthStencil.m_StencilBackFace.m_FailOp ];
    m_VkDepthStencilState.back.passOp                   = StencilOperationsTable[ Info.m_DepthStencil.m_StencilBackFace.m_PassOp ];
    m_VkDepthStencilState.back.reference                = Info.m_DepthStencil.m_StencilBackFace.m_Reference;
    m_VkDepthStencilState.back.compareMask              = Info.m_DepthStencil.m_StencilBackFace.m_CompareMask;

    m_VkDepthStencilState.minDepthBounds                = Info.m_DepthStencil.m_DepthMinBounds;
    m_VkDepthStencilState.maxDepthBounds                = Info.m_DepthStencil.m_DepthMaxBounds;

    //
    // VkPipelineMultisampleStateCreateInfo
    //      Multi sampling state
    //      No multi sampling
    //
    m_VkMultisampleState.sType                          = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    m_VkMultisampleState.pNext                          = nullptr;
    m_VkMultisampleState.flags                          = 0;
    m_VkMultisampleState.pSampleMask                    = nullptr;
    m_VkMultisampleState.rasterizationSamples           = VK_SAMPLE_COUNT_1_BIT;
    m_VkMultisampleState.sampleShadingEnable            = false;
    m_VkMultisampleState.minSampleShading               = 0;
    m_VkMultisampleState.alphaToCoverageEnable          = false;
    m_VkMultisampleState.alphaToOneEnable               = false;
}

//-----------------------------------------------------------------------

void rendering_pipeline::Initialize( 
    device&                                         Device, 
    const setup&                                    Setup, 
    const VkPipelineCache                           pipelineCache ) noexcept
{
    // Create rendering pipeline
    VkResult VKErr = vkCreateGraphicsPipelines( 
        Device.getVKDevice(), 
        pipelineCache, 
        1, 
        &Setup.m_VkPipelineCreateInfo, 
        nullptr, 
        &m_VkPipeline );
    assert( !VKErr );
}

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
// rendering_pipeline_hash
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------

rendering_pipeline& rendering_pipeline_hash::getDrawPipeLine( 
    VkRenderPass                                    VKRenderPass,
    const eng_draw::pipeline                        DrawPipeLine, 
    const bool                                      bRenderPolys ) noexcept
{
    static x_atomic<int> Count = 0;
    // Compute the hash form a draw state
    u32 Hash  = DrawPipeLine.m_Flags & ~u32( eng_draw::pipeline::MASK_MISC );
    Hash     |= bRenderPolys ? (1<<17) : 0;

    rendering_pipeline* pPipeLine = nullptr;

    // Now get the pipeline
    m_HashTable.cpFindOrAddEntry( Hash, [&]( rendering_pipeline& PipeLine )
    {
        if( PipeLine.isValid() == false )
        {
            Count++;
            auto& DrawSystem = m_Device.getDrawSystem();

            rendering_pipeline::setup Setup( 
                VKRenderPass,
                DrawSystem.getShaderStages(),
                DrawSystem.getVkPipelineLayout(),
                DrawSystem.getCreationInfo(),
                DrawPipeLine,
                bRenderPolys );
            PipeLine.Initialize( m_Device, Setup, m_Device.getVKPipelineCash() );
        }

        pPipeLine = &PipeLine;
    });

    x_assume(pPipeLine);
    return *pPipeLine;
}

//------------------------------------------------------------------------------------

rendering_pipeline& rendering_pipeline_hash::getPipeLine( 
    VkRenderPass                                    VKRenderPass,
    material_informed&                              MaterialInformed,
    const engpipeline&                              EngPipeline,
    const u32                                       InformedMaterialMixHash ) noexcept
{
    static x_atomic<int>    Count = 0;
    rendering_pipeline*     pPipeLine = nullptr;

    // Now get the pipeline
    m_HashTable.cpFindOrAddEntry( InformedMaterialMixHash, [&]( rendering_pipeline& PipeLine )
    {
        if( PipeLine.isValid() == false )
        {
            Count++;
            auto& MaterialType = MaterialInformed.getMaterialType();
            auto& DrawSystem = m_Device.getDrawSystem();
            rendering_pipeline::setup Setup( 
                VKRenderPass,
                MaterialType.getVKShaderStages(),
                MaterialType.getVKPipelineLayout(),                     // DrawSystem.m_VkPipelineLayout,
                DrawSystem.getCreationInfo(),
                VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                EngPipeline );     // This descrives the vertex information we need to get his from vertex shader

            PipeLine.Initialize( m_Device, Setup, m_Device.getVKPipelineCash() );
        }

        pPipeLine = &PipeLine;
    });

    x_assume(pPipeLine);
    return *pPipeLine;
}

//------------------------------------------------------------------------------------
// END NAMESPACE
//------------------------------------------------------------------------------------
};