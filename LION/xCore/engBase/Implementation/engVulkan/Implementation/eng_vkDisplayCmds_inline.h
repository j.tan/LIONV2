//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// display_cmd_pool
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------
inline      
void display_cmds::Draw(
    eng_draw::pipeline                  PipeLine,
    xfunction<void( eng_draw& Draw )>   Callback )          noexcept
{
    auto& Draw = getDraw();
    Draw.Begin( PipeLine );
    Callback( Draw );
    Draw.End();
}


//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// display_cmd_pool
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
inline
display_cmd_pool::display_cmd_pool( device& Device ) noexcept :
    m_DelegateJustAfterPageFlip { *this, &display_cmd_pool::onJustAfterPageFlip,  Device.getEvents().m_JustAfterPageFlip  }
{
    x_assert_linear( m_Debug_LQ );
    m_DisplayCmdPool.appendList( MAX_DISPLAY_CMDS, Device );
}

//---------------------------------------------------------------------------------------
inline
display_cmds& display_cmd_pool::AllocateDisplayCmd( const int FrameIndex, const float Order, window* pWindow ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert( m_nFreeEntries > 0 );
    const auto  iEntry      = m_FreeCmds[ --m_nFreeEntries ];
    auto&       DisplayCmds = m_DisplayCmdPool[ iEntry ];

    DisplayCmds.m_Order     = Order;
    DisplayCmds.m_pWindow   = pWindow;

    //
    // Make sure that the command buffer is initialized
    //
    if( DisplayCmds.m_VKCmdBuffer == VK_NULL_HANDLE )
    {
        auto& Device = m_DisplayCmdPool[0].getTheDevice();

        VkCommandBufferAllocateInfo cmdBufAllocateInfo = {};
        cmdBufAllocateInfo.sType                 = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        cmdBufAllocateInfo.commandPool           = m_VKCmdPool;
        cmdBufAllocateInfo.level                 = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
        cmdBufAllocateInfo.commandBufferCount    = 1;

        auto VKErr = vkAllocateCommandBuffers( Device.getVKDevice(), &cmdBufAllocateInfo, &DisplayCmds.m_VKCmdBuffer );
        x_assert( !VKErr );
    }

    return DisplayCmds;
}

//---------------------------------------------------------------------------------------
inline      
void display_cmd_pool::SubmitCmdList( const int FrameIndex, display_cmds& DisplayCmdList ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    x_assert( DisplayCmdList.m_bSubmitted == false );
    auto&   InUsedCmdList   = m_InUsedCmdLists[ FrameIndex ];

    DisplayCmdList.m_bSubmitted = true;
    InUsedCmdList.m_lInUsed[ InUsedCmdList.m_nCmdInUsed++ ] = static_cast<s16>(m_DisplayCmdPool.getIndexByEntry( DisplayCmdList ));
}

//---------------------------------------------------------------------------------------
inline
void display_cmd_pool::onJustAfterPageFlip( void ) noexcept
{
    x_assert_linear( m_Debug_LQ );

    auto&       Device          = m_DisplayCmdPool[0].getTheDevice();
    const auto  FrameIndex      = Device.getFrameIndex();
    auto&       InUsedCmdList   = m_InUsedCmdLists[FrameIndex];
     
    for( xuptr i = 0; i < InUsedCmdList.m_nCmdInUsed; i++ )
    {
        const auto  iEntry      = InUsedCmdList.m_lInUsed[i];
        auto&       DisplayCmds = m_DisplayCmdPool[ iEntry ];
        
        // Clear the command list
        DisplayCmds.Reset();

        // Submit to the empty pool
        m_FreeCmds[m_nFreeEntries++] = iEntry;
    }

    // Clear the count
    InUsedCmdList.m_nCmdInUsed = 0;
}

//---------------------------------------------------------------------------------------
inline      
auto display_cmd_pool::Initialize( int iQueueFamilyIndex ) noexcept
{
    x_assert_linear( m_Debug_LQ );

    //
    // Create the VKPool
    //
    VkCommandPoolCreateInfo cmdPoolInfo = {};
    cmdPoolInfo.sType               = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmdPoolInfo.queueFamilyIndex    = iQueueFamilyIndex;
    cmdPoolInfo.flags               = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT | VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    auto VKErr = vkCreateCommandPool( m_DisplayCmdPool[0].getTheDevice().getVKDevice(), &cmdPoolInfo, nullptr, &m_VKCmdPool );
    if( VKErr )
    {
        static const device::err Error x_error_code( device::errors, ERR_FAILURE, "Fail to create the per thread command pools" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    //
    // Initialize the free list
    //
    s16 Total = static_cast<s16>(m_FreeCmds.getCount());
    for( auto& iEntry : m_FreeCmds ) iEntry = --Total;

    return device::err{ x_error_ok( device::errors ) };
}

//---------------------------------------------------------------------------------------
inline      
display_cmd_pool::~display_cmd_pool( void ) noexcept
{
    x_assert_linear( m_Debug_LQ );
    auto VKDevice = m_DisplayCmdPool[0].getTheDevice().getVKDevice();

    for( auto& Entry : m_DisplayCmdPool )
    {
        if( Entry.m_VKCmdBuffer )
            vkFreeCommandBuffers( VKDevice, m_VKCmdPool,  static_cast<u32>(1), &Entry.m_VKCmdBuffer );
    }

    vkDestroyCommandPool( VKDevice, m_VKCmdPool, nullptr );
}

