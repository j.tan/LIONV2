//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// buffer
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
inline
void buffer::CreateBuffer(   
    int                 EntrySize, 
    xuptr               Count,
    VkBufferUsageFlags  Usage,
    VkSharingMode       Sharing,
    uint32_t            nFamilyIndex, 
    uint32_t*           pQueueFamilyIndices ) noexcept  
{
    x_assert_linear( m_Debug_LQ ); 
    memory_pool&        MemoryPool  =  m_Device.getMemoryPool(); 

    m_EntrySize                 = EntrySize;

    VkDeviceSize        Size = m_EntrySize * Count;

    VkBufferCreateInfo  Info = {};
    Info.sType                  = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    Info.pNext                  = nullptr;
    Info.flags                  = 0;
    Info.size                   = Size;
    Info.usage                  = Usage | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    Info.sharingMode            = Sharing;
    Info.queueFamilyIndexCount  = nFamilyIndex;
    Info.pQueueFamilyIndices    = pQueueFamilyIndices;
 
    auto        Device  = m_Device.getVKDevice();
    VkResult    VKErr   = vkCreateBuffer( 
        Device, 
        &Info, 
        nullptr, 
        &m_Buffer );
    x_assert( VKErr == 0 );

    auto Err = MemoryPool.Alloc( 
        m_Allocation,
        Size, 
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT     | 
        VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
        );
    x_assert( !Err );

    VKErr = vkBindBufferMemory( 
        Device, 
        m_Buffer, 
        m_Allocation.m_VKDeviceMemory, 
        m_Allocation.m_Offset );
    x_assert( VKErr == 0 );
}

//---------------------------------------------------------------------------------------
inline
void buffer::TransferData( const xbuffer_view<xbyte*> SrcData ) noexcept
{
    x_assert_linear( m_Debug_LQ ); 
    auto pData = CreateMap<xbyte>();
    x_assert(pData);
    x_assert( SrcData.getByteSize() < m_Allocation.m_Size );
    memcpy( pData, &SrcData[0], SrcData.getByteSize() );
    ReleaseMap();
}

//---------------------------------------------------------------------------------------
inline
buffer::~buffer( void ) noexcept 
{
    if( m_Buffer != VK_NULL_HANDLE )
    {
        m_Device.getMemoryPool().Free( m_Allocation );
        vkDestroyBuffer( m_Device.getVKDevice(), m_Buffer, nullptr );
    }
}
 
//---------------------------------------------------------------------------------------
inline
buffer::buffer( buffer&& buf ) noexcept : m_Device{ buf.m_Device }
{
    x_assert( m_Buffer == VK_NULL_HANDLE );
    m_Allocation  = buf.m_Allocation;
    m_Buffer      = buf.m_Buffer;
    m_EntrySize   = buf.m_EntrySize;

    // Invalidate buf
    buf.m_Buffer = VK_NULL_HANDLE;
}

//---------------------------------------------------------------------------------------
inline
void buffer::ReleaseMap( void ) noexcept
{
    x_assert_linear( m_Debug_LQ ); 

    /*
    memory_pool&    MemoryPool  =  s_lMemoryPools[ g_context::get().m_Scheduler.getWorkerUID() ];
    vkUnmapMemory( MemoryPool.getEngine().getDevice(), m_Allocation.m_VKDeviceMemory );
    */

    xarray<VkMappedMemoryRange,1> range { VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE };
    range[0].memory    = m_Allocation.m_VKDeviceMemory;
    range[0].offset    = m_Allocation.m_Offset;
    range[0].size      = m_Allocation.m_Size;
    vkFlushMappedMemoryRanges( 
        m_Device.getVKDevice(), 
        static_cast<uint32_t>(range.getCount()), 
        &range[0] );
}


//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// buffer_paged
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
inline
void buffer_paged::Initialize( const setup& Setup ) noexcept
{
    x_assert( Setup.m_MaxBuffers        >   0   );
    x_assert( Setup.m_EntryPerBuffer    >   10  );
    x_assert( Setup.m_EntrySize         >   1   );

    m_Flags             = Setup.m_Flags;
    m_MaxBuffers        = Setup.m_MaxBuffers;
    m_EntryPerBuffer    = Setup.m_EntryPerBuffer;
    m_EntrySize         = Setup.m_EntrySize;
    m_nActiveBuffers    = 0;
             
    m_EngBuffer.New( m_MaxBuffers, m_Device );
    m_lEntries.New( m_MaxBuffers );
}

//---------------------------------------------------------------------------------------
inline
void buffer_paged::Upload( void ) noexcept
{
    x_lk_guard( m_SpinLock );
    x_assert_linear( m_Debug_LQ ); 

    for( int i = 0; i < m_nActiveBuffers; i++ )
    {
        auto& Entry = m_lEntries[i];
        x_assert( Entry.m_pStart );

        // Unmap any pending memory
        m_EngBuffer[ Entry.m_iBuffer ].ReleaseMap( );
                
        // Reset the entry
        Entry.m_pStart          = nullptr;
        Entry.m_nEntriesLeft    = m_EntryPerBuffer;
    }
    m_nActiveBuffers = 0;
}

//---------------------------------------------------------------------------------------
inline
buffer_paged::alloc_return buffer_paged::Alloc( xuptr nEntires ) noexcept
{
    x_lk_guard( m_SpinLock );
    x_assert( nEntires <= m_EntryPerBuffer );
    x_assert_linear( m_Debug_LQ ); 

    int i;
    for( i = 0; i < m_nActiveBuffers; i++ )
    {
        if( m_lEntries[i].m_nEntriesLeft >= nEntires )
            break;
    }

    if( i == m_nActiveBuffers )
    {
        auto& NewEngBuff = m_EngBuffer[m_nActiveBuffers ];
        auto& NewEntry   = m_lEntries[m_nActiveBuffers ];

        x_assert( m_nActiveBuffers < m_MaxBuffers );

        if( NewEngBuff.getCount() == 0 ) 
            NewEngBuff.CreateBuffer( m_EntrySize, m_EntryPerBuffer, m_Flags );

        NewEntry.m_pStart          = NewEngBuff.CreateMap<xbyte>();
        NewEntry.m_nEntriesLeft    = m_EntryPerBuffer;
        NewEntry.m_iBuffer         = m_nActiveBuffers;

        i = m_nActiveBuffers;
        m_nActiveBuffers++;
    }

    alloc_return    Ret;
    Ret.m_Offset    = m_EntryPerBuffer - m_lEntries[i].m_nEntriesLeft;
    Ret.m_pStart    = &m_lEntries[i].m_pStart[ Ret.m_Offset * m_EntrySize ]; 
    Ret.m_iBuffer   = i;
    Ret.m_Count     = nEntires;

    // Remove all entries used
    m_lEntries[i].m_nEntriesLeft -= nEntires;

    return Ret;

    /*
    xuptr       iLocation;
    const bool  bFound = x_BinSearch<entry>( x_buffer_view<entry>( m_lEntries, m_nActiveBuffers ), iLocation, [&]( const entry& Entry ) -> int
    {
        if( nEntires < Entry.m_nEntriesLeft ) return -1;
        return nEntires > Entry.m_nEntriesLeft;
    });

    if( bFound )
    {
        m_lEntries[iLocation].m_nEntriesLeft 
    }
    */
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
// buffer_double_paged
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
inline 
buffer_double_paged::buffer_double_paged( device& Device ) noexcept : 
    m_Device        { Device },                
    m_VertexBuffer  { Device, Device }, 
    m_IndexBuffer   { Device, Device },
    m_DelegateJustAfterPageFlip{  *this, &buffer_double_paged::onJustAfterPageFlip,   Device.getEvents().m_JustAfterPageFlip  },
    m_DelegateJustBeforePageFlip{ *this, &buffer_double_paged::onJustBeforePageFlip,  Device.getEvents().m_JustBeforePageFlip }
    {};

//-----------------------------------------------------------------------
inline
void buffer_double_paged::Initialize( const setup& Setup, const xbuffer_view<tools::vertex_desc::attribute> VertexDesc ) noexcept
{
    //
    // Vertex Buffer
    //
    {
        buffer_paged::setup BufferSetup;
        BufferSetup.m_Flags           = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
        BufferSetup.m_MaxBuffers      = Setup.m_MaxVertexBuffers;
        BufferSetup.m_EntryPerBuffer  = Setup.m_MaxVerticesPerBuffer;
        BufferSetup.m_EntrySize       = Setup.m_VertexEntrySize;
        for( int i=0; i<FRAMES_BEFORE_REUSING_BUFFER; i++ )
            m_VertexBuffer[i].Initialize( BufferSetup );
    }

    //
    // Index Buffer
    //
    {
        buffer_paged::setup BufferSetup;
        BufferSetup.m_Flags           = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
        BufferSetup.m_MaxBuffers      = Setup.m_MaxIndexBuffers;
        BufferSetup.m_EntryPerBuffer  = Setup.m_MaxIndicesPerBuffer;
        BufferSetup.m_EntrySize       = sizeof(u32);
        for( int i=0; i<FRAMES_BEFORE_REUSING_BUFFER; i++ )
            m_IndexBuffer[i].Initialize( BufferSetup );
    }

    //
    // Initialize vertex descritors
    //
    m_VDesc.setup( Setup.m_VertexEntrySize, VertexDesc );
}

//---------------------------------------------------------------------------------------
inline 
buffer_double_paged::vertex_alloc buffer_double_paged::AllocVertex( xuptr Count ) noexcept
{
    auto                                AllocRet = m_VertexBuffer[m_Index].Alloc( Count );
    buffer_double_paged::vertex_alloc   VertAlloc;

    VertAlloc.m_pStart  = AllocRet.m_pStart; 
    VertAlloc.m_Offset  = static_cast<uint32_t>(AllocRet.m_Offset);
    VertAlloc.m_iBuffer = static_cast<i_vertex>(AllocRet.m_iBuffer);
    VertAlloc.m_Count   = AllocRet.m_Count;

    return VertAlloc;
}

//---------------------------------------------------------------------------------------
inline 
buffer_double_paged::index_alloc buffer_double_paged::AllocIndex( xuptr Count ) noexcept
{
    auto                                AllocRet = m_IndexBuffer[m_Index].Alloc( Count );
    buffer_double_paged::index_alloc    IndexAlloc;

    IndexAlloc.m_pStart  = reinterpret_cast<u32*>   (AllocRet.m_pStart); 
    IndexAlloc.m_Offset  = static_cast<uint32_t>    (AllocRet.m_Offset);
    IndexAlloc.m_iBuffer = static_cast<i_index>     (AllocRet.m_iBuffer);
    IndexAlloc.m_Count   = AllocRet.m_Count;

    return IndexAlloc;
}

//---------------------------------------------------------------------------------------
inline auto buffer_double_paged::getVertexVKBuffer          ( i_vertex iBuffer ) const noexcept { return m_VertexBuffer[m_Index].getVKBuffer        (iBuffer.m_Value); }
inline auto buffer_double_paged::getVertexVKBufferOffset    ( i_vertex iBuffer ) const noexcept { return m_VertexBuffer[m_Index].getVKBufferOffset  (iBuffer.m_Value); }
inline auto buffer_double_paged::getIndexVKBuffer           ( i_index  iBuffer ) const noexcept { return m_IndexBuffer [m_Index].getVKBuffer        (iBuffer.m_Value); }
inline auto buffer_double_paged::getIndexVKBufferOffset     ( i_index  iBuffer ) const noexcept { return m_IndexBuffer [m_Index].getVKBufferOffset  (iBuffer.m_Value); }

//---------------------------------------------------------------------------------------
inline
void buffer_double_paged::onJustBeforePageFlip( void ) noexcept
{
    // We are calling adter the page flip which means that the frame index will be updated
    // so the one that we need to upload are the one from the previous frame so we must
    // reverse the frame index

    // todo: we shoud not need to upload the hold buffer, just upload as much memory as needed
    m_VertexBuffer[m_Index].Upload();
    m_IndexBuffer [m_Index].Upload();
 }

//---------------------------------------------------------------------------------------
inline
void buffer_double_paged::onJustAfterPageFlip( void ) noexcept
{
    m_Index = ( m_Index + 1 )%FRAMES_BEFORE_REUSING_BUFFER;
}



#if 0
//---------------------------------------------------------------------------------------
inline
buffer_global_pool::buffer_global_pool( device& Device ) noexcept :
    m_DelegateJustAfterPageFlip { *this, &buffer_global_pool::onJustAfterPageFlip, Device.getEvents().m_JustAfterPageFlip  },
    m_Device( Device )
{
    m_BufferPool.appendList( MAX_DISPLAY_CMDS, Device );
}

//---------------------------------------------------------------------------------------
inline      
auto display_cmd_pool::Initialize( const setup& Setup ) noexcept
{

    //
    // Initialize the free list
    //
    s16 Total = static_cast<s16>(m_FreeCmds.getCount());
    for( auto& iEntry : m_FreeCmds ) iEntry = --Total;

    return x_error_none( device::errors );
}

#endif

