//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

#define VERTEX_BUFFER_BIND_ID   0
#define TEXTURE_BIND_ID         0


//---------------------------------------------------------------------------------

void draw_system::Kill ( void ) noexcept
{

}

//------------------------------------------------------------------------------------

VkDescriptorSet draw_system::AllocDescriptorSet( void )  noexcept
{
    auto& DescriptorSetPool     = m_DescriptorsInUsed[m_Device.getFrameIndex()];
    auto& VKDescriptorSet       = DescriptorSetPool.m_Pool[DescriptorSetPool.m_Count++];

    if( VKDescriptorSet == nullptr )
    {
        x_lk_guard_get( m_LockedVKDescriptorPool, VKDescriptorPool );

        VkDescriptorSetAllocateInfo allocInfo   = {};
        allocInfo.sType                         = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool                = VKDescriptorPool;
        allocInfo.descriptorSetCount            = m_VkDescriptorSetLayout.getCount<uint32_t>();
        allocInfo.pSetLayouts                   = m_VkDescriptorSetLayout;
                
        VkResult VKErr = vkAllocateDescriptorSets( m_Device.getVKDevice(), &allocInfo, &VKDescriptorSet );
        x_assert( !VKErr );
    }

    return VKDescriptorSet;
}

//------------------------------------------------------------------------------------

void draw_system::onJustAfterPageFlip( void ) noexcept
{
    const auto  iPrev = 1 - m_Device.getFrameIndex();

    // Reset the descriptor set for this pool
    m_DescriptorsInUsed[iPrev].m_Count.store(0);
}

//------------------------------------------------------------------------------------

void draw_system::setupDescriptorSetLayout( device& Device ) noexcept
{
    VkResult VKErr;

    //
    // Set the texture descriptor
    //

    // Create descriptor layout to match the shader resources
    xarray<VkDescriptorSetLayoutBinding,1> layoutBinding    = {};
    layoutBinding[0].binding                                = TEXTURE_BIND_ID;
    layoutBinding[0].descriptorType                         = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    layoutBinding[0].descriptorCount                        = 1;
    layoutBinding[0].stageFlags                             = VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBinding[0].pImmutableSamplers                     = nullptr;

    VkDescriptorSetLayoutCreateInfo descriptorLayout        = {};
    descriptorLayout.sType                                  = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorLayout.pNext                                  = nullptr;
    descriptorLayout.bindingCount                           = layoutBinding.getCount<uint32_t>();
    descriptorLayout.pBindings                              = layoutBinding;

    VKErr = vkCreateDescriptorSetLayout( Device.getVKDevice(), &descriptorLayout, nullptr, &m_VkDescriptorSetLayout[0] );
    assert( !VKErr );

    //
    // Set the push constants for this pipeline
    //
    xarray<VkPushConstantRange,1> pushConstantRange         = {};
    pushConstantRange[0].stageFlags                         = VK_SHADER_STAGE_VERTEX_BIT;
    pushConstantRange[0].offset                             = 0;
    pushConstantRange[0].size                               = sizeof(xmatrix4);

    //
    // Set the pipeline descriptor 
    //
    VkPipelineLayoutCreateInfo PipelineLayoutCreateInfo    = {};
    PipelineLayoutCreateInfo.sType                         = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    PipelineLayoutCreateInfo.pNext                         = nullptr;

    // Set the descriptors for the texture
    PipelineLayoutCreateInfo.setLayoutCount                = m_VkDescriptorSetLayout.getCount<uint32_t>();
    PipelineLayoutCreateInfo.pSetLayouts                   = m_VkDescriptorSetLayout;

    // Push constant ranges are part of the pipeline layout
    PipelineLayoutCreateInfo.pushConstantRangeCount        = pushConstantRange.getCount<uint32_t>();
    PipelineLayoutCreateInfo.pPushConstantRanges           = pushConstantRange;

    VKErr = vkCreatePipelineLayout( Device.getVKDevice(), &PipelineLayoutCreateInfo, nullptr, &m_VkPipelineLayout );
    assert( !VKErr );
}

//------------------------------------------------------------------------------------

void draw_system::setupDescriptorPool( device& Device ) noexcept
{
    // We need to tell the API the number of max. requested descriptors per type
    xarray<VkDescriptorPoolSize,1> typeCounts;

    // This example only uses one descriptor type (uniform buffer) and only
    // requests one descriptor of this type
    typeCounts[0].type                              = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    typeCounts[0].descriptorCount                   = 1;    // Draw uses only one texture

    // Create the global descriptor pool
    // All descriptors used in this example are allocated from this pool
    VkDescriptorPoolCreateInfo descriptorPoolInfo   = {};
    descriptorPoolInfo.sType                        = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolInfo.pNext                        = nullptr;
    descriptorPoolInfo.poolSizeCount                = typeCounts.getCount<uint32_t>();
    descriptorPoolInfo.pPoolSizes                   = typeCounts;
    descriptorPoolInfo.maxSets                      = 2*MAX_DESCRIPTOR_SETS;  

    x_lk_guard_get( m_LockedVKDescriptorPool, VKDescriptorPool );
    VkResult VKErr = vkCreateDescriptorPool( Device.getVKDevice(), &descriptorPoolInfo, nullptr, &VKDescriptorPool );
    x_assert( !VKErr );
}

//------------------------------------------------------------------------------------

void draw_system::preparePipelines( device& Device, VkRenderPass RenderPass, const VkPipelineCache pipelineCache ) noexcept
{
static const char s_DrawShader[] = 
R"(

    #version 450

    #extension GL_ARB_separate_shader_objects  : enable
    #extension GL_ARB_shading_language_420pack : enable

    layout (location = 0) in vec3 inPos;
    layout (location = 1) in vec2 inUV;
    layout (location = 2) in vec4 inColor;

    layout (std140, push_constant) uniform PushConsts 
    {
        mat4 L2C;
    } pushConsts;

    layout (location = 0) out vec4 outColor;
    layout (location = 1) out vec2 outUV;

    void main() 
    {
        outColor    = inColor;
        outUV       = inUV;
        gl_Position = pushConsts.L2C * vec4(inPos.xyz, 1.0);
    }

)";

    //
    // Setup vertex shader
    //
    {
        static 
        #include "draw.vert.spv.h"
        auto Err = m_Shader[0].CreateShader( { (xbyte*)draw_vert, static_cast<xuptr>(draw_vert_size) }, VK_SHADER_STAGE_VERTEX_BIT );
        x_assert( !Err );
        m_ShaderStages[0] = m_Shader[0].getStateCreateInfo();
    }

    //
    // Setup fragment shader
    //
    {
        static 
        #include "draw.frag.spv.h"
        auto Err = m_Shader[1].CreateShader( {(xbyte*)draw_frag, static_cast<xuptr>(draw_frag_size) }, VK_SHADER_STAGE_FRAGMENT_BIT );
        x_assert( !Err );
        m_ShaderStages[1] = m_Shader[1].getStateCreateInfo();
    }
}

//------------------------------------------------------------------------------------

void draw_system::prepareVertices( device& Device ) noexcept
{
    //
    // Initialize Draw data
    //
    {
        static const xarray<tools::vertex_desc::attribute,3> DrawVertexAttr
        {
            tools::vertex_desc::attribute{ offsetof( draw::vertex, m_Position ),    tools::vertex_desc::ATTR_SRC_F32x3,  tools::vertex_desc::ATTR_USAGE_POSITION           },
            tools::vertex_desc::attribute{ offsetof( draw::vertex, m_UV ),          tools::vertex_desc::ATTR_SRC_F32x2,  tools::vertex_desc::ATTR_USAGE_00_FULLRANGE_UV    },
            tools::vertex_desc::attribute{ offsetof( draw::vertex, m_Color ),       tools::vertex_desc::ATTR_SRC_U8x4_F, tools::vertex_desc::ATTR_USAGE_00_RGBA            }
        };

        buffer_double_paged::setup Setup;
        Setup.m_MaxIndexBuffers         = 12;
        Setup.m_MaxVertexBuffers        = 12;
        Setup.m_MaxVerticesPerBuffer    = 1024*50;
        Setup.m_MaxIndicesPerBuffer     = 1024*50;
        Setup.m_VertexEntrySize         = sizeof( draw::vertex );
        for( auto& PerThread : Device.m_PerThreadData ) 
        {
            PerThread.m_Draw.m_PagedVertIndexData.Initialize( Setup, DrawVertexAttr );
        }
    }

    //
    // Create the pipeline description
    //
    static const xarray< tools::vertex_desc::attribute_link, 3 > DrawVertexLinks
    {
        tools::vertex_desc::attribute_link{ 0, tools::vertex_desc::ATTR_USAGE_POSITION          },
        tools::vertex_desc::attribute_link{ 1, tools::vertex_desc::ATTR_USAGE_00_FULLRANGE_UV   },
        tools::vertex_desc::attribute_link{ 2, tools::vertex_desc::ATTR_USAGE_00_RGBA           }
    };

    m_VPipelineDescritors.setup( VERTEX_BUFFER_BIND_ID, Device.m_PerThreadData[0].m_Draw.m_PagedVertIndexData.getVertDesc(), DrawVertexLinks );
}

//------------------------------------------------------------------------------------

void draw_system::Initialize( VkRenderPass RenderPass, const VkPipelineCache pipelineCache ) noexcept
{
    // Connect the draw system to the device events
    m_DelegateJustAfterPageFlip.Connect( m_Device.getEvents().m_JustAfterPageFlip );

    prepareVertices           ( m_Device );
    setupDescriptorSetLayout  ( m_Device );
    preparePipelines          ( m_Device, RenderPass, pipelineCache );
    setupDescriptorPool       ( m_Device );

    xbitmap Bitmap;
    Bitmap.CreateBitmap(16, 16);
    Bitmap.getMip<xbyte>(0).MemSet( 0xffffffff );

    eng_texture::handle hTexture;
    m_Device.CreateTexture( hTexture, Bitmap );
    m_Device.CreateSampler( m_hWhiteTextureSampler, eng_sampler::setup{ hTexture } );
}

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------

static const xarray< tools::vertex_desc::attribute_link, 3 > s_DrawVertexLinks
{
    tools::vertex_desc::attribute_link{ 0, tools::vertex_desc::ATTR_USAGE_POSITION          },
    tools::vertex_desc::attribute_link{ 1, tools::vertex_desc::ATTR_USAGE_00_FULLRANGE_UV   },
    tools::vertex_desc::attribute_link{ 2, tools::vertex_desc::ATTR_USAGE_00_RGBA           }
};

//------------------------------------------------------------------------------------

void draw::FlushBufferedData( 
    const xmatrix4&     L2C, 
    const bool          bPolygons,
    const int           IndexOffset, 
    const int           IndexCount ) noexcept
{
    x_assume( m_bDrawBegin );

    auto        VKCmdBuff     = m_DisplayCmds.getVKCmdBuffer();
    auto&       TheDevice     = m_DisplayCmds.getTheDevice();
    auto&       DrawSystem    = TheDevice.getDrawSystem();
 
    // Bind descriptor sets describing shader binding points
    const xarray<VkDescriptorSet,1> DescriptorSets = 
    { 
        m_pSampler->getDrawDescriptorSet(0)
    };

    vkCmdBindDescriptorSets( 
        VKCmdBuff, 
        VK_PIPELINE_BIND_POINT_GRAPHICS, 
        DrawSystem.m_VkPipelineLayout, 
        0, 
        DescriptorSets.getCount<uint32_t>(), 
        DescriptorSets,
        0,
        nullptr );
         
    // Bind the rendering pipeline (including the shaders)
    if( IndexOffset == 0 )
    {
        auto& PipeLine = TheDevice.getRenderPipelineHash().getDrawPipeLine( 
            m_DisplayCmds.getWindow().getVKRenderPass(),
            m_DrawPipeLine, 
            bPolygons );
        vkCmdBindPipeline( VKCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, PipeLine.getVkPipeline() );
    }

    auto&   DoubleBuffer    = TheDevice.getDrawDoubleBuffer();
    auto    ActiveView      = m_DisplayCmds.getWindow().getActiveView();

    x_assert( m_IndexAlloc.m_pStart );
    x_assert( m_VertAlloc.m_pStart  );

    // do we need to set the Scissors
    if( m_Scissor.m_Left != m_Scissor.m_Right )
    {
        xarray<VkRect2D,1> Scissor;

        Scissor[0].extent.width  = m_Scissor.getWidth();
        Scissor[0].extent.height = m_Scissor.getHeight();
        Scissor[0].offset.x      = m_Scissor.m_Left;
        Scissor[0].offset.y      = m_Scissor.m_Top;

        vkCmdSetScissor( VKCmdBuff,
                         0,
                         Scissor.getCount<uint32_t>(),
                         Scissor );
    }

    // Submit via push constant (rather than a UBO)
    vkCmdPushConstants      ( 
        VKCmdBuff, 
        DrawSystem.m_VkPipelineLayout, 
        VK_SHADER_STAGE_VERTEX_BIT, 
        VK_FLAGS_NONE, 
        sizeof(xmatrix4), 
        &L2C );

    // Bind triangle vertices
    const xarray<VkDeviceSize,1>  Offsets          { 0u };
    const xarray<VkBuffer,1>      VertexBuffers    { DoubleBuffer.getVertexVKBuffer( m_VertAlloc.m_iBuffer ) };
    vkCmdBindVertexBuffers  ( 
        VKCmdBuff, 
        VERTEX_BUFFER_BIND_ID, 
        VertexBuffers.getCount<uint32_t>(), 
        VertexBuffers, 
        Offsets );

    // Bind triangle indices
    vkCmdBindIndexBuffer    ( 
        VKCmdBuff, 
        DoubleBuffer.getIndexVKBuffer( m_IndexAlloc.m_iBuffer ), 
        0, 
        VK_INDEX_TYPE_UINT32 );

    // Draw indexed triangles
    vkCmdDrawIndexed        ( 
        VKCmdBuff, 
        static_cast<uint32_t>( IndexCount < 0 ? m_IndexAlloc.m_Count : IndexCount ), 
        1, 
        m_IndexAlloc.m_Offset  + IndexOffset, 
        m_VertAlloc.m_Offset, 
        0 );

    //
    // Reset allocations
    //
    if( IndexCount < 0 || (IndexOffset + IndexCount) == m_IndexAlloc.m_Count )
    {
        m_IndexAlloc.m_pStart   = nullptr;
        m_VertAlloc.m_pStart    = nullptr;
    }
}

//------------------------------------------------------------------------------------

void draw::DrawBufferTriangles2( 
    const xmatrix4&                         L2C, 
    const eng_material_informed::handle&    hHandle,
    const eng_pipeline::handle&             hPipeline,
    const int                               IndexOffset, 
    const int                               IndexCount ) noexcept
{
    x_assume( m_bDrawBegin );

    auto    VKCmdBuff           = m_DisplayCmds.getVKCmdBuffer();
    auto&   TheDevice           = m_DisplayCmds.getTheDevice();
    auto&   DrawSystem          = TheDevice.getDrawSystem();
    auto&   MaterialInformed    = HandleToObject<material_informed>( hHandle );

    // Bind descriptor sets describing shader binding points
    const xarray<VkDescriptorSet,1> DescriptorSets = 
    { 
        MaterialInformed.getVKDescriptionSet() 
    };

    vkCmdBindDescriptorSets( 
        VKCmdBuff, 
        VK_PIPELINE_BIND_POINT_GRAPHICS, 
        DrawSystem.m_VkPipelineLayout, 
        0, 
        DescriptorSets.getCount<uint32_t>(), 
        DescriptorSets,
        0,
        nullptr );
         
    // Bind the rendering pipeline (including the shaders)
    if( IndexOffset == 0 )
    {
        auto VKPipeLine = MaterialInformed.getVKPipeLine( 
            m_DisplayCmds.getWindow().getVKRenderPass(),
            hPipeline,
            true );

        vkCmdBindPipeline( VKCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, VKPipeLine );
    }

    auto&   DoubleBuffer    = TheDevice.getDrawDoubleBuffer();
    auto    ActiveView      = m_DisplayCmds.getWindow().getActiveView();

    x_assert( m_IndexAlloc.m_pStart );
    x_assert( m_VertAlloc.m_pStart  );

    // do we need to set the Scissors
    if( m_Scissor.m_Left != m_Scissor.m_Right )
    {
        xarray<VkRect2D,1> Scissor;

        Scissor[0].extent.width  = m_Scissor.getWidth();
        Scissor[0].extent.height = m_Scissor.getHeight();
        Scissor[0].offset.x      = m_Scissor.m_Left;
        Scissor[0].offset.y      = m_Scissor.m_Top;

        vkCmdSetScissor( VKCmdBuff,
                         0,
                         Scissor.getCount<uint32_t>(),
                         Scissor );
    }

    // Submit via push constant (rather than a UBO)
    vkCmdPushConstants      ( 
        VKCmdBuff, 
        DrawSystem.m_VkPipelineLayout, 
        VK_SHADER_STAGE_VERTEX_BIT, 
        VK_FLAGS_NONE, 
        sizeof(xmatrix4), 
        &L2C );

    // Bind triangle vertices
    const xarray<VkDeviceSize,1>  Offsets          { 0u };
    const xarray<VkBuffer,1>      VertexBuffers    { DoubleBuffer.getVertexVKBuffer( m_VertAlloc.m_iBuffer ) };
    vkCmdBindVertexBuffers  ( 
        VKCmdBuff, 
        VERTEX_BUFFER_BIND_ID, 
        VertexBuffers.getCount<uint32_t>(), 
        VertexBuffers, 
        Offsets );

    // Bind triangle indices
    vkCmdBindIndexBuffer    ( 
        VKCmdBuff, 
        DoubleBuffer.getIndexVKBuffer( m_IndexAlloc.m_iBuffer ), 
        0, 
        VK_INDEX_TYPE_UINT32 );

    // Draw indexed triangles
    vkCmdDrawIndexed        ( 
        VKCmdBuff, 
        static_cast<uint32_t>( IndexCount < 0 ? m_IndexAlloc.m_Count : IndexCount ), 
        1, 
        m_IndexAlloc.m_Offset + IndexOffset, 
        m_VertAlloc.m_Offset, 
        0 );

    //
    // Reset allocations
    //
    if( IndexCount < 0 || (IndexOffset + IndexCount) == m_IndexAlloc.m_Count )
    {
        m_IndexAlloc.m_pStart   = nullptr;
        m_VertAlloc.m_pStart    = nullptr;
    }
}

//------------------------------------------------------------------------------------
void draw::Begin( const pipeline DrawPipeLine ) noexcept 
{
    x_assume( m_bDrawBegin==false );
    m_pSampler      = &HandleToObject<sampler>(m_DisplayCmds.getTheDevice().getDrawSystem().m_hWhiteTextureSampler);
    m_DrawPipeLine  = DrawPipeLine;
    m_bDrawBegin    = true;
}

//------------------------------------------------------------------------------------
void draw::End( void ) noexcept 
{
    x_assume( m_bDrawBegin );
    m_bDrawBegin    = false;
}

//------------------------------------------------------------------------------------
eng_draw::buffers draw::popBuffers( const int nVertices, const int nIndices ) noexcept
{
    // If this triggers seems that we are not giving a chance to reset these offsets
    x_assert( m_VertAlloc.m_pStart == nullptr );
    x_assert( m_IndexAlloc.m_pStart == nullptr );
    x_assert( nVertices > 0 );
    x_assert( nIndices > 0 );

    auto&   TheDevice       = m_DisplayCmds.getTheDevice();
    auto&   DoubleBuffer    = TheDevice.getDrawDoubleBuffer();
    
    m_VertAlloc         = DoubleBuffer.AllocVertex( nVertices );
    m_IndexAlloc        = DoubleBuffer.AllocIndex( nIndices );

    return
    {
        { (draw::vertex*)   m_VertAlloc.m_pStart,   m_VertAlloc.m_Count  },
        { (u32*)            m_IndexAlloc.m_pStart,  m_IndexAlloc.m_Count }
    };
}

//------------------------------------------------------------------------------------

void draw::DrawBufferLines( const xmatrix4& L2C ) noexcept
{
    FlushBufferedData( L2C, false, 0, -1 );
}

//------------------------------------------------------------------------------------

void  draw::DrawBufferTriangles( const xmatrix4& L2C, int IndexOffset, int IndexCount ) noexcept
{
    FlushBufferedData( L2C, true, IndexOffset, IndexCount );
}

//------------------------------------------------------------------------------------

void draw::setSampler( const eng_sampler::handle& hSampler ) noexcept
{
    m_pSampler = &HandleToObject<sampler>(hSampler);
}

//------------------------------------------------------------------------------------

void draw::setScissor( const xirect& Rect ) noexcept
{
    m_Scissor = Rect;
}

//------------------------------------------------------------------------------------

void draw::ClearSampler( void ) noexcept
{
    m_pSampler = &HandleToObject<sampler>(m_DisplayCmds.getTheDevice().getDrawSystem().m_hWhiteTextureSampler);
}

//------------------------------------------------------------------------------------
void draw::ClearScissor( void ) noexcept
{
   m_Scissor.m_Left = m_Scissor.m_Right = 0;
}


//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}
                                                                               