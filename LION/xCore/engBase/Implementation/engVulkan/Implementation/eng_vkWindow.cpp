//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

//---------------------------------------------------------------------------------

eng_device& window::getDevice( void ) noexcept 
{ 
    return m_SwapChain.getTheDevice(); 
}

//---------------------------------------------------------------------------------

window::err window::CreateDepthStencil( VkCommandBuffer VKSetupCmdBuffer, const int Width, const int Height ) noexcept
{                         
    auto&    Device                 = getTheDevice();
    auto     VKDevice               = Device.getVKDevice();

    VkImageCreateInfo Image {};
    Image.sType                     = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    Image.pNext                     = nullptr;
    Image.imageType                 = VK_IMAGE_TYPE_2D;
    Image.format                    = m_DepthStencil.m_VKFormat;
    Image.extent                    = { static_cast<uint32_t>(Width), static_cast<uint32_t>(Height), 1 };
    Image.mipLevels                 = 1;
    Image.arrayLayers               = 1;
    Image.samples                   = VK_SAMPLE_COUNT_1_BIT;
    Image.tiling                    = VK_IMAGE_TILING_OPTIMAL;
    Image.usage                     = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    Image.initialLayout             = VK_IMAGE_LAYOUT_UNDEFINED;
    Image.flags                     = 0;

    VkResult VKErr = vkCreateImage( 
        VKDevice, 
        &Image, 
        nullptr, 
        &m_DepthStencil.m_VKImage );

    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to create the zbuffer image" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    VkMemoryAllocateInfo MemAlloc   = {};
    MemAlloc.sType                  = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    MemAlloc.pNext                  = nullptr;
    MemAlloc.allocationSize         = 0;
    MemAlloc.memoryTypeIndex        = 0;

    VkMemoryRequirements MemReqs;
    vkGetImageMemoryRequirements( VKDevice, m_DepthStencil.m_VKImage, &MemReqs );
    MemAlloc.allocationSize = MemReqs.size;

    Device.getMemoryType( MemReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, MemAlloc.memoryTypeIndex );
    VKErr = vkAllocateMemory( VKDevice, &MemAlloc, nullptr, &m_DepthStencil.m_VKMem );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to Allocate memory for the zbuffer" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    VKErr = vkBindImageMemory( VKDevice, m_DepthStencil.m_VKImage, m_DepthStencil.m_VKMem, 0 );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to bind the depth stencil" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    tools::setImageLayout( 
        VKSetupCmdBuffer, 
        m_DepthStencil.m_VKImage, 
        VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT, 
        VK_IMAGE_LAYOUT_UNDEFINED, 
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL );

    VkImageViewCreateInfo DepthStencilView              = {};
    DepthStencilView.sType                              = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    DepthStencilView.pNext                              = nullptr;
    DepthStencilView.viewType                           = VK_IMAGE_VIEW_TYPE_2D;
    DepthStencilView.format                             = m_DepthStencil.m_VKFormat;
    DepthStencilView.image                              = m_DepthStencil.m_VKImage;
    DepthStencilView.components.r                       = VK_COMPONENT_SWIZZLE_R;
    DepthStencilView.components.g                       = VK_COMPONENT_SWIZZLE_G;
    DepthStencilView.components.b                       = VK_COMPONENT_SWIZZLE_B;
    DepthStencilView.components.a                       = VK_COMPONENT_SWIZZLE_A;
    DepthStencilView.flags                              = 0;

    DepthStencilView.subresourceRange                   = {};
    DepthStencilView.subresourceRange.levelCount        = 1;
    DepthStencilView.subresourceRange.baseMipLevel      = 0;
    DepthStencilView.subresourceRange.layerCount        = 1;
    DepthStencilView.subresourceRange.baseArrayLayer    = 0;
    DepthStencilView.subresourceRange.aspectMask        = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;

    VKErr = vkCreateImageView( 
        VKDevice, 
        &DepthStencilView, 
        nullptr, 
        &m_DepthStencil.m_VKImageView );
    
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to create the depth stencil image view" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    return x_error_ok();
}

//---------------------------------------------------------------------------------

window::err window::CreateFrameBuffer( const int Width, const int Height ) noexcept
{
    VkResult                    VKErr           {};
    xarray<VkImageView,2>       attachments     {};
    auto&                       Device          { getTheDevice() };
    auto                        VKDevice        { Device.getVKDevice() };

    // Depth/Stencil attachment is the same for all frame buffers
    attachments[1] = m_DepthStencil.m_VKImageView;

    VkFramebufferCreateInfo frameBufferCreateInfo   = {};
    frameBufferCreateInfo.sType                     = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    frameBufferCreateInfo.pNext                     = NULL;
    frameBufferCreateInfo.attachmentCount           = static_cast<uint32_t>(attachments.getCount());
    frameBufferCreateInfo.pAttachments              = attachments;
    frameBufferCreateInfo.width                     = Width;
    frameBufferCreateInfo.height                    = Height;
    frameBufferCreateInfo.layers                    = 1;
    frameBufferCreateInfo.renderPass                = m_VKRenderPass;

    // Create frame buffers for every swap chain image
    x_assert( m_SwapChain.getImageCount() == 2 );
    for( uint32_t i = 0; i < m_lVKFrameBuffers.getCount(); i++ )
    {
        attachments[0]  = m_SwapChain.getScreenBuffers(i).m_VKImageView;
        VKErr           = vkCreateFramebuffer( 
            VKDevice, 
            &frameBufferCreateInfo, 
            nullptr, 
            &m_lVKFrameBuffers[i] );

        if( VKErr )
        {
            static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to Create the Frame Buffer" ) ;
            std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
            return Error;
        }
    }

    return x_error_ok (); 
}

//---------------------------------------------------------------------------------

window::err window::FillMainCmdBufferWithEmptyRender( int Width, int Height ) noexcept
{
    auto&           Device              = getTheDevice();
    auto&           lVKMainCmdBuffer    = Device.getVKMainCmdBufferList();

    VkCommandBufferBeginInfo cmdBufInfo = {};
    cmdBufInfo.sType                                = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cmdBufInfo.pNext                                = nullptr;

    VkClearValue clearValues[2];
    clearValues[0].color                            = m_VKClearColorVal.color;   // color used to clear the background
    clearValues[1].depthStencil                     = m_VKClearDepthVal.depthStencil;

    VkRenderPassBeginInfo renderPassBeginInfo = {};
    renderPassBeginInfo.sType                       = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.pNext                       = nullptr;
    renderPassBeginInfo.renderPass                  = m_VKRenderPass;//Device.getVKRenderPass();
    renderPassBeginInfo.renderArea.offset.x         = 0;
    renderPassBeginInfo.renderArea.offset.y         = 0;
    renderPassBeginInfo.renderArea.extent.width     = Width;
    renderPassBeginInfo.renderArea.extent.height    = Height;
    renderPassBeginInfo.clearValueCount             = 2;
    renderPassBeginInfo.pClearValues                = clearValues;

    VkResult VKErr;

    xuptr i=0;
    for( auto& VKMainCmdBuffer : x_iter_ref( i, lVKMainCmdBuffer ) )
    {
        // Set target frame buffer
        renderPassBeginInfo.framebuffer = m_lVKFrameBuffers[i];

        VKErr = vkBeginCommandBuffer( VKMainCmdBuffer, &cmdBufInfo );
        if( VKErr )
        {
            static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to start adding commands to the default cmd list" );
            std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
            return Error;
        }

        vkCmdBeginRenderPass( VKMainCmdBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE );
        vkCmdEndRenderPass( VKMainCmdBuffer );

        VKErr = vkEndCommandBuffer( VKMainCmdBuffer );
        assert( !VKErr );
    }

    return x_error_ok();
}

//---------------------------------------------------------------------------------

window::err window::setupRenderPass( void ) noexcept
{
    VkResult    VKErr;

    xarray<VkAttachmentDescription,2> Attachments {};
    Attachments[0].format                   = m_SwapChain.getVKColorFormat();
    Attachments[0].samples                  = VK_SAMPLE_COUNT_1_BIT;
    Attachments[0].loadOp                   = VK_ATTACHMENT_LOAD_OP_CLEAR;
    Attachments[0].storeOp                  = VK_ATTACHMENT_STORE_OP_STORE;
    Attachments[0].stencilLoadOp            = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    Attachments[0].stencilStoreOp           = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    Attachments[0].initialLayout            = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    Attachments[0].finalLayout              = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    Attachments[0].flags                    = 0;

    Attachments[1].format                   = m_DepthStencil.m_VKFormat;
    Attachments[1].samples                  = VK_SAMPLE_COUNT_1_BIT;
    Attachments[1].loadOp                   = VK_ATTACHMENT_LOAD_OP_CLEAR;
    Attachments[1].storeOp                  = VK_ATTACHMENT_STORE_OP_STORE;
    Attachments[1].stencilLoadOp            = VK_ATTACHMENT_LOAD_OP_CLEAR;
    Attachments[1].stencilStoreOp           = VK_ATTACHMENT_STORE_OP_STORE;
    Attachments[1].initialLayout            = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    Attachments[1].finalLayout              = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    Attachments[1].flags                    = 0;

    xarray<VkAttachmentReference,1> ColorReference {};
    ColorReference[0].attachment            = 0;
    ColorReference[0].layout                = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    xarray<VkAttachmentReference,1> DepthReference {};
    DepthReference[0].attachment            = 1;
    DepthReference[0].layout                = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    xarray<VkSubpassDescription,1> Subpass  {};
    Subpass[0].pipelineBindPoint            = VK_PIPELINE_BIND_POINT_GRAPHICS;
    Subpass[0].flags                        = 0;
    Subpass[0].inputAttachmentCount         = 0;
    Subpass[0].pInputAttachments            = nullptr;
    Subpass[0].colorAttachmentCount         = static_cast<uint32_t>(ColorReference.getCount());
    Subpass[0].pColorAttachments            = ColorReference;
    Subpass[0].pDepthStencilAttachment      = DepthReference;
    Subpass[0].preserveAttachmentCount      = 0;
    Subpass[0].pPreserveAttachments         = nullptr;
    Subpass[0].pResolveAttachments          = nullptr;

    VkRenderPassCreateInfo RenderPassInfo   = {};
    RenderPassInfo.sType                    = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    RenderPassInfo.pNext                    = nullptr;
    RenderPassInfo.attachmentCount          = static_cast<uint32_t>(Attachments.getCount());
    RenderPassInfo.pAttachments             = Attachments;
    RenderPassInfo.subpassCount             = static_cast<uint32_t>(Subpass.getCount());
    RenderPassInfo.pSubpasses               = Subpass;
    RenderPassInfo.dependencyCount          = 0;
    RenderPassInfo.pDependencies            = nullptr;

    VKErr = vkCreateRenderPass( 
        getTheDevice().getVKDevice(), 
        &RenderPassInfo, 
        nullptr, 
        &m_VKRenderPass );
    
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to Create render pass" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }
    
    return x_error_ok(); 
}

//------------------------------------------------------------------------------------

window::err window::Initialize( const setup& Setup ) noexcept
{
    err Error;

    //
    // Create the windows
    //
    Error = CreateSytemWindow( Setup, this );                         
    if( Error ) 
        return Error;

    //
    // Initialize the swap-chain
    //    
    if( m_SwapChain.Initialize( Setup, *this ).isError() )
        return x_error_code( errors, ERR_FAILURE, "Fail to initialize the swapchain" );

    //
    // Prepare the driver base on the window attributes
    //
    auto&               Device = getTheDevice();
    Device.PrepareWindowAttachment( m_SwapChain.getQueueNodeIndex() );

    //
    // Create frame buffers from the swap chain now that we have the driver ready
    //
    m_Width          = Setup.m_Width;
    m_Height         = Setup.m_Height;
    VkCommandBuffer     VKSetupCmdBuffer    = Device.getVKSetupCmdBuffer();
    auto SwapError = m_SwapChain.Create( VKSetupCmdBuffer, m_Width, m_Height );
    if( SwapError ) return x_error_code( errors, ERR_FAILURE, SwapError.getString() );

    //
    // Get Format for the depth format
    //
    {
        auto Error = Device.getSupportedDepthFormat( m_DepthStencil.m_VKFormat );
        if( Error ) return x_error_code( errors, ERR_FAILURE, Error.getString() );
    }

    //
    // Create frame buffers 
    //
    Error = setupRenderPass();
    if( Error ) return Error;

    Error = CreateDepthStencil( VKSetupCmdBuffer, m_Width, m_Height );
    if( Error ) return Error;

    Error = CreateFrameBuffer( m_Width, m_Height );
    if( Error ) return Error;

    auto DeviceError = Device.FlushVKSetupCommandBuffer();
    if( DeviceError ) return x_error_code( errors, ERR_FAILURE, DeviceError.getString() );

    Error = FillMainCmdBufferWithEmptyRender( m_Width, m_Height );
    if( Error ) return Error;

    vkDeviceWaitIdle( Device.getVKDevice() );

    //
    // Initializing draw system
    //
    Device.InitializeDraw( m_VKRenderPass );


    VkFenceCreateInfo fenceCreateInfo = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
    fenceCreateInfo.pNext = nullptr;
    fenceCreateInfo.flags = VK_FLAGS_NONE;

    vkCreateFence( Device.getVKDevice(), &fenceCreateInfo, NULL, &m_RenderFence );

    return x_error_ok( );
}

//------------------------------------------------------------------------------------

void window::BeginRender( const eng_view& View, const bool bUpdateViewPort ) noexcept
{
    auto&       Device = getTheDevice();
    const auto  Width  = m_Width;
    const auto  Height = m_Height;

    vkDeviceWaitIdle( Device.getVKDevice() );

    VkCommandBufferBeginInfo cmdBufInfo = {};
    cmdBufInfo.sType                                = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cmdBufInfo.pNext                                = nullptr;

    VkResult    VKErr;
    auto        MainCmdBuffer = Device.getVKMainCmdBuffer();

    VKErr = vkBeginCommandBuffer( MainCmdBuffer, &cmdBufInfo ); 
    assert( !VKErr );

    xarray<VkClearValue,2> clearValues;
    clearValues[0].color                            = { { 1.00f, 0.25f, 0.25f, 1.0f } };   // color used to clear the background
    clearValues[1].depthStencil                     = { 1.0f, 0 };

    VkRenderPassBeginInfo renderPassBeginInfo = {};
    renderPassBeginInfo.sType                       = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.pNext                       = nullptr;
    renderPassBeginInfo.framebuffer                 = m_lVKFrameBuffers[ Device.getFrameIndex() ];
    renderPassBeginInfo.renderPass                  = m_VKRenderPass;
    renderPassBeginInfo.renderArea.offset.x         = 0;
    renderPassBeginInfo.renderArea.offset.y         = 0;
    renderPassBeginInfo.renderArea.extent.width     = Width;
    renderPassBeginInfo.renderArea.extent.height    = Height;
    renderPassBeginInfo.clearValueCount             = 2;
    renderPassBeginInfo.pClearValues                = clearValues;

    // Update dynamic viewport state
    m_Viewport[0].width                              = (float) Width;
    m_Viewport[0].height                             = (float) Height;
    m_Viewport[0].minDepth                           = (float) 0.0f;
    m_Viewport[0].maxDepth                           = (float) 1.0f;
    vkCmdSetViewport( MainCmdBuffer, 0, m_nViewports, &m_Viewport[0] ); 

    // Update dynamic scissor state
    m_Scissor[0].extent.width                        = Width;
    m_Scissor[0].extent.height                       = Height;
    m_Scissor[0].offset.x                            = 0;
    m_Scissor[0].offset.y                            = 0;
    vkCmdSetScissor( MainCmdBuffer, 0, m_nScissors, &m_Scissor[0] );

    //
    // Beging the render pass
    //
    vkCmdBeginRenderPass( MainCmdBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS );

    //
    // Setup the view
    //
    setActiveView(View,bUpdateViewPort);
}

//------------------------------------------------------------------------------------

void window::EndRender( void ) noexcept
{
    auto&               Device        = getTheDevice();
    auto                MainCmdBuffer = Device.getVKMainCmdBuffer();
    const uint32_t      FrameIndex    = Device.getFrameIndex();
    auto                MainVKQueue   = Device.getMainQueue();
    VkResult            VKErr;

    //
    // Notify the device that we are going to be done with the rendering
    //
    Device.onEndRender();

    //
    // Finish the render pass
    //
    vkCmdEndRenderPass( MainCmdBuffer );

    // Add a present memory barrier to the end of the command buffer
    // This will transform the frame buffer color attachment to a
    // new layout for presenting it to the windowing system integration 
    VkImageMemoryBarrier prePresentBarrier = {};
    prePresentBarrier.sType                     = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    prePresentBarrier.pNext                     = nullptr;
    prePresentBarrier.srcAccessMask             = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    prePresentBarrier.dstAccessMask             = 0;
    prePresentBarrier.oldLayout                 = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    prePresentBarrier.newLayout                 = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    prePresentBarrier.srcQueueFamilyIndex       = VK_QUEUE_FAMILY_IGNORED;
    prePresentBarrier.dstQueueFamilyIndex       = VK_QUEUE_FAMILY_IGNORED;
    prePresentBarrier.subresourceRange          = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
    prePresentBarrier.image                     = m_SwapChain.getScreenBuffers( FrameIndex ).m_VKImage;

    VkImageMemoryBarrier *pMemoryBarrier = &prePresentBarrier;
    vkCmdPipelineBarrier(
        MainCmdBuffer,  
        VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 
        VK_FLAGS_NONE,
        0, nullptr,
        0, nullptr,
        1, &prePresentBarrier);

    //
    // Finish the command list
    //
    VKErr = vkEndCommandBuffer( MainCmdBuffer );
    assert( !VKErr );

    //
    // PAGE FLIP
    //
    xarray<VkSemaphore,1>   presentCompleteSemaphore;
    VkSemaphoreCreateInfo   presentCompleteSemaphoreCreateInfo  = {};
    presentCompleteSemaphoreCreateInfo.sType                    = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    presentCompleteSemaphoreCreateInfo.pNext                    = nullptr;
    presentCompleteSemaphoreCreateInfo.flags                    = 0;//VK_FENCE_CREATE_SIGNALED_BIT;

    VKErr = vkCreateSemaphore( Device.getVKDevice(), &presentCompleteSemaphoreCreateInfo, nullptr, &presentCompleteSemaphore[0] );
    assert( !VKErr );

    // Get next image in the swap chain (back/front buffer)
    VKErr = m_SwapChain.AcquireNextImage( presentCompleteSemaphore[0], 0 );//FrameIndex ); 
    assert(!VKErr);

    // The submit infor strcuture contains a list of
    // command buffers and semaphores to be submitted to a queue
    // If you want to submit multiple command buffers, pass an array
    {
        static const VkPipelineStageFlags   waitStageFlags[2] = { VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT };
        xarray<VkSubmitInfo,1>              submitInfo = {};
        submitInfo[0].sType                            = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo[0].commandBufferCount               = 1;
        submitInfo[0].pCommandBuffers                  = &MainCmdBuffer;
        submitInfo[0].waitSemaphoreCount               = static_cast<uint32_t>(presentCompleteSemaphore.getCount());
        submitInfo[0].pWaitSemaphores                  = &presentCompleteSemaphore[0];
        submitInfo[0].pWaitDstStageMask                = nullptr;
        submitInfo[0].signalSemaphoreCount             = 0;
        submitInfo[0].pSignalSemaphores                = nullptr;
        submitInfo[0].pWaitDstStageMask                = waitStageFlags;

        // Submit to the graphics queue
        VKErr = vkQueueSubmit(  MainVKQueue, 
                                static_cast<uint32_t>(submitInfo.getCount()), 
                                &submitInfo[0], 
                                m_RenderFence );
        assert( !VKErr );
    }

	VkResult fenceRes;
	do
	{
		fenceRes = vkWaitForFences(Device.getVKDevice(), 1, &m_RenderFence, VK_TRUE, 100000000);
	} while (fenceRes == VK_TIMEOUT);
    vkResetFences(Device.getVKDevice(), 1, &m_RenderFence);


    // Present the current buffer to the swap chain
    // This will display the image
    VKErr = m_SwapChain.QueuePresent( MainVKQueue, FrameIndex );
    assert( !VKErr );


    // Add a post present image memory barrier
    // This will transform the frame buffer color attachment back
    // to it's initial layout after it has been presented to the
    // windowing system
    // See buildCommandBuffers for the pre present barrier that 
    // does the opposite transformation 
    VkImageMemoryBarrier postPresentBarrier = {};
    postPresentBarrier.sType                = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    postPresentBarrier.pNext                = NULL;
    postPresentBarrier.srcAccessMask        = 0;
    postPresentBarrier.dstAccessMask        = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    postPresentBarrier.oldLayout            = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    postPresentBarrier.newLayout            = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    postPresentBarrier.srcQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
    postPresentBarrier.dstQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
    postPresentBarrier.subresourceRange     = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
    postPresentBarrier.image                = m_SwapChain.getScreenBuffers( FrameIndex ).m_VKImage;

    // Use dedicated command buffer from example base class for submitting the post present barrier
    VkCommandBufferBeginInfo cmdBufInfo = {};
    cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cmdBufInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

    auto PostPresentVKCmdBuffer = Device.getVKPostPresentCmdBuffer();

    VKErr = vkBeginCommandBuffer( PostPresentVKCmdBuffer, &cmdBufInfo );
    assert( !VKErr );

    // Put post present barrier into command buffer
    vkCmdPipelineBarrier(
        PostPresentVKCmdBuffer,
        VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_FLAGS_NONE,
        0, nullptr,
        0, nullptr,
        1, &postPresentBarrier );

    VKErr = vkEndCommandBuffer( PostPresentVKCmdBuffer );
    assert( !VKErr );

    // Submit to the queue
    {
        xarray<VkSubmitInfo,1> submitInfo = {};
        submitInfo[0].sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo[0].waitSemaphoreCount   = 0;
        submitInfo[0].pWaitSemaphores      = nullptr;
        submitInfo[0].pWaitDstStageMask    = nullptr;
        submitInfo[0].commandBufferCount   = 1;
        submitInfo[0].pCommandBuffers      = &PostPresentVKCmdBuffer;

        VKErr = vkQueueSubmit(  MainVKQueue, 
                                static_cast<uint32_t>(submitInfo.getCount()), 
                                &submitInfo[0], 
                                VK_NULL_HANDLE );
        assert( !VKErr );
    }
            
    VKErr = vkQueueWaitIdle( MainVKQueue );
    assert( !VKErr );

    vkDestroySemaphore( Device.getVKDevice(), presentCompleteSemaphore[0], nullptr );

    //
    // Advance the frame for all subsystems
    //
    Device.onPageFlip();

    //
    // Print FPS
    //
    Sleep(16);
    static WCHAR Buffer[256];
    static f64 CPU = 0; 
    static f64 GPU = 0; 
    static f64 FPS = 0; 
    if( getTheDevice().getFrameNumber()%100 == 0  ) 
    {
        CPU = m_CPUStats.getAvg();
        GPU = m_GPUStats.getAvg();
        FPS = m_GPUStats.getFPS();
    }
    swprintf( Buffer, x_countof(Buffer), TEXT("CPU:%5.1f  Pageflip:%5.1f  FPS:%6.1f"), CPU, GPU, FPS );
    SetWindowText( m_hWindow, Buffer ); 
}

//------------------------------------------------------------------------------------

bool window::HandleEvents( void ) noexcept
{
    bool bContinue = true;

    MSG Message;

    UpdateInput();

    if ( PeekMessage(&Message, m_hWindow, 0, 0, PM_REMOVE) )
    {
        if ( WM_QUIT == Message.message )
        {
            bContinue = false;
        }
        DispatchMessage(&Message);
    }

    return bContinue;
}

//---------------------------------------------------------------------------------

window::err window::FillMainCmdWithDefaults( int Width, int Height ) noexcept
{
    VkCommandBufferBeginInfo cmdBufInfo = {};
    cmdBufInfo.sType                                = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cmdBufInfo.pNext                                = nullptr;

    VkClearValue clearValues[2];
    clearValues[0].color                            = m_VKClearColorVal.color;   // color used to clear the background
    clearValues[1].depthStencil                     = m_VKClearDepthVal.depthStencil;

    VkRenderPassBeginInfo renderPassBeginInfo = {};
    renderPassBeginInfo.sType                       = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.pNext                       = nullptr;
    renderPassBeginInfo.renderPass                  = m_VKRenderPass;
    renderPassBeginInfo.renderArea.offset.x         = 0;
    renderPassBeginInfo.renderArea.offset.y         = 0;
    renderPassBeginInfo.renderArea.extent.width     = Width;
    renderPassBeginInfo.renderArea.extent.height    = Height;
    renderPassBeginInfo.clearValueCount             = 2;
    renderPassBeginInfo.pClearValues                = clearValues;

    VkResult VKErr;

    auto& lMainCmdBuffer = getTheDevice().getVKMainCmdBufferList();
    for( int32_t i = 0; i < lMainCmdBuffer.getCount(); ++i )
    {
        // Set target frame buffer
        renderPassBeginInfo.framebuffer = m_lVKFrameBuffers[i];

        VKErr = vkBeginCommandBuffer( lMainCmdBuffer[i], &cmdBufInfo );
        if( VKErr )
        {
            static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to start adding commands to the default cmd list" );
            std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
            return Error;
        }

        vkCmdBeginRenderPass( lMainCmdBuffer[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE );

        VKErr = vkEndCommandBuffer( lMainCmdBuffer[i] );
        assert( !VKErr );
    }

    return x_error_ok();
}

//------------------------------------------------------------------------------------

void window::Resize( const int Width, const int Height ) noexcept
{
    auto&       Device        = getTheDevice();

    vkDestroyImageView  ( Device.getVKDevice(), m_DepthStencil.m_VKImageView,  nullptr );
    vkDestroyImage      ( Device.getVKDevice(), m_DepthStencil.m_VKImage,      nullptr );
    vkFreeMemory        ( Device.getVKDevice(), m_DepthStencil.m_VKMem,        nullptr );

    for( int i = 0; i < m_lVKFrameBuffers.getCount(); i++ ) 
    {
        vkDestroyFramebuffer( Device.getVKDevice(), m_lVKFrameBuffers[i], nullptr );
    }

    m_SwapChain.Cleanup();

   auto Err0 = Device.CreateVKSetupCommandBuffer();
    x_assert(!Err0);

    eng_window::setup Setup( Device );
    m_Width  = Setup.m_Width   = Width;
    m_Height = Setup.m_Height  = Height;
    auto Errn1 = m_SwapChain.Initialize( Setup, *this );

    auto Err1 = m_SwapChain.Create( Device.getVKSetupCmdBuffer(), m_Width, m_Height );
    x_assert(!Err1);

    auto Err2 = CreateDepthStencil( Device.getVKSetupCmdBuffer(), m_Width, m_Height );
    x_assert(!Err2);

    auto Err3 = CreateFrameBuffer( m_Width, m_Height );
    x_assert(!Err3);

    auto Err4 = Device.FlushVKSetupCommandBuffer();
    x_assert(!Err4);

    // DefaultDrawCommands( Width, Height );
    // FillMainCmdWithDefaults( Width, Height );
    // TODO: we need to recreate all the cmd buffers

    vkQueueWaitIdle ( Device.getMainQueue() );
    vkDeviceWaitIdle( Device.getVKDevice()  );

}

//-------------------------------------------------------------------------------

void window::PageFlip( void ) noexcept
{
    m_GPUStats.Update();
    m_CPUStats.Update();
}

//-------------------------------------------------------------------------------

eng_display_cmds& window::getDisplayCmdList( const f32 Order ) noexcept
{
    auto& Device    = getTheDevice();
    auto& DCmdList  = Device.getDisplayCmdList( Order, this );

    //
    // Make sure that it has started
    //
    if( DCmdList.isStarted() == false )
    {
        DCmdList.setStarted( true );
        auto VKCmdList = DCmdList.getVKCmdBuffer();

        // Inheritance infor for secondary command buffers
        VkCommandBufferInheritanceInfo inheritanceInfo { VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO };
        inheritanceInfo.framebuffer = m_lVKFrameBuffers[Device.getFrameIndex()];
        inheritanceInfo.renderPass  = m_VKRenderPass;

        VkCommandBufferBeginInfo cmdBufInfo { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
        cmdBufInfo.flags            =   VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT    |
		                                VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT         |
		                                VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        cmdBufInfo.pInheritanceInfo = &inheritanceInfo;
        vkBeginCommandBuffer( VKCmdList, &cmdBufInfo );

        // Update dynamic viewport state
        vkCmdSetViewport( VKCmdList, 0, static_cast<uint32_t>(m_nViewports),   &m_Viewport[0] );

        // Update dynamic scissor state
        vkCmdSetScissor ( VKCmdList, 0, static_cast<uint32_t>(m_nScissors),    &m_Scissor[0]  );
    }

    return DCmdList;
}

//-------------------------------------------------------------------------------

void window::SubmitDisplayCmdList( eng_display_cmds& CmdList ) noexcept
{
    auto& Device    = getTheDevice();
    Device.SubmitDisplayCmdList( reinterpret_cast<display_cmds&>(CmdList) );
}

//-------------------------------------------------------------------------------

void window::setActiveView( const eng_view& View, const bool bUpdateViewPort ) noexcept 
{ 
    m_View = View; 
    if( bUpdateViewPort )
    {
        m_View.setViewport( xirect{ 
            static_cast<int>(m_Viewport[0].x), 
            static_cast<int>(m_Viewport[0].y), 
            static_cast<int>(m_Viewport[0].width), 
            static_cast<int>(m_Viewport[0].height) } );
    }

    // Make sure that all matrices are cashed
    m_View.UpdateAllMatrices();
}


//-------------------------------------------------------------------------------

inline 
void window::stats::Update( void ) noexcept
{
    m_FrameTime[ m_Index ] = m_Timer.TripMilliseconds().count();
    m_Index               += 1;
    m_Index               %= m_FrameTime.getCount();
}

//-------------------------------------------------------------------------------

inline 
f64 window::stats::getAvg( void ) const noexcept
{
    xtimer::ticks Sum = 0;
    for( auto F : m_FrameTime ) Sum += F;
    return static_cast<f64>( m_FrameTime.getCount() ) / Sum; 
}

//-------------------------------------------------------------------------------

inline 
f64 window::stats::getLast( void ) const noexcept
{
    const auto Index = m_Index - 1; 
    if( Index < 0 ) return static_cast<f64>( m_FrameTime[ m_FrameTime.getCount() - 1 ] );
    return static_cast<f64>( m_FrameTime[ Index ] );
}

//-------------------------------------------------------------------------------

inline 
f64 window::stats::getFPS( void ) const noexcept
{
    return getAvg() * 1000.0;
}

//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}






