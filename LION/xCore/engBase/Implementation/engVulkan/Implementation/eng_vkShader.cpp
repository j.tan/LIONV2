//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
// FUNCTIONAL
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Load a binary file into a buffer (e.g. SPIR-V)
x_errdef::err readBinaryFile( xafptr<xbyte>& Data, const char* filename ) noexcept
{
//    FILE*           fp = fopen(filename, "rb");
    FILE*           fp;
    errno_t         tr = fopen_s( &fp, filename, "rb");
        
    if (!fp) 
        return x_error_code( x_errdef::errors, ERR_FAILURE, "Fail to open file" );

    fseek(fp, 0L, SEEK_END);
    int size = ftell(fp);

    fseek(fp, 0L, SEEK_SET);

    Data.Alloc( size );
    size_t retval = fread ( &Data[0], Data.getCount(), 1, fp );
    if( retval != 1 ) 
        return x_error_code( x_errdef::errors, ERR_FAILURE, "Fail to read contents of open file" );

    return x_error_ok();
}

//-----------------------------------------------------------------------
// Shader
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------

shader_common::err shader_common::LoadVertexShader( const char* fileName, const char* pShaderEntryPoint ) noexcept
{ 
    return LoadShader( fileName, VK_SHADER_STAGE_VERTEX_BIT ,pShaderEntryPoint ); 
}

//-----------------------------------------------------------------------

shader_common::err shader_common::LoadFragmentShader( const char* fileName, const char* pShaderEntryPoint ) noexcept
{ 
    return LoadShader( fileName, VK_SHADER_STAGE_FRAGMENT_BIT ,pShaderEntryPoint ); 
}

//-----------------------------------------------------------------------

shader_common::err shader_common::LoadShader( const char* fileName, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    const auto L = strlen( fileName );

    //
    // Use the right loader
    //
    for( auto i = L - 1; i != 0; --i )
    {
        if( fileName[i] == '.' )
        {
            if( fileName[i + 1] == 'G' || fileName[i + 1] == 'g' ||     //  .glsl
                fileName[i + 1] == 'F' || fileName[i + 1] == 'f' ||     //  .frag
                fileName[i + 1] == 'V' || fileName[i + 1] == 'v' )      //  .vert
                return LoadGLSL( fileName, stage, pShaderEntryPoint );

            if( fileName[i + 1] == 'S' || fileName[i + 1] == 's' )
                return LoadSVP( fileName, stage, pShaderEntryPoint );
        }
    }

    //
    // Todo: Add auto detect for vertex/fragment shaders base on file extensions
    //

    //
    // Fail
    //
    static const err Error = x_error_code( errors, ERR_FILE, "Unknown file extension" );
    ENG_ERRORLOG( m_Device.getInstance(), 
                    " ("                << 
                    fileName            <<
                    ") : "              << 
                    Error.getString() );

    return Error;
}

//-----------------------------------------------------------------------

shader_common::err shader_common::LoadGLSL( const char* fileName, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    //
    // Read binary file
    //
    xafptr<xbyte>   ShaderData;
    {
        x_errdef::err Err = readBinaryFile( ShaderData, fileName );
        if( Err || ShaderData.getCount() < 5 )
        {
            static const err Error = x_error_code( errors, ERR_FILE, "Fail to read a SVP shader file" );
            ENG_ERRORLOG( m_Device.getInstance(), 
                            Error.getString()   << 
                            " ("                << 
                            fileName            <<
                            ") : "              << 
                            Err.getString() );
        }
    }

    //
    // Handle the shader creation
    //
    {
        err Error = CreateShaderGLSL( ShaderData, stage, pShaderEntryPoint );
        {
            ENG_ERRORLOG( m_Device.getInstance(), " Additional Context: " << fileName  );
            return Error;
        }
    }

    return x_error_ok();
}

//-----------------------------------------------------------------------

shader_common::err shader_common::CreateShaderGLSL( const xbuffer_view<xbyte> Data, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    //
    // Hack for Vulkan to support for GLSL
    //
    xafptr<xbyte>       NewData;
    {
        static const int    VKGLSLHeaderSize = sizeof(uint32_t) * 3;

        NewData.Alloc( Data.getByteSize() + VKGLSLHeaderSize + 1 );

        // Magic SPV number
        reinterpret_cast<uint32_t*>(&NewData[0])[0] = 0x07230203; 
        reinterpret_cast<uint32_t*>(&NewData[0])[1] = 0;
        reinterpret_cast<uint32_t*>(&NewData[0])[2] = static_cast<uint32_t>(stage);

        NewData.CopyToFrom( Data, VKGLSLHeaderSize );

        // Make sure we terminate with zero
        NewData[NewData.getCount()-1] = 0;
    }

    //
    // Handle the shader creation
    //
    return CreateShader( NewData, stage, pShaderEntryPoint );
}

//-----------------------------------------------------------------------

shader_common::err shader_common::LoadSVP( const char* fileName, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    //
    // Read binary file
    //
    xafptr<xbyte>   ShaderData;
    {
        x_errdef::err Err = readBinaryFile( ShaderData, fileName );
        if( Err || ShaderData.getCount() < 5 )
        {
            static const err Error = x_error_code( errors, ERR_FILE, "Fail to read a SVP shader file" );
            ENG_ERRORLOG( m_Device.getInstance(), 
                            Error.getString()   << 
                            " ("                << 
                            fileName            <<
                            ") : "              << 
                            Err.getString() );
            return Error;
        }
    }

    //
    // Handle the shader creation
    //
    {
        err Error = CreateShader( ShaderData, stage, pShaderEntryPoint );
        if( Error )
        {
            ENG_ERRORLOG( m_Device.getInstance(), " Additional Context: " << fileName  );
            return Error;
        }
    }

    return x_error_ok();
}

//-----------------------------------------------------------------------

shader_common::err shader_common::CreateShader( const xbuffer_view<xbyte> Data, VkShaderStageFlagBits stage, const char* pShaderEntryPoint ) noexcept
{
    //
    // Create shader module
    //
    {
        VkShaderModuleCreateInfo  moduleCreateInfo {};

        moduleCreateInfo.sType      = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        moduleCreateInfo.pNext      = nullptr;                         

        moduleCreateInfo.codeSize   = Data.getByteSize();
        moduleCreateInfo.pCode      = reinterpret_cast<const uint32_t*>(&Data[0]);
        moduleCreateInfo.flags      = 0;
        VkResult VKErr = vkCreateShaderModule( m_Device.getVKDevice(), &moduleCreateInfo, nullptr, &m_VkShaderStageCreateInfo.module );
        if( VKErr )
        {
            static const err Error = x_error_code( errors, ERR_VKFAILURE, "Fail to create shader module" );
            ENG_ERRORLOG( m_Device.getInstance(), 
                            Error.getString()   << 
                            " : "               << 
                            debug::VulkanErrorString( VKErr ) );
            return Error;
        }
    }

    //
    // Initialize shader creation info
    //
    m_VkShaderStageCreateInfo.sType   = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    m_VkShaderStageCreateInfo.stage   = stage;
    m_VkShaderStageCreateInfo.pName   = pShaderEntryPoint;

    return x_error_ok();
}

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
// VERTEX SHADER
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------

void shader_vert::Initialize( const setup& Setup ) noexcept
{
    if( Setup.m_MemoryBufferIs == shader_frag::setup::memory_buffer::IS_SHADER_DATA )
    {
        CreateShader( Setup.m_Memory, VK_SHADER_STAGE_VERTEX_BIT );
    }
    else
    {
        CreateShader( Setup.m_Memory, VK_SHADER_STAGE_VERTEX_BIT );
    }
}

//------------------------------------------------------------------------------------
void shader_vert::Release( void ) noexcept
{

}


//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
// FRAGMENT SHADER
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------

void shader_frag::Initialize( const setup& Setup ) noexcept
{
    if( Setup.m_MemoryBufferIs == shader_frag::setup::memory_buffer::IS_SHADER_DATA )
    {
        CreateShader( Setup.m_Memory, VK_SHADER_STAGE_FRAGMENT_BIT );
    }
    else
    {
        CreateShader( Setup.m_Memory, VK_SHADER_STAGE_FRAGMENT_BIT );
    }
}

//------------------------------------------------------------------------------------
void shader_frag::Release( void ) noexcept
{
}

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
// GEOMETRY SHADER
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------

void shader_geom::Initialize( const setup& Setup ) noexcept
{
    if( Setup.m_MemoryBufferIs == shader_frag::setup::memory_buffer::IS_SHADER_DATA )
    {
        CreateShader( Setup.m_Memory, VK_SHADER_STAGE_GEOMETRY_BIT );
    }
    else
    {
        CreateShader( Setup.m_Memory, VK_SHADER_STAGE_GEOMETRY_BIT );
    }
}

//------------------------------------------------------------------------------------
void shader_geom::Release( void ) noexcept
{

}

//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}


