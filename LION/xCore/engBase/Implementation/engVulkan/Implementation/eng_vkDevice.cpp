//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "../eng_vkBase.h"


//------------------------------------------------------------------------------------
// NAMESPACE
//------------------------------------------------------------------------------------
namespace eng_vk{

#define VERTEX_BUFFER_BIND_ID   0
#define TEXTURE_BIND_ID         0


//------------------------------------------------------------------------------------

VkResult device::CreateDevice( const VkDeviceQueueCreateInfo& queueCreateInfo, const bool enableValidation ) noexcept
{
    VkDeviceCreateInfo                      deviceCreateInfo    {};
    xvector<const char*>                    ValidationLayers    {};
    x_constexprvar xarray<const char*,2>    enabledExtensions    
    { 
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        VK_NV_GLSL_SHADER_EXTENSION_NAME
    };

    VkPhysicalDeviceFeatures                Features;
    vkGetPhysicalDeviceFeatures( m_VKPhysicalDevice, &Features );
    Features.shaderClipDistance = true;
    Features.shaderCullDistance = true;

    deviceCreateInfo.sType                          = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pNext                          = nullptr;
    deviceCreateInfo.queueCreateInfoCount           = 1;
    deviceCreateInfo.pQueueCreateInfos              = &queueCreateInfo;
    deviceCreateInfo.pEnabledFeatures               = &Features;
    deviceCreateInfo.enabledExtensionCount          = static_cast<uint32_t>(enabledExtensions.getCount());
    deviceCreateInfo.ppEnabledExtensionNames        = enabledExtensions;

    if( enableValidation )
    {
        debug::getValidationLayers( m_Instance, ValidationLayers );
        deviceCreateInfo.enabledLayerCount          = static_cast<uint32_t>(ValidationLayers.getCount());
        deviceCreateInfo.ppEnabledLayerNames        = ValidationLayers;
    }
    else
    {
        deviceCreateInfo.enabledLayerCount          = 0;
        deviceCreateInfo.ppEnabledLayerNames        = nullptr;
    }

    auto VKErr = vkCreateDevice( 
        m_VKPhysicalDevice, 
        &deviceCreateInfo, 
        nullptr, 
        &m_VKDevice );

    return VKErr;
}

//---------------------------------------------------------------------------------

device::err device::getSupportedDepthFormat( VkFormat& DepthFormat ) const noexcept
{
    // Since all depth formats may be optional, we need to find a suitable depth format to use
    // Start with the highest precision packed format
    static const xarray<VkFormat,5> depthFormats = 
    { 
        VK_FORMAT_D32_SFLOAT_S8_UINT, 
        VK_FORMAT_D32_SFLOAT,
        VK_FORMAT_D24_UNORM_S8_UINT, 
        VK_FORMAT_D16_UNORM_S8_UINT, 
        VK_FORMAT_D16_UNORM 
    };

    for( auto& Format : depthFormats )
    {
        VkFormatProperties FormatProps;
        vkGetPhysicalDeviceFormatProperties( m_VKPhysicalDevice, Format, &FormatProps );

        // Format must support depth stencil attachment for optimal tiling
        if( FormatProps.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT )
        {
            DepthFormat = Format;
            return x_error_ok( );
        }
    }

    static const err Error = x_error_code( errors, ERR_FAILURE, "Could not find a valid depth format" );
    std::cout << Error.getString() << ": " << "";
    return Error;
}


//------------------------------------------------------------------------------------

device::err device::Initialize(
    const xndptr<VkQueueFamilyProperties>&      DeviceProps,
    const int                                   iDeviceQueue,
    const VkPhysicalDevice&                     PhysicalDevice,
    const bool                                  enableValidation ) noexcept
{
    VkResult VKErr;

    //
    // Allocate all the data require for each of the threads
    //
    m_PerThreadData.New( g_context::get().m_Scheduler.getWorkerCount(), *this );

    //
    // Vulkan device
    //
    static const xarray<float, 1>   queuePriorities = { 0.0f };
    VkDeviceQueueCreateInfo         queueCreateInfo = {};

    queueCreateInfo.sType               = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex    = iDeviceQueue;
    queueCreateInfo.queueCount          = 1;
    queueCreateInfo.pQueuePriorities    = &queuePriorities[0];

    m_VKPhysicalDevice  = PhysicalDevice;
    VKErr               = CreateDevice( queueCreateInfo, enableValidation );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Could not create the Vulkan device" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    // Gather physical device memory properties
    vkGetPhysicalDeviceMemoryProperties( m_VKPhysicalDevice, &m_VKDeviceMemoryProperties );

    //
    // Get the graphics queue
    //
    vkGetDeviceQueue( m_VKDevice, iDeviceQueue, 0, &m_VKQueue );

    return x_error_ok( errors );
}

//------------------------------------------------------------------------------------

bool device::getMemoryType( uint32_t typeBits, VkFlags properties, uint32_t& typeIndex ) const noexcept
{
    for( uint32_t i = 0; i < 32; i++ )
    {
        if( (typeBits & 1) == 1 )
        {
            if( ( m_VKDeviceMemoryProperties.memoryTypes[i].propertyFlags & properties ) == properties )
            {
                typeIndex = i;
                return true;
            }
        }
        typeBits >>= 1;
    }

    ENG_WARNINGLOG( m_Instance, "Fail to find memory flags" );
    return false;
}

//---------------------------------------------------------------------------------

device::err device::FlushVKSetupCommandBuffer( void ) noexcept
{
    VkResult VKErr;

    auto& PerThread = getPerThreadData();

    if( PerThread.m_VKSetupCmdBuffer == VK_NULL_HANDLE )
        return x_error_ok(  );

    VKErr = vkEndCommandBuffer( PerThread.m_VKSetupCmdBuffer );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to End the command Buffer" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    VkSubmitInfo submitInfo = {};
    submitInfo.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount   = 1;
    submitInfo.pCommandBuffers      = &PerThread.m_VKSetupCmdBuffer;

    VKErr = vkQueueSubmit( m_VKQueue, 1, &submitInfo, VK_NULL_HANDLE );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to submit the setup cmd buffer" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    VKErr = vkQueueWaitIdle( m_VKQueue );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to wait while idle in the setup cmd buffer" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    vkFreeCommandBuffers( m_VKDevice, PerThread.m_DisplayCmdPool.getVKCmdPool(), 1, &PerThread.m_VKSetupCmdBuffer );

    // todo : check if still necessary
    PerThread.m_VKSetupCmdBuffer = VK_NULL_HANDLE;
    return x_error_ok( );  
}

//---------------------------------------------------------------------------------

device::err device::CreateVKSetupCommandBuffer( void ) noexcept
{
    VkResult    VKErr;
    auto&       PerThread = getPerThreadData();

    if( PerThread.m_VKSetupCmdBuffer != VK_NULL_HANDLE )
    {
        vkFreeCommandBuffers( m_VKDevice, PerThread.m_DisplayCmdPool.getVKCmdPool(), 1, &PerThread.m_VKSetupCmdBuffer );
        PerThread.m_VKSetupCmdBuffer = VK_NULL_HANDLE;                                    // todo : check if still necessary
    }

    VkCommandBufferAllocateInfo cmdBufAllocateInfo = tools::initializers::commandBufferAllocateInfo(
                                            PerThread.m_DisplayCmdPool.getVKCmdPool(),
                                            VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                                            1 );

    VKErr = vkAllocateCommandBuffers( m_VKDevice, &cmdBufAllocateInfo, &PerThread.m_VKSetupCmdBuffer );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to allocate command buffer" ) ;
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    // todo : Command buffer is also started here, better put somewhere else
    // todo : Check if necessary at all...
    VkCommandBufferBeginInfo cmdBufInfo = {};
    cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    // todo : check null handles, flags?

    VKErr = vkBeginCommandBuffer( PerThread.m_VKSetupCmdBuffer, &cmdBufInfo );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to begging to command buffer" ) ;
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    return x_error_ok();
}

//---------------------------------------------------------------------------------

device::err  device::CreateMainCommandBuffers( void ) noexcept
{
    VkResult    VKErr;

    // Create one command buffer per frame buffer in the swap chain. The expectation here is that it will be two.
    // Command buffers store a reference to the frame buffer inside their render pass info so for static usage without having to rebuild 
    // them each frame, we use one per frame buffer
    VkCommandBufferAllocateInfo cmdBufAllocateInfo = tools::initializers::commandBufferAllocateInfo(
            m_VKMainCmdPool,
            VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            (uint32_t)m_lVKMainCmdBuffers.getCount() );

    VKErr = vkAllocateCommandBuffers( m_VKDevice, &cmdBufAllocateInfo, &m_lVKMainCmdBuffers[0] );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to Allocate the command buffers for the page swap" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    // Create one command buffer for submitting the
    // post present image memory barrier
    cmdBufAllocateInfo.commandBufferCount = 1;

    VKErr = vkAllocateCommandBuffers( m_VKDevice, &cmdBufAllocateInfo, &m_VKPostPresentCmdBuffer );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to Allocate the post presets command buffers for the page swap" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    //
    // Allocate the per thread pools
    //
    /*
    for( auto& PerThread : m_PerThreadData )
    {
        // Create one command buffer per frame buffer in the swap chain. The expectation here is that it will be two.
        // Command buffers store a reference to the frame buffer inside their render pass info so for static usage without having to rebuild 
        // them each frame, we use one per frame buffer
        VkCommandBufferAllocateInfo cmdBufAllocateInfo = tools::initializers::commandBufferAllocateInfo(
                PerThread.m_VKCmdPool,
                VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                (uint32_t)1 );

        VKErr = vkAllocateCommandBuffers( m_VKDevice, &cmdBufAllocateInfo, &PerThread.m_VKSetupCmdBuffer );
        if( VKErr )
        {
            static const err Error { x_error_code( errors, ERR_FAILURE, "Fail to Allocate the command buffers for setting up" ) };
            std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
            return Error;
        }
    }
    */

    return x_error_ok();
}

//---------------------------------------------------------------------------------

device::err device::CreatePipelineCache( void ) noexcept
{
    VkResult                    VKErr;
    VkPipelineCacheCreateInfo   pipelineCacheCreateInfo = {};
    pipelineCacheCreateInfo.sType   = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;

    VKErr = vkCreatePipelineCache( m_VKDevice, &pipelineCacheCreateInfo, nullptr, &m_VKPipelineCache );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to Create rthe pipeline cache" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    return x_error_ok(); 
}

//---------------------------------------------------------------------------------

device::err device::CreateCommandPools( const int iQueueNodeIndex ) noexcept
{
    //
    // Create the main cmd pool
    //
    VkCommandPoolCreateInfo cmdPoolInfo = {};
    cmdPoolInfo.sType               = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmdPoolInfo.queueFamilyIndex    = iQueueNodeIndex;
    cmdPoolInfo.flags               = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    auto VKErr = vkCreateCommandPool( m_VKDevice, &cmdPoolInfo, nullptr, &m_VKMainCmdPool );
    if( VKErr )
    {
        static const err Error = x_error_code( errors, ERR_FAILURE, "Fail to create the main command pool" );
        std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
        return Error;
    }

    //
    // Create the per thread cmd pools
    //
    for( auto& PerThread : m_PerThreadData )
    {
    /*
        VkCommandPoolCreateInfo cmdPoolInfo = {};
        cmdPoolInfo.sType               = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        cmdPoolInfo.queueFamilyIndex    = iQueueNodeIndex;
        cmdPoolInfo.flags               = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        auto VKErr = vkCreateCommandPool( m_VKDevice, &cmdPoolInfo, nullptr, &PerThread.m_VKCmdPool );
        if( VKErr )
        {
            static const err Error { x_error_code( errors, ERR_FAILURE, "Fail to create the per thread command pools" ) };
            std::cout << Error.getString() << ": " << debug::VulkanErrorString( VKErr );
            return Error;
        }
        */
        auto Error = PerThread.m_DisplayCmdPool.Initialize( iQueueNodeIndex );
        if( Error ) return Error;
    }

    return x_error_ok(); 
}

//---------------------------------------------------------------------------------

device::err device::PrepareWindowAttachment( const int iQueueNodeIndex ) noexcept
{
    err Err;

    m_iQueueNodeIndex = iQueueNodeIndex; 

    Err = CreateCommandPools( iQueueNodeIndex );
    if( Err ) return Err;

    Err = CreateVKSetupCommandBuffer();
    if( Err ) return Err;

    Err = CreateMainCommandBuffers();
    if( Err ) return Err;

    Err = CreatePipelineCache();
    if( Err ) return Err;

    //
    // Create bla bla
    //

    return x_error_ok();
}

//---------------------------------------------------------------------------------

void device::onEndRender( void ) noexcept
{
    //
    // Notify anyone using the device that pageflip is about to happen
    //
    m_Events.m_JustBeforePageFlip.Notify();

    //
    // Collect all the secondary command buffers from all threads and insert them in the right order
    //
    const auto  FrameIndex = getFrameIndex();
    int         TotalCmdToSubmit = 0;
    for( auto& PerThread : m_PerThreadData )
    {
        TotalCmdToSubmit += PerThread.m_DisplayCmdPool.getInusedCmdCount( FrameIndex );
    }

    //
    // Commit all the secondary command buffers to the main command buffer
    //
    if( TotalCmdToSubmit > 0 )
    {
        using cmlist = xafptr<VkCommandBuffer>;
        using dmlist = xafptr<display_cmds*>;
        int         GlobalIndex         {0};
        dmlist      SecondaryCmdList    {};
        cmlist      Cmd                 {};

        //
        // Allocate the array of entries
        //
        SecondaryCmdList.Alloc( TotalCmdToSubmit );
        Cmd.Alloc(TotalCmdToSubmit);
        
        //
        // Collect them all in one list            shader
        //
        for( auto& PerThread : m_PerThreadData )
        {
            auto&       CmdPool = PerThread.m_DisplayCmdPool;
            const auto  Count   = PerThread.m_DisplayCmdPool.getInusedCmdCount( FrameIndex );
            
            for( int i = 0; i < Count; i++ )
            {
                SecondaryCmdList[GlobalIndex++] = &CmdPool.getInUsedDisplayCmd( FrameIndex, i );
            }
        }
        x_assert( GlobalIndex == TotalCmdToSubmit );

        //
        // Sort all the cmd lists
        //
        SecondaryCmdList.QSort( 
            []( const dmlist::t_entry& A, const dmlist::t_entry& B )
            { 
                if( A->getOrder() < B->getOrder() ) return -1;
                return static_cast<int>(A->getOrder() > B->getOrder()); 
            } );

        //
        // Submit them all in the right order
        //
        auto MainCmdBuffer = getVKMainCmdBuffer();
        int i=0;
        for( auto& pEntry : SecondaryCmdList )
        {
            auto CmdBuffer = pEntry->getVKCmdBuffer();

            vkEndCommandBuffer( CmdBuffer );
            Cmd[i++] = CmdBuffer;
//            vkCmdExecuteCommands( MainCmdBuffer, 1, &CmdBuffer );
        }

        vkCmdExecuteCommands( MainCmdBuffer, static_cast<uint32_t>(Cmd.getCount()), &Cmd[0] );
       // vkCmdExecuteCommands( MainCmdBuffer, static_cast<u32>(Cmd.getCount()), Cmd );
    }
}

//---------------------------------------------------------------------------------

void device::onPageFlip( void ) noexcept 
{ 
    //
    // Advance to next frame
    //
    m_FrameNumber++;

    //
    // Notify anyone using the device that pageflip is about to happen
    //
    m_Events.m_JustAfterPageFlip.Notify();
}

//---------------------------------------------------------------------------------

device::err device::CreateTexture( eng_texture::handle& Texture, const xbitmap& Bitmap ) noexcept
{
    auto& Tex = m_TexturePool.pop( *this );
    Texture = Tex; 

    CreateVKSetupCommandBuffer();
    Tex.setupFrom( getVKSetupCmdBuffer(), Bitmap );

    return x_error_ok( );
}

//---------------------------------------------------------------------------------

void device::ReleaseTexture( texture& Texture ) noexcept
{
    m_TexturePool.push( Texture );
}

//---------------------------------------------------------------------------------

device::err device::CreateSampler( eng_sampler::handle& hSampler, const eng_sampler::setup& Setup ) noexcept
{
    auto& Sampler = m_SamplerPool.pop();
    Sampler.Initialize( Setup, *this );
    hSampler = Sampler;
    return x_error_ok();
}

//---------------------------------------------------------------------------------

void device::ReleaseSampler( sampler& Sampler ) noexcept
{
    m_SamplerPool.push( Sampler );
}

//---------------------------------------------------------------------------------

device::err device::CreateMaterialInformed( eng_material_informed::handle& hMaterial, const eng_material_informed::setup& Setup ) noexcept
{
    auto& Material = m_MaterialInformedPool.pop( *this );
    Material.Initialize( Setup );
    hMaterial = Material;
    return x_error_ok();
}

//---------------------------------------------------------------------------------

void device::ReleaseMaterialInformed( material_informed& Material ) noexcept
{
    m_MaterialInformedPool.push( Material );
}

//---------------------------------------------------------------------------------

device::err device::CreateMaterialType( eng_material_type::handle& hMaterial, const eng_material_type::setup& Setup ) noexcept
{
    auto& Material = m_MaterialTypePool.pop( *this );
    Material.Initialize( Setup );
    hMaterial = Material;
    return x_error_ok();
}

//---------------------------------------------------------------------------------

device::err device::CreateUniformStream( eng_uniform_stream::handle& hUniformStream, const eng_uniform_stream::setup& Setup ) noexcept
{
    auto& UniformStream = m_UniformStreamPool.pop( *this );
    UniformStream.Initialize( Setup );
    hUniformStream = UniformStream;
    return x_error_ok();
}

//---------------------------------------------------------------------------------

void device::ReleaseMaterial( material_type& Material ) noexcept
{
    m_MaterialTypePool.push( Material );
}

//---------------------------------------------------------------------------------

device::err device::CreateVertexShader      ( eng_shader_vert::handle& hShaderVert, const eng_shader_vert::setup& Setup )   noexcept
{
    auto& Shader = m_ShaderVertPool.pop( *this );
    Shader.Initialize( Setup );
    hShaderVert = Shader;
    return x_error_ok();
}

//---------------------------------------------------------------------------------

device::err device::CreateFragmentShader    ( eng_shader_frag::handle& hShaderFrag, const eng_shader_frag::setup& Setup )   noexcept
{
    auto& Shader = m_ShaderFragPool.pop( *this );
    Shader.Initialize( Setup );
    hShaderFrag = Shader;
    return x_error_ok();
}

//---------------------------------------------------------------------------------

device::err device::CreateGeometryShader    ( eng_shader_geom::handle& hShaderGeom, const eng_shader_geom::setup& Setup )   noexcept
{
    auto& Shader = m_ShaderGeomPool.pop( *this );
    Shader.Initialize( Setup );
    hShaderGeom = Shader;
    return x_error_ok();
}

//---------------------------------------------------------------------------------

device::err device::CreatePipeline( eng_pipeline::handle& hPipeline, const eng_pipeline::setup& Setup ) noexcept
{
    auto& EngPipeline = m_EngPipelinePool.pop( *this );
    EngPipeline.Initialize( Setup );
    hPipeline = EngPipeline;
    return x_error_ok();
}

//---------------------------------------------------------------------------------

device::device_type device::getDeviceType ( void ) const noexcept
{
    return DEVICE_TYPE_RENDER;
}

//---------------------------------------------------------------------------------

display_cmds& device::getDisplayCmdList ( const f32 Order, window* pWindow ) noexcept
{
    auto& PerThread     = getPerThreadData();
    auto& EngBuffer     = PerThread.m_DisplayCmdPool.AllocateDisplayCmd( getFrameIndex(), Order, pWindow );    
    return EngBuffer;
}

//---------------------------------------------------------------------------------

void device::SubmitDisplayCmdList( display_cmds& CmdList ) noexcept
{
    auto& PerThread     = getPerThreadData();
    PerThread.m_DisplayCmdPool.SubmitCmdList( getFrameIndex(), CmdList );    
}


//------------------------------------------------------------------------------------
// DONE WITH NAMESPACE
//------------------------------------------------------------------------------------
}
