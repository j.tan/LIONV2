//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class window final : 
    public eng_window,
#if _X_TARGET_WINDOWS
    public eng_pc_system_window
#else
#endif
{
public:

    inline                          window                              ( device& Device )                                      noexcept : m_SwapChain{ Device } {}
                err                 Initialize                          ( const setup& Setup )                                  noexcept;
    constexpr   auto&               getTheDevice                        ( void ) const                                          noexcept            { return m_SwapChain.getTheDevice(); }
    virtual     const eng_view&     getActiveView                       ( void ) const                                          noexcept override   { return m_View; }
    constexpr   auto                getVKRenderPass                     ( void ) const                                          noexcept            { return m_VKRenderPass; }
    virtual     void                Release                     ( void )                                                                        noexcept override {}

protected:

    struct depth_stencil 
    {
        VkImage             m_VKImage           { VK_NULL_HANDLE };
        VkDeviceMemory      m_VKMem             { VK_NULL_HANDLE };
        VkImageView         m_VKImageView       { VK_NULL_HANDLE };
        VkFormat            m_VKFormat          { VK_FORMAT_UNDEFINED };   // Depth buffer format, Depth format is selected during Vulkan initialization
    };

    struct stats
    {
        xtimer                      m_Timer         {};
        xarray<xtimer::ticks,8>     m_FrameTime     { 0     };
        int                         m_Index         { 0     };
        
        inline      void    Update      ( void )            noexcept;
        inline      f64     getAvg      ( void ) const      noexcept;
        inline      f64     getLast     ( void ) const      noexcept;
        inline      f64     getFPS      ( void ) const      noexcept;

    };

protected:

                err                 CreateDepthStencil                  (   VkCommandBuffer VKSetupCmdBuffer, 
                                                                            const int       Width, 
                                                                            const int       Height )                            noexcept;
                err                 CreateFrameBuffer                   ( const int Width, const int Height )                   noexcept;
                err                 FillMainCmdBufferWithEmptyRender    ( const int Width, const int Height )                   noexcept;
                err                 setupRenderPass                     ( void )                                                noexcept;
                void                setActiveView                       ( const eng_view& View, const bool bUpdateViewPort )    noexcept;
                
    virtual     bool                HandleEvents                        ( void )                                                noexcept override;
    virtual     int                 getWidth                            ( void ) const                                          noexcept override { return m_Width;   }
    virtual     int                 getHeight                           ( void ) const                                          noexcept override { return m_Height;  }
    virtual     void                PageFlip                            ( void )                                                noexcept override;
    virtual     void                BeginRender                         ( const eng_view& View, const bool bUpdateViewPort )    noexcept override;
    virtual     void                EndRender                           ( void )                                                noexcept override;
                err                 FillMainCmdWithDefaults             ( const int Width, const int Height )                   noexcept;
    virtual     void                Resize                              ( const int Width, const int Height )                   noexcept override;
    virtual     eng_device&         getDevice                           ( void )                                                noexcept override;
    virtual     eng_display_cmds&   getDisplayCmdList                   ( const f32 Order )                                     noexcept override;
    virtual     void                SubmitDisplayCmdList                ( eng_display_cmds& CmdList )                           noexcept override;
    virtual     u64                 getSystemWindowHandle               ( void ) const                                          noexcept override { return *reinterpret_cast<const u64*>(&m_hWindow); } 
    virtual     const eng_input::keyboard&      getLocalKeyboard        ( void ) const                                          noexcept override { return m_Keyboard; } 
    virtual     const eng_input::mouse&         getLocalMouse           ( void ) const                                          noexcept override { return m_Mouse;    }

protected:

    swapchain                           m_SwapChain;
    depth_stencil                       m_DepthStencil              {};
    xarray<VkFramebuffer,2>             m_lVKFrameBuffers           { nullptr };     // List of available frame buffers (same as number of swap chain images)
    VkClearValue                        m_VKClearColorVal           { { 1.00f, 0.25f, 0.25f, 1.0f } };
    VkClearValue                        m_VKClearDepthVal           { 1.0f, 0 };
    VkRenderPass                        m_VKRenderPass              { VK_NULL_HANDLE };     // Global render pass for frame buffer writes
    int                                 m_nViewports                {1};
    int                                 m_nScissors                 {1};
    xarray<VkViewport,16>               m_Viewport                  {};
    xarray<VkRect2D,16>                 m_Scissor                   {};
    stats                               m_GPUStats                  {};
    stats                               m_CPUStats                  {};
    eng_view                            m_View                      {};
    VkFence                             m_RenderFence               {};

protected:

    friend class eng_window;
};



