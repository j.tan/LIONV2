//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//------------------------------------------------------------------------------------------------
class display_cmds : public eng_display_cmds
{
public:
        
    constexpr                       display_cmds        ( device& Device ) : m_Device{ Device } {}
    inline      draw&               getDraw             ( void )                                                noexcept { return m_Draw;        }
    inline      device&             getTheDevice        ( void )                                                noexcept { return m_Device;      }
    constexpr   float               getOrder            ( void )                                        const   noexcept { return m_Order;       }
    constexpr   VkCommandBuffer     getVKCmdBuffer      ( void )                                        const   noexcept { return m_VKCmdBuffer; }
    inline      void                Reset               ( void )                                                noexcept { x_assert(m_bSubmitted == true ); m_bSubmitted = m_bStarted = false; }
    constexpr   bool                isStarted           ( void )                                        const   noexcept { return m_bStarted;    }
    inline      void                setStarted          ( bool bState )                                         noexcept { m_bStarted = bState;  }
    constexpr   window&             getWindow           ( void )                                        const   noexcept { return *m_pWindow;    }
    virtual     void                Draw                ( eng_draw::pipeline                PipeLine, 
                                                          xfunction<void(eng_draw& Draw)>   Callback )          noexcept;
    virtual     void                Release             ( void )                                                noexcept override {}
        
protected:

    window*             m_pWindow           { nullptr };            // 
    device&             m_Device;                                   // reference back to the device
    VkCommandBuffer     m_VKCmdBuffer       { VK_NULL_HANDLE };     // the actual Vulkan display list
    draw                m_Draw              { *this };              // A reference to draw
    float               m_Order             { 0 };                  // Order in which this command list will be render
    bool                m_bStarted          { false };              // If we have initialize with VK commands
    bool                m_bSubmitted        { false };              // Checking that we have submitted

protected:
    friend class display_cmd_pool;
};

//------------------------------------------------------------------------------------------------
class display_cmd_pool
{
public:

    inline                                  display_cmd_pool        ( device& Device )                                      noexcept;
    inline                                 ~display_cmd_pool        ( void )                                                noexcept;
    inline              auto                Initialize              ( int iQueueFamilyIndex )                               noexcept;
    inline              display_cmds&       AllocateDisplayCmd      ( const int FrameIndex, const float Order, window* pWindow ) noexcept;
    inline              void                SubmitCmdList           ( const int FrameIndex, display_cmds& CmdList )         noexcept;
    x_orinlineconst     VkCommandPool       getVKCmdPool            ( void )                                        const   noexcept { x_assert_linear( m_Debug_LQ ); return m_VKCmdPool; }
    x_orinlineconst     int                 getInusedCmdCount       ( const int FrameIndex )                        const   noexcept { x_assert_linear( m_Debug_LQ ); return m_InUsedCmdLists[FrameIndex].m_nCmdInUsed; }
    inline              display_cmds&       getInUsedDisplayCmd     ( const int FrameIndex, const int Index )               noexcept { x_assert_linear( m_Debug_LQ ); return m_DisplayCmdPool[ m_InUsedCmdLists[FrameIndex].m_lInUsed[Index] ]; }
    
protected:

    enum : int { MAX_DISPLAY_CMDS = 128 };
    struct in_used_cmds
    {
        int                             m_nCmdInUsed    { 0};
        xarray<s16,MAX_DISPLAY_CMDS>    m_lInUsed       {s16(-1)};
    };

protected:

    inline      void                onJustAfterPageFlip     ( void )                                                noexcept;

protected:

#if _X_DEBUG
    x_debug_linear_quantum                  m_Debug_LQ              {};
#endif

    VkCommandPool                           m_VKCmdPool             { VK_NULL_HANDLE };         // The actual VK cmd Pool
    int                                     m_nFreeEntries          {MAX_DISPLAY_CMDS};         // 
    xarray<in_used_cmds,2>                  m_InUsedCmdLists        {};                         // Frame dependent in used commands lists
    xarray<s16,MAX_DISPLAY_CMDS>            m_FreeCmds              {};                         // Array to indices of cmds that are free
    xvector<display_cmds>                   m_DisplayCmdPool;                                   // The actual list of commands. (This is frame indipendent)
    x_message::delegate<display_cmd_pool>   m_DelegateJustAfterPageFlip;                        // Delegate responsable to route messages for Just After Page Flip
};

