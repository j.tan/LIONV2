 #include <windows.h>
 
 #include "eng_base.h"
 //#include "eng_driver_pc.h"
 #include "eng_vkDebug.h"

 #pragma comment(lib, "../../LION/3rdParty/vulkan/libs/vulkan/vulkan-1")

 
 void buildCommandBuffers( eng_instance::base& Engine, int Width, int Height );

//-------------------------------------------------------------------------------                        
//-------------------------------------------------------------------------------                        
//-------------------------------------------------------------------------------                        
 #define ENG_WINDOW_THIS    auto& This = *reinterpret_cast<eng_active_driver::system_window*>(this); 
 #define ENG_BASE_THIS      auto& This = *reinterpret_cast<base_common*>(this);

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// BASE
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace eng_instance
{
    struct base_common : base
    {
        //---------------------------------------------------------------------------------

        VkResult createInstance( bool enableValidation, const char* pAppName )
        {
	        VkApplicationInfo appInfo   = {};
	        appInfo.sType               = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	        appInfo.pApplicationName    = pAppName;
	        appInfo.pEngineName         = "LION";

	        // Temporary workaround for drivers not supporting SDK 1.0.3 upon launch
	        // todo : Use VK_API_VERSION 
	        appInfo.apiVersion = VK_MAKE_VERSION( 1, 0, 2 );


            int                         nEnabledExtensions = 0;
            xarray<const char*,8>      pEnabledExtensions;
             
            pEnabledExtensions[nEnabledExtensions++] = VK_KHR_SURFACE_EXTENSION_NAME;

        #ifdef _WIN32
                pEnabledExtensions[nEnabledExtensions++] = VK_KHR_WIN32_SURFACE_EXTENSION_NAME;
        #else
                pEnabledExtensions[nEnabledExtensions++] = VK_KHR_XCB_SURFACE_EXTENSION_NAME;
        #endif

            if (enableValidation)
                pEnabledExtensions[nEnabledExtensions++] = VK_EXT_DEBUG_REPORT_EXTENSION_NAME;


	        // todo : check if all extensions are present
	        VkInstanceCreateInfo instanceCreateInfo = {};
	        instanceCreateInfo.sType                    = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	        instanceCreateInfo.pNext                    = NULL;
	        instanceCreateInfo.pApplicationInfo         = &appInfo;
		    instanceCreateInfo.enabledExtensionCount    = nEnabledExtensions;
		    instanceCreateInfo.ppEnabledExtensionNames  = &pEnabledExtensions[0];

	        if (enableValidation)
	        {
		        instanceCreateInfo.enabledLayerCount    = eng_vkdebug::s_ValidationLayerCount; // todo : change validation layer names!
		        instanceCreateInfo.ppEnabledLayerNames  = eng_vkdebug::s_pValidationLayerNames;
	        }

            m_bVerification = enableValidation;

	        return vkCreateInstance( &instanceCreateInfo, nullptr, &m_VkInstance );
        }

        //---------------------------------------------------------------------------------

        err InitVulkan( bool bValidate, const char* pAppName )
        {
	        VkResult VKErr;

	        // Vulkan instance
	        VKErr = createInstance( bValidate, pAppName );
            if( VKErr )
            {
                static const err Error { x_error_code( errors, ERR_VULKAN_INITIALIZATION, "Error Initializing Vulkan" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }
               
            return x_error_none( errors );
        }

        //---------------------------------------------------------------------------------

        err EnumerateDevices( xndptr<VkPhysicalDevice>& PhysicalDevices )
        {
	        VkResult VKErr;

	        // Physical device
	        uint32_t gpuCount = 0;

            //
	        // Get number of available physical devices
            //
            if((VKErr = vkEnumeratePhysicalDevices( m_VkInstance, &gpuCount, nullptr )) || gpuCount == 0 )
            {
                static const err Error { x_error_code( errors, ERR_VULKAN_INITIALIZATION, "Could not enumerate phyiscal devices with a count" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }

            //
	        // Enumerate devices
            //
            PhysicalDevices.New( (int)gpuCount );

	        
	        if((VKErr = vkEnumeratePhysicalDevices( m_VkInstance, &gpuCount, &PhysicalDevices[0] )) || gpuCount == 0 )
	        {
                static const err Error { x_error_code( errors, ERR_VULKAN_INITIALIZATION, "Could not enumerate phyiscal devices" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
	        }

            return x_error_none( errors );
        }

        //---------------------------------------------------------------------------------

        err DeviceProperties( xndptr<VkQueueFamilyProperties>& DeviceProps, int& iQueueGraphics, const VkPhysicalDevice& PhysicalDevice )
        {
            // Find a queue that supports graphics operations
            uint32_t queueCount         = 0;
            iQueueGraphics = -1;

            vkGetPhysicalDeviceQueueFamilyProperties( PhysicalDevice, &queueCount, NULL );
            if( queueCount <= 0 )
            {
                static const err Error { x_error_code( errors, ERR_VULKAN_INITIALIZATION, "Could not get device properties" ) };
                std::cout << Error.getString() << ": " << "Count of properties less than expected";
                return Error;
            }

            DeviceProps.New( queueCount );

            vkGetPhysicalDeviceQueueFamilyProperties( PhysicalDevice, &queueCount, &DeviceProps[0] );
            if( queueCount <= 0 )
            {
                static const err Error { x_error_code( errors, ERR_VULKAN_INITIALIZATION, "Could not get device properties" ) };
                std::cout << Error.getString() << ": " << "The actual data for them";
                return Error;
            }

            //
            // Make sure that ones of the properties is graphics
            //            
            for( auto& Prop : DeviceProps )
            {
                if( Prop.queueFlags & VK_QUEUE_GRAPHICS_BIT )
                {
                    iQueueGraphics = int(&Prop - &DeviceProps[0]);
                    break;
                }
	        }

	        if( iQueueGraphics == -1 )
            {
                static const err Error { x_error_code( errors, ERR_VULKAN_INITIALIZATION, "Could not find graphices properties for the selected device" ) };
                std::cout << Error.getString() << ": " << "";
                return Error;
            }

            return x_error_none( errors );
        }

        //---------------------------------------------------------------------------------

        static VkResult CreateDevice( VkDevice& Device, const VkPhysicalDevice& PhysicalDevice, const VkDeviceQueueCreateInfo& queueCreateInfo, const bool enableValidation )
        {
	        xarray<const char*,1> enabledExtensions   = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
	        VkDeviceCreateInfo          deviceCreateInfo    = {};

	        deviceCreateInfo.sType                          = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	        deviceCreateInfo.pNext                          = NULL;
	        deviceCreateInfo.queueCreateInfoCount           = 1;
	        deviceCreateInfo.pQueueCreateInfos              = &queueCreateInfo;
	        deviceCreateInfo.pEnabledFeatures               = NULL;

	        if( enabledExtensions.getCount() > 0 )
	        {
		        deviceCreateInfo.enabledExtensionCount      = (uint32_t)enabledExtensions.getCount();
		        deviceCreateInfo.ppEnabledExtensionNames    = &enabledExtensions[0];
	        }

	        if (enableValidation)
	        {
		        deviceCreateInfo.enabledLayerCount          = eng_vkdebug::s_ValidationLayerCount; // todo : validation layer names
		        deviceCreateInfo.ppEnabledLayerNames        = eng_vkdebug::s_pValidationLayerNames;
	        }

	        return vkCreateDevice( PhysicalDevice, &deviceCreateInfo, nullptr, &Device );
        }

        //---------------------------------------------------------------------------------

        err CreateDevice( const xndptr<VkQueueFamilyProperties>& DeviceProps, const int iQueueGraphics, const VkPhysicalDevice& PhysicalDevice, const bool enableValidation )
        {
            VkResult VKErr;

            // Vulkan device
            xarray<float, 1>      queuePriorities = { 0.0f };
            VkDeviceQueueCreateInfo     queueCreateInfo = {};

            queueCreateInfo.sType               = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfo.queueFamilyIndex    = iQueueGraphics;
            queueCreateInfo.queueCount          = 1;
            queueCreateInfo.pQueuePriorities    = &queuePriorities[0];

            if (( VKErr = CreateDevice( m_VkDevice, PhysicalDevice, queueCreateInfo, enableValidation ) ) )
            {
                static const err Error { x_error_code( errors, ERR_VULKAN_INITIALIZATION, "Could not create the Vulkan device" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }

            return x_error_none( errors );
        }

        //---------------------------------------------------------------------------------

        err getSupportedDepthFormat( VkFormat& DepthFormat, const VkPhysicalDevice& PhysicalDevice )
        {
            // Since all depth formats may be optional, we need to find a suitable depth format to use
            // Start with the highest precision packed format
            xarray<VkFormat,5> depthFormats = 
            { 
                VK_FORMAT_D32_SFLOAT_S8_UINT, 
                VK_FORMAT_D32_SFLOAT,
                VK_FORMAT_D24_UNORM_S8_UINT, 
                VK_FORMAT_D16_UNORM_S8_UINT, 
                VK_FORMAT_D16_UNORM 
            };

            for( auto& Format : depthFormats )
            {
                VkFormatProperties FormatProps;
                vkGetPhysicalDeviceFormatProperties( PhysicalDevice, Format, &FormatProps );

                // Format must support depth stencil attachment for optimal tiling
                if( FormatProps.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT )
                {
                    DepthFormat = Format;
                    return x_error_none( errors );
                }
            }

            static const err Error { x_error_code( errors, ERR_VULKAN_INITIALIZATION, "Could not find a valid depth format" ) };
            std::cout << Error.getString() << ": " << "";
            return Error;
        }

        //---------------------------------------------------------------------------------

        err   Initialize( const setup& Setup )
        {
            err                                Error;
            xndptr<VkQueueFamilyProperties>    DeviceProps;
            int                                iQueueGraphics;

            //
            // Setup the program instance
            //
            SetProgramInstance( (HINSTANCE)Setup.m_AppInstance );

            //
            // Create an instance of vulkan
            //
            if( (Error = base_common::InitVulkan( Setup.m_bValidation, Setup.m_pAppName )) )
                return Error;

            if( Setup.m_bValidation )
            {
                eng_vkdebug::setupDebugging( 
                    m_VkInstance,
                    VK_DEBUG_REPORT_ERROR_BIT_EXT                   | 
                    VK_DEBUG_REPORT_WARNING_BIT_EXT                 | 
                    VK_DEBUG_REPORT_INFORMATION_BIT_EXT             | 
                    VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT     , 
                    nullptr );
            }


            if( (Error = base_common::EnumerateDevices( m_PhysicalDevices )) )
                return Error;

            // Note : 
            // This example will always use the first physical device reported, 
            // change the vector index if you have multiple Vulkan devices installed 
            // and want to use another one
            if( (Error = DeviceProperties( DeviceProps, iQueueGraphics, m_PhysicalDevices[0] )) )
                return Error;

            if( (Error = CreateDevice( DeviceProps, iQueueGraphics, m_PhysicalDevices[0], Setup.m_bValidation )) )
                return Error;


            // Gather physical device memory properties
            vkGetPhysicalDeviceMemoryProperties( m_PhysicalDevices[0], &m_VkDeviceMemoryProperties );

            // Get the graphics queue
            vkGetDeviceQueue( m_VkDevice, iQueueGraphics, 0, &m_VkQueue );

            // Find a suitable depth format
            if( (Error = getSupportedDepthFormat( m_VkDepthFormat, m_PhysicalDevices[0] )) )
                return Error;

            if( m_SwapChain.Initialize( m_VkInstance, m_PhysicalDevices[0], m_VkDevice ).isError() )
                return x_error_code( errors, ERR_VULKAN_INITIALIZATION, "Fail to initialize the swapchain" );

            return x_error_none( errors );
        }

        //---------------------------------------------------------------------------------

        err CreateDrawCommandBuffers( void )
        {
            VkResult    VKErr;

            // Create one command buffer per frame buffer 
            // in the swap chain
            // Command buffers store a reference to the 
            // frame buffer inside their render pass info
            // so for static usage withouth having to rebuild 
            // them each frame, we use one per frame buffer
            m_DrawCmdBuffers.New( m_SwapChain.getImageCount() );

            VkCommandBufferAllocateInfo cmdBufAllocateInfo = eng_vktools::initializers::commandBufferAllocateInfo(
                    m_VkCmdPool,
                    VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    (uint32_t)m_DrawCmdBuffers.getCount() );

            VKErr = vkAllocateCommandBuffers( m_VkDevice, &cmdBufAllocateInfo, &m_DrawCmdBuffers[0] );
            if( VKErr )
            {
                static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to Allocate the command buffers for the page swap" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }

            // Create one command buffer for submitting the
            // post present image memory barrier
            cmdBufAllocateInfo.commandBufferCount = 1;

            VKErr = vkAllocateCommandBuffers( m_VkDevice, &cmdBufAllocateInfo, &m_VkPostPresentCmdBuffer );
            if( VKErr )
            {
                static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to Allocate the post presets command buffers for the page swap" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }

            return x_error_none( errors );
        }

        //---------------------------------------------------------------------------------

        err SetupDepthStencil( uint32_t Width, uint32_t Height )
        {
            VkImageCreateInfo Image         = {};
            Image.sType                     = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
            Image.pNext                     = nullptr;
            Image.imageType                 = VK_IMAGE_TYPE_2D;
            Image.format                    = m_VkDepthFormat;
            Image.extent                    = { Width, Height, 1 };
            Image.mipLevels                 = 1;
            Image.arrayLayers               = 1;
            Image.samples                   = VK_SAMPLE_COUNT_1_BIT;
            Image.tiling                    = VK_IMAGE_TILING_OPTIMAL;
            Image.usage                     = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
            Image.flags                     = 0;

            VkMemoryAllocateInfo MemAlloc   = {};
            MemAlloc.sType                  = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
            MemAlloc.pNext                  = nullptr;
            MemAlloc.allocationSize         = 0;
            MemAlloc.memoryTypeIndex        = 0;

            VkImageViewCreateInfo DepthStencilView              = {};
            DepthStencilView.sType                              = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            DepthStencilView.pNext                              = nullptr;
            DepthStencilView.viewType                           = VK_IMAGE_VIEW_TYPE_2D;
            DepthStencilView.format                             = m_VkDepthFormat;
            DepthStencilView.flags                              = 0;
            DepthStencilView.subresourceRange                   = {};
            DepthStencilView.subresourceRange.aspectMask        = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
            DepthStencilView.subresourceRange.baseMipLevel      = 0;
            DepthStencilView.subresourceRange.levelCount        = 1;
            DepthStencilView.subresourceRange.baseArrayLayer    = 0;
            DepthStencilView.subresourceRange.layerCount        = 1;

            VkMemoryRequirements    MemReqs;
            VkResult                VKErr;

            VKErr = vkCreateImage( m_VkDevice, &Image, nullptr, &m_DepthStencil.m_VkImage );
            if( VKErr )
            {
                static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to create the zbuffer image" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }

            vkGetImageMemoryRequirements( m_VkDevice, m_DepthStencil.m_VkImage, &MemReqs );
            MemAlloc.allocationSize = MemReqs.size;
            getMemoryType( MemReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, MemAlloc.memoryTypeIndex );
            VKErr = vkAllocateMemory( m_VkDevice, &MemAlloc, nullptr, &m_DepthStencil.m_VkMem );
            if( VKErr )
            {
                static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to Allocate memory for the zbuffer" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }

            VKErr = vkBindImageMemory( m_VkDevice, m_DepthStencil.m_VkImage, m_DepthStencil.m_VkMem, 0);
            if( VKErr )
            {
                static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to bind the depth stencil" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }

            eng_vktools::setImageLayout( 
                m_VkSetupCmdBuffer, 
                m_DepthStencil.m_VkImage, 
                VK_IMAGE_ASPECT_DEPTH_BIT, 
                VK_IMAGE_LAYOUT_UNDEFINED, 
                VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL );

            DepthStencilView.image = m_DepthStencil.m_VkImage;
            VKErr = vkCreateImageView( m_VkDevice, &DepthStencilView, nullptr, &m_DepthStencil.m_VkImageView );
            if( VKErr )
            {
                static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to create the depth stencil image view" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }

            return x_error_none( errors );
        }

        //---------------------------------------------------------------------------------

        err setupRenderPass( void )
        {
            VkResult    VKErr;

            VkAttachmentDescription Attachments[2];
            Attachments[0].format                   = m_SwapChain.m_VkColorFormat;
            Attachments[0].samples                  = VK_SAMPLE_COUNT_1_BIT;
            Attachments[0].loadOp                   = VK_ATTACHMENT_LOAD_OP_CLEAR;
            Attachments[0].storeOp                  = VK_ATTACHMENT_STORE_OP_STORE;
            Attachments[0].stencilLoadOp            = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            Attachments[0].stencilStoreOp           = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            Attachments[0].initialLayout            = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            Attachments[0].finalLayout              = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

            Attachments[1].format                   = m_VkDepthFormat;
            Attachments[1].samples                  = VK_SAMPLE_COUNT_1_BIT;
            Attachments[1].loadOp                   = VK_ATTACHMENT_LOAD_OP_CLEAR;
            Attachments[1].storeOp                  = VK_ATTACHMENT_STORE_OP_STORE;
            Attachments[1].stencilLoadOp            = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            Attachments[1].stencilStoreOp           = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            Attachments[1].initialLayout            = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
            Attachments[1].finalLayout              = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

            VkAttachmentReference ColorReference    = {};
            ColorReference.attachment               = 0;
            ColorReference.layout                   = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

            VkAttachmentReference DepthReference    = {};
            DepthReference.attachment               = 1;
            DepthReference.layout                   = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

            VkSubpassDescription Subpass            = {};
            Subpass.pipelineBindPoint               = VK_PIPELINE_BIND_POINT_GRAPHICS;
            Subpass.flags                           = 0;
            Subpass.inputAttachmentCount            = 0;
            Subpass.pInputAttachments               = nullptr;
            Subpass.colorAttachmentCount            = 1;
            Subpass.pColorAttachments               = &ColorReference;
            Subpass.pResolveAttachments             = nullptr;
            Subpass.pDepthStencilAttachment         = &DepthReference;
            Subpass.preserveAttachmentCount         = 0;
            Subpass.pPreserveAttachments            = nullptr;

            VkRenderPassCreateInfo RenderPassInfo   = {};
            RenderPassInfo.sType                    = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
            RenderPassInfo.pNext                    = nullptr;
            RenderPassInfo.attachmentCount          = 2;
            RenderPassInfo.pAttachments             = Attachments;
            RenderPassInfo.subpassCount             = 1;
            RenderPassInfo.pSubpasses               = &Subpass;
            RenderPassInfo.dependencyCount          = 0;
            RenderPassInfo.pDependencies            = nullptr;

            if( (VKErr=vkCreateRenderPass( m_VkDevice, &RenderPassInfo, nullptr, &m_VkRenderPass )) )
            {
                static const err Error { x_error_code( errors, ERR_RENDER_PASS, "Fail to Create render pass" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }
            return x_error_none( errors ); 
        }

        //---------------------------------------------------------------------------------

        err CreatePipelineCache( void )
        {
            VkResult                    VKErr;
            VkPipelineCacheCreateInfo   pipelineCacheCreateInfo = {};
            pipelineCacheCreateInfo.sType   = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
            if( (VKErr = vkCreatePipelineCache( m_VkDevice, &pipelineCacheCreateInfo, nullptr, &m_VkPipelineCache )) )
            {
                static const err Error { x_error_code( errors, ERR_RENDER_PASS, "Fail to Create rthe pipeline cache" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }
            return x_error_none( errors ); 
        }

        //---------------------------------------------------------------------------------

        err setupFrameBuffer( uint32_t Width, uint32_t Height )
        {
            VkResult    VKErr;
            VkImageView attachments[2];

            // Depth/Stencil attachment is the same for all frame buffers
            attachments[1] = m_DepthStencil.m_VkImageView;

            VkFramebufferCreateInfo frameBufferCreateInfo   = {};
            frameBufferCreateInfo.sType                     = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            frameBufferCreateInfo.pNext                     = NULL;
            frameBufferCreateInfo.renderPass                = m_VkRenderPass;
            frameBufferCreateInfo.attachmentCount           = 2;
            frameBufferCreateInfo.pAttachments              = attachments;
            frameBufferCreateInfo.width                     = Width;
            frameBufferCreateInfo.height                    = Height;
            frameBufferCreateInfo.layers                    = 1;

            // Create frame buffers for every swap chain image
            m_FrameBuffers.New( m_SwapChain.getImageCount() );
            for (uint32_t i = 0; i < m_FrameBuffers.getCount(); i++)
            {
                attachments[0] = m_SwapChain.m_Buffers[i].m_VkImageView;
                VKErr = vkCreateFramebuffer( m_VkDevice, &frameBufferCreateInfo, nullptr, &m_FrameBuffers[i] );
                if( VKErr )
                {
                    static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to Create the Frame Buffer" ) };
                    std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                    return Error;
                }
            }

            return x_error_none( errors ); 
        }

        //---------------------------------------------------------------------------------

        err DefaultDrawCommands( int Width, int Height )
        {
            VkCommandBufferBeginInfo cmdBufInfo = {};
            cmdBufInfo.sType                                = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            cmdBufInfo.pNext                                = nullptr;

            VkClearValue clearValues[2];
            clearValues[0].color                            = m_VkClearColorVal.color;   // color used to clear the background
            clearValues[1].depthStencil                     = m_VkClearDepthVal.depthStencil;

            VkRenderPassBeginInfo renderPassBeginInfo = {};
            renderPassBeginInfo.sType                       = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassBeginInfo.pNext                       = nullptr;
            renderPassBeginInfo.renderPass                  = m_VkRenderPass;
            renderPassBeginInfo.renderArea.offset.x         = 0;
            renderPassBeginInfo.renderArea.offset.y         = 0;
            renderPassBeginInfo.renderArea.extent.width     = Width;
            renderPassBeginInfo.renderArea.extent.height    = Height;
            renderPassBeginInfo.clearValueCount             = 2;
            renderPassBeginInfo.pClearValues                = clearValues;

            VkResult VKErr;

            for( int32_t i = 0; i < m_DrawCmdBuffers.getCount(); ++i )
            {
                // Set target frame buffer
                renderPassBeginInfo.framebuffer = m_FrameBuffers[i];

                VKErr = vkBeginCommandBuffer( m_DrawCmdBuffers[i], &cmdBufInfo );
                if( VKErr )
                {
                    static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to start adding commands to the default cmd list" ) };
                    std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                    return Error;
                }

                vkCmdBeginRenderPass( m_DrawCmdBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE );

                VKErr = vkEndCommandBuffer( m_DrawCmdBuffers[i] );
                assert( !VKErr );
            }

            return x_error_none( errors );
        }

    };

    //---------------------------------------------------------------------------------

    base::err base::CreateSetupCommandBuffer( void )
    {
        VkResult    VKErr;

        if( m_VkSetupCmdBuffer != VK_NULL_HANDLE )
        {
            vkFreeCommandBuffers( m_VkDevice, m_VkCmdPool, 1, &m_VkSetupCmdBuffer );
            m_VkSetupCmdBuffer = VK_NULL_HANDLE;                                    // todo : check if still necessary
        }

        VkCommandBufferAllocateInfo cmdBufAllocateInfo = eng_vktools::initializers::commandBufferAllocateInfo(
                                                m_VkCmdPool,
                                                VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                                                1 );

        VKErr = vkAllocateCommandBuffers( m_VkDevice, &cmdBufAllocateInfo, &m_VkSetupCmdBuffer );
        if( VKErr )
        {
            static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to allocate command buffer" ) };
            std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
            return Error;
        }

        // todo : Command buffer is also started here, better put somewhere else
        // todo : Check if necessaray at all...
        VkCommandBufferBeginInfo cmdBufInfo = {};
        cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        // todo : check null handles, flags?

        VKErr = vkBeginCommandBuffer( m_VkSetupCmdBuffer, &cmdBufInfo );
        if( VKErr )
        {
            static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to begging to command buffer" ) };
            std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
            return Error;
        }

        return x_error_none( errors );
    }


    //---------------------------------------------------------------------------------

    base::err base::FlushSetupCommandBuffer( void )
    {
	    VkResult VKErr;

	    if( m_VkSetupCmdBuffer == VK_NULL_HANDLE )
		    return x_error_none( errors );

	    VKErr = vkEndCommandBuffer( m_VkSetupCmdBuffer );
        if( VKErr )
        {
            static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to End the command Buffer" ) };
            std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
            return Error;
        }

	    VkSubmitInfo submitInfo = {};
	    submitInfo.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	    submitInfo.commandBufferCount   = 1;
	    submitInfo.pCommandBuffers      = &m_VkSetupCmdBuffer;

	    VKErr = vkQueueSubmit( m_VkQueue, 1, &submitInfo, VK_NULL_HANDLE );
        if( VKErr )
        {
            static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to submit the setup cmd buffer" ) };
            std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
            return Error;
        }

	    VKErr = vkQueueWaitIdle( m_VkQueue );
        if( VKErr )
        {
            static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to wait while idle in the setup cmd buffer" ) };
            std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
            return Error;
        }

	    vkFreeCommandBuffers( m_VkDevice, m_VkCmdPool, 1, &m_VkSetupCmdBuffer );

        // todo : check if still necessary
	    m_VkSetupCmdBuffer = VK_NULL_HANDLE;
        return x_error_none( errors );  
    }

    //---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------

    void setupConsole( std::string title, bool enableValidation )
    {
        AllocConsole();
        AttachConsole(GetCurrentProcessId());
        freopen("CON", "w", stdout);

        // SetConsoleTitle( TEXT( title.c_str() ) );

        if (enableValidation)
        {
            std::cout << "Validation enabled:\n";
        }
    }

    //---------------------------------------------------------------------------------

    base::err   base::Initialize( const setup& Setup )
    {
        ENG_BASE_THIS;

        setupConsole("Denug Output", Setup.m_bValidation );

        return This.Initialize( Setup );
    }

    //---------------------------------------------------------------------------------

    bool base::getMemoryType( uint32_t typeBits, VkFlags properties, uint32_t& typeIndex ) const
    {
        for( uint32_t i = 0; i < 32; i++ )
        {
            if ((typeBits & 1) == 1)
            {
                if (( m_VkDeviceMemoryProperties.memoryTypes[i].propertyFlags & properties) == properties)
                {
                    typeIndex = i;
                    return true;
                }
            }
            typeBits >>= 1;
        }

        ENG_WARNINGLOG( (*this), "Fail to find memory flags" );
        return false;
    }

    //---------------------------------------------------------------------------------

    base::err base::CreateSystemWindow( xowner<system_window*>& pWindow, system_window::setup& Setup )
    {
        ENG_BASE_THIS;

        xndptr<eng_active_driver::system_window> Window;
        Window.New();

        auto&       SystemWindow = Window[0];
        err         Err;
        VkResult    VKErr;

        //
        // Initialize the window
        //
        if( SystemWindow.Initialize( *this, Setup ) )
            return x_error_code( errors, ERR_WINDOW, "Fail creating the system window" );

        //
        // Chain the window with the engine
        //
        {
            eng_vkswapchain::setup_surface SetupSurface;
            SetupSurface.m_pHWNDWin = (void*)SystemWindow.m_hwnd;
            if( This.m_SwapChain.InitSurface( SetupSurface ) )
                return x_error_code( errors, ERR_SWAPCHAIN, "Error initializing the swapchain" );
        }

        //
        // Create Command Pool
        //
        {
            VkCommandPoolCreateInfo cmdPoolInfo = {};
            cmdPoolInfo.sType               = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
            cmdPoolInfo.queueFamilyIndex    = This.m_SwapChain.m_QueueNodeIndex;
            cmdPoolInfo.flags               = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
            if( (VKErr = vkCreateCommandPool( m_VkDevice, &cmdPoolInfo, nullptr, &m_VkCmdPool )) )
            {
                static const err Error { x_error_code( errors, ERR_GENERAL, "Fail to create the main command buffer" ) };
                std::cout << Error.getString() << ": " << eng_vkdebug::VulkanErrorString( VKErr );
                return Error;
            }
        }

        //
        // Create Command Pool
        //
        if( (Err = This.CreateSetupCommandBuffer()) )
            return Err;

        //
        // Create the swap chain
        //
        uint32_t Width  = static_cast<uint32_t>(Setup.m_Width);
        uint32_t Height = static_cast<uint32_t>(Setup.m_Height); 
        if( This.m_SwapChain.Create( m_VkSetupCmdBuffer, Width, Height ) )
            return x_error_code( errors, ERR_SWAPCHAIN, "Error Creating the swapchain" ); 

        //
        // Create a command buffer per frame buffer in the swap chain
        //
        if( (Err = This.CreateDrawCommandBuffers()) )
            return Err;

        if( (Err = This.setupRenderPass()) )
            return Err;

        if( (Err = This.CreatePipelineCache()) )
            return Err;

        if( (Err = This.SetupDepthStencil( Width, Height )) )
            return Err;

        if( (Err = This.setupFrameBuffer( Width, Height )) )
            return Err;

        if( (Err = This.FlushSetupCommandBuffer()) )
            return Err;

        if( (Err = This.DefaultDrawCommands( Width, Height )) )
            return Err;

	    // Recreate setup command buffer for derived class
 //       if( (Err = This.CreateDrawCommandBuffers()) )
 //           return Err;


        // Ok assign the pointer of the window to the user
        pWindow = Window.TransferOnwerShip();

        vkDeviceWaitIdle( getDevice() );

        return x_error_none( errors );
    }

    //---------------------------------------------------------------------------------

    void base::RenderEnd( void )
    {
        VkResult                VKErr;

        //
        // Finish the render pass
        //
        vkCmdEndRenderPass( m_DrawCmdBuffers[m_iCurrentBuffer] );

        // Add a present memory barrier to the end of the command buffer
        // This will transform the frame buffer color attachment to a
        // new layout for presenting it to the windowing system integration 
        VkImageMemoryBarrier prePresentBarrier = {};
        prePresentBarrier.sType                     = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        prePresentBarrier.pNext                     = nullptr;
        prePresentBarrier.srcAccessMask             = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        prePresentBarrier.dstAccessMask             = 0;
        prePresentBarrier.oldLayout                 = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        prePresentBarrier.newLayout                 = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        prePresentBarrier.srcQueueFamilyIndex       = VK_QUEUE_FAMILY_IGNORED;
        prePresentBarrier.dstQueueFamilyIndex       = VK_QUEUE_FAMILY_IGNORED;
        prePresentBarrier.subresourceRange          = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
        prePresentBarrier.image                     = m_SwapChain.getScreenBuffers(m_iCurrentBuffer).m_VkImage;

        VkImageMemoryBarrier *pMemoryBarrier = &prePresentBarrier;
        vkCmdPipelineBarrier(
            m_DrawCmdBuffers[ m_iCurrentBuffer ], 
            VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 
            VK_FLAGS_NONE,
            0, nullptr,
            0, nullptr,
            1, &prePresentBarrier);

        //
        // Finish the command list
        //
        VKErr = vkEndCommandBuffer( m_DrawCmdBuffers[m_iCurrentBuffer] );
        assert( !VKErr );

        //
        // PAGE FLIP
        //
        VkSemaphore             presentCompleteSemaphore;
        VkSemaphoreCreateInfo   presentCompleteSemaphoreCreateInfo  = {};
        presentCompleteSemaphoreCreateInfo.sType                    = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        presentCompleteSemaphoreCreateInfo.pNext                    = nullptr;
        presentCompleteSemaphoreCreateInfo.flags                    = VK_FENCE_CREATE_SIGNALED_BIT;

        VKErr = vkCreateSemaphore( m_VkDevice, &presentCompleteSemaphoreCreateInfo, nullptr, &presentCompleteSemaphore );
        assert( !VKErr );

        // Get next image in the swap chain (back/front buffer)
        VKErr = m_SwapChain.AcquireNextImage( presentCompleteSemaphore, m_iCurrentBuffer );
        assert(!VKErr);

        // The submit infor strcuture contains a list of
        // command buffers and semaphores to be submitted to a queue
        // If you want to submit multiple command buffers, pass an array
        VkSubmitInfo submitInfo = {};
        submitInfo.sType                            = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount               = 1;
        submitInfo.pWaitSemaphores                  = &presentCompleteSemaphore;
        submitInfo.commandBufferCount               = 1;
        submitInfo.pCommandBuffers                  = &m_DrawCmdBuffers[ m_iCurrentBuffer ];

        // Submit to the graphics queue
        VKErr = vkQueueSubmit( m_VkQueue, 1, &submitInfo, VK_NULL_HANDLE );
        assert( !VKErr );

        // Present the current buffer to the swap chain
        // This will display the image
        VKErr = m_SwapChain.QueuePresent( m_VkQueue, m_iCurrentBuffer );
        assert( !VKErr );

        vkDestroySemaphore( m_VkDevice, presentCompleteSemaphore, nullptr );

        // Add a post present image memory barrier
        // This will transform the frame buffer color attachment back
        // to it's initial layout after it has been presented to the
        // windowing system
        // See buildCommandBuffers for the pre present barrier that 
        // does the opposite transformation 
        VkImageMemoryBarrier postPresentBarrier = {};
        postPresentBarrier.sType                = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        postPresentBarrier.pNext                = NULL;
        postPresentBarrier.srcAccessMask        = 0;
        postPresentBarrier.dstAccessMask        = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        postPresentBarrier.oldLayout            = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        postPresentBarrier.newLayout            = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        postPresentBarrier.srcQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
        postPresentBarrier.dstQueueFamilyIndex  = VK_QUEUE_FAMILY_IGNORED;
        postPresentBarrier.subresourceRange     = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
        postPresentBarrier.image                = m_SwapChain.getScreenBuffers( m_iCurrentBuffer ).m_VkImage;

        // Use dedicated command buffer from example base class for submitting the post present barrier
        VkCommandBufferBeginInfo cmdBufInfo = {};
        cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

        VKErr = vkBeginCommandBuffer( m_VkPostPresentCmdBuffer, &cmdBufInfo );
        assert( !VKErr );

        // Put post present barrier into command buffer
        vkCmdPipelineBarrier(
            m_VkPostPresentCmdBuffer,
            VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            VK_FLAGS_NONE,
            0, nullptr,
            0, nullptr,
            1, &postPresentBarrier );

        VKErr = vkEndCommandBuffer( m_VkPostPresentCmdBuffer );
        assert( !VKErr );

        // Submit to the queue
        submitInfo                      = {};
        submitInfo.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount   = 1;
        submitInfo.pCommandBuffers      = &m_VkPostPresentCmdBuffer;

        VKErr = vkQueueSubmit( m_VkQueue, 1, &submitInfo, VK_NULL_HANDLE );
        assert( !VKErr );
        
        VKErr = vkQueueWaitIdle( m_VkQueue );
        assert( !VKErr );
    }

    //---------------------------------------------------------------------------------

    void base::RenderBegin( const system_window& SysWindow )
    {
        vkDeviceWaitIdle( getDevice() );

        auto Width  = SysWindow.getWidth();
        auto Height = SysWindow.getHeight();

        VkCommandBufferBeginInfo cmdBufInfo = {};
        cmdBufInfo.sType                                = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        cmdBufInfo.pNext                                = nullptr;

        VkClearValue clearValues[2];
        clearValues[0].color                            = { { 1.00f, 0.25f, 0.25f, 1.0f } };   // color used to clear the background
        clearValues[1].depthStencil                     = { 1.0f, 0 };

        VkRenderPassBeginInfo renderPassBeginInfo = {};
        renderPassBeginInfo.sType                       = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassBeginInfo.pNext                       = nullptr;
        renderPassBeginInfo.renderPass                  = m_VkRenderPass;
        renderPassBeginInfo.renderArea.offset.x         = 0;
        renderPassBeginInfo.renderArea.offset.y         = 0;
        renderPassBeginInfo.renderArea.extent.width     = Width;
        renderPassBeginInfo.renderArea.extent.height    = Height;
        renderPassBeginInfo.clearValueCount             = 2;
        renderPassBeginInfo.pClearValues                = clearValues;

        VkResult VKErr;
        
        // Set target frame buffer
        renderPassBeginInfo.framebuffer = m_FrameBuffers[m_iCurrentBuffer];

        VKErr = vkBeginCommandBuffer( m_DrawCmdBuffers[m_iCurrentBuffer], &cmdBufInfo );
        assert( !VKErr );

        // Update dynamic viewport state
        m_Viewport.width                              = (float) Width;
        m_Viewport.height                             = (float) Height;
        m_Viewport.minDepth                           = (float) 0.0f;
        m_Viewport.maxDepth                           = (float) 1.0f;
        vkCmdSetViewport( m_DrawCmdBuffers[m_iCurrentBuffer], 0, 1, &m_Viewport );

        // Update dynamic scissor state
        m_Scissor.extent.width                        = Width;
        m_Scissor.extent.height                       = Height;
        m_Scissor.offset.x                            = 0;
        m_Scissor.offset.y                            = 0;
        vkCmdSetScissor( m_DrawCmdBuffers[m_iCurrentBuffer], 0, 1, &m_Scissor );

        //
        // Beging the render pass
        //
        vkCmdBeginRenderPass( m_DrawCmdBuffers[m_iCurrentBuffer], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE );//VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS );
    }

    //-------------------------------------------------------------------------------                        

    base::~base( void )
    {
        // Clean up Vulkan resources
        m_SwapChain.Cleanup();

        if( m_VkSetupCmdBuffer != VK_NULL_HANDLE ) 
        {
            vkFreeCommandBuffers( m_VkDevice, m_VkCmdPool, 1, &m_VkSetupCmdBuffer );

        }

        //
        // Destroy Command Buffers
        //
        vkFreeCommandBuffers( m_VkDevice, m_VkCmdPool, (uint32_t)m_DrawCmdBuffers.getCount(), &m_DrawCmdBuffers[0] );
        vkFreeCommandBuffers( m_VkDevice, m_VkCmdPool, 1, &m_VkPostPresentCmdBuffer );

        //
        // Destroy Render Pass
        //
        vkDestroyRenderPass( m_VkDevice, m_VkRenderPass, nullptr );
        for( auto& VkFrameBuffer : m_FrameBuffers )
        {
            vkDestroyFramebuffer( m_VkDevice, VkFrameBuffer, nullptr );
        }

        //
        // Destroy Command Buffers
        //
        vkDestroyImageView  ( m_VkDevice, m_DepthStencil.m_VkImageView,     nullptr );
        vkDestroyImage      ( m_VkDevice, m_DepthStencil.m_VkImage,         nullptr );
        vkFreeMemory        ( m_VkDevice, m_DepthStencil.m_VkMem,           nullptr );

        vkDestroyPipelineCache( m_VkDevice, m_VkPipelineCache, nullptr );

        vkDestroyCommandPool( m_VkDevice, m_VkCmdPool, nullptr );

        vkDestroyDevice( m_VkDevice, nullptr ); 

        if( m_bVerification )
        {
            eng_vkdebug::freeDebugCallback( m_VkInstance );
        }

        vkDestroyInstance( m_VkInstance, nullptr );
    }

    //---------------------------------------------------------------------------------

    void base::Resize( void* pData, uint32_t& Width, uint32_t& Height )
    {
        // This is not ready
        // x_assert(false);

        ENG_BASE_THIS;

        vkDestroyImageView( This.m_VkDevice, This.m_DepthStencil.m_VkImageView, NULL);
        vkDestroyImage( This.m_VkDevice, This.m_DepthStencil.m_VkImage, NULL);
        vkFreeMemory( This.m_VkDevice, This.m_DepthStencil.m_VkMem, NULL);

        for( int i = 0; i < This.m_FrameBuffers.getCount(); i++ ) 
        {
            vkDestroyFramebuffer( This.m_VkDevice, This.m_FrameBuffers[i], NULL );
        }
        This.m_FrameBuffers.Delete();

        This.m_SwapChain.Cleanup();


        auto Err0 = This.CreateSetupCommandBuffer();
        x_assert(!Err0);

        eng_vkswapchain::setup_surface Setup;
        Setup.m_pHWNDWin = pData;
        auto Errn1 = This.m_SwapChain.InitSurface( Setup );

        auto Err1 = This.m_SwapChain.Create( m_VkSetupCmdBuffer, Width, Height );
        x_assert(!Err1);
        auto Err2 = This.SetupDepthStencil( Width, Height );
        x_assert(!Err2);
        auto Err3 = This.setupFrameBuffer( Width, Height );
        x_assert(!Err3);

        auto Err4 = This.FlushSetupCommandBuffer();
        x_assert(!Err4);

        This.DefaultDrawCommands( Width, Height );

        vkDeviceWaitIdle( getDevice() );
    }

    //---------------------------------------------------------------------------------

    xndptr_s<base> base::CreateInstance( void )
    {
        return static_cast< xowner<base*> >( x_new( base_common ) );
    }
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// SYSTEM WINDOW
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace eng_instance
{
    system_window::err system_window::Initialize( base& Base, const setup& Setup )
    {
        ENG_WINDOW_THIS;
        err Err;
        
        if( (Err = This.CreateSytemWindow( Setup )) )
            return Err;

        m_pEngBase = &Base;

        return x_error_none( errors );
    }

    //---------------------------------------------------------------------------------

    bool system_window::HandleEvents( void )
    {
        ENG_WINDOW_THIS;
        bool bContinue = true;

        MSG Message;

        if ( PeekMessage(&Message, This.m_hwnd, 0, 0, PM_REMOVE) )
        {
            if ( WM_QUIT == Message.message )
            {
                bContinue = false;
            }
            DispatchMessage(&Message);
        }
        return bContinue;
    }

    //---------------------------------------------------------------------------------

    int system_window::getWidth( void ) const
    {
        return reinterpret_cast<const eng_active_driver::system_window*>(this)->m_Width;
    }

    //---------------------------------------------------------------------------------

    int system_window::getHeight( void ) const
    {
        return reinterpret_cast<const eng_active_driver::system_window*>(this)->m_Height;
    }

}
