//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "eng_Base.h"

//------------------------------------------------------------------------------------

void eng_draw::CopySolidCubeData( xbuffer_view<eng_draw::vertex> pVertex, xbuffer_view<u32> pIndex, const xbbox& BBox, const xcolor Color ) noexcept
{
    x_constexprvar xarray<u32,6*6> StaticIndex 
    {{
        0u,  1u,  2u,      0u,  2u,  3u,    // front
        4u,  5u,  6u,      4u,  6u,  7u,    // back
        8u,  9u,  10u,     8u,  10u, 11u,   // top
        12u, 13u, 14u,     12u, 14u, 15u,   // bottom
        16u, 17u, 18u,     16u, 18u, 19u,   // right
        20u, 21u, 22u,     20u, 22u, 23u    // left
    }};
        
    // Front face
    pVertex[0]  = { { BBox.m_Min.m_X, BBox.m_Min.m_Y, BBox.m_Max.m_Z }, { 0.0f, 1.0f }, Color };
    pVertex[1]  = { { BBox.m_Max.m_X, BBox.m_Min.m_Y, BBox.m_Max.m_Z }, { 1.0f, 1.0f }, Color };
    pVertex[2]  = { { BBox.m_Max.m_X, BBox.m_Max.m_Y, BBox.m_Max.m_Z }, { 1.0f, 0.0f }, Color };
    pVertex[3]  = { { BBox.m_Min.m_X, BBox.m_Max.m_Y, BBox.m_Max.m_Z }, { 0.0f, 0.0f }, Color };
       
    // Back face
    pVertex[4]  = { { BBox.m_Min.m_X, BBox.m_Min.m_Y, BBox.m_Min.m_Z }, { 0.0f, 1.0f }, Color };
    pVertex[5]  = { { BBox.m_Min.m_X, BBox.m_Max.m_Y, BBox.m_Min.m_Z }, { 1.0f, 1.0f }, Color };
    pVertex[6]  = { { BBox.m_Max.m_X, BBox.m_Max.m_Y, BBox.m_Min.m_Z }, { 1.0f, 0.0f }, Color };
    pVertex[7]  = { { BBox.m_Max.m_X, BBox.m_Min.m_Y, BBox.m_Min.m_Z }, { 0.0f, 0.0f }, Color };
        
    // Top face
    pVertex[8]  = { { BBox.m_Min.m_X, BBox.m_Max.m_Y, BBox.m_Min.m_Z }, { 0.0f, 1.0f }, Color };
    pVertex[9]  = { { BBox.m_Min.m_X, BBox.m_Max.m_Y, BBox.m_Max.m_Z }, { 1.0f, 1.0f }, Color };
    pVertex[10] = { { BBox.m_Max.m_X, BBox.m_Max.m_Y, BBox.m_Max.m_Z }, { 1.0f, 0.0f }, Color };
    pVertex[11] = { { BBox.m_Max.m_X, BBox.m_Max.m_Y, BBox.m_Min.m_Z }, { 0.0f, 0.0f }, Color };
        
    // Bottom face
    pVertex[12] = { { BBox.m_Min.m_X, BBox.m_Min.m_Y, BBox.m_Min.m_Z }, { 0.0f, 1.0f }, Color };
    pVertex[13] = { { BBox.m_Max.m_X, BBox.m_Min.m_Y, BBox.m_Min.m_Z }, { 1.0f, 1.0f }, Color };
    pVertex[14] = { { BBox.m_Max.m_X, BBox.m_Min.m_Y, BBox.m_Max.m_Z }, { 1.0f, 0.0f }, Color };
    pVertex[15] = { { BBox.m_Min.m_X, BBox.m_Min.m_Y, BBox.m_Max.m_Z }, { 0.0f, 0.0f }, Color };
        
    // Right face
    pVertex[16] = { { BBox.m_Max.m_X, BBox.m_Min.m_Y, BBox.m_Min.m_Z }, { 0.0f, 1.0f }, Color };
    pVertex[17] = { { BBox.m_Max.m_X, BBox.m_Max.m_Y, BBox.m_Min.m_Z }, { 1.0f, 1.0f }, Color };
    pVertex[18] = { { BBox.m_Max.m_X, BBox.m_Max.m_Y, BBox.m_Max.m_Z }, { 1.0f, 0.0f }, Color };
    pVertex[19] = { { BBox.m_Max.m_X, BBox.m_Min.m_Y, BBox.m_Max.m_Z }, { 0.0f, 0.0f }, Color };
        
    // Left face
    pVertex[20] = { { BBox.m_Min.m_X, BBox.m_Min.m_Y, BBox.m_Min.m_Z }, { 0.0f, 1.0f }, Color };
    pVertex[21] = { { BBox.m_Min.m_X, BBox.m_Min.m_Y, BBox.m_Max.m_Z }, { 1.0f, 1.0f }, Color };
    pVertex[22] = { { BBox.m_Min.m_X, BBox.m_Max.m_Y, BBox.m_Max.m_Z }, { 1.0f, 0.0f }, Color };
    pVertex[23] = { { BBox.m_Min.m_X, BBox.m_Max.m_Y, BBox.m_Min.m_Z }, { 0.0f, 0.0f }, Color };
    
    // copy indices    
    pIndex.CopyToFrom( StaticIndex );
}

//------------------------------------------------------------------------------------

void eng_draw::CopyWireCubeData( xbuffer_view<eng_draw::vertex> pVertex, xbuffer_view<u32> pIndex, const xbbox& BBox, const xcolor Color ) noexcept
{
    x_constexprvar xarray<u32,12*2> StaticIndex {{ 1u,5u, 5u,7u, 7u,3u, 3u,1u, 0u,4u, 4u,6u, 6u,2u, 2u,0u, 3u,2u, 7u,6u, 5u,4u, 1u,0u }};

    pVertex[0].setup(  BBox.m_Min.m_X, BBox.m_Min.m_Y, BBox.m_Min.m_Z, Color );
    pVertex[1].setup(  BBox.m_Min.m_X, BBox.m_Min.m_Y, BBox.m_Max.m_Z, Color );
    pVertex[2].setup(  BBox.m_Min.m_X, BBox.m_Max.m_Y, BBox.m_Min.m_Z, Color );
    pVertex[3].setup(  BBox.m_Min.m_X, BBox.m_Max.m_Y, BBox.m_Max.m_Z, Color );
    pVertex[4].setup(  BBox.m_Max.m_X, BBox.m_Min.m_Y, BBox.m_Min.m_Z, Color );
    pVertex[5].setup(  BBox.m_Max.m_X, BBox.m_Min.m_Y, BBox.m_Max.m_Z, Color );
    pVertex[6].setup(  BBox.m_Max.m_X, BBox.m_Max.m_Y, BBox.m_Min.m_Z, Color );
    pVertex[7].setup(  BBox.m_Max.m_X, BBox.m_Max.m_Y, BBox.m_Max.m_Z, Color );

    // copy indices    
    pIndex.CopyToFrom( StaticIndex );
}

//------------------------------------------------------------------------------------
// https://gamedevdaily.io/four-ways-to-create-a-mesh-for-a-sphere-d7956b825db4#.p6zvnjt65
eng_draw::buffers eng_draw::popSolidUVSphere(
    const int                         nRings,
    const int                         nSectors,
    const xcolor                      Color ) noexcept
{
    const auto R = f32{ 1.0f/(f32)(nRings   - 1) };
    const auto S = f32{ 1.0f/(f32)(nSectors - 1) };
    x_constexprvar auto Radius = 0.5f;

    auto Buffers = popBuffers( nRings*nSectors, nRings*nSectors*6 );

    int iVert   = 0;
    int iIndex  = -1;

    //
    // Copy Vertices
    //
    for( int r = 0; r < nRings; ++r ) 
        for( int s = 0; s < nSectors; ++s ) 
        {
            const xradian   r2      { r * R };
            const xradian   s2      { s * S };
            const f32       Sr      = x_Sin( PI  * r2 );
            const f32       y       = x_Sin( -PI_OVER2 + PI * r2 );
            const f32       x       = x_Cos(  PI2 * s2 ) * Sr;
            const f32       z       = x_Sin(  PI2 * s2 ) * Sr;
            auto&           Vertex  = Buffers.m_Vertices[iVert++];

            Vertex.m_UV             = xvector2{ s2.m_Value, 1-r2.m_Value };
            Vertex.m_Position       = xvector3{ x, y, z } * Radius;
            Vertex.m_Color          = Color;
            // normal = xvector3{ x, y, z }
        }

    //
    // Copy Indices
    // 
    for( int r = 0; r < nRings-1; ++r ) 
        for( int s = 0; s < nSectors-1; ++s ) 
        {
            const int curRow    = r     * nSectors;
            const int nextRow   = (r+1) * nSectors;
                
            Buffers.m_Indices[++iIndex] = curRow    + s;
            x_assert( Buffers.m_Vertices.isIndexValid( Buffers.m_Indices[iIndex] ) );

            Buffers.m_Indices[++iIndex] = nextRow   + s; 
            x_assert( Buffers.m_Vertices.isIndexValid( Buffers.m_Indices[iIndex] ) );

            Buffers.m_Indices[++iIndex] = nextRow   + (s+1); 
            x_assert( Buffers.m_Vertices.isIndexValid( Buffers.m_Indices[iIndex] ) );

            Buffers.m_Indices[++iIndex] = curRow    + s; 
            x_assert( Buffers.m_Vertices.isIndexValid( Buffers.m_Indices[iIndex] ) );

            Buffers.m_Indices[++iIndex] = nextRow   + (s+1);
            x_assert( Buffers.m_Vertices.isIndexValid( Buffers.m_Indices[iIndex] ) );

            Buffers.m_Indices[++iIndex] = curRow    + (s+1);
            x_assert( Buffers.m_Vertices.isIndexValid( Buffers.m_Indices[iIndex] ) );
        }

    return Buffers;
}


//-------------------------------------------------------------------------------

void eng_draw::DrawDebugMarker( const xmatrix4& W2C, const xvector3& Pos, xcolor Color ) noexcept
{
    xvector4 ClipPosa = W2C * xvector4( Pos, 1.0f );

    if( ClipPosa.m_W*ClipPosa.m_W < 0.01f ) ClipPosa.m_W = 0.01f;

    xvector3 ClipPos  { ClipPosa.m_X / ClipPosa.m_W,
                        ClipPosa.m_Y / ClipPosa.m_W,
                        ClipPosa.m_Z / ClipPosa.m_W };

    //
    // Project position on the screen
    //
    const int   nVerts  = ClipPosa.m_Z < 0.01f?8:4;
    auto        Buffers = popBuffers( nVerts, 2*(nVerts/4)*6 );

    ClipPos.m_X = x_Max( -0.98f,  ClipPos.m_X );
    ClipPos.m_X = x_Min(  0.98f,  ClipPos.m_X );
    ClipPos.m_Y = x_Max( -0.98f,  ClipPos.m_Y );
    ClipPos.m_Y = x_Min(  0.98f,  ClipPos.m_Y );
//    ClipPos.m_Z = x_Max(  0.001f, ClipPos.m_Z );
//    ClipPos.m_Z = x_Min(  0.98f,  ClipPos.m_Z ); 

    ClipPos.m_Z = 0.001f;
    
    x_constexprvar float size = 0.02f;
    Buffers.m_Vertices[0].setup( ClipPos - xvector3(       0,  -size*2,  0 ), Color );
    Buffers.m_Vertices[1].setup( ClipPos - xvector3(   -size,        0,  0 ), Color );
    Buffers.m_Vertices[2].setup( ClipPos - xvector3(       0,   size*2,  0 ), Color );
    Buffers.m_Vertices[3].setup( ClipPos - xvector3(    size,        0,  0 ), Color );

    Buffers.m_Indices[0] = 0;
    Buffers.m_Indices[1] = 2;
    Buffers.m_Indices[2] = 1;
    Buffers.m_Indices[3] = 2;
    Buffers.m_Indices[4] = 0;
    Buffers.m_Indices[5] = 3;

    Buffers.m_Indices[6]  = 0;
    Buffers.m_Indices[7]  = 1;
    Buffers.m_Indices[8]  = 2;
    Buffers.m_Indices[9]  = 0;
    Buffers.m_Indices[10] = 2;
    Buffers.m_Indices[11] = 3;

    if( nVerts == 8 )
    {
        x_constexprvar float size2 = size/2;
        x_constexprvar xcolor Black{0,0,0,255};
        Buffers.m_Vertices[4].setup( ClipPos - xvector3(       0,  -size2,  0 ), Black );
        Buffers.m_Vertices[5].setup( ClipPos - xvector3(   -size2,      0,  0 ), Black );
        Buffers.m_Vertices[6].setup( ClipPos - xvector3(       0,   size2,  0 ), Black );
        Buffers.m_Vertices[7].setup( ClipPos - xvector3(    size2,      0,  0 ), Black );

        for( int i=0; i<12; i++ )
            Buffers.m_Indices[i+12]  = Buffers.m_Indices[i]+4;
    }

    ClearSampler();
    DrawBufferTriangles( xmatrix4::Identity() );
}

