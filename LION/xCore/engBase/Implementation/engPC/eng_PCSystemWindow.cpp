//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//

#include "x_base.h"
#include <windows.h>
#include "../eng_base.h"
#include "eng_PCSystemWindow.h"

//----------------------------------------------------------------------------------

x_constexprvar xarray<eng_input::keyboard::digital,256> s_OSKeyToEngInputKeyCode
{
    eng_input::keyboard::digital::KEY_NULL,                     // 0
    eng_input::keyboard::digital::KEY_NULL,                     // 1
    eng_input::keyboard::digital::KEY_NULL,                     // 2
    eng_input::keyboard::digital::KEY_NULL,                     // 3
    eng_input::keyboard::digital::KEY_NULL,                     // 4
    eng_input::keyboard::digital::KEY_NULL,                     // 5
    eng_input::keyboard::digital::KEY_NULL,                     // 6
    eng_input::keyboard::digital::KEY_NULL,                     // 7
    eng_input::keyboard::digital::KEY_BACKSPACE,                // 8
    eng_input::keyboard::digital::KEY_TAB,                      // 9
    eng_input::keyboard::digital::KEY_NULL,                     // 10
    eng_input::keyboard::digital::KEY_NULL,                     // 11
    eng_input::keyboard::digital::KEY_NULL,                     // 12
    eng_input::keyboard::digital::KEY_RETURN,                   // 13
    eng_input::keyboard::digital::KEY_NULL,                     // 14
    eng_input::keyboard::digital::KEY_NULL,                     // 15
    eng_input::keyboard::digital::KEY_LSHIFT,                   // 16
    eng_input::keyboard::digital::KEY_LCONTROL,                 // 17
    eng_input::keyboard::digital::KEY_NULL,                     // 18
    eng_input::keyboard::digital::KEY_NULL,                     // 19
    eng_input::keyboard::digital::KEY_NULL,                     // 20
    eng_input::keyboard::digital::KEY_NULL,                     // 21
    eng_input::keyboard::digital::KEY_NULL,                     // 22
    eng_input::keyboard::digital::KEY_NULL,                     // 23
    eng_input::keyboard::digital::KEY_NULL,                     // 24
    eng_input::keyboard::digital::KEY_NULL,                     // 25
    eng_input::keyboard::digital::KEY_NULL,                     // 26
    eng_input::keyboard::digital::KEY_ESCAPE,                   // 27
    eng_input::keyboard::digital::KEY_NULL,                     // 28
    eng_input::keyboard::digital::KEY_NULL,                     // 29
    eng_input::keyboard::digital::KEY_NULL,                     // 30
    eng_input::keyboard::digital::KEY_NULL,                     // 31
    eng_input::keyboard::digital::KEY_SPACE,                    // 32
    eng_input::keyboard::digital::KEY_PAGEUP,                   // 33
    eng_input::keyboard::digital::KEY_PAGEDOWN,                 // 34
    eng_input::keyboard::digital::KEY_END,                      // 35
    eng_input::keyboard::digital::KEY_HOME,                     // 36
    eng_input::keyboard::digital::KEY_LEFT,                     // 37
    eng_input::keyboard::digital::KEY_UP,                       // 38
    eng_input::keyboard::digital::KEY_RIGHT,                    // 39
    eng_input::keyboard::digital::KEY_DOWN,                     // 40
    eng_input::keyboard::digital::KEY_NULL,                     // 41
    eng_input::keyboard::digital::KEY_NULL,                     // 42
    eng_input::keyboard::digital::KEY_PLUS,                     // 43
    eng_input::keyboard::digital::KEY_COMMA,                    // 44
    eng_input::keyboard::digital::KEY_INSERT,                   // 45
    eng_input::keyboard::digital::KEY_DELETE,                   // 46
    eng_input::keyboard::digital::KEY_SLASH,                    // 47
    eng_input::keyboard::digital::KEY_0,                        // 48
    eng_input::keyboard::digital::KEY_1,                        // 49
    eng_input::keyboard::digital::KEY_2,                        // 50
    eng_input::keyboard::digital::KEY_3,                        // 51
    eng_input::keyboard::digital::KEY_4,                        // 52
    eng_input::keyboard::digital::KEY_5,                        // 53
    eng_input::keyboard::digital::KEY_6,                        // 54
    eng_input::keyboard::digital::KEY_7,                        // 55
    eng_input::keyboard::digital::KEY_8,                        // 56
    eng_input::keyboard::digital::KEY_9,                        // 57
    eng_input::keyboard::digital::KEY_SEMICOLON,                // 58
    eng_input::keyboard::digital::KEY_NULL,                     // 59
    eng_input::keyboard::digital::KEY_NULL,                     // 60
    eng_input::keyboard::digital::KEY_NULL,                     // 61
    eng_input::keyboard::digital::KEY_NULL,                     // 62
    eng_input::keyboard::digital::KEY_NULL,                     // 63
    eng_input::keyboard::digital::KEY_NULL,                     // 64
    eng_input::keyboard::digital::KEY_A,                        // 65
    eng_input::keyboard::digital::KEY_B,                        // 66
    eng_input::keyboard::digital::KEY_C,                        // 67
    eng_input::keyboard::digital::KEY_D,                        // 68
    eng_input::keyboard::digital::KEY_E,                        // 69
    eng_input::keyboard::digital::KEY_F,                        // 70
    eng_input::keyboard::digital::KEY_G,                        // 71
    eng_input::keyboard::digital::KEY_H,                        // 72
    eng_input::keyboard::digital::KEY_I,                        // 73
    eng_input::keyboard::digital::KEY_J,                        // 74
    eng_input::keyboard::digital::KEY_K,                        // 75
    eng_input::keyboard::digital::KEY_L,                        // 76
    eng_input::keyboard::digital::KEY_M,                        // 77
    eng_input::keyboard::digital::KEY_N,                        // 78
    eng_input::keyboard::digital::KEY_O,                        // 79
    eng_input::keyboard::digital::KEY_P,                        // 80
    eng_input::keyboard::digital::KEY_Q,                        // 81
    eng_input::keyboard::digital::KEY_R,                        // 82
    eng_input::keyboard::digital::KEY_S,                        // 83
    eng_input::keyboard::digital::KEY_T,                        // 84
    eng_input::keyboard::digital::KEY_U,                        // 85
    eng_input::keyboard::digital::KEY_V,                        // 86
    eng_input::keyboard::digital::KEY_W,                        // 87
    eng_input::keyboard::digital::KEY_X,                        // 88
    eng_input::keyboard::digital::KEY_Y,                        // 89
    eng_input::keyboard::digital::KEY_Z,                        // 90
    eng_input::keyboard::digital::KEY_LBRACKET,                 // 91
    eng_input::keyboard::digital::KEY_BACKSLASH,                // 92
    eng_input::keyboard::digital::KEY_RBRACKET,                 // 93
    eng_input::keyboard::digital::KEY_NULL,                     // 94
    eng_input::keyboard::digital::KEY_NULL,                     // 95
    eng_input::keyboard::digital::KEY_0,                        // 96
    eng_input::keyboard::digital::KEY_1,                        // 97
    eng_input::keyboard::digital::KEY_2,                        // 98
    eng_input::keyboard::digital::KEY_3,                        // 99
    eng_input::keyboard::digital::KEY_4,                        // 100
    eng_input::keyboard::digital::KEY_5,                        // 101
    eng_input::keyboard::digital::KEY_6,                        // 102
    eng_input::keyboard::digital::KEY_7,                        // 103
    eng_input::keyboard::digital::KEY_8,                        // 104
    eng_input::keyboard::digital::KEY_9,                        // 105
    eng_input::keyboard::digital::KEY_MULTIPLY,                 // 106
    eng_input::keyboard::digital::KEY_PLUS,                     // 107
    eng_input::keyboard::digital::KEY_NULL,                     // 108
    eng_input::keyboard::digital::KEY_MINUS,                    // 109
    eng_input::keyboard::digital::KEY_PERIOD,                   // 110
    eng_input::keyboard::digital::KEY_SLASH,                    // 111
    eng_input::keyboard::digital::KEY_P,                        // 112
    eng_input::keyboard::digital::KEY_Q,                        // 113
    eng_input::keyboard::digital::KEY_R,                        // 114
    eng_input::keyboard::digital::KEY_S,                        // 115
    eng_input::keyboard::digital::KEY_T,                        // 116
    eng_input::keyboard::digital::KEY_U,                        // 117
    eng_input::keyboard::digital::KEY_V,                        // 118
    eng_input::keyboard::digital::KEY_W,                        // 119
    eng_input::keyboard::digital::KEY_X,                        // 120
    eng_input::keyboard::digital::KEY_Y,                        // 121
    eng_input::keyboard::digital::KEY_Z,                        // 122
    eng_input::keyboard::digital::KEY_LBRACKET,                 // 123
    eng_input::keyboard::digital::KEY_NULL,                     // 124
    eng_input::keyboard::digital::KEY_RBRACKET,                 // 125
    eng_input::keyboard::digital::KEY_TILDE,                    // 126
    eng_input::keyboard::digital::KEY_BACKSPACE,                // 127
    eng_input::keyboard::digital::KEY_NULL,                     // 128
    eng_input::keyboard::digital::KEY_NULL,                     // 129
    eng_input::keyboard::digital::KEY_NULL,                     // 130
    eng_input::keyboard::digital::KEY_NULL,                     // 131
    eng_input::keyboard::digital::KEY_NULL,                     // 132
    eng_input::keyboard::digital::KEY_NULL,                     // 133
    eng_input::keyboard::digital::KEY_NULL,                     // 134
    eng_input::keyboard::digital::KEY_NULL,                     // 135
    eng_input::keyboard::digital::KEY_NULL,                     // 136
    eng_input::keyboard::digital::KEY_NULL,                     // 137
    eng_input::keyboard::digital::KEY_NULL,                     // 138
    eng_input::keyboard::digital::KEY_NULL,                     // 139
    eng_input::keyboard::digital::KEY_NULL,                     // 140
    eng_input::keyboard::digital::KEY_NULL,                     // 141
    eng_input::keyboard::digital::KEY_NULL,                     // 142
    eng_input::keyboard::digital::KEY_NULL,                     // 143
    eng_input::keyboard::digital::KEY_NULL,                     // 144
    eng_input::keyboard::digital::KEY_NULL,                     // 145
    eng_input::keyboard::digital::KEY_NULL,                     // 146
    eng_input::keyboard::digital::KEY_NULL,                     // 147
    eng_input::keyboard::digital::KEY_NULL,                     // 148
    eng_input::keyboard::digital::KEY_NULL,                     // 149
    eng_input::keyboard::digital::KEY_NULL,                     // 150
    eng_input::keyboard::digital::KEY_NULL,                     // 151
    eng_input::keyboard::digital::KEY_NULL,                     // 152
    eng_input::keyboard::digital::KEY_NULL,                     // 153
    eng_input::keyboard::digital::KEY_NULL,                     // 154
    eng_input::keyboard::digital::KEY_NULL,                     // 155
    eng_input::keyboard::digital::KEY_NULL,                     // 156
    eng_input::keyboard::digital::KEY_NULL,                     // 157
    eng_input::keyboard::digital::KEY_NULL,                     // 158
    eng_input::keyboard::digital::KEY_NULL,                     // 159
    eng_input::keyboard::digital::KEY_NULL,                     // 160
    eng_input::keyboard::digital::KEY_NULL,                     // 161
    eng_input::keyboard::digital::KEY_NULL,                     // 162
    eng_input::keyboard::digital::KEY_NULL,                     // 163
    eng_input::keyboard::digital::KEY_NULL,                     // 164
    eng_input::keyboard::digital::KEY_NULL,                     // 165
    eng_input::keyboard::digital::KEY_NULL,                     // 166
    eng_input::keyboard::digital::KEY_NULL,                     // 167
    eng_input::keyboard::digital::KEY_NULL,                     // 168
    eng_input::keyboard::digital::KEY_NULL,                     // 169
    eng_input::keyboard::digital::KEY_NULL,                     // 170
    eng_input::keyboard::digital::KEY_NULL,                     // 171
    eng_input::keyboard::digital::KEY_NULL,                     // 172
    eng_input::keyboard::digital::KEY_NULL,                     // 173
    eng_input::keyboard::digital::KEY_NULL,                     // 174
    eng_input::keyboard::digital::KEY_NULL,                     // 175
    eng_input::keyboard::digital::KEY_NULL,                     // 176
    eng_input::keyboard::digital::KEY_NULL,                     // 177
    eng_input::keyboard::digital::KEY_NULL,                     // 178
    eng_input::keyboard::digital::KEY_NULL,                     // 179
    eng_input::keyboard::digital::KEY_NULL,                     // 180
    eng_input::keyboard::digital::KEY_NULL,                     // 181
    eng_input::keyboard::digital::KEY_NULL,                     // 182
    eng_input::keyboard::digital::KEY_NULL,                     // 183
    eng_input::keyboard::digital::KEY_NULL,                     // 184
    eng_input::keyboard::digital::KEY_NULL,                     // 185
    eng_input::keyboard::digital::KEY_NULL,                     // 186
    eng_input::keyboard::digital::KEY_NULL,                     // 187
    eng_input::keyboard::digital::KEY_NULL,                     // 188
    eng_input::keyboard::digital::KEY_COMMA,                    // 189
    eng_input::keyboard::digital::KEY_PERIOD,                   // 190
    eng_input::keyboard::digital::KEY_SLASH,                    // 191
    eng_input::keyboard::digital::KEY_TILDE,                    // 192
    eng_input::keyboard::digital::KEY_NULL,                     // 193
    eng_input::keyboard::digital::KEY_NULL,                     // 194
    eng_input::keyboard::digital::KEY_NULL,                     // 195
    eng_input::keyboard::digital::KEY_NULL,                     // 196
    eng_input::keyboard::digital::KEY_NULL,                     // 197
    eng_input::keyboard::digital::KEY_NULL,                     // 198
    eng_input::keyboard::digital::KEY_NULL,                     // 199
    eng_input::keyboard::digital::KEY_NULL,                     // 200
    eng_input::keyboard::digital::KEY_NULL,                     // 201
    eng_input::keyboard::digital::KEY_NULL,                     // 202
    eng_input::keyboard::digital::KEY_NULL,                     // 203
    eng_input::keyboard::digital::KEY_NULL,                     // 204
    eng_input::keyboard::digital::KEY_NULL,                     // 205
    eng_input::keyboard::digital::KEY_NULL,                     // 206
    eng_input::keyboard::digital::KEY_NULL,                     // 207
    eng_input::keyboard::digital::KEY_NULL,                     // 208
    eng_input::keyboard::digital::KEY_NULL,                     // 209
    eng_input::keyboard::digital::KEY_NULL,                     // 210
    eng_input::keyboard::digital::KEY_NULL,                     // 211
    eng_input::keyboard::digital::KEY_NULL,                     // 212
    eng_input::keyboard::digital::KEY_NULL,                     // 213
    eng_input::keyboard::digital::KEY_NULL,                     // 214
    eng_input::keyboard::digital::KEY_NULL,                     // 215
    eng_input::keyboard::digital::KEY_NULL,                     // 216
    eng_input::keyboard::digital::KEY_NULL,                     // 217
    eng_input::keyboard::digital::KEY_NULL,                     // 218
    eng_input::keyboard::digital::KEY_NULL,                     // 219
    eng_input::keyboard::digital::KEY_NULL,                     // 220
    eng_input::keyboard::digital::KEY_NULL,                     // 221
    eng_input::keyboard::digital::KEY_NULL,                     // 222
    eng_input::keyboard::digital::KEY_NULL,                     // 223
    eng_input::keyboard::digital::KEY_NULL,                     // 224
    eng_input::keyboard::digital::KEY_NULL,                     // 225
    eng_input::keyboard::digital::KEY_NULL,                     // 226
    eng_input::keyboard::digital::KEY_NULL,                     // 227
    eng_input::keyboard::digital::KEY_NULL,                     // 228
    eng_input::keyboard::digital::KEY_NULL,                     // 229
    eng_input::keyboard::digital::KEY_NULL,                     // 230
    eng_input::keyboard::digital::KEY_NULL,                     // 231
    eng_input::keyboard::digital::KEY_NULL,                     // 232
    eng_input::keyboard::digital::KEY_NULL,                     // 233
    eng_input::keyboard::digital::KEY_NULL,                     // 234
    eng_input::keyboard::digital::KEY_NULL,                     // 235
    eng_input::keyboard::digital::KEY_NULL,                     // 236
    eng_input::keyboard::digital::KEY_NULL,                     // 237
    eng_input::keyboard::digital::KEY_NULL,                     // 238
    eng_input::keyboard::digital::KEY_NULL,                     // 239
    eng_input::keyboard::digital::KEY_NULL,                     // 240
    eng_input::keyboard::digital::KEY_NULL,                     // 241
    eng_input::keyboard::digital::KEY_NULL,                     // 242
    eng_input::keyboard::digital::KEY_NULL,                     // 243
    eng_input::keyboard::digital::KEY_NULL,                     // 244
    eng_input::keyboard::digital::KEY_NULL,                     // 245
    eng_input::keyboard::digital::KEY_NULL,                     // 246
    eng_input::keyboard::digital::KEY_NULL,                     // 247
    eng_input::keyboard::digital::KEY_NULL,                     // 248
    eng_input::keyboard::digital::KEY_NULL,                     // 249
    eng_input::keyboard::digital::KEY_NULL,                     // 250
    eng_input::keyboard::digital::KEY_NULL,                     // 251
    eng_input::keyboard::digital::KEY_NULL,                     // 252
    eng_input::keyboard::digital::KEY_NULL,                     // 253
    eng_input::keyboard::digital::KEY_NULL,                     // 254
    eng_input::keyboard::digital::KEY_NULL                      // 255
};

static const xarray<int,256> s_EngInputKeyCodeToOS = [&]
{
    xarray<int,256> EngInputKeyCodeToOS;
    EngInputKeyCodeToOS.MemSet(0);
    for( int i = 0; i < s_OSKeyToEngInputKeyCode.getCount(); i++ )
    {
        EngInputKeyCodeToOS[ static_cast<int>( s_OSKeyToEngInputKeyCode[i] ) ] = i;
    }
    EngInputKeyCodeToOS[0] = 0;

    return EngInputKeyCodeToOS;

} ();

//----------------------------------------------------------------------------------
eng_input::keyboard::digital OSKeyToEngKeyCode( int Key )
{
    return s_OSKeyToEngInputKeyCode[ Key ];
}

//----------------------------------------------------------------------------------
int _EngInputKeyCodeToOS( eng_input::keyboard::digital Key )
{
    return s_EngInputKeyCodeToOS[ Key ];
}

//----------------------------------------------------------------------------------
// Process Window Message Callbacks
LRESULT CALLBACK WindowProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    eng_pc_system_window* pWin = nullptr;

    switch( uMsg )
    {
    case WM_CLOSE:
        PostMessage (hWnd, WM_QUIT, 0, 0);
        return 0;
    case WM_PAINT:
        ValidateRect( hWnd, NULL );
        break;
    case WM_MOUSEMOVE:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            const int x = static_cast<int>(lParam & 0xffff);
            const int y = static_cast<int>(lParam >> 16);

            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_X = x - pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_X;
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_Y = y - pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_Y;

            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_X = static_cast<f32>(x);
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_Y = static_cast<f32>(y);

            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::WHEEL_REL].m_X = 0;
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::WHEEL_REL].m_Y = 0;
        }
        break;
    case WM_MBUTTONDOWN:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            const int x = static_cast<int>(lParam & 0xffff);
            const int y = static_cast<int>(lParam >> 16);

            pWin->m_Mouse.m_ButtonIsDown[eng_input::mouse::digital::BTN_MIDDLE] = 1;
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_X = static_cast<f32>(x);
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_Y = static_cast<f32>(y);

            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_X = 0;
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_Y = 0;
        }
        break;
    case WM_MBUTTONUP:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            pWin->m_Mouse.m_ButtonIsDown[eng_input::mouse::digital::BTN_MIDDLE] = 0;
            pWin->m_Mouse.m_ButtonWasDown[ pWin->m_Mouse.m_ButtonIndex ][ eng_input::mouse::digital::BTN_MIDDLE ] = 1;
        }
        break;
    case WM_LBUTTONDOWN:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            const int x = static_cast<int>(lParam & 0xffff);
            const int y = static_cast<int>(lParam >> 16);

            pWin->m_Mouse.m_ButtonIsDown[eng_input::mouse::digital::BTN_LEFT] = 1;
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_X = static_cast<f32>(x);
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_Y = static_cast<f32>(y);

            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_X = 0;
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_Y = 0;
        }
        break;
    case WM_LBUTTONUP:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            pWin->m_Mouse.m_ButtonIsDown[eng_input::mouse::digital::BTN_LEFT] = 0;
            pWin->m_Mouse.m_ButtonWasDown[pWin->m_Mouse.m_ButtonIndex][eng_input::mouse::digital::BTN_LEFT] = 1;
        }
        break;
    case WM_RBUTTONDOWN:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            const int x = static_cast<int>(lParam & 0xffff);
            const int y = static_cast<int>(lParam >> 16);

            pWin->m_Mouse.m_ButtonIsDown[eng_input::mouse::digital::BTN_RIGHT] = 1;
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_X = static_cast<f32>(x);
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_ABS].m_Y = static_cast<f32>(y);

            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_X = 0;
            pWin->m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_Y = 0;
        }
        break;
    case WM_RBUTTONUP:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            pWin->m_Mouse.m_ButtonIsDown[eng_input::mouse::digital::BTN_RIGHT] = 0;
            pWin->m_Mouse.m_ButtonWasDown[pWin->m_Mouse.m_ButtonIndex][eng_input::mouse::digital::BTN_RIGHT] = 1;
        }
        break;
    case WM_KEYDOWN:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            const auto Code = OSKeyToEngKeyCode( static_cast<int>(wParam) );
            if ( Code >= eng_input::keyboard::digital::KEY_NULL && Code < eng_input::keyboard::digital::ENUM_COUNT )
            {
                pWin->m_Keyboard.m_KeyIsDown[Code] = true;

                pWin->m_Keyboard.m_KeyIsDown[ eng_input::keyboard::digital::KEY_LCONTROL ] = (GetKeyState(VK_CONTROL) & 0x8000) != 0;
                pWin->m_Keyboard.m_KeyIsDown[ eng_input::keyboard::digital::KEY_LSHIFT   ] = (GetKeyState(VK_SHIFT)   & 0x8000) != 0;
                pWin->m_Keyboard.m_KeyIsDown[ eng_input::keyboard::digital::KEY_LALT     ] = (GetKeyState(VK_MENU)    & 0x8000) != 0;

                // Get the ascii character
                BYTE kbs[256]={0};
                GetKeyboardState(kbs);
                if(kbs[VK_CONTROL] & 0x00000080)
                {
                    kbs[VK_CONTROL] &= 0x0000007f;
                    ::ToAscii( (u32)wParam, ::MapVirtualKey( (u32)wParam, MAPVK_VK_TO_VSC), kbs,  &pWin->m_Keyboard.m_MostRecentChar, 0);
                    kbs[VK_CONTROL] |= 0x00000080;
                }
                else
                {
                    ::ToAscii( (u32)wParam, ::MapVirtualKey( (u32)wParam, MAPVK_VK_TO_VSC), kbs, &pWin->m_Keyboard.m_MostRecentChar, 0);
                }
            }
        }
        break;
    case WM_KEYUP:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            const auto Code = OSKeyToEngKeyCode( static_cast<int>(wParam) );
            if ( Code >= eng_input::keyboard::digital::KEY_NULL && Code < eng_input::keyboard::digital::ENUM_COUNT )
            {
                pWin->m_Keyboard.m_KeyIsDown[Code] = false;
                pWin->m_Keyboard.m_KeyIsDown[ eng_input::keyboard::digital::KEY_LCONTROL ] = (GetKeyState(VK_CONTROL) & 0x8000) != 0;
                pWin->m_Keyboard.m_KeyIsDown[ eng_input::keyboard::digital::KEY_LSHIFT   ] = (GetKeyState(VK_SHIFT)   & 0x8000) != 0;
                pWin->m_Keyboard.m_KeyIsDown[ eng_input::keyboard::digital::KEY_LALT     ] = (GetKeyState(VK_MENU)    & 0x8000) != 0;

                pWin->m_Keyboard.m_KeyWasDown[pWin->m_Keyboard.m_KeyWasDownIndex][Code] = 1;
            }
        }
        break;
    case WM_SIZE:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            // Resize the application to the new window size, except when
            // it was minimized. Vulkan doesn't support images or swapchains
            // with width=0 and height=0.
            if (wParam != SIZE_MINIMIZED) 
            {
                pWin->m_Height = static_cast<int>((lParam & 0xffff0000) >> 16);
                pWin->m_Width  = static_cast<int>(lParam & 0xffff);  

                pWin->Resize( pWin->m_Width, pWin->m_Height ); 
            }
        }
        break;
    case WM_MOUSEWHEEL:
        if( (pWin = reinterpret_cast<eng_pc_system_window*>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) )) )
        {
            auto fwKeys = GET_KEYSTATE_WPARAM(wParam);
            auto zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
            f32& V = pWin->m_Mouse.m_Analog[eng_input::mouse::analog::WHEEL_REL].m_X;
            V = (zDelta*100.0f)/X_S16_MAX;
        }
        break;
    case WM_SYSCOMMAND:
        if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
            return 0;
        break;

    }    // End switch

    // Pass Unhandled Messages To DefWindowProc
    return DefWindowProc (hWnd, uMsg, wParam, lParam);
}

//--------------------------------------------------------------------------------------------

void eng_pc_system_window::UpdateInput( void )
{
    m_Mouse.m_ButtonIndex = 1 - m_Mouse.m_ButtonIndex;
    for ( s32 i = 0; i < static_cast<s32>(eng_input::mouse::digital::ENUM_COUNT); i++ )
    {
        m_Mouse.m_ButtonWasDown[m_Mouse.m_ButtonIndex][i] = 0;
    }

    m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_X = 0;
    m_Mouse.m_Analog[eng_input::mouse::analog::POS_REL].m_Y = 0;
    m_Mouse.m_Analog[eng_input::mouse::analog::WHEEL_REL].m_X = 0;
    m_Mouse.m_Analog[eng_input::mouse::analog::WHEEL_REL].m_Y = 0;

    m_Keyboard.m_KeyWasDownIndex = 1 - m_Keyboard.m_KeyWasDownIndex;
    m_Keyboard.m_MostRecentChar  = 0;
    for ( s32 i = 0; i < static_cast<s32>(eng_input::keyboard::digital::ENUM_COUNT); i++ )
    {
        m_Keyboard.m_KeyWasDown[m_Keyboard.m_KeyWasDownIndex][i] = 0;
    }
}


//--------------------------------------------------------------------------------------------

static eng_window::err CreateWindowClass( HINSTANCE hinstance, WNDPROC wndproc )
{
    WNDCLASSEX wndClass;
    wndClass.cbSize         = sizeof(WNDCLASSEX);
    wndClass.style          = CS_HREDRAW | CS_VREDRAW;
    wndClass.lpfnWndProc    = wndproc;
    wndClass.cbClsExtra     = 0;
    wndClass.cbWndExtra     = 0;
    wndClass.hInstance      = hinstance;
    wndClass.hIcon          = LoadIcon(NULL, IDI_APPLICATION);
    wndClass.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wndClass.hbrBackground  = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wndClass.lpszMenuName   = NULL;
    wndClass.lpszClassName  = TEXT("LIONClass");
    wndClass.hIconSm        = LoadIcon(NULL, IDI_WINLOGO);

    if (!RegisterClassEx(&wndClass))
        return x_error_code( eng_window::errors, ERR_SYSTEM_WINDOW, "Could not register window class!" );

    return x_error_ok();
}

//--------------------------------------------------------------------------------------------

static eng_window::err CreateSytemWindow(
    HINSTANCE   hInstance, 
    HWND&       hWnd, 
    bool        bFullScreen,
    int         width,
    int         height )
{
    //
    // Get Resolution
    //
    const int screenWidth  = GetSystemMetrics(SM_CXSCREEN);
    const int screenHeight = GetSystemMetrics(SM_CYSCREEN);

    if( bFullScreen )
    {
        DEVMODE dmScreenSettings;
        memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
        dmScreenSettings.dmSize         = sizeof(dmScreenSettings);
        dmScreenSettings.dmPelsWidth    = screenWidth;
        dmScreenSettings.dmPelsHeight   = screenHeight;
        dmScreenSettings.dmBitsPerPel   = 32;
        dmScreenSettings.dmFields       = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

        if( (width != screenWidth) && (height != screenHeight) )
        {
            if( ChangeDisplaySettings( &dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL )
                return x_error_code( eng_window::errors, ERR_FULLSCREEN_NO_SUPPORTED, "Fullscreen Mode not supported!" );
        }
    }

    //
    // Compute windows flags
    //
    const DWORD dwExStyle = bFullScreen ? WS_EX_APPWINDOW : WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
    const DWORD dwStyle   = bFullScreen ? WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN : WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

    //
    // Determine window rectangle
    //
    RECT windowRect;
    if( bFullScreen )
    {
        windowRect.left     = (long)0;
        windowRect.right    = (long)screenWidth;
        windowRect.top      = (long)0;
        windowRect.bottom   = (long)screenHeight;
    }
    else
    {
        windowRect.left     = (long)screenWidth / 2 - width / 2;
        windowRect.right    = (long)width;
        windowRect.top      = (long)screenHeight / 2 - height / 2;
        windowRect.bottom   = (long)height;
    }

    AdjustWindowRectEx( &windowRect, dwStyle, FALSE, dwExStyle );

    //
    // Create Window
    //
    hWnd = CreateWindowEx(0,
        TEXT("LIONClass"),
        TEXT("LION"),
        dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
        windowRect.left,
        windowRect.top,
        windowRect.right,
        windowRect.bottom,
        NULL,
        NULL,
        hInstance,
        NULL);

    if (!hWnd) 
        return x_error_code( eng_window::errors, ERR_SYSTEM_WINDOW, "Fail to create a window!" );

    ShowWindow( hWnd, SW_SHOW );
    SetForegroundWindow( hWnd );
    SetFocus( hWnd );

    return x_error_ok();
}

//--------------------------------------------------------------------------------------------

eng_window::err eng_pc_system_window::CreateSytemWindow( const eng_window::setup& Setup, void* pDriverWindow ) noexcept
{
    eng_window::err Error;

    Error = CreateWindowClass( (HINSTANCE)Setup.m_pInstance, WindowProc );
    if( Error ) 
        return Error;
             
    Error = ::CreateSytemWindow( (HINSTANCE)Setup.m_pInstance, m_hWindow, Setup.m_bFullScreen, Setup.m_Width, Setup.m_Height );                
    if( Error ) 
        return Error;

    if( m_hWindow ) SetWindowLongPtr( m_hWindow, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this) );

    m_Width             = Setup.m_Width;
    m_Height            = Setup.m_Height;
    m_pDriverWindow     = pDriverWindow;

//    SetWindowLongPtr( m_hWindow, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this) );

    return x_error_ok( );
}



