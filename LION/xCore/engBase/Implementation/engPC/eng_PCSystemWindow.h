//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_pc_system_window 
{
public:

    struct keyboard final : eng_input::keyboard
    {
        using full_keyboard = xarray< bool, static_cast<xuptr>(eng_input::keyboard::digital::ENUM_COUNT) >;

        virtual         bool       isPressedGeneric           ( s32 GadgetID ) const override
        {
            return m_KeyIsDown[GadgetID];
        }

        virtual         bool       wasPressedGeneric          ( s32 GadgetID ) const override
        {
            return m_KeyWasDown[m_KeyWasDownIndex][GadgetID];
        }

        virtual         xvector3d  getValueGeneric            ( s32 GadgetID ) const override
        {
            return xvector3d(0);
        }

        virtual         int        getLatestChar             ( void ) const noexcept override
        {
            return m_MostRecentChar;
        }

        int                                     m_KeyWasDownIndex   {0};
        full_keyboard                           m_KeyIsDown         {false};
        xarray<full_keyboard,2>                 m_KeyWasDown        {false};
        WORD                                    m_MostRecentChar    {0};
    };

    struct mouse final : eng_input::mouse
    {
        using full_digital_mouse = xarray< bool,     static_cast<xuptr>(eng_input::mouse::digital::ENUM_COUNT) >;
        using full_analog_mouse  = xarray< xvector2, static_cast<xuptr>(eng_input::mouse::analog::ENUM_COUNT)  >;


        virtual         bool       isPressedGeneric           ( s32 GadgetID ) const override
        {
            return m_ButtonIsDown[GadgetID];
        }

        virtual         bool       wasPressedGeneric          ( s32 GadgetID ) const override
        {
            return m_ButtonWasDown[m_ButtonIndex][GadgetID];
        }

        virtual         xvector3d  getValueGeneric            ( s32 GadgetID ) const override 
        {
            return { m_Analog[GadgetID].m_X, m_Analog[GadgetID].m_Y, 0 };
        }


        int                                     m_ButtonIndex       {0};
        full_digital_mouse                      m_ButtonIsDown      {false};
        xarray<full_digital_mouse,2>            m_ButtonWasDown     {false};
        full_analog_mouse                       m_Analog            {};
    };

    constexpr   HWND                    getWindowHandle     ( void ) const                                                      noexcept { return m_hWindow; }

protected:

                eng_window::err         CreateSytemWindow   ( const eng_window::setup& Setup, void* pDriverWindow )             noexcept;
    virtual     void                    Resize              ( const int Width, const int Height )                               noexcept = 0;
                void                    UpdateInput         (void);

protected:

    HWND                m_hWindow                       {0};
    keyboard            m_Keyboard                      {};
    mouse               m_Mouse                         {};
    int                 m_Width                         {0};
    int                 m_Height                        {0};
    void*               m_pDriverWindow                 {nullptr};

protected:

    friend LRESULT CALLBACK WindowProc (HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
};

