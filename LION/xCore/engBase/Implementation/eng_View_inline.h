//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//-------------------------------------------------------------------------------

inline
eng_view::eng_view( void ) noexcept
{
    // Reset all the flags
    m_StateFlags.setDefaults();
    //NOTE! The setting of the above flags also causes infinite clipping to be used!
    //So, infinite clipping ON is the default. You must turn it off, if you want it off.


    // Set the Clip matrix
    setNearZ    ( 0.1f );
    setFarZ     ( 100 );
    setFov      ( PI * xradian{ 0.25f } );//x_Radian(95) );
    setAspect   ( 1 );

    // Set the view matrix
    LookAt      ( xvector3(0,0, 30), xvector3(0,0,0) ); 
}

//-------------------------------------------------------------------------------
inline
void eng_view::UpdatedView( void ) noexcept 
{
    x_FlagOn( m_StateFlags.m_Flags, 
        state_flags::MASK_FLAGS_W2V |
        state_flags::MASK_FLAGS_W2C |
        state_flags::MASK_FLAGS_W2S |
        state_flags::MASK_FLAGS_V2C |
        state_flags::MASK_FLAGS_V2S |
        state_flags::MASK_FLAGS_C2W |
        state_flags::MASK_FLAGS_C2V |
        state_flags::MASK_FLAGS_PIXELBASED2D );
}

//-------------------------------------------------------------------------------
inline
void eng_view::UpdatedClip( void ) noexcept 
{
    x_FlagOn( m_StateFlags.m_Flags, 
        state_flags::MASK_FLAGS_W2C |
        state_flags::MASK_FLAGS_W2S |
        state_flags::MASK_FLAGS_V2C |
        state_flags::MASK_FLAGS_V2S |
        state_flags::MASK_FLAGS_C2W |
        state_flags::MASK_FLAGS_C2V );
}

//-------------------------------------------------------------------------------
inline
void eng_view::UpdatedScreen( void ) noexcept 
{
    x_FlagOn( m_StateFlags.m_Flags,  
        state_flags::MASK_FLAGS_W2C |
        state_flags::MASK_FLAGS_W2S |
        state_flags::MASK_FLAGS_V2C |
        state_flags::MASK_FLAGS_V2S |
        state_flags::MASK_FLAGS_C2W |
        state_flags::MASK_FLAGS_C2V |
        state_flags::MASK_FLAGS_C2S |
        state_flags::MASK_FLAGS_PIXELBASED2D );

}

//-------------------------------------------------------------------------------
inline
const xmatrix4& eng_view::getW2V( void ) noexcept
{
    if( m_StateFlags.m_FLAGS_W2V )
    {
        m_StateFlags.m_FLAGS_W2V = false;

        m_W2V    = m_V2W;
        m_W2V.InvertSRT();
    }
    return m_W2V;
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getV2CScales ( const bool bInfiniteClipping ) noexcept
{
    xvector3      Scales;
    const xirect& ViewPort = getViewport();

#ifdef _ENG_TARGET_DIRECTX
    if( x_FlagIsOn(m_Flags,FLAGS_USE_FOV) )
    {
        //Field-of-view based projection matrix
        f32 HalfVPHeight = (ViewPort.getHeight() + 1) * 0.5f;
        f32 HalfVPWidth  = (ViewPort.getWidth()  + 1) * 0.5f;
        f32 Distance     = HalfVPWidth / x_Tan( m_Fov *0.5f );
        f32 YFov         = x_ATan( HalfVPHeight / Distance ) * 2.0f;
        f32 XFov         = x_ATan(m_Aspect * x_Tan(m_Fov * 0.5f)) * 2.0f;

        // computing the fovx from yfov
        Scales.m_X = (f32)( 1.0f / x_Tan( XFov*0.5f ));
        Scales.m_Y = (f32)( 1.0f / x_Tan( YFov*0.5f ));

        if( bInfiniteClipping ) Scales.m_Z = -1;
        else                    Scales.m_Z = -m_FarZ/( m_FarZ - m_NearZ );
    }
    else
    {
        //custom frustum
        //(Note, other parts of the proj matrix change as well, not just diagonal. See getV2C)

        Scales.m_X = (f32)( 2.0f*m_NearZ / (m_FrustumRight - m_FrustumLeft) );
        Scales.m_Y = (f32)( 2.0f*m_NearZ / (m_FrustumTop - m_FrustumBottom) );

        if( bInfiniteClipping ) Scales.m_Z = -1;
        else                    Scales.m_Z = -m_FarZ/( m_FarZ - m_NearZ );
    }
#elif defined(_ENG_TARGET_3DS)
    if( x_FlagIsOn(m_Flags,FLAGS_USE_FOV) )
    {
        f32 Aspect = ((f32)ViewPort.getWidth()) / (f32)(ViewPort.getHeight());
        const f32 Angle = m_Fov * 0.5f;

        const f32 Cot = 1.0f / x_Tan(Angle);

        Scales.m_X = Cot / Aspect;
        Scales.m_Y = Cot;
        Scales.m_Z = m_FarZ / ( m_FarZ - m_NearZ);
    }
    else
    {
        //custom frustum
        //(Note, other parts of the proj matrix change as well, not just diagonal. See getV2C)

        Scales.m_X = (f32)( 2.0f*m_NearZ / (m_FrustumRight - m_FrustumLeft) );
        Scales.m_Y = (f32)( 2.0f*m_NearZ / (m_FrustumTop - m_FrustumBottom) );

        Scales.m_Z = m_FarZ/( m_FarZ - m_NearZ );
    }
#else //elif defined(_ENG_TARGET_OPENGL)
    if( m_StateFlags.m_FLAGS_USE_FOV )
    {
        //Field-of-view based projection matrix
        f32         HalfVPHeight = (ViewPort.getHeight() + 1) * 0.5f;
        f32         HalfVPWidth  = (ViewPort.getWidth()  + 1) * 0.5f;
        f32         Distance     = HalfVPWidth / x_Tan( m_Fov * xradian{ 0.5f } );
        xradian     YFov         = x_ATan( HalfVPHeight / Distance ) * xradian{ 2.0f };
        xradian     XFov         = x_ATan( m_Aspect * x_Tan( m_Fov * xradian{ 0.5f } ) ) * xradian{ 2.0f };
        
        // computing the fovx from yfov
        Scales.m_X = (f32)( 1.0f / x_Tan( XFov * xradian{ 0.5f } ));
        Scales.m_Y = (f32)( 1.0f / x_Tan( YFov * xradian{ 0.5f } ));
        
        if( bInfiniteClipping ) Scales.m_Z = -1;
        else                    Scales.m_Z = -(m_FarZ+m_NearZ)/( m_FarZ - m_NearZ );
    }
    else
    {
        //custom frustum
        //(Note, other parts of the proj matrix change as well, not just diagonal. See getV2C)
        
        Scales.m_X = (f32)( 2.0f*m_NearZ / (m_FrustumRight - m_FrustumLeft) );
        Scales.m_Y = (f32)( 2.0f*m_NearZ / (m_FrustumTop - m_FrustumBottom) );
        
        if( bInfiniteClipping ) Scales.m_Z = -1;
        else                    Scales.m_Z = -(m_FarZ+m_NearZ)/( m_FarZ - m_NearZ );
    }
    #ifdef _ENG_TARGET_VULKAN 
        Scales.m_Y = -Scales.m_Y;
    #endif
#endif
    return Scales;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getV2C( void ) noexcept
{
    if( m_StateFlags.m_FLAGS_V2C )
    {
        m_StateFlags.m_FLAGS_V2C = false;
        m_V2C.setZero();
        m_V2C.setScale( getV2CScales( m_StateFlags.m_FLAGS_INF_CLIP ) );
        if( !m_StateFlags.m_FLAGS_USE_FOV )
        {
            //if using a custom frustum, need to take possible off-centerness into account
            m_V2C(0,2) = (m_FrustumRight + m_FrustumLeft)/(m_FrustumRight - m_FrustumLeft);
            m_V2C(1,2) = (m_FrustumTop + m_FrustumBottom)/(m_FrustumTop - m_FrustumBottom);
        }

#ifdef _ENG_TARGET_DIRECTX
        // now finish the rest
        m_V2C(2,3) = m_V2C(2,2) * m_NearZ;
        m_V2C(3,2) = -1;
#elif defined(_ENG_TARGET_3DS)
        m_V2C(2,3) = m_FarZ * m_NearZ / (m_FarZ - m_NearZ);
        m_V2C(3,2) = -1;
#else
        m_V2C(2,3) = -(2 * m_FarZ * m_NearZ) / (m_FarZ - m_NearZ);
        m_V2C(3,2) = -1;
#endif
    }

    return m_V2C;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getC2S( void ) noexcept
{
    if( m_StateFlags.m_FLAGS_C2S )
    {
        m_StateFlags.m_FLAGS_C2S = false;
        const xirect&   Viewport    = getViewport();
        const f32       W           = Viewport.getWidth()  * 0.5f;
        const f32       H           = Viewport.getHeight() * 0.5f;
        m_C2S.setIdentity();
        m_C2S(0,0) =  W;
        m_C2S(1,1) = -H;
        m_C2S(0,3) =  W + Viewport.m_Left;
        m_C2S(1,3) =  H + Viewport.m_Top;
    }
    return m_C2S;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getC2V( void ) noexcept
{
    if( m_StateFlags.m_FLAGS_C2V )
    {
        m_StateFlags.m_FLAGS_C2V = false;
        m_C2V = getV2C();
        m_C2V.FullInvert();
    }
    return m_C2V;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4&  eng_view::getV2S( void ) noexcept
{
    if( m_StateFlags.m_FLAGS_V2S )
    {
        m_StateFlags.m_FLAGS_V2S = false;
        m_V2S    = getC2S() * getV2C();        
    }
    return m_V2S;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4&  eng_view::getW2C( void ) noexcept
{
    if( m_StateFlags.m_FLAGS_W2C )
    {
        m_StateFlags.m_FLAGS_W2C = false;
        m_W2C    = getV2C() * getW2V();
    }
    return m_W2C;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getW2S( void ) noexcept
{
    if( m_StateFlags.m_FLAGS_W2S )
    {   
        m_StateFlags.m_FLAGS_W2S = false;
        m_W2S    = getV2S() * getW2V();
    }
    return m_W2S;
}

//-------------------------------------------------------------------------------

inline
const xmatrix4& eng_view::getC2W( void ) noexcept
{
    if( m_StateFlags.m_FLAGS_C2W )
    {   
        m_StateFlags.m_FLAGS_C2W = false;
        m_C2W    = getW2C();
        m_C2W.FullInvert();
    }
    return m_C2W;
}

//-------------------------------------------------------------------------------

inline
eng_view& eng_view::LookAt( const xvector3& From, const xvector3& To ) noexcept
{
    return LookAt( From, To, xvector3(0,1,0) );
}

//-------------------------------------------------------------------------------

inline
eng_view& eng_view::LookAt( const xvector3& From, const xvector3& To, const xvector3& Up ) noexcept
{
    UpdatedView();
    m_W2V.LookAt( From, To, Up );
    m_V2W = m_W2V;
    m_V2W.InvertSRT();
    m_StateFlags.m_FLAGS_W2V = false;
    return *this;
}

//-------------------------------------------------------------------------------

inline
void eng_view::setNearZ( const f32 NearZ ) noexcept
{
    m_NearZ = NearZ;
    UpdatedClip();
}

//-------------------------------------------------------------------------------

inline
f32 eng_view::getNearZ( void ) const noexcept
{
    return m_NearZ;
}

//-------------------------------------------------------------------------------

inline
void eng_view::setFarZ( const f32 FarZ ) noexcept
{
    m_FarZ = FarZ;
    UpdatedClip();
}

//-------------------------------------------------------------------------------

inline
f32 eng_view::getFarZ( void ) const  noexcept
{
    return m_FarZ;
}

//-------------------------------------------------------------------------------

inline
void eng_view::setFov( const xradian Fov ) noexcept
{
    //enable use of FOV
    m_StateFlags.m_FLAGS_USE_FOV = true;
    m_Fov = Fov;
    UpdatedClip();
}

//-------------------------------------------------------------------------------

constexpr
xradian eng_view::getFov( void ) const noexcept
{
    return m_Fov;
}

//-------------------------------------------------------------------------------

inline
void eng_view::setAspect( const f32 Aspect ) noexcept
{
    // enable use of FOV
    m_StateFlags.m_FLAGS_USE_FOV = true;
    m_Aspect = Aspect;
    UpdatedClip();
}

//-------------------------------------------------------------------------------

constexpr
f32 eng_view::getAspect( void ) const noexcept
{
    return m_Aspect;
}

//-------------------------------------------------------------------------------

inline
void eng_view::setCustomFrustum( const f32 Left, const f32 Right, const f32 Bottom, const f32 Top ) noexcept
{
    // disable use of FOV
    m_StateFlags.m_FLAGS_USE_FOV = false;

    m_FrustumLeft   = Left;
    m_FrustumRight  = Right;
    m_FrustumBottom = Bottom;
    m_FrustumTop    = Top;

    UpdatedClip();
}

//-------------------------------------------------------------------------------
// this goes back to the FOV method with currently set FOV & Aspect
inline
void eng_view::DisableCustomFrustum( void ) noexcept
{
    // reenable use of FOV
    m_StateFlags.m_FLAGS_USE_FOV = true;

    UpdatedClip();
}

//-------------------------------------------------------------------------------

inline
void eng_view::setViewport( const xirect& Rect ) noexcept
{
    m_Viewport = Rect;
    m_StateFlags.m_FLAGS_VIEWPORT = false;
    UpdatedScreen();
}

//-------------------------------------------------------------------------------

inline
void eng_view::RefreshViewport( void ) noexcept
{
    UpdatedScreen();
}


//-------------------------------------------------------------------------------

constexpr
const xirect& eng_view::getViewport( void ) const noexcept 
{
    return x_assert( m_StateFlags.m_FLAGS_VIEWPORT == false ), m_Viewport;
}

//-------------------------------------------------------------------------------

inline
const xirect& eng_view::getViewport( void ) noexcept 
{
    if( m_StateFlags.m_FLAGS_VIEWPORT )
    {
        // If the view port is not set just return the screen resolution
        m_Viewport.m_Left = 0;
        m_Viewport.m_Top  = 0;
//        eng_GetCurrentContext().GetScreenResolution( m_Viewport.m_Right, m_Viewport.m_Bottom );
        x_assume( false );
    }

    return m_Viewport;
}

//-------------------------------------------------------------------------------
inline
eng_view& eng_view::LookAt( const f32 Distance, const xradian3& aAngles, const xvector3& At ) noexcept
{
    xvector3 From   ( 0, 0, Distance );
    xvector3 WorldUp( 0, 1, 0);

    // Clamp the angles
    xradian3 Angles( aAngles );
    Angles.m_Pitch = x_Max( Angles.m_Pitch, xradian{ -89.0_deg } );
    Angles.m_Pitch = x_Min( Angles.m_Pitch, xradian{  89.0_deg } );
    

    // Set the origin of the camera
    From.RotateX( Angles.m_Pitch );
    From.RotateY( Angles.m_Yaw );
    From = At + From;

    // set the world up
    WorldUp.RotateZ( Angles.m_Roll );

    // build the matrix
    LookAt( From, At, WorldUp );

    return *this;
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getPosition( void ) const noexcept
{
    return getV2W().getTranslation();
}

//-------------------------------------------------------------------------------
inline
void eng_view::setCubeMapView( const s32 Face, const xmatrix4& L2W ) noexcept
{
    xvector3 vLookDir;
    xvector3 vUpDir;
    xvector3 vEye(0,0,0);

    switch( Face )
    {
        case 1: // D3DCUBEMAP_FACE_POSITIVE_X
            vLookDir.setup( 1.0f, 0.0f, 0.0f );
            vUpDir.setup  ( 0.0f, 1.0f, 0.0f );
            break;

        case 0: // D3DCUBEMAP_FACE_NEGATIVE_X
            vLookDir.setup(-1.0f, 0.0f, 0.0f );
            vUpDir.setup  ( 0.0f, 1.0f, 0.0f );
            break;

        case 2: // D3DCUBEMAP_FACE_POSITIVE_Y
            vLookDir.setup( 0.0f, 1.0f, 0.0f );
            vUpDir.setup  ( 0.0f, 0.0f,-1.0f );
            break;

        case 3: // D3DCUBEMAP_FACE_NEGATIVE_Y
            vLookDir.setup( 0.0f,-1.0f, 0.0f );
            vUpDir.setup  ( 0.0f, 0.0f, 1.0f );
            break;

        case 4: // D3DCUBEMAP_FACE_POSITIVE_Z
            vLookDir.setup( 0.0f, 0.0f, 1.0f );
            vUpDir.setup  ( 0.0f, 1.0f, 0.0f );
            break;

        case 5: // D3DCUBEMAP_FACE_NEGATIVE_Z
            vLookDir.setup( 0.0f, 0.0f,-1.0f );
            vUpDir.setup  ( 0.0f, 1.0f, 0.0f );
            break;

        default:
            x_assume( 0 );
    }

    // Make sure that the FOV is set to 90
    // as well as the aspect ratio is set to 1
    // We will let the user set the near and far
    setFov      ( xradian{ 90.0_deg } );
    setAspect   ( 1 );

    // Set the view transform for this cubemap surface
    xmatrix4  NoScales( L2W );
    NoScales.ClearScale();
    vEye     = NoScales * vEye ;
    vLookDir = NoScales * vLookDir;
    vUpDir   = NoScales.RotateVector( vUpDir );
    LookAt( vEye, vLookDir, vUpDir );
}

//-------------------------------------------------------------------------------
inline
void eng_view::setParabolicView( const s32 Side, const xmatrix4& L2W ) noexcept
{
    xvector3 vLookDir;
    xvector3 vUpDir;
    xvector3 vEye(0,0,0);

    switch( Side )
    {
        case 0:
            vLookDir.setup( 0.0f, 0.0f, 1.0f );
            vUpDir.setup  ( 0.0f, 1.0f, 0.0f );
            break;
        case 1: 
            vLookDir.setup( 0.0f, 0.0f,-1.0f );
            vUpDir.setup  ( 0.0f, 1.0f, 0.0f );
            break;

        default:
            x_assume( 0 );
    }

    // Make sure that the FOV is set to 180
    // as well as the aspect ratio is set to 1
    // We will let the user set the near and far
    setFov      ( xradian{ 90.0_deg } );
    setAspect   ( 1 );

    // Set the view transform for this cubemap surface
    xmatrix4  NoScales( L2W );
    NoScales.ClearScale();
    vEye     = NoScales * vEye;
    vLookDir = NoScales * vLookDir;
    vUpDir   = NoScales.RotateVector( vUpDir );
    LookAt( vEye, vLookDir, vUpDir );
}

//-------------------------------------------------------------------------------
inline
void eng_view::setInfiniteClipping( const bool bInfinite ) noexcept
{
    if( m_StateFlags.m_FLAGS_INF_CLIP == bInfinite )
        return;

    // Set the flag
    if( bInfinite ) m_StateFlags.m_FLAGS_INF_CLIP = true;
    else            m_StateFlags.m_FLAGS_INF_CLIP = false;

    // update the cache entries
    UpdatedClip();
}

//-------------------------------------------------------------------------------
inline
bool eng_view::getInfiniteClipping( void ) const noexcept
{
    return m_StateFlags.m_FLAGS_INF_CLIP;
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getWorldZVector( void ) const noexcept
{
    const xmatrix4& V2W = getV2W();
    xvector3        Normal( V2W(0,2), V2W(1,2), V2W(2,2) );
    Normal.Normalize();
    return Normal;
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getWorldXVector( void ) const noexcept
{
    const xmatrix4& V2W = getV2W();
    xvector3        Normal( V2W(0,0), V2W(1,0), V2W(2,0) );
    Normal.Normalize();
    return Normal;
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::getWorldYVector( void ) const noexcept
{
    const xmatrix4& V2W = getV2W();
    xvector3        Normal( V2W(0,1), V2W(1,1), V2W(2,1) );
    Normal.Normalize();
    return Normal;
}

//-------------------------------------------------------------------------------
inline
void eng_view::setPosition( const xvector3& Position  ) noexcept
{
    m_V2W.setTranslation( Position );
    UpdatedView();
}

//-------------------------------------------------------------------------------
inline
void eng_view::setRotation( const xradian3& Rotation ) noexcept
{
    m_V2W.setRotation( Rotation );
    UpdatedView();
}

//-------------------------------------------------------------------------------
inline
void eng_view::Translate( const xvector3& DeltaPos ) noexcept
{
    m_V2W.Translate( DeltaPos );    
    UpdatedView();
}

//-------------------------------------------------------------------------------
inline
xvector3 eng_view::RayFromScreen( const f32 ScreenX, const f32 ScreenY ) noexcept
{
    const xvector3  Distance = getV2CScales( true );
    const xirect&   Viewport = getViewport();

    // Build ray in viewspace.
    const f32       HalfW = (Viewport.m_Left + Viewport.m_Right) * 0.5f;
    xvector3        Ray( -(ScreenX - HalfW ),
                         -(ScreenY - (Viewport.m_Top  + Viewport.m_Bottom) * 0.5f) * m_Aspect,
                         -HalfW*Distance.m_X );

    // Take the ray into the world space
    Ray = getV2W().RotateVector( Ray );
    Ray.Normalize();

    return Ray;
}

//-------------------------------------------------------------------------------
inline
f32 eng_view::getScreenSize( const xvector3& Position, const f32 Radius ) noexcept
{
    const xmatrix4& V2W     = getV2W();
    const xvector3  vPos    = Position - V2W.getTranslation();
    f32             ViewZ   = V2W(0,2) * vPos.m_X + V2W(1,2) * vPos.m_Y + V2W(2,2) * vPos.m_Z;
 
    // Is it completly behind the camera?
    if( ViewZ < -Radius )
        return 0;

    // get the closest distance of the sphere to the camera
    ViewZ = x_Min( ViewZ, Radius );

    // get the biggest scale such the circle will be conservative
    const xmatrix4&     V2C = getV2C();
    const f32           ViewPortScale = x_Max( V2C(0,0), V2C(1,1) );

    // get the diameter of the circle in screen space
    const f32           ScreenRadius  = ( 2 * Radius * ViewPortScale)/ViewZ;

    return ScreenRadius;
}


//-------------------------------------------------------------------------------
inline
void eng_view::UpdateAllMatrices( void ) noexcept
{
    getW2V();
    getW2C();
    getW2S();
    getV2C();
    getV2W();
    getV2S();
    getC2S();
    getC2W();
    getC2V();
    getPixelBased2D();
}

//-------------------------------------------------------------------------------
inline   
xradian3 eng_view::getAngles( void ) const noexcept
{
    return m_V2W.getRotationR3();
}

//-------------------------------------------------------------------------------
inline   
xquaternion eng_view::getRotation( void ) const noexcept
{
    return xquaternion{ m_V2W };
}

//-------------------------------------------------------------------------------
constexpr   
const xmatrix4& eng_view::getMatrix( void )const noexcept
{
    return m_V2W; 
}

//-------------------------------------------------------------------------------
inline   
xvector3 eng_view::getForward( void ) const noexcept
{
    return m_V2W.getForward();
}

//-------------------------------------------------------------------------------

constexpr   const xmatrix4& eng_view::getV2W( void ) const noexcept { return m_V2W;                                                }
constexpr   const xmatrix4& eng_view::getW2V( void ) const noexcept { return x_assert( m_StateFlags.m_FLAGS_W2V == false ), m_W2V; }
constexpr   const xmatrix4& eng_view::getW2C( void ) const noexcept { return x_assert( m_StateFlags.m_FLAGS_W2C == false ), m_W2C; }
constexpr   const xmatrix4& eng_view::getW2S( void ) const noexcept { return x_assert( m_StateFlags.m_FLAGS_W2S == false ), m_W2S; }
constexpr   const xmatrix4& eng_view::getV2C( void ) const noexcept { return x_assert( m_StateFlags.m_FLAGS_V2C == false ), m_V2C; }
constexpr   const xmatrix4& eng_view::getV2S( void ) const noexcept { return x_assert( m_StateFlags.m_FLAGS_V2S == false ), m_V2S; }
constexpr   const xmatrix4& eng_view::getC2S( void ) const noexcept { return x_assert( m_StateFlags.m_FLAGS_C2S == false ), m_C2S; }
constexpr   const xmatrix4& eng_view::getC2W( void ) const noexcept { return x_assert( m_StateFlags.m_FLAGS_C2W == false ), m_C2W; }
constexpr   const xmatrix4& eng_view::getC2V( void ) const noexcept { return x_assert( m_StateFlags.m_FLAGS_C2V == false ), m_C2V; }
constexpr   const xmatrix4& eng_view::getPixelBased2D( void ) const noexcept { return x_assert( m_StateFlags.m_FLAGS_PIXELBASED2D == false ), m_PixelBased2D; }

//-------------------------------------------------------------------------------
inline  
const xmatrix4& eng_view::getParametric2D ( void ) noexcept
{
    static const xmatrix4 Parametric2D = 
    []{
        xmatrix4 M;
        M.setIdentity   ();
        M.setScale      ( xvector3(2.0f,2.0f,1.0f) );
        M.setTranslation( xvector3( -1.0f, -1.0f, 0.1f ) );
        return M;
    }();

    return Parametric2D;
}

//-------------------------------------------------------------------------------
inline  
const xmatrix4& eng_view::getPixelBased2D ( void ) noexcept
{
    if( m_StateFlags.m_FLAGS_PIXELBASED2D )
    {
        m_StateFlags.m_FLAGS_PIXELBASED2D = false;
        const auto  Viewport    = getViewport();
        const f32   XRes        = static_cast<f32>(Viewport.getWidth());
        const f32   YRes        = static_cast<f32>(Viewport.getHeight());
        m_PixelBased2D.setIdentity   ();
        m_PixelBased2D.setScale      ( xvector3( 2.0f/XRes,  2.0f/YRes,  1.0f) );
        m_PixelBased2D.setTranslation( xvector3( -1.0f, -1.0f, 0.1f ) );
    }

    return m_PixelBased2D;
}

 


        