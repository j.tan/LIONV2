//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

//-------------------------------------------------------------------------------------------
constexpr 
eng_draw::vertex::vertex( float X, float Y, float Z, float U, float V, xcolor Color ) noexcept :
    m_Position{ X, Y, Z },
    m_UV{ U, V },
    m_Color{ Color }
{
}

//-------------------------------------------------------------------------------------------
constexpr 
eng_draw::vertex::vertex( const xvector3d& Pos, float U, float V, xcolor Color ) noexcept :
    m_Position{ Pos },
    m_UV{ U, V },
    m_Color{ Color }
{
}

//-------------------------------------------------------------------------------------------
constexpr 
eng_draw::vertex::vertex( const xvector3d& Pos, const xvector2& UV, xcolor Color ) noexcept :
    m_Position{ Pos },
    m_UV{ UV },
    m_Color{ Color }
{
}

//-------------------------------------------------------------------------------------------
constexpr 
eng_draw::vertex::vertex( const xvector3d& Pos, xcolor Color ) noexcept :
    m_Position{ Pos },
    m_UV{ 0, 0 },
    m_Color{ Color }
{
}

//-------------------------------------------------------------------------------------------
constexpr 
eng_draw::vertex::vertex( const xvector3d& Pos ) noexcept :
    m_Position{ Pos },
    m_UV{ 0, 0 },
    m_Color{ 0xffffffff }
{
}

//-------------------------------------------------------------------------------------------
constexpr 
eng_draw::vertex::vertex( float X, float Y, float Z ) noexcept :
    m_Position{ X, Y, Z },
    m_UV{ 0, 0 },
    m_Color{ 0xffffffff }
{
}

//-------------------------------------------------------------------------------------------
constexpr 
eng_draw::vertex::vertex( float X, float Y, float Z, xcolor Color ) noexcept :
    m_Position{ X, Y, Z },
    m_UV{ 0, 0 },
    m_Color{ Color }
{
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::vertex::setup( const xvector3d& Pos, f32 U, f32 V, xcolor Color ) noexcept
{
    m_Position  = Pos;
    m_UV        = {U,V};
    m_Color     = Color;
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::vertex::setup( const xvector3d& Pos, xcolor Color ) noexcept
{
    m_Position  = Pos;
    m_UV        = {0,0};
    m_Color     = Color;
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::vertex::setup( const xvector3d& Pos ) noexcept
{
    m_Position  = Pos;
    m_UV        = {0,0};
    m_Color     = ~0;
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::vertex::setup( float X, float Y, float Z ) noexcept
{
    m_Position.setup( X, Y, Z);
    m_UV        = {0,0};
    m_Color     = ~0;
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::vertex::setup( float X, float Y, float Z, xcolor Color ) noexcept
{
    m_Position.setup( X, Y, Z);
    m_UV        = {0,0};
    m_Color     = Color;
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::vertex::setup( float X, float Y, float Z, float U, float V, xcolor Color ) noexcept
{
    m_Position.setup( X, Y, Z);
    m_UV        = {U,V};
    m_Color     = Color;
}

//-------------------------------------------------------------------------------------------
inline      
void eng_draw::DrawTriangle( const xmatrix4& L2C, const vertex& Vertex1, const vertex& Vertex2, const vertex& Vertex3 ) noexcept
{
    auto Buffers = popBuffers( 3, 3 );
    Buffers.m_Vertices[0] = Vertex1;
    Buffers.m_Vertices[1] = Vertex2;
    Buffers.m_Vertices[2] = Vertex3;
    Buffers.m_Indices[0] = 0;
    Buffers.m_Indices[1] = 1;
    Buffers.m_Indices[2] = 2;
    DrawBufferLines(L2C);
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::DrawTriangle( const xmatrix4& L2C, const xbuffer_view<vertex> Vertices ) noexcept   
{ 
    DrawTriangle( L2C, Vertices[0], Vertices[1], Vertices[2] );     
}

//-------------------------------------------------------------------------------------------
inline      
void eng_draw::DrawLine( const xmatrix4& L2C, const vertex& Vertex1, const vertex& Vertex2 ) noexcept
{
    auto Buffers = popBuffers( 2, 2 );
    Buffers.m_Vertices[0] = Vertex1;
    Buffers.m_Vertices[1] = Vertex2;
    Buffers.m_Indices[0] = 0;
    Buffers.m_Indices[1] = 1;
    DrawBufferLines(L2C);
}

//-------------------------------------------------------------------------------------------
inline      
void eng_draw::DrawLine( const xmatrix4& L2C, const xbuffer_view<vertex> Vertices ) noexcept   
{ 
    DrawLine( L2C, Vertices[0], Vertices[1] );   
}

//------------------------------------------------------------------------------------
inline
void eng_draw::DrawBBox( const xmatrix4& L2C, const xvector3& Center, const xcolor Color  ) noexcept
{
    // Get the thread ID for the worker
    xcolor C = Color;
    C.m_A >>= 1;
    popSolidCube( xbbox{ Center, 0.5f  }, C );
    DrawBufferTriangles( L2C );
    popWireCube( xbbox{ Center, 0.5f  }, Color );
    DrawBufferLines( L2C );
}

//------------------------------------------------------------------------------------
inline
void eng_draw::DrawSphere( const xmatrix4& L2C, const xcolor Color ) noexcept
{
    popSolidUVSphere( 10, 10, Color );
    DrawBufferTriangles( L2C );
}

//------------------------------------------------------------------------------------
inline
void eng_draw::DrawShadedRect( const xmatrix4& L2C, const xrect& Rect, const xcolor TL, const xcolor TR, const xcolor BL, const xcolor BR ) noexcept
{
    auto Buffer = popBuffers( 4, 6 );

    Buffer.m_Vertices[0].setup( Rect.m_Left,  Rect.m_Top,    0, 0, 0, TL );
    Buffer.m_Vertices[1].setup( Rect.m_Right, Rect.m_Top,    0, 1, 0, TR );
    Buffer.m_Vertices[2].setup( Rect.m_Right, Rect.m_Bottom, 0, 1, 1, BR );
    Buffer.m_Vertices[3].setup( Rect.m_Left,  Rect.m_Bottom, 0, 0, 1, BL );

    Buffer.m_Indices[0] = 0;
    Buffer.m_Indices[1] = 2;
    Buffer.m_Indices[2] = 1;
    Buffer.m_Indices[3] = 0;
    Buffer.m_Indices[4] = 3;
    Buffer.m_Indices[5] = 2;

    DrawBufferTriangles( L2C );
}

//------------------------------------------------------------------------------------
inline
void eng_draw::DrawSolidRect( const xmatrix4& L2C, const xrect& Rect, const xcolor Color ) noexcept
{
    DrawShadedRect( L2C, Rect, Color, Color, Color, Color );
}

//------------------------------------------------------------------------------------
inline
void eng_draw::DrawTexturedRect( const xmatrix4& L2C, const xrect& Rect, const xrect& TexCoord, const xcolor Color ) noexcept
{
    auto Buffer = popBuffers( 4, 6 );

    Buffer.m_Vertices[0].setup( Rect.m_Left,  Rect.m_Top,    0, TexCoord.m_Left,  TexCoord.m_Top,     Color );
    Buffer.m_Vertices[1].setup( Rect.m_Right, Rect.m_Top,    0, TexCoord.m_Right, TexCoord.m_Top,     Color );
    Buffer.m_Vertices[2].setup( Rect.m_Right, Rect.m_Bottom, 0, TexCoord.m_Right, TexCoord.m_Bottom,  Color );
    Buffer.m_Vertices[3].setup( Rect.m_Left,  Rect.m_Bottom, 0, TexCoord.m_Left,  TexCoord.m_Bottom,  Color );

    Buffer.m_Indices[0] = 0;
    Buffer.m_Indices[1] = 2;
    Buffer.m_Indices[2] = 1;
    Buffer.m_Indices[3] = 0;
    Buffer.m_Indices[4] = 3;
    Buffer.m_Indices[5] = 2;

    DrawBufferTriangles( L2C );
}

//------------------------------------------------------------------------------------
inline
eng_draw::buffers eng_draw::popSolidCube( const xbbox& BBox, xcolor Color ) noexcept
{
    constexpr const static int    nVertices   { 24            };
    constexpr const static int    nIndices    { 36            };
    auto Buffers = popBuffers( nVertices, nIndices );
    CopySolidCubeData( Buffers.m_Vertices, Buffers.m_Indices, BBox, Color );
    return Buffers;
}

//------------------------------------------------------------------------------------
inline
eng_draw::buffers eng_draw::popWireCube( const xbbox& BBox, xcolor Color ) noexcept
{
    constexpr const static int    nVertices   { 8             };
    constexpr const static int    nIndices    { 12*2          };
    auto Buffers = popBuffers( nVertices, nIndices );
    CopyWireCubeData( Buffers.m_Vertices, Buffers.m_Indices, BBox, Color );
    return Buffers;
}

/*
//-------------------------------------------------------------------------------------------
inline
void eng_draw::Begin( const pipeline PipeLine ) noexcept
{
    m_Engine.DrawBegin( PipeLine );
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::End( void ) noexcept
{
    m_Engine.DrawEnd();
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::DrawTriangles( const xbuffer_view<eng_draw::vertex> Vertex, const xbuffer_view<u16> Index ) noexcept
{
    m_Engine.DrawTriangles( Vertex, Index );
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::DrawLines( const xbuffer_view<eng_draw::vertex> Vertex, const xbuffer_view<u16> Index ) noexcept
{
    m_Engine.DrawLines( Vertex, Index );
}

//-------------------------------------------------------------------------------------------
inline
xbuffer_view<eng_draw::vertex> eng_draw::popBufferVertices( const int nVertices ) noexcept
{
    return m_Engine.popBufferVertices( nVertices );
}

//-------------------------------------------------------------------------------------------
inline
xbuffer_view<u16> eng_draw::popBufferIndices( const int nIndices ) noexcept
{
    return m_Engine.popBufferIndices( nIndices );
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::DrawBufferTriangles( void ) noexcept
{
    m_Engine.DrawBufferTriangles();
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::DrawBufferLines( void ) noexcept
{
    m_Engine.DrawBufferLines();
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::setL2W( const xmatrix4& L2W ) noexcept
{
    m_Engine.DrawSetL2W( L2W );
}

//-------------------------------------------------------------------------------------------
inline
void eng_draw::ClearL2W( void ) noexcept
{
    xmatrix4 L2W;
    L2W.Identity();
    setL2W( L2W );
}

*/




