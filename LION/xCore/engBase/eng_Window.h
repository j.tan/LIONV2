//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_window : public eng_base
{
public:

    enum errors : u8 
    {
        ERR_OK,
        ERR_FAILURE,
        ERR_SYSTEM_WINDOW,
        ERR_FULLSCREEN_NO_SUPPORTED
    };

    using err       = x_err<errors>;
    using handle    = x_ll_share_ref<eng_window>;

    struct setup
    {
        constexpr       setup( eng_device& Device ) noexcept : m_Device{ Device } {}
        
        eng_device&     m_Device;
        bool            m_bFullScreen   { false     };
        int             m_Width         { 1280      };
        int             m_Height        { 720       };
        void*           m_pInstance     { nullptr   };
    };

public:

    virtual     bool                            HandleEvents            ( void )                                                    noexcept = 0;
    virtual     int                             getWidth                ( void ) const                                              noexcept = 0;
    virtual     int                             getHeight               ( void ) const                                              noexcept = 0;
    virtual     void                            PageFlip                ( void )                                                    noexcept = 0;
    virtual     void                            BeginRender             ( const eng_view& View, const bool bUpdateViewPort = true ) noexcept = 0;
    virtual     void                            EndRender               ( void )                                                    noexcept = 0;
    virtual     eng_device&                     getDevice               ( void )                                                    noexcept = 0;
    virtual     eng_display_cmds&               getDisplayCmdList       ( const f32 Order )                                         noexcept = 0;
    virtual     void                            SubmitDisplayCmdList    ( eng_display_cmds& CmdList )                               noexcept = 0;
    virtual     const eng_view&                 getActiveView           ( void ) const                                              noexcept = 0;
    virtual     u64                             getSystemWindowHandle   ( void ) const                                              noexcept = 0;
    virtual     const eng_input::keyboard&      getLocalKeyboard        ( void ) const                                              noexcept = 0;
    virtual     const eng_input::mouse&         getLocalMouse           ( void ) const                                              noexcept = 0;

};

