//----------------------------------------------------------------------------//
//  xCORE -- Copyright @ 2010 - 2016 LIONant LLC.                             //
//----------------------------------------------------------------------------//
// This source file is part of the LIONant core library and it is License     //
// under Apache License v2.0 with Runtime Library Exception.                  //
// You may not use this file except in compliance with the License.           //
// You may obtain a copy of the License at:                                   //
// http://www.apache.org/licenses/LICENSE-2.0                                 //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//----------------------------------------------------------------------------//
#pragma once

class eng_view 
{
    x_object_type( eng_view, is_linear );

public:
    inline                          eng_view                ( void )                                                            noexcept;
    inline      void                setViewport             ( const xirect& Rect )                                              noexcept;
    inline      const xirect&       getViewport             ( void )                                                            noexcept;
    constexpr   const xirect&       getViewport             ( void )                                                    const   noexcept;
    inline      void                RefreshViewport         ( void )                                                            noexcept;
    inline      void                UpdateAllMatrices       ( void )                                                            noexcept;

    inline      xvector3            getPosition             ( void )                                                    const   noexcept;
    inline      xvector3            getV2CScales            ( const bool bInfiniteClipping = true )                             noexcept;
    inline      void                setCubeMapView          ( const s32 Face, const xmatrix4& L2W )                             noexcept;
    inline      void                setParabolicView        ( const s32 Side, const xmatrix4& L2W )                             noexcept;
    inline      void                setInfiniteClipping     ( const bool bInfinite )                                            noexcept;
    inline      bool                getInfiniteClipping     ( void )                                                    const   noexcept;

    inline      void                setNearZ                ( const f32 NearZ )                                                 noexcept;
    inline      f32                 getNearZ                ( void )                                                    const   noexcept;
    inline      void                setFarZ                 ( const f32 FarZ )                                                  noexcept;
    inline      f32                 getFarZ                 ( void )                                                    const   noexcept;
    inline      void                setFov                  ( const xradian Fov )                                               noexcept;
    constexpr   xradian             getFov                  ( void )                                                    const   noexcept;
    inline      void                setAspect               ( const f32 Aspect )                                                noexcept;
    constexpr   f32                 getAspect               ( void )                                                    const   noexcept;
    inline      xradian3            getAngles               ( void )                                                    const   noexcept;
    inline      xquaternion         getRotation             ( void )                                                    const   noexcept;
    constexpr   const xmatrix4&     getMatrix               ( void )                                                    const   noexcept;
    inline      xvector3            getForward               ( void )                                                    const   noexcept;

    //using SetFov or SetAspect counteracts this (and vice/versa):

    inline      void                setCustomFrustum        ( const f32 Left, const f32 Right, const f32 Bottom, const f32 Top) noexcept;
    inline      void                DisableCustomFrustum    ( void )                                                            noexcept; 

    inline      eng_view&           LookAt                  ( const xvector3& From, const xvector3& To)                         noexcept;            
    inline      eng_view&           LookAt                  ( const xvector3& From, const xvector3& To, const xvector3& Up )    noexcept; 
    inline      eng_view&           LookAt                  ( const f32 Distance, const xradian3& Angles, const xvector3& At )  noexcept;

    inline      void                setPosition             ( const xvector3& Position )                                        noexcept;
    inline      void                setRotation             ( const xradian3& Rotation )                                        noexcept;
    inline      void                Translate               ( const xvector3& DeltaPos )                                        noexcept;

    inline      xvector3            RayFromScreen           ( const f32 ScreenX, const f32 ScreenY )                            noexcept;
    inline      xvector3            getWorldZVector         ( void )                                                    const   noexcept;
    inline      xvector3            getWorldXVector         ( void )                                                    const   noexcept;
    inline      xvector3            getWorldYVector         ( void )                                                    const   noexcept;

    inline  static const xmatrix4&  getParametric2D         ( void )                                                            noexcept;     
    inline      const xmatrix4&     getPixelBased2D         ( void )                                                            noexcept;     
    constexpr   const xmatrix4&     getPixelBased2D         ( void )                                                    const   noexcept;     

    constexpr   const xmatrix4&     getV2W                  ( void )                                                    const   noexcept;
    constexpr   const xmatrix4&     getW2V                  ( void )                                                    const   noexcept;
    constexpr   const xmatrix4&     getW2C                  ( void )                                                    const   noexcept;
    constexpr   const xmatrix4&     getW2S                  ( void )                                                    const   noexcept;
    constexpr   const xmatrix4&     getV2C                  ( void )                                                    const   noexcept;
    constexpr   const xmatrix4&     getV2S                  ( void )                                                    const   noexcept;
    constexpr   const xmatrix4&     getC2S                  ( void )                                                    const   noexcept;
    constexpr   const xmatrix4&     getC2W                  ( void )                                                    const   noexcept;
    constexpr   const xmatrix4&     getC2V                  ( void )                                                    const   noexcept;

    inline      const xmatrix4&     getW2V                  ( void )                                                            noexcept;
    inline      const xmatrix4&     getW2C                  ( void )                                                            noexcept;
    inline      const xmatrix4&     getW2S                  ( void )                                                            noexcept;
    inline      const xmatrix4&     getV2C                  ( void )                                                            noexcept;
    inline      const xmatrix4&     getV2S                  ( void )                                                            noexcept;
    inline      const xmatrix4&     getC2S                  ( void )                                                            noexcept;
    inline      const xmatrix4&     getC2W                  ( void )                                                            noexcept;
    inline      const xmatrix4&     getC2V                  ( void )                                                            noexcept;
 
    inline      f32                 getScreenSize           ( const xvector3& Position, const f32 Radius )                      noexcept;

//-------------------------------------------------------------------------------

protected:

    inline      void                UpdatedView             ( void )                                                            noexcept;
    inline      void                UpdatedClip             ( void )                                                            noexcept;
    inline      void                UpdatedScreen           ( void )                                                            noexcept;

//-------------------------------------------------------------------------------

protected:

    X_DEFBITS( state_flags,
                u32,
                0xffffffff,
                X_DEFBITS_ARG( bool, FLAGS_W2V           , 1 ),
                X_DEFBITS_ARG( bool, FLAGS_W2C           , 1 ),
                X_DEFBITS_ARG( bool, FLAGS_W2S           , 1 ),
                X_DEFBITS_ARG( bool, FLAGS_V2C           , 1 ),
                X_DEFBITS_ARG( bool, FLAGS_V2S           , 1 ),
                X_DEFBITS_ARG( bool, FLAGS_C2V           , 1 ),
                X_DEFBITS_ARG( bool, FLAGS_C2S           , 1 ),
                X_DEFBITS_ARG( bool, FLAGS_C2W           , 1 ),
                X_DEFBITS_ARG( bool, FLAGS_PIXELBASED2D  , 1 ),
                X_DEFBITS_ARG( u8,   FLAGS_VIEWPORT      , 8 ),
                X_DEFBITS_ARG( bool, FLAGS_INF_CLIP      , 1 ),
                X_DEFBITS_ARG( bool, FLAGS_USE_FOV       , 1 )
    );

//-------------------------------------------------------------------------------

protected:

    xmatrix4        m_V2W               {};
    xmatrix4        m_W2V               {};
    xmatrix4        m_W2C               {};
    xmatrix4        m_W2S               {};
    xmatrix4        m_V2C               {};
    xmatrix4        m_V2S               {};
    xmatrix4        m_C2S               {};
    xmatrix4        m_C2W               {};
    xmatrix4        m_C2V               {};
    xmatrix4        m_PixelBased2D      {};
    xirect          m_Viewport          {};         // Viewport relative to the screen.
    state_flags     m_StateFlags        {};
    f32             m_NearZ             {};
    f32             m_FarZ              {};
    xradian         m_Fov               {};
    f32             m_Aspect            {};

    // when not using FOV, these are used instead
    f32             m_FrustumLeft       {};
    f32             m_FrustumRight      {};
    f32             m_FrustumBottom     {};
    f32             m_FrustumTop        {};
};


        