-------------------------------------------------------------------------------

### LION engine pre-Alpha
Copyright 2016 LIONant LLC

-------------------------------------------------------------------------------

Introduction:
-------------------------------------------------------------------------------
The LION engine is a C++ Open Source Engine created by LIONant LLC. with a very
friendly Apache 2.0 license. It is designed to create real-time applications
by using techniques meant to take advantage of modern hardware. 
Because important changes in the industry we are porting the engine to conform
with modern standards, such:

* C++14
* New graphics APIs; Vulkan, DX12, Metal
* C++ core guidelines - as general rules to follow.
* VR (Virtual Reality) which focus on latency.   

OS Support:
-------------------------------------------------------------------------------
* Windows, with VS2015
* Android (Pending)
* iOS, with xCode 
* OSX, with xCode 
* Linux (Pending)


Current version ( 0.10 pre-Alpha ):
-------------------------------------------------------------------------------
This is a very early release of the engine - with the objective of sharing the code 
with the community as early possble. It is far from complete at this point.
However the released code can already be used to build applications. 


Feature Areas:
-------------------------------------------------------------------------------
xCore - is a set of low-level libraries that facilitate building cross-platform 
        real-time applications.
        
  * xBase - ( Cross Platform Base ) 
    This is the lowest level library of the engine. It is meant to enhance 
    the standard C++ libraries, and to provide a cross-platform base. 

  * gbLib - ( Game Base Library ) 
    This library deals with game components and entities. The design features true 
    multicore design, so it should scale properly with more CPU cores.
        

How to contribute:        
-------------------------------------------------------------------------------
We would LOVE your contribution. This will help yourself and well as the community. 
Please read the guidelines:
   * CONTRIBUTING.md,  OR the online version at: 
   * https://gitlab.com/LIONant/LIONV2/blob/master/CONTRIBUTING.md

Thanks a lot and have fun!
